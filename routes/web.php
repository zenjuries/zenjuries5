<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\InjuryController;
use App\Http\Controllers\NavController;
use App\Http\Controllers\ZAgentController;
use App\Http\Controllers\ZencareController;
use App\Http\Controllers\ZenployeeController;
use App\Http\Controllers\MobileController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\CommunicationLinkController;
use App\Http\Controllers\StatsController;
use App\Http\Controllers\ActiveCollabController;
use App\Http\Controllers\emailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//AUTH/REGISTRATION


Route::get('/zenjuriesLogin', function(){
	return view('zen_auth.login');
});

Route::get('/agencyRegister', function (){
    return view('zen_auth.agency_register');
})->name("agencyRegister");

Route::post('/agencyRegister', [RegisterController::class, 'registerZAgent'])->name('registerZAgent');

Route::get('/clientRegister', function(){
    return view('zen_auth.client_register');
});

Route::get('/userRegister', function(){
    return view('zen_auth.user_register');
});

//injured user registration through invite
Route::get('/register', function(){
    return view('zen_auth.injured_user_register');
});

Route::get('/billing', function (){
    return view('zen_auth.stripe_billing');
});


Route::post('/registerInjuredUser', [RegisterController::class, 'registerInjuredUser'])->name('registerInjuredUser');

Route::post('/userRegister', [RegisterController::class, 'registerNewUser'])->name('registerNewUser');

Route::middleware(['auth'])->group(function () {

    Route::get('/', [NavController::class, 'getLandingPage']);

    //this route is a catch for the RouteServiceProvider so it redirects to the correct landing page
    Route::get('/dashboard', [NavController::class, 'getLandingPage']);


    Route::get('/zenboard', function(){
        return view('pages.zenboard');
    })->middleware(['hasCompany'])->name('zenboard');



    // //added this because somewhere in the system, there is a /zenjuries_zenboard link that clients are getting.  Need to trace and remove.
    // Route::get('/zenjuries_zenboard', function(){
    //     return view('pages.zenboard');
    // })->middleware(['auth', 'hasCompany'])->name('zenboard');

    Route::get('/companyList', [NavController::class, 'getCompanyList'])->middleware(['agent']);

    Route::get('/selectCompany/{company_id}', [NavController::class, 'selectCompany']);

    Route::post('/selectCompany', [NavController::class, 'selectCompanyPost'])->name('selectCompanyPost');


    Route::get('/paymentWall', function (){
        return view('payments.payment_wall');
    })->name('paymentWall');


    Route::get('/logout', [UserController::class, 'logout']);

     //POLICYHOLDER LIST VIEWS
    Route::get('/admin_list', function(){
        return view('pages.policyholder_lists.zenadmin_list');
    });

    Route::get('/zagent_list', function(){
    return view('pages.policyholder_lists.zagent_list');
    });

    Route::get('/aggregate_list', function(){
    return view('pages.policyholder_lists.aggregate_list');
    });

    Route::get('/zenpro_list', function(){
    return view('pages.policyholder_lists.zenpro_list');
    });

    //ZENPRO
    //make this page the zenpro landing page
    Route::get('/zenpro', function(){
        return view('pages.landing-zenpro');
    });

    //ZAGENT
    Route::get('/zagent', function(){
    return view('pages.landing-zagent');
    })->middleware(['zagent'])->name('zagent');

    Route::post('/newClient', [ZAgentController::class, 'newClient'])
        ->middleware(['zagent'])->name('newClient');

    Route::post('/refreshCompaniesZagent', [ZAgentController::class, 'refreshCompanies'])
        ->middleware(['zagent'])->name('refreshCompaniesZagent');

    //ZENBOARD
    Route::post('/toggleStatsBarSetting', [UserController::class, 'toggleStatsBarSetting'])
        ->name('toggleStatsBarSetting');

    Route::post('/toggleWidget', [UserController::class, 'toggleWidget'])
        ->name('toggleWidget');

    //TREE PAGE
    Route::post('/getTreePosts', [ZencareController::class, 'getTreePosts'])
        ->name('getTreePosts');

    Route::post('/getRecentPosts', [ZencareController::class, 'getRecentPosts'])
        ->name('getRecentPosts');

    Route::post('/updateClaimCosts', [ZencareController::class, 'updateClaimCosts'])
        ->name('updateClaimCosts');

    Route::post('/updateEmployeeStatus', [ZencareController::class, 'updateEmployeeStatus'])
        ->name('updateEmployeeStatus');

    Route::post('/updateAdjuster', [ZencareController::class, 'updateAdjuster'])
        ->name('updateAdjuster');

    Route::post('/updateInjuredEmail', [ZencareController::class, 'updateInjuredEmail'])
        ->name('updateInjuredEmail');

    Route::post('/updateZenGardenEmail', [ZencareController::class, 'updateZenGardenEmail'])
        ->name('updateZenGardenEmail');

    Route::post('/optOutZengarden', [ZencareController::class, 'optOutZengarden'])
        ->name('optOutZengarden');

    Route::post('/updateFinalCost', [ZencareController::class, 'updateFinalCost'])
        ->name('updateFinalCost');

    Route::post('/updateSeverity', [ZencareController::class, 'updateSeverity'])
        ->name('updateSeverity');

    Route::post('/updateInjuryDescription', [ZencareController::class, 'updateInjuryDescription'])
        ->name('updateInjuryDescription');

    Route::post('/newTreePost', [ZencareController::class, 'newTreePost'])
        ->name('newTreePost');

    Route::post('/pauseClaimForLitigation', [ZencareController::class, 'pauseClaimForLitigation'])
        ->name('pauseClaimForLitigation');

    Route::post('/getInjuryList', [ZencareController::class, 'getInjuryList'])
        ->name('getInjuryList');

    //appointments
    Route::post('/addAppointment', [ZencareController::class, 'addAppointment'])
        ->name('addAppointment');

    Route::post('/addAppointmentSummary', [ZencareController::class, 'addAppointmentSummary'])
        ->name('addAppointmentSummary');

    Route::post('/editAppointment', [ZencareController::class, 'editAppointment'])
        ->name('editAppointment');

    Route::post('/addInjuryPhoto', [ZencareController::class, 'addInjuryPhoto'])
        ->name('addInjuryPhoto');

    Route::post('/getInjuryPhotos',[ZencareController::class, 'getInjuryPhotos'])
        ->name('getInjuryPhotos');

    Route::post('/deleteInjuryPhoto',[ZencareController::class, 'deleteInjuryPhoto'])
        ->name('deleteInjuryPhoto');

    Route::post('/updatePhotoDescription',[ZencareController::class, 'updatePhotoDescription'])
        ->name('updatePhotoDescription');

    Route::post('/getInjuryDocument', [ZencareController::class, 'getInjuryDocuments'])
        ->name('getInjuryDocuments');

    Route::post('/addClaimNumber',[ZencareController::class, 'addClaimNumber'])
        ->name('addClaimNumber');

    Route::post('/deliverInjuryAlert',[ZencareController::class, 'deliverInjuryAlert'])
        ->name('deliverInjuryAlert');

    Route::post('/firstOfficialReportDelivered', [ZencareController::class, 'firstOfficialReportDelivered'])
        ->name('firstOfficialReportDelivered');

    Route::post('/resolveInjury', [ZencareController::class, 'resolveInjury'])
        ->name('resolveInjury');

    Route::post('/reopenClaimStatus', [ZencareController::class, 'reopenClaimStatus'])
        ->name('reopenClaimStatus');



    Route::post('/uploadDocument', [ZencareController::class, 'uploadDocument'])
        ->name('uploadDocument');

    Route::post('/getClaimCostHistory', [ZencareController::class, 'getClaimCostHistory'])
        ->name('getClaimCostHistory');
    //PAYMENTS
    //get payment portal
    Route::get('/paymentPortal/{payment_id}', function ($payment_id) {
        return view('payments.paymentPortal', ['payment_id' => $payment_id]);
    })->name('paymentPortal');

    //POST chargeExistingCustomer
    Route::post('/chargeExistingCustomer', [PaymentController::class, 'chargeExistingCustomer'])
        ->name('chargeExistingCustomer');

    //POST createNewStripeCustomer
    Route::post('/createNewStripeCustomer',[PaymentController::class, 'createNewStripeCustomer'])
        ->name('createNewStripeCustomer');

    // Handle Stripe webhook events
    // This will get routed to spatie/laravel-stripe-webhooks
    Route::stripeWebhooks('/webhook/stripe');

    //POST updateCard
    Route::post('/updateCard',[PaymentController::class, 'updateCard'])
        ->name('updateCard');

    //POST createNewSubscription
    Route::post('/createNewSubscription',[PaymentController::class, 'createNewSubscription'])
        ->name('createNewSubscription');

    Route::post('/newPolicyNumber',[CompanyController::class, 'newPolicyNumber'])
        ->name('newPolicyNumber');


    Route::post('/saveInjury', [InjuryController::class, 'saveInjury'])->name('saveInjury');


    Route::get('/weeklysummary/{date?}', function($date = NULL){
        return view('pages.weeklysummary', compact('date'));
    });

    Route::get('/zencarelist/{claim_status?}', function($claim_status = ""){
        return view('pages.zencare_list', compact('claim_status'));
    });

    Route::get('/zencare/{injury_id}/{tab?}', function($injury_id, $tab = "overview"){
        return view('pages.zencare', compact('injury_id', 'tab'));
    })->name('zencare');

    Route::get('/zenisphere', function(){
        return view('pages.zenisphere');
    });

    Route::get('/newinjury', function(){
        return view('pages.new_injury');
    });

    Route::get('/injuryresult', function(){
        return view('pages.injury_submit_result');
    });

    Route::get('/billing', function(){
        return view('pages.billing');
    })->name('billing');

    Route::get('/policyholder_profile', function(){
        return view('pages.policyholder_profile');
    })->name("policyHolderProfile");

    Route::get('/policyholders', function(){
        return view('pages.policyholders');
    });

    Route::get('/messages', function(){
        return view('pages.messages');
    });

    Route::get('/myinfo', function(){
        return view('pages.myinfo');
    });

    Route::get('/zendocs', function(){
        return view('pages.documents');
    });

    Route::get('/stats', [StatsController::class, 'getStatsView']);

Route::get('/zencare/fullscreen/{type}/{id}', function($type, $id){
	return view('pages.fullscreen', compact('type','id'));
});

    Route::get('/printpdfsample', function(){
        return view('pages.printpdf');
    });

    Route::get('/printpdf/{type}/{id}', function($type, $id){
        return view('pages.printpdf', compact('type','id'));
    });


    Route::post('/updateUserTheme', [UserController::class, 'updateUserTheme'])
        ->name('updateUserTheme');


    Route::get('/company', function(){
    return view('pages.company');
    });
    Route::post('/updateCommunicationSettings', [UserController::class, 'updateCommunicationSettings'])
        ->name('updateCommunicationSettings');

    Route::post('/updateCompanyInfo', [CompanyController::class, 'updateCompanyInfo'])
        ->name('updateCompanyInfo');

    Route::post('/updateCompanyBillingInfo', [CompanyController::class, 'updateCompanyBillingInfo'])
        ->name('updateCompanyBillingInfo');
       
    Route::post('/cancelSubscription', [PaymentController::class, 'cancelSubscription'])
        ->name('cancelSubscription');

    Route::post('/updatePolicyInfo', [CompanyController::class, 'updatePolicyInfo'])
        ->name('updatePolicyInfo');

    Route::post('/addNewCarrier', [CompanyController::class, 'addNewCarrier'])
        ->name('addNewCarrier');

    Route::post('/selectZenCarrier', [CompanyController::class, 'selectZenCarrier'])
        ->name('selectZenCarrier');

    Route::post('/changeCompanyOwner', [CompanyController::class, 'changeCompanyOwner'])
        ->name('changeCompanyOwner');

    Route::post('/changeCompanyEmailSubscription', [CompanyController::class, 'changeCompanyEmailSubscription'])
        ->name('changeCompanyEmailSubscription');

    Route::post('/updateCompanyBilling', [CompanyController::class, 'updateCompanyBilling'])
        ->name('updateCompanyBilling');

    Route::post('/updatePushNotificationSettings', [UserController::class, 'updatePushNotificationSettings'])
        ->name('updatePushNotificationSettings');

    Route::get('/cert', function(){
        return view('pages.certificate');
    });

    Route::get('renderLossReport/{start_date}/{end_date}/{claim_type}/{claim_status}',
    [FileController::class, 'renderLossReport']);

    Route::post('/emailAll', [emailController::class, 'emailAll'])
        ->name('emailAll');

    Route::post('/setCompanyColor',[CompanyController::class, 'setCompanyColor']);

    Route::post('/createCompanyLogo',[CompanyController::class, 'createCompanyLogo']);

    Route::post('/createDefaultCompanyLogo',[CompanyController::class, 'createDefaultCompanyLogo']);


    Route::post('/getAgencyStats', [StatsController::class, 'getAgencyStats'])
        ->name('getAgencyStats');

    Route::post('/getCompanyStats', [StatsController::class, 'getCompanyStats'])
        ->name('getCompanyStats');

    Route::post('/getZenboardStats', [StatsController::class, 'getZenboardStats'])
        ->name('getZenboardStats');

    Route::get('/zengarden/zengardenInjuryList', function(){
        return view('zengarden.zengardenInjuryList');
    });

    Route::get('/zengarden/claimLitigated', function(){
        return view('zengarden.zengardenClaimLitigated');
    });

    Route::get('/zengarden/claimResolved', function(){
        return view('zengarden.zengardenClaimResolved');
    });

    Route::get('/zengarden/viewEmployeeExpectations', function(){
        return view('zengarden.viewEmployeeExpectations');
    });

    Route::get('/zengarden/logout', [ZenployeeController::class, 'logout']);

    Route::get('/getZengarden', [NavController::class, 'getZenGarden'])->name('getZenGarden');


    Route::get('/zengarden/home', function () {
        return view('zengarden.zengardenHome');
     });

     Route::get('/zengarden/diary', function () {
         return view('zengarden.zengardenDiary');
     });

     Route::get('/zengarden/schedule', function(){
         return view('zengarden.zengardenSchedule');
     });

     Route::get('/zengarden/myinfo', function(){
         return view('zengarden.zengardenMyInfo');
     });

     Route::get('/zengarden/faq', function(){
         return view('zengarden.zengardenFQA');
     });

     Route::get('/zengarden/settings', function(){
         return view('zengarden.zengardenSettings');
     });

     Route::get('/zengarden/team', function(){
         return view('zengarden.zengardenTeams');
     });

     Route::get('/zengarden/theme', function(){
         return view('zengarden.zengardenTheme');
     });

    Route::get('/communicationLink/currentInjury/{id}', [CommunicationLinkController::class, 'getCurrentInjury'])
        ->name('communicationLink');

    Route::get('/communicationLink/zenboard/{company_id}', [CommunicationLinkController::class, 'getZenboard'])
        ->name('communicationLinkZenboard');

    Route::get('/communicationLink/admin/{company_id}', [CommunicationLinkController::class, 'getAdmin'])
        ->name('communicationLinkAdmin');


    Route::get('downloadDocument', [FileController::class, 'downloadDocument'])
        ->name('downloadDocument');

    Route::post('deleteDocument', [FileController::class, 'deleteDocument'])
        ->name('deleteDocument');


    Route::post('/updateUserShortcuts', [UserController::class, 'updateUserShortcuts'])
        ->name('updateUserShortcuts');


    //TEAMS ROUTES
    Route::get('/getSquadRoster', [InjuryController::class, 'getSquadRoster'])
        ->name('getSquadRoster');

    Route::post('/getTeamList', [InjuryController::class, 'getTeamList'])
        ->name('getTeamList');

    Route::get('/loadSquad', [InjuryController::class, 'loadSquad'])
        ->name('loadSquad');

    Route::post('/postNewSquad', [InjuryController::class, 'postNewSquad'])
        ->name('postNewSquad');

    Route::post('/postNewSquadOld', [InjuryController::class, 'postNewSquadOld'])
        ->name('postNewSquadOld');

    Route::post('/editSquad', [InjuryController::class, 'editSquad'])
        ->name('editSquad');

    Route::post('/pingAgent', [InjuryController::class, 'pingAgent'])
        ->name('pingAgent');

    Route::post('/removeUser', [InjuryController::class, 'removeUser'])
        ->name('removeUser');

    Route::post('/deleteZagentZuser', [InjuryController::class, 'deleteZagentZuser'])
        ->name('deleteZagentZuser');

    Route::post('/replaceZagentZuser', [InjuryController::class, 'replaceZagentZuser'])
        ->name('replaceZagentZuser');

    Route::post('/getZagentList', [InjuryController::class, 'getZagentList'])
        ->name('getZagentList');

    Route::post('/addUser', [InjuryController::class, 'addUser'])
        ->name('addUser');

    Route::post('/renameSquad', [InjuryController::class, 'renameSquad'])
        ->name('renameSquad');

    Route::post('/updateSquadImage', [InjuryController::class, 'updateSquadImage'])
        ->name('updateSquadImage');

    Route::post('/deleteSquad', [InjuryController::class, 'deleteSquad'])
        ->name('deleteSquad');

    Route::post('/myZagentInviteZagent', [ZagentController::class, 'myZagentInviteZagent'])
        ->name('myZagentInviteZagent');

    Route::post('/resendClientInvite', [ZagentController::class, 'resendClientInvite'])
        ->name('resendClientInvite');
});

/* ZENGARDEN */
Route::get('/zenployeesLogin', function(){
    return view('zengarden.zenployeesLogin');
})->middleware(['guest']);


Route::get('/verifyAccountNewPolicyHolder/{token}/{id}/{company_id}', function($token, $id, $company_id){
    //[$token => $token, $id => $id, $company_id => $company_id]
   return view('zen_auth.client_register', compact('token', 'id', 'company_id'));
})->name('getVerifyAccountNewPolicyHolder');

Route::post('/verifyAccountNewPolicyHolder', [RegisterController::class, 'verifyAccountNewPolicyHolder'])
    ->name('verifyAccountNewPolicyHolder');



//REGISTRATION
//company registration through invite
Route::get('/register/company_invite/{token}', function($token){
    return view('registration.register_company_invite', ['token' => $token]);
});

Route::post('/register/company_invite', [CompanyController::class, 'registerCompanyInvite']);

//user registration through invite
Route::get('/register/user_invite/{token}', function($token){
    return view('registration.register_user_invite', ['token' => $token]);
});

//Save the injury data
// Route::post('saveInjury', [
//     'as' => 'saveInjury',
//     'middleware' => ['auth', 'valid'],
//     'uses' => 'AjaxController@saveInjury'
// ]);

//Route::post('/register/user_invite', [UserController::class, 'registerUserInvite']->(['auth', 'valid']));

//re-registration at policy update
Route::get('/register/renewal', function(){
	return view('zen_auth.policyholder_renewal');
});


/* ADMIN */
Route::get('/superadmin', function(){
	return view('admin.superadmin');
});
Route::get('/admin', function(){
	return view('admin.admin');
});
Route::get('/admin_all_users', function(){
	return view('admin.all_users');
});
Route::get('/admin_all_agencies', function(){
	return view('admin.all_agencies');
});
Route::get('/admin_all_policyholders', function(){
	return view('admin.all_policyholders');
});
Route::get('/admin_all_injuries', function(){
	return view('admin.all_injuries');
});
Route::get('/admin_all_reports', function(){
	return view('admin.all_reports');
});
Route::get('/admin_all_alerts', function(){
	return view('admin.all_alerts');
});
Route::get('/admin', function(){
	return view('admin.admin');
});
Route::get('/admin_sendmail', function(){
	return view('admin.sendmail');
});
Route::get('/admin_sendmessage', function(){
	return view('admin.sendmessage');
});
Route::get('/admin_loginmessage', function(){
	return view('admin.loginmessage');
});
Route::get('/admin_system_manager', function(){
	return view('admin.system_manager');
});
Route::get('/admin_zenpro_manager', function(){
	return view('admin.zenpro_manager');
});
Route::get('/admin_litigation_alerts', function(){
	return view('admin.litigation_alerts');
});
Route::get('/admin_version_updates', function(){
	return view('admin.version_updates');
});
Route::get('/admin_zengarden_activiity', function(){
	return view('admin.zengarden_activity');
});
Route::get('/admin_document_manager', function(){
	return view('admin.document_manager');
});




Route::get('/help', function(){
	return view('pages.help');
});

Route::get('/sendmail', function(){
	return view('pages.sendmail');
});

Route::get('/cert', function(){
    return view('pages.certificate');
});

 /*
Route::get('/admin/{company_id}',[
    'as' => 'communicationLink.admin',
    'uses' => 'CommunicationLinkController@getAdmin'
]);

Route::get('/zenboard/{company_id}',[
    'as' => 'communicationLink.zenboard',
    'uses' => 'CommunicationLinkController@getZenboard'
]);

Route::get('/userProfile',[
    'as' => 'communicationLink.userProfile',
    'uses' => 'CommunicationLinkController@getUserProfile'
]);

Route::get('/messages',[
    'as' => 'communicationLink.messages',
    'uses' => 'CommunicationLinkController@getMessages'
]);
*/
Route::post('/zengarden/newMoodPost', [ZenployeeController::class, 'newMoodPost']);

Route::post('/zengarden/newMoodAndNotePost', [ZenployeeController::class, 'newMoodAndNotePost']);

Route::post('/zengarden/newNotePost',[ZenployeeController::class, 'newNotePost']);

Route::post('/zengarden/addAppointment',[ZenployeeController::class, 'addAppointment']);

Route::post('/zengarden/editAppointment', [ZenployeeController::class, 'editAppointment']);

Route::post('/zengarden/getAppointments', [ZenployeeController::class, 'getAppointments']);

Route::post('/zengarden/addAppointmentSummary',[ZenployeeController::class, 'addAppointmentSummary']);

Route::post('/zengarden/updateInfo',[ZenployeeController::class, 'updateUserInfo']);

Route::post('/zengarden/uploadPhoto',[ZenployeeController::class, 'uploadPhoto']);

Route::post('/zengarden/setColor',[ZenployeeController::class, 'setColor']);

Route::post('/zengarden/setIcon',[ZenployeeController::class, 'setIcon']);

Route::post('/zengarden/updateZengardenTheme', [ZenployeeController::class, 'updateZengardenTheme']);

Route::post('/zengarden/createAvatar',[ZenployeeController::class, 'createAvatar']);

Route::post('/zengarden/updatePassword',[ZenployeeController::class, 'updatePassword']);

Route::post('/zengarden/updateAlertSettings', [ZenployeeController::class, 'updateAlertSettings']);

Route::post('/zengarden/updateThemeSettings',[ZenployeeController::class, 'updateThemeSettings']);

/* END ZENGARDEN */

/* DOWNLOAD ROUTES */
Route::get('downloadEmployeeExpectations',[FileController::class, 'downloadEmployeeExpectations']);

Route::get('renderCert/{id}', [FileController::class, 'renderCert']);

Route::get('/viewCert/{company_id}', function($company_id){
    return view('render.renderCert', compact('company_id'));
});


Route::get('renderFirstReport/{injury_id}', [FileController::class, 'renderFirstReport']);

Route::get('summary/{start_date}/{end_date}', [FileController::class, 'renderSummaryReport']
// function(){
//     return view('render.summary');
// }
)->name("summaryReport");

/* END DOWNLOAD ROUTES */

/* DEV/TESTING */

Route::get('/devBS', function(){
	return view('dev.devBS');
});

Route::get('/devRF', function(){
	return view('dev.devRF');
});

Route::get('/devTH', function(){
	return view('dev.devTH');
});

Route::get('/devCH', function(){
	return view('dev.devCH');
});

Route::get('/qatesting', function(){
	return view('feedback.qatesting');
});

Route::get('/feedbacktest', function(){
	return view('feedback.feedbacktest');
});

Route::get('/bugreport', function(){
	return view('pages.bugreport');
});

Route::get('/devLogin', function(){
    if (env('APP_ENV') != 'production'){
        return view('dev.devLogin');
    }
    else {
        $data['title'] = '404';
        $data['name'] = 'Page not found';
        return response()->view('errors.404',$data,404);
    }
});

/* DEV/Email TESTING */
Route::get('/devEmail', function(){
    if (env('APP_ENV') != 'production'){
        return view('dev.devEmail');
    }
    else {
        $data['title'] = '404';
        $data['name'] = 'Page not found';
        return response()->view('errors.404',$data,404);
    }
});

Route::get('/devEmailTemplateView/{template}', function($template){
	return view('dev.devEmailTemplateView', compact('template'));
});

Route::get('/autoLogin/{type}', [UserController::class, 'devLogin']);

Route::get('/dev', function(){
	return view('dev.dev');
});

Route::get('/samples', function(){
	return view('dev.samples');
});

Route::get('/testing', function(){
	return view('dev.testing');
});

Route::get('/fontlist', function(){
	return view('dev.fontlist');
});

Route::get('/zenisphereTest', function(){
    return view('pages.zenisphereTest');
});

//******** MOBILE ROUTES (EXCLUDED FROM CSRF MIDDLEWARE)
Route::post('/mobile/login', [MobileController::class, 'mobileLogin']);
Route::get('/mobile/redirect/{api_token}', [MobileController::class, 'mobileRedirect']);
Route::get('/mobile/sessionExpired', [MobileController::class, 'SessionExpired']);
Route::get('/mobile/checkLogin/{api_token}', [MobileController::class, 'CheckLogin']);
Route::post('/mobile/forgot-password', [MobileController::class, 'store']);
Route::post('/mobile/saveDeviceToken', [MobileController::class, 'saveDeviceToken'])->name('saveDeviceToken');
Route::post('/mobile/manualPushTrigger', [MobileController::class, 'manualPushTrigger'])->name('manualPushTrigger');
Route::post('/mobile/multiUserPushTrigger', [MobileController::class, 'multiUserPushTrigger'])->name('multiUserPushTrigger');
Route::post('/mobile/RogerPushNotification', [MobileController::class, 'RogerPushNotification'])->name('RogerPushNotification');
Route::post('/mobile/JessePushNotification', [MobileController::class, 'JessePushNotification'])->name('JessePushNotification');
Route::get('/mobile/mobileTestPage', function(){
    return view('dev.mobileTestPage');
});


Route::post('/mobile/getUserCompanies', [MobileController::class, 'getUserCompanies']);
Route::post('/mobile/getUserSquad', [MobileController::class, 'getUserSquad']);
Route::post('/mobile/getSquadMembers', [MobileController::class, 'getSquadMembers']);
Route::post('/mobile/saveInjury', [MobileController::class, 'saveInjury']);





require __DIR__.'/auth.php';

//Injury routes
Route::get('/injury', function(){
	return view('pages.new_injury');
});

Route::post('/getCriticalInjuryList', [ZencareController::class, 'getCriticalInjuryList'])
    ->name('getCriticalInjuryList');

Route::post('/getCaseloadInjuries', [ZencareController::class, 'getCaseloadInjuries'])
    ->name('getCaseloadInjuries');

Route::post('/getUnresolvedInjuryList', [ZencareController::class, 'getUnresolvedInjuryList'])
    ->name('getUnresolvedInjuryList');

/* Test Sentry */
Route::get('/test-sentry', function () {
    throw new \Exception("Manually Triggered Exception");
});


/* NFL DEMO */

Route::get('/dashboard_nfl', function(){
	return view('pages.nfl.dashboard_nfl');
});

Route::get('/messages_nfl', function(){
	return view('pages.nfl.messages_nfl');
});

Route::get('/myinfo_nfl', function(){
	return view('pages.nfl.myinfo_nfl');
});

Route::get('/injurylist_nfl', function(){
	return view('pages.nfl.injurylist_nfl');
});

Route::get('/playerinjury_nfl', function(){
	return view('pages.nfl.playerinjury_nfl');
});

Route::get('/help_nfl', function(){
	return view('pages.nfl.help_nfl');
});

Route::get('/injuryteams_nfl', function(){
	return view('pages.nfl.injuryteams_nfl');
});



/**************** Active Collab Routes ****************/
/**************** Active Collab Routes ****************/
Route::post('newSupport', [ActiveCollabController::class, 'postNewTask'])
    ->middleware(['auth'])->name('postNewTask');


//******* JBOX MODAL CONTROLLER ****//

//****** ALL MODALS must be added to the modal array!!!!!! *********

//ZAGENT PAGE
var zagentEditClientModal = new jBox('Modal', {
    attach: '#showModal',
    addClass: 'zBox',
    closeButton: false,
    content: $('#zagentEditClientModal'),
    isolateScroll: true,
    onOpen: function(event){

    }
});

var addClientModal = new jBox('Modal', {
   addClass: 'zBox',
   closeButton: false,
   content: $('#addClientModal'),
   isolateScroll: true,
   onOpen: function(event){

   }
});


//NEW INJURY PAGE


//TREE PAGE
var updateCostsModal = new jBox('Modal', {
   addClass: 'zBox',
   closeButton: false,
   content: $('#updateCostsModal'),
   isolateScroll: true,
   onOpen: function(event){

   }
});

var updateStatusModal = new jBox('Modal', {
    addClass: 'zBox',
    closeButton: false,
    content: $('#updateStatusModal'),
    isolateScroll: true,
    onOpen: function(event){

    }
});



//TEAMS PAGE


//COMPANY PAGE


//PROFILE PAGE

//modals should be initialized as variables.
//****** ALL MODALS must be added to the modal array!!!!!! *********
var modal_array = [zagentEditClientModal, addClientModal, updateCostsModal, updateStatusModal];

console.log(modal_array);

//code to handle the custom close buttons
function closeModals(){
    for(var i = 0; i < modal_array.length; i++){
        modal_array[i].close();
    }
}

$('.close-zModal').on('click', function(){
    console.log('closeModal');
    for(var i = 0; i < modal_array.length; i++){
        modal_array[i].close();
    }
});

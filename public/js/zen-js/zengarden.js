

//****** DIARY PAGE ******//
$('#injuryMoodRange').on('input', function(){
    var mood = $(this).val();
    $('#injuryMoodModal').find('.unsubmitted').attr('class', 'myMood unsubmitted mood-' + mood);
 });
 
 $('#claimMoodRange').on('input', function(){
    var mood = $(this).val();
    $('#claimMoodModal').find('.unsubmitted').attr('class', 'myMood unsubmitted mood-' + mood);
 });
 
 //swaps from the second panel to the update mood view
 $('.updateMood').on('click', function(){
    var parent = $(this).parents('.zg-actionPanel');
    parent.find('.submitted').addClass('testclass').hide();
    parent.find('.unsubmitted').addClass('testclass').show();
 
 });
 
 //swaps between different welcome messages
 function showTitle(){
     var recent_injury_post = 1;
     var recent_claim_post = 1;
 
     $('.zCodeDiaryTitle').hide();
 
     //check if the "no update" state is shown on the mood panels
     if($('#currentInjuryMood').hasClass('mood-0')){
         recent_injury_post = 0;
     }
 
     if($('#currentClaimMood').hasClass('mood-0')){
         recent_claim_post = 0;
     }
 
     //show the correct title
     if(recent_injury_post === 0 && recent_claim_post === 0){
         $('#noUpdateTitle').show();
     }else if(recent_injury_post === 1 && recent_claim_post === 0){
         $('#noClaimExperienceTitle').show();
     }else if(recent_injury_post === 0 && recent_claim_post === 1){
         $('#noInjuryMoodTitle').show();
     }else {
         $('#fullUpdateTitle').show();
     }
 }
 
 //DIARY PAGE UPDATES 
 function resetDiaryModal(modal_id){
     var modal = $('#' + modal_id);
     modal.find('.moodBody').show();
     modal.find('.askNoteBody').hide();
     modal.find('.noteBody').hide();
     modal.find('textarea').val("");
     modal.find('.moodSlider').find('input').val(5);
     modal.find('.unsubmitted').attr('class', 'myMood unsubmitted mood-5');
     
 }
 
 //diary modal tab swapping
 $('#injuryMoodModalSubmitFirstTab').on('click', function(){
     $('#injuryMoodModalTab1').hide();
     $('#injuryMoodModalTab2').show();
 });
 
 $('#addInjuryMoodNote').on('click', function(){
     $('#injuryMoodModalTab2').hide();
     $('#injuryMoodModalTab3').show();
 });
 
 $('#claimMoodModalSubmitFirstTab').on('click', function(){
     $('#claimMoodModalTab1').hide();
     $('#claimMoodModalTab2').show();
 });
 
 $('#addClaimMoodNote').on('click', function(){
     $('#claimMoodModalTab2').hide();
     $('#claimMoodModalTab3').show();
 });
 
 $('.noteTabBackButton').on('click', function(){
     var modal = $(this).parents('.zModal');
     modal.find('.noteBody').hide();
     modal.find('.askNoteBody').show();
 });
 
 
 $('#submitInjuryMood').on('click', function(){
    var mood_val = $('#injuryMoodRange').val();
 
    var submittedPanel = $('#injuryMood').find('.submitted');
    submittedPanel.find('.dateSpan').html(new Date().toDateString());
    submittedPanel.attr('class', 'myMood submitted mood-' + mood_val);
    $('#injurySubmittedMood').html(mood_val);
 
    $('#injuryMood').find('.unsubmitted').hide();
    submittedPanel.show();
    newMoodPost(mood_val, 'injury');
 
 });
 
 $('#submitInjuryMoodNote').on('click', function(){
     /*
     var mood_val = $('#injuryMoodRange').val();
     $('#modalNoteMood').val(mood_val);
     $('#modalNoteType').val('injury');
     diaryNoteModal.open();
     */
     var mood_val = $('#injuryMoodRange').val();
     var note = $('#injuryMoodNoteTextarea').val();
     if(note !== ""){
         newMoodAndNotePost(mood_val, note, "injury");
     }else{
         showAlert("Please enter some text for your note!", "deny", 5);
     }
 });
 
 $('#submitClaimMoodNote').on('click', function(){
     /*
    var mood_val = $('#claimMoodRange').val();
    $('#modalNoteMood').val(mood_val);
    $('#modalNoteType').val("claim");
    diaryNoteModal.open();
    */
    var mood_val = $('#claimMoodRange').val();
    var note = $('#claimMoodNoteTextarea').val();
    if(note !== ""){
        newMoodAndNotePost(mood_val, note, "claim");
    }else{
        showAlert("Please enter some text for your note!", "deny", 5);
    }
 });
 
 $('#submitNoteButton').on('click', function(){
    var note = $('#noteModal').find('textarea').val();
    var type = $('#modalNoteType').val();
    if(note !== ""){
        newNotePost(note);
    }else{
        showAlert("Please enter some text for your note!", "deny", 5);
    }
 });
 
 $('#submitMoodNoteButton').on('click', function(){
     var mood = $('#modalNoteMood').val();
     var type = $('#modalNoteType').val();
 
     var note = $('#noteModal').find('.notePad').val();
 
     //if they didnt submit a note, just update the mood
     if(note !== ""){
         newMoodAndNotePost(mood, note, type);
     }else{
         newMoodPost(mood, type);
     }
 
 });
 
 $('#submitClaimMood').on('click', function(){
     var mood_val = $('#claimMoodRange').val();
     newMoodPost(mood_val, 'claim');
     /*
     var submittedPanel = $('#claimMood').find('.submitted');
     submittedPanel.find('.dateSpan').html(new Date().toDateString());
     submittedPanel.attr('class', 'myMood submitted mood-' + mood_val);
     $('#claimSubmittedMood').html(mood_val);
 
     $('#claimMood').find('.unsubmitted').hide();
     submittedPanel.show();
     */
 
 });
 
 function updateDisplayDiv(type, note, mood){
     //get variables for the correct display div and the note modal 
     if(type === "injury"){
         var display_div = $('#diaryInjury');
         var note_modal = $('#injuryNoteModal');
     }else{
         var display_div = $('#diaryClaim');
         var note_modal = $('#claimNoteModal');
     }
     //if the note is blank, change it to placeholder text
     if(note === ""){
         //display_div.find('.showNoteModal').addClass('ghosted');
         display_div.find('.showNoteModal').hide();
         display_div.find('.noNoteButton').show();
     }else{
         //display_div.find('.showNoteModal').removeClass('ghosted');
         display_div.find('.showNoteModal').show();
         display_div.find('.noNoteButton').hide();
     }
     //set mood
     display_div.find('.myMood').attr('class', 'myMood unsubmitted mood-' + mood);
     //update note
     note_modal.find('.viewNoteModalNote').html(note);
     //set date on thank you message
     var timestamp = new Date().toDateString();
     console.log(timestamp);
     display_div.find('.zd-thankyou').html('<span class="thanks">thanks!</span>you sent your injury mood<br>on ' + timestamp);
     //hide instructions
     display_div.find('.zd-instructions').hide();
     //show thank you message
     display_div.find('.zd-thankyou').show();
     //show buttons
     display_div.find('.buttonSet').show();
     //remove handler causing modal to open on div click
     display_div.unbind();
 
     
 }
 
 function setSecondPanel(type, note, mood){
     if(type === "injury"){
         var second_panel = $('#injuryMood').find('.submitted');
         var first_panel = $('#injuryMood').find('.unsubmitted');
     }else{
         var second_panel = $('#claimMood').find('.submitted');
         var first_panel = $('#claimMood').find('.unsubmitted');
     }
 
     second_panel.find('.dateSpan').html(new Date().toDateString());
     second_panel.attr('class', 'myMood submitted mood-' + mood);
     second_panel.find('.mostRecentNote').val(note);
     if(note === ""){
         second_panel.find('.moodNote').hide();
         second_panel.find('.noMoodNote').show();
     }else{
         second_panel.find('.moodNote').show();
         second_panel.find('.noMoodNote').hide();
     }
 
     first_panel.hide();
     second_panel.show();
 
 
 }
 
 function newMoodPost(mood, type){
 
     $.ajax({
         type: 'POST',
         url: '/zengarden/newMoodPost',
         data: {
             _token: $('#csrfToken').val(),
             injury_id: $('#injuryID').val(),
             user_id: $('#userID').val(),
             mood: mood,
             post_type: type
 
         },
         success: function(){
             closeModals();
             showAlert("Thanks for your update!", "confirm", 5);
             updateDisplayDiv(type, "", mood);
             //setSecondPanel(type, "", mood);
             showTitle();
         },
         error: function(){
             showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
         }
 
     });
 }
 
 function newMoodAndNotePost(mood, note, type){
     $.ajax({
         type: 'POST',
         url: '/zengarden/newMoodAndNotePost',
         data: {
             _token: $('#csrfToken').val(),
             injury_id: $('#injuryID').val(),
             user_id: $('#userID').val(),
             mood: mood,
             type: type,
             note: note
         },
         success: function(){
             closeModals();
             showAlert("Thanks for your update!", "confirm", 5);
             updateDisplayDiv(type, note, mood);
             showTitle();
 
         },
         error: function(){
             closeModals();
             showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
         }
     })
 }
 
 function newNotePost(note, type){
     $.ajax({
         type: 'POST',
         url: '/zengarden/newNotePost',
         data: {
             _token: $('#csrfToken').val(),
             injury_id: $('#injuryID').val(),
             user_id: $('#userID').val(),
             note: note
         },
         success: function(){
             closeModals();
             showAlert("Thanks for your note!", "confirm", 5);
         },
         error: function(){
             closeModals();
             showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
         }
     })
 }
 
 //****** INFO PAGE *****
 $('#infoModalSave').on('click', function(){
    var error_div = $('#infoModalErrors');
    error_div.html("").hide();
    var name = $('#infoModalName').val();
    var email = $('#infoModalEmail').val();
    var phone = $('#infoModalPhone').val();
    phone = phone.replace(/\D/g,'');
    var description = $('#infoModalDescription').val();
 
    var valid = validateInfo(name, email, phone, description);
    if(valid === "valid"){
        updateInfo(name, email, phone, description);
    }else{
         error_div.html(valid).show();
    }
 });
 
 function validateInfo(name, email, phone, description) {
     if(!name){
         return "Name is required!";
     }
     if(!email){
         return "Email is required!";
     }
 
     return "valid";
 }
 
 function updateInfo(name, email, phone, description) {
     $.ajax({
         type: 'POST',
         url: '/zengarden/updateInfo',
         data: {
             _token: $('#csrfToken').val(),
             user_id: $('#userID').val(),
             name: name,
             email: email,
             phone: phone,
             description: description
         },
         success: function (data) {
             $('#myInfoName').html(name);
             $('#myInfoEmail').html(email);
             $('#myInfoPhone').html(phone);
             if(description){
                 $('#myInfoComments').html(description);
             }
             closeModals();
             showAlert("Info Updated!", "confirm", 5);
         },
         error: function (data) {
             console.log(data);
             var errors = data.responseJSON;
             $('#infoModalErrors').html(errors['email']).show();
 
             console.log(errors);
         }
     })
 }
 
 //COLOR PICKER
 var colorPicker = new iro.ColorPicker("#colorpicker", {
     // Set the size of the color picker
     width: 280,
     // Set the initial color to pure red
     color: "#f00"
 });
 
 //save the color, update the display
 $('#colorModalSave').on('click', function(){
    var color = colorPicker.color.hexString;
 
    $.ajax({
       type: 'POST',
        url: '/zengarden/setColor',
        data: {
           _token: $('#csrfToken').val(),
           user_id: $('#userID').val(),
           color: color,
        },
        success: function(){
            closeModals();
 
            var avatar_number = $('#iconModal').find('a.selected').data('avatar');
            //setAvatarOnPage(color, avatar_number);
            createAvatar(color, avatar_number);
 
 
        },
        error: function(){
            showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
        }
 
    });
 });
 
 
 //ICON PICKER
 //set the correct icon as selected by default
 function setStartingAvatar(){
     var current_avatar_number = $('#currentAvatarNumber').val();
     if(typeof current_avatar_number !== 'undefined'){
         $('#iconModal').find('a[data-avatar="' + current_avatar_number +'"').addClass('selected');
     }
 }
 
 //writes the color and icon into the page (checks if icon is selected before writing to page)
 function setAvatarOnPage(color, avatar_number){
     $('.profileAvatarColor').css('background-color', color);
     if($('.iconSelector').find('a.selected').length > 0){
         $('.avatarIcon').css('background-image', "url('/images/avatar_icons/" + avatar_number + ".png')");
     }
 
 }
 
 setStartingAvatar();
 
 
 $('.iconSelector').on('click', 'a', function(){
     $('.iconSelector').find('a').removeClass('selected');
     $(this).addClass('selected');
     //closeModals();
 });
 
 $('#iconModalSave').on('click', function(){
     var avatar = $('.iconSelector').find('a.selected').data('avatar');
     var avatar_number = avatar.replace('avatar', '');
 
     var color = $('.profileAvatarColor').css('background-color');
 
     createAvatar(color, avatar);
 
 });
 
 //this merges the background color with the PNG then sends the result to the server to be saved
 function createAvatar(color, icon_number){
     var canvas = document.createElement("canvas");
     canvas.width = "128";
     canvas.height = "128";
 
     var ctx = canvas.getContext("2d");
     ctx.fillStyle = color;
     ctx.fillRect(0, 0, canvas.width, canvas.height);
 
     var img = document.createElement("img");
     img.setAttribute("src", "/images/avatar_icons/" + icon_number + ".png");
 
     img.onload = function(){
         ctx.drawImage(img, 0, 0);
         img = canvas.toDataURL('image/png');
         $.ajax({
             type: 'POST',
             url: '/zengarden/createAvatar',
             data: {
                 _token: $('#csrfToken').val(),
                 user_id: $('#userID').val(),
                 img: img,
                 avatar_number: icon_number,
                 color: color
             },
             success: function(data){
                 console.log('createAvatar');
                 closeModals();
                 setAvatarOnPage(color, icon_number);
                 console.log(data);
             },
             error: function(data){
                 alert('error');
                 console.log(data);
             }
         })
 
     }
 }
 
 //PHOTO UPLOAD/CROPPIE
 var croppie;
 
 $('#imageModalFileInput').on('change', function(){
     var file = $(this).prop('files')[0];
     console.log(file);
     if(file.type === "image/png" || file.type === "image/jpeg"){
         console.log('in reader');
         var reader = new FileReader();
         reader.onload = function(e){
             $('#croppieDiv').html("");
             croppie = $('#croppieDiv').croppie({
                 viewport: {
                     width: 150,
                     height: 150,
                     type: 'circle'
                 },
                 boundary: {
                     width: 150,
                     height: 150
                 },
                 mouseWheelZoom:false
             });
             croppie.croppie('bind',{
                 url: e.target.result,
                 points: []
             });
         }
         reader.readAsDataURL(file);
     }else{
         showAlert("invalid filetype", "deny", 5);
     }
 
 });
 
 $('#imageModalSave').on('click', function(){
     console.log('saving');
     croppie.croppie('result', 'canvas', 'original').then(function (img) {
         uploadPhoto(img);
     });
 });
 
 //upload a custom photo, remove the selected class from icons
 function uploadPhoto(img){
    var color = $('.profileAvatarColor').css('background-color');
     $.ajax({
         type: 'POST',
         url: '/zengarden/uploadPhoto',
         data: {
             _token: $('#csrfToken').val(),
             user_id: $('#userID').val(),
             img: img,
             color: color
         },
         success: function(data){
             console.log(data);
             closeModals();
             showAlert("Thanks for your upload!", "confirm", 5);
             var image_url = data + "?" + new Date().getTime();
             //$('.avatarIcon').css('background-image', '').css('background-image', 'url(' + image_url + ')');
 
             $('.profileAvatarColor').html('<div class="avatarIcon" style="background-image:url(' + img +')"></div>');
             $('.iconSelector').find('a.selected').removeClass('selected');
             //$('.avatarIcon').css('background-image', '');
             //$('.avatarIcon').css('background-image', img);
 
         },
         failure: function(data){
             closeModals();
             showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
             console.log(data);
         }
     })
 }
 
 //***** SCHEDULE ****
 
 $('#submitSummaryModal').on('click', function(){
     var appointment_id = $('#addSummaryModalAppointmentID').val();
     var summary = $('#addSummaryModal').find('textarea').val();
 
     addAppointmentSummary(appointment_id, summary);
 
 })
 
 $('#setApptBtn').on('click', function(){
     var name = $('#addAppointmentName').val();
     if(name === ""){
         showAlert("The name field is required!", "deny", 10);
         return false;
     }
 
     var email = $('#addAppointmentEmail').val();
     var phone = $('#addAppointmentPhone').val();
 
 /*
     if(email === "" && phone === ""){
         showAlert("You must enter either an email address or a phone number", "deny", 10);
         return false;
     }
 */
     var datetime = datepickerDefault.format("Y-MM-DD hh:mm A");
     var description = $('#addAppointmentNote').val();
     /*
     if(description === ""){
         showAlert("Please enter a brief description of the appointment (X-Rays, Consultation, etc)", "deny", 10);
         return false;
     }
     */
 
     console.log(datetime);
 
     addAppointment(name, email, phone, datetime, description);
 
 });
 
 $('#setFollowBtn').on('click', function(){
     var name = $('#addAppointmentName').val();
     if(name === ""){
         showAlert("The name field is required!", "deny", 10);
         return false;
     }
 
     var email = $('#addAppointmentEmail').val();
     var phone = $('#addAppointmentPhone').val();
 
 /*
     if(email === "" && phone === ""){
         showAlert("You must enter either an email address or a phone number", "deny", 10);
         return false;
     }
 */
     var datetime = datepickerDefault.format("Y-MM-DD hh:mm A");
     var description = $('#addAppointmentNote').val();
     /*
     if(description === ""){
         showAlert("Please enter a brief description of the appointment (X-Rays, Consultation, etc)", "deny", 10);
         return false;
     }
     */
     console.log(datetime);
 
     addAppointment(name, email, phone, datetime, description);
 
 });
 
 function addAppointmentSummary(appointment_id, summary){
     $.ajax({
         type: 'POST',
         url: '/zengarden/addAppointmentSummary',
         data: {
             _token: $('#csrfToken').val(),
             appointment_id: appointment_id,
             summary: summary
         },
         success: function(data){
             $(".apptCard[data-appointmentid='" + appointment_id + "']").find(".apptSummary").html(summary);
             closeModals();
             showAlert("Thanks!", "confirm", 5);
         },
         error: function(data){
             showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
         }
 
     });
 }
 
 function addAppointment(name, email, phone, datetime, description){
     $.ajax({
         type: 'POST',
         url: '/zengarden/addAppointment',
         data: {
             _token: $('#csrfToken').val(),
             injury_id: $('#injuryID').val(),
             name: name,
             email: email,
             phone: phone,
             datetime: datetime,
             description: description,
             user_id: $('#userID').val()
         },
         success: function (data) {
             console.log(data);
             appendNewAppointment(data);
             closeModals();
             showAlert('Thanks for adding your appointment!', 'confirm', 5);
 
         },
         error: function (data){
             showAlert('Sorry, something went wrong. Please try again later', 'deny', 5);
         }
     })
 }
 
 function appendNewAppointment(appointment){
     if(appointment['summary'] == null){
         appointment['summary'] = "";
     }
     
     var html = '<div class="apptCard" data-appointmentid="' + appointment['id'] +'">' +
             '<div class="apptHeader">' +
             '<div class="apptIcon"><div class="icon inline icon-clock"></div></div>' +
             '<div class="apptTime">' + appointment['formatted_date'] + '</div>' +
             '</div>' +
             '<input type="hidden" class="apptTimeFormatted" value="' + appointment['appointment_time'] + '">' +
             '<div class="apptBody">' +
             '<div class="apptDoctor">' + appointment['name'] + '</div>' +
             '<div class="apptPhone">' + appointment['phone'] + '</div>' +
             '<div class="apptEmail">' +
             '<a href="mailto:' + appointment['email'] +'">' +
         appointment['email'] + '</a></div>' +
         '<div class="apptDescription">' + appointment['description'] + '</div>' +
         '<div class="apptSummary">' + appointment['summary'] + '</div>' +
         '<div class="apptControls">' +
             '<button data-appointmentid="' + appointment['id'] + '" class="cyan addSummaryModal">+ summary</button>' +
             '<button data-appointmentid="' + appointment['id'] + '" class="blue addAppointmentModal">+ follow-up</button>' +
             '</div>' + 
             '<div class="apptEdit">' +
                 '<button data-appointmentid="' + appointment['id'] + '" class="gold editAppointmentModal"><div class="icon inline icon-pencil"></div> edit</button></div>' +
             '</div>' +
             '</div>';
 
         //var existing_html = $('.myAppointments.hasList').html();
         
         if(appointment['appointment_status'] === "future"){
             
             $('.myAppointments.hasList').append(html);
             $('.myAppointments.noList').hide();
             $('.myAppointments.hasList').show();
             
         }else{
             $('.pastAppointments.hasList').append(html);
             $('.pastAppointments.noList').hide();
             $('.pastAppointments.hasList').show();
         }
         
         $('.addSummaryModal').off();
 
         var refreshed_summary_modal = new jBox('Modal', {
             attach: '.addSummaryModal',
             addClass: 'zBox',
             closeButton: false,
             content: $('#addSummaryModal'),
             isolateScroll: true,
             onOpen: function(){
                 var button = this.source;
                 $('#addSummaryModalAppointmentID').val(button[0].dataset.appointmentid);
         
             }
         });
         
         modal_array.push(refreshed_summary_modal);
         
         var refreshed_appointment_modal = new jBox('Modal', {
             attach: '.addAppointmentModal',
             addClass: 'zBox',
             closeButton: false,
             content: $('#addAppointmentModal'),
            isolateScroll: true,
             onOpen: function(){
                 setTimeout(function(){
                     datepickerDefault = new MtrDatepicker({
                         target: "zDateTime",
                         animation: true,
                         timestamp: new Date().getTime(),
         
                     });
                 }, 1)
         
                 var button = this.source;
         
                 //followup
                 if(button.hasClass('blue')){
                     $('#setAptTitle').hide();
                     $('#setApptBtn').hide();
         
                     $('#setFollowTitle').show();
                     $('#setFollowBtn').show();
                 }else{
                 //new appt
                     $('#setFollowTitle').hide();
                     $('#setFollowBtn').hide();
         
                     $('#setAptTitle').show();
                     $('#setApptBtn').show();
                 }
         
         
             }
         });
                 
         modal_array.push(refreshed_appointment_modal);		
         
         var refreshed_edit_modal = new jBox('Modal', {
             attach: '.editAppointmentModal',
             addClass: 'zBox',
             closeButton: false,
             content: $('#editAppointmentModal'),
             isolateScroll: true,
             onOpen: function(){
                 var button = this.source;
         
                 var appointment_id = button[0].dataset.appointmentid;
                 
                 var parent = button.closest('.apptCard');
                 var datetime = parent.find('.apptTimeFormatted').val();	
                 
                 setTimeout(function(){
                     console.log(datetime);
                     datepickerDefault = new MtrDatepicker({
                         target: "zDateTimeEditAppointment",
                         animation: true,
                         timestamp: datetime,
         
                     });
                 }, 1);
                 
                 //datepickerDefault.timestamp = 
                 
                 $('#editAppointmentName').val(parent.find('.apptDoctor').html());
                 $('#editAppointmentPhone').val(parent.find('.apptPhone').html());
                 $('#editAppointmentEmail').val(parent.find('.apptEmail').find('a').html());
                 $('#editAppointmentNote').val(parent.find('.apptDescription').html());
                 $('#editAppointmentID').val(appointment_id);
             }
         });
         
         modal_array.push(refreshed_edit_modal);
 }
 
 function clearAppointmentModal(){
     $('#addAppointmentModal').find('input').val("");
     $('#addAppointmentNote').val("");
 
 }
 
 $('#editApptBtnSave').on('click', function(){
     var name = $('#editAppointmentName').val();
     
     if(name === ""){
         showAlert("The name field is required!", "deny", 10);
         return false;
     }
     
     var email = $('#editAppointmentEmail').val();
     var phone = $('#editAppointmentPhone').val();
     var datetime = datepickerDefault.format("Y-MM-DD hh:mm A");
     var description = $('#editAppointmentNote').val();
     var appointment_id = $('#editAppointmentID').val();
     
     $.ajax({
         type: 'POST',
         url: '/zengarden/editAppointment',
         data: {
             _token: $('#csrfToken').val(),
            appointment_id: appointment_id,
            name: name,
            email: email,
            phone: phone,
            datetime: datetime,
            description: description
         },
         success: function(data){
             closeModals();
             var div = $('.apptCard[data-appointmentid="' + appointment_id +'"]');
             div.find('.apptDoctor').html(name);
             div.find('.apptPhone').html(phone);
             div.find('.apptEmail').html("<a href='mailto:" + email +"'>" + email + "</a>");
             div.find('.apptDescription').html(description);
         },
         error: function(data){
             alert('error');
         }
     })
    
 });
 
 /*
 function getAppointments(){
     $.ajax({
         type: 'POST',
         url: '/zengarden/getAppointments',
         data: {
             _token: $('#csrfToken').val(),
             injury_id: $('#injuryID').val(),
         },
         success: function (data){
             console.log(data);
         },
         error: function (data){
             console.log('error refreshing appointments');
             console.log(data);
         }
     });
 }
 
 getAppointments();
 
 */
 
 /*
 
 
 function buildAppointments(data){
     
 }
 */
 
 
 //***** SETTINGS ****
 
 $('#passwordModalSave').on('click', function(){
    var error_div = $('#passwordModalErrors');
    error_div.html("").hide();
 
    var valid = true;
 
    var password = $('#passwordInput').val();
    var password_confirm = $('#confirmPasswordInput').val();
 
    if(password === "" || password_confirm === ""){
        error_div.html("Both fields are required!").show();
        valid = false;
    }
 
    if(password !== password_confirm){
        error_div.html("Please make sure both passwords match!").show();
        valid = false;
    }
 
    if(password.length < 6){
        error_div.html("Your password must be at least 6 characters long!").show();
        valid = false;
    }
    if(valid === true){
        updatePassword(password, password_confirm);
    }
 });
 
 function updatePassword(password, password_confirmation){
     $.ajax({
         type: 'POST',
         url: '/zengarden/updatePassword',
         data: {
             _token: $('#csrfToken').val(),
             user_id: $('#userID').val(),
             password: password,
             password_confirmation: password_confirmation
         },
         success: function(){
             closeModals();
             $('#passwordModal').find('input').val("");
             showAlert("Your password has been updated!", "confirm", 5);
         },
         error: function(data){
             console.log(data.responseText);
             showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
         }
     })
 }
 
 $('.alertSettingsButton').on('click', function(){
    $('.alertSettingsButton').removeClass('selected').removeClass('blue');
    $(this).addClass('selected blue');
 });
 
 $('.alertsButton').on('click', function(){
     var setting = $(this).data('alerts');
     updateAlertSettings(setting);
     $('.alertsButton').removeClass('green cyan');
     if(setting === "none"){
         $(this).addClass('cyan');
     }else if(setting === "full"){
         $(this).addClass('green');
     }
 });
 
 $('#alertModalSave').on('click', function(){
     var setting = $('.alertSettingsButton.selected').data('alerts');
     updateAlertSettings(setting);
 });
 
 function updateAlertSettings(alert_setting){
     $.ajax({
         type: 'POST',
         url: '/zengarden/updateAlertSettings',
         data: {
             _token: $('#csrfToken').val(),
             user_id: $('#userID').val(),
             setting: alert_setting
         },
         success: function(){
             closeModals();
             showAlert("Your settings have been updated!", "confirm", 5);
         },
         error: function(){
             closeModals();
             showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
         }
     });
 }
 
 /*** THEME SWAPPING ***/
 
 /*
 $('#switchTemplateButton').on('click', function(){
    var theme = $(this).data('theme');
 
    if(theme === "dark"){
        $(this).data('theme', 'light');
        $(this).html("light mode");
        updateThemeSettings('light');
        $('body').removeClass('dark').addClass('light');
    }else if(theme === "light"){
        $(this).data('theme', 'dark');
        $(this).html('dark mode');
        $('body').removeClass('light').addClass('dark');
        updateThemeSettings('dark');
    }
 
 });
 */
 
 $('.themeButton').on('click', function(){
     var new_theme = $(this).data('theme');
     changeTheme(new_theme);
 });
 
 function changeTheme(theme){
     $.ajax({
         type: 'POST',
         url: '/zengarden/updateZengardenTheme',
         data: {
             _token: $('#csrfToken').val(),
             user_id: $('#userID').val(),
             theme: theme
         },
         success: function(){
             window.location.href = "/zengarden/settings";
             //showAlert("Your settings have been updated!", "confirm", 5);
             //redirect back to settings page
         },
         error: function(){
             showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
         }
     })
 }
 
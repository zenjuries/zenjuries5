$('#submitLogin').on('click', function(){
    $('#zenployeesLoginForm').submit();
});

//submit the form by pressing enter while focus is on either field
$('#zenployeesLoginForm').on('keyup', function(event){
    //get the key code for the key pressed, 13 is the code for the enter key
    var key = event.which;
    if(key == 13){
        $('#submitLogin').click();
    }

});
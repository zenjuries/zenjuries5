//HOME PAGE

//DIARY PAGE

//injury mood modal
var injuryMoodModal = new jBox('Modal', {
	attach: '.openInjuryModal',
	addClass: 'zBox',
	closeButton: false,
	content: $('#injuryMoodModal'),
	isolateScroll: true, 
	onOpen: function(event){
		var button = this.source;
		
		
	},
	onCloseComplete: function(event){
		resetDiaryModal("injuryMoodModal");
	}
});

var claimMoodModal = new jBox('Modal', {
	attach: '.openClaimModal',
	addClass: 'zBox',
	closeButton: false,
	content: $('#claimMoodModal'),
	isolateScroll: true,
	onOpen: function(event){
		var button = this.source;
	},
	onCloseComplete: function(event){
		resetDiaryModal("claimMoodModal");
	}
})

var injuryNoteModal = new jBox('Modal', {
	attach: '#showInjuryNoteModal',
	addClass: 'zBox',
	closeButton: false,
	content: $('#injuryNoteModal'),
	isolateScroll: true,
	onOpen: function(event){
		var button = this.source;
	}
});

var claimNoteModal = new jBox('Modal', {
	attach: '#showClaimNoteModal',
	addClass: 'zBox',
	closeButton: false,
	content: $('#claimNoteModal'),
	isolateScroll: true,
	onOpen: function(event){
		var button = this.source;
	}
});

/*
var diaryNoteModal = new jBox('Modal', {
    attach: '.showNoteModal',
    addClass: 'zBox',
    closeButton: false,
    content: $('#noteModal'),
    isolateScroll: true,
    onOpen: function(event){
        var button = this.source;

        //console.log(button);
        //$('#modalNoteType').val(button[0].dataset.notetype);
        $('#noteModal').find('textarea').val("");

    }
});
*/

var viewNoteModal = new jBox('Modal', {
    attach: '.viewNoteModalButton',
    addClass: 'zBox',
    closeButton: false,
    content: $('#viewNoteModal'),
    isolateScroll: true,
    onOpen: function(event){
        var button = this.source;

        //button.parents('.myMood').hide();

        var note = button.parents('.myMood').find('.mostRecentNote').val();
       // alert(note);

        $('.viewNoteModalNote').html(note);

    }
});


//SCHEDULE PAGE
var addAppointmentModal = new jBox('Modal', {
    attach: '.addAppointmentModal',
    addClass: 'zBox',
    closeButton: false,
    content: $('#addAppointmentModal'),
   isolateScroll: true,
    onOpen: function(){
        setTimeout(function(){
            datepickerDefault = new MtrDatepicker({
                target: "zDateTime",
                animation: true,
                timestamp: new Date().getTime(),

            });
        }, 1)

        var button = this.source;

        //followup
        if(button.hasClass('blue')){
            $('#setAptTitle').hide();
            $('#setApptBtn').hide();

            $('#setFollowTitle').show();
            $('#setFollowBtn').show();
        }else{
        //new appt
            $('#setFollowTitle').hide();
            $('#setFollowBtn').hide();

            $('#setAptTitle').show();
            $('#setApptBtn').show();
        }


    }
});

var editAppointmentModal = new jBox('Modal', {
	attach: '.editAppointmentModal',
	addClass: 'zBox',
	closeButton: false,
	content: $('#editAppointmentModal'),
	isolateScroll: true,
	onOpen: function(){
		
		
		var button = this.source;
		
		var appointment_id = button[0].dataset.appointmentid;
		
		var parent = button.closest('.apptCard');
		var datetime = parent.find('.apptTimeFormatted').val();	
		
		setTimeout(function(){
			console.log(datetime);
            datepickerDefault = new MtrDatepicker({
                target: "zDateTimeEditAppointment",
                animation: true,
                timestamp: datetime,

            });
        }, 1);
		
		//datepickerDefault.timestamp = 
		
		$('#editAppointmentName').val(parent.find('.apptDoctor').html());
		$('#editAppointmentPhone').val(parent.find('.apptPhone').html());
		$('#editAppointmentEmail').val(parent.find('.apptEmail').find('a').html());
		$('#editAppointmentNote').val(parent.find('.apptDescription').html());
		$('#editAppointmentID').val(appointment_id);
	}
});

var addSummaryModal = new jBox('Modal', {
    attach: '.addSummaryModal',
    addClass: 'zBox',
    closeButton: false,
    content: $('#addSummaryModal'),
    isolateScroll: true,
    onOpen: function(){
        var button = this.source;
        $('#addSummaryModalAppointmentID').val(button[0].dataset.appointmentid);

    }
});

//INFO PAGE
var colorModal = new jBox('Modal', {
    attach: '.showColorModal',
    addClass: 'zBox',
    closeButton: false,
    content: $('#colorModal'),
    isolateScroll: true,
    onOpen: function(){
    }
});

var infoModal = new jBox('Modal', {
    attach: '.showInfoModal',
    addClass: 'zBox',
    closeButton: false,
    content: $('#infoModal'),
    isolateScroll: true,
    onOpen: function(){
    }
});

var imageModal = new jBox('Modal', {
    attach: '.showImageModal',
    addClass: 'zBox',
    closeButton: false,
    content: $('#imageModal'),
    isolateScroll: true,
    onOpen: function(){
        iconModal.close();
    }
});

var iconModal = new jBox('Modal', {
    attach: '.showIconModal',
    addClass: 'zBox',
    closeButton: false,
    content: $('#iconModal'),
    isolateScroll: true,
    onOpen: function(){
    }
});

//INJURY PAGE
var teamModal = new jBox('Modal', {
    attach: '#showTeamModal',
    addClass: 'zBox',
    closeButton: false,
    content: $('#teamModal'),
    isolateScroll: true,
    onOpen: function () {
    }
});

var whatsNextModal = new jBox('Modal', {
    attach: '#whatsNextButton',
    addClass: 'zBox',
    closeButton: false,
    content: $('#whatsNextModal'),
    isolateScroll: true,
    onOpen: function () {

    }
});

//SETTINGS
var passwordModal = new jBox('Modal', {
    attach: '#passwordButton',
    addClass: 'zBox',
    closeButton: false,
    content: $('#passwordModal'),
    isolateScroll: true,
    onOpen: function () {

    }
});

var alertsModal = new jBox('Modal', {
    attach: '#alertsButton',
    addClass: 'zBox',
    closeButton: false,
    content: $('#alertsModal'),
    isolateScroll: true,
    onOpen: function () {

    }
});

//FAQ


var modal_array = [colorModal, infoModal, imageModal, iconModal,
    addAppointmentModal, addSummaryModal, editAppointmentModal, teamModal, whatsNextModal, passwordModal,
    alertsModal, viewNoteModal, injuryMoodModal, claimMoodModal, injuryNoteModal, claimNoteModal];

function closeModals(){
    for(var i = 0; i < modal_array.length; i++){
        modal_array[i].close();
    }
}

$('.close-zModal').on('click', function(){
    for(var i = 0; i < modal_array.length; i++){
        modal_array[i].close();
    }
});
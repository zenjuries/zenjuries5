@extends('layouts.zen5_zengarden_zp_layout_no_nav')
@section('maincontent')
    <?php
    $injuries = \App\Injury::where('injured_employee_id', Auth::user()->id)->get();


    ?>

    <style>
        .injuryListPanel{
            height: 100px;
            width: 200px;
        }
    </style>

<div class="pageContent bgimage-bgheader bghaze-ocean fade-in">
		<!--**********-->
	<div class="headerBlock">
		<div class="headerImage wow">
			<div class="headerImage__panel-small">
				<img class="headerImage__panelimage-small" src="/images/clipart/myinfo-o.png">
			</div>
		</div>
		<div class="container">
			<div class="pageImageHeader wow">
				<div class="pageImageHeader__title txt__white">
					<span>choose claim</span>
				</div>
				<div class="pageImageHeader__subTitle txt__white" style="border-color: var(--zencare);">
					<span>choose the claim you'd like to view</span>
				</div>
			</div>
		</div>
	</div>


        <div class="contentBlock bg__transparent txt__white">
            <div class="container">
                <div class="boxGrid">
                    <div class="boxGrid__box box-size-20"></div>
                    <div class="boxGrid__box box-size-60">
                        <div class="boxContent">
                            <div class="zg-actionPanel">

                                <div class="panelTitle"><div class="icon inline icon-calendar-plus-o"></div> my claims</div>
                                <hr class="subtle">
                                <div class="zAppointments">
                                    <div>
                                        @foreach($injuries as $injury)
                                            <div class="apptCard injuryListPanel" data-injuryid="{{$injury->id}}" style="height: auto; width: 100%">
                                                <div class="apptHeader">
                                                    <div class="apptIcon"><div class="icon inline icon-clock"></div></div>
                                                    <div class="apptTime">{{$injury->injury_date}}</div>
                                                </div>
                                                <div class="apptBody">
                                                    <div class="apptDoctor">Injury Type: {{$injury->printInjuryTypes()}}</div>
                                                    <div class="apptDoctor">Injury Location: {{$injury->printInjuryLocations()}}</div>
                                                    <div class="apptDoctor">Adjuster Name: {{(is_null($injury->adjuster_name) ? "Not Set" : $injury->adjuster_name)}}</div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="boxGrid__box box-size-20"></div>
                </div>
            </div>
        </div>
    </div>



    <script>
        $('.injuryListPanel').on('click', function(){
           var injury_id = $(this).data('injuryid');
           $.ajax({
            type: 'GET',
                url: '{{ route("getZenGarden") }}',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: injury_id
        
                },
                success: function(){
                    //showAlert("Thanks for your update!", "confirm", 5);
                    window.location.href = "/zengarden/home";
                },
                error: function(){
                    showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
                }
           });
           /*
           window.location.href = "/zengarden/chooseClaim/" + injury_id;
           */
        });
    </script>
@endsection
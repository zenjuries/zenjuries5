@extends('layouts.zen5_zengarden_zp_layout')
@section('maincontent')

<div class="pageContent bgimage-bgheader fade-in">
	<div class="headerBlock">
		<div class="headerImage">
			<div class="headerImage__panel-small">
				<img class="headerImage__panelimage-small" src="/images/clipart/zengarden-o.png">
			</div>
		</div>
		<div class="container">
			<div class="pageImageHeader">
				<div class="pageImageHeader__title txt__white">
					<span>my theme</span>
				</div>
				<div class="pageImageHeader__subTitle txt__white" style="border-color: var(--zencare);">
					<span>choose an enjoyable theme</span>
				</div>
			</div>
		</div>
	</div>

<div class="container">
	<div class="settingsButtonArray">
		<div class="buttonSet" style="background-image:url('/images/themes/bamboo.jpg');background-position:left bottom;">
			<span class="themeLabelbg themeColor-bbg-Bamboo">bamboo</span>
			<button class="themeColor-bg-Bamboo themeButton" data-theme="themeBamboo">set bamboo</button>
		</div>
		<div class="buttonSet" style="background-image:url('/images/themes/beach.jpg');background-position:center bottom;">
			<span class="themeLabelbg themeColor-bbg-Beach">beach</span>
			<button class="themeColor-bg-Beach themeButton" data-theme="themeBeach">set beach</button>
		</div>		
		<div class="buttonSet" style="background-image:url('/images/themes/redleaf.jpg');background-position:right bottom;">
			<span class="themeLabelbg themeColor-bbg-Redleaf">redleaf</span>
			<button class="themeColor-bg-Redleaf themeButton" data-theme="themeRedleaf">set redleaf</button>
		</div>
		<div class="buttonSet" style="background-image:url('/images/themes/ocean.jpg');background-position:left bottom;">
			<span class="themeLabelbg themeColor-bbg-Ocean">ocean</span>
			<button class="themeColor-bg-Ocean themeButton" data-theme="themeOcean">set ocean</button>
		</div>
		<div class="buttonSet" style="background-image:url('/images/themes/sand.jpg');background-position:left bottom;">
			<span class="themeLabelbg themeColor-bbg-Sand">sand</span>
			<button class="themeColor-bg-Sand themeButton" data-theme="themeSand">set sand</button>
		</div>
		<div class="buttonSet" style="background-image:url('/images/themes/zenstone.jpg');background-position:right bottom;">
			<span class="themeLabelbg themeColor-bbg-Zenstone">zenstone</span>
			<button class="themeColor-bg-Zenstone themeButton" data-theme="themeZenstone">set zenstone</button>
		</div>	
		<div class="buttonSet" style="background-image:url('/images/themes/lotus.jpg');background-position:right bottom;background-position-y:230px;">
			<span class="themeLabelbg themeColor-bbg-Lotus">lotus</span>
			<button class="themeColor-bg-Lotus themeButton" data-theme="themeLotus">set lotus</button>
		</div>
		<div class="buttonSet" style="background-image:url('/images/themes/island.jpg');background-position:left bottom;background-position-y:270px;">
			<span class="themeLabelbg themeColor-bbg-Serenity">serenity</span>
			<button class="themeColor-bg-Serenity themeButton" data-theme="themeSerenity">set serenity</button>
		</div>		
		<div class="buttonSet" style="background-image:url('/images/themes/autumn.jpg');background-position:center bottom;">
			<span class="themeLabelbg themeColor-bbg-Autumn">autumn</span>
			<button class="themeColor-bg-Autumn themeButton" data-theme="themeAutumn">set autumn</button>
		</div>	
		<div class="buttonSet" style="background-image:url('/images/themes/sky.jpg');background-position:center bottom;">
			<span class="themeLabelbg themeColor-bbg-Sky">sky</span>
			<button class="themeColor-bg-Sky themeButton" data-theme="themeSky">set sky</button>
		</div>
		<div class="buttonSet" style="background-image:url('/images/themes/purplehaze.jpg');background-position:center bottom;background-position-y:170px;">
			<span class="themeLabelbg themeColor-bbg-Purplehaze">purplehaze</span>
			<button class="themeColor-bg-Purplehaze themeButton" data-theme="themePurplehaze">set purplehaze</button>
		</div>												
	</div>
</div>
<a href="/zengarden/settings" class="invisible"><button id="switchThemeButton">back</button></a>
</div>

@endsection
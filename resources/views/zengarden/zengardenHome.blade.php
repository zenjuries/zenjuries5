@extends('layouts.zen5_zengarden_zp_layout') 
@section('maincontent')

<?php
//pull injury info here
if(\App\Injury::where('injured_employee_id', Auth::user()->id)->get()){
	if(empty(session('injury_id'))){
		return redirect('/zengarden/zengardenInjuryList');
	}
}
if(session('injury_id')){
    $injury = \App\Injury::where('id', session('injury_id'))->first();
}else{
    $injury = \App\Injury::where('injured_employee_id', Auth::user()->id)->first();
} ?>

<div class="pageContent themeImage fade-in">
	<div class="mUserStatus">status: <span>{{$injury->employee_status !== NULL ? $injury->employee_status : "None Set"}}</span></div>
		<!--**********-->
	<div class="headerBlock headerFade">
		<div class="headerImage">
			<div class="headerImage__panel-small">
				<div onclick="PlayHappyClick();location.href='/zengarden/myinfo';" class="userAvatar" style="background-image:url({{Auth::user()->picture_location}});background-color:{{Auth::user()->avatar_color}};"></div>
				<div class="defaultHomeUser">
					<div class="userFirstname">{{ explode(' ', Auth::user()->name)[0] }}</div>
					<div class="userLastname">{{ explode(' ', Auth::user()->name)[1] }}</div>
					<div class="userStatus">status: <span>{{$injury->employee_status !== NULL ? $injury->employee_status : "None Set"}}</span></div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="pageImageHeader">
				<div class="pageImageHeader__title txt__white">
					<span class="defaultHomeUser">zenGarden home @if(Auth::user()->checkInjuredUserForInjuryTeams()) Injured on team @else Injured not on team @endif</span>
					<div class="mobileHomeUser">
						<div class="mUserFirstname">{{ explode(' ', Auth::user()->name)[0] }}</div>
				<div class="mUserLastname">{{ explode(' ', Auth::user()->name)[1] }}</div>
					</div>
				</div>
				<div class="pageImageHeader__subTitle txt__white">
					<span>What would you like to do today?</span>
				</div>
			</div>
		</div>
	</div>

	<div class="contentBlock txt__black">
		<div class="container">
			<div class="homeButtons">
				<div class="itemGrid animate__animated animate__bounceIn">			
					<div class="itemGrid__nav"><a class="navbbg-1 invisible" href="/zengarden/diary" onclick="PlayChirpClick()"><span class="navImg"><img src="https://www.zenployees.com/images/clipart/diary-o.png"></span><span class="subText">update my</span><span class="mainText">Diary</span></a></div>
					<div class="itemGrid__nav"><a class="navbbg-2 invisible" href="/zengarden/schedule" onclick="PlayChirpClick()"><span class="navImg"><img src="https://www.zenployees.com/images/clipart/schedule-o.png"></span><span class="subText">view my</span><span class="mainText">Schedule</span></a></div>
					<div class="itemGrid__nav"><a class="navbbg-3 invisible" href="/zengarden/team" onclick="PlayChirpClick()"><span class="navImg"><img src="https://www.zenployees.com/images/clipart/medical-o.png"></span><span class="subText">about my</span><span class="mainText">Injury</span></a></div>														
					<div class="itemGrid__nav"><a class="navbbg-4 invisible" href="/zengarden/myinfo" onclick="PlayChirpClick()"><span class="navImg"><img src="https://www.zenployees.com/images/clipart/myinfo-o.png"></span><span class="subText">check out</span><span class="mainText">My Info</span></a></div>
					<div class="itemGrid__nav"><a class="navbbg-5 invisible" href="/zengarden/settings" onclick="PlayChirpClick()"><span class="navImg"><img src="https://www.zenployees.com/images/clipart/tools-o.png"></span><span class="subText">change my</span><span class="mainText">Settings</span></a></div>
					<div class="itemGrid__nav"><a class="navbbg-6 invisible" href="/zengarden/faq" onclick="PlayChirpClick()"><span class="navImg"><img src="https://www.zenployees.com/images/clipart/help-o.png"></span><span class="subText">I need some</span><span class="mainText">Help</span></a></div>												
				</div>
			</div>			
		</div>
	</div>

</div>

@endsection

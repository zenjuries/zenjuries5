@extends('layouts.zen5_zengarden_zp_layout')
@section('maincontent')

<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="https://www.zenployees.com/js/parallax.js"></script>

<div class="pageContent bgimage-bgheader bghaze-peach fade-in">
		<!--**********-->
	<div class="headerBlock">
		<div class="headerImage wow animate__animated animate__bounceInRight">
			<div class="headerImage__panel-small">
				<img class="headerImage__panelimage-small" src="/images/clipart/help-o.png">
			</div>
		</div>
		<div class="container">
			<div class="pageImageHeader wow animate__animated animate__bounceInLeft">
				<div class="pageImageHeader__title txt__white">
					<span>zengarden help</span>
				</div>
				<div class="pageImageHeader__subTitle txt__white" style="border-color: var(--zencare);">
					<span>information about your zengarden</span>
				</div>
			</div>
		</div>
	</div>

	<div class="contentBlock bg__transparent txt__white">
		<div class="container">
			<div class="faq">
				<ul>
					<li>
					<div class="question">I'm hurt at work.  What do I do now?</div>
					<div class="answer">Your employer has been funding a program designed to protect, nurture and care for you in situations just like this, <i>worker's compensation</i>. Furthermore, your employer has also chosen to enroll in the state-of-the-art injury management program, Zenjuries. Right now, a team of professionals is working hard on your wellbeing and will be communicating with you and each other until your injury is completely resolved...no matter how long it takes. </div>
					</li>					
					
					<li>
					<div class="question">Should I be scared to deal with worker's comp?</div>
					<div class="answer">DO NOT be Afraid! You are inside of a program that is bound by federal statutes governed by each states laws. These laws protect and care for you and your family during your injury. These commitments have begun, and are already underway hard at work for you. Your employer truly cares and is already helping you. </div>
					</li>

					<li>
					<div class="question">Now that I'm injured and in a system, will I be forgotten?</div>
					<div class="answer">Not with Zenployers!  You have the ability to promptly report and communicate every update of your condition and your feelings to your team...all the way to the end. They care deeply and need you to keep them “in the loop”. Your experiences, needs, and desires must be clearly communicated every chance you get to share with them. </div>
					</li>

					<li>
					<div class="question">What if I need more care?</div>
					<div class="answer">If your injury is an emergency, you’ll go where the ambulance takes you. In a nonemergency, your employer may direct you to a particular hospital, clinic or doctor. In most states, you must go where your employer tells you or there is a risk that your bills may not be covered by workers’ compensation. In all cases, try to work as “one team” with your employer’s program.</div>
					</li>

					<li>
					<div class="question">I don't really know anything about this.  How much will it cost me?</div>
					<div class="answer">For you, NOTHING! This includes physician’s bills, prescriptions, hospital bills, surgeries, laboratory tests, other tests including X-rays, physical therapy and assistive devices such as crutches and even travel expenses are zero cost to you or your family. You should NEVER pay first and then seek reimbursement. The cost is not for you in any way.</div>
					</li>

					<li>
					<div class="question">I don't think I'll be able to go back to work right away.. what happens now?</div>
					<div class="answer">If you experience time away from work because of your injury, then you could become eligible for monetary compensation after a predetermined number of consecutive days away from work (set by each state). Monetary compensation is generally one-half to two-thirds of normal compensation (set by each state), but this amount is tax free, which means that you will likely end up with a level of income close to your pre-injury level. And as stated before, all of your medical expenses will be covered.</div>
					</li>

					<li>
					<div class="question">Do I need an attorney? I've heard that involving them may get me better service (or more money).</div>
					<div class="answer">You do not.  The whole point of Zenjuries (and Zenployers) is to provide complete transparency and superior support to every injured employee. With our involvement, we strive to provide satisfaction to businesses and employees alike.  As far as attorneys go, states set workers’ compensation payouts, so there’s no leeway to get more (or less) than what is deserved. In Washington, for example, a thumb is worth 75 weeks of pay. If your thumb is 10 percent disabled in a workplace accident, you get 7.5 weeks of disability. The value system is built into the law, and the doctor decides how permanent your injury is. Most states have a workers’ compensation commission that can help injured people get what they deserve from the system...without giving 20% or much more to an attorney.</div>
					</li>

				</ul>
			</div>
		</div>
	</div>
</div>

@endsection

@extends('layouts.zen5_zengarden_zp_layout')
@section('maincontent')

    <?php
    //pull injury info here
    if(session('injury_id')){
        $injury = \App\Injury::where('id', session('injury_id'))->first();
    }else{
        $injury = \App\Injury::where('injured_employee_id', Auth::user()->id)->first();
    }

    $most_recent_injury_post = $injury->getLatestInjuryPost();
    $most_recent_claim_post = $injury->getLatestClaimPost();

    if(!is_null($most_recent_claim_post) && $most_recent_claim_post->created_at >= \Carbon\Carbon::now()->subDay()){
        $recent_claim_post = true;
        $most_recent_claim_mood = $most_recent_claim_post->mood;
        $claim_post_timestamp = $most_recent_claim_post->created_at->toFormattedDateString();
        $last_claim_note_text = $most_recent_claim_post->note;
        
    }else{
        $recent_claim_post = false;
        $most_recent_claim_mood = 0;
        $claim_post_timestamp = "";
        $last_claim_note_text = "";
    }

    if(!is_null($most_recent_injury_post) && $most_recent_injury_post->created_at >= \Carbon\Carbon::now()->subDay()){
        $recent_injury_post = true;
        $most_recent_injury_mood = $most_recent_injury_post->mood;
        $injury_post_timestamp = $most_recent_injury_post->created_at->toFormattedDateString();
        $last_injury_note_text = $most_recent_injury_post->note;
        if($last_injury_note_text === ""){
	        $last_injury_note_text = "";
        }
    }else{
        $recent_injury_post = false;
        $most_recent_injury_mood = 0;
        $injury_post_timestamp = "";
        $last_injury_note_text = "";
    }

    ?>

	<input type="hidden" id="injuryID" value="{{$injury->id}}">
	<input type="hidden" id="userID" value="{{Auth::user()->id}}">
	<input type="hidden" id="csrfToken" value="{{csrf_token()}}">
	<!-- required page script -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

	<div class="pageContent bgimage-bgheader bghaze-moss fade-in">
		<!--**********-->
		<div class="headerBlock">
			<div class="headerImage">
				<div class="headerImage__panel-small">
					<img class="headerImage__panelimage-small" src="/images/clipart/diary-o.png">
				</div>
			</div>
			<div class="container">
				<div class="pageImageHeader">
					<div class="pageImageHeader__title txt__white">
						<span>your zen diary</span>
					</div>
					<div class="pageImageHeader__subTitle txt__white" style="border-color: var(--zencare);">
						<span>how are you feeling today?</span>
					</div>
				</div>
			</div>
		</div>

		<div class="contentBlock bg__transparent txt__white">
			<div class="container">
				<div class="animate__animated animate__pulse animate__delay-2s">
					<div id="noUpdateTitle" class="txt__white txt__smTitle-thin zCodeDiaryTitle">Your zenDiary is your way to let us know exactly how you feel, at any given moment.  Update often!</div>
					<div id="noClaimExperienceTitle" class="txt__white txt__smTitle-thin zCodeDiaryTitle">You told us about your <b>injury mood</b> today, how about your <b>claim experience</b>?</div>
					<div id="noInjuryMoodTitle" class="txt__white txt__smTitle-thin zCodeDiaryTitle">You told us about your <b>claim experience</b>, how about your <b>injury mood</b>?</div>
					<div id="fullUpdateTitle" class="txt__white txt__smTitle-thin zCodeDiaryTitle"><b>Great job!</b> You sent us your moods for today, would you like to update them?</div>
				
				</div>
				<!-- start -->
				<div class="gridControls">
					<div class="row">
						<!--- INJURY MOOD --->
						<div class="cell w50 padded">
							<div id="diaryInjury" class="zg-actionPanel {{($recent_injury_post === false ? "openInjuryModal" : "")}}" style="min-height:360px;">
								<div class="panelTitle smaller"><div class="icon inline icon-injury"></div> injury mood</div>
								<hr class="subtle">
								<div class="zd-message">
									<div class="zd-messagearea">
										<div class="zd-instructions" style="{{ ($recent_injury_post === true ? "display: none" : "display: block")}}">
											how are you feeling about your work related injury right now?
										</div>
										<div class="zd-thankyou" style="{{ ($recent_injury_post === true ? "display: block" : "display: none")}}">
											<span class="thanks">thanks!</span>you sent your injury mood<br>on {{$injury_post_timestamp}}
										</div>
									</div>
								</div>
								<div id="currentInjuryMood" class="myMood submitted mood-{{$most_recent_injury_mood}}">
									<div class="moodVisual">
										<div class="currentMood"></div>
										<div class="moodLabel fixedHeight"></div>
									</div>
								</div>
								<div id="diaryMoodButtons" class="buttonSet" style="{{($recent_injury_post === true ? "display: block" : "display: none")}}">
									<button id="updateInjuryMood" class="green openInjuryModal" onclick="PlaySlideClick()">update!</button>
									<button id="showInjuryNoteModal" class="gold showNoteModal" style="{{($last_injury_note_text === "" ? "display:none" : "")}}" onclick="PlaySlideClick()">see note</button>
									<button id="noNoteInjuryButton" class="ghost noNoteButton" style="{{($last_injury_note_text === "" ? "" : "display:none")}}">no note</button>
								</div>								
							</div>
						</div>
						<!-- CLAIM MOOD -->
						<div class="cell w50 padded">
							<div id="diaryClaim" class="zg-actionPanel {{($recent_claim_post === false ? "openClaimModal" : "")}}" style="min-height:360px;">
								<div class="panelTitle smaller"><div class="icon inline icon-insurance"></div> claim mood</div>
								<hr class="subtle">
								<div class="zd-message">
									<div class="zd-messagearea">
										<div class="zd-instructions" style="{{ ($recent_claim_post === true ? "display: none" : "display: block")}}">
											how are you feeling about your work comp claim experience?
										</div>
										<div class="zd-thankyou" style="{{ ($recent_claim_post === true ? "display: block" : "display: none")}}">
											<span class="thanks">thanks!</span>you sent your claim mood<br>on {{$claim_post_timestamp}}.
										</div>
									</div>			
								</div>				
								<div id="currentClaimMood" class="myMood submitted mood-{{$most_recent_claim_mood}}">
									<div class="moodVisual">
										<div class="currentMood"></div>
										<div class="moodLabel fixedHeight"></div>
									</div>
								</div>
								<div id="diaryClaimButtons" class="buttonSet" style="{{($recent_claim_post === true ? "display: block" : "display: none")}}">
									<button id="updateClaimMood" class="green openClaimModal" onclick="PlaySlideClick()">update!</button>
									<button id="showClaimNoteModal" class="gold showNoteModal" style="{{($last_claim_note_text === "" ? "display:none" : "")}}" onclick="PlaySlideClick()">see note</button>
									<button id="noNoteClaimButton" class="ghost noNoteButton" style="{{($last_claim_note_text === "" ? "" : "display:none")}}">no note</button>
								</div>								
							</div>
						</div>
					</div>
				</div>
				<!-- end -->
				<hr>
				<div class="txt__grey txt__smParagraph">Your diary will prompt you every day for an update, but you can submit your feelings and notes as often as you like.  They will all get recorded with your injury report, and the more we know about how you are doing, the better!  Want to converse with your team even more?  Go to the <a href="/zengarden/team">TEAMS section</a> and leave a message!</div>
			</div>
		</div>

	</div>


	<!-- I set up this button to trigger the jbox, you can use anything just change the "attach" option in the jbox script to match the target button id/class -->
	
	<!-- ****** INJURY MOOD MODAL ***** -->

	<div id="injuryMoodModal" class="zModal" style="display:none;">
		<a class="close-zModal"><div class="icon icon-times"></div></a>
		<div class="modalContent paddedDark fixedWidth">
			<div class="modalHeader"><span class="modalTitle">Update Injury Mood</span></div>
			<div class="modalBody">
				<!-- MOOD TAB -->
				<div class="moodBody" id="injuryMoodModalTab1">
					<div class="myMood unsubmitted mood-5">
						<div class="moodQuestion">How are you feeling about your <b><u>injury</u></b> right now?
							<span class="instructions">Use the slider below to tell us from 1 (worst) to 10 (best)</span>
						</div>
						<div class="moodVisual">
							<div class="currentMood"></div>
							<div class="moodLabel"></div>
						</div>
						<div class="moodSlider">
							<input type="range" min="1" max="10" value="5" id="injuryMoodRange">
						</div>
						<div class="moodSubmit">
							<button class="injuryMoodButton" id="injuryMoodModalSubmitFirstTab" onclick="PlaySlideClick()">next</button>
						</div>
						<!--
						<div class="moodQuestion">
							<span class="instructions">You can just submit your mood, or you can add note to your mood so we have more information about how you're feeling.</span>
						</div>
						<div class="moodSubmit">
							<button class="injuryMoodButton" id="submitInjuryMood" onclick="PlayHappyClick()">submit only my mood</button>
							<button class="injuryMoodButton" id="submitInjuryMoodNote" onclick="PlayHappyClick()">submit & add a note</button>
						</div>
						-->
					</div>
				</div>	
				<!-- ASK FOR NOTE TAB -->
				<div class="askNoteBody" id="injuryMoodModalTab2" style="display:none">
					<div class="moodQuestion">
							<span class="moodQuestion"><div class="panelTopImage"><img src="/images/clipart/zenthumbsup.png"></div>Thanks for your Injury Mood feedback!<br>We are just about to send your mood..<hr>Would you like to add a short note?<br>Adding a note will give your team even more information about how you're feeling.</span>
					</div>
					<div class="moodSubmit">
						<button class="injuryMoodButton" id="submitInjuryMood" onclick="PlayHappyClick()">no, send now</button>
						<button class="injuryMoodButton" id="addInjuryMoodNote" onclick="PlaySlideClick()">yes, add note</button>
					</div>
				</div>
				<!-- GET NOTE TAB -->
				<div class="noteBody" id="injuryMoodModalTab3" style="display:none">
					<div class="moodQuestion" style="display: none">
						<span class="instructions">You can use a note to let your injury team know exactly how you're feeling about your injury.</span>
					</div>
					<textarea id="injuryMoodNoteTextarea" class="notePad" placeholder="type your note here"></textarea>
					<input type="hidden" id="modalNoteType">
					<input type="hidden" id="modalNoteMood">
					<div class="noteitem nDate">{{\Carbon\Carbon::today()->toDateString()}}</div>
					<div class="noteitem nSent">status: </div>
					<div class="noteitem nTo">to: {{$injury->getSquadName()}}</div>
					<div class="noteitem nClip"><img src=""></div>
					<div class="moodSubmit">
						<button class="injuryMoodButton" id="submitInjuryMoodNote" onclick="PlayHappyClick()">send with note</button>
						<button class="injuryMoodButton noteTabBackButton" id="submitInjuryMoodNoteCancel" onclick="PlayHappyClick()">cancel/back</button>						
					</div>	
				</div>
			</div>
		</div>
	</div>
	
	<!-- ****** CLAIM MOOD MODAL ***** -->
	
	<div id="claimMoodModal" class="zModal" style="display:none;">
    	<a class="close-zModal"><div class="icon icon-times"></div></a>
	    <div class="modalContent paddedDark fixedWidth">
	        <div class="modalHeader"><span class="modalTitle">Update Claim Mood</span></div>
	            <!-- MOOD TAB -->
	            <div class="moodBody" id="claimMoodModalTab1">
	                <div class="myMood unsubmitted mood-5">
	                    <div class="moodQuestion">How are you feeling about your <b><u>claim experience</u></b> right now?
	                        <span class="instructions">Use the slider below to tell us from 1 (worst) to 10 (best)</span>
	                    </div>
	                    <div class="moodVisual">
	                        <div class="currentMood"></div>
	                        <div class="moodLabel"></div>
	                    </div>
	                    <div class="moodSlider">
	                        <input type="range" min="1" max="10" value="5" id="claimMoodRange">
	                    </div>
	                    <div class="moodSubmit">
	                        <button class="claimMoodButton" id="claimMoodModalSubmitFirstTab" onclick="PlaySlideClick()">next</button>
	                    </div>
	
	                </div>
	            </div>
	            <!-- ASK FOR NOTE TAB -->
	            <div class="askNoteBody" id="claimMoodModalTab2" style="display:none">
	                <div class="moodQuestion">
	                    <span class="moodQuestion"><div class="panelTopImage"><img src="/images/clipart/zenthumbsup.png"></div>Thanks for your Claim Mood feedback!<br>We are just about to send your mood..<hr>Would you like to add a short note?<br>Adding a note will give your team even more information about how you're feeling.</span>
	                </div>
	                <div class="moodSubmit">
	                    <button class="claimMoodButton" id="submitClaimMood" onclick="PlayHappyClick()">no, send now</button>
	                    <button class="claimMoodButton" id="addClaimMoodNote" onclick="PlaySlideClick()">yes, add note</button>
	                </div>
	            </div>
	            <!-- GET NOTE TAB -->
	            <div class="noteBody" id="claimMoodModalTab3" style="display:none">
	                <div class="moodQuestion" style="display: none">
	                    <span class="instructions">You can use a note to let your injury team know exactly how you're feeling about your claim.</span>
	                </div>
	                <textarea id="claimMoodNoteTextarea" class="notePad" placeholder="type your note here"></textarea>
	                <input type="hidden" id="modalNoteType">
	                <input type="hidden" id="modalNoteMood">
	                <div class="noteitem nDate">{{\Carbon\Carbon::today()->toDateString()}}</div>
	                <div class="noteitem nSent">status: </div>
	                <div class="noteitem nTo">to: {{$injury->getSquadName()}}</div>
	                <div class="noteitem nClip"><img src=""></div>
	                <div class="moodSubmit">
	                    <button class="claimMoodButton" id="submitClaimMoodNote" onclick="PlayHappyClick()">send with note</button>
	                    <button class="injuryMoodButton noteTabBackButton" id="submitInjuryMoodNoteCancel" onclick="PlayHappyClick()">cancel/back</button>
	                </div>
	            </div>
    	</div>
	</div>

	<div id="injuryNoteModal" class="zModal" style="display:none">
		<a class="close-zModal"><div class="icon icon-times"></div></a>
		<div class="modalContent">
			<div class="modalHeader"><span class="modalTitle">Injury Note</span></div>
			<div class="modalBody" style="min-width:260px;">
				<div class="viewNoteModalNote">{{$last_injury_note_text}}</div>
			</div>
		</div>
	</div>
	
	<div id="claimNoteModal" class="zModal" style="display:none">
		<a class="close-zModal"><div class="icon icon-times"></div></a>
		<div class="modalContent">
			<div class="modalHeader"><span class="modalTitle">Claim Note</span></div>
			<div class="modalBody" style="min-width:260px;">
				<div class="viewNoteModalNote">{{$last_claim_note_text}}</div>
			</div>
		</div>
	</div>

	<script>
        //*** HANDLERS FOR HOVER/FOCUS events ***//
        //the two seperate sides, injury vs claim, have their own handlers due to separate moods

        //hover for claim mood
        $('.claimMoodButton').on('mouseenter', function(){
            var mood = $('#claimMoodRange').val();
            $(this).addClass('mood-' + mood);
        }).on('mouseleave', function(){
            var mood = $('#claimMoodRange').val();
            $(this).removeClass('mood-' + mood);
        });

        //hover for injury mood
        $('.injuryMoodButton').on('mouseenter', function(){
            var mood = $('#injuryMoodRange').val();
            $(this).addClass('mood-' + mood);
        }).on('mouseleave', function(){
            var mood = $('#injuryMoodRange').val();
            $(this).removeClass('mood-' + mood);
        });

        //FOCUS events (I don't think jQuery has an on active event, but I think focusin/focusout does the same thing)

        //adds classes on focus in, removes them on focus out
        $('.claimMoodButton').on('focusin', function(){
            var mood = $('#claimMoodRange').val();
            $(this).addClass('mood-' + mood);
        }).on('focusout', function(){
            var mood = $('#claimMoodRange').val();
            $(this).removeClass('mood-' + mood);
        });

        $('.injuryMoodButton').on('focusin', function(){
            var mood = $('#injuryMoodRange').val();
            $(this).addClass('mood-' + mood);
        }).on('focusout', function(){
            var mood = $('#injuryMoodRange').val();
            $(this).removeClass('mood-' + mood);
        });
        
        //swaps between different welcome messages
	function showTitle(){
	    var recent_injury_post = 1;
	    var recent_claim_post = 1;
	
	    $('.zCodeDiaryTitle').hide();
	
	    //check if the "no update" state is shown on the mood panels
	    if($('#currentInjuryMood').hasClass('mood-0')){
	        recent_injury_post = 0;
	    }
	
	    if($('#currentClaimMood').hasClass('mood-0')){
	        recent_claim_post = 0;
	    }
	
	    //show the correct title
	    if(recent_injury_post === 0 && recent_claim_post === 0){
	        $('#noUpdateTitle').show();
	    }else if(recent_injury_post === 1 && recent_claim_post === 0){
	        $('#noClaimExperienceTitle').show();
	    }else if(recent_injury_post === 0 && recent_claim_post === 1){
	        $('#noInjuryMoodTitle').show();
	    }else {
	        $('#fullUpdateTitle').show();
	    }
	}
	
	showTitle();

	</script>
@endsection

@extends('layouts.zen5_zengarden_zp_layout_no_nav')
@section('maincontent')
    <?php
    //pull injury info here
    if(session('injury_id')){
        $injury = \App\Injury::where('id', session('injury_id'))->first();
    }else{
        $injury = \App\Injury::where('injured_employee_id', Auth::user()->id)->first();
    }
    ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="https://www.zenployees.com/js/parallax.js"></script>

    <div class="pageContent fade-in">
        <!--**********-->
        <div class="headerBlock parallax" id="parallax-1" data-image-src="https://www.zenployees.com/images/backgrounds/stonesbg.jpg" style="background-color:#3597d0;">
            <div class="headerImage wow animate__animated animate__bounceInRight">
                <!--<div class="headerImage__panel-small">
                   <img class="headerImage__panelimage-small" src="https://www.zenployees.com/images/clipart/help-o.png">
                </div>-->
            </div>
            <div class="container">
                <div class="pageImageHeader wow animate__animated animate__bounceInLeft">
                    <div class="pageImageHeader__title txt__white">
                        <span>Claim Resolved!</span>
                    </div>
                    <div class="pageImageHeader__subTitle txt__white" style="border-color:#bd9979;">
                        <span>Your injury has been marked as resolved!

                        We hope you're fully healed and recovered.
        If your claim is re-opened for some reason, you will still be able to make posts in your diary and interact with the team assigned to that injury.
        We hope Zenjuries helped make this difficult time as stress-free as possible for you!</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h1></h1>

    <h3>

    </h3>


    @endsection


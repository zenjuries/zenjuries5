@extends('layouts.zen5_zengarden_zp_layout')
@section('maincontent')

<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="https://www.zenployees.com/js/parallax.js"></script>

<?php

if(Auth::user()->gets_priority_3_emails === 1){
    $alert_settings = "full";
}else{
    $alert_settings = "none";
}

$theme = Auth::user()->zengarden_theme;
$theme = str_replace("theme", "", $theme);
$theme = strtolower($theme);

?>

<div class="pageContent bgimage-bgheader bghaze-steel fade-in">
		<!--**********-->
	<div class="headerBlock">
		<div class="headerImage">
			<div class="headerImage__panel-small">
				<img class="headerImage__panelimage-small" src="/images/clipart/tools-o.png">
			</div>
		</div>
		<div class="container">
			<div class="pageImageHeader">
				<div class="pageImageHeader__title txt__white">
					<span>my settings</span>
				</div>
				<div class="pageImageHeader__subTitle txt__white" style="border-color: var(--zencare);">
					<span>change your settings and password</span>
				</div>
			</div>
		</div>
	</div>
	<div class="contentBlock bg__transparent txt__white">
		<div class="container">
				
<!-- start -->

						<div class="zg-actionPanel" id="appointmentPast">
							<div class="panelTitle"><div class="icon inline icon-setuser"></div> my settings</div>
							<hr class="subtle">
							<div style="height:12px;"></div>
							<div class="settingsButtonArray">
								<div class="buttonSet">
									<span>update your password here</span>
									<button id="passwordButton">set password</button>
								</div>
								<div class="buttonSet">
									<span>how do you want to be alerted?</span>
									<button id="alertsButton">set alerts</button>
								</div>
								<div class="buttonSet {{Auth::user()->zengarden_theme}} themeBG">
									<span class="themeColor-bgg themeLabelbg">current theme - <b>{{$theme}}</b></span>
									<a href="/zengarden/theme" class="invisible"><button id="switchThemeButton">change theme</button></a><br>
								</div>
							</div>
						</div>	

<!-- end -->
<hr>
<div class="txt__grey txt__smParagraph">You can set your avatar style, your name and email, basic information on the "my info" panel.  On the "my team" panel, you can view the team members on your injury, interact with them, and see who your adjuster is.  In addition, you can view your complete injury information and see what your team has been communicating about your injury.</div>			
		</div>
	</div>

</div>

<div id="passwordModal" class="zModal" style="display:none;">
	<a class="close-zModal"><div class="icon icon-times"></div></a>
	<div class="modalContent">
		<div class="modalHeader"><span class="modalTitle">change password</span></div>
		<div class="modalBody">
			<div class="formBlock dark opaque">
				<div class="errors" id="passwordModalErrors" style="display: none; color: red"></div>
				<div class="formGrid">
					<div class="formGrid__row">
						<label>new pw:</label><input id="passwordInput" type="password" placeholder="type password">
					</div>
					<div class="formGrid__row">
						<label>confirm:</label><input id="confirmPasswordInput" type="password" placeholder="retype password">
					</div>
				</div>
			</div>
		</div>
		<div class="modalFooter">
			<button class="cyan" id="passwordModalSave">update password</button>
		</div>
	</div>
</div>


<div id="alertsModal" class="zModal" style="display: none">
	<a class="close-zModal"><div class="icon icon-times"></div></a>
	<div class="modalContent paddedDark">
		<div class="modalHeader"><span class="modalTitle">set alerts</span></div>
		<div class="modalBody">
				Zenjuries can send you email alerts to keep you updated on your claim.<br> Please select what level of alerts you'd like to receive!<br><br>
			<div class="settingsButtonArray">
				<div class="buttonSet">
					<button id="allAlertsButton" class="alertsButton default {{$alert_settings === "full" ? "green" : ""}}" data-alerts="full">full alerts</button>
					<button id="criticalAlertsButton" class="alertsButton default  {{$alert_settings === "none" ? "cyan" : ""}}" data-alerts="none">critical alerts only</button>
				</div>
			</div>
		</div>
		<div class="modalFooter">
		</div>
	</div>
</div>
@endsection

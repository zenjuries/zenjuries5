@extends('layouts.zen5_zenployees')
@section('maincontent')


<style>
.loginPageContainer{width:100%;height:100%;position:relative;}	
#login-panel {
    width:100%;
    max-width:400px;
    height:500px;
    overflow:hidden;
    border-radius:8px;
    box-shadow:0px 0px 14px 3px black;
    padding:40px;
    background-color: rgb(54,54,54);
  margin: 0px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}
@media (max-width: 576px) {
	.loginPageContainer{background-color:rgb(54,54,54);}
    #login-panel{background-color:transparent;box-shadow:none;}
}


#alogo{
width:200px;height:200px;
background-size:auto;
background-repeat:no-repeat;
background-position:center center;
}

.loginBlock{display:table;}
.loginRow{display:table-row;}
.loginCell{display:table-cell;position:relative;padding-bottom:12px;}
.loginCell .icon{font-size:26px;}
.loginCell input{background-color:#c7c7c7;border:none !important;outline:none;}
.loginCell:first-child{width:40px;height:40px;vertical-align:middle;text-align:center;}
.loginCell:last-child{width:100%;padding-left:12px;}
.loginButton a{display:block;width:90%;padding:5px;background-color:#448203;border-radius:20px;text-align:center;color:white !important;font-size:1.2rem;cursor:pointer;margin:0 auto;} 
.loginButton a:hover{background-color:#5ca80c}   

.messageOverlay{position:absolute;top:0px;left:0px;width:100%;}
.alert-text ul{margin:0;padding:0;list-style:none;width:100%;text-align:center;}
.alert-text li{display:block;color:red;}
</style>
 
           
<div class="loginPageContainer">	   


		
	<div id="login-panel">
		<div style="width:200px;height:200px;margin:0 auto;margin-bottom:20px;">
			<div id="alogo">
			<script>document.getElementById('alogo').style.backgroundImage = "url('images/gif/zengarden-anim.gif?v=" + new Date().valueOf() + "')"</script>						
			</div>
		</div>
		
<form id="zenployeesLoginForm" role="form" method="POST" action="/login" id="">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="loginBlock">
		<div class="loginRow">
        	<div class="loginCell"><div class="icon icon-envelope-o inline"></div></div> <div  class="loginCell"><input type="email" class="form-control" name="email" placeholder="email address"></div>
		</div>
	
		<div class="loginRow">
        	<div class="loginCell"><div class="icon icon-lock-1 inline"></div></div> <div class="loginCell"><input type="password" class="form-control" name="password" placeholder="password"></div>
		</div>

		<div class="loginRow">
        	<div class="loginCell"></div> <div class="loginCell" style="text-align:right;padding-right:20px;"><input type="checkbox" name="remember"> Remember Me</div>
		</div>						
    </div>
    

    <!-- Login Button -->
    <div class="loginButton" id="submitLoginForm"><a id="submitLogin">Login</a></div>


</form>


<center><a href="/password/reset">Forgot Your Password?</a></center>

	</div>
	
 	<div class="messageOverlay">
     <!-- login errors -->
    <!-- you can change this html however you want, it only shows if there are errors when you submit the form. -->
    @if (count($errors) > 0)
	
        <div class="alert-text">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        
    @endif
 	</div> 	
	
</div>



    <script src="js/zen-js/zen_login.js"></script>

@endsection
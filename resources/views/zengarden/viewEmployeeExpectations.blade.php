@extends('layouts.zen5_zengarden_zp_layout')
@section('maincontent')
<div class="pageContent bgimage-bgheader fade-in">
<div class="headerBlock">
<img src="/images/documents/expectations-title.jpg" style="width:100%;position:relative;top:-18px;">	
</div>

<div class="container">
<br><br><br><br><br>	
<div class="txt__white txt__smTitle-thin">
Your employer has been funding a program designed to protect, nurture and care for you in situations just like this, workers compensation. Furthermore, your employer has also chosen to enroll in the state-of-the-art injury management program, Zenjuries.
</div>

<div class="txt__white txt__smTitle-thin">
Right now, a team of professionals is working hard on your well-being and will be communicating with you and each other until your injury is completely resolved…no matter how long it takes.
</div>

<hr class="subtle">
<br><br>

<div class="txt__white txt__smTitle">
DO NOT BE AFRAID
</div>
<div class="txt__grey txt__medParagraph">
You are inside of a program that is bound by federal statutes governed by each states laws. These laws protect and care for you and your family during your injury. These commitments have begun, and are already underway hard at work for you. Your employer truly cares and is already helping you.
</div><br><br>

<div class="txt__white txt__smTitle">
Constant Teamwork
</div>
<div class="txt__grey txt__medParagraph">
Promptly report and communicate every update of your condition and your feelings to your team…all the way to the end. They care deeply and need you to keep them “in the loop”. Your experiences, needs, and desires must be clearly communicated every chance you get to share with them.
</div><br><br>

<div class="txt__white txt__smTitle">
Proper Medical Care
</div>
<div class="txt__grey txt__medParagraph">
If your injury is an emergency, you’ll go where the ambulance takes you. In a nonemergency, your employer may direct you to a particular hospital, clinic or doctor. In most states, you must go where your employer tells you or there is a risk that your bills may not be covered by workers’ compensation. In all cases, try to work as “one team” with your employer’s program.
</div><br><br>

<div class="txt__white txt__smTitle">
Medical Care Costs
</div>
<div class="txt__grey txt__medParagraph">
For you, NOTHING! This includes physician’s bills, prescriptions, hospital bills, surgeries, laboratory tests, other tests including X-rays, physical therapy and assistive devices such as crutches and even travel expenses are zero cost to you or your family. You should NEVER pay ﬁrst and then seek reimbursement. The cost is not for you in any way.
</div><br><br>

<div class="txt__white txt__smTitle">
Lost Wages
</div>
<div class="txt__grey txt__medParagraph">
If you experience time away from work because of your injury, then you could become eligible for monetary compensation after a predetermined number of consec utive days away from work (set by each state). Monetary compensation is generally one-half to two-thirds of normal compensation (set by each state), but this amount is tax free, which means that you will likely end up with a level of income close to your pre-injury level. And as stated before, all of your medical expenses will be covered.
</div><br><br>

<div class="txt__white txt__smTitle">
Temporary Total Disability
</div>
<div class="txt__grey txt__medParagraph">
Results from an injury that keeps you from working temporarily. These benefits are calculated at 66 2/3% of the injured employee's average gross weekly wage. There are maximums, which are set by the state, and the employer/insurer is not required to pay more than this maximum amount. These benefits, subject to a waiting period, are paid until you are back to work.
</div><br><br>

<div class="txt__white txt__smTitle">
Temporary Partial Disability
</div>
<div class="txt__grey txt__medParagraph">
Occurs only if you are able to work on light duty at less than full pay. These payments are calculated at 66 2/3% of the diﬀerence in what you earned before the injury and what you are making on light duty. These beneﬁts are paid for a maximum number of predetermined weeks (set by each state).
</div><br><br>

<div class="txt__white txt__smTitle">
Permanent Partial Disability
</div>
<div class="txt__grey txt__medParagraph">
Refers to a permanent disability that does not prevent you from returning to some type of employment. In the event that you are determined to have a permanent partial disability, you may receive a lump sum settlement based on your percentage of disability.
</div><br><br>

<div class="txt__white txt__smTitle">
Permanent Total Disability
</div>
<div class="txt__grey txt__medParagraph">
Applies only if you will never be able to work a regular full time job again. If you are determined to be permanently and totally disabled by the Division of Workers' Compensation, you will be entitled to 2/3 of your salary for life; however, you may choose to settle the case for a lump sum of money in exchange for lifetime payments.
</div><br><br>

<div class="txt__white txt__smTitle">
Return To Work
</div>
<div class="txt__grey txt__medParagraph">
“Light Duty” or “Modiﬁed Duty” is a very important ingredient to your overall physical and mental healing process and wellbeing. In fact, a study by Crawford & Company showed that injured people recover from their injuries 3x’s faster when they are on the job. That means that you are back, part of the team, helping your company, and earning your full wages in a fraction of the time just by returning to work as soon as you feel safely able.
</div><br><br>

<hr class="subtle">
<br><br>

<div class="txt__grey txt__smDesc">
Keep this list as a point of reference that you can return to over and over throughout yourhealing process.
</div><br>

<div class="txt__grey txt__smDesc">
From your TEAM and from your friends at Zenjuries, we wish you a very pleasant experience and a speedy healing process!!
</div><br><br>

<hr class="subtle">
<br><br>

<div class="txt__white txt__smTitle">
*BONUS*
</div>

<div class="txt__white txt__smTitle">
Attorney Involvement
</div>
<div class="txt__grey txt__medParagraph">
States set workers’ compensation payouts, so there’s no leeway to get more (or less) than what is deserved. In Washington, for example, a thumb is worth 75 weeks of pay. If your thumb is 10 percent disabled in a workplace accident, you get 7.5 weeks of disability. The value system is built into the law, and the doctor decides how permanent your injury is.

Most states have a workers’ compensation commission that can help injured people get what they deserve from the system…without giving 20% or much more to an attorney.
</div><br><br>
<a href="/zengarden/myinfo" class="invisible"><button id="switchThemeButton">back</button></a>
</div>	
@endsection

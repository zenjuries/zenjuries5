@extends('layouts.zen5_zengarden_zp_layout')
@section('maincontent')

	<?php
    //pull injury info here
    if(session('injury_id')){
        $injury = \App\Injury::where('id', session('injury_id'))->first();
    }else{
        $injury = \App\Injury::where('injured_employee_id', Auth::user()->id)->first();
    }

    $squad = \App\Squad::where('id', $injury->squad_id)->first();
    $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();

    //we'll calculate what stage the claim is at to determine what section of the "What's Next" modal to show
	$whats_next_step = 1;

	$diary_posts = \App\DiaryPost::where('injury_id', $injury->id)->get();
	$appointments = \App\Appointment::where('injury_id', $injury->id)->get();
	//if the employee is back on light duty, they're on step 3, otherwise they're on step 4.
	if($injury->employee_status === "Light Duty"){
		$whats_next_step = 4;
	}else if(is_null($diary_posts)){
		$whats_next_step = 1;
	}else if(is_null($appointments)){
		$whats_next_step = 2;
	}else{
		$whats_next_step = 3;
	}

	$diary_post = \App\DiaryPost::where('injury_id', $injury->id)->latest()->first();
    if(is_null($diary_post)){
        $mood = "None Set";
    }else{
        $mood = $diary_post->mood;
        if($mood == 1){
	        $mood .= " - Very Bad";
        }else if($mood == 2){
	        $mood .= " - Somewhat Bad";
        }else if($mood == 3){
	        $mood .= " - Not Well";
        }else if($mood == 4){
	        $mood .= " - Alright";
        }else if($mood == 5){
	        $mood .= " - Fine";
        }else if($mood == 6){
	        $mood .= " - Doing Better";
        }else if($mood == 7){
	        $mood .= " - Fairly Good";
        }else if($mood == 8){
	        $mood .= " - Very Good";
        }else if($mood == 9){
	        $mood .= " - Feeling Great";
        }else if($mood == 10){
	        $mood .= " - Awesome";
        }
    }
			?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="https://www.zenployees.com/js/parallax.js"></script>

	<style>
		.whatsNextSection{
			color: white;
		}
	</style>

<div class="pageContent bgimage-bgheader bghaze-plum fade-in">
		<!--**********-->
	<div class="headerBlock">
		<div class="headerImage wow">
			<div class="headerImage__panel-small">
				<img class="headerImage__panelimage-small" src="/images/clipart/medical-o.png">
			</div>
		</div>
		<div class="container">
			<div class="pageImageHeader wow">
				<div class="pageImageHeader__title txt__white">
					<span>injury & team</span>
				</div>
				<div class="pageImageHeader__subTitle txt__white" style="border-color: var(--zencare);">
					<span>about your injury and zenjuries team</span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="contentBlock bg__transparent txt__white">
		<div class="container">
			<div class="txt__white txt__smTitle-thin">All about your injury, where to get info, and the team that is supporting you. </div> 
				
<!-- start -->
			<div class="boxGrid">
				
				<div class="boxGrid__box box-size-50">
					<div class="boxContent">
						<div class="zg-actionPanel" style="min-height:590px;">
							<div class="panelTitle"><div class="icon inline icon-injuryreport"></div> my injury info</div>
							<hr class="subtle">
							<div style="height:12px;"></div>
							<ul class="zTable padded">
								<li>
									<span class="zCell label">Claim Number</span>
									<span class="zCell data">{{($injury->claim_number !== NULL ? $injury->claim_number : "Not Yet Entered")}}</span></li>								
								<li>
									<span class="zCell label">Injury Location</span>
									<span class="zCell data">{{$injury->printInjuryLocations()}}</span></li>
								<li>
									<span class="zCell label">Injury Type</span>
									<span class="zCell data">{{$injury->printInjuryTypes()}}</span></li>
								<li>
									<span class="zCell label">Injury Severity</span>
									<span class="zCell data">{{$injury->severity}}</span></li>
								<li>
									<span class="zCell spacer"></span>
									<span class="zCell spacer"></span>
								</li>	
								<li>
									<span class="zCell label">Status</span>
									<span class="zCell data">{{$injury->employee_status === NULL ? "None Set" : $injury->employee_status}}</span>
								</li>
								<li>
									<span class="zCell label">Current Mood</span>
									<span class="zCell data">{{$mood}}</span>
								</li>									
							</ul>
							
								<ul class="zTable padded">
									<li class="listTitle">my Insurance Adjuster</li>
									<li>
										<span class="zCell icon"></span>
										<span class="zCell label">Name</span>
										<span class="zCell data">not entered {{$injury->adjuster_name}}</span>
									</li>
									<li>
										<span class="zCell icon"></span>
										<span class="zCell label">Phone</span>
										<span class="zCell data">not entered {{$injury->adjuster_phone}}</span>
									</li>
									<li>
										<span class="zCell icon"></span>
										<span class="zCell label">Email</span>
										<span class="zCell data">not entered {{$injury->adjuster_email}}</span>
									</li>																	
								</ul>
															
							<div class="settingsButtonArray">
								<div class="buttonSet">
									<span>what you should do next</span>
									<button id="whatsNextButton" class="green">whats next?</button>
								</div>
								<!-- I'M HIDING THIS UNTIL WE CAN GET IT SET UP ON THE TREE PAGE - I THINK WE'LL USE IT FOR UPLOADS FROM THE TEAM
								<div class="buttonSet">
									<span>available information</span>
									<button class="red">injury info</button>
								</div>
							-->
							</div>
						</div>	
					</div>
				</div>
			
				<div class="boxGrid__box box-size-50">
					<div class="boxContent">
						<div class="zg-actionPanel" style="min-height:590px;">
							<div class="panelTitle"><div class="icon inline icon-teams"></div> my team</div>
							<hr class="subtle">
							<div style="height:12px;"></div>
								<ul class="zTable padded selectList">
									<li class="listTitle">{{$squad->squad_name}}</li>
									<li>
										<a class="invisible">
										<span class="zCell icon"><div class="icon icon-exec"></div></span>
										<span class="zCell label">{{$squad->getChiefExecutiveName()}}</span>
										<span class="zCell data">chief executive</span>
										</a>
									</li>
									<li>
										<a class="invisible">
										<span class="zCell icon"><div class="icon icon-agent"></div></span>
										<span class="zCell label">{{$squad->getChiefNavigatorName()}}</span>
										<span class="zCell data">chief navigator</span>
										</a>
									</li>								
								</ul>
								
								<div style="height:22px;"></div>
						
								<div class="settingsButtonArray">
									<div class="buttonSet">
										<span>view everyone on your team</span>
										<button id="showTeamModal" class="purple">view team</button>
									</div>
									<!--
									<div class="buttonSet">
										<span>communication on your injury</span>
										<button class="orange showNoteModal">ask a question</button><br>
									</div>
									-->									
								</div>						
						</div>	
					</div>
				</div>
			
			</div>
<!-- end -->
<hr>
<div class="txt__grey txt__smParagraph">You can set your avatar style, your name and email, basic information on the "my info" panel.  On the "my team" panel, you can view the team members on your injury, interact with them, and see who your adjuster is.  In addition, you can view your complete injury information and see what your team has been communicating about your injury.</div>			
		</div>
	</div>

</div>


	<div id="noteModal" class="zModal" style="display:none;">
		<a class="close-zModal"><div class="icon icon-times"></div></a>
		<div class="modalContent">
			<div class="modalHeader"><span class="modalTitle">team Question or Comment</span></div>
			<div class="modalBody">
				<div class="noteBody">
					<textarea class="greyPad" placeholder="type your comment or question here"></textarea>
					<input type="hidden" id="modalNoteType">
					<div class="noteitem nDate">{{\Carbon\Carbon::today()->toDateString()}}</div>
					<div class="noteitem nSent">status: </div>
					<div class="noteitem nTo">to: <b>{{$injury->getSquadName()}}</b></div>
					<div class="noteitem nClip"><img src=""></div>
				</div>
			</div>
			<div class="modalFooter">
				<button id="submitNoteButton" class="ghost">post question</button>
			</div>
		</div>
	</div>

	<div id="teamModal" class="zModal" style="display: none">
		<a class="close-zModal"><div class="icon icon-times"></div></a>
		<div class="modalContent">
			<div class="modalHeader"><span class="modalTitle">My Team</span></div>
			<div class="modalBody">
				<div class="zTableContainer">
					<ul class="zTable selectList">
						<li class="listTitle">{{$squad->squad_name}}</li>
						@foreach($squad_members as $member)
							<li>
								<a class="invisible">
									<span class="zCell icon"><div class="icon icon-user"></div></span>
									<span class="zCell label">{{$member->printMemberName()}}</span>
									<span class="zCell data">{{$member->printMemberPosition()}}</span>
								</a>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div id="whatsNextModal" class="zModal" style="display: none">
		<a class="close-zModal"><div class="icon icon-times"></div></a>
		<div class="modalContent">
			<div class="modalHeader"><span class="modalTitle">What's Next?</span></div>
			<div class="modalBody">
				@if($whats_next_step === 1)
				<div class="whatsNextSection" id="whatsNextCommunicate">
					Communication is critical in the early days of a claim.
					Make sure you stay in contact with your team and give them updates on how you're feeling,
					both physically and mentally. You can let your team know how you're feeling right now from the Diary section of the Zengarden.
				</div>
				@elseif($whats_next_step === 2)
				<div class="whatsNextSection" id="whatsNextSetAppointment">
					Keep your team up to date on any doctors appointments or physical therapist visits.
					Help everyone stay on the same page with your progress by posting updates after you've had appointments, tests, or x-rays.
					You can add new appointments and enter details from past visits on your Schedule page of the Zengarden.
				</div>
				@elseif($whats_next_step === 3)
				<div class="whatsNextSection" id="whatsNextLightDuty">
					Talk to your company about returning to work on light duty.
					Studies show that employees who return on light duty recover faster on average.
					When you feel ready, talk to your supervisor about how you could get back into the groove of work through light duty.
				</div>
				@elseif($whats_next_step === 4)
				<div class="whatsNextSection" id="whatsNextFullDuty">
					When you're ready to resume your full duties, your claim process is near the end.
					Keep communicating with your team to complete the process of getting your claim closed out. Congratulations, you're almost there!
				</div>
				@endif
			</div>
		</div>
	</div>

@endsection

@extends('layouts.zen5_zengarden_zp_layout')
@section('maincontent')

	<?php
    //pull injury info here
    if(session('injury_id')){
        $injury = \App\Injury::where('id', session('injury_id'))->first();
    }else{
        $injury = \App\Injury::where('injured_employee_id', Auth::user()->id)->first();
    }

    $future_appointments = DB::table('appointments')->join('injuries_care_locations', 'appointments.location_id', '=', 'injuries_care_locations.id')->where('appointments.injury_id', $injury->id)->where('appointments.cancelled', 0)->where('appointments.appointment_time', '>', \Carbon\Carbon::now())
		->select('appointments.*', 'injuries_care_locations.name', 'injuries_care_locations.email', 'injuries_care_locations.phone')->get();

    //$past_appointments = \App\Appointment::where('injury_id', $injury->id)->where('cancelled', 0)->whereDate('appointment_time', '<', \Carbon\Carbon::now())->get();

    $past_appointments = DB::table('appointments')->join('injuries_care_locations', 'appointments.location_id', '=', 'injuries_care_locations.id')->where('appointments.injury_id', $injury->id)->where('appointments.cancelled', 0)->where('appointments.appointment_time', '<=', \Carbon\Carbon::now())
        ->select('appointments.*', 'injuries_care_locations.name', 'injuries_care_locations.email', 'injuries_care_locations.phone')->get();


    ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="https://www.zenployees.com/js/parallax.js"></script>

<script type="text/javascript" src="/js/zp/mtr-datepicker-timezones.js"></script>
<script type="text/javascript" src="/js/zp/mtr-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="/css/zp/datepicker.css?<$date format='yyyy-MM-dd_HH:mm:ss'/$>" />
<link rel="stylesheet" type="text/css" href="/css/zp/datepicker-theme.css?<$date format='yyyy-MM-dd_HH:mm:ss'/$>" />

<style>

.mtr-content:hover{animation:pulse;animation-duration:.2s;}	
.mtr-arrow:hover{box-shadow:0px 0px 2px black !important;border-left:1px solid black;border-right:1px solid black;}	
@media (max-width: 768px) {	
.mtr-content{-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;}	
}
@media (max-width: 580px) {	
.mtr-content{pointer-events:none;}
}
</style>

<div class="pageContent bgimage-bgheader bghaze-rose fade-in">
		<!--**********-->
	<div class="headerBlock">
		<div class="headerImage wow">
			<div class="headerImage__panel-small">
				<img class="headerImage__panelimage-small" src="/images/clipart/schedule-o.png">
			</div>
		</div>
		<div class="container">
			<div class="pageImageHeader wow">
				<div class="pageImageHeader__title txt__white">
					<span>scheduling</span>
				</div>
				<div class="pageImageHeader__subTitle txt__white" style="border-color: var(--zencare);">
					<span>make and keep your appointments</span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="contentBlock bg__transparent txt__white">
		<div class="container">
			<div class="txt__white txt__smTitle-thin">Your zenSchedule will show any appointments you have, when they are, and where you have them. </div>
				
<!-- start -->
			<div class="boxGrid">
				
				<div class="boxGrid__box box-size-50">
					<div class="boxContent">
						<div class="zg-actionPanel" id="appointmentNew">
							
							<div class="panelTitle"><div class="icon inline icon-calendar-plus-o"></div> new appointments</div>
							<hr class="subtle">
							<div class="zAppointments" id="newAppointmentList">
								<div class="myAppointments noList">
									<span class="default">no new appointments</span>
								</div>
								<div class="myAppointments hasList">

									@foreach($future_appointments as $apt)
										<div class="apptCard" data-appointmentid="{{$apt->id}}">
											<div class="apptHeader">
												<div class="apptIcon"><div class="icon inline icon-clock"></div></div>
												<div class="apptTime">{{\Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $apt->appointment_time)->format("M j Y / g:ia")}}</div>
											</div>
											<input type="hidden" class="apptTimeFormatted" value="{{$apt->appointment_time}}">
											<div class="apptBody">
												<div class="apptDoctor">{{$apt->name}}</div>
												<div class="apptPhone">{{$apt->phone}}</div>
												<div class="apptEmail">
													<a href="mailto:{{$apt->email}}">
														{{$apt->email}}</a></div>
												<div class="apptDescription">{{$apt->description}}</div>
												<div class="apptSummary">{{$apt->summary}}</div>
												<div class="apptControls">
													<button data-appointmentid="{{$apt->id}}" class="cyan addSummaryModal">+ summary</button>
													<button data-appointmentid="{{$apt->id}}" class="blue addAppointmentModal">+ follow-up</button>
												</div>
												<div class="apptEdit">
													<button data-appointmentid="{{$apt->id}}" class="gold editAppointmentModal"><div class="icon inline icon-pencil"></div> edit</button>
												</div>												
											</div>
										</div>
									@endforeach
									
									<!--appointment card-->
										<!--
									<div class="apptCard">
										<div class="apptHeader">
											<div class="apptIcon"><div class="icon inline icon-clock"></div></div>
											<div class="apptTime">Feb 12 2021 / 10:30am</div>
										</div>
										<div class="apptBody">
											<div class="apptDoctor">Dr. James Billingsly</div>
											<div class="apptPhone">(324) 445-3324</div>
											<div class="apptEmail">
												<a href="mailto:drjames@hotmail.com">
													drjames@hotmail.com</a></div>
											<div class="apptControls">
												<button class="cyan addSummaryModal">+ summary</button>
												<button class="blue addAppointmentModal">+ follow-up</button>
											</div>
										</div>
									</div>
									<!-- end appointment card-->
									
									<!--appointment card-->
										<!--
									<div class="apptCard">
										<div class="apptHeader">
											<div class="apptIcon"><div class="icon inline icon-clock"></div></div>
											<div class="apptTime">Feb 12 2021 / 10:30am</div>
										</div>
										<div class="apptBody">
											<div class="apptDoctor">Dr. James Billingsly</div>
											<div class="apptPhone">(324) 445-3324</div>
											<div class="apptEmail">
												<a href="mailto:drjames@hotmail.com">
													drjames@hotmail.com</a></div>
											<div class="apptControls">
												<button class="cyan addSummaryModal">+ summary</button>
												<button class="blue addAppointmentModal">+ follow-up</button>
											</div>
										</div>
									</div>
									<!-- end appointment card-->
									
									<!--appointment card-->
										<!--
									<div class="apptCard">
										<div class="apptHeader">
											<div class="apptIcon"><div class="icon inline icon-clock"></div></div>
											<div class="apptTime">Feb 12 2021 / 10:30am</div>
										</div>
										<div class="apptBody">
											<div class="apptDoctor">Dr. James Billingsly</div>
											<div class="apptPhone">(324) 445-3324</div>
											<div class="apptEmail">
												<a href="mailto:drjames@hotmail.com">
													drjames@hotmail.com</a></div>
											<div class="apptControls">
												<button class="cyan addSummaryModal">+ summary</button>
												<button class="blue addAppointmentModal">+ follow-up</button>
											</div>
										</div>
									</div>
									<!-- end appointment card-->																			
								</div>
								<div class="appointmentListControls">
									<button class="green addAppointmentModal">add new appointment</button><br><br>
									<!--
									<button class="cyan addSummaryModal">add a summary</button><br><br>
									<button class="blue addAppointmentModal">add a follow-up</button>
									-->
								</div>
							</div>
						</div>	
					</div>
				</div>
			
				<div class="boxGrid__box box-size-50">
					<div class="boxContent">
						<div class="zg-actionPanel" id="appointmentPast">
							<div class="panelTitle"><div class="icon inline icon-calendar-check-o"></div> past appointments</div>
							<hr class="subtle">
							<div class="zAppointments" id="pastAppointmentList">
								<div class="pastAppointments noList">
									<span class="default">no past appointments</span>
								</div>
								<div class="pastAppointments hasList">

									@foreach($past_appointments as $apt)
										<div class="apptCard" data-appointmentid="{{$apt->id}}">
											<div class="apptHeader">
												<div class="apptIcon"><div class="icon inline icon-clock"></div></div>
												<div class="apptTime">{{\Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $apt->appointment_time)->format("M j Y / g:ia")}}</div>
											</div>
											<input type="hidden" class="apptTimeFormatted" value="{{$apt->appointment_time}}">
											<div class="apptBody">
												<div class="apptDoctor">{{$apt->name}}</div>
												<div class="apptPhone">{{$apt->phone}}</div>
												<div class="apptEmail">
													<a href="mailto:{{$apt->email}}">
														{{$apt->email}}</a></div>

												<div class="apptDescription">{{$apt->description}}</div>
												<div class="apptSummary">{{$apt->summary}}</div>
												<div class="apptControls">
													<button data-appointmentid="{{$apt->id}}" class="cyan addSummaryModal">+ summary</button>
													<button data-appointmentid="{{$apt->id}}" class="blue addAppointmentModal">+ follow-up</button>
												</div>
												<div class="apptEdit">
													<button data-appointmentid="{{$apt->id}}" class="gold editAppointmentModal"><div class="icon inline icon-pencil"></div> edit</button>
												</div>
											</div>
										</div>
									@endforeach

								<!--appointment card-->
										<!--
									<div class="apptCard">
										<div class="apptHeader">
											<div class="apptIcon"><div class="icon inline icon-clock"></div></div>
											<div class="apptTime">Feb 23 2021 / 10:30am</div>
										</div>
										<div class="apptBody">
											<div class="apptDoctor">Dr. Kareem Vartma</div>
											<div class="apptPhone">(324) 445-3324</div>
											<div class="apptEmail">
												<a href="mailto:kareem@hotmail.com">
													drkareem@hotmail.com</a></div>
											<div class="apptControls">
												<button class="cyan addSummaryModal">+ summary</button>
												<button class="blue addAppointmentModal">+ follow-up</button>
											</div>
										</div>
									</div>
									<!-- end appointment card-->
									
									<!--appointment card-->
										<!--
									<div class="apptCard">
										<div class="apptHeader">
											<div class="apptIcon"><div class="icon inline icon-clock"></div></div>
											<div class="apptTime">Feb 12 2021 / 10:30am</div>
										</div>
										<div class="apptBody">
											<div class="apptDoctor">Dr. James Billingsly</div>
											<div class="apptPhone">(324) 445-3324</div>
											<div class="apptEmail">
												<a href="mailto:drjames@hotmail.com">
													drjames@hotmail.com</a></div>
											<div class="apptControls">
												<button class="cyan addSummaryModal">+ summary</button>
												<button class="blue addAppointmentModal">+ follow-up</button>
											</div>
										</div>
									</div>
									<!-- end appointment card-->

										<!-- I COMMENTED THIS OUT, IT DOESN'T MAKE SENSE TO HAVE A FOLLOW-UP BUTTON NOT TIED TO AN APPOINTMENT LIKE THE OTHERS
									<div class="appointmentListControls">
										<button class="blue">add a follow-up</button>
									</div>
									-->
								</div>
							</div>
						</div>	
					</div>
				</div>
			
			</div>
<!-- end -->
<hr>
<div class="txt__grey txt__smParagraph">Your zenSchedule can be updated by you or your team.  If your team has your doctor and appointment information, they may add it.  If you add your own information, your Zenjuries records will be updated and your team will be alerted.</div>			
		</div>
	</div>

</div>

<div id="addAppointmentModal" class="zModal" style="display: none">
	<a class="close-zModal"><div class="icon icon-times"></div></a>
	<div class="modalContent">
		<div class="modalHeader">
			<span class="modalTitle" id="setAptTitle" style="display:none;">add an Appointment</span>
			<span class="modalTitle" id="setFollowTitle" style="display:auto;">set a Follow-Up</span>
		</div>
		<div class="modalBody">
			<div class="formBlock dark opaque">
				
				<div class="formGrid">
					<div class="formGrid__row">
						<div class="formGrid__cell w40 cell-label"><span class="label">Location Name</span></div>
						<div class="formGrid__cell w60 cell-data"><input type="text" id="addAppointmentName"></div>
					</div>
					<div class="formGrid__row">
						<div class="formGrid__cell w40 cell-label"><span class="label">Location Phone</span></div>
						<div class="formGrid__cell w60 cell-data"><input inputmode="numeric" pattern="[0-9]*" type="number" id="addAppointmentPhone" style="font-weight:900;"></div>
					</div>
					<div class="formGrid__row">
						<div class="formGrid__cell w40 cell-label"><span class="label">Location Email</span></div>
						<div class="formGrid__cell w60 cell-data"><input type="email" id="addAppointmentEmail"></div>
					</div>
				</div>

				<hr class="subtle">

				<div id="datePickerPlaceholder" style="height:260px;touch-action: manipulation;">					
					<span class="cellLabel center">Select Time & Date</span>
					<div id="zDateTime"></div>
				</div>			
				<div class="formGrid">
						<div class="formGrid__cell w20 cell-label"><span class="label">Notes</span></div>
						<div class="formGrid__cell w80 cell-data"><textarea id="addAppointmentNote" style="width:100%;"></textarea></div>

				</div>
		
			</div>
		</div>
		<div class="modalFooter">
			<button class="green" id="setApptBtn">set this appointment</button>
			<button class="blue" id="setFollowBtn" style="display:none;">add follow-up</button>
		</div>
	</div>		
</div>

<div id="editAppointmentModal" class="zModal" style="display: none">
	<a class="close-zModal"><div class="icon icon-times"></div></a>
	<div class="modalContent">
		<div class="modalHeader">
			<span class="modalTitle">Edit Appointment</span>
		</div>
		<div class="modalBody">
			<div class="formBlock dark opaque">
				
				<div class="formGrid">
					<div class="formGrid__row">
						<div class="formGrid__cell w40 cell-label"><span class="label">Location Name</span></div>
						<div class="formGrid__cell w60 cell-data"><input type="text" id="editAppointmentName"></div>
					</div>
					<div class="formGrid__row">
						<div class="formGrid__cell w40 cell-label"><span class="label">Location Phone</span></div>
						<div class="formGrid__cell w60 cell-data"><input inputmode="numeric" pattern="[0-9]*" type="number" id="editAppointmentPhone" style="font-weight:900;"></div>
					</div>
					<div class="formGrid__row">
						<div class="formGrid__cell w40 cell-label"><span class="label">Location Email</span></div>
						<div class="formGrid__cell w60 cell-data"><input type="email" id="editAppointmentEmail"></div>
					</div>
				</div>

				<hr class="subtle">

				<div id="editAppointmentDatePickerPlaceholder" style="height:260px;touch-action: manipulation;">					
					<span class="cellLabel center">Select Time & Date</span>
					<div id="zDateTimeEditAppointment"></div>
				</div>			
				<div class="formGrid">
						<div class="formGrid__cell w20 cell-label"><span class="label">Notes</span></div>
						<div class="formGrid__cell w80 cell-data"><textarea id="editAppointmentNote" style="width:100%;"></textarea></div>

				</div>
				
				<input type="hidden" id="editAppointmentID">
		
			</div>
		</div>
		<div class="modalFooter">
			<button class="green" id="editApptBtnSave">update appointment</button>
		</div>
	</div>		
</div>

<div id="addSummaryModal" class="zModal" style="display:none;">
	<a class="close-zModal"><div class="icon icon-times"></div></a>
	<div class="modalContent">
		<div class="modalHeader"><span class="modalTitle">add a Summary</span></div>
		<div class="modalBody">
			<input type="hidden" id="addSummaryModalAppointmentID">
			<div class="formBlock dark opaque">
				<div class="appointmentTitle">Appointment on <span>2021-01-01 13:00:00</span></div>			
				<textarea placeholder="Add any details you have from the appointment, such as doctor's notes, expected time until maximum medical improvement, etc." style="width:100%;height:200px;"></textarea>
			</div>
		</div>
		<div class="modalFooter">
			<button id="submitSummaryModal" class="cyan">add summary</button>
		</div>
	</div>		
</div>
		
@endsection

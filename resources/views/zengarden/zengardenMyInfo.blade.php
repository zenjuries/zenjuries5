@extends('layouts.zen5_zengarden_zp_layout')
@section('maincontent')

<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="https://www.zenployees.com/js/parallax.js"></script>
<script src="{{ URL::asset('/js/croppie.js') }}"></script>
<link rel="stylesheet" href="{{URL::asset('/css/croppie.css')}}">


<div class="pageContent bgimage-bgheader bghaze-ocean fade-in">
		<!--**********-->
	<div class="headerBlock">
		<div class="headerImage">
			<div class="headerImage__panel-small">
				<img class="headerImage__panelimage-small" src="/images/clipart/myinfo-o.png">
			</div>
		</div>
		<div class="container">
			<div class="pageImageHeader">
				<div class="pageImageHeader__title txt__white">
					<span>info & profile</span>
				</div>
				<div class="pageImageHeader__subTitle txt__white" style="border-color: var(--zencare);">
					<span>check your info, change your profile</span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="contentBlock bg__transparent txt__white">
		<div class="container">
			<div class="txt__white txt__smTitle-thin">Here you can update your personal info, view your injury team and information, and change app settings. </div> 
				
<!-- start -->
			<div class="boxGrid">	
			
				<div class="boxGrid__box box-size-70">
					<div class="boxContent">
						<div class="zg-actionPanel" id="appointmentPast" style="min-height:420px;">
							<div class="panelTitle"><div class="icon inline icon-user"></div> my info</div>
							<hr class="subtle">
							<div style="height:12px;"></div>
							<ul class="zTable padded">
								<li>
									<span class="zCell label">Name</span>
									<span class="zCell data" id="myInfoName">{{Auth::user()->name}}</span></li>
								<li>
									<span class="zCell label">Email</span>
									<span class="zCell data" id="myInfoEmail">{{Auth::user()->email}}</span></li>
								<li>
									<span class="zCell label">Phone</span>
									<span class="zCell data" id="myInfoPhone">{{Auth::user()->phone}}</span></li>
								<li>
									<span class="zCell spacer"></span>
									<span class="zCell spacer"></span>
								</li>	
								<li>
									<span class="zCell label">Comments</span>
									<span class="zCell data" id="myInfoComments">{{(empty(Auth::user()->description) ? "comments here" : Auth::user()->description)}}</span></li>
							</ul>
							
							<div class="showDesktopOnly">						
							<div class="settingsButtonArray inline">
								<div class="buttonSet">
									<span>update your information</span>
									<button class="cyan showInfoModal">update info</button><br>
								</div>
								<div class="buttonSet">
									<span>download your employee expectations</span>
									<a href="/downloadEmployeeExpectations" class="invisible"><button class="red">download</button></a><br>
								</div>
							</div>
							</div>
							
							<div class="showMobileOnly">
							<div class="settingsButtonArray inline">
								<div class="buttonSet">
									<span>update your information</span>
									<button class="cyan showInfoModal">update info</button><br>
								</div>
								<div class="buttonSet">
									<span>view your employee expectations</span>
									<a href="/zengarden/viewEmployeeExpectations" class="invisible"><button class="red">view</button></a><br>
								</div>
							</div>
							</div>
																					
						</div>	
					</div>
				</div>
				
<div class="boxGrid__box box-size-30">
					<div class="boxContent">
						<div class="zg-actionPanel" id="appointmentNew" style="min-height:420px;">
							<div class="panelTitle"><div class="icon inline icon-user"></div> my avatar</div>
							<hr>
							<div class="profileAvatarColor" style="background-color: {{Auth::user()->avatar_color}}"><div class="avatarIcon" style="background-image:url({{Auth::user()->photo_url}})"></div>
							</div>
							<div class="settingsButtonArray">
								<input id="currentAvatarNumber" type="hidden" value="{{Auth::user()->avatar_number}}">
								<div class="buttonSet">
									<span>change your color & icon</span>
									<button class="blue showIconModal">set icon</button>
									<button class="purple showColorModal">set color</button>
								</div>
							</div>
						</div>
					</div>
				</div>
											
			</div>
<!-- end -->
			<hr>
			<div class="txt__grey txt__smParagraph">You can set your avatar style, your name and email, basic information on the "my info" panel.  On the "my team" panel, you can view the team members on your injury, interact with them, and see who your adjuster is.  In addition, you can view your complete injury information and see what your team has been communicating about your injury.</div>
			
		</div>
	</div>
</div>

<!--color modal-->
<div id="colorModal" class="zModal" style="display:none;">
	<a class="close-zModal"><div class="icon icon-times"></div></a>
	<div class="modalContent">
		<div class="modalHeader"><span class="modalTitle">set Color</span></div>
		<div class="modalBody">

		<div id="colorpicker"></div>
	
		</div>
		<div class="modalFooter">
			<button class="purple" id="colorModalSave">save color</button>
		</div>
	</div>
</div>
<script>

</script>
<script>

</script>

<!--icon modal-->
<div id="iconModal" class="zModal" style="display:none;">
	<a class="close-zModal jBox-closeButton"><div class="icon icon-times"></div></a>
	<div class="modalContent">
		<div class="modalHeader"><span class="modalTitle">set Icon</span></div>
		<div class="modalBody">

		<ul class="iconSelector">
				<li><a data-avatar="avatar1" style="background-image: url('/images/avatar_icons/avatar1.png')"></a></li>
				<li><a data-avatar="avatar2" style="background-image: url('/images/avatar_icons/avatar2.png')"></a></li>
				<li><a data-avatar="avatar3" style="background-image: url('/images/avatar_icons/avatar3.png')"></a></li>
				<li><a data-avatar="avatar4" style="background-image: url('/images/avatar_icons/avatar4.png')"></a></li>
				<li><a data-avatar="avatar5" style="background-image: url('/images/avatar_icons/avatar5.png')"></a></li>
				<li><a data-avatar="avatar6" style="background-image: url('/images/avatar_icons/avatar6.png')"></a></li>
				<li><a data-avatar="avatar7" style="background-image: url('/images/avatar_icons/avatar7.png')"></a></li>
				<li><a data-avatar="avatar8" style="background-image: url('/images/avatar_icons/avatar8.png')"></a></li>
				<li><a data-avatar="avatar9" style="background-image: url('/images/avatar_icons/avatar9.png')"></a></li>
				<li><a data-avatar="avatar10" style="background-image: url('/images/avatar_icons/avatar10.png')"></a></li>
				<li><a data-avatar="avatar11" style="background-image: url('/images/avatar_icons/avatar11.png')"></a></li>
				<li><a data-avatar="avatar12" style="background-image: url('/images/avatar_icons/avatar12.png')"></a></li>
				<li><a data-avatar="avatar13" style="background-image: url('/images/avatar_icons/avatar13.png')"></a></li>
				<li><a data-avatar="avatar14" style="background-image: url('/images/avatar_icons/avatar14.png')"></a></li>
				<li><a data-avatar="avatar15" style="background-image: url('/images/avatar_icons/avatar15.png')"></a></li>
				<li><a data-avatar="avatar16" style="background-image: url('/images/avatar_icons/avatar16.png')"></a></li>
				<li><a data-avatar="avatar17" style="background-image: url('/images/avatar_icons/avatar17.png')"></a></li>
				<li><a data-avatar="avatar18" style="background-image: url('/images/avatar_icons/avatar18.png')"></a></li>
				<li><a data-avatar="avatar19" style="background-image: url('/images/avatar_icons/avatar19.png')"></a></li>
				<li><a data-avatar="avatar20" style="background-image: url('/images/avatar_icons/avatar20.png')"></a></li>
				<li><a data-avatar="avatar21" style="background-image: url('/images/avatar_icons/avatar21.png')"></a></li>
				<li><a data-avatar="avatar22" style="background-image: url('/images/avatar_icons/avatar22.png')"></a></li>
				<li><a data-avatar="avatar23" style="background-image: url('/images/avatar_icons/avatar23.png')"></a></li>
				<li><a data-avatar="avatar24" style="background-image: url('/images/avatar_icons/avatar24.png')"></a></li>
				<li><a data-avatar="avatar25" style="background-image: url('/images/avatar_icons/avatar25.png')"></a></li>
				<li><a data-avatar="avatar26" style="background-image: url('/images/avatar_icons/avatar26.png')"></a></li>
				<li><a data-avatar="avatar27" style="background-image: url('/images/avatar_icons/avatar27.png')"></a></li>
				<li><a data-avatar="avatar28" style="background-image: url('/images/avatar_icons/avatar28.png')"></a></li>
				<li><a data-avatar="avatar29" style="background-image: url('/images/avatar_icons/avatar29.png')"></a></li>
				<li><a data-avatar="avatar30" style="background-image: url('/images/avatar_icons/avatar30.png')"></a></li>
				<li><a data-avatar="avatar31" style="background-image: url('/images/avatar_icons/avatar31.png')"></a></li>
				<li><a data-avatar="avatar32" style="background-image: url('/images/avatar_icons/avatar32.png')"></a></li>
				<li><a data-avatar="avatar33" style="background-image: url('/images/avatar_icons/avatar33.png')"></a></li>
				<li><a data-avatar="avatar34" style="background-image: url('/images/avatar_icons/avatar34.png')"></a></li>
				<li><a data-avatar="avatar35" style="background-image: url('/images/avatar_icons/avatar35.png')"></a></li>
				<li><a data-avatar="avatar36" style="background-image: url('/images/avatar_icons/avatar36.png')"></a></li>
				<li><a data-avatar="avatar37" style="background-image: url('/images/avatar_icons/avatar37.png')"></a></li>
				<li><a data-avatar="avatar38" style="background-image: url('/images/avatar_icons/avatar38.png')"></a></li>
				<li><a data-avatar="avatar39" style="background-image: url('/images/avatar_icons/avatar39.png')"></a></li>
				<li><a data-avatar="avatar40" style="background-image: url('/images/avatar_icons/avatar40.png')"></a></li>
				<li><a data-avatar="avatar41" style="background-image: url('/images/avatar_icons/avatar41.png')"></a></li>
				<li><a data-avatar="avatar42" style="background-image: url('/images/avatar_icons/avatar42.png')"></a></li>
				<li><a data-avatar="avatar43" style="background-image: url('/images/avatar_icons/avatar43.png')"></a></li>
				<li><a data-avatar="avatar44" style="background-image: url('/images/avatar_icons/avatar44.png')"></a></li>
				<li><a data-avatar="avatar45" style="background-image: url('/images/avatar_icons/avatar45.png')"></a></li>
				<li><a data-avatar="avatar46" style="background-image: url('/images/avatar_icons/avatar46.png')"></a></li>
				<li><a data-avatar="avatar47" style="background-image: url('/images/avatar_icons/avatar47.png')"></a></li>
				<li><a data-avatar="avatar48" style="background-image: url('/images/avatar_icons/avatar48.png')"></a></li>
				<li><a data-avatar="avatar49" style="background-image: url('/images/avatar_icons/avatar49.png')"></a></li>
				<li><a data-avatar="avatar50" style="background-image: url('/images/avatar_icons/avatar50.png')"></a></li>
				<li><a data-avatar="avatar51" style="background-image: url('/images/avatar_icons/avatar51.png')"></a></li>
				<li><a data-avatar="avatar52" style="background-image: url('/images/avatar_icons/avatar52.png')"></a></li>
				<li><a data-avatar="avatar53" style="background-image: url('/images/avatar_icons/avatar53.png')"></a></li>
				<li><a data-avatar="avatar54" style="background-image: url('/images/avatar_icons/avatar54.png')"></a></li>
				<li><a data-avatar="avatar55" style="background-image: url('/images/avatar_icons/avatar55.png')"></a></li>
			</ul>
	
		</div>
		<div class="modalFooter">
			<button class="blue" id="iconModalSave">save icon</button>
			<button class="cyan showImageModal">upload your own image</button>
		</div>
	</div>
</div>
<script>

/*
  	$('.close-zModal').on('click', function(){
	   $('.jbox-wrapper').click();
  	});
  	*/
</script>

<!--image modal-->
<div id="imageModal" class="zModal" style="display:none;">
	<a class="close-zModal"><div class="icon icon-times"></div></a>
	<div class="modalContent">
		<div class="modalHeader"><span class="modalTitle">set Image</span></div>
		<div class="modalBody">
		<input type="file" id="imageModalFileInput">
		<div id="croppieDiv"></div>
	
		</div>
		<div class="modalFooter">
			<button class="purple" id="imageModalSave">save image</button>
		</div>
	</div>
</div>
<script>



</script>

<!--info modal-->
<div id="infoModal" class="zModal" style="display:none;">
	<a class="close-zModal"><div class="icon icon-times"></div></a>
	<div class="modalContent">
		<div class="modalHeader"><span class="modalTitle">set Info</span></div>
		<div class="modalBody">
			<div class="formBlock dark opaque">
				<div class="errors" id="infoModalErrors" style="display: none; color: red"></div>
				<div class="formGrid">
					<div class="formGrid__row">
					<label>Name</label><input id="infoModalName" type="text" value="{{Auth::user()->name}}">
					</div>
					<div class="formGrid__row">
					<label>Email</label><input id="infoModalEmail" type="text" value="{{Auth::user()->email}}">
					</div>
					<div class="formGrid__row">
					<label>Phone</label><input id="infoModalPhone" type="text" value="{{Auth::user()->phone}}">
					</div>
				
					<div class="formGrid__row">
					<label>Comments</label><textarea id="infoModalDescription" value="{{Auth::user()->description}}"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="modalFooter">
			<button class="cyan" id="infoModalSave">save info</button>
		</div>
	</div>
</div>

@endsection

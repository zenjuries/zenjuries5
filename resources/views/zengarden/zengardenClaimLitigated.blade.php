@extends('layouts.zen5_zengarden_zp_layout_no_nav')
@section('maincontent')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="https://www.zenployees.com/js/parallax.js"></script>

    <div class="pageContent fade-in">
        <!--**********-->
        <div class="headerBlock parallax" id="parallax-1" data-image-src="https://www.zenployees.com/images/backgrounds/stonesbg.jpg" style="background-color:#3597d0;">
            <div class="headerImage wow animate__animated animate__bounceInRight">
                <!--<div class="headerImage__panel-small">
                   <img class="headerImage__panelimage-small" src="https://www.zenployees.com/images/clipart/help-o.png">
                </div>-->
            </div>
            <div class="container">
                <div class="pageImageHeader wow animate__animated animate__bounceInLeft">
                    <div class="pageImageHeader__title txt__white">
                        <span>Claim Litigated</span>
                    </div>
                    <div class="pageImageHeader__subTitle txt__white" style="border-color:#bd9979;">
                        <span>Sorry, your claim has been marked as "Under Litigation", and you will be unable to access the Zengarden.
            If this was done by mistake please contact someone at your company. Thank you!</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.zen5_layout_simple')
@section('maincontent')

<style>
    .errorBackground{background-image:url('images/elements/errors/cloudcutout.png');width:100%;height:50%;background-position:center center;background-size:cover;padding-top:120px;}
    .errorBlock{display:table;width:max-content;margin:0 auto;}
    .errorContent{display:table-row;}
    .errorInfo{user-select: none;display:table-cell;}
    .errorImage{user-select: none;display:table-cell;text-align:center;}
    .errorText{width:100%;text-align:center;font-size:2rem;margin-top:40px;}
    
.floating { 
    animation-name: floating;
    animation-duration: 3s;
    animation-iteration-count: infinite;
    animation-timing-function: ease-in-out;
    margin-left: 30px;
    margin-top: 5px;
}
 
@keyframes floating {
    0% { transform: translate(0,  0px); }
    50%  { transform: translate(0, 15px); }
    100%   { transform: translate(0, -0px); }   
}

.float-shadow { 
    animation-name: float-shadow;
    animation-duration: 3s;
    animation-iteration-count: infinite;
    animation-timing-function: ease-in-out;
    margin-left: 30px;
    margin-top: 5px;
}
 
@keyframes float-shadow {
    0% { transform: scale(.8);opacity:.2; }
    50%  { transform: scale(1);opacity:.8; }
    100%   { transform: scale(.8);opacity:.2; }   
}
</style>

<div class="pageContent bgimage-bgheader fade-in" style="background-color:#333;">		

<div class="errorBackground">
    <div class="errorBlock">
        <div class="errorContent">
            <div class="errorInfo"><img class="animate__animated animate__jello" src="images/elements/errors/error404.png"></div>
            <div class="errorImage"><img class="floating" src="images/elements/errors/zf-hmm.png"><br>
            <img class="float-shadow" src="images/elements/errors/zf-shadow.png"></div>
        </div>
    </div>
</div>
<div class="errorText">We cant seem to find this page!  Click the button below to go home...</div> 
<br><br><br>
<div class="divCenter"><button onclick="window.location.href='/zenjuriesLogin'">back to Login Page</button></div>

</div>

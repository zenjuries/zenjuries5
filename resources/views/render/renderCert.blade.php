<?php
//EDITED - TYLER
$company = \App\Company::where('id', Auth::user()->getCompany()->id)->first();

$team = \App\Team::where('id', $company->team_id)->first();

$subscription = DB::table('team_subscriptions')->where('team_id', $team->id)->orderBy('created_at', 'desc')->first();

if($company->registered_at !== NULL){
    $startDate = \Carbon\Carbon::parse($company->registered_at);

    $endDate = \Carbon\Carbon::parse($company->registered_at);
    $endDate->addYear()->subDay();
}else if(is_null($subscription)){
    $startDate = \Carbon\Carbon::parse($company->created_at);

    $endDate = \Carbon\Carbon::parse($company->created_at);
    $endDate->addYear()->subDay();
}else{
    $startDate = \Carbon\Carbon::parse($subscription->created_at);

    $endDate = \Carbon\Carbon::parse($subscription->created_at);
    $endDate->addYear()->subDay();
}

//we'll use the company's most recent subscription date or sign-up date if no subscription, and increment it until it matches the current year
while(\Carbon\Carbon::now()->gte($endDate)){
    $startDate->addYear();
    $endDate->addYear();
}
?>

        <link rel="stylesheet" href="{{URL::asset('/css/render/cert.css') }}">

<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>

</head>
<body>
        <div class="certificate">
            <div class="title">Zenjuries Certificate</div>
            <div class="certBody">
                THIS CERTIFICATE CERTIFIES THAT<br>
                <h2><?php echo $company->company_name; ?></h2><br>
                HAS SUCCESSFULLY ENROLLED IN THE EMPLOYEE INJURY <br>
                MANAGEMENT SOFTWARE <br>
                <h3>ZENJURIES</h3>
                <hr width="50%">
                5% SCHEDULE CREDIT OFF THE WORKERS COMPENSATION <br>
                INSURANCE PREMIUM IS AVAILABLE FOR THE FOLLOWING <br>
                EFFECTIVE DATES OF ENROLLMENT:<br>
            </div>
                <span>
                    <div id="certDate">
                        <div id="dateString">
                            <b><?php echo $startDate->toFormattedDateString() . ' - ' . $endDate->toFormattedDateString(); ?></b>
                        </div>
                        date</div>
                    <div id="certEnrollmentDept">
                        <div id="certSig"></div><br>
                        Enrollment Department
                    </div>
                </span>
        </div>
</body>
</html>

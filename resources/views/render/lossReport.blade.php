<?php
/* TODO: DELETE COMPANY ID */
//EDITED - TYLER
$company_id = Auth::user()->getCompany()->id;
/* TODO GET ME BLUE TEXT */

$company = \App\Company::where('id', $company_id)->first();
/*
$owner_id = \App\TeamUser::where('team_id', $company->team_id)->where('role', 'owner')->value('user_id');
$owner = \App\User::where('id', $owner_id)->first();
*/
//'2016-10-11', '2016-10-25'


$start_date = $data['start_date'];
$end_date = $data['end_date'];
$claim_type = $data['claim_type'];
$claim_status = $data['claim_status'];

if($claim_type == "moderate"){
    $claim_type = "work comp";
}else if($claim_type == "mild"){
    $claim_type = "first aid";
}


Log::debug($start_date);
Log::debug($end_date);
Log::debug($claim_type);
Log::debug($claim_status);
$start_date = \Carbon\Carbon::createFromFormat('Y-m-d', $start_date)->format('Y-m-d');
$end_date = \Carbon\Carbon::createFromFormat('Y-m-d', $end_date)->format('Y-m-d');
// $start_date = \Carbon\Carbon::createFromFormat('Y-m-d', $start_date)->format('m-d-Y');
// $end_date = \Carbon\Carbon::createFromFormat('Y-m-d', $end_date)->format('m-d-Y');

Log::debug("beginning of view");
Log::debug($start_date);
Log::debug($end_date);



        if($claim_type == "work comp" && $claim_status == "open"){
            $injuries = \App\Injury::where('hidden', 0)->where('company_id', $company->id)->where('type', 'Work Comp')->where('resolved', 0)->whereBetween('created_at', array($start_date, $end_date))->orderBy('created_at', 'desc')->get();
            Log::debug("1st");
        }else if($claim_type == "work comp" && $claim_status == "resolved"){
            $injuries = \App\Injury::where('hidden', 0)->where('company_id', $company->id)->where('type', 'Work Comp')->where('resolved', 1)->whereBetween('created_at', array($start_date, $end_date))->orderBy('created_at', 'desc')->get();
        }else if($claim_type == "work comp" && $claim_status == "both"){
            $injuries = \App\Injury::where('hidden', 0)->where('company_id', $company->id)->where('type', 'Work Comp')->whereBetween('created_at', array($start_date, $end_date))->orderBy('created_at', 'desc')->get();
        }else if($claim_type == "first aid" && $claim_status == "open"){
            $injuries = \App\Injury::where('hidden', 0)->where('company_id', $company->id)->where('type', 'First Aid')->where('resolved', 0)->whereBetween('created_at', array($start_date, $end_date))->orderBy('created_at', 'desc')->get();
        }else if($claim_type == "first aid" && $claim_status == "resolved"){
            $injuries = \App\Injury::where('hidden', 0)->where('company_id', $company->id)->where('type', 'First Aid')->where('resolved', 1)->whereBetween('created_at', array($start_date, $end_date))->orderBy('created_at', 'desc')->get();
        }else if($claim_type == "first aid" && $claim_status == "both"){
            $injuries = \App\Injury::where('hidden', 0)->where('company_id', $company->id)->where('type', 'First Aid')->whereBetween('created_at', array($start_date, $end_date))->orderBy('created_at', 'desc')->get();
        }else if($claim_type == "both" && $claim_status == "open"){
            $injuries = \App\Injury::where('hidden', 0)->where('company_id', $company->id)->where('resolved', 0)->whereBetween('created_at', array($start_date, $end_date))->orderBy('created_at', 'desc')->get();
        }else if($claim_type == "both" && $claim_status == "resolved"){
            $injuries = \App\Injury::where('hidden', 0)->where('company_id', $company->id)->where('resolved', 1)->whereBetween('created_at', array($start_date, $end_date))->orderBy('created_at', 'desc')->get();
        }else if($claim_type == "both" && $claim_status == "both"){
            $injuries = \App\Injury::where('hidden', 0)->where('company_id', $company->id)->whereBetween('created_at', array($start_date, $end_date))->orderBy('created_at', 'desc')->get();
        }
        Log::debug("injuries");
        Log::debug($injuries);

if(count($injuries)){
    Log::debug("hitting count if");
    //variables for summary section
    $injury_type_array = array();
    $injury_location_array = array();
    $final_cost_array = array();
    $reserve_cost_array = array();
    $medical_cost_array = array();
    $indemnity_cost_array = array();
    $legal_cost_array = array();
    $misc_cost_array = array();
    $reserve_medical_array = array();
    $reserve_indemnity_array = array();
    $reserve_legal_array = array();
    $reserve_misc_array = array();
    Log::debug("final cost array");
    Log::debug($final_cost_array);

            foreach($injuries as $injury){
                $types = \App\InjuryType::where('injury_id', $injury->id)->get();
                foreach($types as $type){
                    array_push($injury_type_array, $type->type);
                }
                $locations = \App\InjuryLocation::where('injury_id', $injury->id)->get();
                foreach($locations as $location){
                    array_push($injury_location_array, $location->location);
                }
                if(!is_null($injury->final_cost)){
                    array_push($final_cost_array, $injury->final_cost);
                }

                    $medical = \App\MedicalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                    if(!is_null($medical)){
                        $injury->current_paid_medical = $medical;
                        array_push($medical_cost_array, $medical);
                    }else{
                        $injury->current_paid_medical = NULL;
                        array_push($medical_cost_array, 0);
                    }

                    $indemnity = \App\IndemnityCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                    if(!is_null($indemnity)){
                        $injury->current_paid_indemnity = $indemnity;
                        array_push($indemnity_cost_array, $indemnity);
                    }else{
                        $injury->current_paid_indemnity = NULL;
                        array_push($indemnity_cost_array, 0);
                    }

                    $legal = \App\LegalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                    if(!is_null($legal)){
                        $injury->current_paid_legal = $legal;
                        array_push($legal_cost_array, $legal);
                    }else{
                        $injury->current_paid_legal = NULL;
                        array_push($legal_cost_array, 0);
                    }

                    $misc = \App\MiscellaneousCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                    if(!is_null($misc)){
                        $injury->current_paid_misc = $misc;
                        array_push($misc_cost_array, $misc);
                    }else{
                        $injury->current_paid_misc = NULL;
                        array_push($misc_cost_array, 0);
                    }
                    //((is_null($misc)) ? array_push($misc_cost_array, 0) : array_push($misc_cost_array, $misc));

                    $r_medical = \App\ReserveMedicalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                    if(!is_null($r_medical)){
                        $injury->current_reserve_medical = $r_medical;
                        array_push($reserve_medical_array, $r_medical);
                    }else{
                        $injury->current_reserve_medical = NULL;
                        array_push($reserve_medical_array, 0);
                    }
                    //((is_null($r_medical)) ? array_push($reserve_medical_array, 0) : array_push($reserve_medical_array, $r_medical));

                    $r_indemnity = \App\ReserveIndemnityCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                    if(!is_null($r_indemnity)){
                        $injury->current_reserve_indemnity = $r_indemnity;
                        array_push($reserve_indemnity_array, $r_indemnity);
                    }else{
                        $injury->current_reserve_indemnity = NULL;
                        array_push($reserve_indemnity_array, 0);
                    }
                    //((is_null($r_indemnity)) ? array_push($reserve_indemnity_array, 0) : array_push($reserve_indemnity_array, $r_indemnity));

                    $r_legal = \App\ReserveLegalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                    if(!is_null($r_legal)){
                        $injury->current_reserve_legal = $r_legal;
                        array_push($reserve_legal_array, $r_legal);
                    }else{
                        $injury->current_reserve_legal = NULL;
                        array_push($reserve_legal_array, 0);
                    }
                    //((is_null($r_legal)) ? array_push($reserve_legal_array, 0) : array_push($reserve_legal_array, $r_legal));

                    $r_misc = \App\ReserveMiscellaneousCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                    if(!is_null($r_misc)){
                        $injury->current_reserve_misc = $r_misc;
                        array_push($reserve_misc_array, $r_misc);
                    }else{
                        $injury->current_reserve_misc = NULL;
                        array_push($reserve_misc_array, 0);
                    }
                    //((is_null($r_misc)) ? array_push($reserve_misc_array, 0) : array_push($reserve_misc_array, $r_misc));
            }

    $value_count = array_count_values($injury_type_array);
    Log::debug($value_count);
    $most_common_type = array_search(max($value_count), $value_count);
    $type_count = $value_count[$most_common_type];

    $value_count = array_count_values($injury_location_array);
    $most_common_location = array_search(max($value_count), $value_count);
    $location_count = $value_count[$most_common_location];

    if(count($medical_cost_array)){
        Log::debug($medical_cost_array);
        $medical_total = array_sum($medical_cost_array);
        $medical_average = array_sum($medical_cost_array) / count($medical_cost_array);
        $medical_total = number_format((float)$medical_total, 2, '.', '');
        $medical_average = number_format((float)$medical_average, 2, '.', '');
    }else{
        $medical_total = NULL;
        $medical_average = NULL;
    }
/*
    if(count($reserve_cost_array)){
        $reserve_total = array_sum($reserve_cost_array);
        $reserve_average = array_sum($reserve_cost_array) / count($reserve_cost_array);
        $reserve_total = number_format((float)$reserve_total, 2, '.', '');
        $reserve_average = number_format((float)$reserve_average, 2, '.', '');
    }else{
        $reserve_total = NULL;
        $reserve_average = NULL;
    }

*/

    if(count($reserve_medical_array)){
        $reserve_medical_total = array_sum($reserve_medical_array);
        $reserve_medical_average = array_sum($reserve_medical_array) / count($reserve_medical_array);
        $reserve_medical_total = number_format((float)$reserve_medical_total, 2, '.', '');
        $reserve_medical_average = number_format((float)$reserve_medical_average, 2, '.', '');
    }else{
        $reserve_medical_total = NULL;
        $reserve_medical_average = NULL;
    }

    if(count($reserve_legal_array)){
        $reserve_legal_total = array_sum($reserve_legal_array);
        $reserve_legal_average = array_sum($reserve_legal_array) / count($reserve_legal_array);
        $reserve_legal_total = number_format((float)$reserve_legal_total, 2, '.', '');
        $reserve_legal_average = number_format((float)$reserve_legal_average, 2, '.', '');
    }else{
        $reserve_legal_total = NULL;
        $reserve_legal_average = NULL;
    }

    if(count($reserve_indemnity_array)){
        $reserve_indemnity_total = array_sum($reserve_indemnity_array);
        $reserve_indemnity_average = array_sum($reserve_indemnity_array) / count($reserve_indemnity_array);
        $reserve_indemnity_total = number_format((float)$reserve_indemnity_total, 2, '.', '');
        $reserve_indemnity_average = number_format((float)$reserve_indemnity_average, 2, '.', '');
    }else{
        $reserve_indemnity_total = NULL;
        $reserve_indemnity_average = NULL;
    }

    if(count($reserve_misc_array)){
        $reserve_misc_total = array_sum($reserve_misc_array);
        $reserve_misc_average = array_sum($reserve_misc_array) / count($reserve_misc_array);
        $reserve_misc_total = number_format((float)$reserve_misc_total, 2, '.', '');
        $reserve_misc_average = number_format((float)$reserve_misc_average, 2, '.', '');
    }else{
        $reserve_misc_total = NULL;
        $reserve_misc_average = NULL;
    }

    if(count($indemnity_cost_array)){
        $indemnity_total = array_sum($indemnity_cost_array);
        $indemnity_average = array_sum($indemnity_cost_array) / count($indemnity_cost_array);
        $indemnity_total = number_format((float)$indemnity_total, 2, '.', '');
        $indemnity_average = number_format((float)$indemnity_average, 2, '.', '');
    }else{
        $indemnity_total = NULL;
        $indemnity_average = NULL;
    }

    if(count($legal_cost_array)){
        $legal_total = array_sum($legal_cost_array);
        $legal_average = array_sum($legal_cost_array) / count($legal_cost_array);
        $legal_total = number_format((float)$legal_total, 2, '.', '');
        $legal_average = number_format((float)$legal_average, 2, '.', '');
    }else{
        $legal_total = NULL;
        $legal_average = NULL;
    }

    if(count($misc_cost_array)){
        $misc_total = array_sum($misc_cost_array);
        $misc_average = array_sum($misc_cost_array) / count($misc_cost_array);
        $misc_total = number_format((float)$misc_total, 2, '.', '');
        $misc_average = number_format((float)$misc_average, 2, '.', '');
    }else{
        $misc_total = NULL;
        $misc_average = NULL;
    }

    /* FINAL COSTS */
//number_format((float), 2, '.', '')
    if(count($final_cost_array)){
        $final_cost_total = array_sum($final_cost_array);
        Log::debug("final cost total");
        Log::debug($final_cost_total);
        $final_cost_average = $final_cost_total / count($final_cost_array);
        $final_cost_total = number_format((float)$final_cost_total, 2, '.', '');
        $final_cost_average = number_format((float)$final_cost_average, 2, '.', '');
    }
}
Log::debug("hitting end of php");
?>

    {{-- <link rel="stylesheet" href="{{URL::asset('/css/lossReport.css') }}"> --}}


<style>
    /* FirstReport Page */
    .docContainer{border:1px solid #d6d6d6;border-radius:8px;margin-top:20px;}
    .labelSection{padding:8px;display:block;font-size:28px;color:#8e8e8e;border-bottom:2px solid #f2f2f2;margin:10px 0 20px 0;}
    .docTitle{display:block;text-align:center;padding:20px;margin-top:15px;font-size:38px;color:white;background-color:#b7b7b7;border-radius:8px;}
    .docSubTitle{display:block;width:100%;text-align:center;margin-top:2px;font-size:14px;color:#5f5f5f;}

    ul {
        list-style: none;
    }

    .injuryReportTable{
        margin-top: 5pt;
        margin-bottom: 5pt;
    }

    table{
        border-collapse: collapse;
    }

    td{
        width: 8%;
        margin-left: 1%;
        margin-right: 1%;
        border-bottom: 1px solid black;
    }

    .exampleTable{
        font-weight: bold;
    }

    img{
       height:78px;
    }

    #companyInfo{
        width: 100%;
    }

    #companyInfo td{
        text-align: center;
        padding-right: 5%;
        padding-left: 5%;
    }
</style>

    <img src="<?php echo URL::Asset('/images/utility/companylogo.png') ?>">

    {{-- @if(!$download)
        <ul class="labelButton">
            <li><a href="/lossReportControls"><span class="icon prev"></span><span class="label">&nbsp;&nbsp;Go Back</span></a></li>
            <li><a href="/downloadLossReport/{{Carbon\Carbon::createFromFormat('Y-m-d', $start_date)->format('d-m-Y')}}/{{\Carbon\Carbon::createFromFormat('Y-m-d', $end_date)->format('d-m-Y')}}/{{$claim_type}}/{{$claim_status}}"><span class="icon topdf"></span><span class="label">&nbsp;&nbsp;Download as PDF</span></a></li>
        </ul>
        <!-- /downloadLossReport/{start_date}/{end_date}/{claim_type}/{claim_status} -->
    @endif --}}

<div id="pageTitle" class="docTitle">
    Loss Reports
    <span id="dateSpan" class="docSubTitle">{{$start_date}} through {{$end_date}}<br>
            Note: This is a summary of a company's claims as they appear in Zenjuries. It is NOT official loss runs from an insurance company.
    </span>
</div>
<table id="companyInfo">
    <tr>
        <td><b>Business Name: </b>{{$company->company_name}}</td>
        <td><b>Business Phone: </b>{{(empty($company->phone) ? "None Entered" : $company->phone)}}</td>
        <td><b>Policy Number: </b>{{(empty($company->policy_number) ? "None Entered" : $company->policy_number)}}</td>
    </tr>
</table>

<div class="container addNavSpace padPage">

    <div class="docContainer">
        <div class="row">
            <div class="col-md-12">
                <table class="injuryReportTable exampleTable">
                    <tr>
                        <td>
                            <ul>
                                <li>Claim Number</li>
                                <li>Employee Name</li>
                                <li>Carrier</li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li>Status</li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li>Injury Date</li>
                                <li>Date Reported</li>
                                <li>Days to Report</li>
                                <li>Date Resolved</li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li>Injury Type(s)</li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li>Injury Location(s)</li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li>Injury Severity</li>
                                <li>Injury Description</li>
                                <li>Employee Status</li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li>Final Cost</li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li>Reserve Indemnity</li>
                                <li>Reserve Medical</li>
                                <li>Reserve Legal</li>
                                <li>Reserve Misc.</li>
                                <li>Paid Indemnity</li>
                                <li>Paid Medical</li>
                                <li>Paid Legal</li>
                                <li>Paid Misc.</li>
                                <hr>
                                <li>Total</li>
                            </ul>
                        </td>
                    </tr>
                </table>

                <div class="labelSection">Injuries</div>
                @if(count($injuries))
                <table class="injuryReportTable">
                @foreach($injuries as $injury)
                    <?php
                        Log::debug("hitting label section php");
                        $injured_employee_name = \App\User::where('id', $injury->injured_employee_id)->value('name');
                        $injury_date = \Carbon\Carbon::createFromFormat('m-d-Y h:i a', $injury->injury_date);
                        $types = \App\InjuryType::where('injury_id', $injury->id)->get();
                        $locations = \App\InjuryLocation::where('injury_id', $injury->id)->get();
                        //$medical_cost = \App\MedicalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        //$indemnity_cost = \App\IndemnityCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        //$reserve_cost = \App\ReserveCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        //$legal_cost = \App\LegalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        //$misc_cost = \App\MiscellaneousCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        //$reserve_indemnity = \App\ReserveIndemnityCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        //$reserve_medical = \App\ReserveMedicalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        //$reserve_legal = \App\ReserveLegalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        //$reserve_misc = \App\ReserveMiscellaneousCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        $total = 0;
                        if(!is_null($injury->current_paid_medical)){
                            $total+= $injury->current_paid_medical;
                            $injury->current_paid_medical = "$" . number_format((float)$injury->current_paid_medical, 2, '.', '');
                        }
                        if(!is_null($injury->current_paid_indemnity)){
                            $total+= $injury->current_paid_indemnity;
                            $injury->current_paid_indemnity = "$" . number_format((float)$injury->current_paid_indemnity, 2, '.', '');
                        }
                        /*
                        if(!is_null($reserve_cost)){
                            $total+= $reserve_cost;
                            $reserve_cost = "$" . number_format((float)$reserve_cost, 2, '.', '');
                        }
                        */
                        if(!is_null($injury->current_paid_legal)){
                            $total+= $injury->current_paid_legal;
                            $injury->current_paid_legal = "$" . number_format((float)$injury->current_paid_legal, 2, '.', '');
                        }
                        if(!is_null($injury->current_paid_misc)){
                            $total+= $injury->current_paid_misc;
                            $injury->current_paid_misc = "$" . number_format((float)$injury->current_paid_misc, 2, '.', '');
                        }
                        if(!is_null($injury->current_reserve_medical)){
                            $total+= $injury->current_reserve_medical;
                            $injury->current_reserve_medical = "$" . number_format((float)$injury->current_reserve_medical, 2, '.', '');
                        }
                        if(!is_null($injury->current_reserve_indemnity)){
                            $total+= $injury->current_reserve_indemnity;
                            $injury->current_reserve_indemnity = "$" . number_format((float)$injury->current_reserve_indemnity, 2, '.', '');
                        }
                        if(!is_null($injury->current_reserve_legal)){
                            $total+= $injury->current_reserve_legal;
                            $injury->current_reserve_legal = "$" . number_format((float)$injury->current_reserve_legal, 2, '.', '');
                        }
                        if(!is_null($injury->current_reserve_misc)){
                            $total+= $injury->current_reserve_misc;
                            $injury->current_reserve_misc = "$" . number_format((float)$injury->current_reserve_misc, 2, '.', '');
                        }
                    ?>
                    <tr>
                        <td class="leftTableData">
                            <ul>
                                <li>{{(empty($injury->claim_number) ? "None Entered" : $injury->claim_number)}}</li>
                                <li>{{(empty($injured_employee_name) ? "None Entered" : $injured_employee_name)}}</li>
                                <li>{{(empty($company->carrier) ? "None Entered" : $company->carrier)}}</li>
                            </ul>
                        </td>
                        <td class="middleTableData">
                            <ul><li>
                            @if($injury->resolved)
                                Resolved
                            @else()
                                Open
                            @endif
                                </li></ul>
                        </td>
                        <td class="middleTableData">
                            <ul>
                                <li>{{$injury_date->toDateString()}}</li>
                                <li>{{$injury->created_at->toDateString()}}</li>
                                <li>{{$injury_date->diffInDays($injury->created_at)}}</li>
                                <li>{{(empty($injury->resolved_at) ? "N/A" : $injury->resolved_at)}}</li>
                            </ul>
                        </td>
                        <td class="middleTableData">
                            <ul>
                                @foreach($types as $type)
                                    <li>{{$type->type}}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td class="middleTableData">
                            <ul>
                                @foreach($locations as $location)
                                    <li>{{$location->location}}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td class="middleTableData">
                            <ul>
                                <li>{{$injury->severity}}</li>
                                <li>{{(empty($injury->notes) ? "None" : $injury->notes)}}</li>
                                <li>{{(empty($injury->employee_status) ? "None Entered" : $injury->employee_status)}}</li>
                            </ul>
                        </td>
                        <td class="middleTableData">
                            <ul>
                                <li>{{(empty($injury->final_cost) ? "N/A" : $injury->final_cost)}}</li>
                            </ul>
                        </td>
                        <td class="rightTableData">
                            <ul>
                                <li>{{(empty($injury->current_reserve_indemnity) ? "None Entered" : $injury->current_reserve_indemnity)}}</li>
                                <li>{{(empty($injury->current_reserve_medical) ? "None Entered" : $injury->current_reserve_medical)}}</li>
                                <li>{{(empty($injury->current_reserve_legal) ? "None Entered" : $injury->current_reserve_legal)}}</li>
                                <li>{{(empty($injury->current_reserve_misc) ? "None Entered" : $injury->current_reserve_misc)}}</li>
                                <li>{{(empty($injury->current_paid_indemnity) ? "None Entered" : $injury->current_paid_indemnity)}}</li>
                                <li>{{(empty($injury->current_paid_medical) ? "None Entered" : $injury->current_paid_medical)}}</li>
                                <li>{{(empty($injury->current_paid_legal) ? "None Entered" : $injury->current_paid_legal)}}</li>
                                <li>{{(empty($injury->current_paid_misc) ? "None Entered" : $injury->current_paid_misc)}}</li>
                            <hr>
                                <li><span class="claimCostTotal"><b>${{$total}}</b></span></li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </table>
                <br>

                <div id="summaryDiv">
                    <p>Note: These values are based on the claims currently being displayed. Claims that have not had their costs updated will not be counted in the Claim Averages. </p>
                    <table class="injuryReportTable exampleTable">
                        <tr>
                            <td>
                                <ul>
                                    <li><h3>Claim Stats</h3></li>
                                    <li><b>Total Claims: </b>{{count($injuries)}}</li>
                                    <li><b>Resolved Claims: </b>{{count($injuries->where('resolved', 1))}}</li>
                                    <li><b>Open Claims: </b>{{count($injuries->where('resolved', 0))}}</li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li><h3>Injury Stats</h3></li>
                                    <li><b>Most Common Type: </b>{{$most_common_type}} ({{$type_count}})</li><br>
                                    <li><b>Most Common Location: </b>{{$most_common_location}} ({{$location_count}})</li><br>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li><h3>Claim Totals</h3></li>
                                    <li><b>Final: </b>${{(empty($final_cost_array) ? "0" : $final_cost_array)}}</li>
                                    <li><b>Reserve Indemnity: </b>${{(empty($reserve_indemnity_total) ? "0" : $reserve_indemnity_total)}}</li>
                                    <li><b>Reserve Medical: </b>${{(empty($reserve_medical_total) ? "0" : $reserve_medical_total)}}</li>
                                    <li><b>Reserve Legal: </b>${{(empty($reserve_legal_total) ? "0" : $reserve_legal_total)}}</li>
                                    <li><b>Reserve Misc: </b>${{(empty($reserve_misc_total) ? "0" : $reserve_misc_total)}}</li>
                                    <li><b>Paid Indemnity: </b>${{(empty($indemnity_total) ? "0" : $indemnity_total)}}</li>
                                    <li><b>Paid Medical: </b>${{(empty($medical_total) ? "0" : $medical_total)}}</li>
                                    <li><b>Paid Legal: </b>${{(empty($legal_total) ? "0" : $legal_total)}}</li>
                                    <li><b>Paid Misc: </b>${{(empty($misc_total) ? "0" : $misc_total)}}</li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li><h3>Claim Averages</h3></li>
                                    <li><b>Final: </b>${{(empty($final_cost_array) ? "0" : $final_cost_array)}}</li>
                                    <li><b>Reserve Indemnity: </b>${{(empty($reserve_indemnity_average) ? "0" : $reserve_indemnity_average)}}</li>
                                    <li><b>Reserve Medical: </b>${{(empty($reserve_medical_average) ? "0" : $reserve_medical_average)}}</li>
                                    <li><b>Reserve Legal: </b>${{(empty($reserve_legal_average) ? "0" : $reserve_legal_average)}}</li>
                                    <li><b>Reserve Misc: </b>${{(empty($reserve_misc_average) ? "0" : $reserve_misc_average)}}</li>
                                    <li><b>Indemnity: </b>${{(empty($indemnity_average) ? "0" : $indemnity_average)}}</li>
                                    <li><b>Medical: </b>${{(empty($medical_average) ? "0" : $medical_average)}}</li>
                                    <li><b>Legal: </b>${{(empty($legal_average) ? "0" : $legal_average)}}</li>
                                    <li><b>Misc: </b>${{(empty($misc_average) ? "0" : $misc_average)}}</li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </div>
                @else
                    <h2>No claims to report. Keep up the great work!</h2>
                @endif
            </div>
        </div>
    </div>
</div>

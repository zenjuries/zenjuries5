<?php

    $company = Auth::user()->getCompany();
    $company_id = $company->id;

    $start_date = $data["start_date"];
    $end_date = $data["end_date"];
    $formattedStartDate = \Carbon\Carbon::createFromFormat('m-d-Y', $start_date)->format('m-d-Y');
    $formattedEndDate = \Carbon\Carbon::createFromFormat('m-d-Y', $end_date)->format('m-d-Y');
    
    $top10Claims = $company->getTop10Claims($formattedStartDate, $formattedEndDate);
    $topInjuryLocations = $company->getTopInjuryLocation($formattedStartDate, $formattedEndDate);
    $topInjuryDays = $company->getTopInjuryDay($formattedStartDate, $formattedEndDate);
    $injuryReportTime = $company->getInjuryReportTime($formattedStartDate, $formattedEndDate);
    $totalReserve = $company->getTotalReserve($start_date, $end_date) ?? 0;
    $totalIncurred = $company->getTotalIncurred($start_date, $end_date) ?? 0;
    $totalPaid = $company->getTotalPaid($start_date, $end_date) ?? 0;
    
    // Get Company Stats
    $companyStats = $company->getStats($start_date, $end_date);

    // Create Formatted Information for the charts
    // daily break down
    $dailyBreakDownNames = implode("','", array_map(function ($day) {
        return $day;
    }, array_keys($companyStats["daily_breakdown"])));
    $dailyBreakDownNumbers = implode(",", array_map(function ($day) {
        return $day;
    }, $companyStats["daily_breakdown"]));

    //injury location
    // dd($companyStats);
    $injuryLocationNames = implode("','", array_map(function ($day) {
        return $day;
    }, $companyStats["injury_location_breakdown"][0]));
    $injuryLocationNumbers = implode(",", array_map(function ($day) {
        return $day;
    }, $companyStats["injury_location_breakdown"][1]));

    // injury type
    $injuryTypeNames = implode("','", array_map(function ($day) {
        return $day;
    }, $companyStats["injury_type_breakdown"][0]));
    $injuryTypeNumbers = implode(",", array_map(function ($day) {
        return $day;
    }, $companyStats["injury_type_breakdown"][1]));

?>  
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title></title>
        <!-- <link rel="stylesheet" href="{{URL::asset('/css/zen5_style_default.css') }}"> -->
        <!-- <link rel="stylesheet" href="{{ URL::asset('/css/render/summary.css') }}"> -->
        <style>
            .docContainer{
                /* border:1px solid #d6d6d6;
                border-radius:8px; */
                width: 90%;
                margin: 20px auto;
            }

            .labelSection{
                padding:8px;
                display:block;
                font-size:28px;
                color:#8e8e8e;
                border-bottom:2px solid #f2f2f2;
                margin:10px 0 20px 0;
            }

            .docTitle{
                display:block;
                text-align:center;
                padding:20px;
                margin: 15px auto;
                font-size:38px;
                color:white;
                background-color:#5c9956;
                width: 80%;
                border-radius:8px;
            }

            .docSubTitle{
                display:block;
                width:100%;
                text-align:center;
                margin-top:2px;
                font-size:14px;
                color:#f2f2f2;
            }

            .reportTitle{
                display:block;
                width:100%;
                text-align:center;
                margin-top:40px;
                font-size:28px;
                color:black;
            }

            ul {
                list-style: none;
            }

            .injuryReportTable{
                width: 100%;
                margin: 20px auto 10px;
                border: 1px solid #e1e1e1;
            }

            table{
                border-collapse: collapse;
            }
            .injuryReportTable th {
                background-color: #454ca6;
                padding: 14px 0 10px;
                color: #f2f2f2;
            }
            .injuryReportTable td{
                text-align: center;
                background-color: #e1e1e1;
                padding: 10px 0 8px;
                border-bottom: 1px solid #a8a8a8;
            }

            .exampleTable{
                /* font-weight: bold; */
                font-size: 12px;
            }

            img{
            height:78px;
            }

            #companyInfo{
                width: 100%;
            }

            #companyInfo td{
                text-align: center;
                padding-right: 5%;
                padding-left: 5%;
            }

            #statisticInfo {
                width: 100%;
            }

            #statisticInfo td {
                width: 33.33%;
                padding-right: 5%;
                padding-left: 5%;
            }

            .chartTitle {
                display: block;
                width: 100%;
                font-size: 1.4rem;
                font-weight: 400;
                margin-bottom: 8px;
                color: black;
                text-align: center;
            }
            .chartImage {
                width: 225px;
                height: 144px;
            }
            .itemGrid {
                display: flex;
                align-items: stretch;
                flex-flow: row wrap;
                justify-content: space-evenly;
            }
            .chartContainer {
                padding: 20px;
                text-align: center;
            }
            </style>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>
        <script src="{{URL::asset('/js/jBox.all.min.js') }}"></script>
        <!-- jquerty input masks (date/time/phone/etc) -->
        <script src="/js/jquery.mask.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    </head>
    <body>
        <div id="pageTitle" class="docTitle">
            Summary Report
            <span id="dateSpan" class="docSubTitle">{{$start_date}} through {{$end_date}}</span>
        </div>
        <table id="companyInfo">
            <tr>
                <td><b>Business Name: </b>{{$company->company_name}}</td>
                <td><b>Business Phone: </b>{{(empty($company->phone) ? "None Entered" : $company->phone)}}</td>
                <td><b>Policy Number: </b>{{(empty($company->policy_number) ? "None Entered" : $company->policy_number)}}</td>
            </tr>
        </table>
        <div class="addNavSpace padPage">

        <div class="">
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <h1 class="reportTitle">Overall Statistics</h1>
                    <br>
                    <table id="statisticInfo">
                        <tr>
                            <!-- <td><b>risk level:</b> 0</td>                                                                   -->
                            <td><b>total paid:</b> ${{$totalPaid}}</td>
                            <td><b>total reserved:</b> ${{$totalReserve}}</td>
                            <td><b>total incurred:</b> ${{$totalIncurred}}</td>
                           </tr>
                    </table>
                    <br>
                    <table id="statisticInfo">
                        <tr> 
                            <td><b>total claims:</b> {{$company->getTotalClaims($start_date, $end_date) ?? 0}}</td>
                            <td><b>average claim duration:</b> {{$company->averageClaimDuration($start_date, $end_date) ?? 0}}</td>
                            <td><b>claims reported same day:</b> {{$injuryReportTime["amount"] ?? "empty"}} {{(!is_null($injuryReportTime["percent"])) ? sprintf("(%s%% of claims)", number_format((float)$injuryReportTime["percent"], 2, '.', '')) : ""}}</td>
                            <!-- <td><b>litigation:</b></td> -->
                        </tr>
                    </table>
                    <table id="statisticInfo">
                        <tr>
                            <td><b>top injury location:</b> {{$topInjuryLocations["location"] ?? "empty"}} {{(!is_null($topInjuryLocations["count"])) ? sprintf("(%s injuries)", $topInjuryLocations["count"]) : ""}}</td>
                            <td><b>top injury week day(s):</b> {{(!is_null($topInjuryDays["days"])) ? implode(", ", $topInjuryDays["days"]) : ""}} {{(!is_null($topInjuryDays["max"])) ? sprintf("(%s injuries)", $topInjuryDays["max"]) : ""}}</td>
                        </tr>
                    </table>
                    <br><br><br><br>
                    <h1 class="reportTitle hasInjuries">Charts</h1>
                    <section class="sectionPanel transparent hasInjuries" style="text-align:center;margin:auto;width:100%;">
                        <div class="sectionContent">
                            <table>
                                <tr>
                                    <th>Injury Location</th>
                                    <th>Injury Type</th>
                                    <th>Injury Day</th>
                                </tr>
                                <tr>
                                    <td><img class="chartImage" src="https://quickchart.io/chart?bkg=white&c={type:%27pie%27,data:{labels:['{{$injuryLocationNames}}'],datasets:[{data:[{{$injuryLocationNumbers}}],backgroundColor: ['rgba(255, 99, 132, 0.2)','rgba(54, 162, 235, 0.2)','rgba(255, 206, 86, 0.2)','rgba(75, 192, 192, 0.2)','rgba(153, 102, 255, 0.2)','rgba(255, 159, 64, 0.2)','rgba(230, 230, 250, 0.2)'],borderColor: ['rgba(255, 99, 132, 1)','rgba(54, 162, 235, 1)','rgba(255, 206, 86, 1)','rgba(75, 192, 192, 1)','rgba(153, 102, 255, 1)','rgba(255, 159, 64, 1)','rgba(230, 230, 250, 1)'],borderWidth: 1}]}}" /></td>
                                    <td><img class="chartImage" src="https://quickchart.io/chart?bkg=white&c={type:%27pie%27,data:{labels:['{{$injuryTypeNames}}'],datasets:[{data:[{{$injuryTypeNumbers}}],backgroundColor: ['rgba(255, 99, 132, 0.2)','rgba(54, 162, 235, 0.2)','rgba(255, 206, 86, 0.2)','rgba(75, 192, 192, 0.2)','rgba(153, 102, 255, 0.2)','rgba(255, 159, 64, 0.2)','rgba(230, 230, 250, 0.2)'],borderColor: ['rgba(255, 99, 132, 1)','rgba(54, 162, 235, 1)','rgba(255, 206, 86, 1)','rgba(75, 192, 192, 1)','rgba(153, 102, 255, 1)','rgba(255, 159, 64, 1)','rgba(230, 230, 250, 1)'],borderWidth: 1}]}}" /></td>
                                    <td><img class="chartImage" src="https://quickchart.io/chart?bkg=white&c={type:%27pie%27,data:{labels:['{{$dailyBreakDownNames}}'],datasets:[{data:[{{$dailyBreakDownNumbers}}],backgroundColor: ['rgba(255, 99, 132, 0.2)','rgba(54, 162, 235, 0.2)','rgba(255, 206, 86, 0.2)','rgba(75, 192, 192, 0.2)','rgba(153, 102, 255, 0.2)','rgba(255, 159, 64, 0.2)','rgba(230, 230, 250, 0.2)'],borderColor: ['rgba(255, 99, 132, 1)','rgba(54, 162, 235, 1)','rgba(255, 206, 86, 1)','rgba(75, 192, 192, 1)','rgba(153, 102, 255, 1)','rgba(255, 159, 64, 1)','rgba(230, 230, 250, 1)'],borderWidth: 1}]}}" /></td>
                                </tr>
                            </table>                                           
                        </div>
                    </section>
                    <div style="height: 400px;"/>
                    <h1 class="reportTitle">Top 10 Claims</h1>
                    <table class="injuryReportTable exampleTable">
                        <tr>
                            <th>Claim Number</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Date Of Accident</th>
                            <th>Status</th>
                            <th>Incurred</th>
                            <th>Paid</th>
                            <th>Reserves</th>
                        </tr>
                        @foreach($top10Claims as $injury)
                        <tr>
                            <td>{{$injury["claim_number"]}}</td>
                            <td>{{$injury["first_name"]}}</td>
                            <td>{{$injury["last_name"]}}</td>
                            <td>{{$injury["injured_date"]}}</td>
                            <td>{{($injury["status"] !== 0) ? "Open" : "Closed"}}</td>
                            <td>${{$injury["incurred"]}}</td>
                            <td>${{$injury["paid"]}}</td>
                            <td>${{$injury["reserves"]}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

        
    </body>
</html>
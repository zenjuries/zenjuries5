<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" href="{{ URL::asset('/css/render/firstReport.css') }}">
</head>
<body>
    
    <div class="container addNavSpace padPage">
        <?php
        $injury = \App\Injury::where('id', $data['injury_id'])->first();
        $company = \App\Company::where('id', $injury->company_id)->first();
        $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
        $injuryTypes = \App\InjuryType::where('injury_id', $injury->id)->get();
        $injuryLocations = \App\InjuryLocation::where('injury_id', $injury->id)->get();
        $team = \App\Team::where('id', $company->team_id)->first();
        $owner_id = \App\TeamUser::where('team_id', $team->id)->where('role', 'owner')->value('user_id');
	$owner = \App\User::where('id', $owner_id)->first();
	dd($owner_id);
        $secondary_policies = \App\SecondaryPolicyInfo::where('company_id', $injury->company_id)->get();
        if(!is_null($secondary_policies)){
	        $policy_string = "";
	        foreach($secondary_policies as $policy){
		        $policy_string = $policy_string . " " . $policy->policy_number . ",";
	        }
	        $policy_string = rtrim($policy_string, ",");
        }
        ?>
        <div class="docContainer">
            <div class="row">
                <div class="col-md-12">
                    <span class="docTitle">Injury Alert<span class="docSubTitle"> for <?php echo $injuredEmployee->name; ?>, <?php echo $injury->injury_date; ?></span></span>



                    <div class="labelSection">Company Details</div>

                <div class="sectionInfo" id="firstReportCompany">
                        Company Name: <?php echo $company->company_name; ?>
                    </div>
                    <br>
                    <div class="sectionInfo" id="firstReportCompanyPhone">
                        Company Phone: <?php if(!empty($company->company_phone)){
                            echo $company->company_phone;
                        }else{
                            echo "None Entered";
                        } ?>
                    </div>
                    <br>
                    <div class="sectionInfo" id="firstReportCompanyAddress">
                        Company Address: <?php
                            if(!empty($company->mailing_address) && !empty($company->mailing_city) && !empty($company->mailing_state)){
                        $address = $company->mailing_address . " " . $company->mailing_address_line_2 . ",<br>" .
                                $company->mailing_city . ", " . $company->mailing_state . " " . $company->mailing_zip;
                            echo $address;
                            }else{
                                echo "None Entered";
                            }
                        ?>
                    </div>
                    <br>
                    <div class="sectionInfo" id="firstReportPolicyNumber">
	                    @if(count($secondary_policies) > 0)
	                    	Policy Numbers: {{$company->policy_number}},{{$policy_string}}
	                    	
	                    	                    	
	                    @else
	                    	Policy Number: 
	                    	@if(!empty($company->policy_number))
	                    		{{$company->policy_number}}
	                    	@else
	                    		None Entered
	                    	@endif
	                    @endif
	                   
                        
     
                    </div>
                    <br>
                    <div class="sectionInfo" id="firstReportContactName">
                        Contact Name: <?php echo $owner->name; ?>
                    </div>
                    <br>
                    <div class="sectionInfo" id="firstReportContactEmail">
                        Contact Email: <?php echo $owner->email; ?>
                    </div>
                    <br>
                    <div class="sectionInfo" id="firstReportContactPhone">
                        Contact Phone: <?php if(!empty($owner->phone)){
                            echo $owner->phone;
                        }else{
                            echo "None Entered";
                        } ?>
                    </div>
                    <br>

                    <div class="labelSection">Injured Person Details</div>

                    <div class="sectionInfo" id="firstReportName">
                        Name: <?php echo $injuredEmployee->name; ?>
                    </div>
                    <br>
                    <div class="sectionInfo" id="firstReportGender">
                        Gender: <?php echo $injury->injured_employee_gender; ?>
                    </div>
                        <br>
                    <div class="sectionInfo" id="firstReportEmail">
                        Email: <?php if(!empty($injuredEmployee->email)){
                            echo $injuredEmployee->email;
                        }else{
                            echo "None Entered";
                        } ?>
                    </div>
                        <br>
                    <div class="sectionInfo" id="firstReportPhone">
                        Phone: <?php if(!empty($injuredEmployee->phone)){
                            echo $injuredEmployee->phone;
                        }else{
                            echo "None Entered";
                        } ?>
                    </div>

                    <div class="labelSection">Injury Details</div>

                    <div class="sectionInfo" id="firstReportDate">
                        Injury Date: <?php echo $injury->injury_date; ?>
                    </div>
                        <br>
                    <div class="sectionInfo" id="firstReportLocation">
                        Injury Location(s):
                        <?php
                            $location_count = count($injuryLocations);
                            $location_loop_counter = 1;
                            foreach($injuryLocations as $injuryLocation){
                                if($location_loop_counter !== $location_count){
                                    echo $injuryLocation->location . ', ';
                                }else{
                                    echo $injuryLocation->location;
                                }
                                $location_loop_counter++;
                            }
                        ?>
                    </div>
                        <br>
                    <div class="sectionInfo" id="firstReportTypes">
                        Injury Type(s):
                        <?php
                        $type_count = count($injuryTypes);
                        $type_loop_counter = 1;
                        foreach($injuryTypes as $injuryType){
                            if($type_loop_counter !== $type_count){
                                echo $injuryType->type . ', ';
                            }else{
                                echo $injuryType->type;
                            }
                            $type_loop_counter++;

                        }
                        ?>
                    </div>
                        <br>
                    <div class="sectionInfo" id="firstReportSeverity">
                        Injury Severity: <?php echo $injury->severity; ?>
                    </div>
                        <br>
                    <div class="sectionInfo" id="firstReportNotes">
                        Description of Injury: <?php echo $injury->notes; ?>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

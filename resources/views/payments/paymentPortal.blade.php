<?php


$payment = \App\Payment::where('id', $payment_id)->first();
//var_dump($payment);


?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>



<script>

    //One-off charges (adding new client or client renewal) to be called when user is an existing stripe customer
    function chargeExistingCustomer(){
        var payment_id = '<?php echo $payment->id; ?>';
        $.ajax({
            type: 'POST',
            url: '<?php echo route('chargeExistingCustomer'); ?>',
            data: {
                _token : '<?php echo csrf_token(); ?>',
                payment_id: payment_id
            },
            success: function(data){
                alert('success');
            },
            error: function(data){
                alert('error');
                console.log(data);
            }


        });
    }

    //sets up a new user in Stripe (should be called first before adding payment method/charges)
    function createNewStripeCustomer(company_id){
        $.ajax({
           type: 'POST',
           url: '<?php echo route('createNewStripeCustomer'); ?>',
           data: {
               _token: '<?php echo csrf_token(); ?>',
               company_id: company_id
           },
           success: function(data){
               alert('success');
           },
           error: function(data){
               alert('error');
           }
        });
    }

    function createNewSubscription(company_id){
        $.ajax({
            type: 'POST',
            url: '<?php echo route('createNewSubscription'); ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                company_id: company_id
            },
            success: function(data){
                alert('success!');
            },
            error: function(data){
                console.log(data);
                alert('error');
            }
        });
    }

    createNewSubscription('43');
    //createNewStripeCustomer(138);
    //chargeExistingCustomer();
</script>

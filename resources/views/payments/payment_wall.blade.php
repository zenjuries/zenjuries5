@extends('layouts.zen5_layout_guest')
@section('maincontent')
<?php

    // vvvvvvvvvvvvvvv
    
    // this is a confirmation/status page, we need to make this look a lot better

    // ^^^^^^^^^^^



    $user = Auth::user();
    $company_id = Auth::user()->getCompany()->id;
    
    //arrays for error popup [icon image, error description, error code, button color, button ID, button icon, button label]
    $error1 = array("uhoh", "something went wrong", "zen233", "red", "dismiss", "exclamation-circle", "dismiss");
    $errormessage = $error1;
    //arrays for wait spinner overlay [spinner color, center message, top message, lower message]
    $waitspinner1 = array("green", "one<br>moment", " ", "registering...");
    $spinnercontent = $waitspinner1; 
    
    $brandCompany = env('COMPANY_NAME', "Zenjuries");
    $brandApp = env('BRAND_APP_NAME', "Zenjuries");
    $requirePayment = env('REQUIRE_PAYMENTS', true); /* true, false */        
?>

<script src="https://js.stripe.com/v3/"></script>

<div class="pageContent bgimage-bgheader pagebackground16 fade-in">		
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                    <span><b>{{ $brandCompany }}</b> registration</span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="/images/icons/edit.png"></span><span class="subContent">registration for <b>{{ $brandApp }}</b></span>
                    </div>
            </div>
            <div class="pageIcon animate__animated animate__bounceInRight">
                <img src="/images/icons/userinfo.png">
            </div> 
        </div>           
    </div>

    <div style="height:10px;"></div>
    <div class="contentBlock noPad">
        <div class="container noPad">

            <section class="sectionPanel">
                <div class="zen-actionPanel noHover transparent tight">                
                    <div class="sectionContent">
                        <section class="formBlock dark" style="padding-top: 50px;">
                            <div class="center" id="message" style="padding:20px 0;">Every field is required.  Fill out all information, enter your credit card information, and click the submit registration button below.</div>
                                                           
                            <br>

                            <div class="buttonArray">
                                <button class="cyan centered" onclick="location.href='/zagent';">Continue to Zenjuries</button>
                            </div>
                        </section>    
                    </div>
                </div>
            </section>



        </div>
    </div>
</div>


    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

<script>
    $('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>
<script>
    $('.showZenModalLocked').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true,
            closeButton: false,
            closeOnClick: false
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>

<script>

        const stripe = Stripe('<?php echo config('stripe.key'); ?>');
        // Retrieve the "setup_intent_client_secret" query parameter appended to
        // your return_url by Stripe.js
        const clientSecret = new URLSearchParams(window.location.search).get(
        'setup_intent_client_secret'
        );

        // Retrieve the SetupIntent
        stripe.retrieveSetupIntent(clientSecret).then(({setupIntent}) => {
        const message = document.querySelector('#message')

        // Inspect the SetupIntent `status` to indicate the status of the payment
        // to your customer.
        //
        // Some payment methods will [immediately succeed or fail][0] upon
        // confirmation, while others will first enter a `processing` state.
        //
        // [0]: https://stripe.com/docs/payments/payment-methods#payment-notification
        switch (setupIntent.status) {
            case 'succeeded': {
                message.innerText = 'Success! Your payment method has been saved.';
                break;
            }

            case 'processing': {
                message.innerText = "Processing payment details. We'll update you when processing is complete.";
                break;
            }

            case 'requires_payment_method': {
                message.innerText = 'Failed to process payment details. Please try another payment method.';

                // Redirect your user back to your payment page to attempt collecting
                // payment again

                break;
            }


            case 'requires_action': {
                message.innerText = 'This payment method requires additional steps before we\'re about to verify. Please check your email!';

                // Redirect your user back to your payment page to attempt collecting
                // payment again

                break;
            }
            default: {
                message.innerText = setupIntent.status;
                break;
            }
        }
        });

</script>




@endsection
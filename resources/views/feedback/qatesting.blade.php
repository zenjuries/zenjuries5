@extends('layouts.zen5_layout')
@section('maincontent')
<?php
	$user = Auth::user();
	$company = $user->getCompany();

	//get the user theme
	$theme = $user->zengarden_theme;
	$theme = str_replace("theme", "", $theme);
	$theme = strtolower($theme);
	$arrayPassedIn = str_split($user->shortcuts);

	//arrays for message popup [message icon, message title, message body, button color, button ID, button icon, button label]
	$message1 = array("hmm-color", "message title", "message body", "red", "dismiss", "search", "dismiss");
	$popmessage = $message1;
?>

<div class="pageContent bgimage-bgheader bghaze-midnight fade-in">		
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                    <span><b>QA</b> testing</span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="/images/icons/edit.png"></span><span class="subContent">quality assurance feedback</span>
                    </div> 
            </div>
            <div class="pageIcon animate__animated animate__bounceInRight">
                <img src="/images/icons/zenpro2.png">
            </div> 
        </div>           
    </div>

    <div style="height:10px;"></div>
    <div class="contentBlock noPad">
        <div class="container noPad">

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-bug"></div> issue or suggestion</span>
                <div class="zen-actionPanel noHover transparent tight">                
                    <div class="sectionContent">
                    As the most frequent users of zenjuries, you will be most familiar with any issues that you come across, or ways we could do things better.  Use the form below to send any bug/issues you find, or send along suggestions that will improve the system.
                        <br><br>
                        <section class="formBlock dark">
                            <div class="formGrid"> 
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="agency_name">reported by:</label>
                                    <div class="inputIcon check"><input id="user_name" type="text" value="{{ $user->name }}"/></div>
                                    <span class="inputError">required field</span>
                                </div> 
                                <div class="formInput">
                                <label for="state">type of issue</label>
                                    <div class="inputIcon check">
                                        <select id="issue_type" name="state" style="width:212px;">
                                        <option value="---">--select--</option>
                                        <option value="visual">visual or UI</option>
                                        <option value="text">text issue</option>
                                        <option value="formatting">broken formatting</option>
                                        <option value="better_format">formatting suggestion</option>
                                        <option value="data">wrong data</option>
                                        <option value="functionality">broken functionality</option>
                                        <option value="wrong_code">wrong functionality</option>
                                        <option value="better_idea">function suggestion</option>
                                        <option value="minor_error">minor error</option>
                                        <option value="critical_error">critical error</option>
                                        <option value="security">security issue</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="agency_name">priority</label>
                                    <div class="inputIcon alert">
                                        <select id="priority" name="state" style="width:212px;">
                                        <option value="suggestion" style="color:grey;" selected>suggestion</option>
                                        <option value="low" style="color:cyan;">issue-low</option>
                                        <option value="standard" style="color:yellow;">issue-standard</option>
                                        <option value="high" style="color:orange;">issue-high</option>
                                        <option value="critical" style="color:red;">issue-critical</option>
                                        </select>
                                    </div>
                                </div>                               
                            </div>
                            <div class="formGrid"> 
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="agency_name">subject:</label>
                                    <div class="inputIcon mail"><input id="issue_subject" type="text"/></div>
                                </div> 
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="agency_name">associated client?</label>
                                    <div class="inputIcon company"><input id="client_name" type="text"/></div>
                                </div> 
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="agency_name">frequency</label>
                                    <div class="inputIcon bug">
                                        <select id="error_frequency" name="state" style="width:212px;">
                                        <option value="na" selected>n/a</option>
                                        <option value="repeatable">repeatable</option>
                                        <option value="nonrepeatable">non-repeatable</option>
                                        <option value="random">seems random</option>
                                        </select>
                                    </div>
                                </div>                               
                            </div>                                
                        </section> 
                        
                        <style>
                            textareatest {
                                display: block;
                                width: 100% !important;
                                -webkit-box-sizing: border-box;
                                -moz-box-sizing: border-box;
                                        box-sizing: border-box;
                            
                                max-width:100% !important;
                            }
                        </style>


                        <div class="center" style="padding:20px 0;">Cras elementum turpis vitae sapien porta, in finibus purus commodo. Curabitur gravida, nibh at condimentum porta, ipsum quam varius dui, ac tristique felis nisi in ante. Praesent ultricies, tellus quis mollis varius, lorem odio vehicula lectus, sit amet placerat turpis purus non quam. Quisque laoreet tempus neque quis dictum. Fusce rutrum, lectus sed tempor dignissim, eros risus tempus tortor, ac vestibulum metus turpis id purus.</div>

                        <section class="formBlock dark" style="border:1px solid green;">
                            <div class="formGrid" style="border:1px solid blue;"> 
                                <div class="formInput wide" style="border:1px solid red;">
                                    <!-- input -->
                                    <label for="agency_name">steps taken to repeat</label>
                                    <div class="inputIcon check"><textarea id="error_description" class="textareatest"></textarea></div>
                                    <span class="inputError">required field</span>
                                </div>                               
                            </div>   
                        </section> 


                    </div>
                </div>

                
                    <span class="sectionTitle"><div class="icon icon-television"></div> include screenshots <span style="opacity:.5">(if helpful)</span></span>
                    <div class="sectionContent">
                        <div class="sectionInfo hideFull" style="display:none">Use the <b>take photo</b> below to take a single image to add to your injury.  If you would like to add multiple photos, use the <b>upload image</b> button.</div>
                        <div class="sectionInfo">If screenshots will help to describe and identify the bug in the app, include them here. Include some descriptions with the screenshots if it isn't clear what you are trying to show.</div>
                        <div class="center hideFull" style="display:none">
                            <div class="formGrid">
                                <div class="formInput">
                                    <label>click to take a picture.</label>
                                    <input type="file" accept="image/*" capture="camera" multiple class="camera-upload">
                                </div>
                            </div>
                        </div>

                        <section class="formBlock dark">
                            <div class="formElement">
                                <button class="cyan showFileModal" data-modalcontent="#uploadImageModal">upload a screenshot</button>
                            </div>

                            <div class="formGrid photos">
                                <canvas id="photoCanvas"></canvas>
                                <div id="photoTarget">
                                </div>
                            </div>
                        </section>
                    </div>
                    <hr>
                    <div class="buttonArray">
                        <button class="red centered" id="reportBug"><div class="icon icon-bug"></div> report error</button>
                    </div> 
                    <br><br>
                </section>

                 
        </div>
    </div>
</div>

<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals 
	@include('partials.modals.tos')  -->  
</div> 

<script>
    $('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>


<script>
    $('#reportBug').on('click', function(){
        //var file = document.getElementById('acPhotoUpload').files[0];
        var taskName = "Zenjuries zenGuide crashed when opening";
        var email = "bobtaylor123@example.com"; 
        var name = document.getElementById('user_name').value;
        var body = document.getElementById('error_description').value;
        var label = document.getElementById('priority').value;
        var acLabel;
        if(label == "low"){
            acLabel = "PRIORITY-4";
        }
        else if(label == "standard"){
            acLabel = "PRIORITY-3";
        }
        else if(label == "high"){
            acLabel = "PRIORITY-2";
        }
        else if(label == "emergency"){
            acLabel = "PRIORITY-1";
        }
        var formData = new FormData();
            formData.append('taskName', taskName);
            formData.append('label', acLabel);
            formData.append('name', name);
            formData.append('email', email);
            formData.append('body', body);
            formData.append('_token', '<?php echo csrf_token(); ?>');
        $.ajax({
            type: 'POST',
            url: '<?php echo route("postNewTask") ?>',
            contentType: false,
            processData: false,
            data: formData,
            success: function (data) {
                console.log('success');
            },  error: function(data){
                console.log('failure');
                console.log(data);
            }
        })
    })
</script>


@endsection
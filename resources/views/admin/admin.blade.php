@extends('layouts.zen5_layout')
@section('maincontent')
<?php
	$user = Auth::user();
	$company = $user->getCompany();

	//get the user theme
	$theme = $user->zengarden_theme;
	$theme = str_replace("theme", "", $theme);
	$theme = strtolower($theme);
	$arrayPassedIn = str_split($user->shortcuts);

	//arrays for message popup [message icon, message title, message body, button color, button ID, button icon, button label]
	$message1 = array("hmm-color", "message title", "message body", "red", "dismiss", "search", "dismiss");
	$popmessage = $message1;
?>

<link rel="stylesheet" href="/css/animcharts.css?<$date format='yyyy-MM-dd_HH:mm:ss'/$>">
<script src="/js/animcharts.js"></script>

<div class="pageContent bgimage-bgheader pagebackground9 fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>zenjuries <i>admin</i></span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/gears.png"></span><span class="subContent">admin management</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/zenjuries.png">
            </div> 
        </div>           
	</div>


	<div class="container">
		
		<div class="homeButtons">
			<div class="itemGrid animate__animated animate__bounceIn">			
				<div class="itemGrid__nav noSelect"><a class="navbbg-1 noLink" href="/newinjury"><span class="navImg"><img src="/images/icons/users.png"></span><span class="subText">view all</span><span class="mainText">users</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-3 noLink" href="/admin_all_policyholders"><span class="navImg"><img src="/images/icons/company.png"></span><span class="subText">view all</span><span class="mainText" style="font-size:1rem;">policyholders</span></a></div>
                <div class="itemGrid__nav noSelect"><a class="navbbg-5 noLink" href="/help"><span class="navImg"><img src="/images/icons/injuryguy.png"></span><span class="subText">view all</span><span class="mainText">injuries</span></a></div>	
				<div class="itemGrid__nav noSelect"><a class="navbbg-4 noLink" href="/stats"><span class="navImg"><img src="/images/icons/chart2.png"></span><span class="subText">view all</span><span class="mainText">reports</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-6 noLink" href="/zendocs"><span class="navImg"><img src="/images/icons/alert.png"></span><span class="subText">view all</span><span class="mainText">alerts</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-2 noLink" href="/weeklysummary"><span class="navImg"><img src="/images/icons/zenpro2.png"></span><span class="subText">my</span><span class="mainText">zenPro</span></a></div>												
			</div>

			<div class="itemGrid animate__animated animate__bounceIn">			
				<div class="itemGrid__nav noSelect"><a class="navbbg-1 noLink" href="/newinjury"><span class="navImg"><img src="/images/icons/message.png"></span><span class="subText">send system</span><span class="mainText">message</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-2 noLink" href="/weeklysummary"><span class="navImg"><img src="/images/icons/gear.png"></span><span class="subText">system</span><span class="mainText">manager</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-2 noLink" href="/weeklysummary"><span class="navImg"><img src="/images/icons/zenprobadge.png"></span><span class="subText">my</span><span class="mainText">zenPro</span></a></div>		
				<div class="itemGrid__nav noSelect"><a class="navbbg-4 noLink" href="/stats"><span class="navImg"><img src="/images/icons/stats.png"></span><span class="subText">zenMetrics</span><span class="mainText">reports</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-5 noLink" href="/help"><span class="navImg"><img src="/images/icons/help.png"></span><span class="subText">help &</span><span class="mainText">support</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-6 noLink" href="/zendocs"><span class="navImg"><img src="/images/icons/documents.png"></span><span class="subText">view your</span><span class="mainText">documents</span></a></div>											
			</div>            
		</div>

		<div class="flexPanel">
				<div class="viewControl">
					<div class="compactViewLeft animate__animated animate__bounceInLeft">
						<span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/stats.png"> stats 1</span>
					</div>
					<div class="compactViewExpand"><span>[ view caseload ]</span></div>
					<div class="compactViewRight animate__animated animate__bounceInRight">
						<ul>
							<li><span class="icon"><img src="images/icons/injury-ok.png"></span><span id="caseload__ok" class="value FG__okblue">0</span></li>
							<li><span class="icon"><img src="images/icons/injury-alert.png"></span><span id="caseload__alert" class="value FG__alert">0</span></li>
							<li><span class="icon"><img src="images/icons/injury-critical.png"></span><span id="caseload__critical" class="value FG__critical">0</span></li>
						</ul>
						<div class="compactViewX">[ minimize ]</div>
					</div>
				</div>
				<div class="viewContent">
					<div class="zen-actionPanel transparent tight">
						<div class="sectionContent dynamicSize">
							<table class="caseloadTable zenListTable tight">
								<tbody id="caseloadInjuryDropdown">
								<tr id="myCaseListHeading" class="tableHeadings">
										<td></td>
										<td class="name"><button class="caseloadSorter sorter showIconModal">name</button></td>
										<td class="injurydate"><button class="caseloadSorter sorter showIconModal">date</button></td>
										<td class="severity"><button class="caseloadSorter sorter showIconModal">severity</button></td>
										<td class="agency"><button class="caseloadSorter sorter showIconModal">agency</button></td>
										<td class="policyholder"><button class="caseloadSorter sorter showIconModal">policyholder</button></td>
										<td class="mood"><button class="caseloadSorter sorter showIconModal">mood</button></td>
									</tr>
									<!-- replace with .partial content
									<tr>
										<td><div class="progressMeter small inline status7 critical"><svg viewBox="0 0 100 100"><circle class="pm" r="50%" cx="50%" cy="50%"></circle><div class="overlay"><div class="avatar"></div></div></svg></div></td>
										<td class="name"><span>Henry Smith</span></td>
										<td class="date"><span>12/23/2020</span></td>
										<td class="severity"><span>mild</span></td>
										<td class="agency"><span>an agency</span></td>
										<td class="policyholder"><span>Some company</span></td>
										<td><div class="moodVisual smaller"><div class="currentMood"></div></div></td>
									</tr>
									<tr class="tablerowspace"><td></td></tr>-->
									
								</tbody>
							</table>
						</div>					
					</div>						
				</div>
			</div>


	</div>

</div>
	
@include('js.zenboard_js')

<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
    @include('partials.modals.modalSample')         
	@include('partials.modals.editPolicy')
	@include('partials.modals.newWelcome')
	@include('partials.modals.newPolicy')
	@include('partials.modals.popmessage')
</div>
@if (Auth::user()->type === "agent" || Auth::user()->is_zenpro)
	@include('partials.myWatchList_dropdown')
@endif
@if(Auth::user()->is_zenpro)
	@include('partials.myCaseLoad')
@endif
@include('partials.unresolved_dropdown')
<!--
<script>
    $(document).ready(function(){
        $(".showZencareModal").modal('show');
    });
</script>
-->
<script>
    $('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zeneModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>


@endsection

@extends('layouts.zen5_layout')
@section('maincontent')
<?php
	$user = Auth::user();
	$company = $user->getCompany();

	//get the user theme
	$theme = $user->zengarden_theme;
	$theme = str_replace("theme", "", $theme);
	$theme = strtolower($theme);
	$arrayPassedIn = str_split($user->shortcuts);

	//arrays for message popup [message icon, message title, message body, button color, button ID, button icon, button label]
	$message1 = array("hmm-color", "message title", "message body", "red", "dismiss", "search", "dismiss");
	$popmessage = $message1;
?>

<link rel="stylesheet" href="/css/animcharts.css?<$date format='yyyy-MM-dd_HH:mm:ss'/$>">
<script src="/js/animcharts.js"></script>

<div class="pageContent bgimage-bgheader pagebackground10 fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>zenjuries <i>*superadmin</i></span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/gears.png"></span><span class="subContent">*superadmin management</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/zenjuries.png">
            </div> 
        </div>           
	</div>


	<div class="container">
		
		<div class="homeButtons">
			<div class="itemGrid animate__animated animate__bounceIn">			
				<div class="itemGrid__nav noSelect"><a class="navbbg-1 noLink" href="/admin_all_users"><span class="navImg"><img src="/images/icons/users.png"></span><span class="subText">view all</span><span class="mainText">users</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-2 noLink" href="/admin_all_agencies"><span class="navImg"><img src="/images/icons/insurance.png"></span><span class="subText">view all</span><span class="mainText">agencies</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-3 noLink" href="/admin_all_policyholders"><span class="navImg"><img src="/images/icons/company.png"></span><span class="subText">view all</span><span class="mainText" style="font-size:1rem;">policyholders</span></a></div>	
                <div class="itemGrid__nav noSelect"><a class="navbbg-5 noLink" href="/admin_all_injuries"><span class="navImg"><img src="/images/icons/injuryguy.png"></span><span class="subText">view all</span><span class="mainText">injuries</span></a></div>	
				<div class="itemGrid__nav noSelect"><a class="navbbg-4 noLink" href="/admin_all_reports"><span class="navImg"><img src="/images/icons/chart2.png"></span><span class="subText">zenjuries</span><span class="mainText">reports</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-6 noLink" href="/admin_all_alerts"><span class="navImg"><img src="/images/icons/alert.png"></span><span class="subText">view all</span><span class="mainText">alerts</span></a></div>											
			</div>

			<div class="itemGrid animate__animated animate__bounceIn">			
				<div class="itemGrid__nav noSelect"><a class="navbbg-1 noLink" href="/admin_sendmail"><span class="navImg"><img src="/images/icons/email.png"></span><span class="subText">send mass</span><span class="mainText">email</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-1 noLink" href="/admin_sendmessage"><span class="navImg"><img src="/images/icons/message.png"></span><span class="subText">send mass</span><span class="mainText">message</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-1 noLink" href="/admin_loginmessage"><span class="navImg"><img src="/images/icons/zenjuries.png"></span><span class="subText">login</span><span class="mainText">message</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-2 noLink" href="/admin_system_manager"><span class="navImg"><img src="/images/icons/gear.png"></span><span class="subText">system</span><span class="mainText">manager</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-2 noLink" href="/admin_zenpro_manager"><span class="navImg"><img src="/images/icons/zenpro.png"></span><span class="subText">zenpro</span><span class="mainText">manager</span></a></div>		
				<div class="itemGrid__nav noSelect"><a class="navbbg-4 noLink" href="/admin_litigation_alerts"><span class="navImg"><img src="/images/icons/litigation.png"></span><span class="subText">litigation</span><span class="mainText">alerts</span></a></div>											
			</div> 
			
			<div class="itemGrid animate__animated animate__bounceIn">			
				<div class="itemGrid__nav noSelect"><a class="navbbg-1 noLink" href="/admin_"><span class="navImg"><img src="/images/icons/speedo-dark.png"></span><span class="subText">version</span><span class="mainText">updates</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-2 noLink" href="/admin_bugreport"><span class="navImg"><img src="/images/icons/bug.png"></span><span class="subText">bugreport</span><span class="mainText">issues</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-2 noLink" href="/admin_"><span class="navImg"><img src="/images/icons/question.png"></span><span class="subText">zenpro</span><span class="mainText">manager</span></a></div>		
				<div class="itemGrid__nav noSelect"><a class="navbbg-4 noLink" href="/admin_"><span class="navImg"><img src="/images/icons/question.png"></span><span class="subText">zenMetrics</span><span class="mainText">reports</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-5 noLink" href="/admin_"><span class="navImg"><img src="/images/icons/zengarden.png"></span><span class="subText">zengarden</span><span class="mainText">activity</span></a></div>
				<div class="itemGrid__nav noSelect"><a class="navbbg-6 noLink" href="/admin_"><span class="navImg"><img src="/images/icons/documents.png"></span><span class="subText">document</span><span class="mainText">manager</span></a></div>											
			</div> 			
		</div>



		<div style="height:160px;">
			<div class="buttonCloud devButtons">
			<button class="showZenModal" data-modalcontent="#sampleModal">SAMPLE</button>
			<button class="showZenModal" data-modalcontent="#showInputPolicy">edit policy number</button>
			<button class="showZenModal" data-modalcontent="#showNewPolicy">policy upload blocker</button>
			<button class="showZenModal" data-modalcontent="#welcomeModal">welcome</button>
			<button class="showZenModal" data-modalcontent="#popmessageModal">popup message</button>
			</div>
		</div>

	</div>

</div>
	
@include('js.zenboard_js')

<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
    @include('partials.modals.modalSample')         
	@include('partials.modals.editPolicy')
	@include('partials.modals.newWelcome')
	@include('partials.modals.newPolicy')
	@include('partials.modals.popmessage')
</div>
@if (Auth::user()->type === "agent" || Auth::user()->is_zenpro)
	@include('partials.myWatchList_dropdown')
@endif
@if(Auth::user()->is_zenpro)
	@include('partials.myCaseLoad')
@endif
@include('partials.unresolved_dropdown')
<!--
<script>
    $(document).ready(function(){
        $(".showZencareModal").modal('show');
    });
</script>
-->
<script>
    $('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zeneModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>


@endsection

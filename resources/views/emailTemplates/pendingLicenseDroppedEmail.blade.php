@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "grey";
	$headerImage = "subalert";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Subscription Cancelled";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $team_id = \App\Company::where('id', $communication->company_id)->value('team_id');
    $team = \App\Team::where('id', $team_id)->first();
    $owner_id = \App\TeamUser::where('team_id', $team_id)->where('role', 'owner')->first();
    $owner_name = \App\User::where('id', $owner_id)->first();
    $billingLink = url('settings/teams/' . $team_id . '#/subscription');
    $company_string = "";
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    if($user->id === $owner_id->user_id){
        $owner_string = "Since you're listed as the owner of this company, you may go to billing and purchase a subscription.
        You may also change the company owner from the company page.";
        $linkText = "<a href='" . $billingLink . "'>Click here to go to Billing</a>";
    }else{
        $owner_string = "Your company owner can do this from the billing page, located within their company page.";
        $linkText = "";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> The " . $brandCompany . " subscription gifted to you by your Agent has been cancelled!
    Your company has been placed under a grace period lasting until " . $team->grace_period_ends . ".
    Once this grace period has ended, your company will need to purchase a new subscription in order to continue using " . $brandCompany . ".
     " . $owner_string .
            " If you feel that your company's subscription was cancelled by mistake, please contact the agency that gifted you your subscription.
     Please note that " . $brandCompany . " has no control over a Z-Agent cancelling your account.";
     $linkText;
    ?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.commonTextEmailPartial')
@endsection
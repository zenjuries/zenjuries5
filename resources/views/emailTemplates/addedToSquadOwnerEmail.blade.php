@extends('layouts.emailLayout')
@section('emailcontent')
<?php
$brandCompany = env('COMPANY_NAME');
$headerBG = "purple";
$headerImage = "addteam";
$headerTitle = "Added to a Team";
$footerFlair = "medic";
$bodyFlair = "4admin";	
$user = \App\User::where('id', $queuedMessage->user_id)->first();
$communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
$company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
$headlineText = "Hey " . strtok($user->name, " ");
$zenboardLink = url('/communicationLink/zenboard/' . $communication->company_id);
$company_string = "";
if($user->type !== "internal"){
        $company_string = $company_name . ": ";
}
$bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> You've been added to one of <span style='color:red;font-weight:bold;'>" . $company_name . "'s</span> teams as a <b>Chief Executive</b>.
As a $brandCompany team member, your actions will be crucial in ensuring that your company's injured employees have the most pleasant experience possible.
As a <b>Chief Executive</b>, you'll be in charge of everything within the Zenjuries app, especially managing your team.
<span style='display:block;font-style:italic;padding-top:10px;'>Your duties will include:</span>
<ol style='margin-top:10px;'>
<li style='padding-bottom:4px;'>Letting the employee know you care</li>
<li style='padding-bottom:4px;'>Keeping the entire team accountable</li>
<li style='padding-bottom:4px;'>Remembering that if this isn't a big deal to you, then it isn't to them either</li>
<li style='padding-bottom:4px;'>Directly encouraging your injured employee at least once a month by phone, email, or text</li>
</ol>
If you follow these steps, Zenjuries will make handling your workers comp claims as painless as possible!<br><br>";
$linkText = "<a href='" . $zenboardLink . "'>Click here to go to your dashboard!</a>";
?>


        @include('emailPartials.headline1EmailPartial')
        @include('emailPartials.zenfuciusCommentEmailPartial')

@endsection

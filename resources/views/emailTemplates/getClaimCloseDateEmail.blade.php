@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
    $headerBG = "blue";
    $headerImage = "taskreminder";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Claim Close Date Request";
    $bodyFlair = "";
    $footerFlair = "alert";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = $company_string . "Please add the Claim Close Date for  " . $injuredEmployee->name . "'s injury in " . $brandApp . ".
    The Claim Close Date is the official date of closure set by your insurance carrier.";

    $linkText =  "Click <a href='" . $treeUrl . "'>here</a> to go to the injury's Tree of Communications page to add the date!";
    ?>
    @include('emailPartials.headline2EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection
@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "grey";
	$headerImage = "zenupdate";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Client Joined";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $client_id = \App\Communication::where('id', $queuedMessage->communication_id)->value('company_id');
    $client = \App\Company::where('id', $client_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $url = url('/agent/companyList');

    $agency_name = \App\Agency::where('id', $client->agency_id)->value('name');
    if($agency_name === NULL || $agency_name === ""){
        $agency_name = "Your Agency";
    }

    $bodyFlair = "";
    $bodyText = $agency_name . "'s client <span style='color:red;font-weight:bold;'>" . $client->company_name . "</span> has joined " . $brandCompany . "!
    You now have access to their company, and can join their teams."; 
    $linkText = "<a href='" . $url . "'>You can access their company from your Company List</a>";
    ?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.commonTextEmailPartial')
@endsection
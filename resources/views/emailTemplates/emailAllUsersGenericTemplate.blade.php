@extends('layouts.emailLayout')
@section('emailcontent')

<?php 
	$headerBG = "yellowgreen";
	$headerImage = "attention";
	$brandCompany = env('COMPANY_NAME');
	$brandApp = env('BRAND_APP_NAME');
	$headerTitle = "Attention";
	$footerFlair = "alert";
	$bodyFlair = "";
	$linkText = "";
	$bodyText = "We are pleased to announce " . $brandApp . "!
	 This latest edition adds new communication features, more injury stats, GPS GEO locating for directions to closest urgent care centers and even easier claim management all from the palm of your hand!
	   It’s never been easier to report, manage and experience claims on the go!
	   <br><br><br>
	    Download our latest version now on Google Play/IOS and start experiencing workers’ comp of the future!
?>
    @endsection
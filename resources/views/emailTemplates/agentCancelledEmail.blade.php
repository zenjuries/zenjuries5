@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
    $brandCompany = env('COMPANY_NAME');
	$headerBG = "black";
	$headerImage = "appointmentdeleted";
    $headerTitle = "Deleted Appointment";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $team_id = \App\Company::where('id', $communication->company_id)->value('team_id');
    $team = \App\Team::where('id', $team_id)->first();
    $owner_id = \App\TeamUser::where('team_id', $team_id)->where('role', 'owner')->first();
    $owner_name = \App\User::where('id', $owner_id)->first();
    $billingLink = url('settings/teams/' . $team_id . '#/subscription');
    if($user->id === $owner_id->user_id){
        $owner_string = "Since you're listed as the owner of this company, you may go to billing and purchase a subscription.
        You may also change the company owner from the company page.";
        $linkText = "<a href='" . $billingLink . "'>Click here to go to Billing</a>";
    }else{
        $owner_string = "Your company owner can do this from the billing page, located within their company page.";
        $linkText = "";
    }
    if($team->on_grace_period == 1){
        $bodyString = "Since they were providing your subscription, Your company has been put under a 30 day grace period.
        Once this grace period has ended, your company will need to purchase a subscription in order to continue using Zenjuries. " .
        $owner_string . " Until the grace period ends, your company will continue to have full access to Zenjuries.";
    }else{
        $bodyString = "Your company will still receive a 15% discount on future payments, but you will no longer see that Agency's colors and logo on the site.";
    }
    $company_string = "";
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair - "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> Your insurance agent is no longer a Z-Agent. " . $bodyString;
    $linkText = "";
    ?>


    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.commonTextEmailPartial')
@endsection
# emailTemplates
-  Active Marking Email:
    file name: activeMarketingEmail.blade.php
    description: Marketing email
    date edited: 1/2/2019
    version: 2.2
-  Added to Squad Adjuster Email:
    file name: addedToSquadAdjusterEmail.blade.php
    description: Email when recipient is added to the adjuster spot on the team 
    date edited: 1/2/2019
    version: 2.2
-  Added to Squad Agent Email:
    file name: addedToSquadAgentEmail.blade.php
    description: Email when recipient is added to the agent spot on the team 
    date edited: 1/2/2019
    version: 2.2
-  Added to Squad Customer Service Email:
    file name: addedToSquadAdjusterEmail.blade.php
    description: Email when recipient is added to the customer service spot on the team 
    date edited: 1/2/2019
    version: 2.2
-  Added to Squad Doctor Email:
    file name: addedToSquadAdjusterEmail.blade.php
    description: Email when recipient is added to the doctor spot on the team 
    date edited: 1/2/2019
    version: 2.2
-  Added to Squad Generic Email:
    file name: addedToSquadAdjusterEmail.blade.php
    description: Email when recipient is added to a generic spot on the team 
    date edited: 1/2/2019
    version: 2.2
-  Added to Squad Human Resources Email:
    file name: addedToSquadAdjusterEmail.blade.php
    description: Email when recipient is added to the human resources spot on the team 
    date edited: 1/2/2019
    version: 2.2
-  Added to Squad OtherEmail:
    file name: addedToSquadOtherEmail.blade.php
    description: Email when recipient is added to the "other" spot on the team 
    date edited: 1/2/2019
    version: 2.2
-  Added to Squad Physical Therapist Email:
    file name: addedToSquadPhysicalTherapistEmail.blade.php
    description: Email when recipient is added to the physical therapist spot on the team 
    date edited: 1/2/2019
    version: 2.2
-  Added to Squad Safety Coordinator Email:
    file name: addedToSquadSafetyCoordinatorEmail.blade.php
    description: Email when recipient is added to the safety coordinator spot on the team 
    date edited: 1/2/2019
    version: 2.2
-  Added to Squad Supervisor Email:
    file name: addedToSquadSupervisorEmail.blade.php
    description: Email when recipient is added to the supervisor spot on the team 
    date edited: 1/2/2019
    version: 2.2
-  Added to System Email:
    file name: addedToSquadAdjusterEmail.blade.php
    description: Email when recipient is added to the adjuster spot on the team 
    date edited: 1/2/2019
    version: 2.2
-  Adjuster Info Added Email:
    file name: adjusterInfoAddedEmail.blade.php
    description: Email for when adjuster's info is added to a claim
    date edited: 1/2/2019
    version: 2.2
-  Agent Cancelled Email:
    file name: agentCancelledEmail.blade.php
    description: Email for when subscription is running out
    date edited: 1/2/2019
    version: 2.2
-  Appointment Followup Email:
    file name: appointmentFollowupEmail.blade.php
    description: Email for after appointment
    date edited: 1/2/2019
    version: 2.2
-  Appointment Reminder Email:
    file name: appointmentReminderEmail.blade.php
    description: Email for reminder about upcoming appointment
    date edited: 1/2/2019
    version: 2.2
-  Auto Deliver First Report Email:
    file name: autoDeliverFirstReportEmail.blade.php
    description: Email for auto delivery of first report
    date edited: 1/2/2019
    version: 2.2
-  Claim Number Email:
    file name: claimNumberEmail.blade.php
    description: Email for when the claim number is entered
    date edited: 1/2/2019
    version: 2.2
-  Litigation Email:
    file name: claimPausedEmail.blade.php
    description: Email for when claim goes under litigation
    date edited: 1/2/2019
    version: 2.2
-  Claim Resolved Email:
    file name: claimResolvedEmail.blade.php
    description: Email for when the claim is resolved
    date edited: 1/2/2019
    version: 2.2
-  Claim Values Email:
    file name: claimValuesEmail.blade.php
    description: Email for confirming claim costs are up to date
    date edited: 1/2/2019
    version: 2.2
-  Claim Values Updated Email:
    file name: claimValuesUpdatedEmail.blade.php
    description: Email for showing the updated claim costs
    date edited: 1/2/2019
    version: 2.2
-  Class Code Email:
    file name: classCodeEmail.blade.php
    description: Email for getting the class code
    date edited: 1/2/2019
    version: 2.2
-  Custom Task Reminder Email:
    file name: customTaskReminderEmail.blade.php
    description: Email for reminder about custom class
    date edited: 1/2/2019
    version: 2.2
-  Deleted Appointment Email:
    file name: deletedAppointmentEmail.blade.php
    description: Email for appointment that has been deleted
    date edited: 1/2/2019
    version: 2.2
-  Dropped By Agent Email:
    file name: droppedByAgentEmail.blade.php
    description: Email for companies that were dropped by their agent
    date edited: 1/2/2019
    version: 2.2
-  Educational Material Email:
    file name: educationalMaterialEmail.blade.php
    description: Email containing educational material
    date edited: 1/2/2019
    version: 2.2
-  Generic Email Template:
    file name: emailAllUsersGenericTemplate.blade.php
    description: Email for alerting users about zenjuries updates
    date edited: 1/2/2019
    version: 2.2
-  Employee Status Updated Email:
    file name: employeeStatusEmail.blade.php
    description: Email for when the employee's status has changed
    date edited: 1/2/2019
    version: 2.2
-  Final Cost Updated Email:
    file name: claimValuesEmail.blade.php
    description: Email for showing the final claim costs
    date edited: 1/2/2019
    version: 2.2
-  First Report Email:
    file name: firstReportEmail.blade.php
    description: Email telling the user to make sure they deliver the the first report
    date edited: 1/2/2019
    version: 2.2
-  First Report First Aid Email:
    file name: firstReportFirstAidEmail.blade.php
    description: Email telling the user to make sure they deliver the the first report for first aid injuries
    date edited: 1/2/2019
    version: 2.2
-  First Report Reminder Email:
    file name: firstReportEmail.blade.php
    description: Email reminding the user to make sure they deliver the the first report
    date edited: 1/2/2019
    version: 2.2
-  First Report Reminder Owner Email:
    file name: firstReportOwnerEmail.blade.php
    description: Email reminding the injury owner that they need to submit the first report immediately  
    date edited: 1/2/2019
    version: 2.2
-  Get Adjuster Info Email:
    file name: getAdjusterInfoEmail.blade.php
    description: Email Asking for adjusters info
    date edited: 1/2/2019
    version: 2.2
-  Get Claim Close Date Email:
    file name: getClaimCloseDatesEmail.blade.php
    description: Email Asking for the claim close date
    date edited: 1/2/2019
    version: 2.2
-  Get Claim Number Email:
    file name: getClaimNumberEmail.blade.php
    description: Email Asking for the claim number
    date edited: 1/2/2019
    version: 2.2
-  Get Final Cost Email:
    file name: getFinalCostEmail.blade.php
    description: Email Asking for the final claim cost
    date edited: 1/2/2019
    version: 2.2
-  Get First Appointment Details Email:
    file name: getFirstAppointmentDetailsEmail.blade.php
    description: Email Asking for details after the first appointment
    date edited: 1/2/2019
    version: 2.2
-  Get Renewal Date Email:
    file name: getRenewalDateEmail.blade.php
    description: Email Asking for policy number, renewal date, and carrier info
    date edited: 1/2/2019
    version: 2.2
-  Grace Period Ended Email:
    file name: gracePeriodEndedEmail.blade.php
    description: Email for letting them know the grace period had eneded
    date edited: 1/2/2019
    version: 2.2
-  Grace Period Ending Email:
    file name: gracePeriodEndingEmail.blade.php
    description: Email for letting them know the grace period will end tomorrow
    date edited: 1/2/2019
    version: 2.2
-  Grace Period Week Email:
    file name: gracePeriodWeekEmail.blade.php
    description: Email for letting them know the grace period will end tomorrow
    date edited: 1/2/2019
    version: 2.2
-  Indemnity Cost Update Email:
    file name: Indemnity Cost Update Email.blade.php
    description: Email for letting them know the indemnity cost has been updated
    date edited: 1/2/2019
    version: 2.2
-  Injured Employee Appointment Email:
    file name: injuredEmployeeAppointmentEmail.blade.php
    description: Email for letting the injured user know they have an upcoming appointment
    date edited: 1/2/2019
    version: 2.2
-  Injured Employee Contact Email:
    file name: injuredEmployeeContactEmail.blade.php
    description: Email asking them team if they have had contact with the injured employee
    date edited: 1/2/2019
    version: 2.2
-  Injured Employee Diary Email:
    file name: injuredEmployeeDiaryEmail.blade.php
    description: Email asking the injured employee to write in the diary
    date edited: 1/2/2019
    version: 2.2
-  Injured Employee Expectations Email:
    file name: gracePeriodEndingEmail.blade.php
    description: Email for letting the injured employee helpful things about how to handle the claim
    date edited: 1/2/2019
    version: 2.2
-  Medical Cost Updated Email:
    file name: medicalCostUpdateEmail.blade.php
    description: Email for letting the team know the medical cost has been updated
    date edited: 1/2/2019
    version: 2.2
-  Message Received Email:
    file name: messageReceivedEmail.blade.php
    description: Email for letting them know they got a message
    date edited: 1/2/2019
    version: 2.2
-  Negative Diary Feedback Email:
    file name: negativeDiaryFeedbackEmail.blade.php
    description: Email for letting the team know they had a negative diary feedback
    date edited: 1/2/2019
    version: 2.2
-  New Appointment Email:
    file name: newAppointmentEmail.blade.php
    description: Email for letting the team and the injured employee know a new appointment has been made
    date edited: 1/2/2019
    version: 2.2
-  New Client ZAgent User Email:
    file name: newClientZAgentUserEmail.blade.php
    description: Email for letting the zagent know their client has been added to zenjuries
    date edited: 1/2/2019
    version: 2.2
-  New Custom Task Email:
    file name: newCustomTaskEmail.blade.php
    description: Email for letting the user know there has been a custom task created for them
    date edited: 1/2/2019
    version: 2.2
-  New First Aid Injury Human Resources Email:
    file name: newFirstAidInjuryHumanResourcesEmail.blade.php
    description: Email for letting the person on the team in the sport of human resources there is a new first aid injury
    date edited: 1/2/2019
    version: 2.2
-  New First Aid Injury Other Email:
    file name: newFirstAidInjuryOtherEmail.blade.php
    description: Email for letting the person on the team in the sport of other there is a new first aid injury
    date edited: 1/2/2019
    version: 2.2
-  New First Aid Injury Owner Email:
    file name: newFirstAidInjuryOtherEmail.blade.php
    description: Email for letting the person on the team in the sport of chief executive there is a new first aid injury
    date edited: 1/2/2019
    version: 2.2  
-  New First Aid Injury Safety Coordinator Email:
    file name: newFirstAidInjurySafetyCoordinatorEmail.blade.php
    description: Email for letting the person on the team in the sport of safety coordinator there is a new first aid injury
    date edited: 1/2/2019
    version: 2.2
-  New First Aid Injury Supervisor Email:
    file name: newFirstAidInjurySupervisorEmail.blade.php
    description: Email for letting the person on the team in the sport of supervisor there is a new first aid injury
    date edited: 1/2/2019
    version: 2.2 
-  New Injury Adjuster Email:
    file name: newInjuryAdjusterEmail.blade.php
    description: Email for letting the person on the team in the sport of adjuster there is a new injury
    date edited: 1/2/2019
    version: 2.2
-  New Injury Agent Email:
    file name: newInjuryAgentEmail.blade.php
    description: Email for letting the person on the team in the sport of agent there is a new injury
    date edited: 1/2/2019
    version: 2.2
-  New Injury Customer Service Email:
    file name: newInjuryCustomerServiceEmail.blade.php
    description: Email for letting the person on the team in the sport of customer service there is a new injury
    date edited: 1/2/2019
    version: 2.2
-  New Injury Doctor Email:
    file name: newInjuryDoctorEmail.blade.php
    description: Email for letting the person on the team in the sport of doctor there is a new injury
    date edited: 1/2/2019
    version: 2.2
-  New Injury Generic Email:
    file name: newInjuryGenericEmail.blade.php
    description: Email for letting the person on the team there is a new injury
    date edited: 1/2/2019
    version: 2.2
-  New Injury Human Resources Email:
    file name: newInjuryHumanResourcesEmail.blade.php
    description: Email for letting the person on the team in the sport of human resources there is a new injury
    date edited: 1/2/2019
    version: 2.2
-  New Injury Other Email:
    file name: newInjuryOtherEmail.blade.php
    description: Email for letting the person on the team in the sport of other there is a new injury
    date edited: 1/2/2019
    version: 2.2
-  New Injury Owner Email:
    file name: newInjuryOwnerEmail.blade.php
    description: Email for letting the person on the team in the sport of chief executive there is a new injury
    date edited: 1/2/2019
    version: 2.2
-  New Injury Physical Therapist Email:
    file name: newInjuryPhysicalTherapistEmail.blade.php
    description: Email for letting the person on the team in the sport of physical therapist there is a new injury
    date edited: 1/2/2019
    version: 2.2
-  New Injury Safety Coordinator Email:
    file name: newInjurySafetyCoordinatorEmail.blade.php
    description: Email for letting the person on the team in the sport of safety coordinator there is a new injury
    date edited: 1/2/2019
    version: 2.2
-  New Injury Supervisor Email:
    file name: newInjurySupervisorEmail.blade.php
    description: Email for letting the person on the team in the sport of supervisor there is a new injury
    date edited: 1/2/2019
    version: 2.2
-  New PreExisting Claim Email:
    file name: newPreExistingClaimEmail.blade.php
    description: Email for letting the team know there is a new pre-existing claim
    date edited: 1/2/2019
    version: 2.2
-  New Tree Post Email:
    file name: newTreePostEmail.blade.php
    description: Email for letting the team know there is a new tree post
    date edited: 1/2/2019
    version: 2.2
-  Official First Report Email:
    file name: officialFirstReportEmail.blade.php
    description: Email reminding the team to send the official first report
    date edited: 1/2/2019
    version: 2.2
-  Pending License Cancelled Email:
    file name: pendingLicenseCancelledEmail.blade.php
    description: Email for letting the company know their license was cancelled
    date edited: 1/2/2019
    version: 2.2
-  Pending License Dropped Email:
    file name: pendingLicenseDroppedEmail.blade.php
    description: Email for letting the company know their license was dropped
    date edited: 1/2/2019
    version: 2.2
-  Ping Agent Email:
    file name: pingAgentEmail.blade.php
    description: Email for letting the user know they have been nudged
    date edited: 1/2/2019
    version: 2.2
-  Possible Workers Comp Injury Email:
    file name: possibleWorkersCompInjuryEmail.blade.php
    description: Email for letting the team letting them know an injury might be a workers comp claim
    date edited: 1/2/2019
    version: 2.2
-  Quiz Email:
    file name: quizEmail.blade.php
    description: might not be used
    date edited: 1/2/2019
    version: 2.2
-  Replacement ZAgent ZUser Email:
    file name: replacementZAgentZUserEmail.blade.php
    description: Email for letting the person know they are now the agent on a team
    date edited: 1/2/2019
    version: 2.2
-  Reserve Cost Updated Email:
    file name: reserveCostUpdatedEmail.blade.php
    description: Email for letting the team know the reserve cost has been updated
    date edited: 1/2/2019
    version: 2.2
-  Safety Coordinator Early Injury Email:
    file name: safetyCoordinatorEarlyInjuryEmail.blade.php
    description: Email for letting the safety coordinator to take a closer look at an early reported injury
    date edited: 1/2/2019
    version: 2.2
-  Severity Updated Email:
    file name: severityUpdatedEmail.blade.php
    description: Email for letting the team know that servility has been updated
    date edited: 1/2/2019
    version: 2.2
-  Stats Report Email:
    file name: statsReportEmail.blade.php
    description: Might not be in use
    date edited: 1/2/2019
    version: 2.2
-  Subscription Cancelled Email:
    file name: SubscriptionCancelledEmail.blade.php
    description: Email for letting the company know their zenjuries subscription has been cancelled
    date edited: 1/2/2019
    version: 2.2
-  Tree Post SnapShot Email:
    file name: treePostSnapshotEmail.blade.php
    description: Email for giving the team an overview of the past week for the injury
    date edited: 1/2/2019
    version: 2.2
-  Update Carrier Email:
    file name: updateCarrierEmail.blade.php
    description: Email for letting the company know their policy renewal date is here and to make sure their carrier info is up to date
    date edited: 1/2/2019
    version: 2.2
-  Update Employee Status Email:
    file name: updateEmployeeStatusEmail.blade.php
    description: Email for letting the team know the employee status has changed
    date edited: 1/2/2019
    version: 2.2
-  Update Team Email:
    file name: updateTeamEmail.blade.php
    description: Email for asking the company if their teams are up to date
    date edited: 1/2/2019
    version: 2.2
-  Verify Account Email:
    file name: verifyAccountEmail.blade.php
    description: Email for verifying accounts
    date edited: 1/2/2019
    version: 2.2
-  Weekly Update Email:
    file name: weeklyUpdateEmail.blade.php
    description: Email for giving the company a weekly update about the number of claims
    date edited: 1/2/2019
    version: 2.2
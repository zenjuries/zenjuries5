@extends('layouts.emailLayout')
@section('emailcontent')
<?php
    $brandCompany = env('COMPANY_NAME');
	$headerBG = "purple";
	$headerImage = "addteam";
    $headerTitle = "Added to a Team";
	$bodyFlair = "4claimmanager";	
	$footerFlair = "medic";
$user = \App\User::where('id', $queuedMessage->user_id)->first();
        $headlineText = "Hey " . strtok($user->name, " ");
$communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
$company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
$company_string = "";
$url = url('/communicationLink/zenboard/' . $communication->company_id);
if($user->type !== "internal"){
    $company_string = $company_name . ": ";
}

$bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> You've been added to one of <span style='color:red;font-weight:bold;'>" .
        $company_name . "'s</span> teams as a <b>Claim Manager</b>.
<span style='display:block;font-style:italic;padding-top:10px;'>Your duties will include:</span>
<ol style='margin-top:10px;'>
<li style='padding-bottom:4px;'>Observing the communication tree constantly to \"take the temperature\" of the injury progression (or lack there of)</li>
<li style='padding-bottom:4px;'>Speaking with your team's <b>Chief Navigator</b> and openly communicate the latest injury updates as they occur!</li>
</ol>
If you follow these steps, Zenjuries will make handling your workers comp claims easy and efficient!<br>";
$linkText = "<a href='$url'>Click here to go to $brandCompany now.</a>";
?>

@include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')

@endsection
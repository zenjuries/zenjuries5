@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
    $brandCompany = env('COMPANY_NAME');
	$headerBG = "green";
	$headerImage = "costupdate";
    $headerTitle = "Cost Update";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    if($user->type !== "internal"){
        $injured_company_name = \App\Company::where('id', $injury->company_id)->value('company_name');
        $injured_company_name = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $injured_company_name . ": </span>";
    }else{
        $injured_company_name = "";
    }
    $injured_user_name = \App\User::where('id', $injury->injured_employee_id)->value('name');
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $bodyFlair = "";
    /* PAID */
    $current_medical = \App\MedicalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
    $current_indemnity = \App\IndemnityCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
    //$current_reserve = \App\ReserveCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
    $current_legal = \App\LegalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
    $current_misc = \App\MiscellaneousCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');

    /* RESERVE */
    $reserve_medical = \App\ReserveMedicalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
    $reserve_indemnity = \App\ReserveIndemnityCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
    //$current_reserve = \App\ReserveCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
    $reserve_legal = \App\ReserveLegalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
    $reserve_misc = \App\ReserveMiscellaneousCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');

    $paid_total = 0;
    $paid_total+= (is_null($current_medical) ? 0 : $current_medical);
    //$paid_total+= (is_null($current_reserve) ? 0 : $current_reserve);
    $paid_total+= (is_null($current_indemnity) ? 0 : $current_indemnity);
    $paid_total+= (is_null($current_legal) ? 0 : $current_legal);
    $paid_total+= (is_null($current_misc) ? 0 : $current_misc);

    $reserve_total = 0;
    $reserve_total+= (is_null($reserve_medical) ? 0 : $reserve_medical);
    $reserve_total+= (is_null($reserve_indemnity) ? 0 : $reserve_indemnity);
    $reserve_total+= (is_null($reserve_legal) ? 0 : $reserve_legal);
    $reserve_total+= (is_null($reserve_misc) ? 0 : $reserve_misc);

    //$bodyText = $injuredEmployee->name . "'s status has been updated to " . $injury->employee_status . ". Click <a href='" . $treeUrl . "'>here</a> to review the injury in Zenjuries.";
      $bodyText =  $injured_company_name . "The Claim Costs for <span style='color:red;font-weight:bold;'>" . $injured_user_name . "'s</span> claim have been updated! Here are the current values for their claim:<br><br>" .
              //paid costs
              "<h3> Paid Costs</h3>" .
              "<b>Medical:</b> " . (is_null($current_medical) ? "None Entered Yet" : "<span style='color: green'>$" . $current_medical . "</span>") . "<br>" .
              "<b>Indemnity:</b> " . (is_null($current_indemnity) ? "None Entered Yet" : "<span style='color: green'>$" . $current_indemnity . "</span>") . "<br>" .
              "<b>Legal:</b> " . (is_null($current_legal) ? "None Entered Yet" : "<span style='color: green'>$" . $current_legal . "</span>") . "<br>" .
              "<b>Miscellaneous:</b> " . (is_null($current_misc) ? "None Entered Yet" : "<span style='color: green'>$" . $current_misc . "</span>") . "<br><br>" .
              "<span style='color: red'><b>Paid Total: </b>$" . $paid_total . "</span>" .
              //reserve costs
              "<h3> Reserve Costs</h3>" .
              "<b>Medical:</b> " . (is_null($reserve_medical) ? "None Entered Yet" : "<span style='color: green'>$" . $reserve_medical . "</span>") . "<br>" .
              "<b>Indemnity:</b> " . (is_null($reserve_indemnity) ? "None Entered Yet" : "<span style='color: green'>$" . $reserve_indemnity . "</span>") . "<br>" .
              "<b>Legal:</b> " . (is_null($reserve_legal) ? "None Entered Yet" : "<span style='color: green'>$" . $reserve_legal . "</span>") . "<br>" .
              "<b>Miscellaneous:</b> " . (is_null($reserve_misc) ? "None Entered Yet" : "<span style='color: green'>$" . $reserve_misc . "</span>") . "<br><br>" .
              "<span style='color: red'><b>Reserve Total: </b>$" . $reserve_total . "</span>";
              //zenjuries link
              $linkText = "<a href='$treeUrl'>Check $brandCompany for more info.</a>";
    ?>
    



    @include('emailPartials.headline2EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection
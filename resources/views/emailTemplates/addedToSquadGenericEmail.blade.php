@extends('layouts.emailLayout')
@section('emailcontent')

<?php
$brandCompany = env('COMPANY_NAME');
$headerBG = "red";
$headerImage = "addteam";
$headerTitle = "Added to a Team";
$footerFlair = "medic";
$user = \App\User::where('id', $queuedMessage->user_id)->first();
$headlineText = "Hey " . strtok($user->name, " ");
$communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
$company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
$zenboardLink = url('/communicationLink/zenboard/' . $communication->company_id);
$company_string = "";
$bodyFlair = "";
if($user->type !== "internal"){
    $company_string = $company_name . ": ";
}
$bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> You've been added to one of <span style='color:red;font-weight:bold;'>" . $company_name . "'s</span> teams as a <b>Squad Member!</b>
As a Zenjuries team member, your actions will be crucial in ensuring that the company's injured employees have the most pleasant experience possible.<br><br>";
$linkText = "<a href='" . $zenboardLink . "'>Click here to go to your dashboard!</a>";
?>

@include('emailPartials.headline1EmailPartial')
@include('emailPartials.zenfuciusCommentEmailPartial')

@endsection
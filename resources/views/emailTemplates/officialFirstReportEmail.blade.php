@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "orange";
	$headerImage = "firstreportreminder";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "First Report Reminder";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $firstReport = \App\FirstReportOfInjury::where('injury_id', $injury->id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $headlineText = "Hey " . strtok($user->name, " ");
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> This is a reminder that the Official First Report of Injury for <span style='color:red;font-weight:bold;'> " . $injuredEmployee->name . "'s</span> injury needs to be filed with the insurance carrier within two weeks of the injury.
                                           Please fill out the remainder of the form and deliver it to your insurance professional.
                                            Once you have, please confirm it has been delivered <a href='" . $treeUrl . "'>in the tree of communication.</a>
                                            You may also download another copy of this form from that page, as well as blank version if needed.";
    $linkText = "";
    ?>

    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection
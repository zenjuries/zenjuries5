@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
    $brandCompany = env('COMPANY_NAME');
	$headerBG = "purple";
	$headerImage = "addteam";
    $headerTitle = "Added to a Team";
    $bodyFlair = "4navigator";	
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $zenboardLink = url('/communicationLink/zenboard/' . $communication->company_id);
    $company_string = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> You've been added to one of <span style='color:red;font-weight:bold;'>" . $company_name . "'s</span> teams as a <b>Chief Navigator</b>.
As a $brandCompany team member, your actions will be crucial in ensuring that the company's injured employees have the most pleasant experience possible.
As the <b>Chief Navigator</b>, you will be managing most of the internal communication between team members and the injured employee.
You are absolutely crucial to ensure that claims are handled as quickly and efficiently as possible!
<span style='display:block;font-style:italic;padding-top:10px;'>Your duties will include:</span>
<ol style='margin-top:10px;'>
<li style='padding-bottom:4px;'>Ensuring that the Injury Alert is delivered to your insurance carrier immediately after reporting the injury to Zenjuries</li>
<il style='padding-bottom:4px;'>Complete and deliver the Official First Report of Injury to your insurance carrier within 2 weeks of the injury</li>
<li style='padding-bottom:4px;'>Entering the \"Claim Number\" into Zenjuries and following up with insurance carrier if the Claim Number is not received within an hour of filing the Injury Alert.</li>
<li style='padding-bottom:4px;'>Entering as much \"claim information\" as you can into zenjuries</li>
<li style='padding-bottom:4px;'>Setting up positive expectations with injured Employee (deliver \"Employee Expectations\" document)</li>
<li style='padding-bottom:4px;'>Adding appropriate/needed/desired \"team members\" to the zenjuries profile/team</li>
<li style='padding-bottom:4px;'>Asking for help when needed from other team members... that's why they're on.</li>
<li style='padding-bottom:4px;'>Communicating with Claim Manager once a week for latest update and report to zenjuries on tree of communication</li>
</ol>
<br>
If you follow these steps, Zenjuries will make handling your workers comp claims as painless as possible!<br><br>";
$linkText = "<a href='" . $zenboardLink . "'>Click here to go to your dashboard!</a>";
    ?>

    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')

@endsection

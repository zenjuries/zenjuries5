@extends('layouts.emailLayout')
@section('emailcontent')

    <?php
	$headerBG = "blue";
	$headerImage = "welcome";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Welcome";
	$footerFlair = "medic";
    $headlineText = "Welcome to " . $brandCompany . "!";
    //$url = route('verifyAccountNewPolicyHolder', ['token' => $user->password_token, 'id' => $user->id]);
    $url = env("APP_URL") . "verifyAccountNewPolicyHolder/" . $user->password_token . "/" . $user->id . "/" . $company->id;
    $bodyFlair = "";
    $bodyText = "To get started, please follow the link below to verify your account and create a password.
    After that, you'll be ready to start using " . $brandApp . "!<br><a href='$url'>Get Started</a>";
    $linkText = "";
    ?>
    @include('emailPartials.alertTextEmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection
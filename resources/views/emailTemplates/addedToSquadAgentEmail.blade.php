@extends('layouts.emailLayout')
@section('emailcontent')
<?php
$brandCompany = env('COMPANY_NAME');
$headerBG = "purple";
$headerImage = "addteam";
$headerTitle = "Added to a Team";
$bodyFlair = "4agent";
$footerFlair = "medic";
$user = \App\User::where('id', $queuedMessage->user_id)->first();
$headlineText = "Hey " . strtok($user->name, " ");
$communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
$company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
$company_string = "";
$url = url('/communicationLink/zenboard/' . $communication->company_id);
if($user->type !== "internal"){
        $company_string = $company_name . ": ";
}
        $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> You've been added to one of <span style='color:red;font-weight:bold;'>" . $company_name . "'s</span> " .
        "teams as a <b>Work Comp Agent.</b><br>
<span style='display:block;font-style:italic;padding-top:10px;'>Your duties will include:</span>
<ol style='margin-top:10px;'>
<li style='padding-bottom:4px;'>Communicating with your contacts at the business and setting expectations on importance of prompt and ongoing communication.</li>
<li style='padding-bottom:4px;'>Following up with the insurance company and expediting the assignment of the <b>Claim Manager</b></li>
<li style='padding-bottom:4px;'>Leading and encouraging all communicators on the zenjuries team.</li>
<li style='padding-bottom:4px;'>Proving your value to the <b>Chief Executive</b></li>
</ol>
<br><br>
If you follow these steps, $brandCompany will make handling your workers comp claims as painless as possible!<br>";

$linkText = "<a href='$url'>Click here to go to $brandCompany now.</a>";
?>

@include('emailPartials.headline1EmailPartial')
@include('emailPartials.zenfuciusCommentEmailPartial')

@endsection
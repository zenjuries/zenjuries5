@extends('layouts.emailLayout')
@section('emailcontent')

<?php
	$headerBG = "red";
	$headerImage = "newinjury";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Role Assignment";
	$bodyFlair = "4agent";	
	$footerFlair = "medic";	    
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string ."</span><span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "</span> has been hurt. You've been hand-picked to serve as the <b>Work Comp Agent</b> on this injury.
<span style='display:block;font-style:italic;padding-top:10px;'>Your duties will include:</span>

<ol style='margin-top:10px;'>
<li style='padding-bottom:4px;'>Communicating with the <b>Chief Executive</b> (your contact) at the business and setting expectations on importance of prompt and ongoing communication</li>
<li style='padding-bottom:4px;'>Following up with the insurance company and expediting the assignment of the Claims Manager</li>
<li style='padding-bottom:4px;'>Leading and encouraging all communicators on the " . $brandCompany . " team</li>
<li style='padding-bottom:4px;'>Proving your value to the Chief Executive
</ol>

For your first step, get in touch with your team's <b>Chief Executive</b> and <b>Chief Navigator</b> (through " . $brandCompany . "!) to ensure the <i>Injury Alert</i> has been delivered to the insurance company.
    <br><br>
Your actions will be crucial in ensuring that " . strtok($injuredEmployee->name, " ") . " receives the best care possible!";

    $linkText = "<a href='$treeUrl'> Click here to review the injury in " . $brandApp . ".</a>";
    
?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection

@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
    $brandCompany = env('COMPANY_NAME');
	$headerBG = "grey";
	$headerImage = "infoalert";
    $headerTitle = "Info Alert";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
$injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
$treeUrl = url('communicationLink/currentInjury', [$injury->id]);
$zengardenUrl = url('zengarden/info');
$communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
$company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
$company_string = "";
$bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = "<span style='color: blue'><b>" . $company_name . "</b></span>: ";
    }

    if($user->id == $injury->injured_employee_id){
        $stringPart1 = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> We've received the claim number for your injury! ";
    }else{
        $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->value('name');
        $stringPart1 = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> We've received the claim number for <span style='color:red;font-weight:bold;'>" . $injuredEmployee . "'s</span> injury! ";
    }

    if($user->id == $injury->injured_employee_id){
        $stringLink = " your Zengarden.";
        $linkText = "<a href='" . $zengardenUrl . "'>Click here to go to your Zengarden.</a>";
    }else{
        $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->value('name');
        $stringLink =  " the Tree of Communications.";
        $linkText = "<a href='" . $treeUrl . "'>Click here to go to the Tree of Communications.</a>";
    }

    $headlineText = "Hey " . strtok($user->name, " ");
            $bodyText = $stringPart1 . "<br>Here it is: " . $injury->claim_number . "<br>Keep this number for future reference.
If you need it again, you can find it in $brandCompany by going to" . $stringLink . "
An updated copy of the Medical Information Document for this injury has been attached. ";

?>



@include('emailPartials.headline1EmailPartial')
    @include('emailPartials.welcomeBoxEmailPartial')
    @endsection
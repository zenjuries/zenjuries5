@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "black";
	$headerImage = "zengarden";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Diary Notice";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
	$injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
	$bodyFlair = "";
	$diaryURL = url('zengarden/diary');
    $bodyText = "We've noticed you haven't posted in your diary lately.
Diary posts are a great way to let your company know how you're feeling and what we can do to help you feel better.
Whenever you have time.";
 $linkText = "Click <a href='" .$diaryURL. "'>here</a> to post in your diary because we care about you!";
?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.welcomeBoxEmailPartial')
    @endsection



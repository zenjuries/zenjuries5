@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "red";
	$headerImage = "mildinjury";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Mild Injury";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "4admin";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span><span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "</span> has been hurt. This has been classified as a First Aid injury.
This means the injured employee will not require professional medical attention or miss any work other than the day the injury occurred.
First Aid injuries should be resolved within <i>48 hours</i>.
If you suspect this may not be a First Aid injury, it should be reclassified as a Moderate or Severe injury IMMEDIATELY!
You've been hand-picked to serve as the <b>Chief Executive</b> on this injury.
<span style='display:block;font-style:italic;padding-top:10px;'>Your duties will include:</span>
<ol style='margin-top:10px;'>
<li style='padding-bottom:4px;'>Closely monitoring the injury to see if it becomes any worse. If the claim is not resolved in 48 hours, statistics show it should be reclassified as a Moderate or Severe injury.</li>
<li style='padding-bottom:4px;'>Letting the employee know you care</li>
<li style='padding-bottom:4px;'>Keeping the entire team accountable</li>
<li style='padding-bottom:4px;'>Demonstrating to your team through your actions how important the injured employee's care is to you</li>
</ol>
 For your first step, double check that the injury is properly classified as First Aid.";
$linkText = "<a href='$treeUrl'> Click here to review the injury in " . $brandApp . ".</a>";
    ?>

    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection

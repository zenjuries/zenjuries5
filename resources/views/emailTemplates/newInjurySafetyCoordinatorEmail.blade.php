@extends('layouts.emailLayout')
@section('emailcontent')

<?php
	$headerBG = "red";
	$headerImage = "newinjury";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Role Assignment";
    $bodyFlair = "4safety";
	$footerFlair = "medic";	    
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string ."</span><span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "</span> has been hurt. You've been hand-picked to serve as the <b>Safety Coordinator</b> on this injury.
<span style='display:block;font-style:italic;padding-top:10px;'>Your duties will include:</span>

<ol style='margin-top:10px;'>
<li style='padding-bottom:4px;'>Investigating what led up to the injury and report to the <b>Supervisor</b>, <b>Chief Navigator</b>, and <b>Chief Executive</b> only! <u>Not</u> through the <i>Tree of Communication</i>.</li>
<li style='padding-bottom:4px;'>Investigating and reporting on if the same type of injuries can be avoided and how.</li>
</ol>

For your first step, report the circumstances that led to the injury on the Tree of Communications' Injury Details tab.";

	$linkText = "<a href='$treeUrl'> Click here to review the injury in " . $brandApp . ".</a>";
?>

    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection

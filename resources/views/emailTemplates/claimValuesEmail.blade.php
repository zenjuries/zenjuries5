@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
    $brandCompany = env('COMPANY_NAME');
	$headerBG = "cyan";
	$headerImage = "costupdate";
    $headerTitle = "Cost Update";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
$injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
$injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
$link = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
$bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> Please confirm the Reserve, Medical, and Indemnity Values for <span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "'s</span> claim are up to date.";
$linkText = "<a href='" . $link . "'>You can click here to view them.</a>";
?>


    @include('emailPartials.headline2EmailPartial')
    @include('emailPartials.commonTextEmailPartial')
    @endsection

@extends('layouts.emailLayout')
@section('emailcontent')
<?php
$brandCompany = env('COMPANY_NAME');
$headerBG = "purple";
$headerImage = "addteam";
$headerTitle = "Added to a Team";
$bodyFlair = "4customerservice";	
$footerFlair = "medic";
$user = \App\User::where('id', $queuedMessage->user_id)->first();
$headlineText = "Hey " . strtok($user->name, " ");
$communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
$company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
$zenboardLink = url('/communicationLink/zenboard/' . $communication->company_id);
$company_string = "";
if($user->type !== "internal"){
        $company_string = $company_name . ": ";
}
$bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> You've been added to one of <span style='color:red;font-weight:bold;'>" . $company_name . "'s</span> teams as a <b>Customer Service Agent</b>.
As a $brandCompany team member, your actions will be crucial in ensuring that your company's injured employees have the most pleasant experience possible.
As the <b>Customer Service Agent</b>, your communication will be crucial in ensuring the company's claim is handled quickly and efficiently.
<span style='display:block;font-style:italic;padding-top:10px;'>Your duties will include:</span>
<ol style='margin-top:10px;'>
<li style='padding-bottom:4px;'>Opening communication with your team's <b>Chief Navigator</b> and setting expectations regarding insurance company procedures</li>

<li style='padding-bottom:4px;'>Making sure claim is filed with insurance company</li>

<li style='padding-bottom:4px;'>Finding out who the <b>Claim Manager</b> is on the claim and reporting it to the <b>Chief Navigator</b>.</li>

<li style='padding-bottom:4px;'>Notifying producing agent on account</li>
</ol>
If you follow these steps, Zenjuries will make handling your workers comp claims as painless as possible!<br><br>";
$linkText = "<a href='" . $zenboardLink . "'>Click here to go to your dashboard!</a>";
?>

@include('emailPartials.headline1EmailPartial')
@include('emailPartials.zenfuciusCommentEmailPartial')

@endsection
@extends('layouts.emailLayout')
@section('emailcontent')

    <?php
    $brandCompany = env('COMPANY_NAME');
	$headerBG = "purple";
	$headerImage = "addteam";
    $headerTitle = "Added to a Team";
    $bodyFlair = "4safety";	
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $zenboardLink = url('/communicationLink/zenboard/' . $communication->company_id);
    $company_string = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
            $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> You've been added to one of <span style='color:red;font-weight:bold;'>" . $company_name . "'s</span> teams as a <b>Safety Coordinator</b>.
As a $brandCompany team member, your actions will be crucial in ensuring that your company's injured employees have the most pleasant experience possible.
As a <b>safety coordinator</b>, you will be investigating injuries and working to prevent future accidents.
Your duties will include:<br>
<ol>
<li>Investigating what led up to the injury and report to the Supervisor, Chief Navigator, and Chief Executive only! Not through zenjuries.</li>
<br>
<li>Investigating and reporting on if the same type of injuries can be avoided and how</li>
</ol>
<br>
If you follow these steps, Zenjuries will make handling your workers comp claims as painless as possible!<br><br>";
$linkText = "<a href='" . $zenboardLink . "'>Click here to go to your dashboard!</a>";
?>


    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')

@endsection


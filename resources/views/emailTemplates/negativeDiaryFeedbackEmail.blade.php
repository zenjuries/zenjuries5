@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "orange";
	$headerImage = "employeealert";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Negative Feedback";
	$footerFlair = "alert";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
$injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
$injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> Your injured employee <span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "</span> has just left some negative feedback in their diary.
It is very important to find out why, and see if there's anything your team can do to help!";
$linkText = "<a href='$treeUrl'>Click here to review the claim in " . $brandApp . ".</a>";
?>
@include('emailPartials.alertTextEmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
    @endsection



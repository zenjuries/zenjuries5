@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	 $headerBG = "grey";
	$headerImage = "treeupdate";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Tree Update";
	$footerFlair = "medic";
    //we only want tree posts that are more recent than $oneWeekAgo
    $oneWeekAgo = \Carbon\Carbon::today()->subWeek();
    //get user and communication objects
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    //get the squads/teams the user is a part of
    $userSquads = \App\SquadMember::where('user_id', $user->id)->select('squad_id')->get();
    //get their open injuries
    $userInjuries = \App\Injury::whereIn('squad_id', $userSquads)->where('resolved', 0)->orderBy('company_id', 'asc')->get();
    //get resolved injuries with recent tree posts
    $resolvedInjuries = \App\Injury::whereIn('squad_id', $userSquads)->where('resolved', 1)
            ->join('tree_post', 'tree_post.injury_id', '=', 'injuries.id')
            ->where('tree_post.created_at', '>', $oneWeekAgo)
            ->select('injuries.*')
            ->orderBy('company_id', 'asc')
            ->get();
    if($resolvedInjuries->count()){
        $userInjuries = $userInjuries->merge($resolvedInjuries);
        $userInjuries = $userInjuries->sortBy('company_id');
    }
	$bodyFlair = "";
    $profileUrl = url('/communicationLink/userProfile');

    $headlineText = "Weekly Tree of Communications Summary";

    $bodyText =  "<h3>Your claims have had new Tree of Communication posts in the last week. Click a claim to see who's talking</h3>";

    //injuries are sorted by company, $company_id represents the company
    $company_id = NULL;

    foreach($userInjuries as $injury){
        //if not equal, print the new company's name and update $company_id to the new company
        if($company_id !== $injury->company_id){
            $injuryCompanyName = \App\Company::where('id', $injury->company_id)->value('company_name');
            $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>$injuryCompanyName:</span>" . $bodyText;
            $company_id = $injury->company_id;
        }

        $treePostsCount = \App\TreePost::where('injury_id', $injury->id)->whereNotNull('user_id')->where('created_at', '>', $oneWeekAgo)->orderBy('created_at', 'asc')->count();
        if($treePostsCount == 1){
            $postString = $treePostsCount . " post";
        }else{
            $postString = $treePostsCount . " posts";
        }

        $injuredEmployeeName = \App\User::where('id', $injury->injured_employee_id)->value('name');
        if($injury->resolved){
            $injuredEmployeeName = $injuredEmployeeName . " (resolved)";
        }
        $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
        $bodyText = $bodyText . "<a href='$treeUrl'><b> $injuredEmployeeName </b> - $postString</a><br>";
    }

    $bodyText = $bodyText . "<br>If you would like to be instantly notified when there is a new tree post."; 
    $linkText = "<a href='$profileUrl'>you may enable this feature on the settings tab of your profile.</a>";

    ?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.welcomeBoxEmailPartial')
@endsection
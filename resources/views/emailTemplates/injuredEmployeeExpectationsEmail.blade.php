@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "grey";
	$headerImage = "zendoc";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Document";
	$footerFlair = "medic";
	$bodyFlair = "";
	    $linkText = "";
            $headlineText = "We're really sorry to hear about your injury!";
            $bodyText = "This document will let you know what to expect during the life of your claim.
Remember, you are NOT alone!
You have a squad in " . $brandCompany . " working hard to make sure you have the best experience possible.";
    ?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.welcomeBoxEmailPartial')
    @endsection

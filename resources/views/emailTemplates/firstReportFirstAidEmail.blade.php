@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "orange";
	$headerImage = "mildinjuryalert";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Mild Injury Alert";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
$injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
$injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
$firstReport = \App\FirstReportOfInjury::where('injury_id', $injury->id)->first();
$frUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
            $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> Here's the Injury Alert for " . "<span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "'s</span> Mild injury.
A copy should be kept in your company's personal records, but since this is just a Mild injury, it doesn't need to be reported to your insurance company.
If you would like to report it, you may change the injury's severity from the Tree of Communications page.";
$linkText = "<a href='" . $frUrl . "'>Click here to go to the Tree of Communications page</a>.";
?>
@include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
    @endsection
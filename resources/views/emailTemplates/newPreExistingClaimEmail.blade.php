@extends('layouts.emailLayout')
@section('emailcontent')

    <?php
    $headerBG = "red";
    $headerImage = "newinjury";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Claim Reported";
    $bodyFlair = "";
    $footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    if($injury->severity === "Mild"){
        $severity_string = " This is a Mild injury, so it should be monitored to make sure it doesn't become more serious. ";
    }else{
        $severity_string = " The claim should have already been reported to the insurance carrier.";
    }
    //$injury_squad = \App\Squad::where('id', $injury->squad_id)->get();
    $squad_positions = \App\SquadMember::where('squad_id', $injury->squad_id)->where('user_id', $user->id)->get();
    $squad_string = "";
    if(count($squad_positions) === 1){
        $squad_string = "You've been assigned to the role of ";
        $squad_position = $squad_positions[0];
        if($squad_position->position_id == 1){
            $squad_string .= " Chief Executive ";
        }else if($squad_position->position_id == 2){
            $squad_string .= " Doctor ";
        }else if($squad_position->position_id == 3){
            $squad_string .= " Physical Therapist ";
        }else if($squad_position->position_id == 4){
            $squad_string .= " Claim Manager ";
        }else if($squad_position->position_id == 5){
            $squad_string .= " Work Comp Agent ";
        }else if($squad_position->position_id == 6){
            $squad_string .= " Service Agent ";
        }else if($squad_position->position_id == 7){
            $squad_string .= " Safety Manager ";
        }else if($squad_position->position_id == 8){
            $squad_string .= " Employee Supervisor ";
        }else if($squad_position->position_id == 9){
            $squad_string .= " Chief Navigator ";
        }else if($squad_position->position_id == 10){
            $squad_string .= " Other ";
        }
        $squad_string .= "for this injury.";
    }else if (count($squad_positions) > 1){
        $squad_string = "You've been assigned to the following positions for this injury: ";
        foreach($squad_positions as $squad_position){
            if($squad_position->position_id == 1){
                $squad_string .= " Chief Executive,";
            }else if($squad_position->position_id == 2){
                $squad_string .= " Doctor,";
            }else if($squad_position->position_id == 3){
                $squad_string .= " Physical Therapist,";
            }else if($squad_position->position_id == 4){
                $squad_string .= " Claim Manager,";
            }else if($squad_position->position_id == 5){
                $squad_string .= " Work Comp Agent,";
            }else if($squad_position->position_id == 6){
                $squad_string .= " Service Agent,";
            }else if($squad_position->position_id == 7){
                $squad_string .= " Safety Manager,";
            }else if($squad_position->position_id == 8){
                $squad_string .= " Employee Supervisor,";
            }else if($squad_position->position_id == 9){
                $squad_string .= " Chief Navigator,";
            }
        }
        if (substr($squad_string, -1, 1) == ',')
        {
            $squad_string = substr($squad_string, 0, -1);
        }
        $squad_string.= ". ";
    }

    $claim_number_string = "";
    if($injury->claim_number !== "" && $injury->claim_number !== NULL){
        $claim_number_string = "<br>The Claim Number for this injury is <b>" . $injury->claim_number . "</b>";
    }

    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string ."</span><span style='color:red;font-weight:bold;'> An in-progress claim has been reported for " . $injuredEmployee->name . ": </span>".
    $squad_string .
    " Check the Tree of Communications page for the injury to check on the current claim status." . $severity_string;
    $linkText = "<a href='$treeUrl'> Click here to review the injury in " . $brandApp . ".</a>" .
    $claim_number_string;


    ?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection
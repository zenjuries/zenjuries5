@extends('layouts.emailLayout')
@section('emailcontent')

    <?php
    $brandCompany = env('COMPANY_NAME');
	$headerBG = "purple";
	$headerImage = "addteam";
    $headerTitle = "Added to a Team";
    $bodyFlair = "4supervisor";	
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $zenboardLink = url('/communicationLink/zenboard/' . $communication->company_id);
    $company_string = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
            $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> You've been added to one of <span style='color:red;font-weight:bold;'>" . $company_name . "'s</span> teams as a <b>Supervisor</b>.
As a $brandCompany team member, your actions will be crucial in ensuring that your company's injured employees have the most pleasant experience possible.
As a <b>Supervisor</b>, you'll be personally interacting with the injured employee and making sure they have the best experience possible.
<span style='display:block;font-style:italic;padding-top:10px;'>Your duties will include:</span>
<ol style='margin-top:10px;'>
<li style='padding-bottom:4px;'>Helping deliver employee to prompt medical care</li>
<li style='padding-bottom:4px;'>Setting and delivering employee expectations document to help employee not be afraid</li>
<li style='padding-bottom:4px;'>Talking to the <b>Chief Navigator</b> and fully disclosing all aspects of injury occurrence and diagnosis</li>
<li style='padding-bottom:4px;'>Communicating with employee at least once a day for first five days (\"How are you?\")</li>
<li style='padding-bottom:4px;'>Adding updates to zenjuries</li>
</ol>
If you follow these steps, $brandCompany will make handling your workers comp claims as painless as possible!";
$linkText = "<a href='" . $zenboardLink . "'>Click here to go to your dashboard!</a>";
?>



@include('emailPartials.headline1EmailPartial')
@include('emailPartials.zenfuciusCommentEmailPartial')

@endsection

@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
    $brandCompany = env('COMPANY_NAME');
	$headerBG = "cyan";
	$headerImage = "inforequest";
    $headerTitle = "Info Request";
	$footerFlair = "info";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $link = route('communicationLink.admin', ['company_id' => $communication->company_id]);
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
            $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> What is your Company's dominant class code?
Enter your Class Code into Zenjuries to help us help you prevent future injuries!";
$linkText = "<a href='" . $link . "'>Click Here</a> to enter your most dominant class code.";
?>



@include('emailPartials.headline1EmailPartial')
    @include('emailPartials.commonTextEmailPartial')
    @endsection

<?php
//grab a bunch of injury data to start with
$test_account_array = array(2,3,13,25,26,85,84,87,117,118,119,142,283,284,286,299,300,301,302,303,305,314, 1148, 313, 910);

//Excluding injuries with company_id 2 excludes injuries reported by the "Zenjuries" test account (it has no real injuries).
$all_injuries = \App\Injury::whereNotIn('company_id', $test_account_array)->where('injuries.in_progress', 0)->join('first_report_of_injuries', 'injuries.id', '=', 'first_report_of_injuries.injury_id')
    ->select('injuries.*', 'first_report_of_injuries.action_taken', 'first_report_of_injuries.action_taken_at')
    ->get();

$resolved_injuries = \App\Injury::whereNotIn('company_id', $test_account_array)->where('injuries.in_progress', 0)->where('injuries.resolved', 1)
    ->join('first_report_of_injuries', 'injuries.id', '=', 'first_report_of_injuries.injury_id')
    ->select('injuries.*', 'first_report_of_injuries.action_taken', 'first_report_of_injuries.action_taken_at')
    ->get();

$open_injuries = \App\Injury::whereNotIn('company_id', $test_account_array)->where('injuries.in_progress', 0)->where('injuries.resolved', 0)
    ->join('first_report_of_injuries', 'injuries.id', '=', 'first_report_of_injuries.injury_id')
    ->select('injuries.*', 'first_report_of_injuries.action_taken', 'first_report_of_injuries.action_taken_at')
    ->get();

$this_month_injuries = \App\Injury::whereNotIn('company_id', $test_account_array)->where('injuries.in_progress', 0)->where('injuries.created_at', '>=', \Carbon\Carbon::today()->subMonth())
    ->join('first_report_of_injuries', 'injuries.id', '=', 'first_report_of_injuries.injury_id')
    ->select('injuries.*', 'first_report_of_injuries.action_taken', 'first_report_of_injuries.action_taken_at')
    ->get();

$this_year_injuries = \App\Injury::whereNotIn('company_id', $test_account_array)->where('injuries.in_progress', 0)->where('injuries.created_at', '>=', \Carbon\Carbon::today()->subYear())
    ->join('first_report_of_injuries', 'injuries.id', '=', 'first_report_of_injuries.injury_id')
    ->select('injuries.*', 'first_report_of_injuries.action_taken', 'first_report_of_injuries.action_taken_at')
    ->get();
    
$july_1st_2020 = \Carbon\Carbon::create("2020", "7", "1", "0", "0", "0");

$dec_31st_2021 = \Carbon\Carbon::create("2021", "12", "31", "0", "0", "0");    
    
    
$eighteen_month_injuries = \App\Injury::whereNotIn('company_id', $test_account_array)->where('injuries.in_progress', 0)->where('injuries.created_at', '>=', $july_1st_2020)
->where('injuries.created_at', '<=', $dec_31st_2021)  
->join('first_report_of_injuries', 'injuries.id', '=', 'first_report_of_injuries.injury_id')
    ->select('injuries.*', 'first_report_of_injuries.action_taken', 'first_report_of_injuries.action_taken_at')
    ->get();  
    

//only use this function with resolved injuries
/*
function calculateAverageTimeToClose($injuries){
    $injury_count = 0;
    $resolved_in_12_months_count = 0;
    $resolved_in_24_months_count = 0;
    $resolved_in_36_months_count = 0;
    foreach($injuries as $injury){
        if(!is_null($injury->resolved_at)){
            $injury_count++;
            $injury_date = \Carbon\Carbon::createFromFormat("m-d-Y g:i a", $injury->injury_date);
            //$resolved_date =
        }
    }
}
*/

function calculatePercentLitigated($injuries, $test_account_array){
    $litigated_count = 0;
    $total = count($injuries);
    $percent_litigated = 0;
    if($total > 0){
        foreach($injuries as $injury){
            if($injury->paused_for_litigation === 1){
                $litigated_count++;
            }
        }

        if($litigated_count > 0){
            $percent_litigated = round(($litigated_count / $total) * 100, 2);
        }

    }
    $return_array = array();
    $return_array['total_claims'] = $total;
    $return_array['total'] = $litigated_count;
    $return_array['percent'] = $percent_litigated;

    $injuries = \App\Injury::whereNotIn('company_id', $test_account_array)->get();

    $overall_total = count($injuries);
    
    var_dump($overall_total);
    
    $overall_percent_litigated = 0;
    $overall_litigated_count = 0;
    foreach($injuries as $injury){
        if($injury->paused_for_litigation === 1){
            $overall_litigated_count++;
        }
    }

    if($overall_litigated_count > 0){
        $overall_percent_litigated = round(($overall_litigated_count / $overall_total) * 100, 2);
    }

    $return_array['overall_total'] = $overall_litigated_count;
    $return_array['overall_percent'] = $overall_percent_litigated;
    $return_array['overall_dataset_total'] = $overall_total;

    return $return_array;
}

function getDiaryPostRatings(){

    $diary = \App\DiaryPost::where('user_id', '!=', 912)->where('user_id', '!=', 2764)->where('post_type', 'claim')->get();
    $total = count($diary);
    $one_count = 0;
    $two_count = 0;
    $three_count = 0;
    $four_count = 0;
    $five_count = 0;
    $six_count = 0;
    $seven_count = 0;
    $eight_count = 0;
    $nine_count = 0;
    $ten_count = 0;
    foreach($diary as $post){
        if($post->mood === '1'){
            $one_count++;
        }else if($post->mood === '2'){
            $two_count++;
        }else if($post->mood === '3'){
            $three_count++;
        }else if($post->mood === '4'){
            $four_count++;
        }else if($post->mood === '5'){
            $five_count++;
        }else if($post->mood === '6'){
            $six_count++;
        }else if($post->mood === '7'){
            $seven_count++;
        }else if($post->mood === '8'){
            $eight_count++;
        }else if($post->mood === '9'){
            $nine_count++;
        }else if($post->mood === '10'){
            $ten_count++;
        }
    }

    $return_array = array();
    $return_array['total'] = $total;
    $return_array['one_count'] = $one_count;
    $return_array['one_percentage'] = round(($one_count / $total) * 100);
    $return_array['two_count'] = $two_count;
    $return_array['two_percentage'] = round(($two_count / $total) * 100);
    $return_array['three_count'] = $three_count;
    $return_array['three_percentage'] = round(($three_count / $total) * 100);
    $return_array['four_count'] = $four_count;
    $return_array['four_percentage'] = round(($four_count / $total) * 100);
    $return_array['five_count'] = $five_count;
    $return_array['five_percentage'] = round(($five_count / $total) * 100);
    $return_array['six_count'] = $six_count;
    $return_array['six_percentage'] = round(($six_count / $total) * 100);
    $return_array['seven_count'] = $seven_count;
    $return_array['seven_percentage'] = round(($seven_count / $total) * 100);
    $return_array['eight_count'] = $eight_count;
    $return_array['eight_percentage'] = round(($eight_count / $total) * 100);
    $return_array['nine_count'] = $nine_count;
    $return_array['nine_percentage'] = round(($nine_count / $total) * 100);
    $return_array['ten_count'] = $ten_count;
    $return_array['ten_percentage'] = round(($ten_count / $total) * 100);

    return $return_array;
}

function calculateAverageFirstReportDeliveryTime($injuries){
    $hours = 0;
    $count = 0;
    $count_24_hours = 0;
    $percent_24_hours = 0;
    $count_48_hours = 0;
    $percent_48_hours = 0;
    $count_72_hours = 0;
    $percent_72_hours = 0;
    $count_4_7_days = 0;
    $percent_4_7_days = 0;
    $count_8_14_days = 0;
    $percent_8_14_days = 0;
    $count_15_21_days = 0;
    $percent_15_21_days = 0;
    $count_22_30_days = 0;
    $percent_22_30_days = 0;
    $count_two_months = 0;
    $percent_two_months = 0;
    $count_other = 0;
    $percent_other = 0;

    $count_within_12_days = 0;
    $percent_within_12_days = 0;

    $count_within_7_days = 0;
    $percent_within_7_days = 0;

    foreach($injuries as $injury){
        if($injury->action_taken === 1 && !is_null($injury->action_taken_at)){
            $company_joined_on = DB::table('companies')->where('id', $injury->company_id)->value('created_at');
            $company_joined_on = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $company_joined_on);
            $injury_date = \Carbon\Carbon::createFromFormat("m-d-Y g:i a", $injury->injury_date);
            $reported_at = $injury->created_at;
            //$reported_at = \Carbon\Carbon::createFromFormat("m-d-Y g:i a", $injury->created_at);
            $action_taken_at = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $injury->action_taken_at);
            if($injury_date->gt($company_joined_on)){
                $count++;
                $reported_hours = $reported_at->diffInHours($action_taken_at);
                $hours+= $reported_hours;

                if($reported_hours <= 24){
                    $count_24_hours++;
                }else if($reported_hours <= 48){
                    $count_48_hours++;
                }else if($reported_hours <= 72){
                    $count_72_hours++;
                }else if($reported_hours <= 168){
                    $count_4_7_days++;
                }else if($reported_hours <= 336){
                    $count_8_14_days++;
                }else if($reported_hours <= 504){
                    $count_15_21_days++;
                }else if($reported_hours <= 720){
                    $count_22_30_days++;
                }else if($reported_hours <= 1440){
                    $count_two_months++;
                }else{
                    $count_other++;
                }
                //var_dump($injury->id . " " . $reported_at->diffInHours($action_taken_at));

                //within 12 days
                if($reported_hours <= 288){
                    $count_within_12_days++;
                }

                if($reported_hours <= 168){
                    $count_within_7_days++;
                }
            }
        }
    }
    if($count > 0){
        if($count_24_hours !== 0){
            $percent_24_hours = round(($count_24_hours / $count) * 100);
        }

        if($count_48_hours !== 0){
            $percent_48_hours = round(($count_48_hours / $count) * 100);
        }

        if($count_72_hours !== 0){
            $percent_72_hours = round(($count_72_hours / $count) * 100);
        }

        if($count_4_7_days !== 0){
            $percent_4_7_days = round(($count_4_7_days / $count) * 100);
        }

        if($count_8_14_days !== 0){
            $percent_8_14_days = round(($count_8_14_days / $count) * 100);
        }

        if($count_15_21_days !== 0){
            $percent_15_21_days = round(($count_15_21_days / $count) * 100);
        }

        if($count_22_30_days !== 0){
            $percent_22_30_days = round(($count_22_30_days / $count) * 100);
        }

        if($count_two_months !== 0){
            $percent_two_months = round(($count_two_months / $count) * 100);
        }

        if($count_other !== 0){
            $percent_other = round(($count_other / $count) * 100);
        }

        //percent within 12 days
        if($count_within_12_days !== 0){
            $percent_within_12_days = round(($count_within_12_days / $count) * 100);
        }

        //percent within 7 days
        if($count_within_7_days !== 0){
            $percent_within_7_days = round(($count_within_7_days / $count) * 100);
        }
    }

    if($hours > 0 && $count > 0){
        $hours = $hours / $count;
        $hours = round($hours, 0);
    }

    $html =  $hours . " Hours" . " (" . $count . " injuries)" . "<br>" .
        "Reported within 24 hours: " . $count_24_hours . " injuries (" . $percent_24_hours . "%)<br>" .
        "Reported within 7 days: " . $count_within_7_days . " injuries (" . $percent_within_7_days . "%)<br>" .
        "Reported within 12 days: " . $count_within_12_days . " injuries (" . $percent_within_12_days . "%)<br>" .
        "Reported within 48 hours: " . $count_48_hours . " injuries (" . $percent_48_hours . "%)<br>" .
        "Reported within 72 hours: " . $count_72_hours . " injuries (" . $percent_72_hours . "%)<br>" .
        "Reported within 4 - 7 days: " . $count_4_7_days . " injuries (" . $percent_4_7_days . "%)<br>" .
        "Reported within 8 - 14 days: " . $count_8_14_days . " injuries (" . $percent_8_14_days . "%)<br>" .
        "Reported within 15 - 21 days: " . $count_15_21_days . " injuries (" . $percent_15_21_days . "%)<br>" .
        "Reported within 22 - 30 days: " . $count_22_30_days . " injuries (" . $percent_22_30_days . "%)<br>" .
        "Reported within 2 months: " . $count_two_months . " injuries (" . $percent_two_months . "%)<br>" .
        "Reported after 2 months: " . $count_other . " injuries (" . $percent_other . "%)<br>";

    echo $html;
}

function getAverageLengthOfInjuryIncludingOpen($injuries){
	$injury_count = 0;
    $total_days = 0;
    $average_days = 0;
    $resolved_in_12_months = 0;
    $resolved_in_24_months = 0;
    $resolved_in_36_months = 0;
    $resolved_after_36_months = 0;
    $today = \Carbon\Carbon::today();
    
    foreach ($injuries as $injury){
		    $company_joined_on = DB::table('companies')->where('id', $injury->company_id)->value('created_at');
	        $company_joined_on = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $company_joined_on);
	
	        $reported_at = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $injury->created_at);
	        $occurred_at = \Carbon\Carbon::createFromFormat("m-d-Y g:i a", $injury->injury_date);
	    	if($occurred_at->gt($company_joined_on)){
            //var_dump("New Injury");
            $claim_open_date = $injury->injury_date;
            //var_dump($claim_open_date);
            //07-10-2018 2:47 pm
            $claim_open_date = \Carbon\Carbon::createFromFormat("m-d-Y g:i a", $claim_open_date);
            //var_dump($claim_open_date);

            if($injury->claim_close_date !== NULL){
                $claim_close_date = $injury->claim_close_date;

            }else if($injury->resolved_at !== NULL){
                $claim_close_date = $injury->resolved_at;

            }else {
	            $claim_close_date = $today;
            }
            //var_dump($claim_close_date);

            $injury_days = $claim_open_date->diffInDays($claim_close_date);

            if($injury_days <= 365){
                $resolved_in_12_months++;
            }else if($injury_days <= 730){
                $resolved_in_24_months++;
            }else if($injury_days <= 1095){
                $resolved_in_36_months++;
            }else{
                $resolved_after_36_months++;
            }

            $total_days+= $injury_days;
            $injury_count++;
        }    
            //var_dump("Injury Length was " . $claim_open_date->diffInDays($claim_close_date) . " days.");
        
    }
    if($injury_count > 0 && $total_days > 0){
        $average_days = round($total_days / $injury_count);
    }

    if($injury_count !== 0 && $resolved_in_12_months !== 0){
        $percent_12months = round(($resolved_in_12_months / $injury_count) * 100);
    }else{
        $percent_12months = "No Data";
    }

    if($injury_count !== 0 && $resolved_in_24_months !== 0){
        $percent_24months = round(($resolved_in_24_months / $injury_count) * 100);
    }else{
        $percent_24months = "No Data";
    }

    if($injury_count !== 0 && $resolved_in_36_months !== 0){
        $percent_36months = round(($resolved_in_36_months / $injury_count) * 100);
    }else{
        $percent_36months = "No Data";
    }

    if($injury_count !== 0 && $resolved_after_36_months){
        $percent_after = round(($resolved_after_36_months / $injury_count) * 100);
    }else{
        $percent_after = "No Data";
    }

    //var_dump("Average days per injury = " . $average_days);

    $return_array = array();
    $return_array['average_days'] = $average_days . " Days (" . $injury_count . " injuries)";
    $return_array['total_count'] = $injury_count;
    $return_array['12_month_count'] = $resolved_in_12_months;
    $return_array['12_month_percent'] =  $percent_12months . "%";
    $return_array['24_month_count'] = $resolved_in_24_months;
    $return_array['24_month_percent'] =  $percent_24months . "%";
    $return_array['36_month_count'] = $resolved_in_36_months;
    $return_array['36_month_percent'] =  $percent_36months . "%";
    $return_array['remaining_percent'] = $percent_after . "%";
    return $return_array;
}

function getAverageLengthOfInjury($injuries){
    $injury_count = 0;
    $total_days = 0;
    $average_days = 0;
    $resolved_in_12_months = 0;
    $resolved_in_24_months = 0;
    $resolved_in_36_months = 0;
    $resolved_after_36_months = 0;
    foreach ($injuries as $injury){
        if($injury->resolved === 1){
            //var_dump("New Injury");
            $claim_open_date = $injury->injury_date;
            //var_dump($claim_open_date);
            //07-10-2018 2:47 pm
            $claim_open_date = \Carbon\Carbon::createFromFormat("m-d-Y g:i a", $claim_open_date);
            //var_dump($claim_open_date);

            if($injury->claim_close_date !== NULL){
                $claim_close_date = $injury->claim_close_date;

            }else{
                $claim_close_date = $injury->resolved_at;

            }
            //var_dump($claim_close_date);

            $injury_days = $claim_open_date->diffInDays($claim_close_date);

            if($injury_days <= 365){
                $resolved_in_12_months++;
            }else if($injury_days <= 730){
                $resolved_in_24_months++;
            }else if($injury_days <= 1095){
                $resolved_in_36_months++;
            }else{
                $resolved_after_36_months++;
            }

            $total_days+= $injury_days;
            $injury_count++;
            //var_dump("Injury Length was " . $claim_open_date->diffInDays($claim_close_date) . " days.");
        }
    }
    if($injury_count > 0 && $total_days > 0){
        $average_days = round($total_days / $injury_count);
    }

    if($injury_count !== 0 && $resolved_in_12_months !== 0){
        $percent_12months = round(($resolved_in_12_months / $injury_count) * 100);
    }else{
        $percent_12months = "No Data";
    }

    if($injury_count !== 0 && $resolved_in_24_months !== 0){
        $percent_24months = round(($resolved_in_24_months / $injury_count) * 100);
    }else{
        $percent_24months = "No Data";
    }

    if($injury_count !== 0 && $resolved_in_36_months !== 0){
        $percent_36months = round(($resolved_in_36_months / $injury_count) * 100);
    }else{
        $percent_36months = "No Data";
    }

    if($injury_count !== 0 && $resolved_after_36_months){
        $percent_after = round(($resolved_after_36_months / $injury_count) * 100);
    }else{
        $percent_after = "No Data";
    }

    //var_dump("Average days per injury = " . $average_days);

    $return_array = array();
    $return_array['average_days'] = $average_days . " Days (" . $injury_count . " injuries)";
    $return_array['total_count'] = $injury_count;
    $return_array['12_month_count'] = $resolved_in_12_months;
    $return_array['12_month_percent'] =  $percent_12months . "%";
    $return_array['24_month_count'] = $resolved_in_24_months;
    $return_array['24_month_percent'] =  $percent_24months . "%";
    $return_array['36_month_count'] = $resolved_in_36_months;
    $return_array['36_month_percent'] =  $percent_36months . "%";
    $return_array['remaining_percent'] = $percent_after . "%";
    return $return_array;
}

function calculateReserveDecrease($injuries){
    $injury_count = 0;
    $reduction_total = 0;
    $percentage_total = 0;
    $average_reduction = NULL;
    $average_reduction_percent = NULL;
    $one_month_ago = \Carbon\Carbon::today()->subMonth();
    foreach($injuries as $injury){
        if($injury->resolved === 1){
            //var_dump($injury->id);
            $highest_medical_reserve = \App\ReserveMedicalCost::where('injury_id', $injury->id)->orderBy('cost', 'desc')->value('cost');
            $highest_legal_reserve = \App\ReserveLegalCost::where('injury_id', $injury->id)->orderBy('cost', 'desc')->value('cost');
            $highest_indemnity_reserve = \App\ReserveIndemnityCost::where('injury_id', $injury->id)->orderBy('cost', 'desc')->value('cost');
            $highest_misc_reserve = \App\ReserveMiscellaneousCost::where('injury_id', $injury->id)->orderBy('cost', 'desc')->value('cost');

            if(is_null($highest_medical_reserve)){
                $highest_medical_reserve = 0;
            }

            if(is_null($highest_legal_reserve)){
                $highest_legal_reserve = 0;
            }

            if(is_null($highest_indemnity_reserve)){
                $highest_indemnity_reserve = 0;
            }

            if(is_null($highest_misc_reserve)){
                $highest_misc_reserve = 0;
            }

            $total_reserve = $highest_medical_reserve + $highest_legal_reserve + $highest_indemnity_reserve + $highest_misc_reserve;

            if($total_reserve > 0){
                //var_dump($injury->id);
                $final_cost = $injury->final_cost;
                if(is_null($final_cost)){
                    //var_dump($injury->id);
                    if($injury->resolved_at->lte($one_month_ago)){
                        //var_dump($injury->id);
                        $latest_medical = \App\MedicalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        if(is_null($latest_medical)){
                            $latest_medical = 0;
                        }
                        $latest_legal = \App\LegalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        if(is_null($latest_legal)){
                            $latest_legal = 0;
                        }
                        $latest_indemnity = \App\IndemnityCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        if(is_null($latest_indemnity)){
                            $latest_indemnity = 0;
                        }
                        $latest_misc = \App\MiscellaneousCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        if(is_null($latest_misc)){
                            $latest_misc = 0;
                        }
                        $latest_reserve_medical = \App\ReserveMedicalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        if(is_null($latest_reserve_medical)){
                            $latest_reserve_medical = 0;
                        }
                        $latest_reserve_legal = \App\ReserveLegalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        if(is_null($latest_reserve_legal)){
                            $latest_reserve_legal = 0;
                        }
                        $latest_reserve_indemnity = \App\ReserveIndemnityCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        if(is_null($latest_reserve_indemnity)){
                            $latest_reserve_indemnity = 0;
                        }
                        $latest_reserve_misc = \App\MiscellaneousCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                        if(is_null($latest_reserve_misc)){
                            $latest_reserve_misc = 0;
                        }

                        $final_cost = $latest_medical + $latest_legal + $latest_indemnity + $latest_misc + //continues on next line
                            $latest_reserve_medical + $latest_reserve_legal + $latest_reserve_indemnity + $latest_reserve_misc;

                    }
                }

                if($total_reserve > 0 && $final_cost !== NULL && $final_cost !== 0){
                    $injury_count++;
                    //var_dump("total_reserve = " . $total_reserve);
                    //var_dump("final cost = " . $final_cost);
                    $reduction = $total_reserve - $final_cost;
                    //var_dump("reserve reduction = " . $reduction);
                    $percent_reduction = ($reduction / $total_reserve) * 100;
                    //var_dump("percent reduction = " . $percent_reduction);
                    $percentage_total+= $percent_reduction;
                    $reduction_total+= $reduction;
                }
            }

        }
    }
    if($injury_count > 0){
        $average_reduction = round($reduction_total / $injury_count, 2);
        //$percent_decrease = ($average_reduction / $)
        $average_reduction = number_format($average_reduction, 2);
        $average_reduction = "$" . $average_reduction;
        //var_dump("average reduction = " . $average_reduction);

        $average_reduction_percent = round($percent_reduction / $injury_count);
        $average_reduction_percent = $average_reduction_percent . "%";
    }

    if($average_reduction !== NULL){
        return $average_reduction . ",  " . $average_reduction_percent . " reduction.  (" . $injury_count . " injuries)";
    }else{
        return "Sorry, there was no data for that set of injuries."  . " (" . $injury_count . " injuries)";
    }
}

function calculateAverageReportingTime($injuries){
    $hours = 0;
    $count = 0;
    $count_24_hours = 0;
    $percent_24_hours = 0;
    $count_48_hours = 0;
    $percent_48_hours = 0;
    $count_72_hours = 0;
    $percent_72_hours = 0;
    $count_4_7_days = 0;
    $percent_4_7_days = 0;
    $count_8_14_days = 0;
    $percent_8_14_days = 0;
    $count_15_21_days = 0;
    $percent_15_21_days = 0;
    $count_22_30_days = 0;
    $percent_22_30_days = 0;
    $count_two_months = 0;
    $percent_two_months = 0;
    $count_other = 0;
    $percent_other = 0;

    $count_within_12_days = 0;
    $percent_within_12_days = 0;

    $count_within_7_days = 0;
    $percent_within_7_days = 0;
    
    $count_within_1_day = 0;
    $percent_within_1_day = 0;
    
    $count_within_14_days = 0;
    $percent_within_14_days = 0;
    
    $company_array = array();

    foreach($injuries as $injury){
	    
	    $company = DB::table('companies')->where('id', $injury->company_id)->first();
	    
	    if(!array_key_exists($company->id, $company_array)){
		    $company_array[$company->id]['total_claims_count'] = 0;
		    $company_array[$company->id]['24_hour_count'] = 0;
		    $company_array[$company->id]['7_day_count'] = 0;
		    $company_array[$company->id]['12_day_count'] = 0;
		    $company_array[$company->id]['14_day_count'] = 0;
	    }
	    
	    
        $company_joined_on = DB::table('companies')->where('id', $injury->company_id)->value('created_at');
        $company_joined_on = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $company_joined_on);

        $reported_at = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $injury->created_at);
        $occurred_at = \Carbon\Carbon::createFromFormat("m-d-Y g:i a", $injury->injury_date);

        if($occurred_at->gt($company_joined_on)){
            //injury occurred after company joined Zenjuries, count it
            $reported_hours = $reported_at->diffInHours($occurred_at);
            $hours+= $reported_hours;
            $count++;
            if($reported_hours <= 24){
                $count_24_hours++;
                //$company_array[$company->id]['24_hour_count']++;
            }else if($reported_hours <= 48){
                $count_48_hours++;
            }else if($reported_hours <= 72){
                $count_72_hours++;
            }else if($reported_hours <= 168){
                $count_4_7_days++;
            }else if($reported_hours <= 336){
                $count_8_14_days++;
            }else if($reported_hours <= 504){
                $count_15_21_days++;
            }else if($reported_hours <= 720){
                $count_22_30_days++;
            }else if($reported_hours <= 1440){
                $count_two_months++;
            }else{
                $count_other++;
            }
            
            //within 14 days
            if($reported_hours <= 336){
	            $count_within_14_days++;
	            $company_array[$company->id]['14_day_count']++;
            }

            //within 12 days
            if($reported_hours <= 288){
	            $company_array[$company->id]['12_day_count']++;
                $count_within_12_days++;
            }
            //within 7 days
            if($reported_hours <= 168){
	            $company_array[$company->id]['7_day_count']++;
                $count_within_7_days++;
            }
            //within 1 day
            if($reported_hours <= 24){
	            $company_array[$company->id]['24_hour_count']++;
	            $count_within_1_day++;
            }
            
            $company_array[$company->id]['total_claims_count']++;
        }
    }

    if($count > 0){
        if($count_24_hours !== 0){
            $percent_24_hours = round(($count_24_hours / $count) * 100);
        }

        if($count_48_hours !== 0){
            $percent_48_hours = round(($count_48_hours / $count) * 100);
        }

        if($count_72_hours !== 0){
            $percent_72_hours = round(($count_72_hours / $count) * 100);
        }

        if($count_4_7_days !== 0){
            $percent_4_7_days = round(($count_4_7_days / $count) * 100);
        }

        if($count_8_14_days !== 0){
            $percent_8_14_days = round(($count_8_14_days / $count) * 100);
        }

        if($count_15_21_days !== 0){
            $percent_15_21_days = round(($count_15_21_days / $count) * 100);
        }

        if($count_22_30_days !== 0){
            $percent_22_30_days = round(($count_22_30_days / $count) * 100);
        }

        if($count_two_months !== 0){
            $percent_two_months = round(($count_two_months / $count) * 100);
        }

        if($count_other !== 0){
            $percent_other = round(($count_other / $count) * 100);
        }
        
        //percent within 14 days
		if($count_within_14_days !== 0){
			$percent_within_14_days = round(($count_within_14_days / $count) * 100);
		}
		
        //percent within 12 days
        if($count_within_12_days !== 0){
            $percent_within_12_days = round(($count_within_12_days / $count) * 100);
        }

        //percent within 7 days
        if($count_within_7_days !== 0){
            $percent_within_7_days = round(($count_within_7_days / $count) * 100);
        }
        
        //percent within 1 day
        if($count_within_1_day !== 0){
	        $percent_within_1_day = round(($count_within_1_day / $count) * 100);
        }


    }

    if($hours > 0 && $count > 0){
        $hours = $hours / $count;
        $hours = round($hours, 0);
    }
/*
    $html =  $hours . " Hours" . " (" . $count . " injuries)" . "<br>" .
        "Reported within 24 hours: " . $count_within_1_day . " injuries (" . $percent_within_1_day . "%)<br>" .
        "Reported within 7 days: " . $count_within_7_days . " injuries (" . $percent_within_7_days . "%)<br>" .
        "Reported within 12 days: " . $count_within_12_days . " injuries (" . $percent_within_12_days . "%)<br>" .
        "Reported within 14 days: " . $count_within_14_days . " injuries (" . $percent_within_14_days . "%)<br>" .
        "Reported within 48 hours: " . $count_48_hours . " injuries (" . $percent_48_hours . "%)<br>" .
        "Reported within 72 hours: " . $count_72_hours . " injuries (" . $percent_72_hours . "%)<br>" .
        "Reported within 4 - 7 days: " . $count_4_7_days . " injuries (" . $percent_4_7_days . "%)<br>" .
        "Reported within 8 - 14 days: " . $count_8_14_days . " injuries (" . $percent_8_14_days . "%)<br>" .
        "Reported within 15 - 21 days: " . $count_15_21_days . " injuries (" . $percent_15_21_days . "%)<br>" .
        "Reported within 22 - 30 days: " . $count_22_30_days . " injuries (" . $percent_22_30_days . "%)<br>" .
        "Reported within 2 months: " . $count_two_months . " injuries (" . $percent_two_months . "%)<br>" .
        "Reported after 2 months: " . $count_other . " injuries (" . $percent_other . "%)<br>";

    echo $html;
    */
    
    return $company_array;
}

function processCompanyArray($company_array){
	
	echo "<br>";
	foreach($company_array as $company_id => $claims){
		if($claims > 0 && $claims['total_claims_count'] > 0){
			
			$company = DB::table('companies')->where('id', $company_id)->first();
			$total_claim_count = count(\App\Injury::where('company_id', $company_id)->where('in_progress', 0)->get());
			
			if(!is_null($company->agency_id)){
				$agency = \App\Agency::where('id', $company->agency_id)->first();
				$agency_name = $agency->name;
			}else{
				$agency_name = "No Agency/DIY";
			}
			/*
			echo str_replace(',', '', $company->company_name) . "," . $agency_name . "," . $claims['total_claims_count'] . "," . $claims['24_hour_count'] . "," . $claims['7_day_count'] . "," . $claims['12_day_count'] . "," . $claims['14_day_count'] . "<br>";
			*/
			//echo str_replace(',', '', $company->company_name) . "<br>";
			//echo $agency_name . "<br>";
			//echo $claims['total_claims_count'] . "<br>";
			//echo $claims['24_hour_count'] . "<br>";
			//echo $claims['7_day_count'] . "<br>";
			//echo $claims['12_day_count'] . "<br>";
			echo $claims['14_day_count'] . "<br>";
			
			/*
			$percent_within_24_hrs = round(($claims / $total_claim_count) * 100, 2);
			
		
			if($percent_within_24_hrs >= 90 && $total_claim_count < 3){
				echo $company->company_name . " (Agency: " . $agency_name . "): " . $percent_within_24_hrs . "% of claims reported within 24 hours. total claims = " . $total_claim_count . "<br>";
			}*/
			
		}
	}
}

$average_days_per_injury_monthly = getAverageLengthOfInjury($this_month_injuries);
$average_days_per_injury_yearly = getAverageLengthOfInjury($this_year_injuries);
$average_days_per_injury_alltime = getAverageLengthOfInjury($all_injuries);
//var_dump($average_days_per_injury_alltime);

$litigated_array = calculatePercentLitigated($all_injuries, $test_account_array);
var_dump($litigated_array);

$diary_array = getDiaryPostRatings();

$company_array = calculateAverageReportingTime($eighteen_month_injuries);
//var_dump($company_array);
processCompanyArray($company_array);

//var_dump($diary_array);

?>


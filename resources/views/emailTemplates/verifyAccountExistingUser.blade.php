@extends('layouts.emailLayout')
@section('emailcontent')

    <?php
    $headerBG = "blue";
    $headerImage = "welcome";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Welcome";
    $footerFlair = "medic";
    $headlineText = "Welcome to " . $brandCompany . "!";
    $url = route('verifyAccountExistingUser', ['company_id' => $company->id, 'id' => $user->id]);
    $bodyFlair = "";
    $bodyText = "To get finish registration for your new company, please follow the link below to verify your account.
    After that, you'll be ready to start using " . $brandApp . "!<br><a href='$url'>Get Started</a>";
    $linkText = "";
    ?>
    @include('emailPartials.alertTextEmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection
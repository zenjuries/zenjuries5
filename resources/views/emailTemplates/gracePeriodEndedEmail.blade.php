@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "grey";
	$headerImage = "subalert";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Grace Period Ended";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $team_id = \App\Company::where('id', $communication->company_id)->value('team_id');
    $team = \App\Team::where('id', $team_id)->first();
    $owner_id = \App\TeamUser::where('team_id', $team_id)->where('role', 'owner')->first();
    $owner_name = \App\User::where('id', $owner_id)->first();
    $billingLink = url('settings/teams/' . $team_id . '#/subscription');
    $company_string = "";
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    if($user->id === $owner_id->user_id){
        $owner_string = "Since you're listed as the owner of this company, you may go to"; 
        $linkText = "<a href='" . $billingLink . "'>billing and purchase a new subscription.</a>";
    }else{
        $owner_string = "Your company owner can purchase a new subscription from the billing page, located within their company page.";
        $linkText = "";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> This is an alert that your company's grace period has ended, and your account has been suspended.
    All your information will be saved if you choose to re-join at a later date." . $owner_string;
    $linkText;
    
    ?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.commonTextEmailPartial')
@endsection
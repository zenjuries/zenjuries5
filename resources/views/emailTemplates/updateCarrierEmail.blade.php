@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "cyan";
	$headerImage = "updaterequest";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Update Request";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $policyNumber = \App\Company::where('id', $user->company_id)->value('policy_number');
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $zenboard = url('/communicationLink/admin/' . $communication->company_id);
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> We noticed that your policy renewal date was here, and we wanted to make sure we still have your correct policy information.";
    
$linkText = "<a href='". $zenboard. "'>Click here to go to your company page and update this info!</a>";
?>
@include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
    @endsection
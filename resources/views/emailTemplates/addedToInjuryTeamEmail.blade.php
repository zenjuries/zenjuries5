@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
    $brandCompany = env('COMPANY_NAME');
    $headerBG = "purple";
    $headerImage = "addteam";
    $headerTitle = "Added to a Team";
    $bodyFlair = "";
    $footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $injury = \App\Injury::where('id', $communication->injury_id)->first();
    $injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    //this should go to the tree page
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "";

    //TODO: WE NEED THE INJURY ID, from there we can list the injured person's name,
    // the recipient's positions on the injury team, and link them to the tree page

    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> You've been added to the team for <span style='color:red;font-weight:bold;'>" . $injured_employee->name . "'s</span> claim.";


    $linkText = "<a href='$treeUrl'>Click here to view this injury.</a>";
    ?>

    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')

@endsection
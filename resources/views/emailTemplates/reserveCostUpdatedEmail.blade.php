@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "red";
	$headerImage = "costupdate";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Reserve Cost Update";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injured_name = \App\User::where('id', $injury->injured_employee_id)->value('name');
    $current_val = \App\ReserveCosts::where('injury_id', $injury->id)->where('current', 1)->first();
    $updater_name = \App\User::where('id', $current_val->updater_id)->value('name');
    $old_val = \App\ReserveCosts::where('injury_id', $injury->id)->where('current', 0)->orderBy('created_at', 'desc')->first();
    $bodyFlair = "";
    if(empty($old_val)){
        $old_cost = 0;
    }else{
        $old_cost = $old_val->cost;
    }
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    //updater has updated the indemnity value for injured person's injury from oldvalue to newvalue.
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string ."</span><span style='color:red;font-weight:bold;'>". $updater_name . "</span> has updated the Reserve cost for " . $injured_name . "'s injury from $" . $old_cost . " to $" . $current_val->cost; 
    $linkText = "<a href='" . $treeUrl . "'>Click here to review the injury in " . $brandApp . ".</a>";
    ?>
    @include('emailPartials.headline2EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection
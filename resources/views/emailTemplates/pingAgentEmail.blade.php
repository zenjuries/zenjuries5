@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "message";
	$headerImage = "attention";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Attention";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $url = url('/');
    $bodyFlair = "";
$company = \App\Company::where('id', $user->company_id)->first();
            $bodyText = "Someone just nudged you in " . $brandApp . "!"; 
            $linkText = "<a href='$url'> Click here to go to your Dashboard.</a>";
            
?>
@include('emailPartials.headline1EmailPartial')
    @include('emailPartials.commonTextEmailPartial')
    @endsection

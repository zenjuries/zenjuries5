@extends('layouts.emailLayout')
@section('emailcontent')
<?php
$brandCompany = env('COMPANY_NAME');
$headerBG = "green";
$headerImage = "resolved";
$headerTitle = "Claim Resolved!";
$bodyFlair = "contratulations";
$footerFlair = "medic";
$user = \App\User::where('id', $queuedMessage->user_id)->first();
$headlineText = "Hey " . strtok($user->name, " ");
$injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
$injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
$communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
$company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
$company_string = "";
$bodyFlair = "";
$treeUrl = url('communicationLink/currentInjury', [$injury->id]);
if($user->type !== "internal"){
    $company_string = $company_name . ": ";
}
$bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span><span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . '\'s</span> claim has been resolved! Great work everybody!';
 $linkText = '<a href="' . $treeUrl . '">Check"'. $brandCompany .'"for more info.</a>';
?>       
@include('emailPartials.headline1EmailPartial')
@include('emailPartials.commonTextEmailPartial')
@endsection
@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "purple";
	$headerImage = "teamupdate";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Team Update";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $zagent = \App\Company::where('id', $user->zagent_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $url = url('/');
    $bodyFlair = "";
    $teams = \App\SquadMember::where('user_id', $user->id)
            ->join('squads', 'squads.id', '=', 'squad_members.squad_id')
            ->join('companies', 'companies.id', '=', 'squads.company_id')
            ->select('squads.squad_name', 'companies.company_name')
            ->distinct()
            ->orderBy('companies.company_name', 'desc')
            ->get();
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $zagent->company_name . "</span> has selected you to replace another agent on their teams. Here's a list of your current teams: <br>";
    foreach($teams as $team){
        $bodyText .= $team->company_name . "'s team " . $team->squad_name . "<br>";
    }
    $linkText = "<a href='$url'> Click here to go to " . $brandApp . ".</a>";
    ?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.commonTextEmailPartial')
@endsection
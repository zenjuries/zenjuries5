@extends('layouts.emailLayout')
@section('emailcontent')
<?php
    $headerBG = "blue";
    $headerImage = "infoalert";
    $bodyFlair = "";
    $footerFlair = "alert";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Info Alert";

    $bodyText = "Weekly Update";
    $linkText = "";

    $injuries = $user->returnInjuryIDs();

    $new_injury_count = 0;
    $resolved_injury_count = 0;
    $update_count = 0;

    $one_week_ago = \Carbon\Carbon::today()->subWeek();

    $url = url("/weeklySummary");

    if(count($injuries) > 0){
        Log::debug(var_export($injuries, true));
        $bodyText = "Here is your injury activity since " . $one_week_ago->format("M d, Y") . ".";
        foreach($injuries as $injury){
            if($injury->created_at->gte($one_week_ago)){
                $new_injury_count++;
            }

            if($injury->resolved_at !== NULL && $injury->resolved_at->gte($one_week_ago)){
                $resolved_injury_count++;
            }

            if($injury->resolved !== 1 && count(\App\TreePost::where('injury_id', $injury->id)->where('created_at', '>', $one_week_ago)) > 0){
                $update_count++;
            }
        }

        if($new_injury_count > 0 || $resolved_injury_count > 0 || $update_count > 0){


            if($new_injury_count === 1){
                $bodyText = $bodyText . "<br><h3>You have <b>1</b> new injury.</h3>";
            }else{
                $bodyText = $bodyText . "<br><h3>You have <b>" . $new_injury_count . "</b> new injuries.</h3>";
            }

            if($resolved_injury_count === 1){
                $bodyText = $bodyText . "<h3>You have <b>1</b> resolved injury.</h3>";
            }else{
                $bodyText = $bodyText . "<h3>You have <b>" . $resolved_injury_count . "</b> resolved injuries.</h3>";
            }

            if($update_count === 1){
                $bodyText = $bodyText . "<h3>You have <b>1</b> injury with new tree posts.</h3>";
            }else{
                $bodyText = $bodyText . "<h3>You have <b>" . $update_count . "</b> injuries with new tree posts.</h3>";
            }
        }else{
            $bodyText = "<br><h3>No new updates! Making sure that a claim doesn't get ignored is important, so make sure your teams are reporting any changes!<h3>";
        }

    }else{
        $bodyText = "You currently don't have any active claims in " . $brandApp . "! Did you know you can also use our Injury Wizard to track past claims that are still in the process of being resolved?";
    }


        $linkText = "<a href='" . $url . "'>Click here to view your Weekly Summary in " . $brandApp . " for full details!</a> (You can always find this page from your Dashboard too!)";

        //$bodyText = $bodyText . "<br><h3>You have <b>" . $new_injury_count . "</b> "

?>

    Weekly Update
@endsection


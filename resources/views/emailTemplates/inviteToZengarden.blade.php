@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
    $headerBG = "red";
    $headerImage = "welcome";
    $bodyFlair = "";
    $footerFlair = "medic";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Welcome";

    $headlineText = "Hi!";

    $bodyText = "We're sorry to hear you've been injured in a work place accident.
                    Since your company has " . $brandCompany . ", you can rest assured that your company has a team hard at work handling your claim.
                    As an injured employee, you can use your " . $brandApp . " to let your company know how you're doing, get info related to your claim, and communicate with the team managing your injury.
                    Click the link below to finish creating your " . $brandApp . " account!";

    $linkText = "<a href=" . url('register?invitation='.$invitation->token) . ">" . url('register?invitation='.$invitation->token) . "</a><br>" .
                    "See you soon!";

    ?>
@endsection
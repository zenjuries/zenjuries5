@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "orange";
	$headerImage = "alert";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Alert";
	$footerFlair = "alert2";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
$injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
$injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
$treeUrl = url('communicationLink/currentInjury', [$injury->id]);
            $alertText = "NOTE: This communication was generated automatically due to an early claim being reported.
This communication is not meant to imply that the particular claim is fraudulent, nor does it intend to reflect negatively on the injured employee.
";

    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> A new injury was reported first thing in the morning.
This is the most frequent time fraudulent claims are reported, so you might want to give this one a closer look.";

$linkText = "<a href='" . $treeUrl . "'>Click here to review the injury in " . $brandApp . ".</a>";
?>
    @include('emailPartials.headline2EmailPartial')
    @include('emailPartials.alertTextEmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection






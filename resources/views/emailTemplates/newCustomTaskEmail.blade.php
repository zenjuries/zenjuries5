@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "blue";
	$headerImage = "newtask";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "New Request";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $request_communication = \App\ActionRequest::where('communication_id', $communication->id)->first();
            Log::debug(var_export($request_communication, true));
    $request = \App\Action::where('id', $request_communication->action_id)->first();
            Log::debug(var_export($request, true));
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> You've been assigned to a new request for <span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "'s</span> Injury.
<br>" .
    "<b>Request Name: </b>" . $request->name . "<br>" .
    "<b>Request Description: </b>" . $request->request_body . "<br>" .
    "<b>Due Date: </b>" . (is_null($request_communication->due_on) ? "None" : $request_communication->due_on) . "<br>";
    $linkText = "<a href='$treeUrl'> Click here to review the injury in " . $brandApp . ".</a>";
    ?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection

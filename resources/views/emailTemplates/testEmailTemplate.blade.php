@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
        $headerBG = "cyan";
        $bodyFlair = "";
        $headerImage = "updatestatusrequest";
        $brandCompany = env('COMPANY_NAME');
        $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Test Email";
        $linkText = "";
        $footerFlair = "medic";
        $headlineText = "Test Email";
        $bodyText = "Test email sent from support.";
    ?>
@include('emailPartials.headline2EmailPartial')
@include('emailPartials.zenfuciusCommentEmailPartial')
@endsection
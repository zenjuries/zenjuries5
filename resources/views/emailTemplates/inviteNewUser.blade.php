@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
    $headerBG = "red";
    $headerImage = "welcome";
    $bodyFlair = "";
    $footerFlair = "alert";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Welcome";
    $headlineText = "Welcome to ". $brandCompany . "!";
    $team = \App\Team::where('id', $invitation->team_id)->first();
    $team_owner = \App\User::where('id', $team->owner_id)->first();

    $bodyText = $team_owner->name . " has invited you to join " . $team->name . " in " . $brandApp . "! If you do not already have an account,
    you may click the following link to get started:";

    $linkText = "<a href=" . url('userRegister?invitation='.$invitation->token) . ">" . url('userRegister?invitation='.$invitation->token) . "</a>" .
        "<br>See you soon!<br> Note: To make sure you receive all emails from " . $brandCompany . ", please add us to your contact list!"

    ?>
@endsection
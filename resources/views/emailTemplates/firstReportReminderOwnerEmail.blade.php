@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "orange";
	$headerImage = "firstreportreminder";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Injury Alert Reminder";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
	$injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
	$injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
	$firstReport = \App\FirstReportOfInjury::where('injury_id', $injury->id)->first();
	$treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
$bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> This is an alert that the Injury Alert for <span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "'s</span> injury has NOT been filed. " . $firstReport->number_of_reminders_sent . " reminders have been sent related to this claim.
The Injury Alert needs to be filed with the insurance company IMMEDIATELY, and a copy should be kept for your company's personal records.
Once you have filed it with the insurance company, please confirm it has been delivered <a href='" . $treeUrl . "'>in the tree of communication.</a>
    Swift filing of the Injury Alert reduces the cost of your company's claim significantly and helps get your injured employee the attention they deserve as quickly as possible!";
$linkText = "";
?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection



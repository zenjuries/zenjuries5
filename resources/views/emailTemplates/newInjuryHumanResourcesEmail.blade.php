@extends('layouts.emailLayout')
@section('emailcontent')
<?php
	$headerBG = "red";
	$headerImage = "newinjury";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Role Assignment";
    $bodyFlair = "4navigator";	
	$footerFlair = "medic";	    
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string ."</span><span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "</span> has been hurt. You've been hand-picked to serve as the <b>Chief Navigator</b> on this injury.
<span style='display:block;font-style:italic;padding-top:10px;'>Your duties will include:</span>

<ol style='margin-top:10px;'>
<li style='padding-bottom:4px;'>Filing the claim with insurance company (the \"Injury Alert\").</li>
<li style='padding-bottom:4px;'>Entering as much \"claim information\" as you can into " . $brandApp . "</li>
<li style='padding-bottom:4px;'>Setting up positive expectations with injured employee (confirm delivery of \"Employee Expectations\" document).</li>
<li style='padding-bottom:4px;'>Adding appropriate/needed/desired \"team members\" to the " . $brandCompany . " team.</li>
<li style='padding-bottom:4px;'>Asking for help when needed from other team members... that's why they're there.</li>
<li style='padding-bottom:4px;'>Communicate with adjuster once a week for latest update and report to " . $brandApp . " through tree of communication.</li>
</ol>

For your first step, make sure the Injury Alert is filed with your insurance carrier as quickly as possible!";

	$linkText = "<a href='$treeUrl'> Click here to review the injury in " . $brandApp . ".</a>";

?>

    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection

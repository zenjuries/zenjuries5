@extends('layouts.emailLayout')
@section('emailcontent')

    <?php
    $brandCompany = env('COMPANY_NAME');
	$headerBG = "purple";
	$headerImage = "addteam";
    $headerTitle = "Added to a Team";
    $bodyFlair = "4pt";	
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $zenboardLink = url('/communicationLink/zenboard/' . $communication->company_id);
    $company_string = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
            $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> You've been added to one of <span style='color:red;font-weight:bold;'>" . $company_name . "'s</span> teams as a <b>Physical Therapist</b>.
As a $brandCompany team member, your actions will be crucial in ensuring that the company's injured employees have the most pleasant experience possible.
As a <b>Physical Therapist</b>, your communication with the rest of the Zenjuries team will help them provide the best support possible for their injured Employee!
<span style='display:block;font-style:italic;padding-top:10px;'>Your duties will include:</span>
<ol style='margin-top:10px;'>
<li style='padding-bottom:4px;'>Communicating with your team's <b>Chief Navigator</b> and deliver original diagnosis</li>
<li style='padding-bottom:4px;'>Communicating follow up appointment time and results</li>
<li style='padding-bottom:4px;'>Communicating ongoing recommendations and treatments</li>
</ol>
If you follow these steps, Zenjuries will make handling your workers comp claims as painless as possible!<br>";
$linkText = "<a href='" . $zenboardLink . "'>Click here to go to your dashboard!</a>";
?>

    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')

@endsection


@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "cyan";
	$headerImage = "updatestatusrequest";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Update Status Request";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    if(is_null($injury->employee_status)){
         $status_string = "Please update the Employee Status for $injuredEmployee->name's injury in " . $brandApp . ". ";
    }else{
         $status_string = "Please make sure the Employee Status for $injuredEmployee->name's injury is up to date in " . $brandApp . ".
                          Their current status is <b>\"$injury->employee_status\"</b>. ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string ."</span>". $status_string . " You may do this from the Injury Progress section of that injury's Tree of Communications page.
    <br>NOTE: If the employee's status has not changed since the last update, please disregard this email. ";
     $linkText = "<a href='" . $treeUrl . "'>Click here to review the injury in " . $brandApp . ".</a>";
    ?>
    @include('emailPartials.headline2EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection
@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "cyan";
	$headerImage = "inforequest";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Adjuster Info Request";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_name . ": </span> ";
        $internal_user_string = "";
    }else{
        $internal_user_string = " You can find this information by contacting your insurance carrier or agent.";
    }
    $injured_user_name = \App\User::where('id', $injury->injured_employee_id)->value('name');

    $headlineText = "Hey " . strtok($user->name, " ");

    $bodyText =  $company_string . " Please update <span style='color:red;font-weight:bold;'>" . $injured_user_name . "'s </span> claim with the <b>claim adjuster's</b> contact info. You may do so from that injury's Tree of Communications page.";
    $linkText = "<a href='" . $treeUrl . "'>Click here to go to the Tree of Communications Page.</a>" . $internal_user_string;
    ?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.welcomeBoxEmailPartial')
@endsection
@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "yellowgreen";
	$headerImage = "bestpractices";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Best Practices";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> Have you spoken to <span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "</span> this week?
    Frequent communication with the injured employee is critical to making them feel cared for and helping them return to work as quickly as possible.
    Not only will a speedy return to work help them feel less alone, it has been proven to significantly reduce claim costs.
     After you check on them, use the Tree of Communications to let your teammates know how they're doing.";
     $linkText = "<a href='" . $treeUrl . "'>Click here  to review the injury in " . $brandApp . ".</a>";
    ?>
    @include('emailPartials.headline2EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection
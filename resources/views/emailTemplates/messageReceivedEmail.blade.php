@extends('layouts.emailLayout')
@section('emailcontent')
<?php
	$headerBG = "message";
	$headerImage = "newmessage";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "New Message";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $bodyFlair = "";
if($user->injured == 1 && $user->type === "internal"){
    $url = route('zengardenMessages');
}else{
    $url = route('communicationLink.messages');
}
$bodyText = "You've received a new message in " . $brandApp . ".";
$linkText = "<a href='" . $url . "'>Click here to view your messages.</a>";
?>
@include('emailPartials.headline1EmailPartial')
    @include('emailPartials.commonTextEmailPartial')
    @endsection

@extends('layouts.emailLayout')
@section('emailcontent')

<?php
	$headerBG = "red";
	$headerImage = "newinjury";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Role Assignment";
    $bodyFlair = "4customerservice";	
	$footerFlair = "medic";	    
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string ."</span><span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "</span> has been hurt. You've been hand-picked to serve as the <b>Customer Service Agent</b> on this injury.
<span style='display:block;font-style:italic;padding-top:10px;'>Your duties will include:</span>

<ol style='margin-top:10px;'>
<li style='padding-bottom:4px;'>Opening communication with your team's <b>Chief Navigator</b> and setting expectations regarding insurance company procedures.</li>
<li style='padding-bottom:4px;'>Making sure the Injury Alert and Official First Report of Injury are filed with insurance company.</li>
<li style='padding-bottom:4px;'>Finding out who the <b>Claim Manager</b> is on this claim and reporting it to the <b>Chief Navigator</b>.</li>
<li style='padding-bottom:4px;'>Notifying producing Work Comp Agent on account.</li>
</ol>

For your first step, get in touch with the <b>Chief Navigator</b> (<i>through " . $brandCompany . "!</i>) of the injured employee's company.
<br><br>
Your actions will be crucial in ensuring that " .strtok($injuredEmployee->name, " ") . " receives the best care possible!";
    
	$linkText = "<a href='$treeUrl'> Click here to review the injury in " . $brandApp . ".</a>";
?>

    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection

@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "cyan";
	$headerImage = "inforequest";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Info Request";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $zenboard = url('communicationLink/admin/' . $communication->company_id);
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> Please enter your company's Policy Number, Renewal Date, and Carrier Info.
Your Policy Number and Carrier Info is required to make sure your claims get immediate attention when they're reported.
If you give us your Renewal Date, we'll remind you to update your Policy information in " . $brandApp . " when that date occurs.";
$linkText = "<a href='" . $zenboard . "'>Click Here to go to your company page in " . $brandApp . " to update this info.</a>";
?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.commonTextEmailPartial')
    @endsection

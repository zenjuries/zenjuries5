@extends('layouts.emailLayout')
@section('emailcontent')

    <?php
	$headerBG = "orange";
	$headerImage = "firstreportreminder";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Injury Alert Reminder";
	$footerFlair = "firstreportreminder";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
	$injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
	$injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
	$firstReport = \App\FirstReportOfInjury::where('injury_id', $injury->id)->first();
	$treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
$bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> Here's a reminder about filing the Injury Alert for <span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "'s</span> injury. It needs to be filed with the insurance company IMMEDIATELY, and a copy should be kept for your company's personal records.
Once you've filed it with the insurance company, please confirm it has been delivered <a href='" . $treeUrl . "'>in the tree of communication.</a>
    Until you've confirmed this, you will receive periodic reminders.
    Swift filing of the Injury Alert has been shown to reduce costs up to 47%!
    Reminder number " . $firstReport->number_of_reminders_sent . ".";
$linkText = "";
?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
    @endsection




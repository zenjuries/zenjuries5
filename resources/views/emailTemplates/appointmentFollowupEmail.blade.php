@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
    $brandCompany = env('COMPANY_NAME');
	$headerBG = "black";
	$headerImage = "followup";
    $headerTitle = "Followup";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $appointment = \App\Appointment::where('id', $communication->extra_data)->first();
    $appointment_location = \App\InjuryCareLocation::where('id', $appointment->location_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    //2018-03-01 13:00:00
    $appointment_time = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $appointment->appointment_time)->toDayDateTimeString();
    $company_string = "";
    $bodyFlair = "";
    Log::debug(var_export($queuedMessage, true));
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }

    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> This is a follow-up for <span style='color:red;font-weight:bold;'>$injuredEmployee->name's</span> appointment on $appointment_time at $appointment_location->name.
    Please upload any <b>doctors</b> notes or invoices from the appointment to the injury's Tree of Communications Page.
     If a follow-up appointment has already been scheduled, please make sure it has been entered into $brandCompany. Thank you!";
     $linkText = "<a href='$treeUrl'>Click here to go to the Tree of Communications Page.</a>";
     /*
    if(is_null($injury->employee_status)){
        $status_string = "Please update the Employee Status for $injuredEmployee->name's injury in Zenjuries. ";
    }else{
        $status_string = "Please make sure the Employee Status for $injuredEmployee->name's injury is up to date in Zenjuries.
                          Their current status is <b>\"$injury->employee_status\"</b>. ";
    }

    if($user->id !== $injuredEmployee->id){
        $bodyText = $company_string . "$injuredEmployee->name has a new medical appointment scheduled for $appointment_time at $appointment_location->name.
         Please make sure that the employee is aware and will attend. We will send out reminders the day before the appointment. " .
            "Click <a href='" . $treeUrl . "'>here</a> to review the injury in Zenjuries.";
    }else{
        $bodyText = "A new medical appointment has been scheduled for you in Zenjuries! The appointment is scheduled for $appointment_time at $appointment_location->name.
        We will send you another reminder the day before the appointment. <a href='/'>Click here to go to Zenjuries now.</a> Thanks!";
    }
    */
    ?>


    @include('emailPartials.headline2EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection

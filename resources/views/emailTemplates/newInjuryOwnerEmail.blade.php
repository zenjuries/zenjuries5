@extends('layouts.emailLayout')
@section('emailcontent')

<?php
	$headerBG = "red";
	$headerImage = "newinjury";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Role Assignment";
	$bodyFlair = "4admin";	
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string ."</span><span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "</span> has been hurt. You've been hand-picked to serve as the <b>Chief Executive</b> on this injury.
<span style='display:block;font-style:italic;padding-top:10px;'>Your duties will include:</span>

<ol style='margin-top:10px;'>
<li style='padding-bottom:4px;'>Letting the employee know you care</li>
<li style='padding-bottom:4px;'>Keeping the entire team accountable</li>
<li style='padding-bottom:4px;'>Demonstrating to your team through your actions how important the injured employee's care is to you</li>
<li style='padding-bottom:4px;'>Directly encouraging your injured employee at least once a month by phone, email, or text</li>
</ol>

For your first step, send some encouragement to your injured employee and then check with your <b>Chief Navigator</b> to make sure the <i>First Report of Injury</i> is properly handled.";
            
    $linkText = "<a href='$treeUrl'> Click here to review the injury in " . $brandApp . ".</a>";  
?>          
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection

@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
    $brandCompany = env('COMPANY_NAME');
	$headerBG = "blue";
	$headerImage = "attention";
    $headerTitle = "Attention!";
	$footerFlair = "alert";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span><span style='color:red;font-weight:bold;'>". $injuredEmployee->name . "'s</span> claim has been stopped in $brandCompany due to litigation.
    You will be unable to make any changes to the claim in $brandCompany during this time, and the injured employee will no longer have access to their Zengarden. If this was done by accident please contact support at $brandCompany with the name of the injured employee.";
    $linkText = "<a href='" . $treeUrl . "'>Click here to review the injury in $brandCompany.</a>";
    ?>


    @include('emailPartials.headline2EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection
@extends('layouts.emailLayout')
@section('emailcontent')

    <?php
    $headerBG = "red";
    $headerImage = "alert";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Alert";
    $bodyFlair = "";
    $footerFlair = "alert";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $unsubscribed_user = \App\User::where('id', $communication->extra_data)->first();
    $company_string = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string ."</span>
        This is a notification that <span style='color:red;font-weight:bold;'>" . $unsubscribed_user->name . "</span> has unsubscribed from email alerts.
         High-Priority alerts such as new injuries being reported will still be delivered, but other notifications such as information on ongoing claims will be disabled.";

    $linkText = "As the admin for this company, you can edit users' email settings from the <a href='/zenjuries_company'>Company</a> page in " . $brandApp . ".";


    ?>

    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection
@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "green";
	$headerImage = "costupdate";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Final Cost Update";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injured_name = \App\User::where('id', $injury->injured_employee_id)->value('name');
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $company_name = \App\Company::where('id', $injury->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = "<span style='color: blue'><b>" . $company_name . "</b></span>: ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> The Final Cost for <span style='color:red;font-weight:bold;'>" . $injured_name . "'s</span> claim has been updated to <b>$" . $injury->final_cost . "</b>.";
     $linkText = "<a href='" . $treeUrl . "'>Click here to review the injury in " . $brandApp . ".</a>";
    ?>
    @include('emailPartials.headline2EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection
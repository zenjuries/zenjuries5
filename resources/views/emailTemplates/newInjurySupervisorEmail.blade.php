@extends('layouts.emailLayout')
@section('emailcontent')

<?php
	$headerBG = "red";
	$headerImage = "newinjury";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Role Assignment";
    $bodyFlair = "4supervisor";	
	$footerFlair = "medic";	    
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string ."</span><span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "</span> has been hurt. You've been hand-picked to serve as the <b>Supervisor</b> on this injury.
<span style='display:block;font-style:italic;padding-top:10px;'>Your duties will include:</span>

<ol style='margin-top:10px;>
<li style='padding-bottom:4px;'>Helping deliver employee to prompt medical care</li>
<li style='padding-bottom:4px;'>Setting and delivering employee expectations document to help employee not be afraid</li>
<li style='padding-bottom:4px;'>Talking to the <b>Chief Navigator</b> and fully disclosing all aspects of injury occurrence and diagnosis</li>
<li style='padding-bottom:4px;'>Communicating with employee at least once a day for first five days (\"How are you?\")</li>
<li style='padding-bottom:4px;'>Adding updates to " . $brandApp . "</li>
</ol>

For your first step, make sure the injured employee has gotten the proper medical attention they need, and update their <i>Email and Phone</i> on the Injury Details tab of the <i>Tree of Communications</i>.";

	$linkText = "<a href='$treeUrl'> Click here to review the injury in " . $brandApp . ".</a>";
?>

    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection


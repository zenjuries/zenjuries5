@extends('layouts.emailLayout')
@section('emailcontent')
<?php
$headerBG = "yellowgreen";
$headerImage = "learning";
$brandCompany = env('COMPANY_NAME');
$brandApp = env('BRAND_APP_NAME');
$headerTitle = "Educational Material";
$footerFlair = "books";
$user = \App\User::where('id', $queuedMessage->user_id)->first();
$company = \App\Company::where('id', $user->company_id)->first();
//$edMats = \App\EducationalMaterial::get();
$edMats = DB::table('educational_material')->get();
$count = 1;
$headlineText = "";
$bodyText = "";
$bodyFlair = "";
$linkText = "";
foreach($edMats as $edMat){
		
        if($count == $company->educational_material_number){
            $headlineText = $edMat->title;
            $bodyText = $edMat->body;
            break;
        }
    $count++;
    
    }
?>
@include('emailPartials.headline2EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
    @endsection

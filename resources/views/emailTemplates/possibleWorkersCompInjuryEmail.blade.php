@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "orange";
	$headerImage = "alert";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Alert";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();

    $headlineText = "Hey " . strtok($user->name, " ");

    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();

    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();

    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();

    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
	$bodyFlair = "";
    $company_string = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> It's been 3 days since <span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "'s</span> claim was reported as a \"First Aid\" injury, and it still hasn't been resolved.
If an injury should be classified as First Aid, it usually will be resolved in the first 48 hours.
If this claim isn't ready to be resolved, statistics show that it should be classified as a Moderate to Severe injury.
If you think this might not be a First Aid injury take action IMMEDIATELY so that your injured employee will get the care they require!";
$linkText = "<a href='" . $treeUrl . "'>Click here to go to the Tree of Communications page</a>";
?>

@include('emailPartials.headline2EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
    @endsection

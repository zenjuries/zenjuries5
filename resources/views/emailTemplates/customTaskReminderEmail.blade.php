@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
    $brandCompany = env('COMPANY_NAME');
	$headerBG = "blue";
	$headerImage = "taskreminder";
    $headerTitle = "Task Reminder";
	$footerFlair = "medic";   
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    
    $action_id = \App\ActionRequest::where('reminder_communication_id', $queuedMessage->communication_id)->value('action_id');
    
    $action = \App\Action::where('id', $action_id)->first();
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $days_remaining_string = "";
	
    if(!is_null($action->due_on)){
        $today = \Carbon\Carbon::today();
        $due_date = \Carbon\Carbon::createFromFormat("Y-m-d", $action->due_on)->startOfDay();
        $days_until_due = $due_date->diffInDays($today);
        if($days_until_due === 1){
            $days_remaining_string = "Please note that this request is due in " . $days_until_due . " day!";
        }else{
            $days_remaining_string = "Please note that this request is due in " . $days_until_due . " days!";
        }
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> Here's a reminder for the request for <span style='color:red;font-weight:bold;'>" . $injuredEmployee->name . "'s</span> Injury<br>" .
            "<b>Request Name: </b>" . $action->name . "<br>" .
            "<b>Request Description: </b>" . $action->request_body . "<br>" .
            "<b>Due Date: </b>" . (is_null($action->due_on) ? "None" : $action->due_on) . "<br>" .
            $days_remaining_string;
            $linkText = "<a href='$treeUrl'> Click here to review the injury in $brandCompany .</a>";
            
    ?>
       @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection
@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "message";
	$headerImage = "newmessage";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "New Message";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    $post = \App\TreePost::where('id', $communication->extra_data)->first();
    $poster = \App\User::where('id', $post->user_id)->first();
    if($user->type !== "internal"){
        $company_string = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_name . ":</span> ";
    }
    $injured_user_name = \App\User::where('id', $injury->injured_employee_id)->value('name');

    $headlineText = "Hey " . strtok($user->name, " ");

    $bodyText =  $company_string . "</span> There has been a new Tree of Communications post for <span style='color:red;font-weight:bold;'>" . $injured_user_name . "'s </span> injury: " .
        "<br><b>From: " . $poster->name . "</b><br>" . "\"" . $post->body . "\"";

    $linkText = "<a href='" . $treeUrl . "'>Click here to go to the Tree of Communications page now!</a>";
    ?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.welcomeBoxEmailPartial')
@endsection
@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "black";
	$headerImage = "newappointment";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "New Appointment";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $appointment = \App\Appointment::where('id', $communication->extra_data)->first();
    $appointment_location = \App\InjuryCareLocation::where('id', $appointment->location_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    //2018-03-01 13:00:00
    $appointment_time = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $appointment->appointment_time)->toDayDateTimeString();
    $company_string = "";
    $bodyFlair = "";
    Log::debug(var_export($queuedMessage, true));
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    /*
    if(is_null($injury->employee_status)){
        $status_string = "Please update the Employee Status for $injuredEmployee->name's injury in Zenjuries. ";
    }else{
        $status_string = "Please make sure the Employee Status for $injuredEmployee->name's injury is up to date in Zenjuries.
                          Their current status is <b>\"$injury->employee_status\"</b>. ";
    }
    */
    if($user->id !== $injuredEmployee->id){
        $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span><span style='color:red;font-weight:bold;'> $injuredEmployee->name</span> has a new medical appointment scheduled for $appointment_time at $appointment_location->name.
         Please make sure that the employee is aware and will attend. We will send out reminders the day before the appointment. ";
          $linkText = "<a href='" . $treeUrl . "'>Click here to review the injury in " . $brandApp . ".</a>";
    }else{
        $bodyText = "A new medical appointment has been scheduled for you in " . $brandApp . "! The appointment is scheduled for $appointment_time at $appointment_location->name.
        We will send you another reminder the day before the appointment.";
        $linkText = "<a href='/'>Click here to go to " . $brandApp . " now.</a>";
    }
    ?>
    @include('emailPartials.headline2EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection

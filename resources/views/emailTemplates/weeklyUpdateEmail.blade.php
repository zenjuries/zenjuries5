<?php
$headerBG = "red";
$headerImage = "injuryupdate";
$brandCompany = env('COMPANY_NAME');
$brandApp = env('BRAND_APP_NAME');
$headerTitle = "Injury Update";
$footerFlair = "medic";
$user = \App\User::where('id', $queuedMessage->user_id)->first();
$injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
$injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
$bodyFlair = "";
$bodyText = "Hey strtok($user->name,<br>
There have been X new updates for <span style='color:red;font-weight:bold;'><?php echo $injuredEmployee->name; ?>'s</span> claim since last week.";
$linkText = "<a href='#'> Click here to view them!</a>";
?>
<!--Hey <?php echo strtok($user->name, " "); ?>,
<!-- set this up better once we get the weekly updates figured out in the back end
There have been X new updates for <?php echo $injuredEmployee->name; ?>'s claim since last week.
Click here to view them!-->

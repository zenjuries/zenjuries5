@extends('layouts.emailLayout')
@section('emailcontent')
<?php
	$headerBG = "cyan";
	$headerImage = "costupdaterequest";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Final Cost Update Request";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_name . ": </span> ";
    }
    $reminder_string = "";
    if($injury->final_cost_reminders == 4){
        $reminder_string = "This will be your last reminder for this injury.";
    }

    $injured_user_name = \App\User::where('id', $injury->injured_employee_id)->value('name');

    $headlineText = "Hey " . strtok($user->name, " ");

    $bodyText =  $company_string . " Please update the final cost of <span style='blue'>" . $injured_user_name . "'s </span> claim in " . $brandApp . ". You may do so from that injury's Tree of Communications page.";
    $linkText = "<a href='" . $treeUrl . "'>Click here to go to the Tree of Communications page</a> " . $reminder_string;

    ?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.welcomeBoxEmailPartial')
@endsection
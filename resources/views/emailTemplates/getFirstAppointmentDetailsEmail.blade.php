@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "black";
	$headerImage = "appointmentupdates";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Appointment Info Request";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $injuredEmployee = \App\User::where('id', $injury->injured_employee_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    //2018-03-01 13:00:00
    $company_string = "";
    $bodyFlair = "";
    Log::debug(var_export($queuedMessage, true));
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span>Please add the details for <span style='color:red;font-weight:bold;'>$injuredEmployee->name's</span> medical appointments related to this injury.
    Details about past appointments should be entered, as well as any future appointments that have been scheduled.
    This will help the rest of the team monitor the progress of the injured employee.";
    $linkText = "<a href='" . $treeUrl . "'>Click here  to review the injury in " . $brandApp . ".</a>";

    ?>
    @include('emailPartials.headline2EmailPartial')
    @include('emailPartials.zenfuciusCommentEmailPartial')
@endsection

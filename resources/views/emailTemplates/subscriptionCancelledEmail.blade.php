@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
	$headerBG = "red";
	$headerImage = "subcancelled";
    $brandCompany = env('COMPANY_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $headerTitle = "Subscription Cancelled";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $headlineText = "Hey " . strtok($user->name, " ");
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $team_id = \App\Company::where('id', $communication->company_id)->value('team_id');
    $team = \App\Team::where('id', $team_id)->first();
    $owner_id = \App\TeamUser::where('team_id', $team_id)->where('role', 'owner')->first();
    $owner_name = \App\User::where('id', $owner_id)->first();
    $billingLink = url('settings/teams/' . $team_id . '#/subscription');
    $company_string = "";
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = $company_name . ": ";
    }
    if($user->id === $owner_id->user_id){
        $owner_string = "You may resume your subscription by going to <a href='" . $billingLink . "'>billing</a>.
        If you did not mean to cancel your subscription, it probably means we had trouble processing your most recent payment.
        You may update your card info on billing page.";
    }else{
        $owner_string = "";
    }

    $bodyText = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_string . "</span> This is an alert that your " . $brandCompany . " subscription has been cancelled.
     You will still have access to " . $brandCompany . " for the remainder of the billing period, which will end on " . $team->grace_period_ends . ", at which point your account will be suspended.
     " . $owner_string;
     $linkText = "";
    ?>
    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.commonTextEmailPartial')
@endsection
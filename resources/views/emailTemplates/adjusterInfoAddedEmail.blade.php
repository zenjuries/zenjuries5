@extends('layouts.emailLayout')
@section('emailcontent')
    <?php
    $brandCompany = env('COMPANY_NAME');
	$headerBG = "purple";
	$headerImage = "teamupdate";
    $headerTitle = "Team Update";
	$footerFlair = "medic";
    $user = \App\User::where('id', $queuedMessage->user_id)->first();
    $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
    $treeUrl = url('communicationLink/currentInjury', [$injury->id]);
    $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
    $company_name = \App\Company::where('id', $communication->company_id)->value('company_name');
    $company_string = "";
    $bodyFlair = "";
    if($user->type !== "internal"){
        $company_string = "<span style='display:block;font-weight:bold;font-size:16px;padding-bottom:10px;'>" . $company_name . ":</span> ";
    }
    $injured_user_name = \App\User::where('id', $injury->injured_employee_id)->value('name');
    $headlineText = "Hey " . strtok($user->name, " ");
    $bodyText =  $company_string . " The claim adjuster's contact info has been added for <span style='color:red;font-weight:bold;'>" . $injured_user_name . "'s </span> claim. <br>" .
            "Name: " . $injury->adjuster_name . "<br>" .
            "Phone: " . (is_null($injury->adjuster_phone) ? 'Not Entered' : $injury->adjuster_phone) . "<br>" .
            "Email:" . (is_null($injury->adjuster_email) ? 'Not Entered' : $injury->adjuster_email) . "<br>" .
            "Check the Tree of Communications for more info. An updated copy of the Medical Information Document for this injury has been attached.";
    $linkText = "<a href='" . $treeUrl . "'> Click here to Check the Tree of Communications now!</a>";
    ?>


    @include('emailPartials.headline1EmailPartial')
    @include('emailPartials.welcomeBoxEmailPartial')
@endsection
<script>
    $('.shortStats').on('click', function(e){
        console.log(e.target);
        if(e.target == this){
            toggleStatsBarSetting();
        }
    });

    function toggleStatsBarSetting(){
        $.ajax({
            type: 'POST',
            //naming our routes lets us use laravel's route method to easily pass the url into the ajax function
            url: '<?php echo route('toggleStatsBarSetting'); ?>',
            data: {
                //the _token must be included with all ajax requests to get them through laravel's security,
                // it basically identifies that the request came from within the site
                _token: '<?php echo csrf_token(); ?>'
                //normally there would be some other data here but we'll just get the user using the Auth::user() method on the controller,
                //so we don't need to send that
            },
            success: function(data){
                console.log('success')
                //do whatever code on success (in this case probably toggling the stats bar state)
            },
            error: function(data){
                console.log(data)
                //useful debugging tool. if it gives you a 500 error in the console your problem is most likely routes/controller/model related.
                //server-side validation errors will also show up in the error function, so this is where you'd handle those if you were running validation

            }

        })
    }
</script>
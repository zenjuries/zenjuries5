@extends('layouts.zen5_layout')
@section('maincontent')
<?php 
 $popmessage = array("team", "confirm save template", "message body", "green", "confirmSaveTemplate", "team", "save");
 $popmessage2 = array('gears', 'Delete User', 'Are you sure you want to delete user?', 'red cancelDeletion', '', 'times', 'Cancel', 'green', 'deleteUser', 'removeuser', 'Delete User');

$is_zagent = \App\Company::where('id', Auth::user()->getCompany()->id)->value('is_agency');
$zagency_name = "Agency";
$agency_type = "ZAgent";
if(!is_null(Auth::user()->zagent_id)){
    $company = \App\Company::where('id', Auth::user()->zagent_id)->first();
    $zagency_name = $company->company_name;
	$agency = \App\Agency::where('id', $company->agency_id)->first();
}

$company_id = Auth::user()->getCompany()->id;

$load_squad = \App\Squad::where('company_id', $company_id)->where('injury_squad', 0)->value('id');

$company = \App\Company::where('id', $company_id)->first();
$has_zenpro = $company->has_zenpro;
if($has_zenpro){
    $zenpro_id = $company->getZenproId();
}else $zenpro_id = NULL;
?>
<div class="pageContent bgimage-bgheader pagebackground1 fade-in">
		<!--***********-->		
	<div class="headerBlock noTitle">
		<!--
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>users / teams</span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/teammessages.png"></span><span class="subContent">manage your users and teams</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/team-bright.png">
            </div> 
        </div>-->           
	</div>

<!-- this script controls the tabs at the top -->	
<script>
$(document).ready(function() {
	 
    $('#zenispheretabs').addClass('activeTeams');    
    $('#activeTeamsTab').show();
	$('#zenispheretabs').on('click', 'li', function(){
		var tab_class = $(this).attr('class');
		var target = $(this).data('target');
		$('#zenispheretabs').removeClass().addClass(tab_class);
		$('.zenispheretabContent').hide();
		$('#' + target).show();
		
	});     
});    
</script>

	<div class="contentBlock noPad">
		<div class="container noPad">

		<div id="zenispheretabs">
			<ul>
				<li class="manageTeams" data-target="manageTeamsTab"><a class="tabManageTeams"><div class="icon icon-teams"></div><span>team templates</span></a></li>
				<li class="activeTeams" data-target="activeTeamsTab"><a class="tabActiveTeams"><div class="icon icon-check"></div><span>active teams</span></a></li>				
				<li class="manageUsers" data-target="manageUsersTab"><a class="tabManageUsers"><div class="icon icon-user"></div><span>user management</span></a></li>
			</ul>
		</div>
			<div class="zenispheretabContent" id="manageTeamsTab">
			<div class="teamPicker">
					<div class="buttonArray">
						<button class="showZenModal black" data-modalcontent="#show_team_templates"><div class="icon icon-cog"></div> choose a template</button>
						<button id="resetTemplate" class="cyan centered"><div class="icon icon-plus-circle"></div> new template</button>
                    </div>
				</div>
				<div class="teamStats">
					<div class="teamTitle"><span id="currentTeamTemplateTitle">template title</span></div>
				</div>
				<br>
				<div class="rosterBox">
					<div class="teamRosterContainer">
						<div class="teamRoster teamTemplate">
							<div class="fullTeamRoster" id="fullTeamLayout">
								<div class="teamRosterBG"><div class="circleOfCare">
									<div class="teamManagementControls">
										<button id="saveTeam" class=" ghost"><div class="icon icon-floppy-o"></div> save</button><br>
										<button class="showZenModal" data-modalcontent="#renameTeamModal"><div class="icon icon-pencil-square-o"></div> rename</button><br>
										<!--
										<button class="red"><div class="icon icon-times-circle"></div> delete</button><br>
										<button><div class="icon icon-thumb-tack"></div> default</button><br>
										-->
									</div>
								</div></div>

								<div class="teamRosterLeft">
									<div class="teamElementsLeft">

										<div class="teamElement">
											<div class="element tp1">
												<div class="teamSetPanel tp1">
													<div id="new_chief_executive" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-exec"></div>
														<div class="addTeamUser"><button data-title="Chief Executive" data-modalcontent="#add_user_to_team" data-roleType="roleRequired" data-role="1" class="twoline roleRequired showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> chief<br>executive</button></div>
														<div class="teamMemberIcon templateDelay1"><div class="helpAvatar" style="background-color:var(--roleRequired);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>							
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp2">
												<div class="teamSetPanel tp2">
													<div id="new_safety_manager" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-rehab"></div>
														<div class="addTeamUser"><button data-title="Safety Manager" data-modalcontent="#add_user_to_team" data-roleType="roleSuggested" data-role="2" class="twoline roleSuggested showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> safety<br>manager</button></div>
														<div class="teamMemberIcon templateDelay2"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp3">
												<div class="teamSetPanel tp3">
													<div id="new_employee_supervisor" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-exclamation-triangle"></div>
														<div class="addTeamUser"><button data-title="Employee Supervisor" data-modalcontent="#add_user_to_team" data-roleType="roleSuggested" data-role="3" class="twoline roleSuggested showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> employee<br>supervisor</button></div>
														<div class="teamMemberIcon templateDelay3"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp4">
												<div class="teamSetPanel tp4">
													<div id="new_physical_therapist" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-rehab"></div>
														<div class="addTeamUser"><button data-title="Physical Therapist" data-modalcontent="#add_user_to_team" data-roleType="roleOptional" data-role="4" class="twoline roleOptional showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> physical<br>therapist</button></div>
														<div class="teamMemberIcon templateDelay4"><div class="helpAvatar" style="background-color:var(--roleOptional);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp5">
												<div class="teamSetPanel tp5">
													<div id="new_medical_support" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-doctor"></div>
														<div class="addTeamUser"><button  data-title="Medical Support" data-modalcontent="#add_user_to_team" data-roleType="roleOptional" data-role="5" class="twoline roleOptional showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> medical<br>support</button></div>
														<div class="teamMemberIcon templateDelay5"><div class="helpAvatar" style="background-color:var(--roleOptional);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>

								<div class="teamRosterRight">
									<div class="teamElementsRight">

										<div class="teamElement">
											<div class="element tp6">
												<div class="teamSetPanel tp6">
													<div id="new_human_resources" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-reuser"></div>
														<div class="addTeamUser"><button data-title="Human Resources" data-modalcontent="#add_user_to_team" data-roleType="roleRequired" data-role="6" class="twoline roleRequired showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> human<br>resources</button></div>
														<div class="teamMemberIcon templateDelay10"><div class="helpAvatar" style="background-color:var(--roleRequired);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>							
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp7">
												<div class="teamSetPanel tp7">
													<div id="new_claim_manager" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-injuryreport"></div>
														<div class="addTeamUser"><button data-title="Claim Manager" data-modalcontent="#add_user_to_team" data-roleType="roleSuggested" data-role="7" class="twoline roleSuggested showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> claim<br>manager</button></div>
														<div class="teamMemberIcon templateDelay9"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp8">
												<div class="teamSetPanel tp8">
													<div id="new_work_comp_agent" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-insurance"></div>
														<div class="addTeamUser"><button data-title="Work Comp Agent" data-modalcontent="#add_user_to_team" data-roleType="roleSuggested" data-role="8" class="twoline roleSuggested showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> work comp<br>agent</button></div>
														<div class="teamMemberIcon templateDelay8"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp9">
												<div class="teamSetPanel tp9">
													<div id="new_service_agent" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-support"></div>
														<div class="addTeamUser"><button data-title="Service Agent" data-modalcontent="#add_user_to_team" data-roleType="roleSuggested" data-role="9" class="twoline roleSuggested showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> service<br>agent</button></div>
														<div class="teamMemberIcon templateDelay7"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp10">
												<div class="teamSetPanel tp10">
													<div id="new_other_users" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-adduser"></div>
														<div class="addTeamUser"><button data-title="Other Users" data-modalcontent="#add_user_to_team" data-roleType="roleOptional" data-role="10" class="twoline roleOptional showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> other<br>users</button></div>
														<div class="teamMemberIcon templateDelay6"><div class="helpAvatar" style="background-color:var(--roleOptional);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>	
			</div>

			<div class="zenispheretabContent" id="activeTeamsTab">
				<div class="teamPicker">
				<div class="buttonArray">
						<button class="showZenModal black" data-modalcontent="#show_teams"><div class="icon icon-cog"></div> choose a team</button>
                        <!--<button class="centered"><div class="icon icon-exchange"></div> transfer team</button>-->
                    </div>
				</div>
				<div class="teamStats">
					<div class="teamTitle"><span id="currentTeamTitle"></span></div>
					<!-- mission and effectiveness both have classes lv1- lv7 as a range. -->
					<div class="mission"><span>mission status:</span><div class="missionBar lv6"><div class="barBGContainer"><div class="barBG"></div></div><div class="barOverlay"></div></div></div>
					<div class="effectiveness"><span>effectiveness:</span><div class="effectiveBar lv5"><div class="barBGContainer"><div class="barBG"></div></div><div class="barOverlay"></div></div></div>
				</div>
				<br>
				<div class="rosterBox">
					<div id="teamRoster" class="teamRosterContainer">
						<div class="teamRoster activeTeam">
							<div class="fullTeamRoster" id="fullTeamLayout">
								<div class="teamRosterBG"><div class="circleOfCare">
									<!-- avatar image should be filled in here.  I have color set as well, because Im using raw icons. color setting should not be necessary --
									<div class="avatarContainer"><div class="userAvatar" style="background-color:#4444ff;background-image:url('/images/avatar_icons/avatar29.png');"></div></div>
									<div class="firstName">Joanna</div>
									<div class="lastName">Perkins</div>
									-->
									<div class="teamManagementControls">
										<button id="saveEditedTeam"><div class="icon icon-floppy-o"></div> save</button><br>
										<button class="showZenModal" data-modalcontent="#renameTeamModal"><div class="icon icon-pencil-square-o"></div> rename</button><br>
										<button id="deleteSquad" class="red"><div class="icon icon-times-circle"></div> delete</button><br>
									</div>
								</div></div>

								<div class="teamRosterLeft">
									<div class="teamElementsLeft">

										<div class="teamElement">
											<div class="element tp1">
												<div class="teamSetPanel tp1 showUserCard">
													<div id="chief_executive" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-exec"></div>
														<button class="twoline roleRequired showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> chief<br>executive</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole required">chief executive</span></div>
														<div class="teamMemberIcon teamDelay1"><div class="userAvatar" style="background-color:#ff77ff;background-image:url('/images/avatar_icons/avatar11.png');"></div></div>
													</div>
												</div>							
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp2">
												<div class="teamSetPanel tp2 showUserCard">
													<div id="safety_manager" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-exclamation-triangle"></div>
														<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> safety<br>manager</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole suggested">safety manager</span></div>
														<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:#347766;background-image:url('/images/avatar_icons/avatar6.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp3">
												<div class="teamSetPanel tp3 showUserCard">
													<div id="supervisor" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-hardhat"></div>
														<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> employee<br>supervisor</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole suggested">employee supervisor</span></div>
														<div class="teamMemberIcon teamDelay3"><div class="userAvatar" style="background-color:#77eeff;background-image:url('/images/avatar_icons/avatar2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp4">
												<div class="teamSetPanel tp4 showUserCard">
													<div id="physical_therapist" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-rehab"></div>
														<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> physical<br>therapist</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole optional">physical therapist</span></div>
														<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:#77ffff;background-image:url('/images/avatar_icons/avatar9.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp5">
												<div class="teamSetPanel tp5 showUserCard">
													<div id="doctor" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-doctor"></div>
														<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> medical<br>support</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole optional">medical support</span></div>
														<div class="teamMemberIcon teamDelay5"><div class="userAvatar" style="background-color:#1177ff;background-image:url('/images/avatar_icons/avatar18.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>

								<div class="teamRosterRight">
									<div class="teamElementsRight">

										<div class="teamElement">
											<div class="element tp6">
												<div class="teamSetPanel tp6 showUserCard">
													<div id="chief_navigator" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-reuser"></div>
														<button class="twoline roleRequired showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> human<br>resources</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole required">human resources</span></div>
														<div class="teamMemberIcon teamDelay10"><div class="userAvatar" style="background-color:#dd7733;background-image:url('/images/avatar_icons/avatar28.png');"></div></div>
													</div>
												</div>							
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp7">
												<div class="teamSetPanel tp7 showUserCard">
													<div id="claim_manager" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-injuryreport"></div>
														<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> claim<br>manager</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole suggested">claim manager</span></div>
														<div class="teamMemberIcon teamDelay9"><div class="userAvatar" style="background-color:#117744;background-image:url('/images/avatar_icons/avatar15.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp8">
												<div class="teamSetPanel tp8 showUserCard">
													<div id="work_comp_agent" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-insurance"></div>
														<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> work comp<br>agent</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole suggested">work comp agent</span></div>
														<div class="teamMemberIcon teamDelay8"><div class="userAvatar" style="background-color:#cc77ff;background-image:url('/images/avatar_icons/avatar22.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp9">
												<div class="teamSetPanel tp9 showUserCard">
													<div id="customer_service_agent" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-support"></div>
														<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> service<br>agent</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole suggested">service agent</span></div>
														<div class="teamMemberIcon teamDelay7"><div class="userAvatar" style="background-color:#11ccff;background-image:url('/images/avatar_icons/avatar16.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp10">
												<div class="teamSetPanel tp10 showUserCard">
													<div id="other" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-adduser"></div>
														<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> other<br>users</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole optional">other user(s)</span></div>
														<div class="teamMemberIcon teamDelay6"><div class="userAvatar" style="background-color:#5511ff;background-image:url('/images/avatar_icons/avatar8.png');"></div></div>
													</div>
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

			<div class="zenispheretabContent" id="manageUsersTab">
				<div class="buttonArray">
					<button class="showZenModal" data-modalcontent="#add_user"><div class="icon inline icon-plus"></div> add new user</button>
					@if(Auth::user()->is_zagent)
					<button class="showZenModal" data-modalcontent="#add_zagent"><div class="icon inline icon-plus"></div> add new zagent</button>
					@endif
					<!--<button class="filtered centered showFilter"><div class="icon inline icon-filter"></div> filtered</button>-->
				</div><br>
				<div id="userGrid" class="itemGrid userList">
					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar13.png');background-color:pink;"></div>
							<div class="userFirstName">PoindexterVonBurgundy</div>
							<div class="userLastName">Harringtonsmithsonwillamstein</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar6.png');background-color:blue;"></div>
							<div class="userFirstName">Rose</div>
							<div class="userLastName">Mellingsby</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar3.png');background-color:cyan;"></div>
							<div class="userFirstName">Patrick</div>
							<div class="userLastName">Smeenley</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar12.png');background-color:black;"></div>
							<div class="userFirstName">John</div>
							<div class="userLastName">Zwilinger</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar2.png');background-color:green;"></div>
							<div class="userFirstName">Karen</div>
							<div class="userLastName">Blasking</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar7.png');background-color:cyan;"></div>
							<div class="userFirstName">Leon</div>
							<div class="userLastName">Harmining</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar16.png');background-color:red;"></div>
							<div class="userFirstName">Sarah</div>
							<div class="userLastName">Limmonly</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar8.png');background-color:orange;"></div>
							<div class="userFirstName">Sho</div>
							<div class="userLastName">Kashoki</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar20.png');background-color:grey;"></div>
							<div class="userFirstName">Merlin</div>
							<div class="userLastName">Jasminbly</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar4.png');background-color:blue;"></div>
							<div class="userFirstName">Madeline</div>
							<div class="userLastName">Shillingsworth</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar23.png');background-color:yellow;"></div>
							<div class="userFirstName">Mary</div>
							<div class="userLastName">Jones</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar19.png');background-color:black;"></div>
							<div class="userFirstName">Herman</div>
							<div class="userLastName">Dessinger</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>
				</div>
			</div>

		</div>
    <!-- test to show all modals -->
		<div class="buttonCloud small devButtons">
		<button class="showZenModal" data-modalcontent="#add_user">add user</button>
		<button class="showZenModal" data-modalcontent="#add_zagent">add user</button>
		<button class="showZenModal" data-modalcontent="#add_user_to_team">add user to team</button>
		<button class="showZenModal" data-modalcontent="#user_list">user list</button>
		<button class="showZenModal" data-modalcontent="#user_card">user card</button>
		<button class="showZenModal" data-modalcontent="#show_teams">choose team</button>
		<button class="showZenModal" data-modalcontent="#show_team_templates">choose team template</button>
		<button class="showZenModal" data-modalcontent="#renameTeamModal">rename team</button>
		<button class="showZenModal" data-modalcontent="#show_filter">filter</button>
		</div>
	</div>

</div>

<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
	@include('partials.modals.addUser')
	@include('partials.modals.addZagent')
	@include('partials.modals.addUserToTeam')
	@include('partials.modals.userList')
	@include('partials.modals.userCard')
	@include('partials.modals.showAllTeams')
	@include('partials.modals.showAllTeamTemplates')
	@include('partials.modals.showFilter')
	@include('partials.modals.renameTeam')
	@include('partials.modals.popmessage')
	@include('partials.modals.popmessage2')                
</div>

<script>
    $('body').on('click', '.showZenModal', function(){
		//get the content for the modal
		var target = $(this).data('modalcontent');
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true,
        });
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here

		if(target === "#add_zagent"){
			@if(Auth::user()->is_zagent && Auth::user()->is_zenpro != 1)
            	$('#inviteNewZagent').attr('data-companyid', '{{$agency->id}}');
			@endif
        }
			
			modal.open();
		
    });
</script>


@include('partials.zenisphereJS')
@endsection

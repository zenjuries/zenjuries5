@extends('layouts.zen5_layout')
@section('maincontent')
	
<div class="pageContent bgimage-bgheader pagebackground15 fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>TITLE</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/bandaid.png"></span><span class="subContent">subtitle</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/help.png">
            </div> 
        </div>           
	</div>


	<div class="contentBlock lessPadding">
		<div class="container setMinHeight">

        <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-comments"></div>title</span>
            <div class="sectionContent padded">
                <span class="sectionContentTitle">title</span>
                <p>paragraph text</p>
            </div>
         </section>
						
		</div>
	</div>


</div>

	


@endsection

@extends('layouts.zen5_layout')
@section('maincontent')

<?php
//EDITED - TYLER
if(Auth::user()->type == 'agent'){
    if(session('company_id') == NULL){
        echo "<script>window.location.href='/';</script>";
    }else{
        Auth::user()->getCompany()->id = session('company_id');
    }
}

$company = \App\Company::where('id', Auth::user()->getCompany()->id)->first();

$team = \App\Team::where('id', $company->team_id)->first();

$subscription = DB::table('team_subscriptions')->where('team_id', $team->id)->orderBy('created_at', 'desc')->first();

if($company->registered_at !== NULL){
    $startDate = \Carbon\Carbon::parse($company->registered_at);

    $endDate = \Carbon\Carbon::parse($company->registered_at);
    $endDate->addYear()->subDay();
}else if(is_null($subscription)){
    $startDate = \Carbon\Carbon::parse($company->created_at);

    $endDate = \Carbon\Carbon::parse($company->created_at);
    $endDate->addYear()->subDay();
}else{
    $startDate = \Carbon\Carbon::parse($subscription->created_at);

    $endDate = \Carbon\Carbon::parse($subscription->created_at);
    $endDate->addYear()->subDay();
}

//we'll use the company's most recent subscription date or sign-up date if no subscription, and increment it until it matches the current year
while(\Carbon\Carbon::now()->gte($endDate)){
    $startDate->addYear();
    $endDate->addYear();
}


$link = '/renderCert/' . $company->id;

//$fStartDate = date('F d Y', $startDate);
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.10.1/html2pdf.bundle.min.js" integrity="sha512-GsLlZN/3F2ErC5ifS5QtgpiJtWd43JWSuIgh7mbzZ8zBps+dvLusV+eNQATqgA/HdeKFVgA5v3S/cIrLF7QnIg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<div class="pageContent bgimage-bgheader pagebackground13 fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>official certificate</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/greencheck.png"></span><span class="subContent">your zenjuries certificate</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/documents.png">
            </div> 
        </div>           
	</div>
    
	<div class="contentBlock lessPadding">
		<div class="container setMinHeight">
            <div class="certificateContainer">
                <div class="downloadButton divCenter"><button class="gold hideMobile" id="downloadPDF"><div class="icon icon-file-pdf-o"></div> download pdf</button></div>
                <div class="certificate" id="zencert">
                    
                    <div class="certBody">
                        THIS CERTIFICATE CERTIFIES THAT<br>
                        <div style="color:black;font-weight:400;font-size:2rem;padding-top:1rem;"><?php echo $company->company_name; ?></div><br>
                        <span style="font-size:1rem;text-transform:uppercase;">Is committed to managing its workers’ compensation claims<br>and improving outcomes for injured employees. </span><br>
                        <div style="color:black;font-weight:900;font-size:2rem;padding-top:1rem;">ZENJURIES</div>
                        <hr width="50%">
                        <span style="font-size:1rem;font-style:italic;letter-spacing:.08rem;">The Zenjuries claim management software increases communication<br>
                        between the employer, the claims adjuster and the injured employee<br>
                        which delivers better outcomes. <br></span>
                        
                    </div>
                    <span>
                        <div id="certDate"><span style="font-size:.8rem;">EFFECTIVE DATES OF ENROLLMENT:</span><br>
                            <div id="dateString">
                                <b><?php echo $startDate->toFormattedDateString() . ' - ' . $endDate->toFormattedDateString(); ?></b>
                            </div>
                            date</div>
                        <div id="certEnrollmentDept">
                            <img src="/images/zensig.png" width="150px"><br>
                            Enrollment Department
                        </div>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var certificate = document.getElementById('zencert');
var opt = {
  margin:       0,
  filename:     'zenjuries-certificate.pdf',
  image:        { type: 'jpeg', quality: 0.98 },
  html2canvas:  { scale: 1 },
  jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
};
$('#downloadPDF').on('click', function(){
    html2pdf().set(opt).from(certificate).save();
    });
</script>

@endsection
@extends('layouts.zen5_layout')
@section('maincontent')
	
<div class="pageContent bgimage-bgheader bghaze-violet fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>your <b>zen<span style="color: var(--zenisphere);">isphere</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/teammessages.png"></span><span class="subContent">manage your users and teams</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/team-bright.png">
            </div> 
        </div>           
	</div>

<!-- this script controls the tabs at the top -->	
<script>
$(document).ready(function() {
	 
    $('#zenispheretabs').addClass('activeTeams');    
    $('#activeTeamsTab').show();
	$('#zenispheretabs').on('click', 'li', function(){
		var tab_class = $(this).attr('class');
		var target = $(this).data('target');
		$('#zenispheretabs').removeClass().addClass(tab_class);
		$('.zenispheretabContent').hide();
		$('#' + target).show();
		
	});     
	
});    
</script>

	<div class="contentBlock noPad">
		<div class="container noPad">

		<div id="zenispheretabs">
			<ul>
				<li class="manageTeams" data-target="manageTeamsTab"><a class="tabManageTeams"><div class="icon icon-teams"></div><span>team templates</span></a></li>
				<li class="activeTeams" data-target="activeTeamsTab"><a class="tabActiveTeams"><div class="icon icon-check"></div><span>active teams</span></a></li>				
				<li class="manageUsers" data-target="manageUsersTab"><a class="tabManageUsers"><div class="icon icon-user"></div><span>user management</span></a></li>
			</ul>
		</div>
			<div class="zenispheretabContent" id="manageTeamsTab">
			<div class="teamPicker">
					<div class="buttonArray">
						<button class="showAllTeamTemplates black centered"><div class="icon icon-cog"></div> choose a template</button>
						<button class="cyan centered"><div class="icon icon-plus-circle"></div> new template</button>
                    </div>
				</div>
				<div class="teamStats">
					<div class="teamTitle"><span id="currentTeamTemplateTitle">template title</span></div>
				</div>
				<br>
				<div class="rosterBox">
					<div class="teamRosterContainer">
						<div class="teamRoster teamTemplate">
							<div class="fullTeamRoster" id="fullTeamLayout">
								<div class="teamRosterBG"><div class="circleOfCare">
									<div class="teamManagementControls">
										<button><div class="icon icon-floppy-o"></div> save</button><br>
										<button><div class="icon icon-pencil-square-o"></div> rename</button><br>
										<button class="red"><div class="icon icon-times-circle"></div> delete</button><br>
										<button><div class="icon icon-thumb-tack"></div> default</button><br>
									</div>
								</div></div>

								<div class="teamRosterLeft">
									<div class="teamElementsLeft">

										<div class="teamElement">
											<div class="element tp1">
												<div class="teamSetPanel tp1">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-exec"></div>
														<div class="addTeamUser"><button class="twoline roleRequired showAddUserToTeam"><div class="icon inline icon-plus"></div> chief<br>executive</button></div>
														<div class="teamMemberIcon templateDelay1"><div class="helpAvatar" style="background-color:var(--roleRequired);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>							
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp2">
												<div class="teamSetPanel tp2">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-zencircle"></div>
														<div class="addTeamUser"><button class="twoline roleSuggested showAddUserToTeam"><div class="icon inline icon-plus"></div> insurance<br>adjuster</button></div>
														<div class="teamMemberIcon templateDelay2"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp3">
												<div class="teamSetPanel tp3">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-exclamation-triangle"></div>
														<div class="addTeamUser"><button class="twoline roleSuggested showAddUserToTeam"><div class="icon inline icon-plus"></div> safety<br>manager</button></div>
														<div class="teamMemberIcon templateDelay3"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp4">
												<div class="teamSetPanel tp4">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-hardhat"></div>
														<div class="addTeamUser"><button class="twoline roleSuggested showAddUserToTeam"><div class="icon inline icon-plus"></div> employee<br>supervisor</button></div>
														<div class="teamMemberIcon templateDelay4"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp5">
												<div class="teamSetPanel tp5">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-doctor"></div>
														<div class="addTeamUser"><button class="twoline roleOptional showAddUserToTeam"><div class="icon inline icon-plus"></div> medical<br>support</button></div>
														<div class="teamMemberIcon templateDelay5"><div class="helpAvatar" style="background-color:var(--roleOptional);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>

								<div class="teamRosterRight">
									<div class="teamElementsRight">

										<div class="teamElement">
											<div class="element tp6">
												<div class="teamSetPanel tp6">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-reuser"></div>
														<div class="addTeamUser"><button class="twoline roleRequired showAddUserToTeam"><div class="icon inline icon-plus"></div> human<br>resources</button></div>
														<div class="teamMemberIcon templateDelay10"><div class="helpAvatar" style="background-color:var(--roleRequired);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>							
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp7">
												<div class="teamSetPanel tp7">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-injuryreport"></div>
														<div class="addTeamUser"><button class="twoline roleSuggested showAddUserToTeam"><div class="icon inline icon-plus"></div> claim<br>manager</button></div>
														<div class="teamMemberIcon templateDelay9"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp8">
												<div class="teamSetPanel tp8">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-insurance"></div>
														<div class="addTeamUser"><button class="twoline roleSuggested showAddUserToTeam"><div class="icon inline icon-plus"></div> work comp<br>agent</button></div>
														<div class="teamMemberIcon templateDelay8"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp9">
												<div class="teamSetPanel tp9">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-support"></div>
														<div class="addTeamUser"><button class="twoline roleSuggested showAddUserToTeam"><div class="icon inline icon-plus"></div> service<br>agent</button></div>
														<div class="teamMemberIcon templateDelay7"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp10">
												<div class="teamSetPanel tp10">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-adduser"></div>
														<div class="addTeamUser"><button class="twoline roleOptional showAddUserToTeam"><div class="icon inline icon-plus"></div> other<br>users</button></div>
														<div class="teamMemberIcon templateDelay6"><div class="helpAvatar" style="background-color:var(--roleOptional);background-image:url('/images/icons/question2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>	
			</div>

			<div class="zenispheretabContent" id="activeTeamsTab">
				<div class="teamPicker">
				<div class="buttonArray">
						<button class="showAllTeams black centered"><div class="icon icon-cog"></div> choose a team</button>
                        <button class="centered"><div class="icon icon-exchange"></div> transfer team</button>
                    </div>
				</div>
				<div class="teamStats">
					<div class="teamTitle"><span id="currentTeamTitle">joanna's team</span></div>
					<!-- mission and effectiveness both have classes lv1- lv7 as a range. -->
					<div class="mission"><span>mission status:</span><div class="missionBar lv6"><div class="barBGContainer"><div class="barBG"></div></div><div class="barOverlay"></div></div></div>
					<div class="effectiveness"><span>effectiveness:</span><div class="effectiveBar lv5"><div class="barBGContainer"><div class="barBG"></div></div><div class="barOverlay"></div></div></div>
				</div>
				<br>
				<div class="rosterBox">
					<div class="teamRosterContainer">
						<div class="teamRoster activeTeam">
							<div class="fullTeamRoster" id="fullTeamLayout">
								<div class="teamRosterBG"><div class="circleOfCare">
									<!-- avatar image should be filled in here.  I have color set as well, because Im using raw icons. color setting should not be necessary -->
									<div class="avatarContainer"><div class="userAvatar" style="background-color:#4444ff;background-image:url('/images/avatar_icons/avatar29.png');"></div></div>
									<div class="firstName">Joanna</div>
									<div class="lastName">Perkins</div>
								</div></div>

								<div class="teamRosterLeft">
									<div class="teamElementsLeft">

										<div class="teamElement">
											<div class="element tp1">
												<div class="teamSetPanel tp1 showUserCard">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-exec"></div>
														<button class="twoline roleRequired" style="display:none;"><div class="icon inline icon-plus"></div> chief<br>executive</button>
														<div class="teamUser"><span class="teamUserName">John Smithson</span><span class="teamUserRole required">chief executive</span></div>
														<div class="teamMemberIcon teamDelay1"><div class="userAvatar" style="background-color:#ff77ff;background-image:url('/images/avatar_icons/avatar11.png');"></div></div>
													</div>
												</div>							
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp2">
												<div class="teamSetPanel tp2 showUserCard">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-zencircle"></div>
														<button class="twoline roleSuggested" style="display:none;"><div class="icon inline icon-plus"></div> insurance<br>adjuster</button>
														<div class="teamUser"><span class="teamUserName">Keri Zimmer</span><span class="teamUserRole suggested">insurance adjuster</span></div>
														<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:#77ffff;background-image:url('/images/avatar_icons/avatar9.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp3">
												<div class="teamSetPanel tp3 showUserCard">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-exclamation-triangle"></div>
														<button class="twoline roleSuggested" style="display:none;"><div class="icon inline icon-plus"></div> safety<br>manager</button>
														<div class="teamUser"><span class="teamUserName">Thomas Nordling</span><span class="teamUserRole suggested">safety manager</span></div>
														<div class="teamMemberIcon teamDelay3"><div class="userAvatar" style="background-color:#347766;background-image:url('/images/avatar_icons/avatar6.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp4">
												<div class="teamSetPanel tp4 showUserCard">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-hardhat"></div>
														<button class="twoline roleSuggested" style="display:none;"><div class="icon inline icon-plus"></div> employee<br>supervisor</button>
														<div class="teamUser"><span class="teamUserName">Harold Weinburg</span><span class="teamUserRole suggested">employee supervisor</span></div>
														<div class="teamMemberIcon teamDelay4"><div class="userAvatar" style="background-color:#77eeff;background-image:url('/images/avatar_icons/avatar2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp5">
												<div class="teamSetPanel tp5 showUserCard">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-doctor"></div>
														<button class="twoline roleOptional" style="display:none;"><div class="icon inline icon-plus"></div> medical<br>support</button>
														<div class="teamUser"><span class="teamUserName">Renee Miltzer</span><span class="teamUserRole optional">medical support</span></div>
														<div class="teamMemberIcon teamDelay5"><div class="userAvatar" style="background-color:#1177ff;background-image:url('/images/avatar_icons/avatar18.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>

								<div class="teamRosterRight">
									<div class="teamElementsRight">

										<div class="teamElement">
											<div class="element tp6">
												<div class="teamSetPanel tp6 showUserCard">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-reuser"></div>
														<button class="twoline roleRequired" style="display:none;"><div class="icon inline icon-plus"></div> human<br>resources</button>
														<div class="teamUser"><span class="teamUserName">Kevin Keinston</span><span class="teamUserRole required">human resources</span></div>
														<div class="teamMemberIcon teamDelay10"><div class="userAvatar" style="background-color:#dd7733;background-image:url('/images/avatar_icons/avatar28.png');"></div></div>
													</div>
												</div>							
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp7">
												<div class="teamSetPanel tp7 showUserCard">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-injuryreport"></div>
														<button class="twoline roleSuggested" style="display:none;"><div class="icon inline icon-plus"></div> claim<br>manager</button>
														<div class="teamUser"><span class="teamUserName">Reginald Hemsley</span><span class="teamUserRole suggested">claim manager</span></div>
														<div class="teamMemberIcon teamDelay9"><div class="userAvatar" style="background-color:#117744;background-image:url('/images/avatar_icons/avatar15.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp8">
												<div class="teamSetPanel tp8 showUserCard">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-insurance"></div>
														<button class="twoline roleSuggested" style="display:none;"><div class="icon inline icon-plus"></div> work comp<br>agent</button>
														<div class="teamUser"><span class="teamUserName">Otho VonBergmet</span><span class="teamUserRole suggested">work comp agent</span></div>
														<div class="teamMemberIcon teamDelay8"><div class="userAvatar" style="background-color:#cc77ff;background-image:url('/images/avatar_icons/avatar22.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp9">
												<div class="teamSetPanel tp9 showUserCard">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-support"></div>
														<button class="twoline roleSuggested" style="display:none;"><div class="icon inline icon-plus"></div> service<br>agent</button>
														<div class="teamUser"><span class="teamUserName">Candice Johnson</span><span class="teamUserRole suggested">service agent</span></div>
														<div class="teamMemberIcon teamDelay7"><div class="userAvatar" style="background-color:#11ccff;background-image:url('/images/avatar_icons/avatar16.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp10">
												<div class="teamSetPanel tp10 showUserCard">
													<div class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-adduser"></div>
														<button class="twoline roleOptional" style="display:none;"><div class="icon inline icon-plus"></div> other<br>users</button>
														<div class="teamUser"><span class="teamUserName">Samson Vaugn</span><span class="teamUserRole optional">other user(s)</span></div>
														<div class="teamMemberIcon teamDelay6"><div class="userAvatar" style="background-color:#5511ff;background-image:url('/images/avatar_icons/avatar8.png');"></div></div>
													</div>
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

			<div class="zenispheretabContent" id="manageUsersTab">
				<div class="buttonArray">
					<button class="cyan centered showAddUser"><div class="icon inline icon-plus"></div> add new user</button>
					<button class="filtered centered showFilter"><div class="icon inline icon-filter"></div> filtered</button>
				</div><br>
				<div class="itemGrid userList">
					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar13.png');background-color:pink;"></div>
							<div class="userFirstName">PoindexterVonBurgundy</div>
							<div class="userLastName">Harringtonsmithsonwillamstein</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar6.png');background-color:blue;"></div>
							<div class="userFirstName">Rose</div>
							<div class="userLastName">Mellingsby</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar3.png');background-color:cyan;"></div>
							<div class="userFirstName">Patrick</div>
							<div class="userLastName">Smeenley</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar12.png');background-color:black;"></div>
							<div class="userFirstName">John</div>
							<div class="userLastName">Zwilinger</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar2.png');background-color:green;"></div>
							<div class="userFirstName">Karen</div>
							<div class="userLastName">Blasking</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar7.png');background-color:cyan;"></div>
							<div class="userFirstName">Leon</div>
							<div class="userLastName">Harmining</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar16.png');background-color:red;"></div>
							<div class="userFirstName">Sarah</div>
							<div class="userLastName">Limmonly</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar8.png');background-color:orange;"></div>
							<div class="userFirstName">Sho</div>
							<div class="userLastName">Kashoki</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar20.png');background-color:grey;"></div>
							<div class="userFirstName">Merlin</div>
							<div class="userLastName">Jasminbly</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar4.png');background-color:blue;"></div>
							<div class="userFirstName">Madeline</div>
							<div class="userLastName">Shillingsworth</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar23.png');background-color:yellow;"></div>
							<div class="userFirstName">Mary</div>
							<div class="userLastName">Jones</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>

					<div class="itemGrid__item userContainer">
						<div class="userListUser showUserCard">
							<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar19.png');background-color:black;"></div>
							<div class="userFirstName">Herman</div>
							<div class="userLastName">Dessinger</div>
						</div>
						<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>
					</div>
				</div>
			</div>

		</div>
    <!-- test to show all modals -->
		<div class="buttonCloud small">
		<button class="showAddUser">add user</button>
		<button class="showAddUserToTeam">add user to team</button>
		<button class="showUserList">user list</button>
		<button class="showUserCard">user card</button>
		<button class="showAllTeams">choose team</button>
		<button class="showAllTeamTemplates">choose team template</button>
		<button class="showFilter">filter</button>
		</div>
	</div>

</div>

@include('partials.modals.addUser')
@include('partials.modals.addUserToTeam')
@include('partials.modals.userList')
@include('partials.modals.userCard')
@include('partials.modals.showAllTeams')
@include('partials.modals.showAllTeamTemplates')
@include('partials.modals.showFilter')
<script>
	//each jBox modal needs to be created with this code block, using seperate variable names for each new modal created
	var addMediaDescriptionModal = new jBox('Modal', {
		attach: '.showAddUser', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#add_user'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true
	});
	var addMediaModal = new jBox('Modal', {
		attach: '.showAddUserToTeam', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#add_user_to_team'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true
	});
    var showImageModal = new jBox('Modal', {
		attach: '.showUserList', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#user_list'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true
	});  
    var showImageModal = new jBox('Modal', {
		attach: '.showUserCard', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#user_card'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true
	});	 
    var showImageModal = new jBox('Modal', {
		attach: '.showAllTeams', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#show_teams'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true
	});
    var showImageModal = new jBox('Modal', {
		attach: '.showAllTeamTemplates', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#show_team_templates'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true
	});	 
    var showImageModal = new jBox('Modal', {
		attach: '.showFilter', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#show_filter'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true
	});		
</script>	


@endsection

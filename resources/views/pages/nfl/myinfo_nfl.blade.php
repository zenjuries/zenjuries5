@extends('layouts.zen5_layout')
@section('maincontent')
<?php 
use Carbon\Carbon;
$user = Auth::user();

$theme = $user->zengarden_theme;
$theme = str_replace("theme", "", $theme);
$theme = strtolower($theme);
$currentTeamId = $user->current_team_id;
$teamName =  \App\Team::where('id', $currentTeamId)->value('name');

//getting timestamp for last password change
$passwordUpdatedAt = new DateTime($user->password_updated_at);
$passwordUpdatedAt = $passwordUpdatedAt->format('m-d-Y h:i A');
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="https://www.zenployees.com/js/parallax.js"></script>
<script src="{{ URL::asset('/js/croppie.js') }}"></script>
<link rel="stylesheet" href="{{URL::asset('/css/croppie.css')}}">
<script src="{{URL::asset('/js/zp/colorpicker.js') }}"></script>
<style>
    span.zCell.spacer{padding:0;display: inline-block}
    span.zCell.spacer:before{content:'----------';display:block;color:#747474;}
    span.zCell.spacer:first-child:before {text-align:right}
    span.zCell.spacer:last-child:before {text-align:left;}

    .themePlaceHolder{
        padding: 80px 1px;
    }
</style>

<div class="pageContent bgimage-bgheader bghaze-ocean fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>my info</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/teammessages.png"></span><span class="subContent">edit your information</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/userinfo.png">
            </div> 
        </div>           
	</div>


    <div class="contentBlock lessPadding">
		<div class="container setMinHeight">
            <!--
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> alerts and messages</span>
                <div class="sectionContent">
                    <div class="placeHolder">
                        User name
                        <span>current alerts and messages go here</span>
                    </div>
                    <hr>
                    <div class="buttonArray"><button class="cyan centered">update info</button></div>
                </div>
            </section>
            -->
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> basic info</span>
                <div class="sectionContent">
                    Name: <span id="currentUserName">{{ $user->name }}</span><br>
                    Email: <span id="currentUserEmail">{{ $user->email }}</span><br>
                    Phone: <span id="currentUserPhone">{{ (empty($user->phone) ? "" : $user->phone) }}</span><br>
                    Description: <span id="currentUserDescription">{{ (empty($user->description) ? "comments here" : $user->description) }}</span><br>
                    Last password change: <span id="passwordChangedAt">{{ $passwordUpdatedAt }}</span><br>
                    <br>
                    Update basic info and password
                    <hr>
                    <div class="buttonArray"><button class="cyan centered showMyInfoModal" data-modalcontent="#userInfoModal">update info</button><button class="cyan centered showMyInfoModal" data-modalcontent="#passwordModal">update password</button></div>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-bell"></div> alerts & updates</span>
                <div class="sectionContent">
                    Weekly Summary: <b id="currentWeeklySummary">@if($user->gets_weekly_summary) Enabled @else Disabled @endif</b><br>
                    Priority 2 Emails: <b id="currentPriority2Emails">@if($user->gets_priority_2_emails) Enabled @else Disabled @endif</b><br>
                    Priority 3 Emails: <b id="currentPriority3Emails">@if($user->gets_priority_3_emails) Enabled @else Disabled @endif</b><br>
                    <br>
                    Update your alerts and communication settings here.<br>
                    <b>Priority 2 Emails</b> - consist of major updates on claims and your account such as severity updates and subscription updates.<br>
                    <b>Priority 3 Emails</b> - consist of minor updates on claims and your account such as claim costs being updated and zengarden dirary updates<br>
                    <hr>                  
                    <div class="buttonArray"><button class="cyan centered showMyInfoModal" data-modalcontent="#communicationModal">update alerts</button></div>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-bamboo2"></div> zenjuries theme</span>
                <div class="sectionContent">
                    
                    <div class="setButton themePlaceHolder buttonSet themeBG" style="background-image:url('/images/themes/{{ $theme }}.jpg'); text-align: center;">
                        <span class="themeLabelbg themeColor-bbg-{{  ucfirst($theme) }}">Your current theme: <b>{{ $theme }}</b></span>
                    </div> 
                    
                    <hr>                    
                    <div class="buttonArray"><button class="cyan centered showMyInfoModal" data-modalcontent="#themeModal">choose theme</button></div>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-palette"></div> avatar & color</span>
                <div class="sectionContent" style="display: flex;flex-direction:column;align-items:center;">
                    <div class="profileAvatarColor" style="background-color: {{$user->avatar_color}}"><div class="avatarIcon" style="background-image:url({{$user->photo_url}})"></div></div>
                    <input id="currentAvatarNumber" type="hidden" value="{{$user->avatar_number}}">
                    <!--
                    <div class="placeHolder">
                        avatar | color
                        <span>the set avatar and color here</span>
                    </div>
                    -->
                    <hr>                     
                    <div class="buttonArray">
                        <button class="cyan centered showMyInfoModal" data-modalcontent="#changeAvatar">change avatar</button>
                        <button class="cyan showMyInfoModal" data-modalcontent="#imageModal">upload your own image</button>
                        <button class="cyan centered showMyInfoModal" data-modalcontent="#changeColor">change color</button>
                    </div>
                </div>
            </section>
		</div>
	</div>

</div>
<!-- MODALS -->
<div id="myInfoModal" class="zModal" style="display:none">
    <div class="modalContent modalBlock" id="userInfoModal" style="display: none">
        <div class="modalHeader"><span class="modalTitle">Edit Your Info</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-insurance"></div>Update your info</span>
                    <div class="sectionContent">
                        <section class="formBlock dark">                          
                            <div class="formGrid">  
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="userName">name</label>
                                    <input id="userName" type="text" value="{{ $user->name }}"/>
                                    <span class="inputError">error</span>
                                </div>
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="phone">phone</label>
                                    <input id="phone" type="text" value="{{ (empty($user->phone) ? "" : $user->phone) }}"/>
                                    <span class="inputError">error</span>
                                </div>
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="email">email</label>
                                    <input id="email" type="text" value="{{ $user->email }}"/>
                                    <span class="inputError">error</span>
                                </div>
                                <div class="formInput">
                                    <label for="description">description</label>
                                    <input id="description" type="text" value="{{ (empty($user->description) ? "" : $user->description) }}"/>
                                    <span class="inputError">error</span>
                                </div>
                                <button class="cyan" id="submitUserInfo">submit</button>
                            </div>
                        </section>
                    </div>                    
            </section>           
        </div>
    </div> 

    <!-- PASSWORD MODALBLOCK -->
    <div class="modalContent modalBlock" id="passwordModal" style="display: none">
        <div class="modalHeader"><span class="modalTitle">Update Password</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-insurance"></div>update your passowrd</span>
                    <div class="sectionContent">
                        <section class="formBlock dark">                          
                            <div class="formGrid">  
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="newPassword">new password</label>
                                    <input id="newPassword" type="password" />
                                    <span class="inputError">error</span>
                                </div>
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="confirmNewPassword">confirm new password</label>
                                    <input id="confirmNewPassword" type="password" />
                                    <span class="inputError">error</span>
                                </div>
                                <div class="formInput">
                                    <button class="cyan" id="submitPasswordChange">submit</button>
                                </div>
                            </div>
                        </section>
                    </div>                    
            </section>           
        </div>
    </div> 
    <!--  COMMUNICATION MODAL  -->
    <div class="modalContent modalBlock" id="communicationModal" style="display: none">
        <div class="modalHeader"><span class="modalTitle">Communications Settings</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-insurance"></div>Update your communicaitons settings</span>
                    <div class="sectionContent">
                        <section class="formBlock dark">                          
                            <div class="formGrid">
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="weeklyUpdateEnabled">enable weekly updates</label>
                                    Enable <input id="weeklyUpdateEnabled" name="weeklyUpdate" type="radio"  value="1" @if($user->gets_weekly_summary) checked @endif/>
                                    <span class="inputError">error</span>
                                </div> 
                                <div class="formInput">
                                    <label for="weeklyUpdateDisabled" style="opacity: 0.0;"> Disabled</label>
                                    Disable <input id="weeklyUpdateDisabled" name="weeklyUpdate" type="radio" value="0" @if(!$user->gets_weekly_summary) checked @endif/>
                                </div> 
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="priority2Setting">enable priority 2 emails (major updates on claims and account)</label>
                                    Enable <input id="priority2SettingEnabled" type="radio" name="priority2Email" value="1" @if($user->gets_priority_2_emails) checked @endif/>
                                    <span class="inputError">error</span>
                                </div>
                                <div class="formInput">
                                    <label for="priority2SettingDisabled" style="opacity: 0.0;"> Disabled</label>
                                    Disable <input id="priority2SettingDisabled" type="radio" name="priority2Email" value="0" @if(!$user->gets_priority_2_emails) checked @endif/>
                                </div>
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="priority3Setting">enable priority 3 emails  (minor updates on claims and account)</label>
                                    Enable <input id="priority3SettingEnabled" type="radio" name="proprity3Email" value="1" @if($user->gets_priority_3_emails) checked @endif/>
                                    <span class="inputError">error</span>
                                </div>
                                <div class="formInput">
                                    <label for="priority3SettingDisabled" style="opacity: 0.0;"> Disabled</label>
                                    Disable <input id="priority3SettingDisabled" type="radio" name="proprity3Email" value="0" @if(!$user->gets_priority_3_emails) checked @endif/>
                                </div>
                                <div class="formInput">
                                    <button class="cyan" id="submitCommunicationChange">submit</button>
                                </div>
                            </div>
                        </section>
                    </div>                    
            </section>           
        </div>
    </div> 

    <!-- CHANGE AVATAR MODAL -->
    <div class="modalContent modalBlock" style="display:none" id="changeAvatar">
        <div class="modalHeader"><span class="modalTitle">Change Avatar</span></div>
        <div class="modalBody" style="min-width:260px;">
            <ul class="iconSelector">
				<li><a data-avatar="avatar1" style="background-image: url('/images/avatar_icons/avatar1.png')"></a></li>
				<li><a data-avatar="avatar2" style="background-image: url('/images/avatar_icons/avatar2.png')"></a></li>
				<li><a data-avatar="avatar3" style="background-image: url('/images/avatar_icons/avatar3.png')"></a></li>
				<li><a data-avatar="avatar4" style="background-image: url('/images/avatar_icons/avatar4.png')"></a></li>
				<li><a data-avatar="avatar5" style="background-image: url('/images/avatar_icons/avatar5.png')"></a></li>
				<li><a data-avatar="avatar6" style="background-image: url('/images/avatar_icons/avatar6.png')"></a></li>
				<li><a data-avatar="avatar7" style="background-image: url('/images/avatar_icons/avatar7.png')"></a></li>
				<li><a data-avatar="avatar8" style="background-image: url('/images/avatar_icons/avatar8.png')"></a></li>
				<li><a data-avatar="avatar9" style="background-image: url('/images/avatar_icons/avatar9.png')"></a></li>
				<li><a data-avatar="avatar10" style="background-image: url('/images/avatar_icons/avatar10.png')"></a></li>
				<li><a data-avatar="avatar11" style="background-image: url('/images/avatar_icons/avatar11.png')"></a></li>
				<li><a data-avatar="avatar12" style="background-image: url('/images/avatar_icons/avatar12.png')"></a></li>
				<li><a data-avatar="avatar13" style="background-image: url('/images/avatar_icons/avatar13.png')"></a></li>
				<li><a data-avatar="avatar14" style="background-image: url('/images/avatar_icons/avatar14.png')"></a></li>
				<li><a data-avatar="avatar15" style="background-image: url('/images/avatar_icons/avatar15.png')"></a></li>
				<li><a data-avatar="avatar16" style="background-image: url('/images/avatar_icons/avatar16.png')"></a></li>
				<li><a data-avatar="avatar17" style="background-image: url('/images/avatar_icons/avatar17.png')"></a></li>
				<li><a data-avatar="avatar18" style="background-image: url('/images/avatar_icons/avatar18.png')"></a></li>
				<li><a data-avatar="avatar19" style="background-image: url('/images/avatar_icons/avatar19.png')"></a></li>
				<li><a data-avatar="avatar20" style="background-image: url('/images/avatar_icons/avatar20.png')"></a></li>
				<li><a data-avatar="avatar21" style="background-image: url('/images/avatar_icons/avatar21.png')"></a></li>
				<li><a data-avatar="avatar22" style="background-image: url('/images/avatar_icons/avatar22.png')"></a></li>
				<li><a data-avatar="avatar23" style="background-image: url('/images/avatar_icons/avatar23.png')"></a></li>
				<li><a data-avatar="avatar24" style="background-image: url('/images/avatar_icons/avatar24.png')"></a></li>
				<li><a data-avatar="avatar25" style="background-image: url('/images/avatar_icons/avatar25.png')"></a></li>
				<li><a data-avatar="avatar26" style="background-image: url('/images/avatar_icons/avatar26.png')"></a></li>
				<li><a data-avatar="avatar27" style="background-image: url('/images/avatar_icons/avatar27.png')"></a></li>
				<li><a data-avatar="avatar28" style="background-image: url('/images/avatar_icons/avatar28.png')"></a></li>
				<li><a data-avatar="avatar29" style="background-image: url('/images/avatar_icons/avatar29.png')"></a></li>
				<li><a data-avatar="avatar30" style="background-image: url('/images/avatar_icons/avatar30.png')"></a></li>
				<li><a data-avatar="avatar31" style="background-image: url('/images/avatar_icons/avatar31.png')"></a></li>
				<li><a data-avatar="avatar32" style="background-image: url('/images/avatar_icons/avatar32.png')"></a></li>
				<li><a data-avatar="avatar33" style="background-image: url('/images/avatar_icons/avatar33.png')"></a></li>
				<li><a data-avatar="avatar34" style="background-image: url('/images/avatar_icons/avatar34.png')"></a></li>
				<li><a data-avatar="avatar35" style="background-image: url('/images/avatar_icons/avatar35.png')"></a></li>
				<li><a data-avatar="avatar36" style="background-image: url('/images/avatar_icons/avatar36.png')"></a></li>
				<li><a data-avatar="avatar37" style="background-image: url('/images/avatar_icons/avatar37.png')"></a></li>
				<li><a data-avatar="avatar38" style="background-image: url('/images/avatar_icons/avatar38.png')"></a></li>
				<li><a data-avatar="avatar39" style="background-image: url('/images/avatar_icons/avatar39.png')"></a></li>
				<li><a data-avatar="avatar40" style="background-image: url('/images/avatar_icons/avatar40.png')"></a></li>
				<li><a data-avatar="avatar41" style="background-image: url('/images/avatar_icons/avatar41.png')"></a></li>
				<li><a data-avatar="avatar42" style="background-image: url('/images/avatar_icons/avatar42.png')"></a></li>
				<li><a data-avatar="avatar43" style="background-image: url('/images/avatar_icons/avatar43.png')"></a></li>
				<li><a data-avatar="avatar44" style="background-image: url('/images/avatar_icons/avatar44.png')"></a></li>
				<li><a data-avatar="avatar45" style="background-image: url('/images/avatar_icons/avatar45.png')"></a></li>
				<li><a data-avatar="avatar46" style="background-image: url('/images/avatar_icons/avatar46.png')"></a></li>
				<li><a data-avatar="avatar47" style="background-image: url('/images/avatar_icons/avatar47.png')"></a></li>
				<li><a data-avatar="avatar48" style="background-image: url('/images/avatar_icons/avatar48.png')"></a></li>
				<li><a data-avatar="avatar49" style="background-image: url('/images/avatar_icons/avatar49.png')"></a></li>
				<li><a data-avatar="avatar50" style="background-image: url('/images/avatar_icons/avatar50.png')"></a></li>
				<li><a data-avatar="avatar51" style="background-image: url('/images/avatar_icons/avatar51.png')"></a></li>
				<li><a data-avatar="avatar52" style="background-image: url('/images/avatar_icons/avatar52.png')"></a></li>
				<li><a data-avatar="avatar53" style="background-image: url('/images/avatar_icons/avatar53.png')"></a></li>
				<li><a data-avatar="avatar54" style="background-image: url('/images/avatar_icons/avatar54.png')"></a></li>
				<li><a data-avatar="avatar55" style="background-image: url('/images/avatar_icons/avatar55.png')"></a></li>
			</ul>       
        </div>
        <div class="modalFooter">
			<button class="blue" id="iconModalSave">save icon</button>
		</div>
    </div> 

    <!--  CHANGE COLOR MODAL  -->
    <div class="modalContent modalBlock" id="changeColor" style="display:none;">
		<div class="modalHeader"><span class="modalTitle">set Color</span></div>
		<div class="modalBody">

		<div id="colorpicker"></div>
	
		</div>
		<div class="modalFooter">
			<button class="purple" id="colorModalSave">save color</button>
		</div>
	</div>

    <!--  UPLOAD IMAGE MODAL  -->
    <div class="modalContent modalBlock" id="imageModal" style="display:none;">
		<div class="modalHeader"><span class="modalTitle">set Image</span></div>
		<div class="modalBody" style="min-width:260px;">
		    <input type="file" id="imageModalFileInput">
            <div id="croppieDiv"></div>
        
        </div>
		<div class="modalFooter">
			<button class="purple" id="imageModalSave">save image</button>
		</div>
	</div>

    <!--  THEME MODAL  -->
    <div class="modalContent modalBlock" id="themeModal" style="display:none">
        <div class="modalHeader"><span class="modalTitle">Communications Settings</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-insurance"></div>Update your communicaitons settings</span>
                    <div class="sectionContent">
                        <section class="formBlock dark">                          
                            <div class="formGrid">
                                <div class="settingsButtonArray">
                                    <div class="buttonSet" style="background-image:url('/images/themes/bamboo.jpg');background-position:left bottom;">
                                        <span class="themeLabelbg themeColor-bbg-Bamboo">bamboo</span>
                                        <button class="themeColor-bg-Bamboo themeButton" data-theme="themeBamboo">set bamboo</button>
                                    </div>
                                    <div class="buttonSet" style="background-image:url('/images/themes/beach.jpg');background-position:center bottom;">
                                        <span class="themeLabelbg themeColor-bbg-Beach">beach</span>
                                        <button class="themeColor-bg-Beach themeButton" data-theme="themeBeach">set beach</button>
                                    </div>		
                                    <div class="buttonSet" style="background-image:url('/images/themes/redleaf.jpg');background-position:right bottom;">
                                        <span class="themeLabelbg themeColor-bbg-Redleaf">redleaf</span>
                                        <button class="themeColor-bg-Redleaf themeButton" data-theme="themeRedleaf">set redleaf</button>
                                    </div>
                                    <div class="buttonSet" style="background-image:url('/images/themes/ocean.jpg');background-position:left bottom;">
                                        <span class="themeLabelbg themeColor-bbg-Ocean">ocean</span>
                                        <button class="themeColor-bg-Ocean themeButton" data-theme="themeOcean">set ocean</button>
                                    </div>
                                    <div class="buttonSet" style="background-image:url('/images/themes/sand.jpg');background-position:left bottom;">
                                        <span class="themeLabelbg themeColor-bbg-Sand">sand</span>
                                        <button class="themeColor-bg-Sand themeButton" data-theme="themeSand">set sand</button>
                                    </div>
                                    <div class="buttonSet" style="background-image:url('/images/themes/zenstone.jpg');background-position:right bottom;">
                                        <span class="themeLabelbg themeColor-bbg-Zenstone">zenstone</span>
                                        <button class="themeColor-bg-Zenstone themeButton" data-theme="themeZenstone">set zenstone</button>
                                    </div>	
                                    <div class="buttonSet" style="background-image:url('/images/themes/lotus.jpg');background-position:right bottom;background-position-y:230px;">
                                        <span class="themeLabelbg themeColor-bbg-Lotus">lotus</span>
                                        <button class="themeColor-bg-Lotus themeButton" data-theme="themeLotus">set lotus</button>
                                    </div>
                                    <div class="buttonSet" style="background-image:url('/images/themes/island.jpg');background-position:left bottom;background-position-y:270px;">
                                        <span class="themeLabelbg themeColor-bbg-Serenity">serenity</span>
                                        <button class="themeColor-bg-Serenity themeButton" data-theme="themeSerenity">set serenity</button>
                                    </div>		
                                    <div class="buttonSet" style="background-image:url('/images/themes/autumn.jpg');background-position:center bottom;">
                                        <span class="themeLabelbg themeColor-bbg-Autumn">autumn</span>
                                        <button class="themeColor-bg-Autumn themeButton" data-theme="themeAutumn">set autumn</button>
                                    </div>	
                                    <div class="buttonSet" style="background-image:url('/images/themes/sky.jpg');background-position:center bottom;">
                                        <span class="themeLabelbg themeColor-bbg-Sky">sky</span>
                                        <button class="themeColor-bg-Sky themeButton" data-theme="themeSky">set sky</button>
                                    </div>
                                    <div class="buttonSet" style="background-image:url('/images/themes/purplehaze.jpg');background-position:center bottom;background-position-y:170px;">
                                        <span class="themeLabelbg themeColor-bbg-Purplehaze">purplehaze</span>
                                        <button class="themeColor-bg-Purplehaze themeButton" data-theme="themePurplehaze">set purplehaze</button>
                                    </div>												
                                </div>
                        </section>
                    </div>                    
            </section>           
        </div>
    </div> 
</div>


<!-- END MODALS -->
<script>
    //modal globalVar to contain the reference to the modal 
    var modal;
    
    //THIS FUNCTION SHOULD BE easy to drop on a page, the class triggering the onclick and the modal name should be the main changes. 
    $('.showMyInfoModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            //modal ID goes here
            content: $('#myInfoModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();

        //script related to a specific content block can be added like this
        //status modal
        if(target === "#contentBlock1"){
            //scrpit for contentBlock1 
        }
        modal.open();
    });
   
    var colorPicker = new iro.ColorPicker("#colorpicker", {
        // Set the size of the color picker
        width: 280,
        // Set the initial color to pure red
        color: "#f00"
    });

    //show error message
    //$('#').parent().find('.inputError').addClass('show');
    //****** INFO PAGE *****
    $('#submitUserInfo').on('click', function(){
        var name = $('#userName').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        phone = phone.replace(/\D/g,'');
        var description = $('#description').val();
        var valid = true;

        if(name === ""){
            valid = false;
            $('#userName').parent().find('.inputError').addClass('show');
        }
        
        if(email === ""){
            valid = false;
            $('#email').parent().find('.inputError').addClass('show');
        }

        if(valid === true){
            updateInfo(name, email, phone, description);
        }
    });
    
    
    function updateInfo(name, email, phone, description) {
        $.ajax({
            type: 'POST',
            url: '/zengarden/updateInfo',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                user_id: {{ $user->id }},
                name: name,
                email: email,
                phone: phone,
                description: description
            },
            success: function (data) {
                //update user info section
                $('#currentUserName').html(name);
                $('#currentUserEmail').html(email);
                $('#currentUserPhone').html(phone);
                if(description){
                    $('#currentUserDescription').html(description);
                }
                //update modal
                $('#name').html(name);
                $('#email').html(email);
                $('#phone').html(phone);
                if(description){
                    $('#description').html(description);
                }

                modal.close();
                showAlert("Info Updated!", "confirm", 5);
            },
            error: function (data) {
                console.log(data);
                var errors = data.responseJSON;
                console.log(errors);
                showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
            }
        });
    }
    $('#submitPasswordChange').on('click', function(){
        var valid = true;
    
        var password = $('#newPassword').val();
        var password_confirm = $('#confirmNewPassword').val();
    
        if(password === "" || password_confirm === ""){
            valid = false;
            $('#newPassword').parent().find('.inputError').addClass('show');
            $('#confirmNewPassword').parent().find('.inputError').addClass('show');
        }
    
        if(password !== password_confirm){
            valid = false;
            $('#newPassword').parent().find('.inputError').addClass('show');
            $('#confirmNewPassword').parent().find('.inputError').addClass('show');
        }
    
        if(password.length < 6){
            valid = false;
            $('#newPassword').parent().find('.inputError').addClass('show');
            $('#confirmNewPassword').parent().find('.inputError').addClass('show');
        }
        if(valid === true){
            updatePassword(password, password_confirm);
        }
    });
    
    function updatePassword(password, password_confirmation){
        $.ajax({
            type: 'POST',
            url: '/zengarden/updatePassword',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                user_id: {{ $user->id }},
                password: password,
                password_confirmation: password_confirmation
            },
            success: function(data){
                $('#passwordChangedAt').html(data);
                modal.close();
                $('#passwordModal').find('input').val("");
                showAlert("Your password has been updated!", "confirm", 5);
            },
            error: function(data){
                console.log(data.responseText);
                showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
            }
        })
    }

    $('#submitCommunicationChange').on('click', function(){
        var weeklyUpdate = parseInt($("input[name='weeklyUpdate']:checked").val());
        var priority2Email = parseInt($("input[name='priority2Email']:checked").val());
        var priority3Email = parseInt($("input[name='proprity3Email']:checked").val());
        var valid = true;
        
        if(weeklyUpdate === ""){
            valid = false;
            $('#weeklyUpdateEnabled').parent().find('.inputError').addClass('show');
        }
        if(priority2Email === ""){
            valid = false;
            $('#priority2SettingEnabled').parent().find('.inputError').addClass('show');
        }
        if(priority3Email === ""){
            valid = false;
            $('#priority3SettingEnabled').parent().find('.inputError').addClass('show');
        }

        if(valid === true){
            $.ajax({
                type: 'POST',
                url: '{{ route("updateCommunicationSettings") }}',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    user_id: {{ $user->id }},
                    weeklyUpdate: weeklyUpdate,
                    priority2Email: priority2Email,
                    priority3Email: priority3Email
                },
                success: function (data){
                    if(weeklyUpdate === 1){
                        weeklyUpdate = "Enabled";
                    }else{
                        weeklyUpdate = "Disabled";
                    }
                    if(priority2Email === 1){
                        priority2Email = "Enabled";
                    }else{
                        priority2Email = "Disabled";
                    }
                    if(priority3Email === 1){
                        priority3Email = "Enabled";
                    }else{
                        priority3Email = "Disabled";
                    }
                    //communication page section
                    $('#currentWeeklySummary').html(weeklyUpdate);
                    $('#currentPriority2Emails').html(priority2Email);
                    $('#currentPriority3Emails').html(priority3Email)
                    modal.close();
                    showAlert("Communication Settings Updated!", "confirm", 5);
                },
                error: function(data){
                    console.log(data);
                    var errors = data.responseJSON;
                    console.log(errors);
                    showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
                }
            })
        }

    });

    $('#colorModalSave').on('click', function(){
        var color = colorPicker.color.hexString;
        $.ajax({
        type: 'POST',
            url: '/zengarden/setColor',
            data: {
            _token: '<?php echo csrf_token(); ?>',
            user_id: {{ $user->id }},
            color: color,
            },
            success: function(){
                modal.close();
                var avatar_number = $('#changeAvatar').find('a.selected').data('avatar');
                //setAvatarOnPage(color, avatar_number);
                console.log('success color save');
                $('.profileAvatarColor').css('background-color', color);
                $('.avatarMenu-btn').css('background-color', color);
                createAvatar(color, avatar_number);
            },
            error: function(){
                showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
            }
    
        });
    });
    
    function setStartingAvatar(){
        var current_avatar_number = $('#currentAvatarNumber').val();
        if(typeof current_avatar_number !== 'undefined'){
            $('#changeAvatar').find('a[data-avatar="' + current_avatar_number +'"').addClass('selected');
        }
    }

    setStartingAvatar();

    $('#iconModalSave').on('click', function(){
        var avatar = $('.iconSelector').find('a.selected').data('avatar');
        var avatar_number = avatar.replace('avatar', '');
    
        var color = $('.profileAvatarColor').css('background-color');
        console.log('color is set');
        createAvatar(color, avatar);
    
    });

    function createAvatar(color, icon_number){
        console.log('in create avatar');
        console.log(color);
        var canvas = document.createElement("canvas");
        canvas.width = "128";
        canvas.height = "128";
    
        var ctx = canvas.getContext("2d");
        ctx.fillStyle = color;
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        //making sure the avatar background color and the avatar div have the same color
        $('.profileAvatarColor').css('background-color', color);

        var img = document.createElement("img");
        img.setAttribute("src", "/images/avatar_icons/" + icon_number + ".png");
        //img.style.color = color;
    
        img.onload = function(){
            ctx.drawImage(img, 0, 0);
            img = canvas.toDataURL('image/png');
            $.ajax({
                type: 'POST',
                url: '/zengarden/createAvatar',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    user_id: {{ $user->id }},
                    img: img,
                    avatar_number: icon_number,
                    color: color
                },
                success: function(data){
                    console.log('createAvatar');
                    modal.close();
                    setAvatarOnPage(color, icon_number);
                    console.log(data);
                },
                error: function(data){
                    alert('error');
                    console.log(data);
                }
            })
    
        }
    }

    function setAvatarOnPage(color, avatar_number){
        console.log(color);
        $('.profileAvatarColor').css('background-color', color);
        if($('.iconSelector').find('a.selected').length > 0){
            $('.avatarIcon').css('background-image', "url('/images/avatar_icons/" + avatar_number + ".png')");
            $('.avatarMenu-btn').css('background-color', color);
            $('.avatarMenu-btn').css('background-image', "url('/images/avatar_icons/" + avatar_number + ".png')");   
        }
    
    }

    $('.iconSelector').on('click', 'a', function(){
        $('.iconSelector').find('a').removeClass('selected');
        $(this).addClass('selected');
    });

    //PHOTO UPLOAD/CROPPIE
    var croppie;
    
    $('#imageModalFileInput').on('change', function(){
        var file = $(this).prop('files')[0];
        console.log(file);
        if(file.type === "image/png" || file.type === "image/jpeg"){
            console.log('in reader');
            var reader = new FileReader();
            reader.onload = function(e){
                $('#croppieDiv').html("");
                croppie = $('#croppieDiv').croppie({
                    viewport: {
                        width: 150,
                        height: 150,
                        type: 'circle'
                    },
                    boundary: {
                        width: 150,
                        height: 150
                    },
                    mouseWheelZoom:false
                });
                croppie.croppie('bind',{
                    url: e.target.result,
                    points: []
                });
            }
            reader.readAsDataURL(file);
        }else{
            showAlert("invalid filetype", "deny", 5);
        }
    
    });
    
    $('#imageModalSave').on('click', function(){
        console.log('saving');
        croppie.croppie('result', 'canvas', 'original').then(function (img) {
            uploadPhoto(img);
        });
    });
    
    //upload a custom photo, remove the selected class from icons
    function uploadPhoto(img){
        var color = $('.profileAvatarColor').css('background-color');
        $.ajax({
            type: 'POST',
            url: '/zengarden/uploadPhoto',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                user_id: {{ $user->id }},
                img: img,
                color: color
            },
            success: function(data){
                console.log(data);
                modal.close();
                showAlert("Thanks for your upload!", "confirm", 5);
                var image_url = data + "?" + new Date().getTime();
                //$('.avatarIcon').css('background-image', '').css('background-image', 'url(' + image_url + ')');
    
                $('.profileAvatarColor').html('<div class="avatarIcon" style="background-image:url(' + img +')"></div>');
                $('.avatarMenu-btn').css('background-image', 'url(' + img + ')');
                $('.iconSelector').find('a.selected').removeClass('selected');
                //$('.avatarIcon').css('background-image', '');
                //$('.avatarIcon').css('background-image', img);
    
            },
            failure: function(data){
                modal.close();
                showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
                console.log(data);
            }
        })
    }

    $('.themeButton').on('click', function(){
        var new_theme = $(this).data('theme');
        changeTheme(new_theme);
    });
    
    function changeTheme(theme){
        $.ajax({
            type: 'POST',
            url: '/zengarden/updateZengardenTheme',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                user_id: {{ $user->id }},
                theme: theme
            },
            success: function(){
                window.location.href = "/zenboard";
                //showAlert("Your settings have been updated!", "confirm", 5);
                //redirect back to settings page
            },
            error: function(){
                showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
            }
        })
    }

</script>

@endsection


@extends('layouts.zen5_layout')
@section('maincontent')

    <script type="text/javascript" src="/js/zp/mtr-datepicker-timezones.js"></script>
    <script type="text/javascript" src="/js/zp/mtr-datepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/zp/datepicker.css?<$date format='yyyy-MM-dd_HH:mm:ss'/$>" />
    <link rel="stylesheet" type="text/css" href="/css/zp/datepicker-theme.css?<$date format='yyyy-MM-dd_HH:mm:ss'/$>" />

	<?php
		$injury = \App\Injury::where('id', $injury_id)->first();

		//$latest_mood_post =
		//$latest_experience_post =
        

		$injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();
        $injury_alert = \App\FirstReportOfInjury::where('injury_id', $injury->id)->first();

		function printTypes($injury){
		    $types = $injury->getTypes();
		    $string = "";
		    foreach($types as $type){
		        $string.= $type->type . ", ";
			}
			echo rtrim($string, ", ");
		}

		function printLocations($injury){
			$locations = $injury->getLocations();
			$string = "";
			foreach($locations as $location){
			    $string.= $location->location . ", ";
			}
			echo rtrim($string, ", ");
		}

    $claim_posts = $injury->getDiaryPosts('claim');
	if(count($claim_posts) > 0){
        $latest_claim_post = $claim_posts->last();
    }else $latest_claim_post = NULL;

    $injury_posts = $injury->getDiaryPosts('injury');
    if(count($injury_posts) > 0){
        $latest_injury_post = $injury_posts->last();
    }else $latest_injury_post = NULL;

    $diary_posts = $injury->getDiaryPosts();
    $tree_posts = $injury->getTreePosts();

    function calculatePostsChart($tree_posts){
        $chat_count = 0;
        $diary_count = 0;
        foreach($tree_posts as $post){
            if($post->type === "chatPost"){
                $chat_count++;
            }else if($post->type === "diaryPost"){
                $diary_count++;
            }
        }
        $return_array = [];
        $return_array['total_posts'] = $chat_count + $diary_count;
        $return_array['chat_count'] = $chat_count;
        $return_array['diary_count'] = $diary_count;

        if($diary_count > $chat_count){
            $return_array['setting'] = "user";
        }else if($diary_count < $chat_count){
            $return_array['setting'] = "team";
        }else{
            $return_array['setting'] = "base";
        }

        //$return_array['setting']

        return $return_array;
    }

    $post_chart_array = calculatePostsChart($tree_posts);

    function calculateProgressChart($injury){


        $days = $injury->getLength();
        if($injury->resolved === 1){
            $state = 7;
        }else if($days === 0){
            $state = 1;
        }else if(($days / 30) < 1){
            $state = 2;
        }else if(($days / 30) < 2){
            $state = 3;
        }else if(($days / 30) < 3){
            $state = 4;
        }else if(($days / 30) < 4){
            $state = 5;
        }else {
            $state = 6;
        }

        return $state;


    }

    function calculateMoodChart($injury){
        $mood_array = array();

        $diary_posts = \App\DiaryPost::where('injury_id', $injury->id)->latest()->take(5)->get();
        if($diary_posts !== NULL){
            $diary_posts = $diary_posts->reverse();

            foreach($diary_posts as $post){
                array_push($mood_array, $post->mood);
            }
        }


        while(count($mood_array) < 5){
            array_unshift($mood_array, 5);
        }

        return $mood_array;

    }

    $recent_mood_array = calculateMoodChart($injury);

    function calculateStatusChart($injury){
        $status = $injury->employee_status;
        if($status === "Hospital" || $status === "Urgent Care" || $status === "Family Practice"){
            $state = 1;
        }else if($status === "Home"){
            $state = 3;
        }else if($status === "Light Duty"){
            $state = 4;
        }else if($status === "Full Duty"){
            $state = 5;
        }else if($status === "Maximum Medical Improvement"){
            $state = 7;
        }else{
            $state = 0;
        }

        return $state;
    }

    $state = calculateStatusChart($injury);

    $costs = $injury->getClaimCosts();
    if(!empty($injury->final_medical_cost)){
        $final_costs_set = true;
    }else if(!empty($injury->final_indemnity_cost)){
        $final_costs_set = true;
    }else if(!empty($injury->final_legal_cost)){
        $final_costs_set = true;
    }else if(!empty($injury->final_misc_cost)){
        $final_costs_set = true;
    }else{
        $final_costs_set = false;
    }

    if($injury->getLength() > 99){
        $injury_duration = "99+";
    }else{
        $injury_duration = $injury->getLength();
    }


    ?>
<div class="pageContent bgimage-bgheader bghaze-ember fade-in">		
    <!--***********-->
    <div class="headerBlock" style="padding: 100px 0 20px 0;height:210px;"><!-- added this style to offset the padding occuring at top of page.need to figure out where its coming from-->
        <div class="headerContainer">
            <div class="pageHeader animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                	<span>manage injury</span>
                </div>

                <div class="pageHeader__subTitle">
                        @if($injury->resolved === 1)
                            <span class="icon"><img src="/images/icons/checkheart.png"></span><span class="subContent"><b>{{$injured_employee->name}}'s</b> injury resolved on Resolved At</span>
                        @elseif($injury->paused_for_litigation === 1)
                            <span class="icon"><img src="/images/icons/litigation.png"></span><span class="subContent"><b>{{$injured_employee->name}}'s</b> injury litigated on Litigated At</span>
                        @else
                            <span class="icon"><img src="/images/icons/heart.png"></span><span class="subContent">provide care for <b>{{$injured_employee->name}}</b></span>
                        @endif
                </div>

            </div>              
            
            <div class="pageIcon animate__animated animate__bounceInRight">
                @if($injury->resolved === 1)
                    <img src="/images/icons/heart.png">
                @elseif($injury->paused_for_litigation === 1)
                    <img src="/images/icons/time-crit.png">
                @else
                    <img src="/images/icons/stethescope.png">
                @endif
            </div>

        </div>           
	</div>

<script>
$(document).ready(function() {

    var tab = '<?php echo $tab; ?>';

    $('#zencaretabs').addClass(tab);
    $('#' + tab + 'Tab').show();

	$('#zencaretabs').on('click', 'li', function(){
		var tab_class = $(this).attr('class');
		var target = $(this).data('target');
		$('#zencaretabs').removeClass().addClass(tab_class);
		$('.zecaretabContent').hide();
		$('#' + target).show();
		
	});     
	
});    
</script>
<script>
$(document).ready(function() {

});   
</script>



	<div class="contentBlock noPad">
		<div class="container noPad">
			<div class="zen-actionPanel noHover noPad" style="min-height:620px;margin-bottom:50px;">

				<div id="zencaretabs">
					<ul>
						<li class="overview" data-target="overviewTab"><a class="tabOverview"><div class="icon icon-profile"></div><span>employee</span></a></li>
						<li class="zenguide" data-target="zenguideTab"><a class="tabZenguide"><div class="icon icon-map-signs"></div><span>zenguide</span></a></li>
						<li class="communicate" data-target="communicateTab"><a class="tabCommunicate"><div class="icon icon-comments"></div><span>communicate</span></a></li>
						<li class="team" data-target="teamTab"><a class="tabTeam"><div class="icon icon-teams"></div><span>team</span></a></li>
						<li class="claimcosts" data-target="claimcostsTab"><a class="tabClaimcosts"><div class="icon icon-claimcosts"></div><span>claim costs</span></a></li>
						<li class="appointments" data-target="appointmentsTab"><a class="tabAppointments"><div class="icon icon-calendar-o"></div><span>appointments</span></a></li>
						<li class="files" data-target="filesTab"><a class="tabFiles"><div class="icon icon-paperclip"></div><span>files</span></a></li>
					</ul>
				</div>
				
				<div class="zecaretabContent" id="overviewTab">
					@include('zencarePartials.overview')
				</div>

				<div class="zecaretabContent" id="zenguideTab" style="width:80%;margin:0 auto;">
					@include('zencarePartials.zenguide')
				</div>
										
				<div class="zecaretabContent" id="communicateTab">
					@include('zencarePartials.communicate')
				</div>
				
				<div class="zecaretabContent" id="teamTab">
					@include('zencarePartials.team')
				</div>

				<div class="zecaretabContent" id="claimcostsTab">
					@include('zencarePartials.claimcosts')
				</div>

				<div class="zecaretabContent" id="appointmentsTab">
					@include('zencarePartials.appointments')
				</div>

				<div class="zecaretabContent" id="filesTab">
					@include('zencarePartials.files')
				</div>

			</div>					
		</div>
	</div>



    
    <!-- test to show all modals -->
    <div class="buttonCloud small">
    <button class="showZencareModal" data-modalcontent="#updateCostsModal">update costs</button>
    <button class="showZencareModal" data-modalcontent="#updateStatusModal">injured status</button>
    <button class="showZencareModal" data-modalcontent="#treePostModal">message post</button>
    <button class="showZencareModal" data-modalcontent="#claimNumberModal">claim number</button>
    <button class="showZencareModal" data-modalcontent="#adjusterModal">add adjuster</button>
    <button class="showZencareModal" data-modalcontent="#employeeInfoModal">employee info</button>
    <button class="showZencareModal" data-modalcontent="#addAppointmentModal">add appointment</button>
    <button class="showZencareModal" data-modalcontent="#editAppointmentModal">edit appointment</button>
    <button class="showZencareModal" data-modalcontent="#addSummaryModal">add summary</button>
    <button class="showZencareModal" data-modalcontent="#resolveInjuryModal">resolve injury</button>
    <button class="showZencareModal" data-modalcontent="#sampleModal">SAMPLE</button>
    </div>
</div>

    @include('zencarePartials.modals')

    <script src="{{ URL::asset('/js/moment.js') }}"></script>

    <script src="{{ URL::asset('/js/combodate.js') }}"></script>

    <script>
        var modal;

        $(document).ready(function(){
            $('.statusEffectiveDate').combodate({
                firstItem: 'none',
                maxYear: (moment().year()),
                format: "MM-DD-YYYY",
                template: "D MMM YYYY",
                value: moment(),
                smartDays: true,
                yearAscending: false,
            });

            //$('#taskListStatusDate').combodate('setValue', moment().now());
        });


        var claim_litigated = false;

        function updateEmployeeStatus(status){
            $.ajax({
                type: "POST",
                url: '<?php echo route('updateEmployeeStatus'); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: '<?php echo $injury->id; ?>',
                    user_id: '<?php echo Auth::user()->id; ?>',
                    status: status
                },
                success: function(data){
                    console.log(data);
                    $('#statusSpan').html(status);
                },
                error: function(data){
                    console.log(data);
                }
            })
        }

        //TREE POSTS

        $('#submitTreePost').on('click', function(){
            //var body = document.getElementById("treePostTextArea").innerHTML;
            var body = $('#treePostInput').val();
            var error_div = $('#treePostModal').find('.inputError');
            error_div.removeClass('show');
            //addTreePost(body);
            console.log(body);
            var length = getPostLength(body);
            console.log(body);
            console.log('length = ' + length);
            if(length >= 1000){
                error_div.html("sorry, your post is too long").addClass('show');
            }else if(body === "") {
                error_div.html("nothing to submit!").addClass('show');
                showAlert("Nothing to submit!", "deny", 5);
            }else{
                newTreePost(body);
                modal.close();
                $('#treePostInput').val("");
                getPostLength("");
            }
        });

        function getPostLength(string){
            var replaced_post = string.replace(/&nbsp;/g, " ");
            console.log(replaced_post);
            var length = replaced_post.length;
            //alert(length);
            //console.log(replaced_post);
            //document.getElementById('charCount').innerHTML = length;
            if(length >= 1000){
                //return false;
                /*
                document.getElementById('charDiv').style.color = 'red';
                document.getElementById('treePostErrors').innerHTML = "Sorry, your post is too long!";
                document.getElementById('treePostErrors').style.color = "red";
                */
            }else{
                //return true;
                /*
                document.getElementById('charDiv').style.color = 'white';
                document.getElementById('treePostErrors').innerHTML = "Click an emoji to add it to your post. You may click emojis already in the post to remove them.";
                document.getElementById('treePostErrors').style.color = "white";
                */
            }
            return length;
        }

        function newTreePost(body){
            var _token = '<?php echo csrf_token(); ?>';
            var injury_id = '<?php echo $injury->id; ?>';
            var user_id = '<?php echo Auth::user()->id ?>';
            var body = body;
            $.ajax({
                type: 'POST',
                url: '<?php echo route("newTreePost") ?>',
                data: {
                    injury_id: injury_id,
                    user_id: user_id,
                    body: body,
                    _token: _token
                },
                success: function (data) {
                    getTreePosts();
                    //document.getElementById("treePostTextArea").value = "";
                    $('#treePostInput').val('');
                }
            })
        }

        //TODO:UPDATE CLAIM COSTS

        function cleanClaimValueInput(input){
            input = input.replace(/\s+/g, '');
            input = input.replace(/[^0-9.]/g, "");
            input = parseFloat(input);
            if(input === ""){
                input = 0;
            }
            return input;
        }

        $('#submitClaimCostModal').on('click', function(){
            updateClaimCosts();
        });

        function updateClaimCosts(){
            var paid_medical = cleanClaimValueInput($('#paidMedicalInput').val());
            var paid_indemnity = cleanClaimValueInput($('#paidIndemnityInput').val());
            var paid_legal = cleanClaimValueInput($('#paidLegalInput').val());
            var paid_misc = cleanClaimValueInput($('#paidMiscInput').val());
            var reserve_medical = cleanClaimValueInput($('#reserveMedicalInput').val());
            var reserve_indemnity = cleanClaimValueInput($('#reserveIndemnityInput').val());
            var reserve_legal = cleanClaimValueInput($('#reserveLegalInput').val());
            var reserve_misc = cleanClaimValueInput($('#reserveMiscInput').val());

            if(isNaN(paid_medical) || isNaN(paid_indemnity) || isNaN(paid_legal) || isNaN(paid_misc) ||
                isNaN(reserve_medical) || isNaN(reserve_indemnity) || isNaN(reserve_legal)|| isNaN(reserve_misc)){
                showAlert("Please make sure you only entered numerical values!", "deny", 5);
            }
            $.ajax({
                type: 'POST',
                url: '<?php echo route('updateClaimCosts'); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: '<?php echo $injury->id; ?>',
                    user_id: '<?php echo Auth::user()->id; ?>',
                    paid_medical: paid_medical,
                    paid_indemnity: paid_indemnity,
                    paid_legal: paid_legal,
                    paid_misc: paid_misc,
                    reserve_medical: reserve_medical,
                    reserve_indemnity: reserve_indemnity,
                    reserve_legal: reserve_legal,
                    reserve_misc: reserve_misc

                },
                success: function(data){
                    showAlert("Thanks for updating the claim costs!", "confirm", 5);

                    modal.close();

                    $('#medicalPaid').html("$" + paid_medical);
                    $('#indemnityPaid').html("$" + paid_indemnity);
                    $('#legalPaid').html("$" + paid_legal);
                    $('#miscPaid').html("$" + paid_misc);
                    $('#medicalReserve').html("$" + reserve_medical);
                    $('#indemnityReserve').html("$" + reserve_indemnity);
                    $('#legalReserve').html("$" + reserve_legal);
                    $('#miscReserve').html("$" + reserve_misc);

                    getTreePosts();


                },
                error: function(data){
                    console.log(data);
                }
            })
        }

        //UPDATE EMPLOYEE STATUS

        //NEW MODAL
        $('#zencareModal').on('click', '#submitStatusUpdate', function(){
            if(!claim_litigated){
                    // var status = $('.employeeStatusButton.selected').data('status');
                    //var status = $('.statusSelector')
                    var status = $('#employeeStatusInput').find(":selected").val();
                    var date = $('#taskListStatusDate').combodate('getValue', 'YYYY-MM-DD');
                    updateEmployeeStatus(status, date);

            }else{
                showAlert("Sorry, this claim is under litigation.", "warning", 3);
            }
        });

        function updateEmployeeStatus(status, date){
            $.ajax({
                type: 'POST',
                url: '<?php echo route("updateEmployeeStatus"); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    status: status,
                    date: date,
                    injury_id: '<?php echo $injury->id; ?>',
                },
                success: function(data){
                    showAlert("Status updated!", "confirm", 5);
                    modal.close();
                    $('#statusSpan').html(status);
                    getTreePosts();
                },
                error: function(data){
                    showAlert("Sorry, something went wrong.", "deny", 5);
                }

            })
        }

        //pause for litigation
        //NEW MODAL
        $('#zencareModal').on('click', '#claimStopButton1', function(){
            $('.litigationDiv').hide();
            $('#litigationDiv2').show();
        });

        $('#zencareModal').on('click', '#claimStopButton2', function(){
            $('.litigationDiv').hide();
            $('#litigationDiv3').show();
        });

        $('#zencareModal').on('click', '#confirmClaimStopButton', function(){
            pauseForLitigation();
        });

        function pauseForLitigation(){
            $.ajax({
                type: 'POST',
                url: '<?php echo route('pauseClaimForLitigation'); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: '<?php echo $injury->id; ?>',
                },
                success: function(data){
                    window.location.reload();
                },
                error: function(data){
                    alert('error');
                }
            });
        }


        //APPOINTMENTS
        $('#submitSummaryModal').on('click', function(){
            var appointment_id = $('#addSummaryModalAppointmentID').val();
            var summary = $('#addSummaryModal').find('textarea').val();

            addAppointmentSummary(appointment_id, summary);

        })

        $('#setApptBtn').on('click', function(){
            var name = $('#addAppointmentName').val();
            if(name === ""){
                showAlert("The name field is required!", "deny", 10);
                return false;
            }

            var email = $('#addAppointmentEmail').val();
            var phone = $('#addAppointmentPhone').val();

            /*
                if(email === "" && phone === ""){
                    showAlert("You must enter either an email address or a phone number", "deny", 10);
                    return false;
                }
            */
            var datetime = datepickerDefault.format("Y-MM-DD hh:mm A");
            var description = $('#addAppointmentNote').val();
            /*
            if(description === ""){
                showAlert("Please enter a brief description of the appointment (X-Rays, Consultation, etc)", "deny", 10);
                return false;
            }
            */

            console.log(datetime);

            addAppointment(name, email, phone, datetime, description);

        });

        $('#setFollowBtn').on('click', function(){
            var name = $('#addAppointmentName').val();
            if(name === ""){
                showAlert("The name field is required!", "deny", 10);
                return false;
            }

            var email = $('#addAppointmentEmail').val();
            var phone = $('#addAppointmentPhone').val();

            /*
                if(email === "" && phone === ""){
                    showAlert("You must enter either an email address or a phone number", "deny", 10);
                    return false;
                }
            */
            var datetime = datepickerDefault.format("Y-MM-DD hh:mm A");
            var description = $('#addAppointmentNote').val();
            /*
            if(description === ""){
                showAlert("Please enter a brief description of the appointment (X-Rays, Consultation, etc)", "deny", 10);
                return false;
            }
            */
            console.log(datetime);

            addAppointment(name, email, phone, datetime, description);

        });

        function addAppointmentSummary(appointment_id, summary){
            $.ajax({
                type: 'POST',
                url: '<?php echo route('addAppointmentSummary'); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    appointment_id: appointment_id,
                    summary: summary
                },
                success: function(data){
                    $(".apptCard[data-appointmentid='" + appointment_id + "']").find(".apptSummary").html(summary);
                    modal.close();
                    showAlert("Thanks!", "confirm", 5);
                },
                error: function(data){
                    showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
                }

            });
        }

        function addAppointment(name, email, phone, datetime, description){
            $.ajax({
                type: 'POST',
                url: '<?php echo route('addAppointment'); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: '<?php echo $injury->id; ?>',
                    name: name,
                    email: email,
                    phone: phone,
                    datetime: datetime,
                    description: description,
                    user_id: $('#userID').val()
                },
                success: function (data) {
                    window.location.href = "/zencare/" + '<?php echo $injury->id; ?>' + "/appointments";

                },
                error: function (data){
                    showAlert('Sorry, something went wrong. Please try again later', 'deny', 5);
                }
            })
        }

        function appendNewAppointment(appointment){
            if(appointment['summary'] == null){
                appointment['summary'] = "";
            }

            var html = '<div class="apptCard" data-appointmentid="' + appointment['id'] +'">' +
                '<div class="apptHeader">' +
                '<div class="apptIcon"><div class="icon inline icon-clock"></div></div>' +
                '<div class="apptTime">' + appointment['formatted_date'] + '</div>' +
                '</div>' +
                '<input type="hidden" class="apptTimeFormatted" value="' + appointment['appointment_time'] + '">' +
                '<div class="apptBody">' +
                '<div class="apptDoctor">' + appointment['name'] + '</div>' +
                '<div class="apptPhone">' + appointment['phone'] + '</div>' +
                '<div class="apptEmail">' +
                '<a href="mailto:' + appointment['email'] +'">' +
                appointment['email'] + '</a></div>' +
                '<div class="apptDescription">' + appointment['description'] + '</div>' +
                '<div class="apptSummary">' + appointment['summary'] + '</div>' +
                '<div class="apptControls">' +
                '<button data-appointmentid="' + appointment['id'] + '" class="cyan addSummaryModal">+ summary</button>' +
                '<button data-appointmentid="' + appointment['id'] + '" class="blue addAppointmentModal">+ follow-up</button>' +
                '</div>' +
                '<div class="apptEdit">' +
                '<button data-appointmentid="' + appointment['id'] + '" class="gold editAppointmentModal"><div class="icon inline icon-pencil"></div> edit</button></div>' +
                '</div>' +
                '</div>';

            //var existing_html = $('.myAppointments.hasList').html();

            if(appointment['appointment_status'] === "future"){

                $('.myAppointments.hasList').append(html);
                $('.myAppointments.noList').hide();
                $('.myAppointments.hasList').show();

            }else{
                $('.pastAppointments.hasList').append(html);
                $('.pastAppointments.noList').hide();
                $('.pastAppointments.hasList').show();
            }
        }

        function clearAppointmentModal(){
            $('#addAppointmentModal').find('input').val("");
            $('#addAppointmentNote').val("");

        }

        $('#editApptBtnSave').on('click', function(){
            var name = $('#editAppointmentName').val();

            if(name === ""){
                showAlert("The name field is required!", "deny", 10);
                return false;
            }

            var email = $('#editAppointmentEmail').val();
            var phone = $('#editAppointmentPhone').val();
            var datetime = datepickerDefault.format("Y-MM-DD hh:mm A");
            var description = $('#editAppointmentNote').val();
            var appointment_id = $('#editAppointmentID').val();

            $.ajax({
                type: 'POST',
                url: '<?php echo route('editAppointment'); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    appointment_id: appointment_id,
                    name: name,
                    email: email,
                    phone: phone,
                    datetime: datetime,
                    description: description
                },
                success: function(data){
                    modal.close();
                    var div = $('.apptCard[data-appointmentid="' + appointment_id +'"]');
                    div.find('.apptDoctor').html(name);
                    div.find('.apptPhone').html(phone);
                    div.find('.apptEmail').html("<a href='mailto:" + email +"'>" + email + "</a>");
                    div.find('.apptDescription').html(description);
                },
                error: function(data){
                    alert('error');
                }
            })

        });

        $('#submitClaimNumber').on('click', function(){
            var claimNumber = $("#claimNumberInput").val();
            var claimNumberConfirm = $("#confirmClaimNumberInput").val();

            if(claimNumber !== claimNumberConfirm){
                alert('numbers do not match');
                alert('claim number: ' + claimNumber + ' confirm number: ' + claimNumberConfirm);
                return false;
            }

            $.ajax({
				type: 'POST',
				url: '<?php echo route('addClaimNumber'); ?>',
				data: {
				    _token: '<?php echo csrf_token(); ?>',
					claim_number: claimNumber,
                    claim_number_confirm: claimNumberConfirm,
                    injury_id: {{ $injury->id }}
				},
				success: function(data){
                    showAlert("Thanks for add the claim number!", "confirm", 5);
                    $('#taskListClaimNumber').hide();
                    $('#claimNumberAdded').find('.longlabel').html('You added the claim number - ' + claimNumber);
                    $('#claimNumberAdded').show();
                    $('.injuryData.claimnumber').html(claimNumber);
				    modal.close();
				}
			});
        });

        $('#taskListInjuryAlert').on('click', function(){
            $.ajax({
                type:'POST',
                url: '<?php echo route('deliverInjuryAlert') ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: {{ $injury->id }}
                },
                success: function(data){
                    showAlert("Thanks delivering the injury alert!", "confirm", 5);
                    $('#taskListInjuryAlert').hide();
                    $('#injuryAlertDelivered').show();
                },
                error: function($xhr){
                    var data = $xhr.responseJSON;
                    console.log(data);
                    showAlert('Sorry, something went wrong. Please try again later', 'deny', 5);
                }
            });
        });

        $('#taskListOfficialReport').on('click', function(){
            $.ajax({
                type:'POST',
                url: '<?php echo route('firstOfficialReportDelivered') ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: {{ $injury->id }}
                },
                success: function(data){
                    showAlert("Thanks delivering the Official First Report!", "confirm", 5);
                    $('#taskListOfficialReport').hide();
                    $('#fullInjuryReportDelivered').show();
                },
                error: function($xhr){
                    var data = $xhr.responseJSON;
                    console.log(data);
                    showAlert('Sorry, something went wrong. Please try again later', 'deny', 5);
                }

            });
        });

        $('#submitInjuryResolved').on('click', function(){
            $.ajax({
                type:'POST',
                url: '<?php echo route('resolveInjury') ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: {{ $injury->id }}
                },
                success: function(data){
                    showAlert("Thanks resolving the Injury!", "confirm", 5);
                    modal.close();
                    $('#injuryResolved').show();
                    $('#taskListResolve').hide();
                },
                error: function($xhr){
                    var data = $xhr.responseJSON;
                    console.log(data);
                    showAlert('Sorry, something went wrong. Please try again later', 'deny', 5);
                }

            });
        });

    </script>


@endsection

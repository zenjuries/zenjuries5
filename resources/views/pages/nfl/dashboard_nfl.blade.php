@extends('layouts.zen5_layout_nfl')
@section('maincontent')


<link rel="stylesheet" href="/css/animcharts.css?<$date format='yyyy-MM-dd_HH:mm:ss'/$>">
<script src="/js/animcharts.js"></script>

<div class="pageContent themeCardinals bgimage-bgrb-f fade-in" id="zenboardPageContent" style="background-image:url('/images/themes/cardinals.jpg');min-height:800px;">

	<div class="headerBlock headerFade">
		<div class="headerContainer">
			<div class="pageHeader wow animate__animated animate__bounceInLeft">
				<div class="pageHeader__title">
				<span>Welcome to <b>zenNFLPA</b></span>
				</div>
				<div class="pageHeader__subTitle">
						<span class="icon"><img src="images/nflpa/football.png"></span><span class="subContent">logged in with <b>Arizona Cardinals</b></span>
					</div>
			</div>
			<div class="pageIcon wow animate__animated animate__bounceInRight">
				<img src="/images/nflpa/team-cardinals.png">
			</div> 
		</div>           
	</div>

	<div class="container">
		<div class="statsBar contract">
				<div class="shortStats" onclick="$('.statsBar').toggleClass('expand contract');">
					<div class="shortStatsLeft animate__animated animate__bounceInLeft">
						<ul>
							<li><span class="icon"><img src="images/icons/bandaid-simple.png"></span><span class="value">5</span></li>
							<li><span class="icon"><img src="images/icons/plus-simple.png"></span><span class="value">5</span></li>
							<li><span class="icon"><img src="images/icons/heart-simple.png"></span><span class="value">5</span></li>
						</ul>
					</div>
					<div class="shortStatsExpand">[ expand ]</div>
					<div class="shortStatsRight animate__animated animate__bounceInRight">
						<ul>
							<li><span class="icon"><img src="images/icons/time-good.png"></span><span class="value">6</span></li>
							<li><span class="icon"><img src="images/icons/date-alert.png"></span><span class="value">4d</span></li>
							<li><span class="icon"><img src="images/icons/litigation-good.png"></span><span class="value">3</span></li>
						</ul>
						<div class="shortStatsX">[ minimize ]</div>
					</div>
				</div>
				<div class="fullStats">
					<div class="statsBasic">
						<section>
							<div><span class="metricIcon animate__animated animate__bounce" style="background-image:url('images/icons/bandaid-circle.png');">6</span><span class="metricLabel">total injuries</span></div>
							<div><span class="metricIcon animate__animated animate__bounce animate__delay-1s" style="background-image:url('images/icons/plus-circle.png');">3</span><span class="metricLabel">open injuries</span></div>
							<div><span class="metricIcon animate__animated animate__bounce animate__delay-2s" style="background-image:url('images/icons/heart-circle.png');">8</span><span class="metricLabel">closed injuries</span></div>
						</section>
					</div>
					<hr>
					<div class="riskRating">
						<section>
							<div>
								<span class="riskCard animate__animated animate__headShake animate__delay-3s"><img src="images/icons/risk-low.png"></span>
							</div>
							<div>
								<table>
									<tr class="animate__animated animate__pulse animate__delay-3s"><td><span class="metricIcon" style="background-image:url('images/icons/time-good.png');line-height:78px;">4</span></td>
										<td><span class="metricLabel">average injuries reported within 24 hours</span></td></tr>
									<tr class="animate__animated animate__pulse animate__delay-4s"><td><span class="metricIcon" style="background-image:url('images/icons/date-alert.png');">6<span>days</span></span></td>
										<td><span class="metricLabel">average duration of injuries</span></td></tr>
									<tr class="animate__animated animate__pulse animate__delay-5s"><td><span class="metricIcon" style="background-image:url('images/icons/litigation-good.png');line-height:66px;">7</span></td>
										<td><span class="metricLabel">average litigation of injuries</span></td></tr>
								</table>
							</div>
						</section>
					</div>
					<hr>
					<div class="costStats">
						<span class="statTitle">claim costs</span>
						<section>
							<div><span class="vbar" style="height:80px;">$1200</span><span class="vbarLabel">open</span></div>
							<div><span class="vbar" style="height:100px;">$1500</span><span class="vbarLabel">paid</span></div>
							<div><span class="vbar" style="height:70px;">$1000</span><span class="vbarLabel">reserved</span></div>
							<div><span class="vbar" style="height:60px;">$900</span><span class="vbarLabel">closed</span></div>
							<div><span class="vbar" style="height:60px;">$1100</span><span class="vbarLabel">total incurred</span></div>
						</section>
					</div>					
				</div>	
			</div>			
			<div class="homeButtons">
				<div class="itemGrid animate__animated animate__bounceIn">			
					<div class="itemGrid__nav noSelect"><a class="navbbg-1 noLink" href="/newinjury"><span class="navImg"><img src="/images/icons/plus.png"></span><span class="subText">report new</span><span class="mainText">injury</span></a></div>
					<div class="itemGrid__nav noSelect"><a class="navbbg-2 noLink" href="/messages"><span class="navImg"><img src="/images/icons/messages.png"></span><span class="subText">view my</span><span class="mainText">messages</span></a></div>
					<div class="itemGrid__nav noSelect"><a class="navbbg-3 noLink" href="/zencarelist/open"><span class="navImg"><img src="/images/icons/injurylist.png"></span><span class="subText">view my</span><span class="mainText">injuries</span></a></div>
					<div class="itemGrid__nav noSelect"><a class="navbbg-4 noLink" href="/stats"><span class="navImg"><img src="/images/icons/stats.png"></span><span class="subText">related</span><span class="mainText">injuries</span></a></div>
					<div class="itemGrid__nav noSelect"><a class="navbbg-5 noLink" href="/help"><span class="navImg"><img src="/images/icons/help.png"></span><span class="subText">view my</span><span class="mainText">benefits</span></a></div>
					<div class="itemGrid__nav noSelect"><a class="navbbg-6 noLink" href="/documents"><span class="navImg"><img src="/images/icons/documents.png"></span><span class="subText">view my</span><span class="mainText">documents</span></a></div>												
				</div>
			</div>

			<div class="homeFavorites animate__animated animate__bounceInUp" style="display:block;">
				<section>
					<div class="favBox"><a class="noLink favItem"><span class="favTitle">injury <span style="font-weight:400;">activity</span></span>
					<span class="favContent">
 
					</span></a></div>

					<div class="favBox"><a class="noLink favItem"><span class="favTitle">user&team <span style="font-weight:400;">activity</span></span>
					<span class="favContent">
					
					</span></a></div>
					
					<div class="favBox"><a class="noLink favItem"><span class="favTitle">John Smith</span></span>
					<span class="favContent">
					
					</span></a></div>
					
					<div class="favBox"><a class="noLink favItem"><span class="favTitle">custom chart</span></span>
					<span class="favContent">

					</span></a></div>
					
					<div class="favBox"><a class="noLink favItem"><span class="favTitle">custom report</span></span>
					<span class="favContent">

					</span></a></div>

					<div class="favBox"><a class="noLink favItem"><span class="favTitle">custom report</span></span>
					<span class="favContent">

					</span></a></div>

					<div class="favBox"><a class="noLink favItem"><span class="favTitle">custom report</span></span>
					<span class="favContent">

					</span></a></div>

					<div class="favBox"><a class="noLink favItem"><span class="favTitle">custom report</span></span>
					<span class="favContent">

					</span></a></div>
				</section>

			</div>			
		</div>
	</div>
	

@endsection

@extends('layouts.zen5_layout')
@section('maincontent')
	
<div class="pageContent bgimage-bgheader pagebackground14 fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>zenjuries messages</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/magnify.png"></span><span class="subContent">view all messages</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/message.png">
            </div> 
        </div>           
	</div>

<script>
$(document).ready(function() {
  $('.toggleChat').click(function() {
    $('.messageListing').toggleClass('open');
  });
  $('.messagePost').click(function() {
    $(this).toggleClass('open');
  });  
});    
</script>

	<div class="contentBlock lessPadding">
		<div class="container setMinHeight">

        <section class="sectionPanel">
                <div class="sectionNav">
                    <div class="buttonArray inline">
                        <button class="orange"><div class="icon icon-comment"></div> new message</button>
                        <button class="blue"><div class="icon icon-filter"></div> filter</button>
                        <button class="grey toggleChat"><div class="icon icon-ellipsis-v"></div> max</button>
					</div>
                </div>
                <div class="sectionContent">
                <section class="messageListing">
                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType resolvedPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">resolvedPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reopenedPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reopenedPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost chat">
                            <div class="messageContent">
                                <span class="messageType chatPost"><span class="messageIcon" style="background-color:purple;background-image:url('images/avatar_icons/avatar12.png');"></span></span>
                                <span class="messageTitle">chatPost</span>
                                <span class="messageBody">Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus. Morbi in dui quis erat iaculis interdum. Nam sit amet eros felis. Donec laoreet quam vulputate nisl pellentesque commodo.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">hide all replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="messageReplyThread" style="display:none;">
                            <div class="messagePost chat">
                                <div class="messageContent">
                                    <span class="messageType chatPost"><span class="messageIcon" style="background-color:green;background-image:url('images/avatar_icons/avatar24.png');"></span></span>
                                    <span class="messageTitle">chatPost</span>
                                    <span class="messageBody">Morbi massa sapien, semper eget nisi nec, fringilla posuere eros. Sed aliquam lorem quis dolor venenatis laoreet.</span>
                                    <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                    <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType firstReportPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">firstReportPost</span>
                                <span class="messageBody">Proin fringilla dapibus nisi at luctus. Donec posuere id magna ut ullamcorper. Duis pretium metus diam, id porta nisi varius et. Nulla facilisi. Morbi massa sapien, semper eget nisi nec, fringilla posuere eros. Sed aliquam lorem quis dolor venenatis laoreet. Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus. Morbi in dui quis erat iaculis interdum. Nam sit amet eros felis. Donec laoreet quam vulputate nisl pellentesque commodo.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType claimNumberPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">claimNumberPost</span>
                                <span class="messageBody">Proin fringilla dapibus nisi at luctus. Donec posuere id magna ut ullamcorper. Duis pretium metus diam, id porta nisi varius et. Nulla facilisi. Morbi massa sapien, semper eget nisi nec, fringilla posuere eros. Sed aliquam lorem quis dolor venenatis laoreet.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    
                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reclassifiedPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reclassifiedPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost chat">
                            <div class="messageContent">
                                <span class="messageType diaryPost"><span class="messageIcon" style="background-color:brown;background-image:url('images/avatar_icons/avatar7.png');"></span></span>
                                <span class="messageTitle">diaryPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType valuePost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">valuePost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType systemPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">systemPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reportTimeGoodPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reportTimeGoodPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reportTimeBadPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reportTimeBadPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reportTimeOkayPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reportTimeOkayPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType finalCostPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">finalCostPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType adjusterPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">adjusterPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType notesPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">notesPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType photoPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">photoPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType filePost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">filePost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType newInjuryPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">newInjuryPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reserveBadPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reserveBadPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reserveGoodPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reserveGoodPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reminderPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reminderPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType newTaskPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">newTaskPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType completeTaskPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">completeTaskPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType taskReminderPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">taskReminderPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>                                
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType expiredTaskPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">expiredTaskPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">
                                    <div class="buttonArray inline small">
                                        <button class="orange"><div class="icon icon-commenting"></div> reply</button>
                                        <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>
                                        <button class="red"><div class="icon icon-times"></div> delete</button>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>

                </section>
                <!--
                <div class="placeHolder">
                    Message Area
                    <span>current alerts and messages go here</span>
                </div>-->
                </div>
            </section>
						
		</div>
	</div>


</div>

	


@endsection

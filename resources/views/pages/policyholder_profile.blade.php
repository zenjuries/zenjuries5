@extends('layouts.zen5_layout')
@section('maincontent')

<?php

    $requirePayment = env('REQUIRE_PAYMENTS', true); /* true, false */  

    //EDITED - TYLER
    $user = Auth::user();
    $company_id = Auth::user()->getCompany()->id;
    $company = \App\Company::where('id', $company_id)->first();
    $team = \App\Team::where('id', $company->team_id)->first();
    $owner_name = \App\User::where('id', $team->owner_id)->value('name');
    $teamID = \App\Company::where('id', $company_id)->value('team_id');
    $carrier = \App\Carrier::where('id', $company->carrier_id)->first(); 
    //$isOwner = DB::table('team_users')->where('team_id', $teamID)->
    //where('user_id', Auth::user()->id)->
    //where('role', 'owner')->first();

    $month = "Entered a renewal month";
    $day = "Entered a renewal day";
    if(!empty($company->renewal_date)){
        $date = new \Carbon\Carbon($company->renewal_date);
        $month = $date->month;
        $day = $date->day;
    }
    $carrier = \App\Carrier::where('id', $company->carrier_id)->first();

    if($team->owner_id === $user->id){
        $isOwner = true;
    }else{
        $isOwner = NULL;
    }

    if(Auth::user()->is_zagent && Auth::user()->zagent_id !== $company_id){
        $isOwner = true;
    }


    //if($isOwner !== NULL){
        $teamUsers = DB::table('team_users')->where('team_id', $teamID)->where(function($query){
        $query->where('role', 'owner')->orWhere('role', 'employee')->orWhere('role', 'agent');
        })->get();
	//}

    $intent = $company->createSetupIntent();
    if($company->stripe_id === NULL){
        $has_card = false;
    }else{$has_card = true;}

    $subscription = ($company->subscribed('ZAgent_Monthly_Fee')) ? $company->subscription('ZAgent_Monthly_Fee') : null;
    $subscriptionStripe = ($company->subscribed('ZAgent_Monthly_Fee')) ? $company->subscription('ZAgent_Monthly_Fee')->asStripeSubscription() : null;
    $billingDate = new DateTime();
    if (!is_null($subscriptionStripe)) $billingDate->setTimestamp($subscriptionStripe->current_period_end);
    $displayBillingDate = $billingDate->format('F jS, Y') ?? null;
    $billingPrice = (!is_null($subscriptionStripe)) ? number_format(($subscriptionStripe->plan->amount /100), 2, '.', ' ') : null;
    $displayBillingPrice = $billingPrice ? "$" . $billingPrice . "/mo" : null;
    // if(!is_null($company_id)){

    //     //$team_id = \App\Company::where('id', $company_id)->value('team_id');
    //     // $team_id = \App\Company::where('id', Auth::user()->getCompany()->id)->value('team_id');
    //     $owner_id = \App\Team::where('team_id', $team_id)->where('role', 'owner')->value('user_id');
    //     $owner_name = \App\User::where('id', $owner_id)->value('name');
        
    //     $teamUsers = DB::table('team_users')->where('team_id', $teamID)->where(function($query){
    //         $query->where('role', 'owner')->orWhere('role', 'employee');
    //     })->get();            
    // }
    
    //$teamID = \App\Company::where('id', Auth::user()->getCompany()->id)->value('team_id');
?>
<style>
    button.selected{background-color:#0f7688 !important;}
</style>
<div class="pageContent bgimage-bgheader pagebackground4 fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>policyholder profile</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                    <span class="icon"><img src="images/icons/edit.png"></span><span class="subContent">edit <b>{{Auth::user()->getCompany()->company_name}}</b></span>
                </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <!--<div class="companyIcon" style="background-image:url('/images/utility/companylogo.png');"></div>-->
                <img src="/images/icons/gear.png">
            </div> 
        </div>           
	</div>

<script>
$(document).ready(function() {
  $('.toggleChat').click(function() {
    $('.messageListing').toggleClass('open');
  });
  $('.messagePost').click(function() {
    $(this).toggleClass('open');
  });  
});    
</script>

	<div class="contentBlock lessPadding">
		<div class="container setMinHeight">

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-cogs"></div> administration</span>
                <div class="sectionDescription">Use this control to change the ownership of admin, the main email setting, and view/download your official zenjuries certificate.</div> 
                <div class="sectionContent">
                        <div class="buttonArray">
                            <button class="showZenModal" data-modalcontent="#editCompanyAdminModal"><div class="icon icon-cogs"></div>  change admin</button>
                            <button class="showZenModal" data-modalcontent="#editUpdateSettingsModal"><div class="icon icon-setemail"></div>  email settings</button>
                            <!-- <button class="showZenModal" data-modalcontent="#getLossReportModal"><div class="icon icon-pie-chart"></div> get loss report</button> -->
                            <button class="gold hideMobile" onclick="window.location='/cert'"><div class="icon icon-file"></div>  view certificate</button>
                        </div>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-company"></div> company details</span>
                <div class="sectionDescription">This is the set mailing address details of your company.  Make sure this is always updated and current so you will never have a lapse in your Zenjuries support.</div> 
                <div class="sectionContent">
                    <ul class="sectionInfoPanel">
                        <li><span class="label">Phone:</span> <span class="data" id="currentCompanyPhone"><?php echo $company->company_phone; ?></span></li>
                        <li><span class="label">Mailing&nbsp;Address:</span> <span class="data" id="currentCompanyMailingAdress"><?php echo $company->mailing_address; ?></span></li>
                        <li><span class="label">Mailing&nbsp;Address2:</span> <span class="data" id="currentCompanyMailingLine2"><?php echo $company->mailing_address_line_2; ?></span></li>
                        <li><span class="label">City:</span> <span class="data" id="currentCompanyMailingCity"><?php echo $company->mailing_city; ?></span></li>
                        <li><span class="label">State:</span> <span class="data" id="currentCompanyMailingState"><?php echo $company->mailing_state; ?></span></li>
                        <li><span class="label">Mailing&nbsp;Zipcode:</span> <span class="data" id="currentCompanyMailingZipcode"><?php echo $company->mailing_zip; ?></span></li>
                    </ul>
                    <br>
                    <div class="buttonArray">
                        <button class="showZenModal" data-modalcontent="#editCompanyInfoModal"><div class="icon icon-repeat"></div> update company info</button>
                    </div>   
                    
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-palette"></div> company logo & color</span>
                <div class="sectionDescription">Upload your company logo & choose your company color. Please make sure all uploaded logos are in a square format.  All logos will be cropped to a square image.</div>
                <div class="sectionContent" style="display: flex;flex-direction:column;align-items:center;">
                    <div class="profileCompanyColor" style="background-position:center;background-size:contain; background-color: {{$company->logo_color}};border-color: {{$company->logo_color}}; background-image:url({{Auth::user()->getCompany()->logo_location}}?{{ $date = str_replace(' ', '', \Carbon\Carbon::now()) }})"></div>                
                    <div class="buttonArray">
                        <button class="showZenModal" data-modalcontent="#uploadLogoModal"><div class="icon icon-profile"></div> upload your logo</button>
                        <button class="showZenModal" data-modalcontent="#companyColor"><div class="icon icon-paint-bucket"></div> choose color</button>
                    </div>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-file-text"></div> policy info</span>
                <div class="sectionDescription">This is your current policy information.  Make sure this is always kept up to date and current so zenjuries will always be able to send out your first reports with the correct information.</div> 
                <div class="sectionContent">
                    <ul class="sectionInfoPanel">
                        <li><span class="label">main&nbsp;class&nbsp;code:</span> <span class="data" id="currentClassCode">{{ $company->class_code }}</span></li>
                        <li><span class="label">policy&nbsp;number:</span> <span class="data" id="currentPolicyNumber">{{ $company->policy_number }}</span></li>
                        <li><span class="label">policy&nbsp;premium:</span> <span class="data" id="currentPolicyPremium">${{ $company->getPolicySubmission()->premium ?? '0' }}</span></li>
                        <li><span class="label">renewal&nbsp;date&nbsp;day:</span> <span class="data" id="currentRenewalDay">{{ $day }}</span></li>
                        <li><span class="label">renewal&nbsp;date&nbsp;month:</span> <span class="data" id="currentRenewalMonth">{{ $month }}</span></li>
                    </ul>
                    <br>
                    <div class="buttonArray">
                        <button class="showZenModal" data-modalcontent="#editPolicyInfoModal"><div class="icon icon-repeat"></div> update policy info</button>
                    </div>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-insurance"></div> carrier</span>
                <div class="sectionDescription">This is your carrier information.  Make sure this is always kept up to date.  If you ever change carriers or have updated information, make sure you update this immedietly.</div> 
                <div class="sectionContent">
                    <ul class="sectionInfoPanel">
                    <li><span class="label">Carrier&nbsp;Name:</span> <span class="data" id="currentCarrierName">{{(is_null($carrier) ? "" : $carrier->name)}}</span></li>
                    <li><span class="label">Carrier&nbsp;Phone:</span> <span class="data" id="currentCarrierPhone">{{(is_null($carrier) ? "" : $carrier->phone)}}</span></li>
                    <li><span class="label">Carrier&nbsp;Email:</span> <span class="data" id="currentCarrierEmail">{{ (is_null($carrier) ? "" : $carrier->email)}}</span></li>
                    </ul>
                    <br>          
                    <div class="buttonArray">
                        <button class="showZenModal" data-modalcontent="#editCarrierInfoModal"><div class="icon icon-repeat"></div> update carrier info</button>
                    </div>          
                    
                </div>
            </section>

            @if($company->pays())
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-credit-card"></div> billing</span>
                <div class="sectionDescription">This is your automatic billing information.  If your credit card changes or your billing address changes make sure you update your information here so you won't have a lapse in your zenjuries support.</div> 
                <div class="sectionContent">
                    <ul class="sectionInfoPanel">
                        <li><span class="label">Billing&nbsp;Address:</span> <span class="data" id="currentCompanyBillingAdress"><?php echo $company->billing_address; ?></span></li>
                        <li><span class="label">Billing&nbsp;Address2:</span> <span class="data" id="currentCompanyBillingLine2"><?php echo $company->billing_address_line_2; ?></span></li>
                        <li><span class="label">City:</span> <span class="data" id="currentCompanyBillingCity"><?php echo $company->billing_city; ?></span></li>
                        <li><span class="label">State:</span> <span class="data" id="currentCompanyBillingState"><?php echo $company->billing_state; ?></span></li>
                        <li><span class="label">Billing&nbsp;Zipcode:</span> <span class="data" id="currentCompanyBillingZipcode"><?php echo $company->billing_zip; ?></span></li>
                    </ul>
                    <ul class="sectionHelpPanel blue">
                        <li class="sectionPanelIcon"><img class="wow animate__animated animate__swing animate__repeat-2" src="/images/icons/creditcard.png"></li>
                        <li style="text-align:center;">
                        <span id="hasCard" {{($has_card ? "" : "style=display:none")}}>
                            {{$company->company_name}} is set to use the payment ending in <b><span id="cardLast4">{{$company->pm_last_four}}</span></b>
                        </span>
                        <span id="noCard" {{($has_card ? "style=display:none" : "")}}>
                                No card on file for {{$company->company_name}}. Add one now?
                        </span>
                        </li>   
                    </ul>           
                    <div class="buttonArray">
                        <button class="showZenModal" data-modalcontent="#editBillingInfoModal"><div class="icon icon-repeat"></div> change billing settings</button>
                    </div>
                </div>
            </section>
            @if($company->is_agency==1)
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-credit-card"></div> subscription</span>
                <div class="sectionDescription">This is your automatic subscription information.</div> 
                <div class="sectionContent">
                    <ul class="sectionInfoPanel">
                        <li><span class="label">Plan:</span> <span class="data" id="currentCompanyBillingAdress">{{(!is_null($subscription)) ? ucfirst($subscription->name) : 'None'}}</span></li>
                        <li><span class="label">Status:</span> <span class="data" id="currentCompanyBillingLine2">{{(!is_null($subscription)) ? ucfirst($subscription->stripe_status) : 'None'}}</span></li>
                        <li><span class="label">Amount:</span> <span class="data" id="currentCompanyBillingLine2">{{(!is_null($subscription)) ? $displayBillingPrice : 'None'}}</span></li>
                        <li><span class="label">Next&nbsp;Payment:</span> <span class="data" id="currentCompanyBillingLine2">{{(!is_null($subscription)) ? $displayBillingDate : 'None'}}</span></li>
                    </ul>       
                    <br>
                    <div class="buttonArray">
                        <button class="red showZenModal" data-modalcontent="#showCancelSubscription"><div class="icon icon-repeat"></div> cancel subscription</button>
                    </div>
                </div>
            </section>
            @endif
            @endif

		</div>
	</div>
</div>

<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
    @include('partials.modals.editCompanyAdmin')  
    @include('partials.modals.editCompanyInfo') 
    @include('partials.modals.editPolicyInfo') 
    @include('partials.modals.editUpdateSettings') 
    @include('partials.modals.editCarrierInfo') 
    @if($company->pays())
        @include('partials.modals.editBillingInfo') 
        @if($company->is_agency==1)
            @include('partials.modals.cancelSubscription')  
        @endif
    @endif
    @include('partials.modals.getLossReport')
	@include('partials.modals.companyColor')
	@include('partials.modals.uploadLogo')               
</div>

<script>
    $('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>

@endsection

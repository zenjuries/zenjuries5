@extends('layouts.zen5_layout_simple')
@section('maincontent')

    <?php
        if($type === "file"){
            $doc = \App\InjuryFile::where('id', $id)->first();
        }else if($type === 'image'){
            $doc = \App\InjuryImage::where('id', $id)->first();
        }
        
    ?>
<div class="center"><button onclick="history.back()">go back</button></div>
@if($type === "file")
    <iframe src="/{{$doc->location}}?{{$date = str_replace(' ', '_', \Carbon\Carbon::now())}}" type="application/pdf" width="100%" height="100%"></iframe>
@elseif($type === "image")
    <img src="{{$doc->location}}" width="100%">
@endif
<div class="center"><button onclick="history.back()">go back</button></div>
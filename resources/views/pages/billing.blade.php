@extends('layouts.zen5_layout')
@section('maincontent')

<?php
    //EDITED - TYLER
    $user = Auth::user();
    $company_id = Auth::user()->getCompany()->id;
    $company = \App\Company::where('id', $company_id)->first();
    $team = \App\Team::where('id', $company->team_id)->first();
    $owner_name = \App\User::where('id', $team->owner_id)->value('name');
    $teamID = \App\Company::where('id', Auth::user()->getCompany()->id)->value('team_id');
    $carrier = \App\Carrier::where('id', $company->carrier_id)->first(); 
    $isOwner = DB::table('team_users')->where('team_id', $teamID)->
    where('user_id', Auth::user()->id)->
    where('role', 'owner')->first();
?>    
	
<div class="pageContent bgimage-bgheader pagebackground15 fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>zenjuries billing</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/edit.png"></span><span class="subContent">edit your billing settings</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/heart.png">
            </div> 
        </div>           
	</div>


	<div class="contentBlock lessPadding">
		<div class="container setMinHeight">

        <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-credit-card"></div> billing settings</span>
            <div class="sectionContent">
                Phone: <span id="currentCompanyPhone"><?php echo $company->company_phone; ?></span><br>
                Mailing Address: <span id="currentCompanyMailingAdress"><?php echo $company->mailing_address; ?></span><br>
                Mailing Address Line Two: <span id="currentCompanyMailingLine2"><?php echo $company->mailing_address_line_2; ?></span><br>
                City: <span id="currentCompanyMailingCity"><?php echo $company->mailing_city; ?></span><br>
                State: <span id="currentCompanyMailingState"><?php echo $company->mailing_state; ?></span><br>
                Mailing Zipcode: <span id="currentCompanyMailingZipcode"><?php echo $company->mailing_zip; ?></span><br>    
                <div class="buttonArray"><button class="cyan centered showModal" data-modalcontent="#companyInfoModal">Update Billing Info</button></div>
            </div>
         </section>
						
		</div>
	</div>

    <div id="showModal" class="zModal" style="display:none">
        <div class="modalContent modalBlock" id="companyInfoModal" style="display: none"> 
          <div class="modalContent dark">
              <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/company.png">Billing Info</span></div>
              <div class="modalBody" style="min-width:260px;">
                  <section class="sectionPanel dark">
                      <section class="formBlock dark">
                          <div class="modalDescription">Enter contact info</div> 
                              <!-- your code here-->
                              <div class="formGrid">  
                                  <div class="formInput">
                                      <!-- input -->
                                      <label for="companyPhone">phone</label>
                                      <div class="inputIcon user"><input id="companyPhone" class="phone_us" type="tel" aria-label="Please enter your phone number" placeholder="ex. (111)-111-1111" value="{{ $company->company_phone }}"/></div>
                                      <span class="inputError">numbers only</span>
                                  </div>
                                  <div class="formInput">
                                      <!-- input -->
                                      <label for="mailingAddress">address line 1</label>
                                      <div class="inputIcon address"><input id="mailingAddress" type="text" value="{{ $company->mailing_address }}"/></div>
                                      <span class="inputError">--</span>
                                  </div>
                                  <div class="formInput">
                                      <!-- input -->
                                      <label for="MailingAddressLine2">address line 2</label>
                                      <div class="inputIcon address"><input id="MailingAddressLine2" type="text" value="{{ $company->mailing_address_line_2 }}"/></div>
                                      <span class="inputError">--</span>
                                  </div>
                                  <div class="formInput">
                                      <!-- input -->
                                      <label for="mailingCity">city</label>
                                      <div class="inputIcon address"><input id="mailingCity" type="text" value="{{ $company->mailing_city }}"/></div>
                                      <span class="inputError">--</span>
                                  </div>
      
                                  <div class="formInput">
                                      <label for="mailingState">state</label>
                                      <div class="inputIcon address">
                                          <select id="mailingState" name="state" value="{{ $company->mailing_state }}"><option>choose state</option><option value="Alabama">Alabama</option><option value="Alaska">Alaska</option><option value="Arizona">Arizona</option><option value="Arkansas">Arkansas</option><option value="California">California</option><option value="Colorado">Colorado</option><option value="Connecticut">Connecticut</option><option value="Delaware">Delaware</option><option value="District of Columbia">District of Columbia</option><option value="Florida">Florida</option><option value="Georgia">Georgia</option><option value="Guam">Guam</option><option value="Hawaii">Hawaii</option><option value="Idaho">Idaho</option><option value="Illinois">Illinois</option><option value="Indiana">Indiana</option><option value="Iowa">Iowa</option><option value="Kansas">Kansas</option><option value="Kentucky">Kentucky</option><option value="Louisiana">Louisiana</option><option value="Maine">Maine</option><option value="Maryland">Maryland</option><option value="Massachusetts">Massachusetts</option><option value="Michigan">Michigan</option><option value="Minnesota">Minnesota</option><option value="Mississippi">Mississippi</option><option value="Missouri">Missouri</option><option value="Montana">Montana</option><option value="Nebraska">Nebraska</option><option value="Nevada">Nevada</option><option value="New Hampshire">New Hampshire</option><option value="New Jersey">New Jersey</option><option value="New Mexico">New Mexico</option><option value="New York">New York</option><option value="North Carolina">North Carolina</option><option value="North Dakota">North Dakota</option><option value="Northern Marianas Islands">Northern Marianas Islands</option><option value="Ohio">Ohio</option><option value="Oklahoma">Oklahoma</option><option value="Oregon">Oregon</option><option value="Pennsylvania">Pennsylvania</option><option value="Puerto Rico">Puerto Rico</option><option value="Rhode Island">Rhode Island</option><option value="South Carolina">South Carolina</option><option value="South Dakota">South Dakota</option><option value="Tennessee">Tennessee</option><option value="Texas">Texas</option><option value="Utah">Utah</option><option value="Vermont">Vermont</option><option value="Virginia">Virginia</option><option value="Virgin Islands">Virgin Islands</option><option value="Washington">Washington</option><option value="West Virginia">West Virginia</option><option value="Wisconsin">Wisconsin</option><option value="Wyoming">Wyoming</option></select>
                                      </div>	
                                  </div>
      
                                  <div class="formInput">
                                      <!-- input -->
                                      <label for="mailingZip">zip</label>
                                      <div class="inputIcon address"><input id="mailingZip" type="text" value="{{ $company->mailing_zip }}"/></div>
                                      <span class="inputError">numbers only</span>
                                  </div>
                              </div>					
                              <!-- end your code here--> 
                              <div class="buttonArray">
                                  <button class="cyan centered" id="submitBillingInfo"><div class="icon icon-retweet"></div> update</button>
                              </div>            
                      </section>                 
                  </section>  
              </div> 
          </div>
        </div>
    </div>    
</div>

<script>
    //modal globalVar to contain the reference to the modal 
    var modal;
       
   //THIS FUNCTION SHOULD BE easy to drop on a page, the class triggering the onclick and the modal name should be the main changes. 
   $('.showModal').on('click', function(){
           //initialize the modal
           modal = new jBox('Modal', {
               addClass: 'zBox',
               //modal ID goes here
               content: $('#showModal'),
               isolateScroll: true
           });
           //get the content for the modal
           var target = $(this).data('modalcontent');
           //hide all content blocks
           $('.modalBlock').hide();
           //show the target
           $(target).show();
   
           //script related to a specific content block can be added like this
           //status modal
           if(target === "#contentBlock1"){
               //scrpit for contentBlock1 
           }
           modal.open();
       });

       

</script>       	


@endsection

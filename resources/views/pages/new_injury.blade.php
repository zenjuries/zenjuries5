@extends('layouts.zen5_layout')
@section('maincontent')

    <script type="text/javascript" src="/js/zp/mtr-datepicker-timezones.js"></script>
    <script type="text/javascript" src="/js/zp/mtr-datepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/zp/datepicker.css?<$date format='yyyy-MM-dd_HH:mm:ss'/$>" />
    <link rel="stylesheet" type="text/css" href="/css/zp/datepicker-theme.css?<$date format='yyyy-MM-dd_HH:mm:ss'/$>" />

    <script>

    </script>

<?php
$value = config('constants.file_storage_limit');
/*
if (Auth::user()->getCompany()->id === NULL && Auth::user()->type === "agent"){
    $company_id = session('company_id');
} else {
    $company_id = Auth::user()->getCompany()->id;
}
$company = \App\Company::where('id', $company_id)->first();
*/
$company = Auth::user()->getCompany();
$squads = \App\Squad::where('company_id', $company->id)->where('injury_squad', 0)->get();
foreach($squads as $squad){
    $members = \App\SquadMember::where('squad_id', $squad->id)->select('user_id', 'position_id')->get();
    foreach($members as $member){
        $user_name = \App\User::where('id', $member->user_id)->select('name')->get();
        $member->name = $user_name;
    }
    $squad->members = $members;
}
?>
<style>
    fieldset.infoButton{border-radius:6px;border:2px solid #ffffff30;padding:6px 18px 26px 18px;}
    fieldset.infoButton legend{text-align:center;}
    fieldset.infoButton.teamList{text-align:center;font-weight:300;font-size:1rem;margin-bottom:16px;}
    fieldset.infoButton.teamList ul.teamMember{padding:0;margin:0;list-style:none;}
    fieldset.infoButton.teamList ul.teamMember li{display:inline-block;font-size:.8rem;font-weight:900;position:relative;padding-left:18px;margin:0 4px;}
    fieldset.infoButton.teamList ul.teamMember li span.title{font-weight:300;color:#ffffff95;}
    fieldset.infoButton.teamList ul.teamMember li span.title2{font-weight:300;color:#ffffff70;font-style:italic;font-size:.7rem;}
</style>
<div class="pageContent bgimage-bgheader pagebackground8 fade-in">
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>new injury</span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/heart.png"></span><span class="subContent">report a new injury in seconds.</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/plus.png">
            </div>
        </div>
	</div>

	<div class="contentBlock noPad">
		<div class="container noPad">
            <div class="newInjuryPanel">
                <section id="claimPanel" class="sectionPanel">
                    <span class="sectionTitle"><div class="icon icon-paramedic"></div> basic information</span>
                    <div class="sectionContent">
                        <section class="formBlock dark">
                            <div class="sectionInfo">Use the input below to submit your injury for first report.  Fill in every field completely, and at the bottom of this page you will be able to submit the injury to zenjuries.<br><br>As you complete sections, you will see a checkmark on the lower right.  All sections are not required for the first report (and are noted as such) but the more information you can complete, the better.</span>
                            </div>
                            <div class="sectionInfo">Select your <b>type of claim</b>. <span style="opacity:.5;">Demo claim</span> is for training purposes.
                                <span class="inputError show" id="claimStatus" style="top:5px;left:0px;">Claim Type Required</span>
                            </div>
                            <div class="formGrid short">
                                <div class="formInput">
                                    <input type="radio" name="r1" id="newClaim" onchange="onDataChanged()" >
                                    <label for="newClaim">new claim</label>
                                </div>
                                <div class="formInput">
                                    <input type="radio" name="r1" id="preClaim" onchange="onDataChanged();">
                                    <label for="preClaim">pre-existing claim</label>
                                </div>
                                <div class="formInput">
                                    <input type="radio" name="r1" id="demoClaim" class="grey" onchange="onDataChanged();">
                                    <label for="demoClaim" style="color:grey;">demo claim</label>
                                </div>
                            </div>
                        </section>
                        <hr class="thin">
                        <section class="formBlock dark">
                            <div class="sectionInfo">Choose the <b>severity</b> of the injury.
                                <span class="inputError show" id="sevStatus" style="top:5px;left:0px;">Severity Required</span>
                            </div>
                            <div class="formGrid short">
                                <div class="formInput">
                                    <input type="radio" name="r2" id="firstAid" class="yellow" onchange="onDataChanged();">
                                    <label for="firstAid" style="color:yellow;font-weight:900;">first aid</label>
                                </div>
                                <div class="formInput">
                                    <input type="radio" name="r2" id="modInjury" class="orange" onchange="onDataChanged();">
                                    <label for="modInjury" style="color:orange;font-weight:900;">moderate injury</label>
                                </div>
                                <div class="formInput">
                                    <input type="radio" name="r2" id="sevInjury" class="red" onchange="onDataChanged();">
                                    <label for="sevInjury" style="color:red;font-weight:900;">severe injury</label>
                                </div>
                            </div>
                        </section>
                        <hr class="thin">
                        <section class="formBlock dark">
                            <div class="sectionInfo">Injured employee's information.
                                <span class="inputError show" id="infoStatus" style="top:5px;left:0px;">Gender Required</span>
                            </div>
                            <div class="formGrid">
                                <div class="formInput">
                                    <input type="radio" name="r3" id="male" class="blue" onchange="onDataChanged();">
                                    <label for="male">male</label>
                                    <input type="radio" name="r3" id="female" class="pink" onchange="onDataChanged();">
                                    <label for="female">female</label>
                                </div>
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="firstName">first name</label>
                                    <div class="inputIcon user"><input id="firstName" type="text" onblur="onDataChanged();" /></div>
                                    <span class="inputError show">First Name Required</span>
                                </div>
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="lastName">last name</label>
                                    <div class="inputIcon user"><input id="lastName" type="text" onblur="onDataChanged();" /></div>
                                    <span class="inputError show">Last Name Required</span>
                                </div>
                            </div>
                        </section>
                        <section class="formBlock dark">
                            <div class="sectionInfo">Set the Date and Time of the Injury
                                <span class="inputError show" id="dateError" style="top:5px;left:0px;">Date Required</span>
                            </div>

                            <div class="formGrid">
                                <!--<div id="injuryDate"></div>-->
                                <div class="formInput">
                                <label for="injury-datetime">date/time</label>
                                <input type="datetime-local" id="injury-datetime" name="injury-datetime"
                                    pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}" onblur="onDataChanged()">
                                </div>
                            </div>
                        </section>

                        <hr class="thin">
                        <section class="formBlock dark">
                            <div class="sectionInfo">Short summary describing injury event.</div>
                            <div class="formElement">
                                <!-- textarea -->
                                <label for="textarea">summary</label>
                                <textarea id="textarea" style="width:100%;" onblur="onDataChanged();"></textarea>
                                <span class="inputError show" style="top:-4px;">Summary Required</span>
                            </div>
                        </section>
                    </div>
                </section>

                <!-- <section id='contactPanel'class="sectionPanel">
                    <span class="sectionTitle"><div class="icon icon-phone"></div> additional information <span style="opacity:.5">(not required for initial report)</span></span>
                    <div class="sectionContent">
                    <section class="formBlock dark">
                        <div class="formGrid">
                            <div class="formInput">
                                <label for="email">employee email</label>
                                <div class="inputIcon mail"><input id="email" type="text" onblur="onDataChanged();" /></div>
                                <span class="inputError show">Please enter a valid email address</span>
                            </div>
                            <div class="formInput">
                                <label for="phone">employee mobile #</label>
                                <div class="inputIcon phone"><input id="phone" class="phone_us" type="tel" aria-label="Please enter your phone number" placeholder="ex. (111)-111-1111" onblur="onDataChanged();" /></div>
                                <span class="inputError show">Please enter a valid phone number</span>
                            </div>
                        </div>
                    </section>
                </section> -->

                <section id='injuryPanel' class="sectionPanel">
                    <span class="sectionTitle"><div class="icon icon-injury"></div> body part(s)</span>
                    <div class="sectionContent">
                        <div class="injuryLocationPicker medium">
                            <div class="pickerContainer">
                                <!--injuryGuy layers.  style "each div with "display:block" to make red area show.-->
                                <div class="layer base"></div>
                                <div class="layer systematic"></div>
                                <div class="layer head"></div>
                                <div class="layer neck"></div>
                                <div class="layer upper"></div>
                                <div class="layer r-arm"></div>
                                <div class="layer l-arm"></div>
                                <div class="layer trunk"></div>
                                <div class="layer lower"></div>
                                <div class="layer r-leg"></div>
                                <div class="layer l-leg"></div>
                                <!--buttons-->
                                <div class="action viewAll"><button class="showLocationModal" data-modalcontent="#showAll">view<br>all</button></div>
                                <div class="action systematic"><button class="showLocationModal" data-modalcontent="#showSystematic">systematic</button></div>
                                <div class="action head"><button class="showLocationModal" data-modalcontent="#showHead">head</button></div>
                                <div class="action neck"><button class="showLocationModal" data-modalcontent="#showNeck">neck</button></div>
                                <div class="action upper"><button class="showLocationModal" data-modalcontent="#showBothArms">both arms</button></div>
                                <div class="action trunk"><button class="showLocationModal" data-modalcontent="#showTrunk">trunk</button></div>
                                <div class="action arms"><button class="showLocationModal" data-modalcontent="#showRightArm">right arm</button><span></span><button class="showLocationModal" data-modalcontent="#showLeftArm">left arm</button></div>
                                <div class="action lower"><button class="showLocationModal" data-modalcontent="#showBothLegs">both legs</button></div>
                                <div class="action legs"><button class="showLocationModal" data-modalcontent="#showRightLeg">right leg</button><span></span><button  class="showLocationModal" data-modalcontent="#showLeftLeg">left leg</button></div>
                            </div>
                            <div class="selectedInjuryLocations">Location(s): <span class="locationSpan">none selected -- select an injury location</span><button class="small reset resetInjuryLocations"><div class="icon icon-repeat"></div> reset</button></div>

                        </div>
                        </div>

                </section>
                <section id='typesPanel' class="sectionPanel">
                    <span class="sectionTitle"><div class="icon icon-crutch"></div> injury type(s)</span>
                    <div class="sectionContent">
                        <div class="injuryTypePicker">
                            <div class="buttonCloud">
                                <button class="typeButton">amputation</button>
                                <button class="typeButton">asphixia</button>
                                <button class="typeButton">chemical burn</button>
                                <button class="typeButton">fire/heat burn</button>
                                <button class="typeButton">bruise</button>
                                <button class="typeButton">dermatitis</button>
                                <button class="typeButton">dislocation</button>
                                <button class="typeButton">electrical burn</button>
                                <button class="typeButton">foreign body</button>
                                <button class="typeButton">fracture</button>
                                <button class="typeButton">freezing damage</button>
                                <button class="typeButton">laceration</button>
                                <button class="typeButton">abrasion</button>
                                <button class="typeButton">puncture</button>
                                <button class="typeButton">sprain</button>
                                <button class="typeButton">breathing trouble</button>
                                <button class="typeButton">tooth trauma</button>
                                <button class="typeButton">exposure damage</button>
                                <button class="typeButton">vision loss</button>
                                <button class="typeButton">hearing loss</button>
                                <button class="typeButton">hernia, rupture</button>
                                <button class="typeButton">strain</button>
                                <button class="typeButton">pinch</button>
                                <button class="typeButton">head trauma</button>
                                <button class="typeButton">bite</button>
                                <button class="typeButton">poisoning</button>
                                <button class="typeButton">radiation</button>
                            </div>
                        </div>
                    </div>

                </section>

                <section id="teamsPanel" class="sectionPanel">
                    <span class="sectionTitle"><div class="icon icon-teams"></div> select an injury team from <b>{{$company->company_name}}</b></span>
                    <div class="sectionContent">
                        <div class="sectionInfo">You are viewing teams for <b>{{$company->company_name}}</b>  Please be sure to always choose the correct team for your injury;  double check to make sure the company you are logged into is the one you are reporting for.</b></div>
                        <br><br>
                        <!-- fix this in CSS later -->
                        <div class="teamPicker" style="height:max-content;">
                            <div class="buttonCloud">
                                @foreach ($squads as $squad)
                                    <!--<button class="teamButton" data-teamid="{{$squad->id}}">{{$squad->squad_name}}</button>-->
                                    <fieldset class="infoButton teamList">
                                        <legend><button class="teamButton" data-teamid="{{$squad->id}}"><div class="icon icon-user"></div> {{$squad->squad_name}}</button></legend>
                                        <ul class="teamMember">
                                            @foreach($squad->members as $member)
                                                @switch($member->position_id)
                                                    @case(1)
                                                        <li><span class="roleIcon role1"></span>{{$member->name[0]->name}}<span class="title"> - chief executive</span></li>
                                                        @break
                                                    @case(2)
                                                        <li><span class="roleIcon role2"></span>{{$member->name[0]->name}}<span class="title"> - medical support</span></li>
                                                        @break
                                                    @case(3)
                                                        <li><span class="roleIcon role3"></span>{{$member->name[0]->name}}<span class="title"> - physical therapist</span></li>
                                                        @break
                                                    @case(4)
                                                        <li><span class="roleIcon role4"></span>{{$member->name[0]->name}}<span class="title"> - claim manager</span></li>
                                                        @break
                                                    @case(5)
                                                        <li><span class="roleIcon role5"></span>{{$member->name[0]->name}}<span class="title"> - work comp agent</span></li>
                                                        @break
                                                    @case(6)
                                                        <li><span class="roleIcon role6"></span>{{$member->name[0]->name}}<span class="title"> - service agent</span></li>
                                                        @break
                                                    @case(7)
                                                        <li><span class="roleIcon role7"></span>{{$member->name[0]->name}}<span class="title"> - safety manager</span></li>
                                                        @break
                                                    @case(8)
                                                        <li><span class="roleIcon role8"></span>{{$member->name[0]->name}}<span class="title"> - employee supervisor</span></li>
                                                        @break
                                                    @case(9)
                                                        <li><span class="roleIcon role9"></span>{{$member->name[0]->name}}<span class="title"> - human resources</span></li>
                                                        @break
                                                    @case(10)
                                                        <li><span class="roleIcon role10"></span>{{$member->name[0]->name}}<span class="title"> - other</span></li>
                                                        @break
                                                    @default

                                                @endswitch
                                            @endforeach
                                            <!--
                                            <li><span class="roleIcon role1"></span>john smith<span class="title"> - title1</span></li>
                                            <li><span class="roleIcon role2"></span>karen jones<span class="title"> - title1</span></li>
                                            <li><span class="roleIcon role3"></span>william hornsby<span class="title"> - title1</span></li>
                                            <li><span class="roleIcon role4"></span>jermaine jackson<span class="title"> - title1</span><span class="title2"> | title2</span></li>
                                            <li><span class="roleIcon role5"></span>henry forbes</li>
                                            <li><span class="roleIcon role6"></span>tony stark</li>
                                            <li><span class="roleIcon role7"></span>jean grey<span class="title"> - title1</span><span class="title2"> | title2</span></li>
                                            <li><span class="roleIcon role8"></span>marvin sperk<span class="title"> - title1</span></li>
                                            <li><span class="roleIcon role9"></span>helen johansen</li>
                                            <li><span class="roleIcon role10"></span>bill billingsly</li>
                                            -->
                                        </ul>
                                    </fieldset>
                                @endforeach
                            </div>
                        </div>
                        <br>
                    </div>
                </section>

                <section id='imagePanel' class="sectionPanel">
                        <span class="sectionTitle"><div class="icon icon-picture"></div> include images <span style="opacity:.5">(not required for initial report)</span></span>
                        <div class="sectionContent">
                            <div class="sectionInfo hideFull" style="display:none">Use the <b>take photo</b> below to take a single image to add to your injury.  If you would like to add multiple photos, use the <b>upload image</b> button.</div>
                            <div class="sectionInfo">Use the <b>upload image</b> button to add photos to this injury. You have a limited amount of space for photos per injury, but add as many as will help in the injury case.</div>
                            <div class="center hideFull" style="display:none">
                                <div class="formGrid">
                                    <div class="formInput">
                                        <label>click to take a picture.</label>
                                        <input type="file" accept="image/*" capture="camera" multiple class="camera-upload">
                                    </div>
                                </div>
                            </div>
                            <section class="formBlock dark">
                                <div class="formElement">
                                    <button class="cyan showFileModal" data-modalcontent="#uploadImageModal">upload an image</button>
                                </div>
                                <div class="formGrid photos">
                                        <!-- upload bar- change percent on .uploadSpace for bar length, add 'warning | critical' on .uploadSpaceBar for yellow and red coloring.  change .remainingSpace value for current values.-->
                                        <div class="uploadSpaceIndicator"><div class="uploadSpaceBar critical"><div class="uploadSpace" style="width:0%;"></div></div><div class="remainingSpace">0k of 50000k</div></div>
                                        <!--<canvas id="photoCanvas"></canvas>-->
                                        <div id="photoTarget" class="center"></div>
                                </div>
                            </section>
                        </div>
                </section>

                <!--
                <section id='contactPanel' class="sectionPanel" style="display:none;">
                    <span class="sectionTitle"><div class="icon icon-shield"></div> risk management <span style="opacity:.5">(not required for initial report)</span></span>
                    <div class="sectionInfo">Choose a state, this determines the document set that is available.</div>
                    <div class="sectionContent">
                        <section class="formBlock dark">
                            <div class="formGrid">
                                <div class="formInput" style="text-align:center;width:142px;">
                                <label for="setState">Choose state</label>
                                <div class="inputIcon map" style="display:inline-block;">
                                    <select name="setState" id="setState">                
                                        <option value="select">select a state</option>
                                        <option value="AL">Alabama</option>
                                        <option value="AK">Alaska</option>
                                        <option value="AZ">Arizona</option>
                                        <option value="AR">Arkansas</option>
                                        <option value="CA">California</option>
                                        <option value="CO">Colorado</option>
                                        <option value="CT">Connecticut</option>
                                        <option value="DE">Delaware</option>
                                        <option value="DC">District Of Columbia</option>
                                        <option value="FL">Florida</option>
                                        <option value="GA">Georgia</option>
                                        <option value="HI">Hawaii</option>
                                        <option value="ID">Idaho</option>
                                        <option value="IL">Illinois</option>
                                        <option value="IN">Indiana</option>
                                        <option value="IA">Iowa</option>
                                        <option value="KS">Kansas</option>
                                        <option value="KY">Kentucky</option>
                                        <option value="LA">Louisiana</option>
                                        <option value="ME">Maine</option>
                                        <option value="MD">Maryland</option>
                                        <option value="MA">Massachusetts</option>
                                        <option value="MI">Michigan</option>
                                        <option value="MN">Minnesota</option>
                                        <option value="MS">Mississippi</option>
                                        <option value="MO">Missouri</option>
                                        <option value="MT">Montana</option>
                                        <option value="NE">Nebraska</option>
                                        <option value="NV">Nevada</option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">New Jersey</option>
                                        <option value="NM">New Mexico</option>
                                        <option value="NY">New York</option>
                                        <option value="NC">North Carolina</option>
                                        <option value="ND">North Dakota</option>
                                        <option value="OH">Ohio</option>
                                        <option value="OK">Oklahoma</option>
                                        <option value="OR">Oregon</option>
                                        <option value="PA">Pennsylvania</option>
                                        <option value="RI">Rhode Island</option>
                                        <option value="SC">South Carolina</option>
                                        <option value="SD">South Dakota</option>
                                        <option value="TN">Tennessee</option>
                                        <option value="TX">Texas</option>
                                        <option value="UT">Utah</option>
                                        <option value="VT">Vermont</option>
                                        <option value="VA">Virginia</option>
                                        <option value="WA">Washington</option>
                                        <option value="WV">West Virginia</option>
                                        <option value="WI">Wisconsin</option>
                                        <option value="WY">Wyoming</option>
                                    </select> 
                                </div>
                            </div>
                            <div class="formInput">
                            <div class="buttonArray" style="margin-top:12px;">
                                <button class="red" id="showState"><div class="icon icon-retweet"></div> select a state</button>
                                <button class="green" id="submitStateInfo" style="display:none"><div class="icon icon-retweet"></div> get user info</button>
                            </div>
                            </div>
                        </div>
                    </section>
                </section>

                <section id='contactPanel' class="sectionPanel" style="display:none;">
                    <span class="sectionTitle"><div class="icon icon-shield"></div> risk management <span style="opacity:.5">(not required for initial report)</span></span>
                    <div class="sectionInfo">Choose a state, this determines the document set that is available.</div>
                    <div class="sectionContent">
                        <section class="formBlock dark">
                            <div class="formGrid">
                                <div class="formInput" style="text-align:center;width:142px;">
                                <label for="setState">Choose state</label>
                                <div class="inputIcon map" style="display:inline-block;">
                                    <select name="setState" id="setState">                
                                        <option value="select">select a state</option>
                                        <option value="AL">Alabama</option>
                                        <option value="AK">Alaska</option>
                                        <option value="AZ">Arizona</option>
                                        <option value="AR">Arkansas</option>
                                        <option value="CA">California</option>
                                        <option value="CO">Colorado</option>
                                        <option value="CT">Connecticut</option>
                                        <option value="DE">Delaware</option>
                                        <option value="DC">District Of Columbia</option>
                                        <option value="FL">Florida</option>
                                        <option value="GA">Georgia</option>
                                        <option value="HI">Hawaii</option>
                                        <option value="ID">Idaho</option>
                                        <option value="IL">Illinois</option>
                                        <option value="IN">Indiana</option>
                                        <option value="IA">Iowa</option>
                                        <option value="KS">Kansas</option>
                                        <option value="KY">Kentucky</option>
                                        <option value="LA">Louisiana</option>
                                        <option value="ME">Maine</option>
                                        <option value="MD">Maryland</option>
                                        <option value="MA">Massachusetts</option>
                                        <option value="MI">Michigan</option>
                                        <option value="MN">Minnesota</option>
                                        <option value="MS">Mississippi</option>
                                        <option value="MO">Missouri</option>
                                        <option value="MT">Montana</option>
                                        <option value="NE">Nebraska</option>
                                        <option value="NV">Nevada</option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">New Jersey</option>
                                        <option value="NM">New Mexico</option>
                                        <option value="NY">New York</option>
                                        <option value="NC">North Carolina</option>
                                        <option value="ND">North Dakota</option>
                                        <option value="OH">Ohio</option>
                                        <option value="OK">Oklahoma</option>
                                        <option value="OR">Oregon</option>
                                        <option value="PA">Pennsylvania</option>
                                        <option value="RI">Rhode Island</option>
                                        <option value="SC">South Carolina</option>
                                        <option value="SD">South Dakota</option>
                                        <option value="TN">Tennessee</option>
                                        <option value="TX">Texas</option>
                                        <option value="UT">Utah</option>
                                        <option value="VT">Vermont</option>
                                        <option value="VA">Virginia</option>
                                        <option value="WA">Washington</option>
                                        <option value="WV">West Virginia</option>
                                        <option value="WI">Wisconsin</option>
                                        <option value="WY">Wyoming</option>
                                    </select> 
                                </div>
                            </div>
                            <div class="formInput">
                            <div class="buttonArray" style="margin-top:12px;">
                                <button class="red" id="showState"><div class="icon icon-retweet"></div> select a state</button>
                                <button class="green" id="submitStateInfo" style="display:none"><div class="icon icon-retweet"></div> get user info</button>
                            </div>
                            </div>
                        </div>
                    </section>
                </section>
                -->

                <section id='contactPanel' class="sectionPanel">
                    <span class="sectionTitle"><div class="icon icon-phone"></div> additional information <span style="opacity:.5">(not required for initial report)</span></span>
                    <div class="sectionContent">
                    <section class="formBlock dark">
                        <div class="formGrid">
                            <div class="formInput">
                                <!-- input -->
                                <label for="email">employee email</label>
                                <div class="inputIcon mail"><input id="email" type="text" onblur="onDataChanged();" /></div>
                                <span class="inputError show">Please enter a valid email address</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="phone">employee mobile #</label>
                                <div class="inputIcon phone"><input id="phone" class="phone_us" type="tel" aria-label="Please enter your phone number" placeholder="ex. (111)-111-1111" onblur="onDataChanged();" /></div>
                                <span class="inputError show">Please enter a valid phone number</span>
                            </div>
                        </div>
                    </section>
                </section>

                <section id='reviewPanel' class="sectionPanel">
                    <span class="sectionTitle"><div class="icon icon-firstreport2"></div> review & submit</span>
                    <div class="sectionContent">
                        
                        <div class="sectionInfo">Review all of your input and make sure everything is correct.  <br><span class="FG__ltred TXT__bold">Red items</span> indicate required elements, and the submit button will appear when all requirements are met. When you're satisfied, submit the first report!</div>


                        <div class="itemGrid">

                            <section class="sectionPanel" style="max-width:300px;">
                                <span class="sectionTitle small"><div class="icon icon-paramedic"></div>Basic Information</span>
                                <div class="sectionContent">

                                    <div class="formDataBlock">
                                        <span class="outputLabel" id="claimLabel">claim type:</span><br>
                                        <span id="set_claimtype" class="outputData">not set</span>
                                    </div>

                                    <div class="formDataBlock">           
                                        <span class="outputLabel" id="sevLabel">injury severity:</span><br>
                                        <span id="set_injuryseverity" class="outputData">not set</span>
                                    </div>  

                                    <div class="formDataBlock">
                                        <span class="outputLabel" id="nameLabel">employee name:</span><br>
                                        <span id="set_employeename" class="outputData">not set</span>
                                    </div>  

                                    <div class="formDataBlock">
                                        <span class="outputLabel" id="genLabel">gender:</span><br>
                                        <span id="set_employeegender" class="outputData">not set</span>
                                    </div>

                                    <div class="formDataBlock">
                                        <span class="outputLabel" id="dateLabel">date/time:</span><br>
                                        <span id="set_injurydatetime" class="outputData">no time</span>
                                    </div>

                                    <div class="formDataBlock">   
                                        <span class="outputLabel" id="summaryLabel">summary:</span><br>
                                        <span id="set_textarea" class="outputData">no summary</span>
                                    </div>

                                </div>    
                            </section>

                            <section class="sectionPanel" style="max-width:300px;">
                                <span class="sectionTitle small"><div class="icon icon-injury"></div>Injury Information</span>
                                <div class="sectionContent">
                                    <div class="formDataBlock">
                                        <span class="outputLabel" id="locLabel">injury location(s):</span><br>
                                        <span id="set_injurylocationarray" class="outputData locationSpan">not set</span>
                                    </div>

                                    <div class="formDataBlock">
                                        <span class="outputLabel" id="typeLabel">injury type(s):</span><br>
                                        <span id="set_injurytypearray" class="outputData">not set</span>
                                    </div>

                                    <div class="formDataBlock">
                                        <span class="outputLabel" id="teamLabel">injury team:</span><br>
                                        <span id="set_injuryteam" class="outputData">not set</span>
                                    </div>

                                    <div class="formDataBlock">
                                        <span class="outputLabel" id="imgLabel">included images:</span><br>
                                        <span id="set_imagearray" class="outputData">no images</span>
                                    </div>

                                </div>
                            </section>

                            <section class="sectionPanel" style="max-width:300px;">
                                <span class="sectionTitle small"><div class="icon icon-question"></div>Additional Info</span>
                                <div class="sectionContent">

                                    <div class="formDataBlock">
                                        <span class="outputLabel" id="emailLabel">email:</span><br>
                                        <span id="set_employeemail" class="outputData">not set</span>
                                    </div>

                                    <div class="formDataBlock">
                                        <span class="outputLabel" id="mobileLabel">mobile #:</span><br>
                                        <span id="set_employeemobile" class="outputData">not set</span>
                                    </div>

                                </div>
                            </section>

                        </div> 

                        <hr>

                        <div class="buttonArray">
                            <button class="red" id="showMessage"><div class="icon icon-retweet"></div> enter all required info</button>
                            <button class="green" id="submitButton" style="display:none"><div class="icon icon-retweet"></div> submit first report</button>
                        </div>


                    </div>
                </section>

            </div>
		</div>
	</div>
</div>



<!-- BASE MODAL HTML -->
<div id="locationModal" class="zModal" style="display:none">
    <!-- this is filled with each location modal data -->
    <!-- SWAPPABLE CONTENT, with IDs that match the modalcontent attribute for its respective button -->
    <!-- using the class "selected" will make the button red.  use that to signify something user picked.-->
    <div class="modalContent modalBlock" id="showAll" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle underlined"><b>systematic</b></span>
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="800 body system" data-bodyareas="systematic">body system</button>
                        <button class="locationButton" data-naiscscode="999 nonclassifiable" data-bodyareas="systematic">nonclassifiable</button>
                        <button class="locationButton" data-naiscscode="700 multiple parts" data-bodyareas="systematic">multiple parts</button>
                        <button class="locationButton" data-naiscscode="900 body parts" data-bodyareas="systematic">body parts</button>
                        <button class="locationButton" data-naiscscode="000 death" data-bodyareas="systematic">death</button>
                    </div>
                    <span class="sectionTitle underlined"><b>head</b></span>
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="100 head" data-bodyareas="head">head</button>
                        <button class="locationButton" data-naiscscode="110 brain" data-bodyareas="head">brain</button>
                        <button class="locationButton" data-naiscscode="120 ear(s)" data-bodyareas="head">ear(s)</button>
                        <button class="locationButton" data-naiscscode="121 ear(s) external" data-bodyareas="head">ear(s) external</button>
                        <button class="locationButton" data-naiscscode="122 lef ear external" data-bodyareas="head">left ear external</button>
                        <button class="locationButton" data-naiscscode="123 right ear external" data-bodyareas="head">right ear external</button>
                        <button class="locationButton" data-naiscscode="124 ear(s) internal" data-bodyareas="head">ear(s) internal</button>
                        <button class="locationButton" data-naiscscode="125 left ear internal" data-bodyareas="head">left ear internal</button>
                        <button class="locationButton" data-naiscscode="126 right ear internal" data-bodyareas="head">right ear internal</button>
                        <button class="locationButton" data-naiscscode="130 eye(s)" data-bodyareas="head">eye(s)</button>
                        <button class="locationButton" data-naiscscode="132 left eye" data-bodyareas="head">left eye</button>
                        <button class="locationButton" data-naiscscode="134 right eye" data-bodyareas="head">right eye</button>
                        <button class="locationButton" data-naiscscode="140 face" data-bodyareas="head">face</button>
                        <button class="locationButton" data-naiscscode="141 jaw" data-bodyareas="head" >jaw</button>
                        <button class="locationButton" data-naiscscode="144 mouth" data-bodyareas="head">mouth</button>
                        <button class="locationButton" data-naiscscode="146 nose" data-bodyareas="head">nose</button>
                        <button class="locationButton" data-naiscscode="148 face, multi-part" data-bodyareas="head">face, multi-part</button>
                        <button class="locationButton" data-naiscscode="149 face, NEC" data-bodyareas="head">face, NEC</button>
                        <button class="locationButton" data-naiscscode="150 scalp" data-bodyareas="head">scalp</button>
                        <button class="locationButton" data-naiscscode="160 skull" data-bodyareas="head">skull</button>
                        <button class="locationButton" data-naiscscode="198 head, multi-part" data-bodyareas="head">head, multi-part</button>
                        <button class="locationButton" data-naiscscode="199 head, NEC" data-bodyareas="head">head, NEC</button>
                    </div>
                    <span class="sectionTitle underlined"><b>neck</b></span>
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="200 neck">neck</button>
                    </div>
                    <span class="sectionTitle underlined"><b>right arm</b></span>
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="304 right arm" data-bodyareas="r-arm">right arm</button>
                        <button class="locationButton" data-naiscscode="300 upper right arm" data-bodyareas="r-arm">upper right arm</button>
                        <button class="locationButton" data-naiscscode="314 right elbow" data-bodyareas="r-arm">right elbow</button>
                        <button class="locationButton" data-naiscscode="317 right forearm" data-bodyareas="r-arm">right forearm</button>
                        <button class="locationButton" data-naiscscode="324 right wrist" data-bodyareas="r-arm">right wrist</button>
                        <button class="locationButton" data-naiscscode="334 right hand" data-bodyareas="r-arm">right hand</button>
                        <button class="locationButton" data-naiscscode="344 right fingers, thumb" data-bodyareas="r-arm">right fingers, thumb</button>
                        <button class="locationButton" data-naiscscode="397 right lower extremities" data-bodyareas="r-arm">right lower extremities</button>
                    </div>
                    <span class="sectionTitle underlined"><b>left arm</b></span>
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="302 left arm" data-bodyareas="l-arm">left arm</button>
                        <button class="locationButton" data-naiscscode="308 upper left arm" data-bodyareas="l-arm">upper left arm</button>
                        <button class="locationButton" data-naiscscode="312 left elbow" data-bodyareas="l-arm">left elbow</button>
                        <button class="locationButton" data-naiscscode="316 left forearm" data-bodyareas="l-arm">left forearm</button>
                        <button class="locationButton" data-naiscscode="322 left wrist" data-bodyareas="l-arm">left wrist</button>
                        <button class="locationButton" data-naiscscode="332 left hand" data-bodyareas="l-arm">left hand</button>
                        <button class="locationButton" data-naiscscode="342 left fingers, thumb" data-bodyareas="l-arm">left fingers, thumb</button>
                        <button class="locationButton" data-naiscscode="396 left lower extremities" data-bodyareas="l-arm">left lower extremities</button>
                    </div>
                    <span class="sectionTitle underlined"><b>trunk</b></span>
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="400 trunk" data-bodyareas="trunk">trunk</button>
                        <button class="locationButton" data-naiscscode="410 abdomen" data-bodyareas="trunk">abdomen</button>
                        <button class="locationButton" data-naiscscode="420 back" data-bodyareas="trunk">back</button>
                        <button class="locationButton" data-naiscscode="430 chest" data-bodyareas="trunk">chest</button>
                        <button class="locationButton" data-naiscscode="440 hips, both" data-bodyareas="trunk">hips, both</button>
                        <button class="locationButton" data-naiscscode="442 left hip" data-bodyareas="trunk">left hip</button>
                        <button class="locationButton" data-naiscscode="444 right hip" data-bodyareas="trunk">right hip</button>
                        <button class="locationButton" data-naiscscode="450 shoulders, both" data-bodyareas="trunk">shoulders, both</button>
                        <button class="locationButton" data-naiscscode="452 left shoulder" data-bodyareas="trunk">left shoulder</button>
                        <button class="locationButton" data-naiscscode="454 right shoulder" data-bodyareas="trunk">right shoulder</button>
                        <button class="locationButton" data-naiscscode="498 trunk, multi-part" data-bodyareas="trunk">trunk, multi-part</button>
                        <button class="locationButton" data-naiscscode="499 trunk, NEC" data-bodyareas="trunk">trunk, NEC</button>
                    </div>
                    <span class="sectionTitle underlined"><b>both arms</b> (upper extremities)</span>
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="310 arms" data-bodyareas="r-arm l-arm">arms</button>
                        <button class="locationButton" data-naiscscode="311 upper arms" data-bodyareas="r-arm l-arm">upper arms</button>
                        <button class="locationButton" data-naiscscode="313 elbows" data-bodyareas="r-arm l-arm">elbows</button>
                        <button class="locationButton" data-naiscscode="315 forearms" data-bodyareas="r-arm l-arm">forearms</button>
                        <button class="locationButton" data-naiscscode="318 arms, multi-part" data-bodyareas="r-arm l-arm">arms, multi-part</button>
                        <button class="locationButton" data-naiscscode="319 arms, NEC" data-bodyareas="r-arm l-arm">arms, NEC</button>
                        <button class="locationButton" data-naiscscode="320 wrists" data-bodyareas="r-arm l-arm">wrists</button>
                        <button class="locationButton" data-naiscscode="330 hands" data-bodyareas="r-arm l-arm">hands</button>
                        <button class="locationButton" data-naiscscode="340 fingers, thumbs" data-bodyareas="r-arm l-arm">fingers, thumbs</button>
                        <button class="locationButton" data-naiscscode="398 upper extremities" data-bodyareas="r-arm l-arm">upper extremities</button>
                        <button class="locationButton" data-naiscscode="399 upper extremities, NEC" data-bodyareas="r-arm l-arm">upper extremities, NEC</button>
                    </div>
                    <span class="sectionTitle underlined"><b>right leg</b></span>
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="508 right leg" data-bodyareas="r-leg">right leg</button>
                        <button class="locationButton" data-naiscscode="514 right knee" data-bodyareas="r-leg">right knee</button>
                        <button class="locationButton" data-naiscscode="517 right shin" data-bodyareas="r-leg">right shin</button>
                        <button class="locationButton" data-naiscscode="524 right ankle" data-bodyareas="r-leg">right ankle</button>
                        <button class="locationButton" data-naiscscode="534 right foot" data-bodyareas="r-leg">right foot</button>
                        <button class="locationButton" data-naiscscode="542 right toes" data-bodyareas="r-leg">right toes</button>
                        <button class="locationButton" data-naiscscode="597 lower extremities, right" data-bodyareas="r-leg">lower extremities, right</button>
                    </div>
                    <span class="sectionTitle underlined"><b>left leg</b></span>
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="507 left leg" data-bodyareas="l-leg">left leg</button>
                        <button class="locationButton" data-naiscscode="512 left knee" data-bodyareas="l-leg">left knee</button>
                        <button class="locationButton" data-naiscscode="516 left shin" data-bodyareas="l-leg">left shin</button>
                        <button class="locationButton" data-naiscscode="522 left ankle" data-bodyareas="l-leg">left ankle</button>
                        <button class="locationButton" data-naiscscode="532 left foot" data-bodyareas="l-leg">left foot</button>
                        <button class="locationButton" data-naiscscode="542 left toes" data-bodyareas="l-leg">left toes</button>
                        <button class="locationButton" data-naiscscode="596 lower extremities, left" data-bodyareas="l-leg">lower extremities, left</button>
                    </div>
                    <span class="sectionTitle underlined"><b>both legs</b> (lower extremities)</span>
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="510 both legs" data-bodyareas="r-leg l-leg">both legs</button>
                        <button class="locationButton" data-naiscscode="513 both knees" data-bodyareas="r-leg l-leg">both knees</button>
                        <button class="locationButton" data-naiscscode="515 lower legs" data-bodyareas="r-leg l-leg">lower legs</button>
                        <button class="locationButton" data-naiscscode="518 legs, multi-part" data-bodyareas="r-leg l-leg">legs, multi-part</button>
                        <button class="locationButton" data-naiscscode="520 both ankles" data-bodyareas="r-leg l-leg">both ankles</button>
                        <button class="locationButton" data-naiscscode="530 both feet" data-bodyareas="r-leg l-leg">both feet</button>
                        <button class="locationButton" data-naiscscode="540 all toes" data-bodyareas="r-leg l-leg">all toes</button>
                        <button class="locationButton" data-naiscscode="598 lower extremities, multi" data-bodyareas="r-leg l-leg">lower extremities, multi</button>
                        <button class="locationButton" data-naiscscode="598 lower extremities, NEC" data-bodyareas="r-leg l-leg">lower extremities, NEC</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;margin-bottom:60px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                        </div>
                    </div>
                    <br><br>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showSystematic" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>systematic</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <div class="buttonCloud">
                            <button class="locationButton" data-naiscscode="800 body system" data-bodyareas="systematic">body system</button>
                            <button class="locationButton" data-naiscscode="999 nonclassifiable" data-bodyareas="systematic">nonclassifiable</button>
                            <button class="locationButton" data-naiscscode="700 multiple parts" data-bodyareas="systematic">multiple parts</button>
                            <button class="locationButton" data-naiscscode="900 body parts" data-bodyareas="systematic">body parts</button>
                            <button class="locationButton" data-naiscscode="000 death" data-bodyareas="systematic">death</button>
                        </div>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showHead" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>head</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="100 head" data-bodyareas="head">head</button>
                        <button class="locationButton" data-naiscscode="110 brain" data-bodyareas="head">brain</button>
                        <button class="locationButton" data-naiscscode="120 ear(s)" data-bodyareas="head">ear(s)</button>
                        <button class="locationButton" data-naiscscode="121 ear(s) external" data-bodyareas="head">ear(s) external</button>
                        <button class="locationButton" data-naiscscode="122 left ear external" data-bodyareas="head">left ear external</button>
                        <button class="locationButton" data-naiscscode="123 right ear external" data-bodyareas="head">right ear external</button>
                        <button class="locationButton" data-naiscscode="124 ear(s) internal" data-bodyareas="head">ear(s) internal</button>
                        <button class="locationButton" data-naiscscode="125 left ear internal" data-bodyareas="head">left ear internal</button>
                        <button class="locationButton" data-naiscscode="126 right ear internal" data-bodyareas="head">right ear internal</button>
                        <button class="locationButton" data-naiscscode="130 eye(s)" data-bodyareas="head">eye(s)</button>
                        <button class="locationButton" data-naiscscode="132 left eye" data-bodyareas="head">left eye</button>
                        <button class="locationButton" data-naiscscode="134 right eye" data-bodyareas="head">right eye</button>
                        <button class="locationButton" data-naiscscode="140 face" data-bodyareas="head">face</button>
                        <button class="locationButton" data-naiscscode="141 jaw" data-bodyareas="head">jaw</button>
                        <button class="locationButton" data-naiscscode="144 mouth" data-bodyareas="head">mouth</button>
                        <button class="locationButton" data-naiscscode="146 nose" data-bodyareas="head">nose</button>
                        <button class="locationButton" data-naiscscode="148 face, multi-part" data-bodyareas="head">face, multi-part</button>
                        <button class="locationButton" data-naiscscode="149 face, NEC" data-bodyareas="head">face, NEC</button>
                        <button class="locationButton" data-naiscscode="150 scalp" data-bodyareas="head">scalp</button>
                        <button class="locationButton" data-naiscscode="160 skull" data-bodyareas="head">skull</button>
                        <button class="locationButton" data-naiscscode="198 head, multi-part" data-bodyareas="head">head, multi-part</button>
                        <button class="locationButton" data-naiscscode="199 head, NEC" data-bodyareas="head">head, NEC</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showNeck" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>neck</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="data-naiscscode=200 neck" data-bodyareas="neck">neck</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showRightArm" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>right arm</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="304 right arm" data-bodyareas="r-arm">right arm</button>
                        <button class="locationButton" data-naiscscode="309 upper right arm" data-bodyareas="r-arm">upper right arm</button>
                        <button class="locationButton" data-naiscscode="314 right elbow" data-bodyareas="r-arm">right elbow</button>
                        <button class="locationButton" data-naiscscode="317 right forearm" data-bodyareas="r-arm">right forearm</button>
                        <button class="locationButton" data-naiscscode="324 right wrist" data-bodyareas="r-arm">right wrist</button>
                        <button class="locationButton" data-naiscscode="334 right hand" data-bodyareas="r-arm">right hand</button>
                        <button class="locationButton" data-naiscscode="344 right fingers, thumb" data-bodyareas="r-arm">right fingers, thumb</button>
                        <button class="locationButton" data-naiscscode="397 right lower extremities" data-bodyareas="r-arm">right lower extremities</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showLeftArm" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>left arm</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="302 left arm" data-bodyareas="l-arm">left arm</button>
                        <button class="locationButton" data-naiscscode="308 upper left arm" data-bodyareas="l-arm">upper left arm</button>
                        <button class="locationButton" data-naiscscode="312 left elbow" data-bodyareas="l-arm">left elbow</button>
                        <button class="locationButton" data-naiscscode="316 left forearm" data-bodyareas="l-arm">left forearm</button>
                        <button class="locationButton" data-naiscscode="322 left wrist" data-bodyareas="l-arm">left wrist</button>
                        <button class="locationButton" data-naiscscode="332 left hand" data-bodyareas="l-arm">left hand</button>
                        <button class="locationButton" data-naiscscode="342 left fingers, thumb" data-bodyareas="l-arm">left fingers, thumb</button>
                        <button class="locationButton" data-naiscscode="396 left lower extremities" data-bodyareas="l-arm">left lower extremities</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showTrunk" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>trunk</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="400 trunk" data-bodyareas="trunk">trunk</button>
                        <button class="locationButton" data-naiscscode="410 abdomen" data-bodyareas="trunk">abdomen</button>
                        <button class="locationButton" data-naiscscode="420 back" data-bodyareas="trunk">back</button>
                        <button class="locationButton" data-naiscscode="430 chest" data-bodyareas="trunk">chest</button>
                        <button class="locationButton" data-naiscscode="440 hips, both" data-bodyareas="trunk">hips, both</button>
                        <button class="locationButton" data-naiscscode="442 left hip" data-bodyareas="trunk">left hip</button>
                        <button class="locationButton" data-naiscscode="444 right hip" data-bodyareas="trunk">right hip</button>
                        <button class="locationButton" data-naiscscode="450 shoulders, both" data-bodyareas="trunk">shoulders, both</button>
                        <button class="locationButton" data-naiscscode="452 left shoulder" data-bodyareas="trunk">left shoulder</button>
                        <button class="locationButton" data-naiscscode="454 right shoulder" data-bodyareas="trunk">right shoulder</button>
                        <button class="locationButton" data-naiscscode="498 trunk, multi-part" data-bodyareas="trunk">trunk, multi-part</button>
                        <button class="locationButton" data-naiscscode="499 trunk, NEC" data-bodyareas="trunk">trunk, NEC</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showBothArms" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>both arms</b> (upper extremities)</span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="310 arms" data-bodyareas="r-arm l-arm">arms</button>
                        <button class="locationButton" data-naiscscode="311 upper arms" data-bodyareas="r-arm l-arm">upper arms</button>
                        <button class="locationButton" data-naiscscode="313 elbows" data-bodyareas="r-arm l-arm">elbows</button>
                        <button class="locationButton" data-naiscscode="315 forearms" data-bodyareas="r-arm l-arm">forearms</button>
                        <button class="locationButton" data-naiscscode="318 arms, multi-part" data-bodyareas="r-arm l-arm">arms, multi-part</button>
                        <button class="locationButton" data-naiscscode="319 arms, NEC" data-bodyareas="r-arm l-arm">arms, NEC</button>
                        <button class="locationButton" data-naiscscode="320 wrists" data-bodyareas="r-arm l-arm">wrists</button>
                        <button class="locationButton" data-naiscscode="330 hands" data-bodyareas="r-arm l-arm">hands</button>
                        <button class="locationButton" data-naiscscode="340 fingers, thumbs" data-bodyareas="r-arm l-arm">fingers, thumbs</button>
                        <button class="locationButton" data-naiscscode="398 upper extremities" data-bodyareas="r-arm l-arm">upper extremities</button>
                        <button class="locationButton" data-naiscscode="399 upper extremities, NEC" data-bodyareas="r-arm l-arm">upper extremities, NEC</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showRightLeg" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>right leg</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="508 right leg" data-bodyareas="r-leg">right leg</button>
                        <button class="locationButton" data-naiscscode="514 right knee" data-bodyareas="r-leg">right knee</button>
                        <button class="locationButton" data-naiscscode="517 right shin" data-bodyareas="r-leg">right shin</button>
                        <button class="locationButton" data-naiscscode="524 right ankle" data-bodyareas="r-leg">right ankle</button>
                        <button class="locationButton" data-naiscscode="534 right foot" data-bodyareas="r-leg">right foot</button>
                        <button class="locationButton" data-naiscscode="542 right toes" data-bodyareas="r-leg">right toes</button>
                        <button class="locationButton" data-naiscscode="597 lower extremities, right" data-bodyareas="r-leg">lower extremities, right</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showLeftLeg" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>left leg</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="508 left leg" data-bodyareas="l-leg">left leg</button>
                        <button class="locationButton" data-naiscscode="514 left knee" data-bodyareas="l-leg">left knee</button>
                        <button class="locationButton" data-naiscscode="522 left ankle" data-bodyareas="l-leg">left ankle</button>
                        <button class="locationButton" data-naiscscode="532 left foot" data-bodyareas="l-leg">left foot</button>
                        <button class="locationButton" data-naiscscode="542 left foot toes" data-bodyareas="l-leg">left foot toes</button>
                        <button class="locationButton" data-naiscscode="596 left lower extremities" data-bodyareas="l-leg">left lower extremities</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showBothLegs" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>both legs</b> (lower extremities)</span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="510 both legs" data-bodyareas="r-leg l-leg">both legs</button>
                        <button class="locationButton" data-naiscscode="513 both knees" data-bodyareas="r-leg l-leg">both knees</button>
                        <button class="locationButton" data-naiscscode="515 lower legs" data-bodyareas="r-leg l-leg">lower legs</button>
                        <button class="locationButton" data-naiscscode="518 legs, multi-part" data-bodyareas="r-leg l-leg">legs, multi-part</button>
                        <button class="locationButton" data-naiscscode="520 both ankles" data-bodyareas="r-leg l-leg">both ankles</button>
                        <button class="locationButton" data-naiscscode="530 both feet" data-bodyareas="r-leg l-leg">both feet</button>
                        <button class="locationButton" data-naiscscode="540 all toes" data-bodyareas="r-leg l-leg">all toes</button>
                        <button class="locationButton" data-naiscscode="598 lower extremities, multi" data-bodyareas="r-leg l-leg">lower extremities, multi</button>
                        <button class="locationButton" data-naiscscode="598 lower extremities, NEC" data-bodyareas="r-leg l-leg">lower extremities, NEC</button>
                        <button class="locationButton" data-naiscscode="599 lower extremities (all)" data-bodyareas="r-leg l-leg">lower extremities (all)</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div id="fileModal" class="zModal" style="display:none">
    <!-- upload Image modal -->
    <div class="modalContent modalBlock wide" id="uploadImageModal" style="display: none">
        <div class="modalHeader"><span class="modalTitle"><div class="icon inline icon-picture"></div> upload image</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="formBlock dark">
                <div class="formInput">
                    <label for="photoUpload">Choose Photo:</label>
                    <input id="photoUpload" type="file">
                    <span class="inputError"></span>
                </div>
                <div class="formInput">
                    <label for="photoNameInput">Name:</label>
                    <input id="photoNameInput" name="photoNameInput">
                    <span class="inputError"></span>
                </div>
                <div class="formInput">
                    <label for="photoUploadDescription">Description:</label>
                    <textarea id="photoUploadDescription"></textarea>
                </div>
                <button id="submitUpload">Upload</button>
            </section>
        </div>
    </div>

    <div class="modalContent modalBlock dark" id="mediaDescriptionModal" style="display: none">
        <div class="modalHeader"><span class="modalTitle"><div class="icon inline icon-picture"></div> edit description</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                    <section class="formBlock dark">
                        <div class="formGrid">
                            <div class="formInput">
                                <!-- input -->
                                <input type="hidden" id="descriptionIndex">
                                <label for="photo_description">description</label>
                                <textarea id="photo_description"></textarea>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="center"><button id="setDescription" class="cyan">set description</button></div>
            </section>
        </div>
    </div>
</div>
<script>
    var squads = JSON.parse('<?php echo json_encode($squads) ?>');
    console.log(squads);
    var modal;
    var totalPercent = 0;
    $('body').on('click', '.showFileModal', function(){
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#fileModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');

        if(target === "#mediaDescriptionModal"){
            var index =$(this).data('index');
            $('#descriptionIndex').val(index);
        }

        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
        modal.open();
    });

    //THIS FUNCTION SHOULD BE easy to drop on a page, the class triggering the onclick and the modal name should be the main changes.
    $('.showLocationModal').on('click', function(){
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#locationModal'),
            isolateScroll: true,
            onCloseComplete: function(event){
                onInjuryLocationsChanged();
            }
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
        modal.open();
    });

    $('#locationModal').on('click', 'button[class="red"]', function(){
        var modalId = '#' + $(this).data("modal-id");
        $(modalId).find('.locationButton').removeClass('selected');
        onInjuryLocationsChanged();
    });


    $('#locationModal').on('click', 'button[class="green"]', function(){
        modal.close();
    });

</script><script>
    // injuryDate = new MtrDatepicker({
    //     target: "injuryDate",
    //     animation: true,
    //     smartHours: true,
    //     timestamp: new Date(),

    // });





    $('.inputError').removeClass('show');
    //$('.submitButton').removeClass('show');

    $('.typeButton').on('click', function(){
        $(this).toggleClass('selected');
        onDataChanged();
    });

    $('.locationButton').on('click', function(){
       var code = $(this).data('naiscscode');
       console.log(code);
       //this should toggle the class on the duplicate buttons (on the view all modal + other modals)
       $('.locationButton[data-naiscscode="' + code +'"]').toggleClass('selected');
       // Update injured person display
       setInjuredPersonDisplay();
    });


    var claimType = '';
    var sev = false;
    var gen = false;

    //set review panel labels to red until populated
    $('#claimLabel').css('color', 'red');
    $('#sevLabel').css('color', 'red');
    $('#nameLabel').css('color', 'red');
    $('#genLabel').css('color', 'red');
    $('#dateLabel').css('color', 'red');
    $('#emailLabel').css('color', 'white');
    $('#mobileLabel').css('color', 'white');
    $('#locLabel').css('color', 'red');
    $('#typeLabel').css('color', 'red');
    $('#teamLabel').css('color', 'red');
    $('#imgLabel').css('color', 'white');
    $('#summaryLabel').css('color', 'red');

    var bodyAreas = [
        'systematic',
        'head',
        'neck',
        'trunk',
        'r-arm',
        'l-arm',
        'r-leg',
        'l-leg'
    ];

    var injury = {
        claim_setting: null,
        policyNumber: null,
        classification: null,
        severity: null,
        name: null,
        gender: null,
        email: null,
        phone: null,
        date: null,
        squadID: null,
        squadName: null,
        notes: null,
        img: null,
        locations: [],
        location_codes: [],
        types: [],
        photos: [],
        injuryLocationsImage: null,
    }

    // $('.newInjuryPanel').on('click', function(){
    //     storeBasicInfo();
    //     setReviewPanel();
    // });

    //Post the injury data
    $('#submitButton').on('click', function(){
        var valid = false;
        var email = injury.email;
        var phone = injury.phone;

        console.log("submit clicked");

        if (validateEmail(email)){
            valid = true;
        }else{
            // errorDiv = $('#email').parent().find('.inputError')
            $('#email').parent().find('.inputError').addClass('show');
            // errorDiv.html("Please enter a valid email address");
            valid = false;
        }

        if (validatePhone(phone)){
            valid = true;
        }else{
            // errorDiv = $('#phone').parent().find('.inputError')
            $('#phone').parent().find('.inputError').addClass('show');
            // errorDiv.html("Please enter a valid email address");
            valid = false;
        }

        if (valid === true){
            postInjury(injury);
        }
    });

    function onDataChanged() {
        storeBasicInfo();
        setReviewPanel();
    }

    function storeBasicInfo(){
        //check all fields to see if they are populated - display or hide errors
        //      runValidation();
        var claimPanel = false;
        if ($("#newClaim").prop("checked")){
            claimPanel = true;
            injury.claim_setting = "new";
        } else if ($("#preClaim").prop("checked")){
            claimPanel = true;
           injury.claim_setting = "preexisting";
        } else if ($("#demoClaim").prop("checked")){
            claimPanel = true;
            injury.claim_setting = 'demo';
        }

        injury.name = $('#firstName').val() + " " + $('#lastName').val();

        if ($("#male").prop("checked")){
            injury.gender = "male";
            gen = true;
        } else if ($("#female").prop("checked")){
            injury.gender = "female";
            gen = true;
        }
        if ($("#firstAid").prop("checked")){
            injury.severity = "Mild";
            sev = true;
        }else if ($("#modInjury").prop("checked")){
            injury.severity = "Moderate";
            sev = true;
        }else if ($("#sevInjury").prop("checked")){
            injury.severity = "Severe";
            sev = true;
        }

        //injury.email = $('#email').val().replace(/ /g, '');
        injury.policyNumber = "1000001";
        injury.email = $('#email').val();
        injury.phone = $('#phone').val();
        if(injury.severity === "Mild"){
            injury.classification = "First Aid";
        }else{
            injury.classification = "Work Comp";
        }
        //injury.squadID = "15";
        //injury.date = $('#datetime12').combodate('getValue', null);
        injury.date = $('#injury-datetime').val();
        //console.log(injury.date);
        injury.notes = $('#textarea').val();

        onInjuryLocationsChanged();

        setTypesArray();
        //injury.squadName = "team1"

        //runValidation();
        console.log(injury);
    }

    function onInjuryLocationsChanged() {
        setLocationsArray();
        writeLocations();
        setInjuredPersonDisplay();
        setReviewPanel();
    }

    function setTypesArray(){
        //create a new array for the types
        var types = []
        //get all elements that have both classes
        var selected_types = $('.typeButton.selected');
        //loop through each element
        selected_types.each(function(){
            //push the type stored in the element's html to the array
            types.push($(this).html());
        });
        injury.types = types;
        writeTypes();
    }

    function setLocationsArray(){
        //create a new array for the types
        var locations = [];
        var location_codes = [];
        //get all elements that have both classes
        var selected_locations = $('.locationButton.selected');
        //loop through each element
        selected_locations.each(function(){
            //push the type stored in the element's html to the array
            location_codes.push($(this).data('naiscscode'));
            locations.push($(this).html());
        });
        //there will be duplicants that need to be removed
        injury.locations = Array.from(new Set(locations));
        injury.location_codes = Array.from(new Set(location_codes));
    }

    function setInjuredPersonDisplay() {
        // highlight the body area if any codes for this body area are selected
        bodyAreas.forEach(function (area) {
            if (area) {
                var selectedCodes = $('.locationButton.selected[data-bodyareas~="' + area + '"]');

                if (selectedCodes.length > 0) {
                    // console.log('found ' + selectedCodes.length + ' selected codes for area ' + area);
                    // highlight the body area if any codes for this body area are selected
                    $('.layer.' + area).css('display', 'block');
                }
                else {
                    // remove highlight
                    $('.layer.' + area).css('display', 'none');
                }
            }
        });
    }

    function writeLocations(){
        var locations = injury.locations;
        var html = "";
        for(var i = 0; i < locations.length; i ++){
            html += locations[i] + ", ";
        }
        html = html.slice(0, -2);

        if (html === '')
            html = 'none selected -- select an injury location';

        //console.log(html);
        $('.locationSpan').html(html);
    }

    $('.resetInjuryLocations').on('click', function(){
        resetInjuryLocations();
        modal.close();
        validate();
    });
    function resetInjuryLocations(){
        injury.locations = "";
        injury.location_codes = "";
        $('#locLabel').css('color', 'red');
        $('.locationSpan').html('none selected -- select an injury location');
        $('#set_injurylocationarray').html("");
        // remove highlight the body area if any codes for this body area are selected
        bodyAreas.forEach(function (area) {
            if (area) {
                $('.layer.' + area).css('display', 'none');

                var selectedCodes = $('.locationButton.selected[data-bodyareas~="' + area + '"]').removeClass('selected');
            }
        });

        console.log(injury);

    }

    function writeTypes(){
        var types = injury.types;
        console.log(types);
        var html = "";
        for(var i = 0; i < types.length; i ++){
            html += types[i] + ", ";
        }
        html = html.slice(0, -2);

        $('#set_injurytypearray').html(html);
    }

    function setReviewPanel(){
        $('#set_claimtype').html(injury.claim_setting);
        $('#set_injuryseverity').html(injury.severity);
        $('#set_employeename').html(injury.name);
        $('#set_employeegender').html(injury.gender);
        $('#set_injurydatetime').html(injury.date);
        $('#set_employeemail').html(injury.email);
        $('#set_employeemobile').html(injury.phone);
        $('#set_injurylocationarray').html(injury.location);
        $('#set_injurytypearray').html(injury.type);
        $('#set_injuryteam').html(injury.squadName);
        $('#set_textarea').html(injury.notes);
        $('#set_imagearray').html(injury.photos);
        validate();

    }

    // $('#submitButton').on('click', function){
    //     validateData();
    // });

    function validate(){
        var valid = true;
        var checkPanel = true;
        console.log('validating');

        if($('#submitButton').is(':visible')){
            $('#submitButton').hide();
            $('#showMessage').show();
        }
  
        if ($("#newClaim").prop("checked")){
            injury.claim_setting = "new";
        } else if ($("#preClaim").prop("checked")){
           injury.claim_setting = "preexisting";
        } else if ($("#demoClaim").prop("checked")){
            injury.claim_setting = 'demo';
        }

        if (!injury.claim_setting){
            valid = false;
            checkPanel = false;
            $('#claimLabel').css('color', 'red');
        }else{
            $('#claimLabel').css('color', '');
        }

        var sev = false;
        if ($("#firstAid").prop("checked")){
            injury.severity = "Mild";
            sev = true;
        }else if ($("#modInjury").prop("checked")){
            injury.severity = "Moderate";
            sev = true;
        }else if ($("#sevInjury").prop("checked")){
            injury.severity = "Severe";
            sev = true;
        }

        if (sev === false){
            valid = false;
            checkPanel = false;
            $('#sevLabel').css('color', 'red');
        }else{
            $('#sevLabel').css('color', '');
            
        }

        var gen = false;
        if ($("#male").prop("checked")){
            injury.gender = "male";
            gen = true;
        } else if ($("#female").prop("checked")){
            injury.gender = "female";
            gen = true;
        }
        if (gen === false || gen === undefined){
            valid = false;
            checkPanel = false;
            $('#genLabel').css('color', 'red');
        }else{
            $('#genLabel').css('color', '');
        }

        var text = $('#textarea').val();
        if (text === "" || text === undefined){
            valid = false;
            checkPanel = false;
            $('#summaryLabel').css('color', 'red');
        }else{
            $('#summaryLabel').css('color', '');
        }

        var contactEmailPanel = false;
        var email = injury.email;
        // clear any previous email errors
        $('#email').parent().find('.inputError').removeClass('show');
        if (email){
            contactEmailPanel = true;
            if (!validateEmail(email)){
                $('#email').parent().find('.inputError').addClass('show');
                valid = false;
                contactEmailPanel = false;
                $('#emailLabel').css('color', 'red');
            }
            else{
                $('#emailLabel').css('color', 'white');
            }
        }
        else{
            $('#emailLabel').css('color', 'white');
        }

        var phone = injury.phone;
        var contactPhonePanel = false;
        // clear any previous email errors
        $('#phone').parent().find('.inputError').removeClass('show');
        if (phone){
            contactPhonePanel = true;
            if (!validatePhone(phone)){
                $('#phone').parent().find('.inputError').addClass('show');
                valid = false;
                contactPhonePanel = false;
                $('#mobileLabel').css('color', 'red');   
            }
            else{
                $('#mobileLabel').css('color', 'white');
            }
        }
        else{
            $('#mobileLabel').css('color', 'white');
        }
        
        if(contactEmailPanel === true && contactPhonePanel === true){
            $('#contactPanel').addClass("sectionPanel complete");
        }
        else{
            $('#contactPanel').removeClass("complete");
        }
        // var empEmail = $('#email').val();
        // if (empEmail === "" || undefined){
        //     //NOT REQUIRED
        //     /*
        //     $('#email').parent().find('.inputError').addClass('show');
        //     mailDiv = false;
        //     */
        //     valid = false;
        // }else{
        //    // $('#email').parent().find('.inputError').removeClass('show');
        //     $('#emailLabel').css('color', '');
        // }

        // var empPhone = $('#phone').val();
        // if (empPhone === "" || undefined){
        //     //NOT REQUIRED
        //     /*
        //     $('#phone').parent().find('.inputError').addClass('show');
        //     phoneDiv = false;
        //     */
        //    valid = false;
        // }else{
        //     //$('#phone').parent().find('.inputError').removeClass('show');
        //     $('#mobileLabel').css('color', '');
        // }

        var fName = $('#firstName').val();
        var lName = $('#lastName').val();
    
        if (!fName || !lName){
            valid = false;
            checkPanel = false
            $('#nameLabel').css('color', 'red');
        }else{
            $('#nameLabel').css('color', '');
        }

        if (!injury.date){
            $('#dateLabel').css('color', 'red');
            valid = false;
            checkPanel = false
            $('#dateLabel').css('color', 'red');
        }else{
            $('#dateLabel').css('color', '');
        }
        if(checkPanel === true){
            $('#claimPanel').addClass("sectionPanel complete");
        }
        else{
            $('#claimPanel').removeClass("complete");
        }
        var typesPanel = true;
        if(injury.types === undefined || injury.types.length == 0){
            valid = false;
            typesPanel = false;
            $('#typeLabel').css('color', 'red');
        }else{
            $('#typeLabel').css('color', '');
        }
        if(typesPanel === true){
            $('#typesPanel').addClass("sectionPanel complete");
        }else{
            //$('#typesPanel').addClass("sectionPanel");
            $('#typesPanel').removeClass("complete");
        }
        var injuryPanel = true
        if(injury.locations === undefined || injury.locations.length == 0){
            valid = false;
            injuryPanel = false;
            $('#locLabel').css('color', 'red');
        }else{
            $('#locLabel').css('color', '');
        }

        if(injuryPanel === true){
            $('#injuryPanel').addClass("sectionPanel complete");
        }
        else{
            $('#injuryPanel').removeClass("complete");
        }

        if (injury.squadName === "" || injury.squadName === undefined || injury.squadName == null){
            valid = false;
            $('#teamLabel').css('color', 'red');
        }else{
            $('#teamLabel').css('color', '');
        }

        var imagePanel = true;
        if (!injury.photos.length){
           // valid = false;
           imagePanel = false;

        }else{
            $('#set_imagearray').html("Yes");
            //$('#imgLabel').css('color', '');
        }
        if(imagePanel === true){
            $('#imagePanel').addClass("sectionPanel complete");
        }
        else{
            $('#imagePanel').removeClass("complete");
        }

        if (valid === true){
            $('#submitButton').show();
            $('#showMessage').hide();
            $('#reviewPanel').addClass("sectionPanel complete");
        }

    }

    $('.teamButton').on('click', function(){
        $('.teamButton').removeClass('selected');
        $(this).addClass('selected');
        injury.squadID = $(this).data('teamid');
        injury.squadName = $(this).text();
        $('#set_injuryteam').html($(this).text());
        $('#teamLabel').css('color', '');
        $('#teamsPanel').addClass('complete');

        console.log(injury);
        validate();
    });

    //photo upload
    $('#submitUpload').on('click', function(){
        uploadPhoto();
       //getting the file from the input field
       //var file = $('#photoUpload').val();
       var maxSize = '<?php echo $value; ?>';
       maxSize = maxSize * 1000;
       var file = $('#photoUpload').prop('files');
       file = file[0].size
       file = file / 1024;
       file = Math.ceil(file);
       //seeing if we actually get a file
       console.log("file " + file);
       var totalSize = file / 50000;
       totalSize = totalSize * 100;
       totalSize = Math.ceil(totalSize);
       totalPercent += totalSize;
       $('.uploadSpace').width(totalPercent);
       document.getElementsByClassName('remainingSpace')[0].innerHTML = totalPercent + '% of ' + maxSize + 'k';
    });

    var photos = [];

    function uploadPhoto(){
        var upload_errors = $('#photoUpload').parent().find('.inputError')
        var file = document.getElementById('photoUpload').files[0];
        if(file === undefined || file === ""){
            upload_errors.html("Photo Required").addClass('show');
            return false;
        }


        if(file.type == "image/png" || file.type == "image/jpeg"){
            var name = $('#photoNameInput').val();
            var description = $('#photoUploadDescription').val();
            var reader = new FileReader();
            reader.onload = function(){
                var photo = {
                    file: file,
                    name: name,
                    description: description,
                    data: reader.result,

                }
                photos.push(photo);
                console.log(photo);
                buildPhotos();
                modal.close();
            }
            reader.readAsDataURL(file);


        }else{
            showAlert("Sorry, please make sure your image is a png or a jpeg.", "deny", 5);
            upload_errors.html("Invalid File Type").addClass('show');
        }
    }

    function validateEmail(email)
    {
        // email isn't required, so return true if blank
        if (!email)
            return true;

        var regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return regex.test(email);
    }

    function validatePhone(phone)
    {
        // phone number isn't required, so return true if blank
        if (!phone)
            return true;

        var regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
        return regex.test(phone);
    }

    function buildPhotos(){
        var html = "";
        //var canvas = document.getElementById('photoCanvas');
        //var ctx = canvas.getContext("2d");

        for(var i = 0; i < photos.length; i++){

            /*
            var image = new Image;
            image.onload = function(){
                ctx.drawImage(image, 0, 0);
            }
            image.src = photos[i].file;
            var base64 = canvas.toDataURL();
*/
            //var fileSize = photos[i].size;
            html+= '<div class="formMedia" data-index="' + i + '">' +
                '<div class="uploadedMedia" style="background-image:url(' + photos[i].data + ');"><div class="mediaSelectOverlay"></div>' +
                '<div class="mediaCaptionSummary">' + photos[i].description +'</div><span class="mediaInfo">' + photos[i].name + '</span><span class="mediaDelete" data-index="' + i + '"></span><span class="mediaCaption showFileModal" data-modalcontent="#mediaDescriptionModal" data-index="' + i + '"></span></div>' +
                '</div>';
        }
        $('#photoTarget').html(html);
        injury.photos = photos;
        onDataChanged();
    }

    $('#photoTarget').on('click', '.mediaDelete', function(){
        var index = $(this).data('index');
        photos.splice(index, 1);
        buildPhotos();
       
        var maxSize = '<?php echo $value; ?>';
       maxSize = maxSize * 1000;
       var file = $('#photoUpload').prop('files');
       file = file[0].size
       file = file / 1024;
       file = Math.ceil(file);
       //seeing if we actually get a file
       console.log("file " + file);
       var totalSize = file / 50000;
       totalSize = totalSize * 100;
       totalSize = Math.ceil(totalSize);
       totalPercent -= totalSize;
       if(totalPercent < 0){
            totalPercent = 0;
       }
       $('.uploadSpace').width(totalPercent);
       document.getElementsByClassName('remainingSpace')[0].innerHTML = totalPercent + '% of ' + maxSize + 'k'; 
    });

    $('#setDescription').on('click', function(){
       var index = $('#descriptionIndex').val();
       var description = $('#photo_description').val();

       photos[index].description = description;
       buildPhotos();
        modal.close();
    });

    function postInjury(injury){

        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            type: 'POST',
            url: '<?php echo route('saveInjury') ?>',
            data: JSON.stringify({injury}),
            processData: false,
            contentType: 'application/json',
            success: function(data){
                //alert('success!');
                window.location.href = "/injuryresult";
            },
            error: function(data){
                console.log(data);
                alert('error');
            }
        });

    }
</script>
<div id="add_media_description" class="zModal" style="display:none">
    <div class="modalContent dark">
        <div class="modalHeader"><span class="modalTitle"><div class="icon inline icon-picture"></div> edit description</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                <section class="formBlock dark">
                    <div class="formGrid">
                        <div class="formInput">
                            <!-- input -->
                            <label for="photo_description">description</label>
                            <textarea id="photo_description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="center"><button class="cyan">set description</button></div>
            </section>
        </div>
    </div>
</div><div id="add_media_links" class="zModal" style="display:none">
    <div class="modalContent dark">
        <div class="modalHeader"><span class="modalTitle"><div class="icon inline icon-link"></div> add media links</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                    <section class="formBlock dark">
                        <div class="formGrid">
                        <div class="formGrid short">
                            <div class="formInput">
                                <input type="radio" name="r1" id="r1">
                                <label for="r1">link</label>
                            </div>
                            <div class="formInput">
                                <input type="radio" name="r1" id="r2">
                                <label for="r2">photo</label>
                            </div>
                            <div class="formInput">
                                <input type="radio" name="r1" id="r3">
                                <label for="r3">video</label>
                            </div>
                            <div class="formInput">
                                <input type="radio" name="r1" id="r4">
                                <label for="r4">document</label>
                            </div>
                            <div class="formInput">
                                <input type="radio" name="r1" id="r5">
                                <label for="r5">collection</label>
                            </div>
                            <div class="formInput">
                                <input type="radio" name="r1" id="r6">
                                <label for="r6">medical</label>
                            </div>
                        </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="media_url">URL of media</label>
                                <input id="media_url" />
                                <span class="inputError show">error</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="media_description">description</label>
                                <textarea id="media_description"></textarea>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="center"><button class="cyan">add link</button></div>
            </section>
        </div>
    </div>
</div><div id="show_image" class="zModal" style="display:none">
    <div class="modalContent wide">
        <div class="modalHeader"><span class="modalTitle"><div class="icon inline icon-picture"></div> image title</span></div>
        <div class="modalBody" style="min-width:260px;">
            <div class="buttonArray center">
                <button class="cyan centered" onclick="location.href='/fullscreen';">full screen</button>
                <button class="blue centered">edit description</button>
                <button class="red centered">delete</button>
            </div>
            <div class="showFullImage">
                <div class="fullImage" style="background-image:url('/images/utility/testimage-landscape.jpg');"></div>
                <div class="fullImageCaption">-- no caption --</div>

            </div>
        </div>
    </div>
</div>
<script>
	//each jBox modal needs to be created with this code block, using seperate variable names for each new modal created
	var addMediaDescriptionModal = new jBox('Modal', {
		attach: '.mediaCaption', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#add_media_description'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true,
		onOpen: function(event){

		},
		onCloseComplete: function(event){

		}
	});
	var addMediaModal = new jBox('Modal', {
		attach: '#addMedia', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#add_media_links'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true,
		onOpen: function(event){

		},
		onCloseComplete: function(event){

		}
	});
    var showImageModal = new jBox('Modal', {
		attach: '.showImage', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#show_image'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true,
		onOpen: function(event){

		},
		onCloseComplete: function(event){

		}
	});
</script>

<!-- SWAPPABLE CONTENT, with IDs that match the modalcontent attribute for its respective button -->
<!-- using the class "selected" will make the button red.  use that to signify something user picked.-->
{{-- <div class="modalContent" id="showAll" style="display:none">
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <div class="selectionContainer">
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>systematic</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>800 body system</button>
                    <button>999 nonclassifiable</button>
                    <button>700 multiple parts</button>
                    <button>900 body parts</button>
                    <button>000 death</button>
                </div>
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>head</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>100 head</button>
                    <button>110 brain</button>
                    <button>120 ear(s)</button>
                    <button>121 ear(s) external</button>
                    <button>122 left ear external</button>
                    <button>123 right ear external</button>
                    <button>124 ear(s) internal</button>
                    <button>125 left ear internal</button>
                    <button>126 right ear internal</button>
                    <button>130 eye(s)</button>
                    <button>132 left eye</button>
                    <button>134 right eye</button>
                    <button>140 face</button>
                    <button>141 jaw</button>
                    <button>144 mouth</button>
                    <button>146 nose</button>
                    <button>148 face, multi-part</button>
                    <button>149 face, NEC</button>
                    <button>150 scalp</button>
                    <button>160 skull</button>
                    <button>198 head, multi-part</button>
                    <button>199 head, NEC</button>
                </div>
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>neck</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>200 neck</button>
                </div>
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>right arm</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>304 right arm</button>
                    <button>309 upper right arm</button>
                    <button>314 right elbow</button>
                    <button>317 right forearm</button>
                    <button>324 right wrist</button>
                    <button>334 right hand</button>
                    <button>344 right fingers, thumb</button>
                    <button>397 right lower extremities</button>
                </div>
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>left arm</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>302 left arm</button>
                    <button>308 upper left arm</button>
                    <button>312 left elbow</button>
                    <button>316 left forearm</button>
                    <button>322 left wrist</button>
                    <button>332 left hand</button>
                    <button>342 left fingers, thumb</button>
                    <button>396 left lower extremities</button>
                </div>
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>trunk</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>400 trunk</button>
                    <button>410 abdomen</button>
                    <button>420 back</button>
                    <button>430 chest</button>
                    <button>440 hips, both</button>
                    <button>442 left hip</button>
                    <button>444 right hip</button>
                    <button>450 shoulders, both</button>
                    <button>452 left shoulder</button>
                    <button>454 right shoulder</button>
                    <button>498 trunk, multi-part</button>
                    <button>499 trunk, NEC</button>
                </div>
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>both arms</b> (upper extremities)</span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>310 arms</button>
                    <button>311 upper arms</button>
                    <button>313 elbows</button>
                    <button>315 forearms</button>
                    <button>318 arms, multi-part</button>
                    <button>319 arms, NEC</button>
                    <button>320 wrists</button>
                    <button>330 hands</button>
                    <button>340 fingers, thumbs</button>
                    <button>398 upper extremities</button>
                    <button>399 upper extremities, NEC</button>
                </div>
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>right leg</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>508 right leg</button>
                    <button>514 right knee</button>
                    <button>517 right shin</button>
                    <button>524 right ankle</button>
                    <button>534 right foot</button>
                    <button>5442 right toes</button>
                    <button>597 lower extremities, right</button>
                </div>
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>left leg</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>507 left leg</button>
                    <button>512 left knee</button>
                    <button>516 left shin</button>
                    <button>522 left ankle</button>
                    <button>532 left foot</button>
                    <button>542 left toes</button>
                    <button>596 lower extremities, right</button>
                </div>
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>both legs</b> (lower extremities)</span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>510 both legs</button>
                    <button>513 both knees</button>
                    <button>515 lower legs</button>
                    <button>518 legs, multi-part</button>
                    <button>520 both ankles</button>
                    <button>530 both feet</button>
                    <button>540 all toes</button>
                    <button>598 lower extremities, multi</button>
                    <button>598 lower extremities, NEC</button>
                </div>
                <hr class="opaque" style="margin-top:30px;margin-bottom:60px;">
                <div class="center">
                    <div class="buttonArray">
                        <button class="green"><div class="icon inline icon-check"></div> set</button>
                        <button class="red resetInjuryLocations"><div class="icon inline icon-refresh"></div> reset</button>
                    </div>
                </div>
                <br><br>
            </div>
        </div>
    </div>
</div>
<div class="modalContent" id="showSystematic" style="display:none">
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <div class="selectionContainer">
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>systematic</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <div class="buttonCloud">
                    <button>800 body system</button>
                    <button>999 nonclassifiable</button>
                    <button>700 multiple parts</button>
                    <button>900 body parts</button>
                    <button>000 death</button>
                </div>
                </div>
                <hr class="opaque" style="margin-top:30px;">
                <div class="center">
                    <div class="buttonArray">
                        <button class="green"><div class="icon inline icon-check"></div> set</button>
                        <button class="red resetInjuryLocations"><div class="icon inline icon-refresh"></div> reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modalContent" id="showHead" style="display:none">
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <div class="selectionContainer">
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>head</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>100 head</button>
                    <button>110 brain</button>
                    <button>120 ear(s)</button>
                    <button>121 ear(s) external</button>
                    <button>122 left ear external</button>
                    <button>123 right ear external</button>
                    <button>124 ear(s) internal</button>
                    <button>125 left ear internal</button>
                    <button>126 right ear internal</button>
                    <button>130 eye(s)</button>
                    <button>132 left eye</button>
                    <button>134 right eye</button>
                    <button>140 face</button>
                    <button>141 jaw</button>
                    <button>144 mouth</button>
                    <button>146 nose</button>
                    <button>148 face, multi-part</button>
                    <button>149 face, NEC</button>
                    <button>150 scalp</button>
                    <button>160 skull</button>
                    <button>198 head, multi-part</button>
                    <button>199 head, NEC</button>
                </div>
                <hr class="opaque" style="margin-top:30px;">
                <div class="center">
                    <div class="buttonArray">
                        <button class="green"><div class="icon inline icon-check"></div> set</button>
                        <button class="red resetInjuryLocations"><div class="icon inline icon-refresh"></div> reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modalContent" id="showNeck" style="display:none">
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <div class="selectionContainer">
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>neck</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>200 neck</button>
                </div>
                <hr class="opaque" style="margin-top:30px;">
                <div class="center">
                    <div class="buttonArray">
                        <button class="green"><div class="icon inline icon-check"></div> set</button>
                        <button class="red resetInjuryLocations"><div class="icon inline icon-refresh"></div> reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modalContent" id="showRightArm" style="display:none">
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <div class="selectionContainer">
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>right arm</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>304 right arm</button>
                    <button>309 upper right arm</button>
                    <button>314 right elbow</button>
                    <button>317 right forearm</button>
                    <button>324 right wrist</button>
                    <button>334 right hand</button>
                    <button>344 right fingers, thumb</button>
                    <button>397 right lower extremities</button>
                </div>
                <hr class="opaque" style="margin-top:30px;">
                <div class="center">
                    <div class="buttonArray">
                        <button class="green"><div class="icon inline icon-check"></div> set</button>
                        <button class="red resetInjuryLocations"><div class="icon inline icon-refresh"></div> reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modalContent" id="showLeftArm" style="display:none">
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <div class="selectionContainer">
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>left arm</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>302 left arm</button>
                    <button>308 upper left arm</button>
                    <button>312 left elbow</button>
                    <button>316 left forearm</button>
                    <button>322 left wrist</button>
                    <button>332 left hand</button>
                    <button>342 left fingers, thumb</button>
                    <button>396 left lower extremities</button>
                </div>
                <hr class="opaque" style="margin-top:30px;">
                <div class="center">
                    <div class="buttonArray">
                        <button class="green"><div class="icon inline icon-check"></div> set</button>
                        <button class="red resetInjuryLocations"><div class="icon inline icon-refresh"></div> reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modalContent" id="showTrunk" style="display:none">
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <div class="selectionContainer">
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>trunk</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>400 trunk</button>
                    <button>410 abdomen</button>
                    <button>420 back</button>
                    <button>430 chest</button>
                    <button>440 hips, both</button>
                    <button>442 left hip</button>
                    <button>444 right hip</button>
                    <button>450 shoulders, both</button>
                    <button>452 left shoulder</button>
                    <button>454 right shoulder</button>
                    <button>498 trunk, multi-part</button>
                    <button>499 trunk, NEC</button>
                </div>
                <hr class="opaque" style="margin-top:30px;">
                <div class="center">
                    <div class="buttonArray">
                        <button class="green"><div class="icon inline icon-check"></div> set</button>
                        <button class="red resetInjuryLocations"><div class="icon inline icon-refresh"></div> reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modalContent" id="showBothArms" style="display:none">
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <div class="selectionContainer">
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>both arms</b> (upper extremities)</span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>310 arms</button>
                    <button>311 upper arms</button>
                    <button>313 elbows</button>
                    <button>315 forearms</button>
                    <button>318 arms, multi-part</button>
                    <button>319 arms, NEC</button>
                    <button>320 wrists</button>
                    <button>330 hands</button>
                    <button>340 fingers, thumbs</button>
                    <button>398 upper extremities</button>
                    <button>399 upper extremities, NEC</button>
                </div>
                <hr class="opaque" style="margin-top:30px;">
                <div class="center">
                    <div class="buttonArray">
                        <button class="green"><div class="icon inline icon-check"></div> set</button>
                        <button class="red resetInjuryLocations"><div class="icon inline icon-refresh"></div> reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modalContent" id="showRightLeg" style="display:none">
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <div class="selectionContainer">
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>right leg</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>599 lower extremities (all)</button>
                    <button>510 legs (both)</button>
                    <button>513 knees (both)</button>
                    <button>520 ankles (both)</button>
                    <button>530 feet (both)</button>
                    <button>540 toes (both feet)</button>
                    <button>596 left lower extremities</button>
                    <button class="selected">508 left leg</button>
                    <button>514 left knee</button>
                    <button>522 left ankle</button>
                    <button>532 left foot</button>
                    <button>542 left foot toes</button>
                </div>
                <hr class="opaque" style="margin-top:30px;">
                <div class="center">
                    <div class="buttonArray">
                        <button class="green"><div class="icon inline icon-check"></div> set</button>
                        <button class="red resetInjuryLocations"><div class="icon inline icon-refresh"></div> reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modalContent" id="showLeftLeg" style="display:none">
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <div class="selectionContainer">
                <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>left leg</b></span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>599 lower extremities (all)</button>
                    <button>510 legs (both)</button>
                    <button>513 knees (both)</button>
                    <button>520 ankles (both)</button>
                    <button>530 feet (both)</button>
                    <button>540 toes (both feet)</button>
                    <button>596 left lower extremities</button>
                    <button class="selected">508 left leg</button>
                    <button>514 left knee</button>
                    <button>522 left ankle</button>
                    <button>532 left foot</button>
                    <button>542 left foot toes</button>
                </div>
                <hr class="opaque" style="margin-top:30px;">
                <div class="center">
                    <div class="buttonArray">
                        <button class="green"><div class="icon inline icon-check"></div> set</button>
                        <button class="red resetInjuryLocations"><div class="icon inline icon-refresh"></div> reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modalContent" id="showBothLegs" style="display:none">
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <div class="selectionContainer">
            <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>both legs</b> (lower extremities)</span>
                <hr class="opaque">
                <div class="buttonCloud">
                    <button>510 both legs</button>
                    <button>513 both knees</button>
                    <button>515 lower legs</button>
                    <button>518 legs, multi-part</button>
                    <button>520 both ankles</button>
                    <button>530 both feet</button>
                    <button>540 all toes</button>
                    <button>598 lower extremities, multi</button>
                    <button>598 lower extremities, NEC</button>
                </div>
                <hr class="opaque" style="margin-top:30px;">
                <div class="center">
                    <div class="buttonArray">
                        <button class="green"><div class="icon inline icon-check"></div> set</button>
                        <button class="red resetInjuryLocations"><div class="icon inline icon-refresh"></div> reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<!-- BASE MODAL HTML -->
{{-- <div id="locationModal" class="zModal" style="display:none">
        <!-- this is filled with each location modal data -->
</div> --}}

<script>
	//THIS FUNCTION SHOULD BE easy to drop on a page, the class triggering the onclick and the modal name should be the main changes.
	// $('.showLocationModal').on('click', function(){
	// 	//initialize the modal
	// 	var modal = new jBox('Modal', {
	// 		addClass: 'zBox',
	// 		closeButton: true,
	// 		//modal ID goes here
	// 		content: $('#locationModal'),
	// 		isolateScroll: true
	// 	});
	// 	//get the content for the modal
	// 	var target = $(this).data('modalcontent');
	// 	var html = $(target).prop('outerHTML');

	// 	//script related to a specific content block can be added like this
	// 	if(target === ""){

	// 	}
	// 	//modal ID goes here
	// 	$('#locationModal').html(html).find(target).show();
	// 	modal.open();
	// });
</script>
@endsection

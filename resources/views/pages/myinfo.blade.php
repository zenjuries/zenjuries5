@extends('layouts.zen5_layout')
@section('maincontent')
<?php 
use Carbon\Carbon;
$user = Auth::user();

$theme = $user->zengarden_theme;
$theme = str_replace("theme", "", $theme);
$theme = strtolower($theme);
//new theme values
$brandID = env('BRAND_NAME');
$themeID = 'theme' . $user->theme_id;
$currentTeamId = $user->current_team_id;
$teamName =  \App\Team::where('id', $currentTeamId)->value('name');
$arrayPassedIn = str_split($user->shortcuts);

//getting timestamp for last password change
$passwordUpdatedAt = new DateTime($user->password_updated_at);
$passwordUpdatedAt = $passwordUpdatedAt->format('m-d-Y h:i A');
?>

<style>
    span.zCell.spacer{padding:0;display: inline-block}
    span.zCell.spacer:before{content:'----------';display:block;color:#747474;}
    span.zCell.spacer:first-child:before {text-align:right}
    span.zCell.spacer:last-child:before {text-align:left;}

    .themePlaceHolder{
        padding: 80px 1px;
    }
</style>

<div class="pageContent bgimage-bgheader pagebackground7 fade-in">		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>my info</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/teammessages.png"></span><span class="subContent">edit your information</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/userinfo.png">
            </div> 
        </div>           
	</div>

    <div class="contentBlock lessPadding">
		<div class="container setMinHeight">
            <!--
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> alerts and messages</span>
                <div class="sectionContent">
                    <div class="placeHolder">
                        User name
                        <span>current alerts and messages go here</span>
                    </div>
                    <hr>
                    <div class="buttonArray"><button class="cyan centered">update info</button></div>
                </div>
            </section>
            -->
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> basic information</span>
                <div class="sectionDescription">Set your name, email, and phone number here.  You can also add a description to help other Zenjuries users or admins be aware of any additional info or special circumstances.  You can also update your password.</div> 
                <div class="sectionContent">
                    <ul class="sectionInfoPanel">
                    <li><span class="label">Name:</span> <span class="data" id="currentUserName">{{ $user->name }}</span></li>
                    <li><span class="label">Email:</span> <span class="data" id="currentUserEmail">{{ $user->email }}</span></li>
                    <li><span class="label">Phone:</span> <span class="data" id="currentUserPhone">{{ (empty($user->phone) ? "no phone entered" : $user->phone) }}</span></li>
                    <li><span class="label"> Description:</span> <span style="max-width:120px;height:60px;overflow:hidden;font-weight:400;font-style:italic;" class="data" id="currentUserDescription">{{ (empty($user->description) ? "no comments yet" : $user->description) }}</span></li>
                    </ul>
                    <ul class="sectionHelpPanel green">
                        <li class="sectionPanelIcon"><img class="wow animate__animated animate__headShake animate__repeat-2" src="/images/icons/greencheck.png"></li>
                        <li style="text-align:center;"><span class="label small FG__yellow">password updated: </span><span class="data small FG__orange" id="passwordChangedAt"><b>{{ $passwordUpdatedAt }}</b></span></li>
                    </ul>
                    <br>
                    <div class="buttonArray">
                        <button class="showZenModal" data-modalcontent="#myInfoModal"><div class="icon icon-user"></div> update info</button>
                        <button class="showZenModal" data-modalcontent="#passwordModal"><div class="icon icon-lock"></div> update password</button>
                    </div>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-bell"></div> alerts & updates</span>
                <div class="sectionDescription">Your weekly summary will inform you of all the injury and team activity you have had all week.  Use this control to set the frequency of the alerts and emails you recieve. We recommend all alerts enabled to get the most out of Zenjuries.</div> 
                <div class="sectionContent">
                    <ul class="sectionInfoPanel">
                    <li><span class="label FG__cyan"><b>Weekly&nbsp;Summary:</b></span><span class="data" id="currentWeeklySummary">@if($user->gets_weekly_summary) Enabled @else Disabled @endif</span></li>
                    <li><span class="label FG__orange"><b>Priority&nbsp;2&nbsp;Emails:</b></span> <span class="data" id="currentPriority2Emails">@if($user->gets_priority_2_emails) Enabled @else Disabled @endif</span></li>
                    <li><span class="label FG__yellow"><b>Priority&nbsp;3&nbsp;Emails:</b></span> <span class="data" id="currentPriority3Emails">@if($user->gets_priority_3_emails) Enabled @else Disabled @endif</span></li>
                    </ul>
                    <br>
                    <ul class="sectionHelpPanel orange">
                        <li class="sectionPanelIcon"><img class="wow animate__animated animate__swing animate__repeat-2" src="/images/icons/alert.png"></li>
                        <li><b class="FG__orange">Priority 2 Emails</b> - consists of major updates on claims and your account such as severity updates and subscription updates.</li>
                        <li><b class="FG__yellow">Priority 3 Emails</b> - consists of minor updates on claims and your account such as claim costs being updated and zengarden dirary updates.</li>
                    </ul>                  
                    <div class="buttonArray">
                        <button class="showZenModal" data-modalcontent="#communicationModal"><div class="icon icon-bell"></div> update alerts</button>
                    </div>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-mobile"></div> mobile notifications</span>
                <div class="sectionDescription">Zenjuries will push notifications to your mobile device when you are using the Zenjuries mobile app.  You can change the type and how many notifications you get here.</div> 
                <div class="sectionContent">
                    <ul class="sectionInfoPanel">
                    <li><span class="label FG__green"><b>Base&nbsp;notifications:</b></span><span class="data" id="basePushNotifications">@if($user->notification_priority_1) enabled @else disabled @endif</span></li>
                    <li><span class="label FG__cyan"><b>Additional&nbsp;notifications:</b></span> <span class="data" id="additionalPushNotifications">@if($user->notification_priority_2) enabled @else disabled @endif</span></li>
                    <li><span class="label FG__orange"><b>All&nbsp;communications:</b></span> <span class="data" id="allPushNotifications">@if($user->notification_priority_3) enabled @else disabled @endif</span></li>
                    </ul>
                    <br>
                    <ul class="sectionHelpPanel cyan">
                        <li class="sectionPanelIcon"><img class="wow animate__animated animate__swing animate__repeat-2" src="/images/icons/mobile.png"></li>
                        <li><b class="FG__green">Base notifications</b> - you will get notified via push notifications on your mobile device of new injuries, any litigation, and high prority alerts.</li>
                        <li><b class="FG__cyan">Additional notifications</b> - additionally, you will see any communications and alerts from any injury on your watchlist, and zenjuries reminders.</li>
                        <li><b class="FG__orange">All communications</b> - you will be also alerted of any communication on any injury you are a part of.</li>
                    </ul>                  
                    <div class="buttonArray">
                        <button class="showZenModal" data-modalcontent="#pushalertsModal"><div class="icon icon-mobile"></div> update notifications</button>
                    </div>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-bamboo2"></div> theme / style</span>
                <div class="sectionDescription">Select a theme that will change your color style and dashboard background.  Check back periodically to find more themes to use for your dashboard.</div>
                <div class="sectionContent">
                    <div class="setButton themePlaceHolder buttonSet themeBG" style="background-image:url('/branding/{{ $brandID }}/themes/{{ $themeID }}/dashboard.jpg'); text-align: center;max-width:800px;border-radius:12px;margin:20px auto">
                        <span class="themeLabelbg {{ $themeID }} color1">Your current theme: <b><span class="{{ $themeID }} themeName"></span></b></span>
                        <span class="themeColorBox {{ $themeID }} color1"></span><span class="themeColorBox {{ $themeID }} color2"></span><span class="themeColorBox {{ $themeID }} color3"></span><span class="themeColorBox {{ $themeID }} color4"></span>
                    </div>                    
                    
                    <div class="buttonArray">
                        <button class="showZenModal" data-modalcontent="#themeModal"><div class="icon icon-layout"></div> choose theme</button>
                       
                        @if($arrayPassedIn[2] === "1")
                            <button id="darkModeMyInfoBtn" class="white FG__black"><div class="icon icon-adjust FG__black"></div> <b>light mode</b> | <span style="font-weight:300 !important;opacity:.5;">dark mode</span></button>
                            <button id="lightModeMyInfoBtn"class="black FG__white" style="display:none"><div class="icon icon-adjust FG__white"></div> <span style="font-weight:300 !important;opacity:.5;">light mode</span> | <b>dark mode</b></button>
                         @else
                            <button id="lightModeMyInfoBtn"class="black FG__white"><div class="icon icon-adjust FG__white"></div> <span style="font-weight:300 !important;opacity:.5;">light mode</span> | <b>dark mode</b></button>
                            <button id="darkModeMyInfoBtn" class="white FG__black" style="display:none"><div class="icon icon-adjust FG__black"></div> <b>light mode</b> | <span style="font-weight:300 !important;opacity:.5;">dark mode</span></button>
                        @endif
                        
                    </div>                   
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-palette"></div> avatar & color</span>
                <div class="sectionDescription">Change your avatar icon, choose your colors, or upload your own photo to save as your avatar icon image if you would like. Please make sure all uploaded images are appropriate and safe for the workplace.</div>
                <div class="sectionContent" style="display: flex;flex-direction:column;align-items:center;">
                    <div class="profileAvatarColor" style="background-color: {{$user->avatar_color}}; border-color: {{$user->avatar_color}}"><div class="avatarIcon" style="background-image:url({{$user->photo_url}})"></div></div>
                    <input id="currentAvatarNumber" type="hidden" value="{{$user->avatar_number}}">
                    <!--
                    <div class="placeHolder">
                        avatar | color
                        <span>the set avatar and color here</span>
                    </div>
                    -->                  
                    <div class="buttonArray">
                        <button class="showZenModal" data-modalcontent="#changeAvatar"><div class="icon icon-bamboo2"></div> change avatar</button>
                        <button class="showZenModal" data-modalcontent="#changeColor"><div class="icon icon-paint-bucket"></div> change color</button>
                        <button class="showZenModal" data-modalcontent="#imageModal"><div class="icon icon-profile"></div> upload your own image</button>
                    </div>
                </div>
            </section>
		</div>
	</div>
</div>

<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
    @include('partials.modals.setMyInfo')         
	@include('partials.modals.setPassword')
	@include('partials.modals.setCommunication')
    @include('partials.modals.setPushNotifications')
    @include('partials.modals.chooseAvatar')         
	@include('partials.modals.colorPicker')
	@include('partials.modals.uploadAvatarImage')
    @include('partials.modals.chooseTheme')                
</div>

<script>
    $('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>

<script>
    $(document).ready(function() {
        var favArray = <?php echo json_encode($arrayPassedIn); ?>;

        $('#darkModeMyInfoBtn').on("click", function() {
                $('#darkModeMyInfoBtn').hide();
                $('#lightModeMyInfoBtn').show(); 
                favArray[2] = "0";
				updateStringPassedIn();
                $('#layoutBodyMode').removeClass('light');
                $('#layoutBodyMode').addClass('dark');
		});

        $('#lightModeMyInfoBtn').on("click", function() {
            $('#lightModeMyInfoBtn').hide(); 
            $('#darkModeMyInfoBtn').show();
                favArray[2] = "1";
				updateStringPassedIn();
                $('#layoutBodyMode').removeClass('dark');
                $('#layoutBodyMode').addClass('light');
		});

		function updateStringPassedIn(){
        	var stringPassedIn = favArray.toString().replaceAll(",", "");
        	$.ajax({
        			type: "POST",
        			url: "{{ route('updateUserShortcuts') }}",
        			data: {
            		_token: '<?php echo csrf_token(); ?>',
            		user_id: {{$user->id}},
            		shortcuts: stringPassedIn
    			}
    		});
            console.log(stringPassedIn);
    	}
    });
</script>




@endsection

@section('devcontent')
<div class="buttonArray">
    <button class="red centered small"><div class="icon icon-retweet"></div> report error</button>
    <button class="cyan centered small"><div class="icon icon-retweet"></div> comment</button>
</div> 
@endsection

@section('testcontent')
<div class="buttonArray">
    <button class="red centered small"><div class="icon icon-retweet"></div> report error</button>
    <button class="cyan centered small"><div class="icon icon-retweet"></div> comment</button>
</div> 
@endsection
@extends('layouts.zen5_layout_simple')
@section('maincontent')

    <?php
        $pdf = true;
        if($type === "file"){
            $doc = \App\InjuryFile::where('id', $id)->first();
            if(str_contains($doc->location, ".csv")){
                $pdf = false;
            }
        }else if($type === 'image'){
            $doc = \App\InjuryImage::where('id', $id)->first();
        }
    ?>
<div class="center"><button onclick="history.back()">go back</button></div>

@if($type === "file" && $pdf == true)
    <iframe src="/{{$doc->location}}?{{$date = str_replace(' ', '_', \Carbon\Carbon::now())}}" type="application/pdf" width="100%" height="100%" style="height:100vh;"></iframe>
@elseif($type === "file" && $pdf == false)
    <div id="CSVTable"></div>
    <script>
    $(function() {
        $('#CSVTable').CSVToTable('{{$doc->location}}');
    });
    </script>
@elseif($type === "image")
    <img src="{{$doc->location}}" width="100%">
@endif
<div class="center"><button onclick="history.back()">go back</button></div>
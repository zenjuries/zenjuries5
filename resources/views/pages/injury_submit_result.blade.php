@extends('layouts.zen5_layout')
@section('maincontent')

   
<div class="pageContent bgimage-bgheader pagebackground13 fade-in">
    <!--***********-->
    <div class="headerBlock" style="height:0px;">         
	</div>		

	<div class="contentBlock noPad">
		<div class="container noPad">

                           <div class="divCenter animate__animated animate__headShake"><img src="/images/icons/zenfucius.png"></div>
                           <div class="center">Your injury was successfully submitted.  Thank you!</div>
                           <br>
                           <div class="buttonArray">
                                <button onclick="window.location='/newinjury'" class="cyan centered"><div class="icon icon-arrow-left"></div> back</button>
                                <button onclick="window.location='/'" class="green centered" id="submitCompanyInfo"><div class="icon icon-home"></div> home</button>
                            </div>                            

		</div>
	</div>
</div>




@endsection

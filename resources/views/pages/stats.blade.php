@extends('layouts.zen5_layout')
@section('maincontent')
<?php
$company = Auth::user()->getCompany();
$company_id = $company->id;
$company_name = $company->company_name;
$agency_id = ($company->is_agency) ? $company->agency_id : NULL;
$load_company = ($company->is_agency) ? 0 : 1;
$logo_location = ($company->is_agency) ? NULL : $company->logo_location;

if($agency_id != NULL){
    $agency = \App\Agency::where('id', $agency_id)->first();
    $agency_company = \App\Company::where('is_agency', 1)->where('agency_id', $agency->id)->first();

    $zendex = $agency->calculateZendex();
    $zendex_gauge = "green";
    $zendex['points'] =  $zendex['points'] - 10;

    //adjust for max and min points 40-99
    if($zendex['points'] > 99){
        $zendex['points'] = 99;
    }
    if($zendex['points'] < 40){
        $zendex['points'] = 40;
    }
    //get the deg for the zendex gauge
    $zendex_deg = ($zendex['points'] * 1.8);

    //currently the zenrisk is just the difference of the current and max zendex(will be updated to actual calculation)
    $zenrisk = 100 - $zendex['points'];
    
    //overall effectiveness is currently based on the zendex(will be updated to actual calculation)
    $overall_effectiveness = 7;
    $effectiveness_message = "as effective as possible";
    if($zendex['points'] <= 99 && $zendex['points'] >= 80){
        $overall_effectiveness = 6;
        $effectiveness_message = "outstandingly effective";
    }elseif($zendex['points'] <= 79 && $zendex['points'] >= 60){
        $overall_effectiveness = 5;
        $effectiveness_message = "very effective";
    }elseif($zendex['points'] <= 59 && $zendex['points'] >= 40){
        $overall_effectiveness = 4;
        $effectiveness_message = "effective";
        $zendex_gauge = "yellow";
    }elseif($zendex['points'] <= 39 && $zendex['points'] >= 20){
        $overall_effectiveness = 3;
        $effectiveness_message = "moderately effective";
        $zendex_gauge = "yellow";
    }elseif($zendex['points'] <= 19 && $zendex['points'] >= 10){
        $overall_effectiveness = 2;
        $effectiveness_message = "could be more effective";
        $zendex_gauge = "red";
    }elseif($zendex['points'] <= 9 && $zendex['points'] >= 0){
        $overall_effectiveness = 1;
        $effectiveness_message = "not very effective";
        $zendex_gauge = "red";
    }

    $agency_company->total_injuries = count($agency_company->getInjuries());
    $agency_company->open_injuries = count($agency_company->getInjuries("open"));
    $agency_company->resolved_injuries = count($agency_company->getInjuries("resolved"));
    $agency_company->critical_injuries = count($agency_company->getCriticalInjuries());

    $companies = $agency->getCompanies("activated");
}
$company_start_date = \Carbon\Carbon::parse($company->created_at)->format('m-d-Y');
$comapny_end_date =  \Carbon\Carbon::today()->format('m-d-Y');

?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.5.1/jspdf.umd.min.js" integrity="sha512-qZvrmS2ekKPF2mSznTQsxqPgnpkI4DNTlrdUmTzrDgektczlKNRRhy5X5AAOnx5S09ydFYWWNSfcEqDTTHgtNA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<div class="pageContent bgimage-bgheader pagebackground8 fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                    <span>zenjuries stats </span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/magnify.png"></span><span id="companyName" class="subContent">viewing stats for {{$company_name}}</span>
                        
                </div>
                
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/stats.png">
            </div> 
        </div>           
	</div>


	<div class="contentBlock lessPadding">
		<div class="container setMinHeight">
            <div class="buttonArray">
                
                <button class="purple centered showZenModal" data-modalcontent="#dateRangeModal"><div class="icon icon-calendar"></div> edit date range</button>
                @if(Auth::user()->is_zagent)<button class="blue showZenModal" style="position:relative;top:-2px;" data-modalcontent="#selectPolicyholderModal"><div class="icon icon-company"></div> set policyholder</button>@endif
                <button class="grey centered"><div class="icon icon-crosshairs"></div> reset view</button>
                <!-- <button class="grey centered"><div class="icon icon-file-pdf-o"></div> save all pdf</button> -->
            </div>
            <div id="displayDateRange" class="center">Showing data from {{$company_start_date}} - {{$comapny_end_date}}</div>
            <br>
              
            <hr>
            
            <section class="sectionPanel transparent hasInjuries">
            <span class="sectionTitle"><div class="icon icon-graph-pie"></div> summary for {{$company_name}}</span>
            <button onclick="window.open('{{route('summaryReport', [$company_start_date, $comapny_end_date])}}','_blank')" class="savePrint"><div class="icon icon-file-pdf-o"></div> save pdf</button>
            <div class="sectionContent">
                <div class="itemGrid">
                    <div class="itemGrid__stat">
                        <div class="statsPanel">
                            <div class="statsPanelSetContent">
                                <div class="icon statRoleTypeIcon icon-money"></div>
                                <div class="teamUser">
                                    <span id="statTotalIncurred" class="statPanelText" data-id="0">0</span><span class="statPanelHeader required">Total Incurred</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="itemGrid__stat">
                        <div class="statsPanel">
                            <div class="statsPanelSetContent">
                                <div class="icon statRoleTypeIcon icon-cube"></div>
                                <div class="teamUser">
                                    <span id="statTotalPaid" class="statPanelText" data-id="0">0</span><span class="statPanelHeader required">Total Paid</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="itemGrid__stat">
                        <div class="statsPanel">
                            <div class="statsPanelSetContent">
                                <div class="icon statRoleTypeIcon icon-cubes"></div>
                                <div class="teamUser">
                                    <span id="statTotalReserves" class="statPanelText" data-id="0">0</span><span class="statPanelHeader required">Total Reserves</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="itemGrid__stat">
                        <div class="statsPanel">
                            <div class="statsPanelSetContent">
                                <div class="icon statRoleTypeIcon icon-exec"></div>
                                <div class="teamUser">
                                    <span id="statTotalClaims" class="statPanelText" data-id="0">0</span><span class="statPanelHeader required">Total Claims</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="itemGrid__stat">
                        <div class="statsPanel">
                            <div class="statsPanelSetContent">
                                <div class="icon statRoleTypeIcon icon-graph-trend"></div>
                                <div class="teamUser">
                                    <span id="statAverageDuration" class="statPanelText" data-id="0">0</span><span class="statPanelHeader required">Average Duration</span>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="itemGrid__stat">
                        <div class="statsPanel">
                            <div class="statsPanelSetContent">
                                <div class="icon statRoleTypeIcon icon-flash"></div>
                                <div class="teamUser">
                                    <span id="statSameDay" class="statPanelText" data-id="0">0</span><span class="statPanelHeader required">Reported Same Day</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="itemGrid">
                    @if($agency_id != NULL)
                    <div class="itemGrid__item thirds tall">
                        <div class="chartContainer">
                            <div class="darkChart wow animate__animated animate__fadeIn">
                                <span class="chartTitle center">zenRisk</span>
                                <span class="chartSubTitle center">your zenrisk score is how we measure your company projected success within zenjuries.  The lower the better.</span>
                                <div class="arcGauge">
                                    <div class="arcGaugeMask">
                                        <div class="arcGaugeContainer">
                                            <div class="arcGaugeTrack"></div>
                                            <div class="arcArc" style="--arcGaugeValue: {{$zenrisk}}deg">
                                            <div class="arcGaugeArc green"></div>
                                        </div>
                                        </div>
                                    </div>
                                    <span class="arcLabel1">currently</span>
                                    <span class="arcPercent">{{$zenrisk}}%</span>
                                    <span class="arcLabel2 FG__green">LOW</span>
                                </div>
                            </div>
                        </div>
                    </div>    
                    <div class="itemGrid__item thirds tall">
                        <div class="chartContainer">
                            <div class="darkChart wow animate__animated animate__fadeIn">
                                <span class="chartTitle center"> overall effectiveness</span>
                                <span class="chartSubTitle center">the higher the better.  this is a display of your team's effectiveness deterimined by performance and activity. </span>
                                <div class="effectiveMeter lv{{$overall_effectiveness}}">
                                    <div class="meterContainer">
                                        <div class="barBGContainer"><div class="barBG"></div></div>
                                        <div class="barOverlay"></div>
                                    </div>
                                    <span class="label1">{{$effectiveness_message}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="itemGrid__item thirds tall">
                        <div class="chartContainer">
                            <div class="darkChart wow animate__animated animate__bounce">
                                <span class="chartTitle center">zenDex Score</span>
                                <span class="chartSubTitle center">zendex is our propriatary scoring system which measures against other businesses within zenjuries.  Higher the better. </span>
                                <div class="arcGauge">
                                    <div class="arcGaugeMask">
                                        <div class="arcGaugeContainer">
                                            <div class="arcGaugeTrack"></div>
                                            <div class="arcArc" style='--arcGaugeValue:{{$zendex_deg}}deg;'>
                                                <div class="arcGaugeArc {{$zendex_gauge}}"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="arcLabel1">label</span>
                                    <span class="arcValue">{{$zendex['points']}}</span>
                                    <!--<span class="arcLabel2">improving</span>-->
                                </div>                                
                            </div>
                            <!--
                            <div class="darkChart wow animate__animated animate__fadeIn">
                                <span class="chartTitle center">zenDex Score</span>
                                <div class="arcGauge {{$zendex_gauge}}">
                                    <div class="arcGaugeMask">
                                        <div class="arcGaugeContainer">
                                            <div class="arcGaugeTrack"></div>
                                            <div class="arcArc">
                                            <div class="arcGaugeArc red"></div>
                                        </div>
                                        </div>
                                    </div>
                                    <span class="arcLabel1"></span>
                                    <span class="arcValue">{{$zendex['points']}}</span>
                                    <!--<span class="arcLabel2">improving</span>--
                                </div>                                
                            </div>
                            -->
                        </div>
                    </div>
                </div>
                @endif
                <div class="itemGrid">
                    <div class="itemGrid__item thirds">
                        <div class="chartContainer">
                            <div class="darkChart wow animate__animated animate__fadeIn">
                                <span class="chartTitle center">paid costs</span>
                                <span id="paidChartDates" class="chartSubTitle center">[{{$company_start_date}} - {{$comapny_end_date}}]</span>
                                <span id="paidChartDetails" class="chartSubTitle center">[details]</span>
                                <canvas class="totalChart" id="paidCostChartTotal"></canvas>
                                <span id="noPaidData" style="display:none">No Data</span>
                            </div>
                        </div>
                    </div>    
                    <div class="itemGrid__item thirds">
                        <div class="chartContainer">
                            <div class="darkChart wow animate__animated animate__fadeIn">
                                <span class="chartTitle center">reserve costs</span>
                                <span id="reserveChartDates" class="chartSubTitle center">[{{$company_start_date}} - {{$comapny_end_date}}]</span>
                                <span id="reserveChartDetails" class="chartSubTitle center">[details]</span>
                                <canvas class="totalChart" id="reserveCostChartTotal"></canvas>
                                <span id="noReserveData" style="display:none">No Data</span>
                            </div>
                        </div>
                    </div>
                    <div class="itemGrid__item thirds">
                        <div class="chartContainer">
                            <div class="darkChart wow animate__animated animate__fadeIn">
                                <span class="chartTitle center">final costs</span>
                                <span id="finalChartDates" class="chartSubTitle center">[{{$company_start_date}} - {{$comapny_end_date}}]</span>
                                <span id="finalChartDetails" class="chartSubTitle center">[details]</span>
                                <canvas class="totalChart" id="finalCostChartTotal"></canvas>
                                <span id="noFinalData" style="display:none">No Data</span>
                            </div>
                        </div>
                    </div>
                </div>  

                <div class="itemGrid" style="display: none;">
                    <div class="itemGrid__item half">
                        <div class="chartContainer" style="height:370px;">
                            <div class="darkChart wow animate__animated animate__fadeIn">
                                <span class="chartTitle center">paid costs analysis</span>
                                <span class="chartTitle center">final costs</span>
                                <span id="paidAnalysisChartDates" class="chartSubTitle center">[{{$company_start_date}} - {{$comapny_end_date}}]</span>
                                <span id="paidAnalysisChartDetails" class="chartSubTitle center">[details]</span>
                                <canvas id="paidCostAnalysisChart"></canvas>
                                <span id="noPaidCostAnalysis" style="display:none">No Data</span>
                            </div>
                        </div>
                    </div>    
                    <div class="itemGrid__item half">
                        <div class="chartContainer" style="height:370px;">
                            <div class="darkChart wow animate__animated animate__fadeIn">
                                <span class="chartTitle center">reserve costs analysis</span>
                                <span id="reserveAnalysisChartDates" class="chartSubTitle center">[{{$company_start_date}} - {{$comapny_end_date}}]</span>
                                <span id="reserveAnalysisChartDetails">[details]</span>
                                <canvas id="reserveCostsAnalysisChart"></canvas>
                                <span id="noReserveCostAnalysis" style="display:none">No Data</span>
                                <div style="height:60px;">
                                <span style="font-size:.7rem;display:block;" id="averageReserveTrend">Average Reserve Trend: Decreased </span>
                                <span style="font-size:.7rem;display:block;" id="reserveChange">Reserve Change: $531.00 Decrease </span>
                                <span style="font-size:.7rem;display:block;" id="averageReserveIncrease">Average Reserve Increase: $0.00 </span>
                                <span style="font-size:.7rem;display:block;" id="reserveDecrease">Reserve Decrease: $2,124.00 </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br><br>                                                                  
            </div>
            </section> 

<hr>

            <section class="sectionPanel transparent hasInjuries">
                <span class="sectionTitle"><div class="icon icon-graph-pie"></div> injury overview for {{$company_name}}</span>
                <button class="savePrint" id="downloadInjuryOverview" style="display:none;"><div class="icon icon-file-pdf-o"></div> save pdf</button>
                <div class="sectionContent">
                    <!--
                    <div class="center">
                        <div class="chartContainer">
                            <div class="darkChart overviewChart">
                                <span class="chartTitle">all injuries</span>
                                <table class="chartTable">
                                    <thead><td class="nostyle"></td> </td><td colspan="2">TOTAL</td><td colspan="2">MO</td><td colspan="2">LT</td></thead>
                                    <tr><td class="nostyle" style="width:100px;"> </td><td>count</td><td>incurred</td><td>count</td><td>incurred</td><td>count</td><td>incurred</td></tr>
                                    <tr><td class="nostyle setright"># of claims</td><td>32</td><td>45</td><td>54</td><td>54</td><td>32</td><td>24</td></tr>
                                    <tr><td class="nostyle setright">open claims</td><td>32</td><td>34</td><td>34</td><td>34</td><td>12</td><td>31</td></tr>
                                    <tr><td class="nostyle setright">closee claims</td><td>12</td><td>12</td><td>12</td><td>65</td><td>21</td><td>15</td></tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    -->
                    <div class="center">
                        <div class="chartContainer">
                            <div class="darkChart overviewChart" style="min-height:400px;">
                                <span class="chartTitle">injury lag time</span>
                                <span id="lagTimeTableDates" class="chartSubTitle center">[{{$company_start_date}} - {{$comapny_end_date}}]</span>
                                <span id="lagTimeTableDetails" class="chartSubTitle center">[details]</span>
                                <table class="chartTable">
                                    <tr>
                                        <td>same day</td>
                                        <td>next day</td>
                                        <td>same week</td>
                                        <td>2 weeks</td>
                                        <td>> 2 weeks</td>
                                    </tr>
                                    <tr>
                                        <td id="sameDay">0</td>
                                        <td id="nextDay">0</td>
                                        <td id="sameWeek">0</td>
                                        <td id="twoWeeks">0</td>
                                        <td id="greaterThanTwoWeeks">0</td>
                                    </tr>                                
                                </table>
                            </div>
                        </div>
                        
                        <div class="chartContainer">
                            <div class="darkChart overviewChart" style="min-height:400px;">
                                <span class="chartTitle">daily breakdown table</span>
                                <span id="dailyBreakdownTableDates" class="chartSubTitle center">[{{$company_start_date}} - {{$comapny_end_date}}]</span>
                                <span id="dailyBreakdownTableDetails" class="chartSubTitle center">[details]</span>
                                <table class="chartTable">
                                    <tr>
                                        <td>monday</td>
                                        <td>tuesday</td>
                                        <td>wednesday</td>
                                        <td>thursday</td>
                                        <td>friday</td>
                                        <td>saturday</td>
                                        <td>sunday</td>
                                    </tr>
                                    <tr>
                                        <td id="dbData1">0</td>
                                        <td id="dbData2">0</td>
                                        <td id="dbData3">0</td>
                                        <td id="dbData4">0</td>
                                        <td id="dbData5">0</td>
                                        <td id="dbData6">0</td>
                                        <td id="dbData7">0</td>
                                    </tr>                                
                                </table>
                            </div>
                        </div>
                        <div class="chartContainer">
                            <div class="darkChart overviewChart" style="min-height:400px;">
                                <span class="chartTitle">injury location table</span>
                                <span id="injuryLocationTableDates" class="chartSubTitle center">[{{$company_start_date}} - {{$comapny_end_date}}]</span>
                                <span id="injuryLocationDetails" class="chartSubTitle center">[details]</span>
                                <table class="chartTable">
                                    <tr>
                                        <td id="ilTitle1">N/A</td>
                                        <td id="ilTitle2">N/A</td>
                                        <td id="ilTitle3">N/A</td>
                                        <td id="ilTitle4">N/A</td>
                                        <td id="ilTitle5">N/A</td>
                                    </tr>
                                    <tr>
                                        <td id="ilData1">0</td>
                                        <td id="ilData2">0</td>
                                        <td id="ilData3">0</td>
                                        <td id="ilData4">0</td>
                                        <td id="ilData5">0</td>
                                    </tr>                                
                                </table>
                            </div>
                        </div>
                        <div class="chartContainer">
                            <div class="darkChart overviewChart" style="min-height:400px;">
                                <span class="chartTitle">injury type table</span>
                                <span id="injuryTypeTableDates" class="chartSubTitle center">[{{$company_start_date}} - {{$comapny_end_date}}]</span>
                                <span id="injuryTypeTableDetails" class="chartSubTitle center">[details]</span>
                                <table class="chartTable">
                                    <tr>
                                        <td id="itTitle1">N/A</td>
                                        <td id="itTitle2">N/A</td>
                                        <td id="itTitle3">N/A</td>
                                        <td id="itTitle4">N/A</td>
                                        <td id="itTitle5">N/A</td>
                                    </tr>
                                    <tr>
                                        <td id="itData1">0</td>
                                        <td id="itData2">0</td>
                                        <td id="itData3">0</td>
                                        <td id="itData4">0</td>
                                        <td id="itData5">0</td>
                                    </tr>                                  
                                </table>
                            </div>
                        </div>
                    </div>                                                  
                </div>
                <fieldset class="disclaimer">
                    <legend>disclaimer</legend>
                    Only injuries that occured after registering for Zenjuries will be counted in injury lag time chart.
                </fieldset>
            </section> 
<!--
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-graph-pie"></div> top 10 claims- by incurred</span>
                <button class="savePrint"><div class="icon icon-file-pdf-o"></div> save / print</button>
                <div class="sectionContent">
                    <div class="chartContainer">
                        <div class="darkChart">
                            <table class="chartTable">
                                    <tr>
                                        <td>claim #</td>
                                        <td>claimant</td>
                                        <td>DOI</td>
                                        <td>status</td>
                                        <td>paid</td>
                                        <td>reserves</td>
                                        <td>incurred</td>
                                    </tr>
                                    <tr><td>043-345-345657</td><td>thompson, kate</td><td>3/12/21</td><td>0</td><td>$2,345</td><td>$3,000</td><td>$5,345</td></tr>
                                    <tr><td>043-345-345657</td><td>thompson, kate</td><td>3/12/21</td><td>0</td><td>$2,345</td><td>$3,000</td><td>$5,345</td></tr>
                                    <tr><td>043-345-345657</td><td>thompson, kate</td><td>3/12/21</td><td>0</td><td>$2,345</td><td>$3,000</td><td>$5,345</td></tr>
                                    <tr><td>043-345-345657</td><td>thompson, kate</td><td>3/12/21</td><td>0</td><td>$2,345</td><td>$3,000</td><td>$5,345</td></tr>
                                    <tr><td>043-345-345657</td><td>thompson, kate</td><td>3/12/21</td><td>0</td><td>$2,345</td><td>$3,000</td><td>$5,345</td></tr>
                                    <tr><td>043-345-345657</td><td>thompson, kate</td><td>3/12/21</td><td>0</td><td>$2,345</td><td>$3,000</td><td>$5,345</td></tr>
                                    <tr><td>043-345-345657</td><td>thompson, kate</td><td>3/12/21</td><td>0</td><td>$2,345</td><td>$3,000</td><td>$5,345</td></tr>
                                    <tr><td>043-345-345657</td><td>thompson, kate</td><td>3/12/21</td><td>0</td><td>$2,345</td><td>$3,000</td><td>$5,345</td></tr>
                                    <tr><td>043-345-345657</td><td>thompson, kate</td><td>3/12/21</td><td>0</td><td>$2,345</td><td>$3,000</td><td>$5,345</td></tr>
                                    <tr><td>043-345-345657</td><td>thompson, kate</td><td>3/12/21</td><td>0</td><td>$2,345</td><td>$3,000</td><td>$5,345</td></tr>                                
                                </table>
                        </div>
                    </div>                                                  
                </div>
            </section> 
        -->
<!--
            <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-graph-pie"></div> top 5 stats</span>
            <button class="savePrint" id="downloadTop5Charts"><div class="icon icon-file-pdf-o"></div> save / print</button>
            <div class="sectionContent">
                <div class="itemGrid">
                    <div class="itemGrid__item thirds">
                    <div class="chartContainer" id="top5Charts">
                            <div class="darkChart">
                                top 5 causes pie chart
                                <canvas class="top5Chart" id="top5CausesPieChart"></canvas>
                            </div>
                        </div>
                    </div>    
                    <div class="itemGrid__item thirds">
                    <div class="chartContainer">
                            <div class="darkChart">
                                top 5 body parts pie chart
                                <canvas class="top5Chart" id="top5BodyPartsPieChart"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="itemGrid__item thirds">
                        <div class="chartContainer">
                            <div class="darkChart">
                                top 5 results pie chart
                                <canvas class="top5Chart" id="top5ResultsPieChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>                                                  
            </div>
            </section> 
        -->
            <section class="sectionPanel transparent hasInjuries" style="min-height:550px;">
                <span class="sectionTitle"><div class="icon icon-graph-pie"></div> injury breakdown for {{$company_name}}</span>
                <button class="savePrint" id="downloadBreakdownCharts" style="display:none;"><div class="icon icon-file-pdf-o"></div> save pdf</button>
                <div class="sectionContent">
                    <div class="itemGrid">
                        <div class="itemGrid__item thirds">
                            <div class="chartContainer">
                                <div class="darkChart">
                                    <span class="chartTitle">chart 1</span>
                                    <span id="locationChartDates" class="chartSubTitle center">[{{$company_start_date}} - {{$comapny_end_date}}]</span>
                                    <span id="locationChartDetails" class="chartSubTitle center">[details]</span>
                                    <canvas class="breakdownChart" id="injuryLocationChart"></canvas>
                                </div>
                            </div>
                        </div>    
                        <div class="itemGrid__item thirds">
                            <div class="chartContainer">
                                <div class="darkChart">
                                    <span class="chartTitle">chart 2</span>
                                    <span id="injuryTypeChartDates" class="chartSubTitle center">[{{$company_start_date}} - {{$comapny_end_date}}]</span>
                                    <span id="injuryTypeChartDetails" class="chartSubTitle center">[details]</span>
                                    <canvas class="breakdownChart" id="injuryTypeChart"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="itemGrid__item thirds">
                        <div class="chartContainer">
                                <div class="darkChart">
                                    <span class="chartTitle">chart 3</span>
                                    <span id="dailyBreakdownChartDates" class="chartSubTitle center">[{{$company_start_date}} - {{$comapny_end_date}}]</span>
                                    <span id="dailyBreakdownChartDetails" class="chartSubTitle center">[details]</span>
                                    <canvas class="breakdownChart" id="dailyBreakdownIncurredChart"></canvas>
                                </div>
                            </div>
                        </div>                    
                    </div>                                                  
                </div>
            </section>

<!--
            <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-graph-pie"></div> claims by location</span>
            <button class="savePrint"><div class="icon icon-file-pdf-o"></div> save / print</button>
            <div class="sectionContent">
                <div class="center">claims by location</div>
                    <div class="chartContainer">
                        <div class="darkChart">
                            <canvas id="claimsByLocationChart"></canvas>
                        </div>
                    </div>                                                  
                </div>                                                      
            </div>
            </section>          
        -->
            <section class="sectionPanel noInjuries">
                <span class="sectionTitle"><div class="icon icon-check-circle"></div> No Injuries</span>
                <div class="sectionContent">
                    <span>No injuries for this date range!</span>
                </div>
            </section>
            <div style="height:100px;"></div>
		</div>
	</div>

</div>

<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
    @include('partials.modals.dateRange')
    @include('partials.modals.selectPolicyholder')
</div>
 
<!-- CREATE CHART 1-->
<script>

$('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });


//this line is needed for jspdf do not remove
window.jsPDF = window.jspdf.jsPDF

//charts
var paidCostTotal;
var reserveCostTotal;
var finalCostTotal;
var costAvgChart;
var reserveCostAvgChart;
var dailyBreakdownIncurredChart;
var injuryLocationChart;
var injuryTypeChart;

var agency_id = '<?php echo $agency_id; ?>';
var startDate = null;
var endDate = null;
var load_company = '<?php echo $load_company; ?>';

//get all the stats to start off with
getStartingStats();

function getStartingStats(){
    if(load_company != 1){
        getAgencyStats(agency_id);
    }else{
        var company_id = '<?php echo $company_id; ?>';
        var company_name = '<?php echo json_encode($company_name); ?>';
        var logo_location = '<?php echo $logo_location; ?>';
        if(logo_location == ""){
            logo_location = "/images/avatars/no_profile_image.jpg";
        }
        getCompanyStats(company_id, company_name, logo_location);
    }
}

$('.companySelectButton').on('click', function(){
    var isCompany = $(this).data('company');
    var id = $(this).data('id');
    var companyName = $(this).find('span').text();
    $('#companyName').html('viewing stats for ' + companyName);
    startDate = null;
    endDate = null;
   
    if(isCompany === false){
        load_company = 0;
        //alert('agency');
        getAgencyStats(id);
        modal.close();
    }else if(isCompany === true){
        load_company = 1;
        getCompanyStats(id);
        modal.close();
        //alert('company');
    }else{
        alert('error');
    }
});

function getAgencyStats(agency_id){
    console.log(startDate);
    console.log(endDate);
    $.ajax({
        url: '<?php echo route('getAgencyStats') ?>',
        type: 'POST',
        data:{
            agency_id: agency_id,
            start_date: startDate,
            end_date: endDate,
            _token: "<?php echo csrf_token(); ?>"
            },
        success: function(data){
            console.log('agency');
            console.log(data);
            setTimeout(addStats(data), 5);
            //showAllStats();
        },
        error: function(data){
            
        }
    })
}

function getCompanyStats(company_id){
        currently_showing = company_id;
        $.ajax({
            url: '<?php echo route('getCompanyStats') ?>',
            type: 'POST',
            data:{
                company_id: company_id,
                start_date: startDate,
                end_date: endDate,
                _token: '<?php echo csrf_token(); ?>'
            },
        success: function(data){
            console.log('company');
            console.log(data);
            setTimeout(addStats(data), 5);
            //loadCompanyInfo(company_name, logo_location, company_id);
            //alert('success');
        },
        error: function(data){
            //console.log(data);
            // alert('error');
        }
    });
}

//update the page with the current stats
function addStats(stats_array){
    console.log('addStats');
    if(stats_array.total_injury_count === 0 || stats_array === ""){
        $('.hasInjuries').hide();
        $('.noInjuries').show();
    }else{
        $('.noInjuries').hide();
        $('.hasInjuries').show();

        //set the percent variables
        calculatePaidPercents(stats_array.average_paid_medical, stats_array.average_paid_legal, stats_array.average_paid_indemnity, stats_array.average_paid_miscellaneous);
        calculateReservePercents(stats_array.average_reserve_medical, stats_array.average_reserve_legal, stats_array.average_reserve_indemnity, stats_array.average_reserve_miscellaneous);
        calculateFinalPercents(stats_array.average_final_medical, stats_array.average_final_legal, stats_array.average_final_indemnity, stats_array.average_final_miscellaneous);

        // create stat panels
        createStatPanel(stats_array);

        //create the charts
        makeStatsCharts(stats_array);
    }
}
function createStatPanel(stats_array){
    if (stats_array['total_paid'] != '0'){
        $('#statTotalPaid').html(stats_array['total_paid']);
    }
    if (stats_array['total_reserves'] != '0'){
        $('#statTotalReserves').html(stats_array['total_reserves']);
    }
    if (stats_array['total_incurred'] != '0'){
        $('#statTotalIncurred').html(stats_array['total_incurred']);
    }
    if (stats_array['total_injury_count'] > 0){
        $('#statTotalClaims').html(stats_array['total_injury_count']);
    }
    // if (stats_array['average_injury_length'] > 0){
    //     $('#statAverageDuration').html(stats_array['average_injury_length']);
    // }
    if (stats_array['reporting_time_array'][0] > 0){
        $('#statSameDay').html(stats_array['reporting_time_array'][0]);
    }
}

function calculatePaidPercents(medical, legal, indemnity, misc){
    medical = parseInt(removeDollarSignForChart(medical));
    legal = parseInt(removeDollarSignForChart(legal));
    indemnity = parseInt(removeDollarSignForChart(indemnity));
    misc = parseInt(removeDollarSignForChart(misc));
    var total = (medical + legal + indemnity + misc);
            
    paidMedicalPercentage = ((medical / total) * 100).toFixed(2);
    if(isNaN(paidMedicalPercentage)){
        paidMedicalPercentage = 0;
    }
    paidLegalPercentage = ((legal / total) * 100).toFixed(2);
    if(isNaN(paidLegalPercentage)){
        paidLegalPercentage = 0;
    }
    paidIndemnityPercentage = ((indemnity / total) * 100).toFixed(2);
    if(isNaN(paidIndemnityPercentage)){
        paidIndemnityPercentage = 0;
    }
    paidMiscellaneousPercentage = ((misc / total) * 100).toFixed(2);
    if(isNaN(paidMiscellaneousPercentage)){
        paidMiscellaneousPercentage = 0;
    }
}

function calculateReservePercents(medical, legal, indemnity, misc){
    medical = parseInt(removeDollarSignForChart(medical));
    legal = parseInt(removeDollarSignForChart(legal));
    indemnity = parseInt(removeDollarSignForChart(indemnity));
    misc = parseInt(removeDollarSignForChart(misc));
    var total = (medical + legal + indemnity + misc);
            
    reserveMedicalPercentage = ((medical / total) * 100).toFixed(2);
    if(isNaN(reserveMedicalPercentage)){
        reserveMedicalPercentage = 0;
    }
    reserveLegalPercentage = ((legal / total) * 100).toFixed(2);
    if(isNaN(reserveLegalPercentage)){
        reserveLegalPercentage = 0;
    }
    reserveIndemnityPercentage = ((indemnity / total) * 100).toFixed(2);
    if(isNaN(reserveIndemnityPercentage)){
        reserveIndemnityPercentage = 0;
    }
    reserveMiscellaneousPercentage = ((misc / total) * 100).toFixed(2);
    if(isNaN(reserveMiscellaneousPercentage)){
        reserveMiscellaneousPercentage = 0;
    }
}

function calculateFinalPercents(medical, legal, indemnity, misc){
    medical = parseInt(removeDollarSignForChart(medical));
    legal = parseInt(removeDollarSignForChart(legal));
    indemnity = parseInt(removeDollarSignForChart(indemnity));
    misc = parseInt(removeDollarSignForChart(misc));
    var total = (medical + legal + indemnity + misc);
    finalMedicalPercentage = ((medical / total) * 100).toFixed(2);
    if(isNaN(finalMedicalPercentage)){
        finalMedicalPercentage = 0;
    }
    finalLegalPercentage = ((legal / total) * 100).toFixed(2);
    if(isNaN(finalLegalPercentage)){
        finalLegalPercentage = 0;
    }
    finalIndemnityPercentage = ((indemnity / total) * 100).toFixed(2);
    if(isNaN(finalIndemnityPercentage)){
        finalIndemnityPercentage = 0;
    }
    finalMiscellaneousPercentage = ((misc / total) * 100).toFixed(2);
    if(isNaN(finalMiscellaneousPercentage)){
        finalMiscellaneousPercentage = 0;
    }
}
function makeStatsCharts(stats_array){
    console.log('makeStatsCharts');
    var chart_options = {
        legend: {
            position: 'bottom'
        },
        animation: false
    }
    clearOldCharts();
    makePaidCostChart(stats_array, chart_options);
    makeReserveChart(stats_array, chart_options);
    makeFinalCostChart(stats_array, chart_options);
    makePaidCostAnalysisChart(stats_array, chart_options);
    makeReserveCostAnalysisChart(stats_array, chart_options);
    buildLagTimeTable(stats_array);
    buildDailyBreakdownChart(stats_array, chart_options);
    buildInjuryLocationChart(stats_array);
    buildInjuryTypeChart(stats_array);
    /*
    makeRcAnalysisReserveBarChart(stats_array, chart_options);
    makeRcAnalysisReservePieChart(stats_array, chart_options);
    makePcAnalysisPaidBarChart(stats_array);
    makePcAnalysisPaidPieChart(stats_array);
    */
}

//clear the old charts before adding the new ones
function clearOldCharts(){
    console.log(paidCostTotal);
    if(paidCostTotal !== undefined){
        paidCostTotal.destroy();
    }
    if(reserveCostTotal !== undefined){
        reserveCostTotal.destroy();
    }
    if(finalCostTotal !== undefined){
        finalCostTotal.destroy();
    }
    if(costAvgChart !== undefined){
        costAvgChart.destroy();
    }
    if(reserveCostAvgChart !== undefined){
        reserveCostAvgChart.destroy();
    }
    if(dailyBreakdownIncurredChart !== undefined){
        dailyBreakdownIncurredChart.destroy();
    }
    if(injuryLocationChart !== undefined){
        injuryLocationChart.destroy();
    }
    if(injuryTypeChart !== undefined){
        injuryTypeChart.destroy();
    }
    
}

function makePaidCostChart(stats_array, options){
    if(startDate !== null || endDate !== null){
        $('#paidChartDates').html(startDate + " - " + endDate);
    }
    $('#paidChartDetails').html('showing data for ' + stats_array['total_injury_count'] + ' injuries');
    if(stats_array['total_paid_medical'] === "$0.00" && stats_array['total_paid_indemnity'] === "$0.00" && stats_array['total_paid_legal'] === "$0.00" && stats_array['total_paid_miscellaneous'] === "$0.00"){
        console.log("No paid statistics");
        $('#noPaidData').show();
    }else{
        $('#noPaidData').hide();
        var ctx = document.getElementById("paidCostChartTotal").getContext('2d');
        paidCostTotal = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Medical', 'Indemnity', 'Legal', 'Miscellaneous'],
                datasets: [{
                    label: 'Costs',
                    data: [
                        removeDollarSignForChart(stats_array['total_paid_medical']),
                        removeDollarSignForChart(stats_array['total_paid_indemnity']),
                        removeDollarSignForChart(stats_array['total_paid_legal']),
                        removeDollarSignForChart(stats_array['total_paid_miscellaneous'])
                    ],
                    backgroundColor: [
                        'rgba(198,25,25,0.2)', 'rgba(210,135,22,0.2)',   'rgba(206,191,25,0.2)', 'rgba(125,205,34,0.2)'
                    ],
                    borderColor: [
                        'rgb(198,25,25)', 'rgb(210,135,22)',   'rgb(206,191,25)', 'rgb(125,205,34)'
                    ],
                    borderWidth: 1
                }],
            },
            options:{
                tooltips:{
                    backgroundColor: "red",
                    callbacks:{
                        label: function(tooltipItems) {
                            return "Costs $" + tooltipItems.yLabel ;
                        }
                    }
                },
                legend: false,
                scales:{
                    yAxes:[{
                        ticks:{
                            beginAtZero: true
                        }
                    }]
                },
                animation:{
                    duration: 2000,
                    easing: 'easeInExpo'
                },
            }
        });
    }
}

function makeReserveChart(stats_array, options){
    if(startDate !== null || endDate !== null){
        $('#reserveChartDates').html(startDate + " - " + endDate);
    }
    $('#reserveChartDetails').html('showing data for ' + stats_array['total_injury_count'] + ' injuries');
    if(stats_array['total_reserve_medical'] === "$0.00" && stats_array['total_reserve_indemnity'] === "$0.00" && stats_array['total_reserve_legal'] === "$0.00" && stats_array['total_reserve_miscellaneous'] === "$0.00"){
        console.log("No reserve statistics");
        $('#noReserveData').show();
    }else{
        $('#noReserveData').hide();
        var ctx = document.getElementById("reserveCostChartTotal").getContext('2d');
        reserveCostTotal = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Medical', 'Indemnity', 'Legal', 'Miscellaneous'],
                datasets: [{
                    label: 'Costs',
                    data: [
                        removeDollarSignForChart(stats_array['total_reserve_medical']),
                        removeDollarSignForChart(stats_array['total_reserve_indemnity']),
                        removeDollarSignForChart(stats_array['total_reserve_legal']),
                        removeDollarSignForChart(stats_array['total_reserve_miscellaneous'])
                    ],
                    backgroundColor: [
                        'rgba(22,168,194,0.2)', 'rgba(64,93,220,0.2)', 'rgba(181,45,202,0.2)', 'rgba(234,46,108,0.2)'
                    ],
                    borderColor: [
                        'rgb(22,168,194)', 'rgb(64,93,220)', 'rgb(181,45,202)', 'rgb(234,46,108)'
                    ],
                    borderWidth: 1
                }],
            },
            options:{
                tooltips:{
                    backgroundColor: "blue",
                    callbacks:{
                        label: function(tooltipItems) {
                            return "Costs $" + tooltipItems.yLabel ;
                        }
                    }
                },
                legend: false,
                scales:{
                    yAxes:[{
                        ticks:{
                            beginAtZero: true
                        }
                    }]
                },
                animation:{
                    duration: 2000,
                    easing: 'easeInBounce'
                },
            }
        });
    }
}

function makeFinalCostChart(stats_array, options){
    if(startDate !== null || endDate !== null){
        $('#finalChartDates').html(startDate + " - " + endDate);
    }
    $('#finalChartDetails').html('showing data for ' + stats_array['total_injury_count'] + ' injuries');
    if(stats_array['total_final_medical'] === "$0.00" && stats_array['total_final_indemnity'] === "$0.00" && stats_array['total_final_legal'] === "$0.00" && stats_array['total_final_miscellaneous'] === "$0.00"){
        console.log("No reserve statistics");
        $('#noFinalData').show();
    }else{
        $('#noFinalData').hide();
        var ctx = document.getElementById("finalCostChartTotal").getContext('2d');
        finalCostTotal = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Medical', 'Indemnity', 'Legal', 'Miscellaneous'],
                datasets: [{
                    label: 'Costs',
                    data: [
                        removeDollarSignForChart(stats_array['total_final_medical']),
                        removeDollarSignForChart(stats_array['total_final_indemnity']),
                        removeDollarSignForChart(stats_array['total_final_legal']),
                        removeDollarSignForChart(stats_array['total_final_miscellaneous'])
                    ],
                    backgroundColor: [
                        'rgba(198,25,25,0.2)', 'rgba(210,135,22,0.2)',   'rgba(206,191,25,0.2)', 'rgba(125,205,34,0.2)'
                    ],
                    borderColor: [
                        'rgb(198,25,25)', 'rgb(210,135,22)',   'rgb(206,191,25)', 'rgb(125,205,34)'
                    ],
                    borderWidth: 1
                }]
            },
            options:{
                showAllTooltips: true,
                tooltips:{
                    backgroundColor: "orange",
                    callbacks:{
                        label: function(tooltipItems) {
                            return "Costs $" + tooltipItems.yLabel ;
                        }
                    }
                },
                legend: false,
                scales:{
                    yAxes:[{
                        ticks:{
                            beginAtZero: true
                        }
                    }]
                },
                animation:{
                    duration: 2000,
                    easing: 'easeInBounce'
                },
            }
        });
    }
}

function  makePaidCostAnalysisChart(stats_array, chart_options){
    if(startDate !== null || endDate !== null){
        $('#paidAnalysisChartDates').html(startDate + " - " + endDate);
    }
    $('#paidAnalysisChartDetails').html('showing data for ' + stats_array['total_injury_count'] + ' injuries');
    var paidTotal = parseInt(removeDollarSignForChart(stats_array['average_paid_medical'])) + parseInt(removeDollarSignForChart(stats_array['average_paid_indemnity'])) + parseInt(removeDollarSignForChart(stats_array['average_paid_legal'])) + parseInt(removeDollarSignForChart(stats_array['average_paid_miscellaneous']));
    var finalTotal = parseInt(removeDollarSignForChart(stats_array['average_final_medical'])) + parseInt(removeDollarSignForChart(stats_array['average_final_indemnity'])) + parseInt(removeDollarSignForChart(stats_array['average_final_legal'])) + parseInt(removeDollarSignForChart(stats_array['average_final_miscellaneous']));
    if(stats_array['average_paid_medical'] === "$0.00" &&
     stats_array['average_paid_indemnity'] === "$0.00" && 
     stats_array['average_paid_legal'] === "$0.00" && 
     stats_array['average_paid_miscellaneous'] === "$0.00" &&
     stats_array['average_final_medical'] === "$0.00" &&
     stats_array['average_final_indemnity'] === "$0.00" &&
     stats_array['average_final_legal'] === "$0.00" &&
     stats_array['average_final_miscellaneous'] === "$0.00"){
        $("#noPaidCostAnalysis").show();
    }else{
        var ctx = document.getElementById("paidCostAnalysisChart").getContext('2d');
        costAvgChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [
                    'Paid Total',
                    'Paid Medical',
                    'Paid Indemnity',
                    'Paid Legal',
                    'Paid Misc',
                    'Final Total',
                    'Final Medical',
                    'Final Indemnity',
                    'Final Legal',
                    'Final Misc'
                ],
                datasets: [{
                    label: 'Costs',
                    data: [
                        paidTotal,
                        removeDollarSignForChart(stats_array['average_paid_medical']),
                        removeDollarSignForChart(stats_array['average_paid_indemnity']),
                        removeDollarSignForChart(stats_array['average_paid_legal']),
                        removeDollarSignForChart(stats_array['average_paid_miscellaneous']),
                        finalTotal,
                        removeDollarSignForChart(stats_array['average_final_medical']),
                        removeDollarSignForChart(stats_array['average_final_indemnity']),
                        removeDollarSignForChart(stats_array['average_final_legal']),
                        removeDollarSignForChart(stats_array['average_final_miscellaneous'])
                    ],
                    backgroundColor: [
                        'rgba(198,25,25,0.2)', 
                        'rgba(210,135,22,0.2)', 
                        'rgba(206,191,25,0.2)', 
                        'rgba(125,205,34,0.2)', 
                        'rgba(198,25,25,0.2)', 
                        'rgba(210,135,22,0.2)', 
                        'rgba(206,191,25,0.2)', 
                        'rgba(125,205,34,0.2)',
                        'rgba(198,25,25,0.2)', 
                        'rgba(210,135,22,0.2)'
                    ],
                    borderColor: [
                        'rgb(198,25,25)', 
                        'rgb(210,135,22)', 
                        'rgb(206,191,25)', 
                        'rgb(125,205,34)', 
                        'rgb(198,25,25)', 
                        'rgb(210,135,22)', 
                        'rgb(206,191,25)', 
                        'rgb(125,205,34)',
                        'rgb(198,25,25)', 
                        'rgb(210,135,22)'
                    ],
                    borderWidth: 1
                }]
            },
            options:{
                showAllTooltips: true,
                tooltips:{
                    backgroundColor: "orange",
                    callbacks:{
                        label: function(tooltipItems) {
                            return "Costs $" + tooltipItems.yLabel ;
                        }
                    }
                },
                legend: false,
                scales:{
                    yAxes:[{
                        ticks:{
                            beginAtZero: true
                        }
                    }]
                },
                animation:{
                    duration: 2000,
                    easing: 'easeInBounce'
                },
            }
        });
    }

}

function makeReserveCostAnalysisChart(stats_array, chart_options){
    if(startDate !== null || endDate !== null){
        $('#reserveAnalysisChartDates').html(startDate + " - " + endDate);
    }
    $('#reserveAnalysisChartDetails').html('showing data for ' + stats_array['total_injury_count'] + ' injuries');
    var reserveTotal = parseInt(removeDollarSignForChart(stats_array['average_reserve_medical'])) + parseInt(removeDollarSignForChart(stats_array['average_reserve_indemnity'])) + parseInt(removeDollarSignForChart(stats_array['average_reserve_legal'])) + parseInt(removeDollarSignForChart(stats_array['average_reserve_miscellaneous']));
    if(stats_array['average_reserve_medical'] === "$0.00" && stats_array['average_reserve_indemnity'] === "$0.00" && stats_array['average_reserve_legal'] === "$0.00" && stats_array['average_reserve_miscellaneous'] === "$0.00"){
        $("#noReserveCostAnalysis").show();
    }else{
        $("#noReserveCostAnalysis").hide();
        var ctx = document.getElementById("reserveCostsAnalysisChart").getContext('2d');
        reserveCostAvgChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Total', 'Medical', 'Indemnity', 'Legal', 'Miscellaneous'],
                datasets: [{
                    label: 'Costs',
                    data: [
                        reserveTotal,
                        removeDollarSignForChart(stats_array['average_reserve_medical']),
                        removeDollarSignForChart(stats_array['average_reserve_indemnity']),
                        removeDollarSignForChart(stats_array['average_reserve_legal']),
                        removeDollarSignForChart(stats_array['average_reserve_miscellaneous'])
                    ],
                    backgroundColor: [
                        'rgba(22,168,194,0.2)', 'rgba(64,93,220,0.2)', 'rgba(181,45,202,0.2)', 'rgba(234,46,108,0.2)', 'rgba(22,168,194,0.2)'
                    ],
                    borderColor: [
                        'rgb(22,168,194)', 'rgb(64,93,220)', 'rgb(181,45,202)', 'rgb(234,46,108)', 'rgb(22,168,194)'
                    ],
                    borderWidth: 1
                }],
            },
            options:{
                tooltips:{
                    backgroundColor: "blue",
                    callbacks:{
                        label: function(tooltipItems) {
                            return "Costs $" + tooltipItems.yLabel ;
                        }
                    }
                },
                legend: false,
                scales:{
                    yAxes:[{
                        ticks:{
                            beginAtZero: true
                        }
                    }]
                },
                animation:{
                    duration: 2000,
                    easing: 'easeInBounce'
                },
            }
        });
    }

    $('#averageReserveTrend').html("Average Reserve Trend: " + stats_array['reserve_trend']);
    $('#reserveChange').html("Reserve Change: " + stats_array['average_reserve_movement']);
    $('#averageReserveIncrease').html("");
    $('#reserveDecrease').html("");
}

function buildLagTimeTable(stats_array){
    if(startDate !== null || endDate !== null){
        $('#lagTimeTableDates').html(startDate + " - " + endDate);
    }
    $('#lagTimeTableDetails').html('showing data for ' + stats_array['total_injury_count'] + ' injuries');
    $('#sameDay').html(stats_array['reporting_time_array'][0]);
    $('#nextDay').html(stats_array['reporting_time_array'][1]);
    $('#sameWeek').html(stats_array['reporting_time_array'][2]);
    $('#twoWeeks').html(stats_array['reporting_time_array'][3]);
    $('#greaterThanTwoWeeks').html(stats_array['reporting_time_array'][4]);
}

function buildDailyBreakdownChart(stats_array, chart_options){
    if(startDate !== null || endDate !== null){
        $('#dailyBreakdownChartDates').html(startDate + " - " + endDate);
        $('#dailyBreakdownTableDates').html(startDate + " - " + endDate);
    }
    $('#dailyBreakdownChartDetails').html('showing data for ' + stats_array['total_injury_count'] + ' injuries');
    $('#dailyBreakdownTableDetails').html('showing data for ' + stats_array['total_injury_count'] + ' injuries');
    dailyBreakdownIncurredChart = new Chart(document.getElementById('dailyBreakdownIncurredChart'), {
        type: 'pie',
        data: {
            labels: ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'],
            datasets: [{
                label: '# of Injuries',
                data: [
                    stats_array['daily_breakdown']["Monday"], 
                    stats_array['daily_breakdown']["Tuesday"], 
                    stats_array['daily_breakdown']["Wednesday"], 
                    stats_array['daily_breakdown']["Thursday"], 
                    stats_array['daily_breakdown']["Friday"], 
                    stats_array['daily_breakdown']["Saturday"], 
                    stats_array['daily_breakdown']["Sunday"]
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(230, 230, 250, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(230, 230, 250, 1)'
                ],
                borderWidth: 1
            }]
        },
    });
    $('#dbData1').html(stats_array['daily_breakdown']["Monday"]);
    $('#dbData2').html(stats_array['daily_breakdown']["Tuesday"]);
    $('#dbData3').html(stats_array['daily_breakdown']["Wednesday"]);
    $('#dbData4').html(stats_array['daily_breakdown']["Thursday"]);
    $('#dbData5').html(stats_array['daily_breakdown']["Friday"]);
    $('#dbData6').html(stats_array['daily_breakdown']["Saturday"]);
    $('#dbData7').html(stats_array['daily_breakdown']["Sunday"]);
}

function buildInjuryLocationChart(stats_array){
    if(startDate !== null || endDate !== null){
        $('#locationChartDates').html(startDate + " - " + endDate);
        $('#injuryLocationTableDates').html(startDate + " - " + endDate);
    }
    $('#locationChartDetails').html('showing data for ' + stats_array['total_injury_count'] + ' injuries');
    $('#injuryLocationTableDetails').html('showing data for ' + stats_array['total_injury_count'] + ' injuries');
    injuryLocationChart = new Chart(document.getElementById('injuryLocationChart'), {
        type: 'pie',
        data: {
            labels: [
                stats_array['injury_location_breakdown'][0][0], 
                stats_array['injury_location_breakdown'][0][1], 
                stats_array['injury_location_breakdown'][0][2], 
                stats_array['injury_location_breakdown'][0][3], 
                stats_array['injury_location_breakdown'][0][4]
                ],
            datasets: [{
                label: '# of Injuries',
                data: [
                    stats_array['injury_location_breakdown'][1][0], 
                    stats_array['injury_location_breakdown'][1][1], 
                    stats_array['injury_location_breakdown'][1][2], 
                    stats_array['injury_location_breakdown'][1][3], 
                    stats_array['injury_location_breakdown'][1][4]
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(230, 230, 250, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(230, 230, 250, 1)'
                ],
                borderWidth: 1
            }]
        }
    });

    if(stats_array['injury_location_breakdown'][0][0]){
        $('#ilTitle1').html(stats_array['injury_location_breakdown'][0][0]);
    }else{
        $('#ilTitle1').html("N/A");
    }
    if(stats_array['injury_location_breakdown'][0][1]){
        $('#ilTitle2').html(stats_array['injury_location_breakdown'][0][1]);
    }else{
        $('#ilTitle2').html("N/A");
    }
    if(stats_array['injury_location_breakdown'][0][2]){
        $('#ilTitle3').html(stats_array['injury_location_breakdown'][0][2]);
    }else{
        $('#ilTitle3').html("N/A");
    }
    if(stats_array['injury_location_breakdown'][0][3]){
        $('#ilTitle4').html(stats_array['injury_location_breakdown'][0][3]);
    }else{
        $('#ilTitle4').html("N/A");
    }
    if(stats_array['injury_location_breakdown'][0][4]){
        $('#ilTitle5').html(stats_array['injury_location_breakdown'][0][4]);
    }else{
        $('#ilTitle5').html("N/A");
    }

    $('#ilData1').html(stats_array['injury_location_breakdown'][1][0]);
    $('#ilData2').html(stats_array['injury_location_breakdown'][1][1]);
    $('#ilData3').html(stats_array['injury_location_breakdown'][1][2]);
    $('#ilData4').html(stats_array['injury_location_breakdown'][1][3]);
    $('#ilData5').html(stats_array['injury_location_breakdown'][1][4]);
}

function buildInjuryTypeChart(stats_array){
    if(startDate !== null || endDate !== null){
        $('#injuryTypeChartDates').html(startDate + " - " + endDate);
        $('#injuryTypeTableDates').html(startDate + " - " + endDate);
    }
    $('#injuryTypeChartDetails').html('showing data for ' + stats_array['total_injury_count'] + ' injuries');
    $('#injuryTypeTableDetails').html('showing data for ' + stats_array['total_injury_count'] + ' injuries');
    injuryTypeChart = new Chart(document.getElementById('injuryTypeChart'), {
        type: 'pie',
        data: {
            labels: [
                stats_array['injury_type_breakdown'][0][0], 
                stats_array['injury_type_breakdown'][0][1], 
                stats_array['injury_type_breakdown'][0][2], 
                stats_array['injury_type_breakdown'][0][3], 
                stats_array['injury_type_breakdown'][0][4]
                ],
            datasets: [{
                label: '# of Injuries',
                data: [
                    stats_array['injury_type_breakdown'][1][0], 
                    stats_array['injury_type_breakdown'][1][1], 
                    stats_array['injury_type_breakdown'][1][2], 
                    stats_array['injury_type_breakdown'][1][3], 
                    stats_array['injury_type_breakdown'][1][4]
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(230, 230, 250, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(230, 230, 250, 1)'
                ],
                borderWidth: 1
            }]
        }
    });

    if(stats_array['injury_type_breakdown'][0][0]){
        $('#itTitle1').html(stats_array['injury_type_breakdown'][0][0]);
    }else{
        $('#itTitle1').html('N/A');
    }
    if(stats_array['injury_type_breakdown'][0][1]){
        $('#itTitle2').html(stats_array['injury_type_breakdown'][0][1]);
    }else{
        $('#itTitle2').html('N/A');
    }
    if(stats_array['injury_type_breakdown'][0][2]){
        $('#itTitle3').html(stats_array['injury_type_breakdown'][0][2]);
    }else{
        $('#itTitle3').html('N/A');
    }
    if(stats_array['injury_type_breakdown'][0][3]){
        $('#itTitle4').html(stats_array['injury_type_breakdown'][0][3]);
    }else{
        $('#itTitle4').html('N/A');
    }
    if(stats_array['injury_type_breakdown'][0][4]){
        $('#itTitle5').html(stats_array['injury_type_breakdown'][0][4]);
    }else{
        $('#itTitle5').html('N/A');
    }

    $('#itData1').html(stats_array['injury_type_breakdown'][1][0]);
    $('#itData2').html(stats_array['injury_type_breakdown'][1][1]);
    $('#itData3').html(stats_array['injury_type_breakdown'][1][2]);
    $('#itData4').html(stats_array['injury_type_breakdown'][1][3]);
    $('#itData5').html(stats_array['injury_type_breakdown'][1][4]);
}

/*
  
//<!-- CREATE TOP 5 CAUSES PIE CHART -->

    var causesPieChart = new Chart(document.getElementById('top5CausesPieChart'), {
        type: 'pie',
        data: {
            labels: ['strain','cut/puncture','lifting','hand tools/machine use','twisting'],
            datasets: [{
                label: '%',
                data: [25, 16, 16.67, 16.67, 25.66],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        }
    });

//<!-- CREATE TOP 5 BODY PARTS -->

    var bodyPartsPieChart = new Chart(document.getElementById('top5BodyPartsPieChart'), {
        type: 'pie',
        data: {
            labels: ['foot','hand','fingers','ankle','thumb'],
            datasets: [{
                label: '%',
                data: [21.43, 28.57, 21.43, 14.29, 14.29],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        }
    });


//<!-- CREATE TOP 5 RESULTS PIE CHART -->

    var bodyResultsPieChart = new Chart(document.getElementById('top5ResultsPieChart'), {
        type: 'pie',
        data: {
            labels: ['crushing','strain','laceration','multiple injuries','all others'],
            datasets: [{
                label: '%',
                data: [22.22, 22.22, 33.33, 16.67, 5.5],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        }
    });



//<!-- CLAIMS BY LOCATION BAR CHART -->

    var claimsByLocationChart = new Chart(document.getElementById('claimsByLocationChart'), {
        type: 'bar',
        data: {
            labels: [
                'Baston Monuments, Inc',
                'Keystone Menorials, Inc',
                'Eagle Granite Compnay, Inc',
                'Imex International, Inc', 
                'Savannah Valley Quarries, LLC', 
                'Walker Granite Comapny, Inc', 
                'Hillcrest Granite Company, Inc',
                'Quarries II, LLC',
                'Sterling Gray Quarries, LLC',
                'Childs & Childs Granite Company. Inc',
                'King\'s Monument Company, Inc',
                'Eagle\'s Danburg Quarry, Inc',
                'Hillcrest Veterans, LLC'
            ],
            datasets: [{
                label: '# of Injuries',
                data: [1, 4, 4, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(230, 230, 250, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(230, 230, 250, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
*/
$('#downloadInjuryOverview').on('click', function(event){
    //find a way to download html tables
});

$('#downloadBreakdownCharts').on('click', function(event){
    //get the innder height and width of the div
    var breakdownChartsHeight = $('.contentBlock').innerHeight();
    var breakdownChartsWidth = $('.contentBlock').innerWidth();

    //create new canvas object that will be populated with all the chart canvases
    var pdfCanvas = $('<canvas />').attr({
        id: "canvaspdf",
        width: breakdownChartsWidth,
        height: breakdownChartsHeight
    });

    //keep track of canvas position
    var pdfctx = $(pdfCanvas)[0].getContext('2d');
    var pdfctxX = 0;
    var pdfctxY = 0;
    var buffer = 100;

    //for each canvas going on the pdf (each chart shoudl have a class for the section it is in)
    $(".breakdownChart").each(function(index){
        //get heigh and width of chart
        var canvasHeight = $(this).innerHeight();
        var canvasWidth = $(this).innerWidth();

        //draw the chart onto the canvas
        pdfctx.drawImage($(this)[0], pdfctxX, pdfctxY, canvasWidth, canvasHeight);
        pdfctxX += canvasWidth + buffer;

        //replicate grid pattern on new canvas
        if(index % 2 == 1){
            pdfctxX = 0;
            pdfctxY += canvasHeight + buffer;
        }
    });

    //create the pdf
    var pdf = new jsPDF('1', 'pt', [breakdownChartsWidth, breakdownChartsWidth]);
    //add the images
    pdf.addImage($(pdfCanvas)[0], 'PNG', 0, 0);
    //download the pdf
    pdf.save('injuryBreakdownCharts.pdf');

});

$('#downloadTop5Charts').on('click', function(event){
    //get the innder height and width of the div
    var top5ChartsHeight = $('.contentBlock').innerHeight();
    var top5ChartsWidth = $('.contentBlock').innerWidth();

    //create new canvas object that will be populated with all the chart canvases
    var pdfCanvas = $('<canvas />').attr({
        id: "canvaspdf",
        width: top5ChartsWidth,
        height: top5ChartsHeight
    });

    //keep track of canvas position
    var pdfctx = $(pdfCanvas)[0].getContext('2d');
    var pdfctxX = 0;
    var pdfctxY = 0;
    var buffer = 100;

    //for each canvas going on the pdf (each chart shoudl have a class for the section it is in)
    $(".top5Chart").each(function(index){
        //get heigh and width of chart
        var canvasHeight = $(this).innerHeight();
        var canvasWidth = $(this).innerWidth();

        //draw the chart onto the canvas
        pdfctx.drawImage($(this)[0], pdfctxX, pdfctxY, canvasWidth, canvasHeight);
        pdfctxX += canvasWidth + buffer;

        //replicate grid pattern on new canvas
        if(index % 2 == 1){
            pdfctxX = 0;
            pdfctxY += canvasHeight + buffer;
        }

    });

    //create the pdf
    var pdf = new jsPDF('1', 'pt', [top5ChartsWidth, top5ChartsWidth]);
    //add the images
    pdf.addImage($(pdfCanvas)[0], 'PNG', 0, 0);
    //download the pdf
    pdf.save('top5charts.pdf');
});

function removeDollarSignForChart(cost){
    return cost.replace(/\$|,/g, "");
}

$('#submitDateRange').on('click', function(){
	startDate = $('#fromDate').val();
	endDate = $('#toDate').val();
    $('#displayDateRange').html("showing data from " + startDate + ' through ' + endDate);
    if(load_company != 1){
        getAgencyStats(agency_id);
    }else{
        var company_id = '<?php echo $company_id; ?>';
        var company_name = '<?php echo json_encode($company_name); ?>';
        var logo_location = '<?php echo $logo_location; ?>';
        if(logo_location == ""){
            logo_location = "/images/avatars/no_profile_image.jpg";
        }
        getCompanyStats(company_id, company_name, logo_location);
    }
    modal.close();
	
});
</script>
@endsection


@extends('layouts.zen5_layout')
@section('maincontent')

    <style>
        .uploadedMedia span.mediaDownload{bottom:4px;right:5px;}
        .uploadedMedia span.mediaDownload:after{font-family:'zenjuriesfont';font-size:1.2rem;content:'\e01e';color:white;opacity:.5;}
        .uploadedMedia span.mediaDownload:hover:after{color:rgb(0, 204, 255);opacity:1;}
    </style>

	<?php
		$injury = \App\Injury::where('id', $injury_id)->first();
        $squad = \App\Squad::where('id', $injury->squad_id)->first();
		//$latest_mood_post =
		//$latest_experience_post =


		$injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();
        $injury_alert = \App\FirstReportOfInjury::where('injury_id', $injury->id)->first();

        $employee_id = $injured_employee->id;

        $employeeStatus = $injury->employee_status;
        $status_history = \App\EmployeeStatusHistory::where('injury_id', $injury_id)->get();

        $trimedResolvedAt = substr($injury->resolved_at, 0, 10);
        $trimedLitigationAt = substr($injury->litigation_paused_date, 0, 10);


		function printTypes($injury){
		    $types = $injury->getTypes();
		    $string = "";
		    foreach($types as $type){
		        $string.= $type->type . ", ";
			}
			echo rtrim($string, ", ");
		}

		function printLocations($injury){
			$locations = $injury->getLocations();
			$string = "";
			foreach($locations as $location){
			    $string.= $location->location . ", ";
			}
			echo rtrim($string, ", ");
		}

    $claim_posts = $injury->getDiaryPosts('claim');
	if(count($claim_posts) > 0){
        $latest_claim_post = $claim_posts->last();
    }else $latest_claim_post = NULL;

    $injury_posts = $injury->getDiaryPosts('injury');
    if(count($injury_posts) > 0){
        $latest_injury_post = $injury_posts->last();
    }else $latest_injury_post = NULL;

    $diary_posts = $injury->getDiaryPosts();
    $tree_posts = $injury->getTreePosts();

    function calculatePostsChart($tree_posts){
        $chat_count = 0;
        $diary_count = 0;
        foreach($tree_posts as $post){
            if($post->type === "chatPost"){
                $chat_count++;
            }else if($post->type === "diaryPost"){
                $diary_count++;
            }
        }
        $return_array = [];
        $return_array['total_posts'] = $chat_count + $diary_count;
        $return_array['chat_count'] = $chat_count;
        $return_array['diary_count'] = $diary_count;

        if($diary_count > $chat_count){
            $return_array['setting'] = "user";
        }else if($diary_count < $chat_count){
            $return_array['setting'] = "team";
        }else{
            $return_array['setting'] = "base";
        }

        //$return_array['setting']

        return $return_array;
    }

    $post_chart_array = calculatePostsChart($tree_posts);

    function calculateProgressChart($injury){


        $days = $injury->getLength();
        if($injury->resolved === 1){
            $state = 7;
        }else if($days === 0){
            $state = 1;
        }else if(($days / 30) < 1){
            $state = 2;
        }else if(($days / 30) < 2){
            $state = 3;
        }else if(($days / 30) < 3){
            $state = 4;
        }else if(($days / 30) < 4){
            $state = 5;
        }else {
            $state = 6;
        }

        return $state;


    }

    function calculateMoodChart($injury){
        $mood_array = array();

        $diary_posts = \App\DiaryPost::where('injury_id', $injury->id)->latest()->take(5)->get();
        if($diary_posts !== NULL){
            $diary_posts = $diary_posts->reverse();

            foreach($diary_posts as $post){
                array_push($mood_array, $post->mood);
            }
        }


        while(count($mood_array) < 5){
            array_unshift($mood_array, '-?-');
        }

        return $mood_array;

    }

    $recent_mood_array = calculateMoodChart($injury);

    function calculateStatusChart($injury){
        $status = $injury->employee_status;
        if($status === "Hospital" || $status === "Urgent Care" || $status === "Family Practice"){
            $state = 1;
        }else if($status === "Home"){
            $state = 3;
        }else if($status === "Light Duty"){
            $state = 4;
        }else if($status === "Full Duty"){
            $state = 5;
        }else if($status === "Maximum Medical Improvement"){
            $state = 7;
        }else{
            $state = 0;
        }

        return $state;
    }

    $state = calculateStatusChart($injury);

    $costs = $injury->getClaimCosts();
    $medical_total = $costs['reserve_medical'] + $costs['paid_medical'];
    $indemnity_total = $costs['reserve_indemnity'] + $costs['paid_indemnity'];
    $legal_total = $costs['reserve_legal'] + $costs['paid_legal'];
    $misc_total = $costs['reserve_misc'] + $costs['paid_misc'];

    if($costs['reserve_medical'] <= 0 && $medical_total <= 0){
        $medical_percent = 50;
    }else{
        $medical_percent = ($costs['reserve_medical'] / $medical_total) * 100;
    }
    if($costs['reserve_indemnity'] <= 0 && $indemnity_total <= 0){
        $indemnity_percent = 50;
    }else{
        $indemnity_percent = ($costs['reserve_indemnity'] / $indemnity_total) * 100;
    }
    if($costs['reserve_legal'] <= 0 && $legal_total <= 0){
        $legal_percent = 50;
    }else{
        $legal_percent = ($costs['reserve_legal'] / $legal_total) * 100;
    }
    if($costs['reserve_misc'] <= 0 && $misc_total <= 0){
        $misc_percent = 50;
    }else{
        $misc_percent = ($costs['reserve_misc'] / $misc_total) * 100;
    }

    if(!empty($injury->final_medical_cost)){
        $final_costs_set = true;
    }else if(!empty($injury->final_indemnity_cost)){
        $final_costs_set = true;
    }else if(!empty($injury->final_legal_cost)){
        $final_costs_set = true;
    }else if(!empty($injury->final_misc_cost)){
        $final_costs_set = true;
    }else{
        $final_costs_set = false;
    }

    if($injury->getLength() > 99){
        $injury_duration = "99+";
    }else{
        $injury_duration = $injury->getLength();
    }

	//arrays for message popup [message icon, message title, message body, button color, button ID, button icon, button label]
    $buttonID1 = "confirm";
	$message1 = array("hmm-color", "message title", "message body", "red", "dismiss", "search", "dismiss", "green", $buttonID1, "check", "confirm");
	$popmessage = $message1;
    ?>


<div class="pageContent bgimage-bgheader pagebackground3 fade-in">

    <div class="headerBlock noTitle"></div>
        <!--***********
        <div class="headerContainer">
            <div class="pageHeader animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                    @if($injury->resolved === 1)
                        <span>injury resolved</span><span id="resolvedOnDate1" class="headerStatus FG__green">{{$trimedResolvedAt}}</span>
                    @elseif($injury->paused_for_litigation === 1)
                        <span>injury in litigation</span><span class="headerStatus FG__red">{{$trimedLitigationAt}}</span>
                    @else
                        <span>injury details</span><span class="headerStatus FG__grey">status:<b>open</b></span>
                    @endif
                </div>

                <div class="pageHeader__subTitle">
                        @if($injury->resolved === 1)
                            <span class="icon"><img src="/images/icons/checkheart.png"></span><span id="resolvedOnDate2" class="subContent">resolved on {{$trimedResolvedAt}}</span>
                        @elseif($injury->paused_for_litigation === 1)
                            <span class="icon"><img src="/images/icons/litigation.png"></span><span class="subContent">litigated @if($trimedLitigationAt != "") on {{$trimedLitigationAt}}@endif</span>
                        @else
                            <span class="icon"><img src="/images/icons/heart.png"></span><span class="subContent">{{$injured_employee->name}}'s injury</span>
                        @endif
                </div>

            </div>

            <div class="pageIcon animate__animated animate__bounceInRight">
                @if($injury->resolved === 1)
                    <img src="/images/icons/heart.png">
                @elseif($injury->paused_for_litigation === 1)
                    <img src="/images/icons/time-crit.png">
                @else
                    <img src="/images/icons/stethescope.png">
                @endif
            </div>

        </div>
	</div>-->

<script>
$(document).ready(function() {

    var tab = '<?php echo $tab; ?>';

    $('.zenTabs').addClass(tab);
    $('.zenTabContent.' + tab + 'Tab').show();
	$('.zenTabs').on('click', 'li', function(){
		var tab_class = $(this).attr('class');
		var target = $(this).data('target');
		$('.zenTabs').removeClass().addClass('zenTabs ' + tab_class);
		$('.zenTabContent').hide();
		$('.zenTabContent.' + target).show();
	});

});
</script>
<script>
$(document).ready(function() {
    @if($injury->resolved_at === null && $injury->resolved === 1)
        $('.showZencareModal[data-modalcontent="#completeInjuryModal"]').click();
    @endif
});
</script>

<style>
.injuryDetailsHeader{display:table;width:100%;height:40px;padding:5px;}
.injuryDetailsHeader .leftSide{display:table-cell;width:50%;}
.injuryDetailsHeader .rightSide{display:table-cell;width:50%;text-align:right;}
.injuryDetailsHeader span.avatar{top:1px;left:2px;display:inline-block;width:30px;height:30px;background-size:cover;border-radius:50%;}
.injuryDetailsHeader span{position:relative;display:inline-block;}
.injuryDetailsHeader span.employeeName{top:-8px;left:6px;color:white;}
.injuryDetailsHeader span.employeeStatus{top:-8px;right:6px;color:white;}
</style>


	<div class="contentBlock noPad">
		<div class="container noPad" style="padding-top:8px;">
			<div class="zen-actionPanel noHover noPad" style="min-height:620px;margin-bottom:50px;">
                <div class="injuryDetailsHeader">
                    <div class="leftSide">
                        <span class="avatar" style="background-color:{{$injured_employee->avatar_color}};background-image:url('{{$injured_employee->photo_url}}');"></span>
                        <span class="employeeName">{{$injured_employee->name}}</span> 
                    </div>
                    <div class="rightSide">
                        <span class="employeeStatus">status: {{($injury->resolved) ? 'Claim Closed' : $injury->employee_status ?? 'none set'}}</span>
                    </div>                    
                </div>
				<div class="zenTabs">
					<ul class="sevenTabs">
						<li class="overview" data-target="overviewTab"><a class="tabOverview"><div class="icon icon-profile"></div><span>employee</span></a></li>
						<li class="zenguide" data-target="zenguideTab"><a class="tabZenguide"><div class="icon icon-map-signs"></div><span>goals</span></a></li>
						<li class="communicate" data-target="communicateTab"><a class="tabCommunicate"><div class="icon icon-comments"></div><span>communicate</span></a></li>
						<li class="team" data-target="teamTab"><a class="tabTeam"><div class="icon icon-teams"></div><span>team</span></a></li>
						<li class="claimcosts" data-target="claimcostsTab"><a class="tabClaimcosts"><div class="icon icon-claimcosts"></div><span>claim costs</span></a></li>
						<li class="appointments" data-target="appointmentsTab"><a class="tabAppointments"><div class="icon icon-calendar-o"></div><span>appointments</span></a></li>
						<li class="files" data-target="filesTab"><a class="tabFiles"><div class="icon icon-paperclip"></div><span>files</span></a></li>
					</ul>
				</div>

				<div class="zenTabContent overviewTab" id="">
					@include('zencarePartials.employee')
				</div>

				<div class="zenTabContent zenguideTab" id="" style="width:80%;margin:0 auto;">
					@include('zencarePartials.zenguide')
				</div>

				<div class="zenTabContent communicateTab" id="">
					@include('zencarePartials.communicate')
				</div>

				<div class="zenTabContent teamTab" id="">
					@include('zencarePartials.team')
				</div>

				<div class="zenTabContent claimcostsTab" id="">
					@include('zencarePartials.claimcosts')
				</div>

				<div class="zenTabContent appointmentsTab" id="">
					@include('zencarePartials.appointments')
				</div>

				<div class="zenTabContent filesTab" id="">
					@include('zencarePartials.files')
				</div>

			</div>
		</div>
	</div>
</div>

    @include('zencarePartials.modals')

    <script src="{{ URL::asset('/js/moment.js') }}"></script>

    <script src="{{ URL::asset('/js/combodate.js') }}"></script>

    <script>
        var claimCostsHistoryTriggered = false;
        var line_chart = null;
        var modal;

        $(document).ready(function(){
            $('.statusEffectiveDate').combodate({
                firstItem: 'none',
                maxYear: (moment().year()),
                format: "MM-DD-YYYY",
                template: "D MMM YYYY",
                value: moment(),
                smartDays: true,
                yearAscending: false,
            });

            //$('#taskListStatusDate').combodate('setValue', moment().now());
        });

        var numberGoalsLeftLoad = 9;
        var openGoalAlert = 0;
        var openGoalReport = 0;
        var openGoalClaimNumber = 0;
        var openGoalClaimCosts = 0;
        var openGoalStatus = 0;
        var openGoalResolved = 0;
        var openGoalEmail = 0;
        var openGoalAdjuster = 0;
        var openGoalFinalCost = 0;

        @if(!empty($injury_alert->action_taken))
            numberGoalsLeftLoad--;
        @else
            openGoalAlert = 1;
        @endif
        @if(!empty($injury->full_injury_report_delivered))
            numberGoalsLeftLoad--;
        @else
            openGoalReport = 1;
        @endif
        @if(!empty($injury->claim_number))
            numberGoalsLeftLoad--;
        @else
            openGoalClaimNumber = 1;
        @endif
        @if(!empty($injury->adjuster_name))
            numberGoalsLeftLoad--;
        @else
            openGoalAdjuster = 1;
        @endif
        @if(!empty($injured_employee->email))
            numberGoalsLeftLoad--;
        @else
            openGoalEmail = 1;
        @endif
        @if($injury->resolved !== 0)
            numberGoalsLeftLoad--;
        @else
            openGoalResolved = 1;
        @endif
        @if(!empty($injury->employee_status))
            numberGoalsLeftLoad--;
        @else
            openGoalStatus = 1;
        @endif
        @if(!empty($injury->final_cost))
            numberGoalsLeftLoad--;
        @else
            openGoalFinalCost = 1;
        @endif
        @if(!empty($medical_total) || !empty($legal_total) || !empty($indemnity_total) || !empty($misc_total))
            numberGoalsLeftLoad--;
        @else
            openGoalClaimCosts = 1;
        @endif

        var claim_litigated = false;

        //TREE POSTS

        $('#submitTreePost').on('click', function(){
            //var body = document.getElementById("treePostTextArea").innerHTML;
            var body = $('#treePostInput').val();
            var error_div = $('#treePostModal').find('.inputError');
            error_div.removeClass('show');
            //addTreePost(body);
            console.log(body);
            var length = getPostLength(body);
            console.log(body);
            console.log('length = ' + length);
            if(length >= 1000){
                error_div.html("sorry, your post is too long").addClass('show');
            }else if(body === "") {
                error_div.html("nothing to submit!").addClass('show');
                showAlert("Nothing to submit!", "deny", 5);
            }else{
                newTreePost(body);
                modal.close();
                $('#treePostInput').val("");
                getPostLength("");
            }
        });

        function getPostLength(string){
            var replaced_post = string.replace(/&nbsp;/g, " ");
            console.log(replaced_post);
            var length = replaced_post.length;
            //alert(length);
            //console.log(replaced_post);
            //document.getElementById('charCount').innerHTML = length;
            if(length >= 1000){
                //return false;
                /*
                document.getElementById('charDiv').style.color = 'red';
                document.getElementById('treePostErrors').innerHTML = "Sorry, your post is too long!";
                document.getElementById('treePostErrors').style.color = "red";
                */
            }else{
                //return true;
                /*
                document.getElementById('charDiv').style.color = 'white';
                document.getElementById('treePostErrors').innerHTML = "Click an emoji to add it to your post. You may click emojis already in the post to remove them.";
                document.getElementById('treePostErrors').style.color = "white";
                */
            }
            return length;
        }

        function newTreePost(body){
            var _token = '<?php echo csrf_token(); ?>';
            var injury_id = '<?php echo $injury->id; ?>';
            var user_id = '<?php echo Auth::user()->id ?>';
            var body = body;
            $.ajax({
                type: 'POST',
                url: '<?php echo route("newTreePost") ?>',
                data: {
                    injury_id: injury_id,
                    user_id: user_id,
                    body: body,
                    _token: _token
                },
                success: function (data) {
                    getTreePosts();
                    //document.getElementById("treePostTextArea").value = "";
                    $('#treePostInput').val('');
                }
            })
        }

        //TODO:UPDATE CLAIM COSTS

        function cleanClaimValueInput(input){
            input = input.replace(/\s+/g, '');
            input = input.replace(/[^0-9.]/g, "");
            input = parseFloat(input);
            if(input === ""){
                input = 0;
            }
            return input;
        }


        $('#submitClaimCostModal').on('click', function(){
            updateClaimCosts();
        });

        function updateClaimCosts(){
            @if($injury->paused_for_litigation !== 1)

            var paid_medical = cleanClaimValueInput($('#paidMedicalInput').val());
            var paid_indemnity = cleanClaimValueInput($('#paidIndemnityInput').val());
            var paid_legal = cleanClaimValueInput($('#paidLegalInput').val());
            var paid_misc = cleanClaimValueInput($('#paidMiscInput').val());
            var reserve_medical = cleanClaimValueInput($('#reserveMedicalInput').val());
            var reserve_indemnity = cleanClaimValueInput($('#reserveIndemnityInput').val());
            var reserve_legal = cleanClaimValueInput($('#reserveLegalInput').val());
            var reserve_misc = cleanClaimValueInput($('#reserveMiscInput').val());

            if(isNaN(paid_medical) || isNaN(paid_indemnity) || isNaN(paid_legal) || isNaN(paid_misc) ||
                isNaN(reserve_medical) || isNaN(reserve_indemnity) || isNaN(reserve_legal)|| isNaN(reserve_misc)){
                showAlert("Please make sure you only entered numerical values!", "deny", 5);
            }
            $.ajax({
                type: 'POST',
                url: '<?php echo route('updateClaimCosts'); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: '<?php echo $injury->id; ?>',
                    user_id: '<?php echo Auth::user()->id; ?>',
                    paid_medical: paid_medical,
                    paid_indemnity: paid_indemnity,
                    paid_legal: paid_legal,
                    paid_misc: paid_misc,
                    reserve_medical: reserve_medical,
                    reserve_indemnity: reserve_indemnity,
                    reserve_legal: reserve_legal,
                    reserve_misc: reserve_misc

                },
                success: function(data){
                    showAlert("Thanks for updating the claim costs!", "confirm", 5);
                    if(openGoalClaimCosts == 1){
                        checkIfAllGoalsAreComplete();
                        $('#taskListClaimCosts').hide();
                        $('#spacerClaimCosts').hide();
                        $('#taskListClaimCostsRepeat').show();
                        openGoalClaimCosts = 2;
                    }
                    modal.close();
                    var medicalTotal = paid_medical + reserve_medical;
                    var indemnityTotal = paid_indemnity + reserve_indemnity;
                    var legalTotal = paid_legal + reserve_legal;
                    var miscTotal = paid_misc + reserve_misc;

                    updateClaimCostsBarHeight(medicalTotal, reserve_medical, "medical");
                    updateClaimCostsBarHeight(indemnityTotal, reserve_indemnity, "indemnity");
                    updateClaimCostsBarHeight(legalTotal, reserve_legal, "legal");
                    updateClaimCostsBarHeight(miscTotal, reserve_misc, "misc");

                    $('#medicalPaid').html("$" + paid_medical);
                    $('#indemnityPaid').html("$" + paid_indemnity);
                    $('#legalPaid').html("$" + paid_legal);
                    $('#miscPaid').html("$" + paid_misc);
                    $('#medicalReserve').html("$" + reserve_medical);
                    $('#indemnityReserve').html("$" + reserve_indemnity);
                    $('#legalReserve').html("$" + reserve_legal);
                    $('#miscReserve').html("$" + reserve_misc);


                    var paid_total = paid_medical + paid_indemnity + paid_legal + paid_misc;
                    var reserve_total = reserve_medical + reserve_indemnity + reserve_legal + reserve_misc;
                    var totalPlusReserve = paid_total + reserve_total;

                    $('#claimCostsPaidTotal').html("$" + paid_total);
                    $('#claimCostsReserveTotal').html("$" + reserve_total);
                    $('#claimCostsPaidPlusReserveTotal').html("$" + totalPlusReserve);


                    $('#finalCostText').html('Final <span class="FG_green">TOTAL</span>: $' + paid_total + ' | Final <span class="FG_red">RESERVE</span>: $' + reserve_total);
                    $('#finalCostTotalText').html('Final <span class="FG_green">TOTAL</span> + <span class="FG_red">RESERVE</span>: $' + totalPlusReserve);
                    $('#finalInjuryCost').val("$" + totalPlusReserve);

                    getTreePosts();


                },
                error: function(data){
                    console.log(data);
                }
            })
            @else
                showAlert("Sorry, this claim is under litigation.", "warning", 3);
            @endif

        }

        //calculate bar height %
        function updateClaimCostsBarHeight(total, reserve, bar){
            console.log("total: " + total);
            console.log("bar: " + bar);
            console.log("reserve: " + reserve);
            var percent = (reserve / total) * 100;
            $('#' + bar + "Bar").css("height", percent + "%");
        }

        //UPDATE EMPLOYEE STATUS

        //NEW MODAL
        $('#zencareModal').on('click', '#submitStatusUpdate', function(){
            @if($injury->paused_for_litigation !== 1)
                    // var status = $('.employeeStatusButton.selected').data('status');
                    //var status = $('.statusSelector')
                    updateEmployeeStatus();
            @else
                showAlert("Sorry, this claim is under litigation.", "warning", 3);
            @endif

        });

        function updateEmployeeStatus(){
            var dateStatus = document.getElementById("status-date").value;
            var buttons = document.querySelectorAll("button");
            for(var i = 0; i < buttons.length; i++){
                if(buttons[i].name == "blah" && buttons[i].clicked == true){
                    var saveStatus = "";
                    var stateNumberUpdate;
                    if(buttons[i].id == "hospitalButton"){
                        saveStatus = "Hospital";
                        stateNumberUpdate = 1;
                    }
                    else if(buttons[i].id == "urgentcareButton"){
                        saveStatus = "Urgent Care";
                        stateNumberUpdate = 1;
                    }
                    else if(buttons[i].id == "doctorButton"){
                        saveStatus = "Family Practice";
                        stateNumberUpdate = 1;
                    }
                    else if(buttons[i].id == "recoverhomeButton"){
                        saveStatus = "Home";
                        stateNumberUpdate = 3;
                    }
                    else if(buttons[i].id == "lightdutyButton"){
                        saveStatus = "Light Duty";
                        stateNumberUpdate = 4;
                    }
                    else if(buttons[i].id == "fulldutyButton"){
                        saveStatus = "Full Duty";
                        stateNumberUpdate = 5;
                    }
                    else if(buttons[i].id == "maxrecoveryButton"){
                        saveStatus = "Maximum Medical Improvement";
                        stateNumberUpdate = 7;
                    }
                    else if(buttons[i].id == "terminatedButton"){
                        saveStatus = "Terminated";
                        stateNumberUpdate = 0;
                    }
                    else if(buttons[i].id == "resignedButton"){
                        saveStatus = "Resigned";
                        stateNumberUpdate = 0;
                    }
                    else if(buttons[i].id == "deathButton"){
                        saveStatus = "Death";
                        stateNumberUpdate = 0;
                    }

                    $.ajax({
                        type: 'POST',
                        url: '<?php echo route("updateEmployeeStatus"); ?>',
                        data: {
                            _token: '<?php echo csrf_token(); ?>',
                            injury_id: {{ $injury->id }},
                            status: saveStatus,
                            date: dateStatus,
                        }, success: function(data){
                                showAlert("Thanks for updating the status!", "confirm", 5);
                                if(openGoalStatus == 1){
                                    checkIfAllGoalsAreComplete();
                                    $('#taskListEmployeeStatus').hide();
                                    $('#spacerEmployeeStatus').hide();
                                    $('#taskListEmployeeStatusRepeat').show();
                                    openGoalStatus = 2;
                                }
                                var statusCircle = document.getElementById("statusProgressCircle");
                                statusCircle.className = "progressMeter status" + stateNumberUpdate;
                                modal.close();
                                getTreePosts();
                                displayProgressBar(saveStatus);
                        },  error: function(data){
                                showAlert("Sorry, there was a problem updating status. Please try again later.", "deny", 5);
                        }
                    });
                }
            }
        }


        //pause for litigation
        //NEW MODAL
        $('#zencareModal').on('click', '#claimStopButton1', function(){
            $('.litigationDiv').hide();
            $('#litigationDiv2').show();
        });

        $('#zencareModal').on('click', '#claimStopButton2', function(){
            $('.litigationDiv').hide();
            $('#litigationDiv3').show();
        });

        $('#zencareModal').on('click', '#confirmClaimStopButton', function(){
            pauseForLitigation();
        });

        function pauseForLitigation(){
            $.ajax({
                type: 'POST',
                url: '<?php echo route('pauseClaimForLitigation'); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: '<?php echo $injury->id; ?>',
                },
                success: function(data){
                    window.location.reload();
                },
                error: function(data){
                    alert('error');
                }
            });
        }


        //APPOINTMENTS
        $('#submitSummaryModal').on('click', function(){
            var appointment_id = $('#addSummaryModalAppointmentID').val();
            var summary = $('#addSummaryModal').find('textarea').val();

            addAppointmentSummary(appointment_id, summary);

        })

        $('#setApptBtn').on('click', function(){
            // if(button.hasClass('green') && target === "#addAppointmentsModal"){
            //     ('#setFollowTitle').hide();
            //     ('#newAppointmentTitle').show();
            // }
            var name = $('#addAppointmentName').val();

            if(name === ""){
                showAlert("The name field is required!", "deny", 10);
                return false;
            }

            var email = $('#addAppointmentEmail').val();
            var phone = $('#addAppointmentPhone').val();
            console.log("phone: " + phone);

            /*
                if(email === "" && phone === ""){
                    showAlert("You must enter either an email address or a phone number", "deny", 10);
                    return false;
                }
            */
             //var datetime = datepickerDefault.format("Y-MM-DD hh:mm A");
             //console.log("datetime: " + datetime);
            var datetime = $('#datetime').val();
            console.log("datetime: " + datetime);
            var description = $('#addAppointmentNote').val();
            /*
            if(description === ""){
                showAlert("Please enter a brief description of the appointment (X-Rays, Consultation, etc)", "deny", 10);
                return false;
            }
            */


            addAppointment(name, email, phone, datetime, description);

        });

        $('#setFollowBtn').on('click', function(){
            // if(button.hasClass('blue') && target === "#addAppointmentsModal"){
            //     ('#setFollowTitle').show();
            //     ('#newAppointmentTitle').hide();
            // }
            var name = $('#addAppointmentName').val();
            if(name === ""){
                showAlert("The name field is required!", "deny", 10);
                return false;
            }

            var email = $('#addAppointmentEmail').val();
            var phone = $('#addAppointmentPhone').val();

            /*
                if(email === "" && phone === ""){
                    showAlert("You must enter either an email address or a phone number", "deny", 10);
                    return false;
                }
            */
            var datetime = $('#datetime').val();
            var description = $('#addAppointmentNote').val();
            /*
            if(description === ""){
                showAlert("Please enter a brief description of the appointment (X-Rays, Consultation, etc)", "deny", 10);
                return false;
            }
            */
            //console.log(datetime);

            addAppointment(name, email, phone, datetime, description);

        });

        function addAppointmentSummary(appointment_id, summary){
            $.ajax({
                type: 'POST',
                url: '<?php echo route('addAppointmentSummary'); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    appointment_id: appointment_id,
                    summary: summary
                },
                success: function(data){
                    $(".apptCard[data-appointmentid='" + appointment_id + "']").find(".apptSummary").html(summary);
                    modal.close();
                    showAlert("Thanks!", "confirm", 5);
                },
                error: function(data){
                    showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
                }

            });
        }

        function addAppointment(name, email, phone, datetime, description){
            $.ajax({
                type: 'POST',
                url: '<?php echo route('addAppointment'); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: '<?php echo $injury->id; ?>',
                    name: name,
                    email: email,
                    phone: phone,
                    datetime: datetime,
                    description: description,
                    user_id: $('#userID').val()
                },
                success: function (data) {
                    window.location.href = "/zencare/" + '<?php echo $injury->id; ?>' + "/appointments";
                    getTreePosts();
                },
                error: function (data){
                    showAlert('Sorry, something went wrong. Please try again later', 'deny', 5);
                }
            })
        }

        function appendNewAppointment(appointment){
            if(appointment['summary'] == null){
                appointment['summary'] = "";
            }

            var html = '<div class="apptCard" data-appointmentid="' + appointment['id'] +'">' +
                '<div class="apptHeader">' +
                '<div class="apptIcon"><div class="icon inline icon-clock"></div></div>' +
                '<div class="apptTime">' + appointment['formatted_date'] + '</div>' +
                '</div>' +
                '<input type="hidden" class="apptTimeFormatted" value="' + appointment['appointment_time'] + '">' +
                '<div class="apptBody">' +
                '<div class="apptDoctor">' + appointment['name'] + '</div>' +
                '<div class="apptPhone">' + appointment['phone'] + '</div>' +
                '<div class="apptEmail">' +
                '<a href="mailto:' + appointment['email'] +'">' +
                appointment['email'] + '</a></div>' +
                '<div class="apptDescription">' + appointment['description'] + '</div>' +
                '<div class="apptSummary">' + appointment['summary'] + '</div>' +
                '<div class="apptControls">' +
                '<button data-appointmentid="' + appointment['id'] + '" class="cyan addSummaryModal">+ summary</button>' +
                '<button data-appointmentid="' + appointment['id'] + '" class="blue addAppointmentModal" data-apptType="followup">+ follow-up</button>' +
                '</div>' +
                '<div class="apptEdit">' +
                '<button data-appointmentid="' + appointment['id'] + '" class="gold editAppointmentModal"><div class="icon inline icon-pencil"></div> edit</button></div>' +
                '</div>' +
                '</div>';

            //var existing_html = $('.myAppointments.hasList').html();

            if(appointment['appointment_status'] === "future"){

                $('.myAppointments.hasList').append(html);
                $('.myAppointments.noList').hide();
                $('.myAppointments.hasList').show();

            }else{
                $('.pastAppointments.hasList').append(html);
                $('.pastAppointments.noList').hide();
                $('.pastAppointments.hasList').show();
            }
        }

        function clearAppointmentModal(){
            $('#addAppointmentModal').find('input').val("");
            $('#addAppointmentNote').val("");

        }

        $('#editApptBtnSave').on('click', function(){
            var name = $('#editAppointmentName').val();
            console.log("appointment name" + name);

            if(name === ""){
                showAlert("The name field is required!", "deny", 10);
                return false;
            }

            var email = $('#editAppointmentEmail').val();
            var phone = $('#editAppointmentPhone').val();
            var datetime = $('#editdatetime').val();
            console.log("edit datetime: " + datetime)
            var description = $('#editAppointmentNote').val();
            var appointment_id = $('#editAppointmentID').val();
            //var appointment_id = $(this).data('appointmentid');

            $.ajax({
                type: 'POST',
                url: '<?php echo route('editAppointment'); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: {{$injury_id}},
                    appointment_id: appointment_id,
                    name: name,
                    email: email,
                    phone: phone,
                    datetime: datetime,
                    description: description
                },
                success: function(data){
                    // modal.close();
                    // var div = $('.apptCard[data-appointmentid="' + appointment_id +'"]');
                    // div.find('.apptDoctor').html(name);
                    // div.find('.apptPhone').html(phone);
                    // div.find('.apptEmail').html("<a href='mailto:" + email +"'>" + email + "</a>");
                    // div.find('.apptDescription').html(description);

                    window.location.href = "/zencare/" + '<?php echo $injury->id; ?>' + "/appointments";
                },

                error: function($xhr){
                    var data = $xhr.responseJSON;
                    console.log(data);
                    showAlert('Sorry, something went wrong. Please try again later', 'deny', 5);
                }
            })

        });

        $('#submitClaimNumber').on('click', function(){
            @if($injury->paused_for_litigation !== 1)
                var claimNumber = $("#claimNumberInput").val();
                var claimNumberConfirm = $("#confirmClaimNumberInput").val();

                if(claimNumber !== claimNumberConfirm){
                    alert('numbers do not match');
                    return false;
                }
                if(claimNumber != ""){
                $.ajax({
				type: 'POST',
				url: '<?php echo route('addClaimNumber'); ?>',
				data: {
				    _token: '<?php echo csrf_token(); ?>',
					claim_number: claimNumber,
                    claim_number_confirm: claimNumberConfirm,
                    injury_id: {{ $injury->id }}
				},
				success: function(data){
                    showAlert("Thanks for add the claim number!", "confirm", 5);
                    if(openGoalClaimNumber == 1){
                        checkIfAllGoalsAreComplete();
                        openGoalClaimNumber = 2;
                    }
                    var updateClaimNumberText = document.getElementById("updateClaimNumberInput");
                    updateClaimNumberText.value = claimNumber;
                    var updateConfirmClaimNumberText = document.getElementById("updateConfirmClaimNumberInput");
                    updateConfirmClaimNumberText.value = claimNumberConfirm;
                    $('#taskListClaimNumber').hide();
                    $('#spacerClaimNumber').hide();
                    $('#claimNumberAdded').find('.longlabel').html('You added the claim number - ' + claimNumber);
                    $('#claimNumberAdded').show();
                    $('#taskListInjuryAlert').hide();
                    $('#spacerInjuryAlert').hide();
                    $('#injuryAlertDelivered').show();
                    $('.injuryData.claimnumber').html(claimNumber);
                    getTreePosts();
				    modal.close();

				}
			    });
            }
            @else
                showAlert("Sorry, this claim is under litigation.", "warning", 3);
            @endif

        });

        $('#submitTheInjuryAlert').on('click', function(){
            @if($injury->paused_for_litigation !== 1)
                $.ajax({
                type:'POST',
                url: '<?php echo route('deliverInjuryAlert') ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: {{ $injury->id }}
                },
                success: function(data){
                    showAlert("Thanks delivering the injury alert!", "confirm", 5);
                    if(openGoalAlert == 1){
                        checkIfAllGoalsAreComplete();
                        $('#taskListInjuryAlert').hide();
                        $('#spacerInjuryAlert').hide();
                        $('#injuryAlertDelivered').show();
                        openGoalAlert = 2;
                    }
                    getTreePosts();
                    modal.close();
                },
                error: function($xhr){
                    var data = $xhr.responseJSON;
                    console.log(data);
                    showAlert('Sorry, something went wrong. Please try again later', 'deny', 5);
                }
            });
            @else
                showAlert("Sorry, this claim is under litigation.", "warning", 3);
            @endif
        });

        $('#cancelTheInjuryAlert').on('click', function(){
            modal.close();
        });

        $('#submitTheFirstReport').on('click', function(){
            @if($injury->paused_for_litigation !== 1)
                $.ajax({
                type:'POST',
                url: '<?php echo route('firstOfficialReportDelivered') ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: {{ $injury->id }}
                },
                success: function(data){
                    showAlert("Thanks delivering the Official First Report!", "confirm", 5);
                    if(openGoalReport == 1){
                        checkIfAllGoalsAreComplete();
                        $('#taskListOfficialReport').hide();
                        $('#spacerOfficialReport').hide();
                        $('#fullInjuryReportDelivered').show();
                        openGoalReport = 2;
                    }
                    getTreePosts();
                    modal.close();
                },
                error: function($xhr){
                    var data = $xhr.responseJSON;
                    console.log(data);
                    showAlert('Sorry, something went wrong. Please try again later', 'deny', 5);
                }

            });
            @else
                showAlert("Sorry, this claim is under litigation.", "warning", 3);
            @endif

        });

        $('#cancelTheFirstReport').on('click', function(){
            modal.close();
        });

        $('#submitInjuryResolved').on('click', function(){
            @if($injury->paused_for_litigation !== 1)
                var resolveDateInput = $('#resolveDate').val();
                if(resolveDateInput != ""){
                    $.ajax({
                        type:'POST',
                        url: '<?php echo route('resolveInjury') ?>',
                        data: {
                            _token: '<?php echo csrf_token(); ?>',
                            injury_id: {{ $injury->id }},
                            date: resolveDateInput,
                        },
                        success: function(data){
                            showAlert("Thanks for resolving the Injury!", "confirm", 5);
                            if(openGoalResolved == 1){
                                checkIfAllGoalsAreComplete();
                                $('#taskListResolve').hide();
                                $('#spacerInjuryResolved').hide();
                                $('#injuryResolved').show();
                                openGoalResolved = 2;
                            }
                            getTreePosts();
                            modal.close();
                        },
                        error: function($xhr){
                            var data = $xhr.responseJSON;
                            console.log(data);
                            showAlert('Sorry, something went wrong. Please try again later', 'deny', 5);
                        }

                    });
                }
                else{
                    showAlert('Please select the claim close date.', 'deny', 5);
                }

            @else
                showAlert("Sorry, this claim is under litigation.", "warning", 3);
            @endif

        });


        $('#submitInjuryResolvednoDate').on('click', function(){
            @if($injury->paused_for_litigation !== 1)
                $.ajax({
                    type:'POST',
                    url: '<?php echo route('resolveInjury') ?>',
                    data: {
                        _token: '<?php echo csrf_token(); ?>',
                        injury_id: {{ $injury->id }},
                    },
                    success: function(data){
                        showAlert("Thanks resolving the Injury!", "confirm", 5);
                        if(openGoalResolved == 1){
                            checkIfAllGoalsAreComplete();
                            $('#taskListResolve').hide();
                            $('#spacerInjuryResolved').hide();
                            $('#injuryResolved').show();
                            openGoalResolved = 2;
                        }
                        getTreePosts();
                        modal.close();
                    },
                    error: function($xhr){
                        var data = $xhr.responseJSON;
                        console.log(data);
                        showAlert('Sorry, something went wrong. Please try again later', 'deny', 5);
                    }

                });

            @else
                showAlert("Sorry, this claim is under litigation.", "warning", 3);
            @endif
        });


        $('#submitInjuryResolvedNewDate').on('click', function(){
            @if($injury->paused_for_litigation !== 1)
                var resolveDateInput = $('#resolveDateNewDate').val();
                if(resolveDateInput != ""){
                    $.ajax({
                        type:'POST',
                        url: '<?php echo route('resolveInjury') ?>',
                        data: {
                            _token: '<?php echo csrf_token(); ?>',
                            injury_id: {{ $injury->id }},
                            date: resolveDateInput,
                        },
                        success: function(data){
                            showAlert("Thanks for resolving the Injury!", "confirm", 5);
                            //var date1 = document.getElementById("resolvedOnDate1");
                            //var date2 = document.getElementById("resolvedOnDate2");
                            //date1.innerText = resolveDateInput;
                            //date2.innerText = "resolved on " +  resolveDateInput;
                            getTreePosts();
                            modal.close();
                        },
                        error: function($xhr){
                            var data = $xhr.responseJSON;
                            console.log(data);
                            showAlert('Sorry, something went wrong. Please try again later', 'deny', 5);
                        }

                    });
                }
                else{
                    showAlert('Please select the claim close date.', 'deny', 5);
                }

            @else
                showAlert("Sorry, this claim is under litigation.", "warning", 3);
            @endif
        });

    $('#editClaimStatusReopen').on('click', function(){

        @if($injury->paused_for_litigation !== 1)

        $.ajax({
            type: 'POST',
            url: '<?php echo route('reopenClaimStatus') ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                injury_id: {{ $injury->id }},
            },
            success: function(data){
                showAlert("Claim has been reopened!", "confirm", 5);
                modal.close();
                setTimeout(() => window.location.reload(), 2000);
            },
            error: function(data){
                showAlert("Sorry, there was a problem updating this claim!", "deny", 5);
            }
        })
        @else
            showAlert("Sorry, this claim is under litigation.", "warning", 3);
        @endif
    });


    $('#submitFinalCost').on('click', function(){
        @if($injury->paused_for_litigation !== 1)
        var finalInjuryCostInput = $('#finalInjuryCost').val();
        var finalInjuryCostNum = finalInjuryCostInput.replace("$", "");
        var hasInjuryBeenResolved = <?php echo json_encode($injury->resolved); ?>;

        if(hasInjuryBeenResolved !== 0 || $('#injuryResolved').is(':visible')){
            $.ajax({
                type: 'POST',
                url: '<?php echo route("updateFinalCost"); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: {{ $injury->id }},
                    final_cost: finalInjuryCostNum,
                    medical_cost: {{$medical_total}},
                    legal_cost: {{$legal_total}},
                    indemnity_cost: {{$indemnity_total}},
                    misc_cost: {{$misc_total}},
                }, success: function(data){
                        showAlert("Thanks for updating final cost!", "confirm", 5);
                        if(openGoalFinalCost == 1){
                            checkIfAllGoalsAreComplete();
                            $('#taskListFinalCost').hide();
                            $('#finalCostsUpdated').show();
                            openGoalFinalCost = 2;
                        }
                        getTreePosts();
                        modal.close();
                },  error: function(data){
                        showAlert("Sorry, there was a problem. Please try again later.", "deny", 5);
                }
            });
        }
        else{
            showAlert("Please resolve injury before final costs.", "deny", 5);
        }

        @else
            showAlert("Sorry, this claim is under litigation.", "warning", 3);
        @endif

    });
    $('#submitAdjusterInfo').on('click', function(){
        @if($injury->paused_for_litigation !== 1)
            var submitAdjusterName = $('#adjusterNameInput').val();
            var submitAdjusterEmail = $('#adjusterEmailInput').val();
            var submitAdjusterPhone = $('#adjusterPhoneInput').val();

            if(submitAdjusterName != "" && submitAdjusterEmail != "" && submitAdjusterPhone.length >= 14){
                $.ajax({
                        type: 'POST',
                        url: '<?php echo route("updateAdjuster"); ?>',
                        data: {
                            _token: '<?php echo csrf_token(); ?>',
                            injury_id: {{ $injury->id }},
                            adjuster_name: submitAdjusterName,
                            adjuster_email: submitAdjusterEmail,
                            adjuster_phone: submitAdjusterPhone,
                        }, success: function(data){
                                showAlert("Thanks for updating adjuster!", "confirm", 5);
                                if(openGoalAdjuster == 1){
                                    checkIfAllGoalsAreComplete();
                                    $('#taskListAdjuster').hide();
                                    $('#spacerAdjuster').hide();
                                    $('#adjusterInfoCompleted').show();
                                    openGoalAdjuster = 2;
                                }
                                var adjusterName = document.getElementById("adjustername2");
                                var adjusterEmail = document.getElementById("adjusteremail2");
                                var adjusterPhone = document.getElementById("adjusterphone2");
                                adjusterName.innerText = submitAdjusterName;
                                adjusterEmail.innerText = submitAdjusterEmail;
                                adjusterPhone.innerText = submitAdjusterPhone;
                                getTreePosts();
                                modal.close();
                        },  error: function(data){
                                showAlert("Sorry, there was a problem. Please try again later.", "deny", 5);
                        }
                    });
                }

        @else
            showAlert("Sorry, this claim is under litigation.", "warning", 3);
        @endif
        });

        $('#inviteToZengarden').on('click', function(){
            var email = $('#inviteZengardenEmail').val();

            //if no email is input, check the hidden input
            if(email === "" || email === null || email === undefined){
                email = $('#nonInputEmail').text();
            }
            if(validateEmail(email)){
                sendEmployeeEmail(email);
            }

        });

        function validateEmail(email){
            var regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            return regex.test(email);
        }

        $('#submitEmployeeEmail').on('click', function(){
            @if($injury->paused_for_litigation !== 1)
            var employeeName = $("#employeeNameInput").val();
		    var employeeEmail = $("#employeeEmailInput").val();
            var employeePhone = $("#employeePhoneInput").val();
            var employeeNameStored = document.getElementById("overviewEmployeeName");
            var employeeEmailStored =  document.getElementById("overviewEmployeeEmail");
            var employeePhoneStored = document.getElementById("overviewEmployeePhone");
            var employeeClaimNumber = $("#updateClaimNumberInput").val();
            var employeeConfirmClaimNumber = $("#updateConfirmClaimNumberInput").val();
            var canSubmitInfo = 0;

            var savedName = <?php echo json_encode($injured_employee->name); ?>;
            var savedEmail = <?php echo json_encode($injured_employee->email); ?>;
            var savedPhone = <?php echo json_encode($injured_employee->phone); ?>;

            if(employeeNameStored.innerText != employeeName && employeeName != savedName){
                canSubmitInfo++;
            }
            if(employeeEmailStored.innerText != employeeEmail && employeeEmail != "" && employeeEmail != savedEmail){
                canSubmitInfo++;
            }
            if(employeePhoneStored.innerText != employeePhone && employeePhone != "" && employeePhone.length >= 14 && employeePhone != savedPhone){
                canSubmitInfo++;
            }

            if(employeePhone.length < 14 && employeePhone != "" && employeePhoneStored.innerText == ""){
                employeePhone = "";
            }
            else if(employeePhone.length < 14 && employeePhone != "" && employeePhoneStored.innerText != ""){
                employeePhone = employeePhoneStored.innerText;
            }

            if(canSubmitInfo >= 1){
                $.ajax({
                type: 'POST',
                url: '<?php echo route("updateInjuredEmail"); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    injury_id: {{$injury->id}},
                    employee_id: {{$injured_employee->id}},
                    employee_name: employeeName,
                    employee_email: employeeEmail,
                    employee_phone: employeePhone,
                },
                success: function(data){
                        showAlert("Thanks for updating employee info!", "confirm", 5);
                        employeeNameStored.innerText = employeeName;
                        if(employeeEmail != ""){
                            if(openGoalEmail == 1){
                                checkIfAllGoalsAreComplete();
                                $('#taskListEmployeeEmail').hide();
                                $('#spacerEmployeeEmail').hide();
                                $('#employeeInfoCompleted').show();
                                openGoalEmail = 2;
                            }
                            employeeEmailStored.innerText = employeeEmail;
                        }
                        if(employeePhone != ""){
                            employeePhoneStored.innerText = employeePhone;
                        }
                        getTreePosts();
                        modal.close();

                },  error: function(data){
                        showAlert("Sorry, there was a problem. Please try again later.", "deny", 5);
                }
                });
            }

            var submitAdjusterName = $('#adjusterNameInput2').val();
            var submitAdjusterEmail = $('#adjusterEmailInput2').val();
            var submitAdjusterPhone = $('#adjusterPhoneInput2').val();
            var savedAdjusterName = document.getElementById("adjustername2");
            var savedAdjusterEmail = document.getElementById("adjusteremail2");
            var savedAdjusterPhone = document.getElementById("adjusterphone2");

            if(submitAdjusterName != "" && submitAdjusterEmail != "" && submitAdjusterPhone.length >= 14 && 
            (submitAdjusterName != savedAdjusterName.innerText || submitAdjusterEmail != savedAdjusterEmail.innerText || submitAdjusterPhone != savedAdjusterPhone.innerText)){
                $.ajax({
                        type: 'POST',
                        url: '<?php echo route("updateAdjuster"); ?>',
                        data: {
                            _token: '<?php echo csrf_token(); ?>',
                            injury_id: {{ $injury->id }},
                            adjuster_name: submitAdjusterName,
                            adjuster_email: submitAdjusterEmail,
                            adjuster_phone: submitAdjusterPhone,
                        }, success: function(data){
                                showAlert("Thanks for updating adjuster!", "confirm", 5);
                                if(openGoalAdjuster == 1){
                                    checkIfAllGoalsAreComplete();
                                    $('#taskListAdjuster').hide();
                                    $('#spacerAdjuster').hide();
                                    $('#adjusterInfoCompleted').show();
                                    openGoalAdjuster = 2;
                                }
                                var adjusterName = document.getElementById("adjustername2");
                                var adjusterEmail = document.getElementById("adjusteremail2");
                                var adjusterPhone = document.getElementById("adjusterphone2");
                                adjusterName.innerText = submitAdjusterName;
                                adjusterEmail.innerText = submitAdjusterEmail;
                                adjusterPhone.innerText = submitAdjusterPhone;
                                getTreePosts();
                                modal.close();
                        },  error: function(data){
                                showAlert("Sorry, there was a problem. Please try again later.", "deny", 5);
                        }
                });
            }


            var notesVar = document.getElementById('textarea').value;
            var descriptionText = document.getElementById("descriptionOverviewText");

            if(notesVar != "" && descriptionText.innerText != notesVar){
                $.ajax({
                    type: 'POST',
                    url: '<?php echo route("updateInjuryDescription"); ?>',
                    data: {
                        _token: '<?php echo csrf_token(); ?>',
                        injury_id: {{ $injury->id }},
                        notes: notesVar,
                    }, success: function(data){
                            showAlert("Thanks for updating injury notes!", "confirm", 5);
                            descriptionText.innerText = notesVar;
                            var injuryDetailsNotesText = document.getElementById("injuryDetailsNotesText");
                            injuryDetailsNotesText.innerText = notesVar;
                            getTreePosts();
                            modal.close();
                    },  error: function(data){
                            showAlert("Sorry, there was a problem. Please try again later.", "deny", 5);
                    }
                });
            }

            var savedClaimNumber = <?php echo json_encode($injury->claim_number); ?>;
            var claimNumberText = document.getElementById("overviewClaimNumberText");

            if(employeeClaimNumber === employeeConfirmClaimNumber && employeeClaimNumber != "" && savedClaimNumber != employeeClaimNumber && employeeClaimNumber != claimNumberText.innerText){
                $.ajax({
				type: 'POST',
				url: '<?php echo route('addClaimNumber'); ?>',
				data: {
				    _token: '<?php echo csrf_token(); ?>',
					claim_number: employeeClaimNumber,
                    claim_number_confirm: employeeConfirmClaimNumber,
                    injury_id: {{ $injury->id }}
                    },
                    success: function(data){
                        showAlert("Thanks for add the claim number!", "confirm", 5);
                        if(openGoalClaimNumber == 1){
                            checkIfAllGoalsAreComplete();
                            openGoalClaimNumber = 2;
                        }
                        $('#taskListClaimNumber').hide();
                        $('#spacerClaimNumber').hide();
                        $('#claimNumberAdded').find('.longlabel').html('You added the claim number - ' + employeeClaimNumber);
                        $('#claimNumberAdded').show();
                        $('#taskListInjuryAlert').hide();
                        $('#spacerInjuryAlert').hide();
                        $('#injuryAlertDelivered').show();
                        $('.injuryData.claimnumber').html(employeeClaimNumber);
                        getTreePosts();
                        modal.close();

                    }
                });
            }

            @else
                showAlert("Sorry, this claim is under litigation.", "warning", 3);
            @endif

        });

        $('.claimcosts').on('click', function(){
            if(!claimCostsHistoryTriggered){
                claimCostsHistoryTriggered = true;
                $.ajax({
                    type:'POST',
                    url: '<?php echo route('getClaimCostHistory')?>',
                    data:{
                        _token: '<?php echo csrf_token(); ?>',
                        injury_id: {{ $injury->id }}
                    },
                    success:  function(data){
                        month_array = data['months'];
                        cost_history_array = data['history'];
                        createClaimHistoryChart(month_array, cost_history_array);
                    },
                    error: function(){
                        showAlert('Sorry, something went wrong getting claim cost history', 'deny', 5);
                    }
                });
            }else{
                console.log('not triggering');
            }
        });


        function checkIfAllGoalsAreComplete(){
            numberGoalsLeftLoad--;
            if(numberGoalsLeftLoad == 0){
                $("#goalsCompleteTextNotCompletedOnLoad").show();
            }

        }

        function createClaimHistoryChart(month_array, cost_history_array){
        //console.log(Object.values(month_array));
        month_array = Object.values(month_array);
        //console.log(month_array);
        //console.log(cost_history_array);
        if(cost_history_array['month_resolved'] !== false){
            for(var i = 0; i < month_array.length; i++){
                if(month_array[i] === cost_history_array['month_resolved']){
                    month_array[i]+= " - Claim Resolved";
                }
            }
        }
        var medical_data = Object.values(cost_history_array['medical']);
        var legal_data = Object.values(cost_history_array['legal']);
        var indemnity_data = Object.values(cost_history_array['indemnity']);
        var misc_data = Object.values(cost_history_array['misc']);
        var reserve_medical_data = Object.values(cost_history_array['reserve_medical']);
        var reserve_legal_data = Object.values(cost_history_array['reserve_legal']);
        var reserve_indemnity_data = Object.values(cost_history_array['reserve_indemnity']);
        var reserve_misc_data = Object.values(cost_history_array['reserve_misc']);

        var final_medical_data = Object.values(cost_history_array['final_medical']);
        var final_legal_data = Object.values(cost_history_array['final_legal']);
        var final_indemnity_data = Object.values(cost_history_array['final_indemnity']);
        var final_misc_data = Object.values(cost_history_array['final_misc']);


        if(line_chart !== null){
            line_chart.destroy();
        }

        var context = document.getElementById('myChart');

        var data = {
            labels: month_array,
            datasets: [
                {
                    label: "Medical",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgb(198,25,25)",
                    borderColor: "rgb(198,25,25)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgb(198,25,25)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgb(198,25,25)",
                    pointHoverBorderColor: "rgb(198,25,25))",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: medical_data,
                    spanGaps: false,
                },
                {
                    label: "Legal",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgb(206,191,25)",
                    borderColor: "rgb(206,191,25)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgb(206,191,25)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgb(206,191,25)",
                    pointHoverBorderColor: "rgb(206,191,25)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: legal_data,
                    spanGaps: false,
                },
                {
                    label: "Indemnity",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgb(210,135,22)",
                    borderColor: "rgb(210,135,22)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgb(210,135,22)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgb(210,135,22)",
                    pointHoverBorderColor: "rgb(210,135,22)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: indemnity_data,
                    spanGaps: false,
                },
                {
                    label: "Miscellaneous",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgb(125,205,34)",
                    borderColor: "rgb(125,205,34)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgb(125,205,34)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgb(125,205,34)",
                    pointHoverBorderColor: "rgb(125,205,34)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: misc_data,
                    spanGaps: false,
                },
                {
                    label: "Reserve Medical",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgb(22,168,194)",
                    borderColor: "rgb(22,168,194)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgb(22,168,194)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgb(22,168,194)",
                    pointHoverBorderColor: "rgb(22,168,194)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: reserve_medical_data,
                    spanGaps: false,
                },
                {
                    label: "Reserve Legal",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgb(181,45,202)",
                    borderColor: "rgb(181,45,202)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgb(181,45,202)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgb(181,45,202)",
                    pointHoverBorderColor: "rgb(181,45,202)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: reserve_legal_data,
                    spanGaps: false,
                },
                {
                    label: "Reserve Indemnity",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgb(64,93,220)",
                    borderColor: "rgb(64,93,220)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgb(64,93,220)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgb(64,93,220)",
                    pointHoverBorderColor: "rgb(64,93,220)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: reserve_indemnity_data,
                    spanGaps: false,
                },
                {
                    label: "Reserve Miscellaneous",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgb(234,46,108)",
                    borderColor: "rgb(234,46,108)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgb(234,46,108)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgb(234,46,108)",
                    pointHoverBorderColor: "rgb(234,46,108)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: reserve_misc_data,
                    spanGaps: false,
                },
            ]
        }


        if(cost_history_array['month_resolved'] !== false && cost_history_array['final_costs'] == true){
            data.datasets.unshift({
                label: "Final Medical",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgb(130, 14, 14)",
                borderColor: "rgb(130, 14, 14)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgb(130, 14, 14)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgb(130, 14, 14)",
                pointHoverBorderColor: "rgb(130, 14, 14)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: final_medical_data,
                spanGaps: false,
            });
            data.datasets.unshift({
                label: "Final Legal",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgb(145, 134, 17)",
                borderColor: "rgb(145, 134, 17)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgb(145, 134, 17)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgb(145, 134, 17)",
                pointHoverBorderColor: "rgb(145, 134, 17)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: final_legal_data,
                spanGaps: false,
            });
            data.datasets.unshift({
                label: "Final Indemnity",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgb(150, 96, 15)",
                borderColor: "rgb(150, 96, 15)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgb(150, 96, 15)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgb(150, 96, 15)",
                pointHoverBorderColor: "rgb(150, 96, 15)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: final_indemnity_data,
                spanGaps: false,
            });
            data.datasets.unshift({
                label: "Final Miscellaneous",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgb(75, 124, 19)",
                borderColor: "rgb(75, 124, 19)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgb(75, 124, 19)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgb(75, 124, 19)",
                pointHoverBorderColor: "rgb(75, 124, 19)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: final_misc_data,
                spanGaps: false,
            });

        }

        drawCostChart(context, data);

    }

    function drawCostChart(context, data){
        line_chart = new Chart(context,{
            type: 'line',
            data: data,
            options: {
                tooltips: {
                    //mode: 'label',
                    callbacks: {
                        label: function (tooltipItems, data) {
                            var dataset_index = tooltipItems.datasetIndex;
                            return data.datasets[dataset_index].label + ": $" + tooltipItems.yLabel.toString();
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function(value, index, values){
                                return "$" + value.toFixed(2);
                            }
                        }
                    }]
                }
            }
        })
    }

    function sendEmployeeEmail(employeeEmail){
        $.ajax({
                type: 'POST',
                url: '<?php echo route("updateZenGardenEmail"); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    employee_id: {{$injured_employee->id}},
                    injury_id: {{$injury->id}},
                    employee_email: employeeEmail
                }, success: function(data){
                        showAlert("Invite sent!", "confirm", 5);
                        modal.close();

                },  error: function(data){
                        showAlert("Sorry, there was a problem. Please try again later.", "deny", 5);
                }
        });


    }

    function sendLabelEmail(labelEmail, employeeName){

        $.ajax({
                type: 'POST',
                url: '<?php echo route("updateZenGardenEmail"); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    employee_id: {{$injured_employee->id}},
                    injury_id: {{$injury->id}},
                    employee_name: employeeName,
                    employee_email: labelEmail
                }, success: function(data){
                    showAlert("Invite sent!", "confirm", 5);
                        modal.close();

                },  error: function(data){
                        showAlert("Sorry, there was a problem. Please try again later.", "deny", 5);
                }
        });


    }

    </script>


<script>
    $(document).ready(function() {
        getRecentPosts();
        var employeeStatusVar = <?php echo json_encode($employeeStatus); ?>;
        
        $('#hospitalButton').on('click', function(){
            this.clicked = true;
            this.className = this.className + " selected";
            var buttons = document.querySelectorAll("button");
            for(var i = 0; i < buttons.length; i++){
                if(buttons[i].name == "blah" && this.id != buttons[i].id){
                    buttons[i].className = buttons[i].id;
                    buttons[i].clicked = false;
                }
            }
            checkButton(this.id);
        })
        $('#urgentcareButton').on('click', function(){
            this.clicked = true;
            this.className = this.className + " selected";
            var buttons = document.querySelectorAll("button");
            for(var i = 0; i < buttons.length; i++){
                if(buttons[i].name == "blah" && this.id != buttons[i].id){
                    buttons[i].className = buttons[i].id;
                    buttons[i].clicked = false;
                }
            }
            checkButton(this.id);
        })
        $('#doctorButton').on('click', function(){
            this.clicked = true;
            this.className = this.className + " selected";
            var buttons = document.querySelectorAll("button");
            for(var i = 0; i < buttons.length; i++){
                if(buttons[i].name == "blah" && this.id != buttons[i].id){
                    buttons[i].className = buttons[i].id;
                    buttons[i].clicked = false;
                }
            }
            checkButton(this.id);
        })
        $('#recoverhomeButton').on('click', function(){
            this.clicked = true;
            this.className = this.className + " selected";
            var buttons = document.querySelectorAll("button");
            for(var i = 0; i < buttons.length; i++){
                if(buttons[i].name == "blah" && this.id != buttons[i].id){
                    buttons[i].className = buttons[i].id;
                    buttons[i].clicked = false;
                }
            }
            checkButton(this.id);
        })
        $('#lightdutyButton').on('click', function(){
            this.clicked = true;
            this.className = this.className + " selected";
            var buttons = document.querySelectorAll("button");
            for(var i = 0; i < buttons.length; i++){
                if(buttons[i].name == "blah" && this.id != buttons[i].id){
                    buttons[i].className = buttons[i].id;
                    buttons[i].clicked = false;
                }
            }
            checkButton(this.id);
        })
        $('#fulldutyButton').on('click', function(){
            this.clicked = true;
            this.className = this.className + " selected";
            var buttons = document.querySelectorAll("button");
            for(var i = 0; i < buttons.length; i++){
                if(buttons[i].name == "blah" && this.id != buttons[i].id){
                    buttons[i].className = buttons[i].id;
                    buttons[i].clicked = false;
                }
            }
            checkButton(this.id);
        })
        $('#maxrecoveryButton').on('click', function(){
            this.clicked = true;
            this.className = this.className + " selected";
            var buttons = document.querySelectorAll("button");
            for(var i = 0; i < buttons.length; i++){
                if(buttons[i].name == "blah" && this.id != buttons[i].id){
                    buttons[i].className = buttons[i].id;
                    buttons[i].clicked = false;
                }
            }
            checkButton(this.id);
        })
        $('#terminatedButton').on('click', function(){
            this.clicked = true;
            this.className = this.className + " selected";
            var buttons = document.querySelectorAll("button");
            for(var i = 0; i < buttons.length; i++){
                if(buttons[i].name == "blah" && this.id != buttons[i].id){
                    buttons[i].className = buttons[i].id;
                    buttons[i].clicked = false;
                }
            }
            checkButton(this.id);
        })
        $('#resignedButton').on('click', function(){
            this.clicked = true;
            this.className = this.className + " selected";
            var buttons = document.querySelectorAll("button");
            for(var i = 0; i < buttons.length; i++){
                if(buttons[i].name == "blah" && this.id != buttons[i].id){
                    buttons[i].className = buttons[i].id;
                    buttons[i].clicked = false;
                }
            }
            checkButton(this.id);
        })
        $('#deathButton').on('click', function(){
            this.clicked = true;
            this.className = this.className + " selected";
            var buttons = document.querySelectorAll("button");
            for(var i = 0; i < buttons.length; i++){
                if(buttons[i].name == "blah" && this.id != buttons[i].id){
                    buttons[i].className = buttons[i].id;
                    buttons[i].clicked = false;
                }
            }
            checkButton(this.id);
        })
        $('#status-date').on('input', function(){
            checkButton(this.id);
        })
    });

    function checkButton(id){
        var dateStatus = document.getElementById("status-date");
        var fillBtn = document.getElementById("fillInBtn");
        var submitBtn = document.getElementById("submitStatusUpdate");
        if(id != "status-date"){
            var btn = document.getElementById(id);
            if(btn.clicked == true && dateStatus.value != ""){
                fillBtn.style = "display:none";
                submitBtn.style = "display:inline";
            }
            else{
                fillBtn.style = "display:inline";
                submitBtn.style = "display:none";
            }
        }
        else{
            var hospitalBtn = document.getElementById("hospitalButton");
            var urgentcareBtn = document.getElementById("urgentcareButton");
            var doctorBtn = document.getElementById("doctorButton");
            var recoverhomeBtn = document.getElementById("recoverhomeButton");
            var lightdutyBtn = document.getElementById("lightdutyButton");
            var fulldutyBtn = document.getElementById("fulldutyButton");
            var maxrecoveryBtn = document.getElementById("maxrecoveryButton");
            var terminatedBtn = document.getElementById("terminatedButton");
            var resignedBtn = document.getElementById("resignedButton");
            var deathBtn = document.getElementById("deathButton");
            if((hospitalBtn.clicked == true || urgentcareBtn.clicked == true || doctorBtn.clicked == true || recoverhomeBtn.clicked == true || lightdutyBtn.clicked == true || fulldutyBtn.clicked == true || maxrecoveryBtn.clicked == true || terminatedBtn.clicked == true || resignedBtn.clicked == true || deathBtn.clicked == true) && dateStatus.value != ""){
                fillBtn.style = "display:none";
                submitBtn.style = "display:inline";
            }
            else{
                fillBtn.style = "display:inline";
                submitBtn.style = "display:none";
            }
        }
    }

    function getRecentPosts(){
        $.ajax({
            type: 'POST',
            url: '<?php echo route('getRecentPosts'); ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                injury_id: '<?php echo $injury->id; ?>'
            },
            success: function(data){
                console.log(JSON.stringify(data));
                var recent_posts = data;
                buildRecentPosts(recent_posts);
            }
        })
    }

    function buildRecentPosts(posts){
        var i = 0;
        var html = "";
        for(i; i < posts.length; i++){
            if(posts[i].user_id != null){
                var addedByUser = "Submitted by " + posts[i].user_id;
            }else{
                var addedByUser = "";
            }

            
            var postTitle = "";
            var type = posts[i].type;
            var body = posts[i].body;
                
                
            html += '<div class="messageContainer">' +
                '       <div class="messagePost">' +
                '           <div class="messageContent">' +
                '               <span class="messageType ' + posts[i].type +'"><span class="messageIcon" style="background-image:url('+posts[i].profile_image+')"></span></span>' +
                '               <span class="messageTitle">' + posts[i].summary +'</span>' +
                //'               <span class="messageBody">' + tree_posts[i].summary +' ' + '<br />' + addedByUser +'</span>' +
                '               <span class="messageDateTime">' + posts[i].formatted_date + '</span>' +
                '            </div>' +
                '         </div>' +
                '      </div>' +
                '   </div>';
        }
        $('#recentComms').html(html);
    }

</script>

@endsection

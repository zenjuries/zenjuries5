<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>zenjuries</title>

    <!-- **** MOBILE DETECT CODE **** -->
    @include('partials.detectMobile')
    <?php

    use Illuminate\Console\Command;
    use Illuminate\Support\Facades\Mail;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;

    //global var containing the mobile type (android/iPhone/browser)
    $mobile_check = checkApps();
    $emails = [];
    $emails = \App\User::where('deleted_at', NULL)->whereNotNull('email')->pluck('email');
    Log::debug($emails);

    ?>
    <script>
        var mobile_check = '<?php echo checkApps(); ?>';
    </script>

    <!-- this can be used to load platform-specific css or script. Anything in this if statement will only be included if the user's platform matches. -->
@if($mobile_check === "android")
    <!-- android specific code -->
@elseif($mobile_check === "iPhone")
    <!-- iOS specific code -->
@else
    <!-- regular browser code -->
        <script>console.log('browser');</script>
@endif

<!-- **** END MOBILE DETECT CODE -->

    <!-- zen5 Style -->
    <link rel="stylesheet" href="{{URL::asset('/css/zen5_style.css') }}">
    <!--ZenjuriesFont-->
    <link rel="stylesheet" href="{{URL::asset('/css/zenfontstyles.css') }}">
    <!--Animation-->
    <link rel="stylesheet" href="{{URL::asset('/css/animate.css') }}">
    <!--jBox-->
    <link rel="stylesheet" href="{{URL::asset('/css/jBox.all.min.css') }}">
    <!-- zen5 Style -->
    <link rel="stylesheet" href="{{URL::asset('/css/zen5_style.css') }}">

    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>
    <script src="{{URL::asset('/js/jBox.all.min.js') }}"></script>
    <script src="/js/wow.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <!-- Icons -->
    <link rel="apple-touch-icon" sizes="180x180" href="/icon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/icon/favicon-16x16.png">
    <link rel="manifest" href="/icon/site.webmanifest">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
</head>

<body ontouchstart="">

@include('partials.zen5_topNav')
<div class="mainContent">

    @yield('maincontent')

    <div class="contentBlock lessPadding">
		<div class="container setMinHeight">

            <section class="sectionPanel">
                {{-- <span class="sectionTitle"> Email</span>
                <div class="sectionDescription">Send Email to all Users</div>  --}}
                <div class="sectionContent">
                    <span class="sectionTitle"> Email</span>
                    <div class="sectionDescription">Send Email to all Users</div>
                        <div class="buttonArray">
                            <button class="cyan centered" id="selectAll">All</button>
                            <button class="cyan centered" id="selectNone">None</button>
                            {{-- <button class="olive centered" id="sendEmail">Send Email</button> --}}
                            {{-- <button class="showEmailModal" data-modalcontent="#emailModal">Send Email</button> --}}
                            <button class="cyan centered showModal" data-modalcontent="#emailModal">Send Email</button>
                        </div>
                </div>
            {{-- </section> --}}
            {{-- <section class="sectionPanel"> --}}
                <div class="sectionContent">
                    @foreach($emails as $email)
                    {{-- <input type="checkbox" name="email" id="email"><label for="email" id="userEmail" style="color:white;">{{$email}}</label><br> --}}
                    <label style="color:white;"><input type="checkbox" name="email" id="email"/> {{$email}}</label>
                    <br>
                    @endforeach
                </div>
            </section>
            {{-- </section> --}}
</div>
<div id="showModal" class="zModal" style="display:none">
    <div class="modalContent modalBlock" id="emailModal" style="display: none">
      <div class="modalContent dark">
          <div class="modalHeader"><span class="modalTitle">Send Email</span></div>
          <div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark">
			<section class="formBlock dark">

					<div class="formGrid">
						<div class="formInput">
							<!-- input -->
							<label for="mail">Subject</label>
							<div class="mail"><input type="text" id="subject"/></div>
						</div>
                        <div class="formGrid">
                            <div class="formInput">
                                <!-- input -->
                                <label for="mail">Message</label>
                                <div class="mail"><input type="text" id="message"/></div>
                            </div>
                        </div>
                    </div>
                    <div class="buttonArray">
						<button class="green" id="sendEmail"><div class="icon icon-retweet"></div>Send</button>
					</div>

			</section>
		</section>
	</div>
</div>



<script>
    $('#selectAll').on('click', function(){
        //$("input[type=checkbox]").prop('checked', $(this).prop('checked'));
       // $(email).prop('checked', $(this).prop('checked'));
       $("input:checkbox").prop("checked", true);
    });

    $('#selectNone').on('click', function(){
        //$("input[type=checkbox]").prop('checked', $(this).prop('checked'));
       // $(email).prop('checked', $(this).prop('checked'));
       $("input:checkbox").prop("checked", false);
    });

    $('#sendEmail').on('click', function(){
        var emailList = [];
        var subject = $('#subject').val();
        var msgText = $('#message').val();

        $("input:checkbox[name='email']:checked").each(function(index) {

                    // var labelEmail = document.getElementById("userEmail");
                    // var labelText = labelEmail.textContent.trim();
                    // // emailList.push($(this).val());
                    // emailList.push(labelText(index));
                    // console.log(emailList);
                    emailList.push($(this).parent().text());


        });
                        $.ajax({
                            type: 'POST',
                            url: '{{ route("emailAll") }}',
                            data: {
                                _token: '<?php echo csrf_token(); ?>',
                                emailList: emailList,
                                subject: subject,
                                msgText: msgText

                            },
                            success: function (data){

                                modal.close();
                                showAlert("Email Sent!", "confirm", 5);
                            },
                            error: function (data){
                                console.log(data);
                                var errors = data.responseJSON;
                                console.log(errors);
                                showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
                            }

                        });

    });

</script>
<script src="{{URL::asset('/js/colorpicker.js') }}"></script>
<script>
    function showAlert(text, alert_class, display_time_in_seconds){
        var alert_div = document.createElement('div');
        var alert_html = '<div class="zAlertIcon ' + alert_class + '"><div class="icon"></div></div>' +
            '<div class="message">' + text + '</div>' +
            '<div class="dismiss"><div class="icon icon-times"></div></div>';
        $(alert_div)
            .addClass("zAlertDialogue open")
            .html(alert_html)
            .click(function(){
                $(this).remove();
            });

        //alert('showing alert');

        // to animate alert, add class of -open, -close, -exit to zAlertDialogue.  all animations are 1s

        $('.zAlertArea').append(alert_div);

        setTimeout(function(){
            $(alert_div).addClass("zAlertDialogue close");
            setTimeout(function(){
                $(alert_div).height(0).remove();
            }, 900);
            //$(alert_div).remove();
        }, (display_time_in_seconds * 900));

    }
</script>
<script>
    //modal globalVar to contain the reference to the modal
    var modal;

    //THIS FUNCTION SHOULD BE easy to drop on a page, the class triggering the onclick and the modal name should be the main changes.
    $('.showModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            //modal ID goes here
            content: $('#showModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();

        //script related to a specific content block can be added like this
        //status modal
        if(target === "#contentBlock1"){
            //scrpit for contentBlock1
        }
        modal.open();
    });
</script>
<script>
    wow = new WOW(
        {
            boxClass:     'wow',      // default
            animateClass: 'animate__animated', // default
            offset:       0,          // default
            mobile:       true,       // default
            live:         true        // default
        }
    )
    wow.init();
</script>
</body>

</html>

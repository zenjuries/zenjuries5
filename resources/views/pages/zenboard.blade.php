@extends('layouts.zen5_layout')
@section('maincontent')
<?php
	$user = Auth::user();
	$company = $user->getCompany();
	//get the user theme
	$theme = $user->zengarden_theme;
	$theme = str_replace("theme", "", $theme);
	$theme = strtolower($theme);
	//new theme values
	$brandID = env('BRAND_NAME');
    $themeID = 'theme' . $user->theme_id;
	$arrayPassedIn = str_split($user->shortcuts);

	//arrays for message popup [message icon, message title, message body, button color, button ID, button icon, button label]
	$message1 = array("hmm-color", "message title", "message body", "red", "dismiss", "search", "dismiss");
	$popmessage = $message1;

    $company_logo_location = $company->logo_location;

	//getting the values for the drop downs
	$injury_count = count($company->getInjuries());
	$open_injury_count = count($company->getInjuries("open"));
	$resolved_injury_count = count($company->getInjuries("resolved"));
	
	$today = \Carbon\Carbon::now();
	$policy_check_date = $company->created_at;
	$diff_in_days = 0;
	
	$diff_in_days = 60 - $policy_check_date->diffInDays($today);
	if($company->policy_number === NULL && Auth::user()->is_zenpro === 0){
		if($diff_in_days < 0){
			$policyholder_state = 'locked';
			$diff_in_days = 0;
		}else{
			$policyholder_state = 'unlocked';
		}
	}else{
		$policyholder_state = 'clear';
	}
	
?>

<link rel="stylesheet" href="/css/animcharts.css?<$date format='yyyy-MM-dd_HH:mm:ss'/$>">
<script src="/js/animcharts.js"></script>

<div class="pageContent theme<?php echo ucfirst( $theme ?? '' ); ?> bgimage-bgrb-f fade-in" id="zenboardPageContent" style="background-image:url('/branding/{{ $brandID }}/themes/{{ $themeID }}/dashboard.jpg');">

	<div class="headerBlock headerFade">
		<div class="headerContainer">
			<div class="pageHeader wow animate__animated animate__bounceInLeft">
				<div class="pageHeader__title">
				<div class="companyIcon header" id="zenboardCompanyLogo" style="background-image:url({{Auth::user()->getCompany()->logo_location}}?{{ $date = str_replace(' ', '', \Carbon\Carbon::now()) }})"></div>
				<span class="logoTitle"><b>{{Auth::user()->getCompany()->company_name}}</b></span>
				</div>
				<div class="pageHeader__subTitle">
						<span class="icon"><img src="images/icons/user.png"></span><span class="subContent">logged in as <b>{{ $user->name }}</b></span>
					</div>
			</div>
			<div class="pageIcon wow animate__animated animate__bounceInRight">
				<img src="/images/icons/dashboard-dull.png">
			</div> 
		</div>           
	</div>


	<div class="container">
		
		<div class="homeButtons">
			<div class="itemGrid animate__animated animate__bounceIn">			
				<div class="itemGrid__nav noSelect"><a href="/newinjury"><span class="navImg"><img src="/images/icons/plus.png"></span><span class="subText">report new</span><span class="mainText">injury</span></a></div>
				<div class="itemGrid__nav noSelect"><a href="/weeklysummary"><span class="navImg"><img src="/images/icons/reports.png"></span><span class="subText">weekly</span><span class="mainText">summary</span></a></div>
				<!--<div class="itemGrid__nav noSelect"><a href="/messages"><span class="navImg"><img src="/images/icons/messages.png"></span><span class="subText">view my</span><span class="mainText">messages</span></a></div>-->
				<div class="itemGrid__nav noSelect"><a href="/zencarelist"><span class="navImg"><img src="/images/icons/injurylist.png"></span><span class="subText">open</span><span class="mainText">injuries</span></a></div>
				
				<div class="itemGrid__nav noSelect"><a href="/stats"><span class="navImg"><img src="/images/icons/stats.png"></span><span class="subText">system</span><span class="mainText">reports</span></a></div>

				<!--<div class="itemGrid__nav noSelect comingsoon"><a href="javascript:void(0);"><span class="navImg"><img src="/images/icons/stats.png"></span><span class="subText">zenMetrics</span><span class="mainText">reports</span></a></div>-->

				<div class="itemGrid__nav noSelect"><a href="/help"><span class="navImg"><img src="/images/icons/help.png"></span><span class="subText">help &</span><span class="mainText">support</span></a></div>
				<div class="itemGrid__nav noSelect"><a href="/zendocs"><span class="navImg"><img src="/images/icons/documents.png"></span><span class="subText">view your</span><span class="mainText">documents</span></a></div>											
			</div>
		</div>


			<div id="statsBar" class="statsBar expand">
			<div id="statsBarStats" class="shortStats">
				<div class="shortStatsLeft animate__animated animate__bounceInLeft">
					<ul>
						<li><div class="setToolTip inline"><span class="toolTip">total<br>injuries</span><span class="icon"><img src="images/icons/bandaid-simple.png"></span><span class="value">{{$injury_count}}</span></li>
						<li><div class="setToolTip inline"><span class="toolTip">open<br>injuries</span><span class="icon"><img src="images/icons/plus-simple.png"></span><span class="value">{{$open_injury_count}}</span></li>
						<li><div class="setToolTip inline"><span class="toolTip">closed<br>injuries</span><span class="icon"><img src="images/icons/heart-simple.png"></span><span class="value">{{$resolved_injury_count}}</span></li>
					</ul>
				</div>
				<div class="shortStatsExpand"><div class="doMax">[&nbsp;see&nbsp;current&nbsp;stats&nbsp;]</div></div>
				<div class="shortStatsRight animate__animated animate__bounceInRight">
					<ul>
						<li><div class="setToolTip inline"><span class="toolTip">average injuries<br>reported</span><span class="icon"><img src="images/icons/time-good.png"></span><span class="value">{{$company->percentageOfOnTimeClaims()}}</span></div></li>
						<li><div class="setToolTip inline"><span class="toolTip">average injury<br>duration</span><span class="icon"><img src="images/icons/date-alert.png"></span><span class="value">{{$company->averageClaimDuration()}}d</span></div></li>
						<li><div class="setToolTip inline"><span class="toolTip">average injury<br>litigation</span><span class="icon"><img src="images/icons/litigation-good.png"></span><span class="value">{{$company->percentageOfClaimsLitigated()}}</span></div></li>
					</ul>
					<div class="shortStatsX doMin">[&nbsp;minimize&nbsp;]</div>
				</div>
			</div>
			<div class="fullStats">
				<div class="statsBasic">
					<section>
						<div><span class="metricIcon animate__animated animate__bounce" style="background-image:url('images/icons/bandaid-circle.png');">{{count($company->getInjuries())}}</span><span class="metricLabel">total injuries</span></div>
						<div><span class="metricIcon animate__animated animate__bounce animate__delay-1s" style="background-image:url('images/icons/plus-circle.png');">{{count($company->getInjuries("open"))}}</span><span class="metricLabel">open injuries</span></div>
						<div><span class="metricIcon animate__animated animate__bounce animate__delay-2s" style="background-image:url('images/icons/heart-circle.png');">{{count($company->getInjuries("resolved"))}}</span><span class="metricLabel">closed injuries</span></div>
					</section>
				</div>
				<hr>
				<div class="riskRating">
					<section>
						<div>
							<span class="riskCard animate__animated animate__headShake animate__delay-3s"><img src="images/icons/risk-low.png"></span>
						</div>
						<div>
							<table>
								<tr class="animate__animated animate__pulse animate__delay-3s"><td><span class="metricIcon" style="background-image:url('images/icons/time-good.png');line-height:78px;">{{$company->percentageOfOnTimeClaims()}}%</span></td>
									<td><span class="metricLabel">average injuries reported within 24 hours</span></td></tr>
								<tr class="animate__animated animate__pulse animate__delay-4s"><td><span class="metricIcon" style="background-image:url('images/icons/date-alert.png');">{{$company->averageClaimDuration()}}<span>days</span></span></td>
									<td><span class="metricLabel">average duration of injuries</span></td></tr>
								<tr class="animate__animated animate__pulse animate__delay-5s"><td><span class="metricIcon" style="background-image:url('images/icons/litigation-good.png');line-height:66px;">{{$company->percentageOfClaimsLitigated()}}</span></td>
									<td><span class="metricLabel">average litigation of injuries</span></td></tr>
							</table>
						</div>
					</section>
				</div>
				<!-- <hr>
				<div class="costStats">
					<span class="statTitle">claim costs</span>
					<section> -->
						<!--<div><span class="vbar" style="height:80px;">$1200</span><span class="vbarLabel">open</span></div>
						<div><span class="vbar" style="height:100px;">$1500</span><span class="vbarLabel">paid</span></div>
						<div><span class="vbar" style="height:70px;">$1000</span><span class="vbarLabel">reserved</span></div>
						<div><span class="vbar" style="height:60px;">$900</span><span class="vbarLabel">closed</span></div>
						<div><span class="vbar" style="height:60px;">$1100</span><span class="vbarLabel">total incurred</span></div>-->
						<!-- <div><span class="vbar" style="height:100px">N/A</span><span class="vbarLabel">paid</span></div>;
						<div><span class="vbar" style="height:100px">N/A</span><span class="vbarLabel">final</span></div>;
						<div><span class="vbar" style="height:100px">N/A</span><span class="vbarLabel">reserve</span></div>;
						<div><span class="vbar" style="height:100px">N/A</span><span class="vbarLabel">total incurred</span></div>;
					</section>
				</div>					 -->
			</div>	
		</div>	

		

		<div class="homeFavorites animate__animated animate__bounceInUp">
			<section>
				<div class="favBox"><a class="noLink favItem"><span class="favTitle">injury <span style="font-weight:400;">activity</span></span>
				<span class="favContent">

				</span></a></div>

				<div class="favBox"><a class="noLink favItem"><span class="favTitle">user&team <span style="font-weight:400;">activity</span></span>
				<span class="favContent">
				
				</span></a></div>
				
				<div class="favBox"><a class="noLink favItem"><span class="favTitle">John Smith</span></span>
				<span class="favContent">
				
				</span></a></div>
				
				<div class="favBox"><a class="noLink favItem"><span class="favTitle">custom chart</span></span>
				<span class="favContent">

				</span></a></div>
				
				<div class="favBox"><a class="noLink favItem"><span class="favTitle">custom report</span></span>
				<span class="favContent">

				</span></a></div>

				<div class="favBox"><a class="noLink favItem"><span class="favTitle">custom report</span></span>
				<span class="favContent">

				</span></a></div>

				<div class="favBox"><a class="noLink favItem"><span class="favTitle">custom report</span></span>
				<span class="favContent">

				</span></a></div>

				<div class="favBox"><a class="noLink favItem"><span class="favTitle">custom report</span></span>
				<span class="favContent">

				</span></a></div>
			</section>
		</div>

		<div>		

			<!-- display only for zenPro.  Should be a list of all injuries associated with them. -->
			@if(Auth::user()->is_zenpro)
			<div class="flexPanel">
				<div class="viewControl">
					<div class="compactViewLeft animate__animated animate__bounceInLeft">
						<span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/insurance.png"> my caseload</span>
					</div>
					<div class="compactViewExpand"><div class="doMax">[&nbsp;view&nbsp;caseload&nbsp;]</div></div>
					<div class="compactViewRight animate__animated animate__bounceInRight">
						<ul>
							<li><span class="icon"><img src="images/icons/injury-ok.png"></span><span id="caseload__ok" class="value FG__okblue">0</span></li>
							<li><span class="icon"><img src="images/icons/injury-alert.png"></span><span id="caseload__alert" class="value FG__alert">0</span></li>
							<li><span class="icon"><img src="images/icons/injury-critical.png"></span><span id="caseload__critical" class="value FG__critical">0</span></li>
						</ul>
						<div class="compactViewX doMin">[&nbsp;minimize&nbsp;]</div>
					</div>
				</div>
				<div class="viewContent">
					<div class="zen-actionPanel transparent tight">
						<div class="sectionContent dynamicSize">
							<table class="caseloadTable zenListTable tight">
								<tbody id="caseloadInjuryDropdown">
								<tr id="myCaseListHeading" class="tableHeadings">
										<td></td>
										<td class="name"><button class="caseloadSorter sorter showIconModal">name</button></td>
										<td class="injurydate"><button class="caseloadSorter sorter showIconModal">date</button></td>
										<td class="severity"><button class="caseloadSorter sorter showIconModal">severity</button></td>
										<td class="agency"><button class="caseloadSorter sorter showIconModal">agency</button></td>
										<td class="policyholder"><button class="caseloadSorter sorter showIconModal">policyholder</button></td>
										<td class="mood"><button class="caseloadSorter sorter showIconModal">mood</button></td>
									</tr>
									<!-- replace with .partial content
									<tr>
										<td><div class="progressMeter small inline status7 critical"><svg viewBox="0 0 100 100"><circle class="pm" r="50%" cx="50%" cy="50%"></circle><div class="overlay"><div class="avatar"></div></div></svg></div></td>
										<td class="name"><span>Henry Smith</span></td>
										<td class="date"><span>12/23/2020</span></td>
										<td class="severity"><span>mild</span></td>
										<td class="agency"><span>an agency</span></td>
										<td class="policyholder"><span>Some company</span></td>
										<td><div class="moodVisual smaller"><div class="currentMood"></div></div></td>
									</tr>
									<tr class="tablerowspace"><td></td></tr>-->
									
								</tbody>
							</table>
						</div>					
					</div>						
				</div>
			</div>
@endif
			<!-- for all users -->

			<!-- <div class="flexPanel">
				<div class="viewControl">
					<div class="compactViewLeft animate__animated animate__bounceInLeft">
						<span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/bandaid.png">your open <span class="hideMobile">injuries</span></span>
					</div>
					<div class="compactViewExpand"><div class="doMax">[&nbsp;see&nbsp;open&nbsp;injuries&nbsp;]</div></div>
					<div class="compactViewRight animate__animated animate__bounceInRight">
						<ul>
							<li><span class="icon"><img src="images/icons/injury-ok.png"></span><span id="unresolved__ok" class="value FG__okblue">0</span></li>
							<li><span class="icon"><img src="images/icons/injury-alert.png"></span><span id="unresolved__alert" class="value FG__alert">0</span></li>
							<li><span class="icon"><img src="images/icons/injury-critical.png"></span><span id="unresolved__critical" class="value FG__critical">0</span></li>
						</ul>
						<div class="compactViewX doMin">[&nbsp;minimize&nbsp;]</div>
					</div>
				</div>
				<div class="viewContent">
					<div class="zen-actionPanel transparent tight">
						<div class="sectionContent dynamicSize">
							<table class="unresolvedTable zenListTable tight">
								<tbody id="unresolvedInjuryDropdown">
								<tr class="tableHeadings unresolvedHeadings">
										<td></td>
										<td class="name"><button class="unresolvedSorter sorter showIconModal">name</button></td>
										<td class="injurydate"><button class="unresolvedSorter sorter showIconModal">date</button></td>
										<td class="severity"><button class="unresolvedSorter sorter showIconModal">severity</button></td>
										<td class="total"><button class="unresolvedSorter sorter showIconModal">total$</button></td>
										<td class="progress"><button class="unresolvedSorter sorter showIconModal">progress</button></td>
										<td class="mood"><button class="unresolvedSorter sorter showIconModal">mood</button></td>
									</tr>
									
								</tbody>
							</table>
						</div>
					</div>
					<div class="pagination">
						<button id="prevUnresolved" class="pagePrev ghost">prev</button>
						<div class="pages" id="unresolvedPageInfoSpan">showing 1 of 6</div>
						<button id="nextUnresolved" class="pageNext ">next</button>
					</div>
				</div>
			</div> -->

			<!-- <div class="flexPanel">
				<div class="viewControl">
					<div class="compactViewLeft animate__animated animate__bounceInLeft">
						<span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/stats.png"> injury <span class="hideMobile">overview</span></span>
					</div>
					<div class="compactViewExpand"><div class="doMax">[&nbsp;check&nbsp;overview&nbsp;]</div></div>
					<div class="compactViewRight animate__animated animate__bounceInRight">
						<div class="compactViewX doMin">[&nbsp;minimize&nbsp;]</div>
					</div>
				</div>
				<div class="viewContent">
					<div class="zen-actionPanel transparent tight">
						<div class="sectionContent dynamicSize">
							<div class="center">
								<div class="chartContainer">
									<div class="darkChart overviewChart">
										<span class="chartTitle">injury lag time</span>
										<table class="chartTable">
											<tr>
												<td>same day</td>
												<td>next day</td>
												<td>same week</td>
												<td>2 weeks</td>
												<td>> 2 weeks</td>
											</tr>
											<tr>
												<td id="sameDay">23</td>
												<td id="nextDay">1</td>
												<td id="sameWeek">0</td>
												<td id="twoWeeks">0</td>
												<td id="greaterThanTwoWeeks">0</td>
											</tr>                                
										</table>
									</div>
								</div>
							</div>                                                  
						</div>
					</div>
				</div>
			</div> -->
			
			<!-- for zenpro and agents.  watchlist, list of all injuries tagged or alert/critical state -->
			<!-- @if (Auth::user()->type === "agent" || Auth::user()->is_zenpro)
			<div class="flexPanel">
				<div class="viewControl">
					<div class="compactViewLeft animate__animated animate__bounceInLeft">
						<span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/magnify.png"> my watchlist</span>
					</div>
					<div class="compactViewExpand"><div class="doMax">[&nbsp;check&nbsp;watchlist&nbsp;]</div></div>
					<div class="compactViewRight animate__animated animate__bounceInRight">
						<ul>
							<li><span class="icon"><img src="images/icons/injury-alert.png"></span><span id="watchList__alert" class="value FG__alert">0</span></li>
							<li><span class="icon"><img src="images/icons/injury-critical.png"></span><span id="watchList__critical" class="value FG__critical">0</span></li>
						</ul>
						<div class="compactViewX doMin">[&nbsp;minimize&nbsp;]</div>
					</div>
				</div>
				<div class="viewContent">
					<div class="zen-actionPanel transparent tight">
						<div class="sectionContent dynamicSize">
							<table class="watchlistTable zenListTable tight">
								<tbody id="myWatchListDropdown">
									<tr class="watchlistHeadings tableHeadings">
										<td></td>
										<td class="name"><button class="watchlistSorter sorter showIconModal">name</button></td>
										<td class="injurydate"><button class="watchlistSorter sorter showIconModal">date</button></td>
										<td class="severity"><button class="watchlistSorter sorter showIconModal">severity</button></td>
										<td class="total"><button class="watchlistSorter sorter showIconModal">total$</button></td>
										<td class="progress"><button class="watchlistSorter sorter showIconModal">progress</button></td>
										<td class="mood"><button class="watchlistSorter sorter showIconModal">mood</button></td>
									</tr>
								</tbody>
							</table>
						</div>					
					</div>
					<div class="pagination">
						<button id="prevMyWatchList" class="pagePrev ghost">prev</button>
						<div class="pages" id="myWatchListpageInfoSpan">showing 1 of 6</div>
						<button id="nextMyWatchList" class="pageNext ">next</button>
					</div>
				</div>				
			</div>
			@endif -->


		</div>
	</div>
</div>
	
@include('js.zenboard_js')

<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
    @include('partials.modals.modalSample')         
	@include('partials.modals.editPolicy')
	@include('partials.modals.newWelcome')
	@include('partials.modals.newPolicy')
	@include('partials.modals.popmessage')
</div>
@if (Auth::user()->type === "agent" || Auth::user()->is_zenpro)
	@include('partials.myWatchList_dropdown')
@endif
@if(Auth::user()->is_zenpro)
	@include('partials.myCaseLoad')
@endif
@include('partials.unresolved_dropdown')
<!--
<script>
    $(document).ready(function(){
        $(".showZenModal").modal('show');
    });
</script>
-->
<script>
    $('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
	$('.showZenModalLocked').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true,
            closeButton: false,
            closeOnClick: false
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>
<script>
	$(document).ready(function() {
		var policyholder_state = '{{$policyholder_state}}';
		console.log('policy state: ' + policyholder_state);
		if(policyholder_state != 'clear'){
			if(policyholder_state === "locked"){
				console.log('locked');
				$("#policyBlockerLocked").click();
				$('#daysLeft').text('0');
			}else{
				console.log('unlocked');
				$("#policyBlocker").click();
				$('#daysLeft').text('{{$diff_in_days}}');
			}
		}
		//if(localStorage.getItem('popWelcomeState') != 'shown'){
		//	$('.showZenModal[data-modalcontent="#welcomeModal"]').click();
		//	localStorage.setItem('popWelcomeState', 'shown');
		//}


		var thisRandomCompanyImage = Math.floor(Math.random() * 9) + 1;
        var company_logo_location =  JSON.parse('<?php echo json_encode($company_logo_location); ?>');
        if(company_logo_location === null || company_logo_location === ""){
            $.ajax({
                type: 'POST',
                url: '/createDefaultCompanyLogo',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    company_id: {{$company->id}},
                    random_image_number: thisRandomCompanyImage
                },
                success: function(data){
					$('#policyholderLogo').css('background-image', 'url("/images/default_logos/company' + thisRandomCompanyImage + '.png")');
                    $('#zenboardCompanyLogo').css('background-image', 'url("/images/default_logos/company' + thisRandomCompanyImage + '.png")');
                },
                error: function(data){
                    console.log(data);
                }
            });
        }
	});
</script>
<script>
	$(document).ready(function() {
        //var favArray = <?php echo json_encode($arrayPassedIn); ?>;
		var statsBarObj = document.getElementById("statsBar"); 

		var barContainer = $('.costStats').find('section');
		barContainer.addClass('center');
		
		$('#statsBarStats').on("click", function() {
			$('#statsBar').toggleClass('expand contract');
			//if(statsBarObj.className == "statsBar expand"){
			//	favArray[0] = "0";
			//	updateStringPassedIn();
			//}
			//else if(statsBarObj.className == "statsBar contract"){
			//	favArray[0] = "1";
			//	updateStringPassedIn();
			//}
    	}) 

		//function updateStringPassedIn(){
        //	var stringPassedIn = favArray.toString().replaceAll(",", "");
        //	$.ajax({
        //			type: "POST",
        //			url: "{{ route('updateUserShortcuts') }}",
        //			data: {
        //    		_token: '<?php echo csrf_token(); ?>',
        //    		user_id: {{$user->id}},
        //    		shortcuts: stringPassedIn
    	//		}
    	//	});
    	//}

		getZenboardStats();
	});

	function getZenboardStats(){
		$.ajax({
			type: "POST",
			url: "{{ route('getZenboardStats') }}",
			data: {
				_token: '<?php echo csrf_token(); ?>',
            	company_id: {{$company->id}},
			},
			success: function(data){
				console.log('got stats');
				//console.log(data);
				setTimeout(displayZenboardStats(data), 5);
			},
		});
	}

	function displayZenboardStats(data){
		console.log(stats_array);
		var stats_array = data;
		buildLagTimeTable(stats_array);
		buildCostStatsSection(stats_array);
		getAvgSameDayReportTime(stats_array['reporting_time_array']);
	}

	function getAvgSameDayReportTime(report_time_array){
		var total_injuries = report_time_array[0] + report_time_array[1] + report_time_array[2] + report_time_array[3] + report_time_array[4];
		console.log('total injuries: ' + total_injuries);
		same_day_percent = report_time_array[0] / total_injuries;
		console.log('same day percent' + same_day_percent);
	}

	function buildLagTimeTable(stats_array){
		$('#sameDay').html(stats_array['reporting_time_array'][0]);
		$('#nextDay').html(stats_array['reporting_time_array'][1]);
		$('#sameWeek').html(stats_array['reporting_time_array'][2]);
		$('#twoWeeks').html(stats_array['reporting_time_array'][3]);
		$('#greaterThanTwoWeeks').html(stats_array['reporting_time_array'][4]);
	}

	function buildCostStatsSection(stats_array){
		var barContainer = $('.costStats').find('section');
		console.log('stats paid medical');
		console.log(removeDollarSignForChart(stats_array['average_paid_medical']));
		var paidTotal = parseInt(removeDollarSignForChart(stats_array['average_paid_medical'])) + parseInt(removeDollarSignForChart(stats_array['average_paid_indemnity'])) + parseInt(removeDollarSignForChart(stats_array['average_paid_legal'])) + parseInt(removeDollarSignForChart(stats_array['average_paid_miscellaneous']));
		var finalTotal = parseInt(removeDollarSignForChart(stats_array['average_final_medical'])) + parseInt(removeDollarSignForChart(stats_array['average_final_indemnity'])) + parseInt(removeDollarSignForChart(stats_array['average_final_legal'])) + parseInt(removeDollarSignForChart(stats_array['average_final_miscellaneous']));
		var reserveTotal = parseInt(removeDollarSignForChart(stats_array['average_reserve_medical'])) + parseInt(removeDollarSignForChart(stats_array['average_reserve_indemnity'])) + parseInt(removeDollarSignForChart(stats_array['average_reserve_legal'])) + parseInt(removeDollarSignForChart(stats_array['average_reserve_miscellaneous']));
		html = "";

		var paidTotalHeight = "";
		var paidFinalTotalHeight = "";
		var reserveTotalHeight = "";

		if(paidTotal >= 5000){
			paidTotalHeight = "100px";
		}
		else if(paidTotal >= 4000 && paidTotal < 5000){
			paidTotalHeight = "90px";
		}
		else if(paidTotal >= 3000 && paidTotal < 4000){
			paidTotalHeight = "80px";
		}
		else if(paidTotal >= 2000 && paidTotal < 3000){
			paidTotalHeight = "70px";
		}
		else if(paidTotal >= 1000 && paidTotal < 2000){
			paidTotalHeight = "60px";
		}
		else if(paidTotal >= 500 && paidTotal < 1000){
			paidTotalHeight = "50px";
		}
		else if(paidTotal >= 0 && paidTotal < 500){
			paidTotalHeight = "40px";
		}

		if(finalTotal >= 5000){
			paidFinalTotalHeight = "100px";
		}
		else if(finalTotal >= 4000 && finalTotal < 5000){
			paidFinalTotalHeight = "90px";
		}
		else if(finalTotal >= 3000 && finalTotal < 4000){
			paidFinalTotalHeight = "80px";
		}
		else if(finalTotal >= 2000 && finalTotal < 3000){
			paidFinalTotalHeight = "70px";
		}
		else if(finalTotal >= 1000 && finalTotal < 2000){
			paidFinalTotalHeight = "60px";
		}
		else if(finalTotal >= 500 && finalTotal < 1000){
			paidFinalTotalHeight = "50px";
		}
		else if(finalTotal >= 0 && finalTotal < 500){
			paidFinalTotalHeight = "40px";
		}

		
		if(reserveTotal >= 5000){
			reserveTotalHeight = "100px";
		}
		else if(reserveTotal >= 4000 && reserveTotal < 5000){
			reserveTotalHeight = "90px";
		}
		else if(reserveTotal >= 3000 && reserveTotal < 4000){
			reserveTotalHeight = "80px";
		}
		else if(reserveTotal >= 2000 && reserveTotal < 3000){
			reserveTotalHeight = "70px";
		}
		else if(reserveTotal >= 1000 && reserveTotal < 2000){
			reserveTotalHeight = "60px";
		}
		else if(reserveTotal >= 500 && reserveTotal < 1000){
			reserveTotalHeight = "50px";
		}
		else if(reserveTotal >= 0 && reserveTotal < 500){
			reserveTotalHeight = "40px";
		}

		//<div><span class="vbar" style="height:80px;">$1200</span><span class="vbarLabel">open</span></div>
		html += '<div><span class="vbar" style="height:' + paidTotalHeight + '">$' + paidTotal + '</span><span class="vbarLabel">paid</span></div>';
		html += '<div><span class="vbar" style="height:' + paidFinalTotalHeight + '">$' + finalTotal + '</span><span class="vbarLabel">final</span></div>';
		html += '<div><span class="vbar" style="height:' + reserveTotalHeight + '">$' + reserveTotal + '</span><span class="vbarLabel">reserve</span></div>';

		barContainer.addClass('center');
		barContainer.html(html);


	}

	function removeDollarSignForChart(cost){
    	return cost.replace(/\$|,/g, "");
	}
	
</script>

@endsection

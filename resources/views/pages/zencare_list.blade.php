@extends('layouts.zen5_layout')
@section('maincontent')


<div class="pageContent bgimage-bgheader pagebackground2 fade-in">
		<!--**********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>all injury claims</span>
                </div>
                <div class="pageHeader__subTitle">
				<span class="icon"><img src="/images/icons/magnify.png"></span><span>manage all of your <b>injury claims</b><button class="setFavorite"></button></span>
                </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/injurylist.png">
            </div> 
        </div>           
	</div>
	<div class="contentBlock noPad">
		<div class="container noPad">
			
			<!-- <div class="buttonArray noBorder">
				<div class="buttonSet inline">
					<button class="cyan" onclick="location.href='/newinjury';"><div class="icon icon-plus"></div>new injury</button>
				</div>
			</div> -->

            <div id="noInjuriesDiv" style="display:none">
                <h3>{{Auth::user()->getCompany()->company_name}} has no reported claims! You can use the "New Injury" button to report a claim.
                    Remember, you can also use it to track claims that are already in progress! </h3>
            </div>

			<div class="zen-actionPanel noPad noHover" id="zencareList" style="min-height:420px;margin-bottom:40px;margin-top:40px;">
				<div class="zenListTable">
					<table>
						<tbody>
						<tr class="tableHeadings">
							<td></td>
							<td class="name"><button class="sorter showIconModal">name</button></td>
							<td class="injurydate"><button class="sorter showIconModal">date</button></td>
							<td class="reserve"><button class="sorter showIconModal">reserve $</button></td>
							<td class="current"><button class="sorter showIconModal">paid $</button></td>
							<td class="severity"><button class="sorter showIconModal">severity</button></td>
							<td class="claim"><button class="sorter showIconModal">claim #</button></td>
                            <td class="progress"><button class="sorter showIconModal">progress</button></td>
							<!-- <td class="mood"><button class="sorter showIconModal">mood</button></td> -->
                            <td class="resolved"><button class="sorter showIconModal">status</button></td>
							<td class="recordEdit"></td>
						</tr>

						</tbody>
					</table>
				</div>
				<div class="pagination">
					<button class="pagePrev ghost">prev</button>
					<div class="pages" id="pageInfoSpan">showing 1 of 6</div>
					<button class="pageNext ">next</button>
				</div>							
			</div>	
		</div>
	</div>
	<div style="height:40px;"></div>
</div>
<div id="zencareListModal" class="zModal" style="display:none">
    <!-- CONTENT FOR claimNumberModal -->
    <div class="modalContent modalBlock" id="claimNumberModal" style="display: none;">
        <div class="modalHeader"><span class="modalTitle">Add Claim Number</span></div>
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <section class="sectionPanel">
                    <span class="sectionTitle"><div class="icon icon-commenting"></div><span style="opacity:.5">Add the Claim Number</span></span>
                    <div class="sectionContent">
                        <section class="formBlock dark">
                            <div class="formGrid">
                                <div class="formInput">
                                    <label for="claimNumberInput">Claim Number</label>
                                    <input id="claimNumberInput" name="claimNumberInput">
                                    <span class="inputError"></span>
                                </div>
                                <div class="formInput">
                                    <label for="confirmClaimNumberInput">Confirm Claim Number</label>
                                    <input id="confirmClaimNumberInput" name="confirmClaimNumberInput">
                                    <span class="inputError"></span>
                                </div>
                                <input type="hidden" id="injuryNumber">
                            </div>
                        </section>
                        <button id="submitClaimNumber">Submit</button>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

	<script>

        $('.zenListTable').on('click', '.showZencareListModal', function(e){
            e.stopPropagation();
            //initialize the modal
            modal = new jBox('Modal', {
                addClass: 'zBox',
                closeButton: false,
                //modal ID goes here
                content: $('#zencareListModal'),
                isolateScroll: true
            });
            //get the content for the modal
            var target = $(this).data('modalcontent');
            //hide all content blocks
            $('.modalBlock').hide();
            //show the target
            $(target).show();
            
            if(target === "#claimNumberModal"){
                var injuryId = $(this).data('injuryid');
                $('#injuryNumber').val(injuryId);
            }
            //modal ID goes here
            modal.open();
        });

        function showList(injuries){
            if(injuries.length === 0){
                $('#zencareList').hide();
                $('#noInjuriesDiv').show();
            }
        }

        var default_status = '<?php echo $claim_status; ?>';

		$('.zenListTable').on('click', '.sorter', function(){
		    var type = $(this).parent().attr('class');
		    if($(this).hasClass('down')){
		        var sort = "up";
                $('.sorter').removeClass('down').removeClass('up');
		        $(this).removeClass('down').addClass('up');
			}else {
		        sort = "down";
                $('.sorter').removeClass('down').removeClass('up');
		        $(this).removeClass('up').addClass('down');
			}



			if(type === "name"){
                if(sort === "up"){
                    sortInjuriesByNameAsc(injuries);
                }else if(sort === "down"){
                    sortInjuriesByNameDesc(injuries);
                }
			}else if(type === "injurydate"){
                if(sort === "up"){
                    sortByDateAsc(injuries);
                }else if(sort === "down"){
                    sortByDateDesc(injuries);
                }
			}else if(type === "reserve"){
                if(sort === "up"){
                    sortByReserveCostAsc(injuries);
                }else if(sort === "down"){
                    sortByReserveCostDesc(injuries);
                }
			}else if(type === "current"){
                if(sort === "up"){
                    sortByCurrentCostAsc(injuries);
                }else if(sort === "down"){
                    sortByCurrentCostDesc(injuries);
                }
			}else if(type === "severity"){
                if(sort === "up"){
                    sortBySeverityAsc(injuries);
                }else if(sort === "down"){
                    sortBySeverityDesc(injuries);
                }
			}else if(type === "claim"){
                if(sort === "up"){
                    sortByClaimNumberAsc(injuries);
                }else if(sort === "down"){
                    sortByClaimNumberDesc(injuries);
                }
			}
            // else if(type === "mood"){
            //     if(sort === "up"){
            //         sortByMoodAsc(injuries);
            //     }else if(sort === "down"){
            //         sortByMoodDesc(injuries);
            //     }
			// }
            else if(type === "resolved"){
			    if(sort === "up"){
			        sortByStateAsc(injuries);
                }else if(sort === "down"){
			        sortByStateDesc(injuries);
                }
            }
            else if(type === "progress"){
			    if(sort === "up"){
			        sortByProgressAsc(injuries);
                }else if(sort === "down"){
			        sortByProgressDesc(injuries);
                }
            }

		});

		$('.zenListTable').on('click', '.injuryRow', function(){

		   var id = $(this).data('id');
		   window.location.href = "/zencare/" + id;
		});

        function setButtonClasses(){
            if(current_page === 1){
                //first page
                $('.pagePrev').attr('class', 'pagePrev ghost');
                $('.pageNext').attr('class', 'pageNext');
            }else if(current_page === page_count){
                //last page
                $('.pagePrev').attr('class', 'pagePrev');
                $('.pageNext').attr('class', 'pageNext ghost');
            }else{
                //middle pages
                $('.pagePrev').attr('class', 'pagePrev');
                $('.pageNext').attr('class', 'pageNext');
            }
        }

        $('.pagePrev').on('click', function(){
            if(!$(this).hasClass('ghost')){
                var cur_page = current_page;
                cur_page = cur_page - 1;
                if(cur_page > 0){
                    current_page = cur_page;
                    buildTableNew(false);
                }
                setButtonClasses();
			}
        });

        $('.pageNext').on('click', function(){
            if(!$(this).hasClass('ghost')){
                var cur_page = current_page;
                cur_page = cur_page + 1;
                if(cur_page <= page_count){
                    current_page = cur_page;
                    buildTableNew(false);
                }
                setButtonClasses();
			}
        });

		var injuries = null;
		var page_count = 0;
		var current_page = 1;
		var number_per_page = 10;
		var max_page_number = 0//Math.round(count(injuries) / 10);

		function getInjuryList(){
		    $.ajax({
				type: 'POST',
				url: '<?php echo route('getInjuryList'); ?>',
				data: {
				    _token: '<?php echo csrf_token(); ?>',
					company_id: '<?php echo Auth::user()->getCompany()->id ?>'
				},
				success: function(data){
				    injuries = data;
                    if(injuries.length <= number_per_page){
                        $('.pageNext').addClass('ghost');
                    }else{
                        $('.pageNext').removeClass('ghost');
                    }
				    console.log('injuries: ' + injuries);
				    if(default_status === "open"){
				        sortByStateDesc(injuries);
                    }else if(default_status === "resolved"){
				        sortByStateAsc(injuries);
                    } else {
				        sortByStateDesc(injuries);
                    }

                    showList(injuries);
				    buildTableNew(true);
				},
                error: function(jqxhr, status, exception){
                    console.log(jqxhr);
                }
			});
		}

		getInjuryList();


		function buildTableNew(revert_to_page_one){
            var i = 0;
            var l = injuries.length;
            var html = "";
            var valid = true;
            var colspan = getColspanValue();
            //SEARCH
			//TODO: HOOK UP SEARCH
			var search_string = "";
            //var search_string = $("#claimSearchBar").val();
            var results_array = [];
            if(search_string !== ""){
                for(i = 0; i < l; i++){
                    var show_this_row = false;
                    if(injuries[i]['user_name'] !== null && injuries[i]['user_name'].toLowerCase().indexOf(search_string.toLowerCase()) !== -1){
                        show_this_row = true;
                    }

                    if(injuries[i]['injury_date'] !== null && injuries[i]['injury_date'].indexOf(search_string) !== -1){
                        show_this_row = true;
                    }

                    if(injuries[i]['claim_number'] !== null && injuries[i]['claim_number'].indexOf(search_string) !== -1){
                        show_this_row = true;
                    }
                    if(show_this_row === true){
                        results_array.push(injuries[i]);
                    }
                }

            }else{
                results_array = injuries;
            }

            //console.log(results_array);

            //PAGINATION
            if(revert_to_page_one === true){
                current_page = 1;
            }
            var results_count = results_array.length;
            //calculate how many results to show per page
            var page_length = number_per_page;
            //if there are less results than the default number_per_page, set it to the results count
            if(page_length > results_count){
                page_length = results_count;
            }

            //i will contain the number of messages needed to skip through to get to the current page
            //for example, if on page 2, we'll want to skip to the 11th message to display it
            //so i will equal 10 if on the 2nd page
            i = 0;
            for (var j = 1; j < current_page; j++){
                //add the number shown per page to i each time we advance a page
                i = i + number_per_page;
                //add to the page length
                page_length = page_length + number_per_page;
                //make sure page_length doesn't exceed the number of messages
                if(page_length > results_count){
                    page_length = results_count;
                }
            }

            page_count = Math.ceil(results_count / number_per_page);
			$('#pageInfoSpan').html("page " + current_page + " of " + page_count);
            //console.log(page_count + " = " + results_count + " / " + number_per_page );



            /*
            <li class="visibilityToggle" id="dateVisibilityToggle" data-target="date"><a>date</a></li>
                <li class="visibilityToggle" id="currentVisibilityToggle" data-target="currentcost"><a>current</a></li>
                <!--<li id="costVisibilityToggle"><a>cost</a></li>-->
                <li class="visibilityToggle" id="reserveVisibilityToggle" data-target="reservecost"><a>reserve</a></li>
                <!--<li id="costVisibility" class="hide"><a>cost</a></li>-->
                <li class="visibilityToggle" id="severityVisibilityToggle" data-target="severity"><a>severity</a></li>
                <li class="visibilityToggle" id="claimnumberVisibilityToggle" data-target="claimnumber"><a>claim #</a></li>
                <li class="visibilityToggle" id="statusVisibilityToggle" data-target="employeestatus"><a>status</a></li>
                <li class="visibilityToggle" id="moodVisibilityToggle" data-target="mood"><a>mood</a></li>
                <li class="visibilityToggle" id="stateVisibilityToggle" data-target="state"><a>state</a></li>

                */
            /*
            var show_date = "";

            if($('#dateVisibilityToggle').hasClass('hide')){
                show_date = "style='display: none'";
            }

            var show_current_cost = "";

            if($('#currentVisibilityToggle').hasClass('hide')){
                show_current_cost = "style='display: none'";
            }

            var show_reserve_cost = "";

            if($('#reserveVisibilityToggle').hasClass('hide')){
                show_reserve_cost = "style='display: none'";
            }

            var show_severity = "";

            if($('#severityVisibilityToggle').hasClass('hide')){
                show_severity = "style='display: none'";
            }

            var show_claim_number = "";

            if($('#claimnumberVisibilityToggle').hasClass('hide')){
                show_claim_number = "style='display: none'";
            }

            var show_status = "";

            if($('#statusVisibilityToggle').hasClass('hide')){
                show_status = "style='display: none'";
            }

            var show_mood = "";

            if($('#moodVisibilityToggle').hasClass('hide')){
                show_mood = "style='display: none'";
            }

            var show_state = "";

            if($('#stateVisibilityToggle').hasClass('hide')){
                show_state = "style='display: none'";
            }
            */


            var heading_html = $('.tableHeadings').prop('outerHTML');
            console.log(heading_html);
            html = heading_html;

            for(i; i < page_length; i++){
                //claim number
                console.log(results_array);
                var claim_number = results_array[i].claim_number;
                if(claim_number === null){
                    claim_number = "<button class='showZencareListModal red' data-modalcontent='#claimNumberModal' data-injuryId='"+results_array[i].id+"'>-enter #-</button>";
                }

                //mood

                //current_cost
                var current_cost = results_array[i].current_cost;
                if(current_cost === 0 || current_cost === null){
                    current_cost = "$-enter cost-";
                }else{
                    current_cost = "$" + current_cost;
                }

                //reserve_cost
                var reserve_cost = results_array[i].reserve_cost;
                if(reserve_cost === 0){
                    reserve_cost = "$-enter cost-";
                }else{
                    reserve_cost = "$" + reserve_cost;
                }

                //mood
                var mood = results_array[i].mood;
                if(mood === null){
                    mood = "unknown";
                }else{
                    mood = "lv" + mood;
                }

                var mood_label = results_array[i].mood;
                if(mood_label === null){
                    mood_label = "-";
				}
                var mood_color = "";
                var mood_string ='<div class="moodVisual scale60"><div class="currentMood"></div></div>';

                if(mood_label === '10'){
                    mood_label = "10 - Awesome";
                    mood_color = "mood-10";
                }else if(mood_label === '9'){
                    mood_label = "9 - Terrific";
                    mood_color = "mood-9";
                }else if(mood_label === '8'){
                    mood_label = "8 - Great";
                    mood_color = "mood-8";
                }else if(mood_label === '7'){
                    mood_label = "7 - Good";
                    mood_color = "mood-7";
                }else  if(mood_label === '6'){
                    mood_label = "6 - Ok";
                    mood_color = "mood-6";
                }else if(mood_label === '5'){
                    mood_label = "5 - Alright";
                    mood_color = "mood-5";
                }else if(mood_label === '4'){
                    mood_label = "4 - Uncomfortable";
                    mood_color = "mood-4";
                }else if(mood_label === '3'){
                    mood_label = "3 - Not Good";
                    mood_color = "mood-3";
                }else if(mood_label === '2'){
                    mood_label = "2 - Unwell";
                    mood_color = "mood-2";
                }else if(mood_label === '1'){
                    mood_label = "1 - Bad";
                    mood_color = "mood-1";
                }else if(mood_label === '0'){
                    mood_label = "0 - Terrible";
                    mood_color = "mood-0";
                }else if(mood_label === "x"){
                    mood_label = "Dead";
                    mood_color = "mood-x";
                }else{
                    mood_label = "Unknown";
                    mood_color = "unknown";
                    mood_string = "-?-";
                }
				
                //claim status
                if(results_array[i].resolved === 1){
                    var status = "<div class='icon FG__green icon-checkheart' style='font-size:1.4rem;position:relative;top:.2rem;'></div>";
                }else{
                    var status = "<div class='icon FG__red icon-plusheart' style='font-size:1.4rem;position:relative;top:.2rem;'></div>";
                }

                //employee status
                if(results_array[i].employee_status === "Hospital"){
                    var employee_status = "hospital";
                    var employee_status_title = "Hospital";
                }else if(results_array[i].employee_status === "Urgent Care"){
                    var employee_status = "urgentcare";
                    var employee_status_title = "Urgent Care";
                }else if(results_array[i].employee_status === "Family Practice"){
                    var employee_status = "doctor";
                    var employee_status_title = "Family Practice";
                }else if(results_array[i].employee_status === "Home"){
                    var employee_status = "home";
                    var employee_status_title = "Home";
                }else if(results_array[i].employee_status === "Light Duty"){
                    var employee_status = "lightduty";
                    var employee_status_title = "Light Duty";
                }else if(results_array[i].employee_status === "Full Duty"){
                    var employee_status = "fullduty";
                    var employee_status_title = "Full Duty";
                }else if(results_array[i].employee_status === "Maximum Medical Improvement"){
                    var employee_status = "max";
                    var employee_status_title = "Maximum Medical Improvement";
                }else if(results_array[i].employee_status === "Terminated"){
                    var employee_status = "terminated";
                    var employee_status_title = "Terminated";
                }else if(results_array[i].employee_status === "Resigned"){
                    var employee_status = "resigned";
                    var employee_status_title = "Resigned";
                }else{
                    var employee_status = "unknown";
                    var employee_status_title = "Unknown";
                }
				
                var severity_color = '';
                if(results_array[i].severity === "Mild"){
                    severity_color = "var(--severityMild)";

                }else if(results_array[i].severity === "Moderate"){
                    severity_color = "var(--severityModerate)";
                }else{
                    severity_color = "var(--severitySevere)";
                }

                var duration_color = '';
                if(results_array[i].days >= 30){
                    duration_color = 'critical';
                }else if(results_array[i].days >= 10 && results_array[i].days < 30){
                    duration_color = 'alert';
                }

				/*
				<tr>
							<td><div class="progressMeter small inline status5"><svg viewBox="0 0 100 100"><circle r="50%" cx="50%" cy="50%"></circle><div class="overlay"><div class="avatar"></div></div></svg></div></td>
							<td class="name">Henry Smith</td>
							<td class="date">12/23/2020</td>
							<td class="reserve">$2345</td>
							<td class="current">$5687</td>
							<td class="severity">mild</td>
							<td class="claim">3432354235</td>
							<td class="mood">5</td>
							<td><button class="smallIcon"></button></td>
						</tr>
						<tr class="tablerowspace"><td></td></tr>
				*/



				html = html + '<tr class="injuryRow" data-id=' + results_array[i].id +'>'
                            + '<td><div class="progressMeter '+duration_color+' small inline status'+results_array[i].status_number+'"><svg viewBox="0 0 100 100"><circle class="pm" r="50%" cx="50%" cy="50%"></circle><div class="overlay"><div class="avatar" style="background-image:url(' + results_array[i].user_pic_location +'); background-color: ' + results_array[i].user_avatar_color + '"></div></div></svg></div></td>'
							+ '<td class="name"><span>' + results_array[i].user_name + '</span></td>'
							+ '<td class="injurydate"><span>' + results_array[i].injury_date + '</span></td>'
							+ '<td class="reserve"><span>' + reserve_cost + '</span></td>'
							+ '<td class="current"><span>' + current_cost + '</span></td>'
							+ '<td class="severity"><span class="'+results_array[i].severity+'"></span></td>'
							+ '<td class="claim"><span>' + claim_number + '</span></td>'
                            + '<td class="progress"><span>' + employee_status + '</span></td>'
							// + '<td class="mood myMood '+mood_color+'"><span>'+ mood_string +'</span></td>'
                            + '<td class="resolved"><span>' + status + '</span></td>' 
							+ '<td class="recordEdit"><button class="smallIcon"></button></td></tr>'
							+ '<tr class="tablerowspace"><td></td></tr>';




				/*
                html = html + '<tr class="treePageLink" data-injuryid="' + results_array[i].id + '">' +
                    '<td class="name" style="cursor:pointer;"><div class="avatarIcon" style="background-image:url(' + results_array[i].user_pic_location + ')"></div>' + results_array[i].user_name + '</td>' +
                    '<td class="date" ' + show_date + '>' + results_array[i].injury_date + '</td>' +
                    '<td class="currentcost" ' + show_current_cost + '>' + current_cost + '</td>' +
                    '<td class="reservecost" ' + show_reserve_cost + '>' + reserve_cost + '</td>' +
                    '<td data-tooltip-simple data-title="' + results_array[i].severity + '" class="severity" ' + show_severity + '><span class="iconSeverity small ' + results_array[i].severity + '"></span</span></td>' +
                    '<td class="claimnumber" ' + show_claim_number + '>' + claim_number + '</td>' +
                    '<td data-tooltip-simple data-title="' + employee_status_title + '" class="employeestatus"' + show_status + '><span class="iconStatus small ' + employee_status + '"></span></td>' +
                    '<td data-tooltip-simple ' + mood_color + ' data-title="' + mood_label + '" class="mood"' + show_mood + '><span class="iconMood small ' + mood + '"></span></td>' +
                    '<td class="state"' + show_status + '>' + status + '</td>' +
                    '<td data-litigation="' + results_array[i].paused_for_litigation + '" class="edit" data-injuryid="' + results_array[i].id + '"><span class="iconStatus small pencil"></span></td>' +
                    '</tr>' +
                    '<tr><td colspan="' + colspan + '"><hr></td></tr>';
                */
            }

			$('tbody').html(html);
            //document.getElementById('tableBody').innerHTML = html;
			/*
            $('.currentPage').html(current_page);
            $('.totalPages').html(page_count);
            */
		}

        function sortInjuriesByNameDesc(injury_array){

            function compare(a, b) {
                if(a.user_name.toLowerCase() < b.user_name.toLowerCase()){
                    return -1;
                }
                if(a.user_name.toLowerCase() > b.user_name.toLowerCase()){
                    return 1;
                }

                return 0;
            }

            injury_array.sort(compare);
            buildTableNew(true);
        }

        function sortInjuriesByNameAsc(injury_array){

            function compare(a, b) {
                if(a.user_name.toLowerCase() < b.user_name.toLowerCase()){
                    return 1;
                }
                if(a.user_name.toLowerCase() > b.user_name.toLowerCase()){
                    return -1;
                }

                return 0;
            }

            injury_array.sort(compare);
            buildTableNew(true);
        }

        function sortByDateDesc(injury_array){
            function compare(a, b){
                var a = new Date(a.injury_date);
                var b = new Date(b.injury_date);
                if(a < b){
                    return -1;
                }
                if(a > b){
                    return 1;
                }
                return 0;
            }
            injury_array.sort(compare);
            buildTableNew(true);
        }

        function sortByDateAsc(injury_array){
            function compare(a, b){
                var a = new Date(a.injury_date);
                var b = new Date(b.injury_date);
                if(a < b){
                    return 1;
                }
                if(a > b){
                    return -1;
                }
                return 0;
            }
            injury_array.sort(compare);
            buildTableNew(true);
        }

        function sortByCurrentCostDesc(injury_array){
            function compare(a, b){
                if(a.current_cost < b.current_cost){
                    return -1;
                }
                if(a.current_cost > b.current_cost){
                    return 1;
                }
                return 0;
            }
            injury_array.sort(compare);
            buildTableNew(true);
        }

        function sortByCurrentCostAsc(injury_array){
            function compare(a, b){
                if(a.current_cost < b.current_cost){
                    return 1;
                }
                if(a.current_cost > b.current_cost){
                    return -1;
                }
                return 0;
            }
            injury_array.sort(compare);
            buildTableNew(true);
        }

        function sortByReserveCostDesc(injury_array){
            function compare(a, b){
                if(a.reserve_cost < b.reserve_cost){
                    return -1;
                }
                if(a.reserve_cost > b.reserve_cost){
                    return 1;
                }
                return 0;
            }
            injury_array.sort(compare);
            console.log(injury_array);
            buildTableNew(true);
        }

        function sortByReserveCostAsc(injury_array){
            function compare(a, b){
                if(a.reserve_cost < b.reserve_cost){
                    return 1;
                }
                if(a.reserve_cost > b.reserve_cost){
                    return -1;
                }
                return 0;
            }
            injury_array.sort(compare);
            console.log(injury_array);
            buildTableNew(true);
        }

        function sortBySeverityDesc(injury_array){
            function compare(a, b){
                if(a.severity < b.severity){
                    return -1;
                }
                if(a.severity > b.severity){
                    return 1;
                }
                return 0;
            }
            injury_array.sort(compare);
            console.log('sort by severity desc');
            console.log(injury_array);
            buildTableNew(true);
        }

        function sortBySeverityAsc(injury_array){
            function compare(a, b){
                if(a.severity < b.severity){
                    return 1;
                }
                if(a.severity > b.severity){
                    return -1;
                }
                return 0;
            }
            injury_array.sort(compare);
            console.log('sort by severity asc');
            console.log(injury_array);
            buildTableNew(true);
        }

        function sortByProgressDesc(injury_array){
            function compare(a, b){
                if(a.employee_status == null){
                    return -1;
                }
                else if(b.employee_status == null){
                    return 1;
                }
                else if(a.employee_status < b.employee_status){
                    return 1;
                }
                else if(a.employee_status > b.employee_status){
                    return -1;
                }
            }
            injury_array.sort(compare);
            console.log('sort by progress desc');
            console.log(injury_array);
            buildTableNew(true);
        }

        function sortByProgressAsc(injury_array){
            function compare(a, b){
                if(a.employee_status == null){
                    return 1;
                }
                else if(b.employee_status == null){
                    return -1;
                }
                else if(a.employee_status < b.employee_status){
                    return -1;
                }
                else if(a.employee_status > b.employee_status){
                    return 1;
                }
            }
            injury_array.sort(compare);
            console.log('sort by progress asc');
            console.log(injury_array);
            buildTableNew(true);
        }

        function sortByClaimNumberDesc(injury_array){
            function compare(a, b){
                a = a.claim_number;
                b = b.claim_number;
                if(a === null){
                    a = 0;
                }
                if(b === null){
                    b = 0;
                }
                if(a < b){
                    return -1;
                }
                if(a > b){
                    return 1;
                }
                return 0;
            }

            injury_array.sort(compare);
            console.log(injury_array);
            buildTableNew(true);
        }

        function sortByClaimNumberAsc(injury_array){
            function compare(a, b){
                a = a.claim_number;
                b = b.claim_number;
                if(a === null){
                    a = 0;
                }
                if(b === null){
                    b = 0;
                }
                if(a < b){
                    return 1;
                }
                if(a > b){
                    return -1;
                }
                return 0;
            }
            injury_array.sort(compare);
            console.log('injury array - ' + injury_array);
            buildTableNew(true);
        }

        function sortByStatusDesc(injury_array){
            function compare(a, b){
                if(a.status_number < b.status_number){
                    return -1;
                }
                if(a.status_number > b.status_number){
                    return 1;
                }
                return 0;
            }
            injury_array.sort(compare);
            buildTableNew(true);
        }

        function sortByStatusAsc(injury_array){
            function compare(a, b){
                if(a.status_number < b.status_number){
                    return 1;
                }
                if(a.status_number > b.status_number){
                    return -1;
                }
                return 0;
            }
            injury_array.sort(compare);
            buildTableNew(true);
        }

        function sortByMoodDesc(injury_array){
            function compare(a, b){
                if(a.mood < b.mood){
                    return -1;
                }
                if(a.mood > b.mood){
                    return 1;
                }
                return 0;
            }
            injury_array.sort(compare);
            buildTableNew(true);
        }

        function sortByMoodAsc(injury_array){
            function compare(a, b){
                if(a.mood < b.mood){
                    return 1;
                }
                if(a.mood > b.mood){
                    return -1;
                }
                return 0;
            }
            injury_array.sort(compare);
            buildTableNew(true);
        }

        function sortByStateDesc(injury_array){
            function compare(a, b){
                if(a.resolved < b.resolved){
                    return -1;
                }
                if(a.resolved > b.resolved){
                    return 1;
                }
                return 0;
            }
            injury_array.sort(compare);
            buildTableNew(true);
        }

        function sortByStateAsc(injury_array){
            function compare(a, b){
                if(a.resolved < b.resolved){
                    return 1;
                }
                if(a.resolved > b.resolved){
                    return -1;
                }
                return 0;
            }
            injury_array.sort(compare);
            buildTableNew(true);
        }

        //use this function to deterimine what the colspan for filler table rows should be, based on how many columns are currently visible
        function getColspanValue(){
            var count = 2;
            $('.visibilityToggle').each(function(){
                if(!$(this).hasClass('hide')){
                    count++;
                }
            });
            return count;
        }

        function getInjuryIndexById(id){
            for(var i = 0; i < injuries.length; i++){
                if(id == injuries[i].id){
                    return i;
                }
            }
            return 0;
        }

        $('#submitClaimNumber').on('click', function(){
            var claimNumber = $("#claimNumberInput").val();
            var claimNumberConfirm = $("#confirmClaimNumberInput").val();
            var injuryId = $('#injuryNumber').val();
            if(claimNumber !== claimNumberConfirm){
                alert('numbers do not match');
                alert('claim number: ' + claimNumber + ' confirm number: ' + claimNumberConfirm);
                return false;
            }
            if(injuryId === null || injuryId === ""){
                alert('no injury id');
                alert('injury id: ' + injuryId);
                return false;
            }

            $.ajax({
				type: 'POST',
				url: '<?php echo route('addClaimNumber'); ?>',
				data: {
				    _token: '<?php echo csrf_token(); ?>',
					claim_number: claimNumber,
                    claim_number_confirm: claimNumberConfirm,
                    injury_id: injuryId
				},
				success: function(data){
                    $(".injuryRow[data-id='" + injuryId +"']").find('.claim').html(claimNumber);
				    modal.close();
                    //window.location.href = "/zencare/" + injuryId;
				}
			});
        });
	</script>
	


@endsection

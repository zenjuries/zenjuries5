@extends('layouts.zen5_layout')
@section('maincontent')
    <?php
    $user = Auth::user();

    $sorted_companies = Auth::user()->getZenproCompanyListByAgency();
   // var_dump($sorted_companies);
    /*
    echo "<div class='agencyCompaniesDiv'>";
        echo "<h3>" . $agency . "</h3><br>";
            foreach($companies as $company){
                echo "<li class='companyListItem'><a href='/selectCompany/" . $company->id ."'>" . $company->company_name . "</a></li>";
            }
    echo "</div>";
}
    */
    $company = $user->getCompany();
    $company_logo_location = Auth::user()->getCompany()->logo_location;

    ?>

<div class="pageContent bgimage-bgheader pagebackground6 fade-in">		
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <div class="companyIcon header" id="zenproLandingLogo" style="background-image:url({{Auth::user()->getCompany()->logo_location}}?{{ $date = str_replace(' ', '', \Carbon\Carbon::now()) }})"></div>
                <span class="logoTitle"><b>{{Auth::user()->getCompany()->company_name}}</b></span>
                </div>
                <div class="pageHeader__subTitle">
                    <span class="icon"><img src="images/icons/user.png"></span><span class="subContent">logged in as <b>{{ $user->name }}</b></span>
                </div>
            </div>
            <div class="pageIcon animate__animated animate__bounceInRight">
                <img src="/images/icons/zenpro2.png">
            </div> 
        </div>           
	</div>
    <div class="container noPad">

        <!--WATCH LIST-->
        <div class="flexPanel">
            <div class="viewControl">
                <div class="compactViewLeft animate__animated animate__headShake">
                    <span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/magnify.png"> my watchlist</span>
                </div>
                <div class="compactViewExpand">[ view watchlist ]</div>
                <div class="compactViewRight animate__animated animate__fadeInRight animDelay4">
                    <ul>
                        <li><span class="icon"><img src="images/icons/injury-alert.png"></span><span id="watchList__alert" class="value FG__alert">0</span></li>
                        <li><span class="icon"><img src="images/icons/injury-critical.png"></span><span id="watchList__critical" class="value FG__critical">0</span></li>
                    </ul>
                    <div class="compactViewX">[ minimize ]</div>
                </div>
            </div>
            <div class="viewContent">
                <div class="zen-actionPanel transparent tight">
                    <div class="sectionContent">
                        <table class="watchlistTable zenListTable tight" id="caseload2">
                            <tbody id="myWatchListDropdown">
                                <tr class="watchlistHeadings tableHeadings">
                                    <td></td>
                                    <td class="name"><button class="watchlistSorter sorter showIconModal">name</button></td>
                                    <td class="date"><button class="watchlistSorter sorter showIconModal">date</button></td>
                                    <td class="severity"><button class="watchlistSorter sorter showIconModal">severity</button></td>
                                    <td class="total"><button class="watchlistSorter sorter showIconModal">total$</button></td>
                                    <td class="progress"><button class="watchlistSorter sorter showIconModal">progress</button></td>
                                    <td class="mood"><button class="watchlistSorter sorter showIconModal">mood</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>					
                </div>
                <div class="pagination">
                    <button id="prevMyWatchList" class="pagePrev ghost">prev</button>
                    <div class="pages" id="myWatchListpageInfoSpan">showing 1 of 6</div>
                    <button id="nextMyWatchList" class="pageNext ">next</button>
                </div>
            </div>				
        </div>

        <div class="flexPanel">
				<div class="viewControl">
					<div class="compactViewLeft animate__animated animate__bounceInLeft">
						<span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/insurance.png"> my caseload</span>
					</div>
					<div class="compactViewExpand"><span>[ view caseload ]</span></div>
					<div class="compactViewRight animate__animated animate__bounceInRight">
                        <!--
						<ul>
							<li><span class="icon"><img src="images/icons/injury-ok.png"></span><span id="caseload__ok" class="value FG__okblue">0</span></li>
							<li><span class="icon"><img src="images/icons/injury-alert.png"></span><span id="caseload__alert" class="value FG__alert">0</span></li>
							<li><span class="icon"><img src="images/icons/injury-critical.png"></span><span id="caseload__critical" class="value FG__critical">0</span></li>
						</ul>
                        -->
						<div class="compactViewX">[ minimize ]</div>
					</div>
				</div>
				<div class="viewContent">
					<div class="zen-actionPanel transparent tight">
						<div class="sectionContent dynamicSize">
							<table class="caseloadTable zenListTable tight">
								<tbody id="caseloadInjuryDropdown">
								    <tr id="myCaseListHeading" class="tableHeadings">
										<td></td>
										<td class="name"><button class="caseloadSorter sorter showIconModal">name</button></td>
										<td class="injurydate"><button class="caseloadSorter sorter showIconModal">date</button></td>
										<td class="severity"><button class="caseloadSorter sorter showIconModal">severity</button></td>
										<td class="claimNumber"><button class="caseloadSorter sorter showIconModal">claim number</button></td>
                                        <td class="policyholder"><button class="caseloadSorter sorter showIconModal">policyholder</button></td>
										<td class="mood"><button class="caseloadSorter sorter showIconModal">mood</button></td>
									</tr>
									<!-- replace with .partial content
									<tr>
										<td><div class="progressMeter small inline status7 critical"><svg viewBox="0 0 100 100"><circle class="pm" r="50%" cx="50%" cy="50%"></circle><div class="overlay"><div class="avatar"></div></div></svg></div></td>
										<td class="name"><span>Henry Smith</span></td>
										<td class="date"><span>12/23/2020</span></td>
										<td class="severity"><span>mild</span></td>
										<td class="agency"><span>an agency</span></td>
										<td class="policyholder"><span>Some company</span></td>
										<td><div class="moodVisual smaller"><div class="currentMood"></div></div></td>
									</tr>
									<tr class="tablerowspace"><td></td></tr>-->
									
								</tbody>
							</table>
						</div>					
					</div>
                    <div class="pagination">
                        <button class="mycaseload pagePrev ghost">prev</button>
                        <div class="pages" id="myCaseloadpageInfoSpan">showing 1 of 6</div>
                        <button class="mycaseload pageNext">next</button>
                    </div>						
				</div>
			</div>

        <div class="divider">assigned policyholders</div>
        <!--
            since we don't know how many companies each agency has the whole caseload is being build by the partial
            and then will be added to the caseloadContainer
        -->
       <!--ACTIVE COMPANY 1--

       I kept the active company 1 here just for future reference

        <div class="flexPanel">
            <div class="viewControl">
                <div class="compactViewLeft animate__animated animate__headShake">
                    <span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/company-assigned.png"> agency1 name</span>
                </div>
                <div class="compactViewExpand">[ expand ]</div>
                <div class="compactViewRight animate__animated animate__fadeInRight animDelay8">
                    <ul>
                        <li><div class="setToolTip inline"><span class="toolTip">total<br>open</span><span class="icon"><img src="images/icons/heart-simple.png"></span><span id="active__open" class="value FG__green">0</span></div></li>
                        <li><div class="setToolTip inline"><span class="toolTip">total<br>critical</span><span class="icon"><img src="images/icons/time-crit.png"></span><span id="active__critical" class="value FG__critical">0</span></div></li>
                    </ul>
                    <div class="compactViewX">[ minimize ]</div>
                </div>
            </div>
            <div class="viewContent">
                <div class="zen-actionPanel transparent tight">
                    <div class="sectionContent dynamicSize-nopad">
                        <table class="watchlistTable zenListTable tight" id="">
                            <tbody id="myWatchListDropdown">
                                <tr class="watchlistHeadings tableHeadings">
                                    <td></td>
                                    <td class="name"><button class="watchlistSorter sorter showIconModal">name</button></td>
                                    <td class="numvalue"><button class="watchlistSorter sorter showIconModal">open</button></td>
                                    <td class="numvalue"><button class="watchlistSorter sorter showIconModal">critical</button></td>
                                    <td class="policyNum"><button class="watchlistSorter sorter showIconModal">policy #</button></td>
                                    <td class="itemdate"><button class="watchlistSorter sorter showIconModal">renewal</button></td>
                                </tr>
                                <!-- replace content --
                                <tr>
                                    <td class='companyLogo'><div class='companyIcon' style="background-image:url('/images/default_logos/company1.png');"></div></td>
                                    <td class="name"><span>New Company Name</span></td>
                                    <td class="numvalue"><span class="FG__green">23</span></td>
                                    <td class="numvalue"><span class="FG__red">4</span></td>
                                    <td class="policyNum"><span>34rt45h4</span></td>
                                    <td class="itemdate"><span>2/12/2023</span></td>
                                </tr>
                                <tr class="tablerowspace"><td></td></tr>
                                <tr>
                                    <td class='companyLogo'><div class='companyIcon' style="background-image:url('/images/default_logos/company6.png');"></div></td>
                                    <td class="name"><span>New Company Name</span></td>
                                    <td class="numvalue"><span class="FG__green">23</span></td>
                                    <td class="numvalue"><span class="FG__red">4</span></td>
                                    <td class="policyNum"><span>34rt45h4</span></td>
                                    <td class="itemdate"><span>2/12/2023</span></td>
                                </tr>
                                <tr class="tablerowspace"><td></td></tr>                                
                                <!-- end replace content --                               
                            </tbody>
                        </table>
                    </div>					
                </div>
            </div>				
        </div>
-->
        <div id="tableContainer"></div>

    </div> 
   
    
	@include('partials.myCaseLoad')
    <script>
         var companies = JSON.parse('<?php echo json_encode($sorted_companies, JSON_HEX_APOS); ?>');
        //console.log("Companies: " + companies);
        buildTableArray();
        //THIS IS THE OVERALL FUNCTION THAT LOOPS THROUGH ALL THE COMPANIES AND BUILDS THE MAIN HTML BLOCK
        function buildTableArray(){
            var html = "";
            var i = 0;
            console.log('Companies length: ' + Object.keys(companies).length);
            for(i; i < Object.keys(companies).length; i++){
                //console.log(companies[i]);
                html+= buildAgencyTable(companies[i]);
            }

            //console.log("html before tablecontainer: " + html);
            $('#tableContainer').html(html);
           // console.log("after table container: " + html);
        }


        //THIS FUNCTION CREATES THE HTML FOR EACH AGENCY BLOCK, THIS IS WHAT YOU'LL BE CHANGING
        function buildAgencyTable(agency){
            console.log(agency[0].agency_name);

            //get the agency_name var
            if(agency[0].agency_name === ""){
                var agency_name = "Independent Companies";
            }else{
                var agency_name = agency[0].agency_name;
            }
            var randomCompanyImage = Math.floor(Math.random() * 9) + 1;
                var html = '<div class="flexPanel">'+
                            '<div class="viewControl">'+
                                '<div class="compactViewLeft animate__animated animate__headShake">'+
                                    '<span class="sectionTitle wide"><img class="sectionIcon" <img src="/images/icons/company-assigned.png" >  '+agency_name+'</span>'+
                                '</div>'+
                                '<div class="compactViewExpand">[ expand ]</div>'+
                                '<div class="compactViewRight animate__animated animate__fadeInRight animDelay8">'+
                                    '<ul>'+
                                    '</ul>'+
                                '<div class="compactViewX">[ minimize ]</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="viewContent">'+
                                '<div class="zen-actionPanel transparent tight">'+
                                    '<div class="sectionContent dynamicSize-nopad">'+
                                        '<table class="watchlistTable zenListTable tight" id="">'+
                                            '<tbody id="myWatchListDropdown">'+
                                                '<tr class="watchlistHeadings tableHeadings">'+
                                                    '<td></td>'+
                                                    '<td class="name"><button class="watchlistSorter sorter showIconModal">name</button></td>'+
                                                    '<td class="numvalue"><button class="watchlistSorter sorter showIconModal">open</button></td>'+
                                                '</tr>';
            
            /*
            //generate the html for each section with the agency name; aside from agency_name this part is static but repeated
            var html = '<section class="sectionPanel">' +
            '               <span class="sectionTitle"><div class="icon icon-company">' + agency_name + '</div></span>' +
            '               <div class="zen-actionPanel transparent tight">' +
            '                   <div class="sectionContent">' +
            '                       <table class="zenListTable tight" id="[companynameID]">' +        
            '                           <tbody>' +
            '                           <tr class="tableHeadings">' +
            '                               <td></td>' +
            '                                <td><button class="sorter showIconModal">name</button></td>' +
            '                                <td><button class="sorter showIconModal">open claims</button></td>' +
            '                                <td></td>' +
            '                           </tr>';
            */
            //this generates the html for each company row and is appended to the html var
            var i = 0;
            for(i; i < Object.keys(agency).length; i++){
                //console.log("angency " + Object.keys(agency));
                //console.log("angency id" + agency);
                //var row_html = "<tr class='hoverLink companyListRow' data-id=" + agency[i].id + "><td class='companyLogo'><div class='companyIcon'></div></td><td class='companyName'>" + agency[i].company_name + "</td><td class='openClaims'>" + agency[i].open_claims + "</td><td><button class='smallIcon'></button></td></tr><tr class='tablerowspace'><td></td></tr>";
                if(agency[i].logo_location === null || agency[i].logo_location === ""){
                    $.ajax({
                        type: 'POST',
                        url: '/createDefaultCompanyLogo',
                        data: {
                            _token: '<?php echo csrf_token(); ?>',
                            company_id: agency[i].id,
                            random_image_number: randomCompanyImage
                        }
                    });
                }


                if(agency[i].logo_location === null){
                    var row_html = '<tr class="hoverLink companyListRow" data-id=' + agency[i].id + '>'+
                                    '<td class="companyLogo"><div class="companyIcon" style=background-image:url("/images/default_logos/company' + randomCompanyImage + '.png")></div></td>'+
                                    '<td class="name"><span>'+ agency[i].company_name +'</span></td>'+
                                    '<td class="numvalue"><span class="FG__red">'+ agency[i].open_claims +'</span></td>'+
                                '</tr><tr class="tablerowspace"><td></td></tr>';
                }
                else{
                    var row_html = '<tr class="hoverLink companyListRow" data-id=' + agency[i].id + '>'+
                                    "<td class='companyLogo'><div class='companyIcon' style=\"background-image:url('" + agency[i].logo_location + "?{{ $date = str_replace(' ', '', \Carbon\Carbon::now()) }}" + "');\"></div></td>" +
                                    '<td class="name"><span>'+ agency[i].company_name +'</span></td>'+
                                    '<td class="numvalue"><span class="FG__red">'+ agency[i].open_claims +'</span></td>'+
                                '</tr><tr class="tablerowspace"><td></td></tr>';
                }
                html+= row_html;
                randomCompanyImage = Math.floor(Math.random() * 9) + 1;
            }
            //console.log("row_html: " + row_html);

            //this adds the closing table html
            html += "</table></div></div></div></div>";
            return html;

        }
        

        //buildTableArray();
        //this redirects to zenboard with the selected company
        $('#tableContainer').on('click', '.companyListRow', function(){
           var id = $(this).data('id');
           //console.log('id' + id);
           if (id != undefined){
              window.location.href = "/selectCompany/" + id;
            }
        });

        $('.sorter').on('click', function(){
            var type = (this).innerText;
            var sort;
            if($(this).hasClass('down')){
                sort = "up";
                $('.sorter').removeClass('down').removeClass('up');
                $(this).removeClass('down').addClass('up');
            }else {
                sort = "down";
                $('.sorter').removeClass('down').removeClass('up');
                $(this).removeClass('up').addClass('down');
            }
            if(type == "name"){
                if(sort == "up"){
                    sortByNameAsc(watchlist_injuries);
                }
                else if(sort === "down"){
                    sortByNameDesc(watchlist_injuries);
                }
            }
            /*
            else if(type == "open claims"){
                console.log("open claims clicked");
                if(sort == "up"){
                    sortByOpenClaimsAsc(watchlist_injuries);
                }
                else if(sort === "down"){
                    sortByOpenClaimsDesc(watchlist_injuries);
                }
            }
            */
        });

        function sortByNameAsc(injury_array){
            function compare(a, b) {
                if(a.company_name.toLowerCase() < b.company_name.toLowerCase()){
                    return 1;
                }
                if(a.company_name.toLowerCase() > b.company_name.toLowerCase()){
                    return -1;
                }
                return 0;
            }
            (watchlist_injuries).sort(compare);
            buildAgencyTable(true);
        }

        function sortByNameDesc(injury_array){
            function compare(a, b) {
                if(a.company_name.toLowerCase() < b.company_name.toLowerCase()){
                    return -1;
                }
                if(a.company_name.toLowerCase() > b.company_name.toLowerCase()){
                    return 1;
                }
                return 0;
            }
            (watchlist_injuries).sort(compare);
            buildAgencyTable(true);
        }

        /*
        function sortByOpenClaimsAsc(injury_array){
            function compare(a, b){
                if(a.current_cost + a.reserve_cost < b.current_cost + b.reserve_cost){
                    return 1;
                }
                if(a.current_cost + a.reserve_cost > b.current_cost + b.reserve_cost){
                    return -1;
                }
                return 0;
            }
            watchlist_injuries.sort(compare);
            buildMyWatchListable(true);
        }

        function sortByOpenClaimsDesc(injury_array){
            function compare(a, b){
                if(a.current_cost < b.current_cost){
                    return -1;
                }
                if(a.current_cost > b.current_cost){
                    return 1;
                }
                return 0;
            }
            watchlist_injuries.sort(compare);
            buildMyWatchListable(true);
        }
        */
        var thisRandomCompanyImage = Math.floor(Math.random() * 9) + 1;
        var company_logo_location =  JSON.parse('<?php echo json_encode($company_logo_location); ?>');
        if(company_logo_location === null || company_logo_location === ""){
            $.ajax({
                type: 'POST',
                url: '/createDefaultCompanyLogo',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    company_id: {{$company->id}},
                    random_image_number: thisRandomCompanyImage
                },
                success: function(data){
					$('#policyholderLogo').css('background-image', 'url("/images/default_logos/company' + thisRandomCompanyImage + '.png")');
                    $('#zenproLandingLogo').css('background-image', 'url("/images/default_logos/company' + thisRandomCompanyImage + '.png")');
                },
                error: function(data){
                    console.log(data);
                }
            });
        }


    </script>
@endsection
	


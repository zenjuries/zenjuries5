@extends('layouts.zen5_layout')
@section('maincontent')
<?php
    $brandCompany = env('COMPANY_NAME');
?>	
<div class="pageContent bgimage-bgheader pagebackground12 fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>{{ $brandCompany }} documents</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/edit.png"></span><span class="subContent">upload/download documents</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/documents.png">
            </div> 
        </div>           
	</div>

	<div class="contentBlock lessPadding">
		<div class="container setMinHeight">

        <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-folder"></div> {{ $brandCompany }} documents</span>

            <div class="sectionContent">
                <div class="documentList">
                    <ul>
                        <li><a href="/documents/global/Incident_Analysis_Form_A_Short.pdf" target="_blank">
                            <span class="documentIcon"><img src="/images/icons/pdf.png"></span>
                            <span class="documentTitle">Incident Analysis Form A Short</span>
                            <span class="documentDesc">(.pdf format) this is a desccription of the document.</span></a></li>
                        <li><a href="/documents/global/Incident_Analysis_Form_A_Short.docx" target="_blank">
                            <span class="documentIcon"><img src="/images/icons/docx.png"></span>
                            <span class="documentTitle">Incident Analysis Form A Short</span>
                            <span class="documentDesc">(.docx format) this is a desccription of the document.</span></a></li>                            
                        <li><a href="/documents/global/Refusal_of_Medical_Treatment_or_Observation.pdf" target="_blank">
                            <span class="documentIcon"><img src="/images/icons/pdf.png"></span>
                            <span class="documentTitle">Refusal of Medical Treatment or Observation</span>
                            <span class="documentDesc">(.pdf format) this is a desccription of the document.</span></a></li>
                        <li><a href="/documents/global/Refusal_of_Medical_Treatment_or_Observation.docx" target="_blank">
                            <span class="documentIcon"><img src="/images/icons/docx.png"></span>
                            <span class="documentTitle">Refusal of Medical Treatment or Observation</span>
                            <span class="documentDesc">(.docx format) this is a desccription of the document.</span></a></li>                            
                    </ul>
                </div>

                <div class="buttonArray" style="display:none;">
                    <button class="showZenModal" data-modalcontent="#uploadDocumentModal"><div class="icon icon-file-text"></div> upload document</button>
                </div>

            </div>
         </section>

         <section class="sectionPanel" style="display:none;">
            <span class="sectionTitle"><div class="icon icon-folder"></div> company documents</span>

            <div class="sectionContent">

                <div class="documentList">
                    <ul>
                        <li>
                            <span class="documentPlaceholder">No documents</span></li>
                    </ul>
                </div>
 
                <div class="buttonArray" style="display:none;">
                    <button class="showZenModal" data-modalcontent="#uploadDocumentModal"><div class="icon icon-file-text"></div> upload document</button>
                </div>                

            </div>
         </section>

         <!-- <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-folder"></div> uploaded documents</span>

            <div class="sectionContent">

                <div class="documentList">
                    <ul>
                        <li>
                            <span class="documentPlaceholder">No documents</span></li>
                    </ul>
                </div>
 
                <div class="buttonArray" style="display:none;">
                    <button class="showZenModal" data-modalcontent="#uploadDocumentModal"><div class="icon icon-file-text"></div> upload document</button>
                </div>                

            </div>
         </section>

         <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-folder"></div> personal documents</span>

            <div class="sectionContent">

                <div class="documentList">
                    <ul>
                        <li>
                            <span class="documentPlaceholder">No documents</span></li>
                    </ul>
                </div>
 
                <div class="buttonArray" style="display:none;">
                    <button class="showZenModal" data-modalcontent="#uploadDocumentModal"><div class="icon icon-file-text"></div> upload document</button>
                </div>                

            </div>
         </section> -->

		</div>
	</div>


</div>

<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
    @include('partials.modals.uploadDocument')         
</div>
<script>
    $('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>	


@endsection

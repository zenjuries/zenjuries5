@extends('layouts.zen5_layout')
@section('maincontent')
    <?php
        $agency = Auth::user()->getAgency();

        $agency_company = \App\Company::where('is_agency', 1)->where('agency_id', $agency->id)->first();

        if(!is_null($agency_company->pm_last_four)){
            $has_card = true;
        }else $has_card = false;

        $active_companies = $agency->getCompanies("activated");
        $pending_companies = $agency->getCompanies("pending");



    ?>

<div class="pageContent bgimage-bgheader bghaze-plum fade-in">		
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                	<span>Welcome <b>[agency name]</b>!</span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/company.png"></span><span class="subContent">manage your <b>policyholders</b></span>
                </div>
            </div>
            <div class="pageIcon animate__animated animate__bounceInRight">
                <img src="/images/icons/gears.png">
            </div> 
        </div>           
	</div>


    <div class="buttonArray inline">
        <button class="orange showZagentModal" data-modalcontent="#newClientModal">new client</button>
        <button class="blue showZagentModal" data-modalcontent="#editClientModal" id="buttonThree">button</button>
        <button class="grey toggleChat" id="buttonTwo">button</button>
    </div>
    <div style="height:10px;"></div>
	<div class="contentBlock noPad">
		{{-- <div class="container noPad"> --}}
        <div class="container noPad" id="tablecontainer">    
            <div class="itemGrid pad5">
                <div class="itemGrid__item half max">
                    <div class="zen-actionPanel noHover">
                        <div class="panelTitle full"><div class="icon inline icon-pencil-square-o"></div> active companies</div>
                        <div class="panelElement">
                            <table class="zenListTable tight" id="x1">
                                <tbody>
                                <tr class="tableHeadings">
                                    <td></td>
                                    <td><button class="sorter showIconModal">name</button></td>
                                    <td><button class="sorter showIconModal">open claims</button></td>
                                    <td><button class="sorter showIconModal">renewal date</button></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="companyLogo"><div class="companyIcon"></div></td>
                                    <td class="companyName">company</td>
                                    <td class="openClaims">34</td>
                                    <td class="date">03/10/2022</td>
                                    <td><button class="smallIcon zagentEditClientModal" data-modalcontent="#editClientModal"></button></td>
                                </tr>
                                <tr class="tablerowspace"><td></td></tr>
                                <tr>
                                    <td class="companyLogo"><div class="companyIcon"></div></td>
                                    <td class="companyName">company</td>
                                    <td class="openClaims">34</td>
                                    <td class="date">03/10/2022</td>
                                    <td><button class="smallIcon zagentEditClientModal" data-modalcontent="#editClientModal"></button></td>
                                </tr>                                
                                </tbody>
                            </table>
                            <div style="height:40px;"></div>
                        </div>
                    </div>			
                </div>

                <div id="tableContainer"></div>

                <div class="itemGrid__item half max">
                    <div class="zen-actionPanel noHover">
                        <div class="panelTitle full"><div class="icon inline icon-pencil-square-o"></div> pending companies</div>
                        <div class="panelElement">
                            <table class="zenListTable tight" id="x2">
                            <tbody>
                                <tr class="tableHeadings">
                                    <td></td>
                                    <td class="name"><button class="sorter showIconModal">name</button></td>
                                    <td class="injuries"><button class="sorter showIconModal">invited on</button></td>
                                    <td class="date"><button class="sorter showIconModal">owner email</button></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="companyLogo"><div class="companyIcon"></div></td>
                                    <td class="companyName">company</td>
                                    <td class="date">03/10/2022</td>
                                    <td class="email">email@emailaddress.com</td>
                                    <td><button class="smallIcon"></button></td>
                                </tr>
                                <tr class="tablerowspace"><td></td></tr>
                                <tr>
                                    <td class="companyLogo"><div class="companyIcon"></div></td>
                                    <td class="companyName">company</td>
                                    <td class="date">03/10/2022</td>
                                    <td class="email">email@emailaddress.com</td>
                                    <td><button class="smallIcon"></button></td>
                                </tr>
                                </tbody>
                            </table>
                            <div style="height:40px;"></div>
                        </div>
                    </div>			
                </div>
            </div>
            
                <section class="sectionPanel">
                    <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> use if needed, otherwise comment out.</span>
                    <div class="sectionContent">

                    </div>
                </section>
        </div>
    </div>
</div>
<div id="zagentModal" class="zModal" style="display:none">
    <!--ADD CLIENT MODAL-->
    <div class="modalContent modalBlock" id="newClientModal" style="display:none">
       <div class="modalHeader"><span class="modalTitle">Add Client</span></div>
       <div class="modalBody" style="min-width:260px;">
          <section class="sectionPanel">
             <span class="sectionTitle">
                <div class="icon icon-phone"></div>
                additional information <span style="opacity:.5">(not required for initial report)</span>
             </span>
             <div class="sectionContent">
                <section class="formBlock dark">
                   <div class="formGrid">
                      <div class="formInput">
                         <!-- input -->
                         <label for="companyName">Company Name</label>
                         <input id="companyName" type="text" />
                         <span class="inputError">required</span>
                      </div>
                      <div class="formInput">
                         <!-- input -->
                         <label for="contactName">Contact Name</label>
                         <input id="contactName" type="text" />
                         <span class="inputError">required</span>
                      </div>
                      <div class="formInput">
                         <!-- input -->
                         <label for="contactEmail">Contact Email</label>
                         <input id="contactEmail" type="text" />
                         <span class="inputError">required</span>
                      </div>
                      <div class="formInput">
                         <!-- input -->
                         <label for="renewalDay">Policy Renewal Date</label>
                         <input id="renewalMonth" type="text" placeholder="MM"/>
                         <input id="renewalDay" type="text" placeholder="DD"/>
                         <span class="inputError">required</span>
                      </div>
                      <div class="formInput">
                         <!-- input -->
                         <label for="premium">Work Comp Annual Premium</label>
                         <input  id="premium" type="text"/>
                         <span class="inputError">required (must be a number)</span>
                      </div>
                      <div class="formInput">
                         <!-- input -->
                         <label for="upload">Upload Work Comp Policy</label>
                         <input  id="file" type="file"/>
                         <span class="inputError">required</span>
                      </div>
                      <span id="finalCostSpan" style="display: none">The cost for this account will be $<span id="finalCostTarget"></span>.</span>
                      <div class="sectionInfo">Who will pay for this account?
                         <span class="inputError" style="top:5px;left:0px;" id="whoPaysError">required</span>
                      </div>
                      <div class="formGrid short" id="whoPaysDiv">
                         <div class="formInput">
                            <input type="radio" name="whoPaysButton" id="agentPays">
                            <label for="whoPaysButton">Agency</label>
                         </div>
                         <div class="formInput">
                            <input type="radio" name="whoPaysButton" id="clientPays">
                            <label for="whoPaysButton">Client</label>
                         </div>
                      </div>
                      <div id="paymentDiv" style="display: none">
                         <input type="hidden" id="hasCard" value="1}">
                         <div class="sectionInfo">You don't have a payment method on file. <a href="/paymentMethod">Click here</a> to add a card, or choose from an option below.
                            <span class="inputError" style="top:5px;left:0px;" id="paymentMethodError">required</span>
                         </div>
                         <div class="formGrid short" id="paymentMethodDiv">
                            <div class="formInput">
                               <input type="radio" name="useCheck" id="useCheck">
                               <label for="useCheck">Pay With Check</label>
                            </div>
                         </div>
                      </div>
                      <div class="formElement">
                         <button id="submitNewClient" class="green">submit new client</button>
                      </div>
                   </div>
                </section>
             </div>
          </section>
       </div>
    </div>
    <!--EDIT CLIENT MODAL-->
    <div class="modalContent modalBlock" id="editClientModal" style="display:none">
       <div class="modalHeader"><span class="modalTitle">Title</span></div>
       <div class="modalBody" style="min-width:260px;">
          <div class="modalContent">
             <section class="sectionPanel">
                <span class="sectionTitle">
                   <div class="icon icon-phone"></div>
                   additional information <span style="opacity:.5">(not required for initial report)</span>
                </span>
                <div class="sectionContent">
                </div>
             </section>
          </div>
       </div>
    </div>
 </div>
   

    <script>
        //modal globalVar to contain the reference to the modal 
        var modal;

        //THIS FUNCTION SHOULD BE easy to drop on a page, the class triggering the onclick and the modal name should be the main changes. 
        $(document).on('click', ".showZagentModal", function(){    
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            //modal ID goes here
            content: $('#zagentModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();

        //script related to a specific content block can be added like this
        //status modal
        if(target === "#contentBlock1"){
            //scrpit for contentBlock1 
        }
        modal.open();
        });
    

        $('#buttonOne').hide();
        $('#buttonTwo').hide();
        $('#buttonThree').hide();
        var active_companies =  JSON.parse('<?php echo json_encode($active_companies); ?>');
        var pending_companies = JSON.parse('<?php echo json_encode($pending_companies); ?>');

        console.log(active_companies);
        console.log(pending_companies);

        function buildActiveCompanies(){
            var html = '<tr class="tableHeadings">' +
                '            <td></td>' +
                '            <td><button class="sorter showIconModal">name</button></td>' +
                '            <td><button class="sorter showIconModal">open claims</button></td>' +
                '            <td><button class="sorter showIconModal">renewal date</button></td>' +
                '            <td></td>' +
                '        </tr>';

            var i = 0;
            for(i; i < active_companies.length; i++){
                html += "<tr><td class='companyLogo'><div class='companyIcon'></div></td><td class='companyName'></td>" +
                        "<td class='companyName'>" + active_companies[i].company_name + "</td>" +
                        "<td class='openClaims'>" + active_companies[i].open_injuries + "</td>" +
                        "<td class='date'>" + active_companies[i].renewal_date + "</td>" +
                        "<td><button class='smallIcon showZagentModal' data-modalcontent='#editClientModal'></button></td>" +
                        "<tr class='tablerowspace'><td></td></tr>"
                        "</tr>";
                console.log("html: " + html);    
            }
            
            $('#x1').find('tbody').html(html);
            console.log("final html: " + html);
        }

        function buildPendingCompanies(){
            var html = '<tr class="tableHeadings">' +
                '            <td></td>' +
                '            <td class="name"><button class="sorter showIconModal">name</button></td>' +
                '            <td class="injuries"><button class="sorter showIconModal">invited on</button></td>' +
                '            <td class="date"><button class="sorter showIconModal">owner email</button></td>' +
                '            <td></td>' +
                '        </tr>';
            var i = 0;
            for(i; i < pending_companies.length; i++){
                /*
                var invited_on = pending_companies[i].invite_resent_on;
                if(invited_on === null){
                    invited_on = pending_companies[i].created_at;
                }*/

                html += "<tr><td class='companyLogo'><div class='companyIcon'></div></td>" +
                        "<td class='companyName'>" + pending_companies[i].company_name + "</td>" +
                        "<td class='date'>" + pending_companies[i].invited_at + "</td>" +
                        "<td class='email'>" + pending_companies[i].owner.email + "</td>" +
                        "<td><button class='smallIcon showZagentModal' data-modalcontent='#editClientModal'></button></td>" +
                        "<tr class='tablerowspace'><td></td></tr>"
                        "</tr>";
            }
          
            $('#x2').find('tbody').html(html);
        }

        buildActiveCompanies();
        buildPendingCompanies();

        var premium_rate = '<?php echo $agency->premium_rate; ?>';

        $('#submitNewClient').on('click', function(){
           validateClient();
        });

        function validateClient(){
            var valid = true;
            $('.inputError').removeClass('show');
            console.log('validating');

            var company_name = $('#companyName').val();
            if(company_name === ""){
                valid = false;
                $('#companyName').parent().find('.inputError').addClass('show');
            }

            var contact_name = $('#contactName').val();
            if(contact_name === ""){
                valid = false;
                $('#contactName').parent().find('.inputError').addClass('show');
            }

            var contact_email = $('#contactEmail').val();
            if(contact_email === ""){
                valid = false;
                $('#contactEmail').parent().find('.inputError').addClass('show');
            }

            var renewal_month = $('#renewalMonth').val();
            if(renewal_month === ""){
                valid = false;
                $('#renewalMonth').parent().find('.inputError').addClass('show');
            }

            var renewal_day = $('#renewalDay').val();
            if(renewal_day === ""){
                valid = false;
                $('#renewalDay').parent().find('.inputError').addClass('show');
            }

            var premium = $('#premium').val();
            //TODO:remove non numeric non decimal characters
            //premium = premium.replace(/\D/g,'');
            if(premium === ""){
                $('#premium').parent().find('.inputError').addClass('show');
            }

            var file = $('#file').prop('files')[0];

            if($('#agentPays').is(':checked')){
                var client_pays = 0;
            }else if($('#clientPays').is(':checked')){
                var client_pays = 1;
            }else{
                console.log('no pays');
                valid = false;
                $('#whoPaysError').addClass('show');
                //TODO: validate no selection
            }

            if(client_pays === 0){
                var has_card = $('#hasCard').val();
                if($('#useCard').is(":checked")){
                    var payment_method = "card";
                }else if($('#useCheck').is(":checked")){
                    var payment_method = "check";
                }else{
                    console.log('no card');
                    valid = false;
                    $('#paymentMethodError').addClass('show');
                    //TODO: validate no selection
                }
            }else{
                var has_card = null;
                var payment_method = null;
            }

            if(valid){

                var formData = new FormData();
                formData.append('_token', '<?php echo csrf_token(); ?>');
                formData.append('company_name', company_name);
                formData.append('contact_name', contact_name);
                formData.append('email', contact_email);
                formData.append('premium', premium);
                formData.append('renewal_day', renewal_day);
                formData.append('renewal_month', renewal_month);
                formData.append('payment_method', payment_method);
                formData.append('client_pays', client_pays);
                formData.append('file', file);
                formData.append('agency_id', '<?php echo $agency->id; ?>');

                $.ajax({
                    type: 'POST',
                    url: '<?php echo route('newClient'); ?>',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(data){
                        clearNewClientModal();
                        refreshCompanies();

                    },
                    error: function(data){
                        //alert('error!');
                        console.log(data);
                        handleServerValidation(data);

                    }

                });
            }


        }

        function clearNewClientModal(){
            addClientModal.close();
            //TODO: CLEAR OUT DATA FROM MODAL
        }

        function handleServerValidation(data) {
            if(data['status'] === 422) {
                var errors = (data['responseJSON']['errors']);
                console.log(errors);
                if (errors['client_pays']) {
                    $('#whoPaysError').html(errors['client_pays']).addClass('show');
                }
                if (errors['company_name']) {
                    $('#companyName').parent().find('.inputError').html(errors['company_name']).addClass('show');
                }
                if (errors['contact_name']) {
                    $('#contactName').parent().find('.inputError').html(errors['contact_name']).addClass('show');
                }
                if (errors['email']) {
                    $('#contactEmail').parent().find('.inputError').html(errors['email']).addClass('show');
                }
                if (errors['payment_method']) {
                    $('#paymentMethodError').html(errors['payment_method']).addClass('show');
                }
                if (errors['premium']) {
                    $('#premium').parent().find('.inputError').html(errors['premium']).addClass('show');
                }
                if (errors['renewal_day']) {
                    $('#renewalDay').parent().find('.inputError').html(errors['renewal_day']).addClass('show');
                }
                if (errors['renewal_month']) {
                    $('#renewalMonth').parent().find('.inputError').html(errors['renewal_month']).addClass('show');
                }
                if (errors['file']) {
                    $('#file').parent().find('.inputError').html(errors['file']).addClass('show');
                }
            }else{
                //TODO:: handle server errors
            }
        }

        $('#whoPaysDiv').on('click', function(){
            if($('#agentPays').is(':checked')){
                $('#paymentDiv').show();
            }else{
                $('#paymentDiv').hide();
            }

        });

        $('#activeCompanyTable').on('click', '.activeCompanyRow', function(e){
           if($(e.target).hasClass('editCompany') || $(e.target).hasClass('smallIcon')){
               var url = "/company";
           }else {
               var url = "/zenboard";
           }
            var company_id = $(this).data('id');
            $.ajax({
                type: 'POST',
                url: '<?php echo route('selectCompanyPost'); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    company_id: company_id
                },
                success: function(data){
                    console.log('success');
                    window.location.href = url;
                },
                error: function(data){
                    console.log(data);
                }
            });

        });


        $('#premium, #renewalMonth, #renewalDay').on('focusout', function(){
           getPrice();
        });

        $('#premium').on('input', function(){
            getPrice();
        });

        function getPrice(){
            var premium = $('#premium').val();
            premium = premium.replace(/[^0-9.]/g, '');
            var renewal_day = $('#renewalDay').val();
            renewal_day = renewal_day.replace(/\D/g,'');
            var renewal_month = $('#renewalMonth').val();
            renewal_month = renewal_month.replace(/\D/g,'');
            if(renewal_day !== "" && renewal_month !== "" && premium !== ""){
                var total_cost = returnPrice(premium, renewal_day, renewal_month);
                $('#finalCostTarget').html(total_cost);
                $('#finalCostSpan').show();
            }
        }

        function returnPrice(premium, renewal_day, renewal_month){

            var now = new Date();
            now.setHours("0", "0", "0", "0");

            var renewal = new Date(now.getFullYear(), renewal_month - 1, renewal_day);

            if(renewal.getTime() === now.getTime()){
                //console.log('equal');
            }else if(now.getTime() > renewal.getTime()){
                renewal = new Date(now.getFullYear() + 1, renewal_month - 1, renewal_day);
            }

            var difference_in_time = renewal.getTime() - now.getTime();

            var difference_in_days = Math.ceil(difference_in_time / (1000 * 3600 * 24));

            console.log('premium');
            console.log(premium);

            premium = Math.round(premium);

            //console.log(premium)


            var price = premium * (premium_rate * .01);
            console.log(price);

            if(renewal.getTime() !== now.getTime()){
                var price_per_day = price / 365;

                var price = price_per_day * difference_in_days;
            }


            //console.log(price);
            //console.log(price.toFixed(2));
            return price.toFixed(0);
        }

        function refreshCompanies(){
            $.ajax({
               type: 'POST',
               url: '<?php echo route("refreshCompaniesZagent"); ?>',
               data: {
                   _token: '<?php echo csrf_token(); ?>',
                   agency_id: '<?php echo $agency->id; ?>',
               },
                success: function(data){
                   console.log(data);
                   active_companies = data['activated'];
                   pending_companies = data['pending'];
                   buildActiveCompanies();
                   buildPendingCompanies();
                },
                error: function(data){
                    console.log(data);
                }
            });
        }

        refreshCompanies();
    </script>

@endsection
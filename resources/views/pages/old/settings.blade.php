@extends('layouts.zen5_layout')
@section('maincontent')

<script src="https://www.zenployees.com/js/jBox.all.min.js"></script>
	
<div class="pageContent bgimage-bgheader bghaze-plum fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>my settings</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/teammessages.png"></span><span class="subContent">update password and system settings</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/gears.png">
            </div> 
        </div>           
	</div>


	<div class="contentBlock lessPadding">
		<div class="container setMinHeight">

        settings<br>
		<button id="launchModal">launch modal</button>
        <a>launch modal</a>				
		</div>
	</div>


</div>

@include('partials.testModal')

<script>
	//each jBox modal needs to be created with this code block, using seperate variable names for each new modal created
	var claimMoodModal = new jBox('Modal', {
		attach: '#launchModal', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#edit_myinfo'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true,
		onOpen: function(event){
			
		},
		onCloseComplete: function(event){
			
		}
	});
	
	//*** CUSTOM CLOSE BUTTON CODE ***//
	//the variable storing each modal should be added to the modal array
	var modal_array = [claimMoodModal];
	
	function closeModals(){
	    for(var i = 0; i < modal_array.length; i++){
	        modal_array[i].close();
	    }
	}
	
	$('.close-zModal').on('click', function(){
	    for(var i = 0; i < modal_array.length; i++){
	        modal_array[i].close();
	    }
	});

</script>	

	


@endsection

@extends('layouts.zen5_layout')
@section('maincontent')
    <?php


    $sorted_companies = Auth::user()->getCompanyListByAgency();

   // var_dump($sorted_companies);
    /*
    echo "<div class='agencyCompaniesDiv'>";
        echo "<h3>" . $agency . "</h3><br>";
            foreach($companies as $company){
                echo "<li class='companyListItem'><a href='/selectCompany/" . $company->id ."'>" . $company->company_name . "</a></li>";
            }
    echo "</div>";
}
    */


    ?>

<div class="pageContent bgimage-bgheader bghaze-ocean fade-in">		
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                	<span>Welcome <b>Some other Entity</b>!</span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/company.png"></span><span class="subContent">manage your <b>clients</b></span>
                    </div>
            </div>
            <div class="pageIcon animate__animated animate__bounceInRight">
                <img src="/images/icons/zenpro2.png">
            </div> 
        </div>           
	</div>
    <div class="container noPad">
       <div id="tableContainer" class="container noPad">
        <!-- start of the company block -->
        <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-company" id="companyName"></div> [company name here]</span>
            <div class="zen-actionPanel transparent tight">
                <div class="sectionContent">
                    <table class="zenListTable tight" id="[companynameID]">
                        <tbody>
                        <tr class="tableHeadings">
                            <td></td>
                            <td><button class="sorter showIconModal">name</button></td>
                            <td><button class="sorter showIconModal">open claims</button></td>
                            <td></td>
                        </tr>
    
                        <!-- start of repeatable table area -->    
                        <tr class="hoverLink">
                            <td class="companyLogo"><div class="companyIcon"></div></td>
                            <td class="companyName"></td>
                            <td class="openClaims"></td>
                            <!--this next button links to the company edit page, for this listed company-->
                            <td><button class="smallIcon"></button></td>
                        </tr>
                        <tr class="tablerowspace"><td></td></tr>
                        <!-- end of repeatable table area -->
                        
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- end of the company block -->
      </div>
    </div> 
   
    <div id="tableContainer"></div>

    <script>
        var companies = JSON.parse('<?php echo json_encode($sorted_companies); ?>');
        console.log("Companies: " + companies);

        //THIS IS THE OVERALL FUNCTION THAT LOOPS THROUGH ALL THE COMPANIES AND BUILDS THE MAIN HTML BLOCK
        function buildTableArray(){
            var html = "";
            var i = 0;
            for(i; i < Object.keys(companies).length; i++){
                console.log(companies[i]);
                html+= buildAgencyTable(companies[i]);
            }

            console.log("html before tablecontainer: " + html);
            $('#tableContainer').html(html);
            console.log("after table container: " + html);
        }


        //THIS FUNCTION CREATES THE HTML FOR EACH AGENCY BLOCK, THIS IS WHAT YOU'LL BE CHANGING
        function buildAgencyTable(agency){
            console.log(agency[0].agency_name);

            //get the agency_name var
            if(agency[0].agency_name === ""){
                var agency_name = "Independent Companies";
            }else{
                var agency_name = agency[0].agency_name;
            }

            //generate the html for each section with the agency name; aside from agency_name this part is static but repeated
            var html = '<section class="sectionPanel">' +
            '               <span class="sectionTitle"><div class="icon icon-company">' + agency_name + '</div></span>' +
            '               <div class="zen-actionPanel transparent tight">' +
            '                   <div class="sectionContent">' +
            '                       <table class="zenListTable tight" id="[companynameID]">' +        
            '                           <tbody>' +
            '                           <tr class="tableHeadings">' +
            '                               <td></td>' +
            '                                <td><button class="sorter showIconModal">name</button></td>' +
            '                                <td><button class="sorter showIconModal">open claims</button></td>' +
            '                                <td></td>' +
            '                           </tr>';
            
            //this generates the html for each company row and is appended to the html var
            var i = 0;
            for(i; i < Object.keys(agency).length; i++){
                console.log("angency " + Object.keys(agency));
                console.log("angency id" + agency);
                var row_html = "<tr class='hoverLink companyListRow' data-id=" + agency[i].id + "><td class='companyLogo'><div class='companyIcon'></div></td><td class='companyName'>" + agency[i].company_name + "</td><td class='openClaims'>" + agency[i].open_claims + "</td><td><button class='smallIcon'></button></td></tr><tr class='tablerowspace'><td></td></tr>";
                html+= row_html;
            }
            console.log("row_html: " + row_html);

            //this adds the closing table html
            html += "</tbody></table></div></div></section>";
            return html;

        }
        

        buildTableArray();
        //this redirects to zenboard with the selected company
        $('#tableContainer').on('click', '.companyListRow', function(){
           var id = $(this).data('id');
           console.log('id' + id);
           if (id != undefined){
              window.location.href = "/selectCompany/" + id;
            }
        });


    </script>
@endsection
	


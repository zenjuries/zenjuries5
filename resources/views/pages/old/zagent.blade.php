@extends('layouts.zen5_layout')
@section('maincontent')
    <?php
        $agency = Auth::user()->getAgency();

        $agency_company = \App\Company::where('is_agency', 1)->where('agency_id', $agency->id)->first();

        if(!is_null($agency_company->pm_last_four)){
            $has_card = true;
        }else $has_card = false;

        $active_companies = $agency->getCompanies("activated");
        $pending_companies = $agency->getCompanies("pending");



    ?>

<div class="pageContent bgimage-bgheader bghaze-plum fade-in">		
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                	<span>All policyholders</span> <span class="devcomment">(old zagent ploicyholders)</span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/company.png"></span><span class="subContent">manage your <b>policyholders</b></span>
                </div>
            </div>
            <div class="pageIcon animate__animated animate__bounceInRight">
                <img src="/images/icons/gears.png">
            </div> 
        </div>           
	</div>


    <div class="buttonArray inline">
        <button class="orange showZagentModal" data-modalcontent="#newClientModal"><div class="icon inline icon-plus"></div> new client</button>
    </div>
    <div style="height:10px;"></div>
	<div class="contentBlock noPad">
		{{-- <div class="container noPad"> --}}
        <div class="container noPad" id="tablecontainer">    
            <div class="itemGrid pad5">
                <div class="itemGrid__item half max">
                    <div class="zen-actionPanel noHover">
                        <div class="panelTitle full"><div class="icon inline icon-pencil-square-o"></div> active companies</div>
                        <div class="panelElement">
                            <table class="zenListTable tight" id="x1">
                                <tbody>
                                <tr class="tableHeadings">
                                    <td></td>
                                    <td><button class="sorter showIconModal">name</button></td>
                                    <td><button class="sorter showIconModal">open claims</button></td>
                                    <td><button class="sorter showIconModal">renewal date</button></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="companyLogo"><div class="companyIcon"></div></td>
                                    <td class="companyName">company</td>
                                    <td class="openClaims">34</td>
                                    <td class="date">03/10/2022</td>
                                    <td><button class="smallIcon zagentEditClientModal" data-modalcontent="#editClientModal"></button></td>
                                </tr>
                                <tr class="tablerowspace"><td></td></tr>
                                <tr>
                                    <td class="companyLogo"><div class="companyIcon"></div></td>
                                    <td class="companyName">company</td>
                                    <td class="openClaims">34</td>
                                    <td class="date">03/10/2022</td>
                                    <td><button class="smallIcon zagentEditClientModal" data-modalcontent="#editClientModal"></button></td>
                                </tr>                                
                                </tbody>
                            </table>
                            <div style="height:40px;"></div>
                        </div>
                    </div>			
                </div>

                <div id="tableContainer"></div>

                <div class="itemGrid__item half max">
                    <div class="zen-actionPanel noHover">
                        <div class="panelTitle full"><div class="icon inline icon-pencil-square-o"></div> pending companies</div>
                        <div class="panelElement">
                            <table class="zenListTable tight" id="x2">
                            <tbody>
                                <tr class="tableHeadings">
                                    <td></td>
                                    <td class="name"><button class="sorter showIconModal">name</button></td>
                                    <td class="injuries"><button class="sorter showIconModal">invited on</button></td>
                                    <td class="date"><button class="sorter showIconModal">owner email</button></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="companyLogo"><div class="companyIcon"></div></td>
                                    <td class="companyName">company</td>
                                    <td class="date">03/10/2022</td>
                                    <td class="email">email@emailaddress.com</td>
                                    <td><button class="smallIcon"></button></td>
                                </tr>
                                <tr class="tablerowspace"><td></td></tr>
                                <tr>
                                    <td class="companyLogo"><div class="companyIcon"></div></td>
                                    <td class="companyName">company</td>
                                    <td class="date">03/10/2022</td>
                                    <td class="email">email@emailaddress.com</td>
                                    <td><button class="smallIcon"></button></td>
                                </tr>
                                </tbody>
                            </table>
                            <div style="height:40px;"></div>
                        </div>
                    </div>			
                </div>
            </div>
            
        </div>



<!-- sample with comments -->
        <div class="container noPad" id="test1">    
            <div class="itemGrid pad5">
                <div class="itemGrid__item half max">
                    <div class="zen-actionPanel noHover">
                        <div class="panelTitle full"><div class="icon inline icon-pencil-square-o"></div> active companies</div>
                        <div class="panelElement">
                            <table class="zenListTable tight" id="x1">
                                <tbody>
                                <tr class="tableHeadings">
                                    <td></td>
                                    <td><button class="sorter showIconModal">name</button></td>
                                    <td><button class="sorter showIconModal">open</button></td>
                                    <td><button class="sorter showIconModal">critical</button></td>
                                    <td><button class="sorter showIconModal">policy#</button></td>
                                    <td><button class="sorter showIconModal">renewal</button></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="companyLogo"><div class="companyIcon" style="background-image:url('/images/default_logos/company5.png');"></div></td>
                                    <td class="companyName"><span>really long company name here</span></td>
                                    <td class="openClaims">12</td>
                                    <td class="criticalClaims">2</td>
                                    <td class="policyNum"><span>334453t34</span></td>
                                    <td class="date"><span>03/10/2022</span></td>
                                    <td><button class="smallIcon zagentEditClientModal" data-modalcontent="#editClientModal"></button></td>
                                </tr>
                                <!-- make sure "tablerowspace" is between each company row element -->
                                <tr class="tablerowspace"><td></td></tr>

                                <tr>
                                    <td class="companyLogo"><div class="companyIcon" style="background-image:url('/images/default_logos/company2.png');"></div></td>
                                    <td class="companyName"><span>company name</span></td>
                                    <td class="openClaims">22</td>
                                    <td class="criticalClaims">1</td>
                                    <td class="policyNum"><span></span><button class="showZagentModal" data-modalcontent="#showAddInputPolicy">enter #</button></span></td>
                                    <!-- date can have class of attention, alert, critical -->
                                    <td class="date critical"><span>03/10/2022</span></td>
                                    <td><button class="smallIcon zagentEditClientModal" data-modalcontent="#editClientModal"></button></td>
                                </tr>                                
                                </tbody>
                            </table>
                            <div style="height:40px;"></div>
                        </div>
                    </div>			
                </div>

                <div id="tableContainer"></div>

                <div class="itemGrid__item half max">
                    <div class="zen-actionPanel noHover">
                        <div class="panelTitle full"><div class="icon inline icon-pencil-square-o"></div> pending companies</div>
                        <div class="panelElement">
                            <table class="zenListTable tight" id="x2">
                            <tbody>
                                <tr class="tableHeadings">
                                    <td></td>
                                    <td class="name"><button class="sorter showIconModal">name</button></td>
                                    <td class="injuries"><button class="sorter showIconModal">invited on</button></td>
                                    <td class="date"><button class="sorter showIconModal">owner email</button></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="companyLogo"><div class="companyIcon" style="background-image:url('/images/default_logos/company7.png');"></div></td>
                                    <td class="companyName"><span>company with another long name</span></td>
                                    <td class="date"><span>03/10/2022</span></td>
                                    <td class="email"><span>email@emailaddress.com</span></td>
                                    <td><button class="smallIcon"></button></td>
                                </tr>
                                <tr class="tablerowspace"><td></td></tr>
                                <tr>
                                    <td class="companyLogo"><div class="companyIcon" style="background-image:url('/images/default_logos/company1.png');"></div></td>
                                    <td class="companyName"><span>company</span></td>
                                    <td class="date"><span>03/10/2022</span></td>
                                    <td class="email"><span>email@emailaddress.com</span></td>
                                    <td><button class="smallIcon"></button></td>
                                </tr>
                            </tbody>
                            </table>
                            <div style="height:40px;"></div>
                        </div>
                    </div>			
                </div>
            </div>
            
        </div>

        

    </div>
</div>

<div id="zagentModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
    @include('partials.modals.addClient')
    @include('partials.modals.addPolicy')          
	@include('partials.modals.editClient')
</div>


    <script>
        //modal globalVar to contain the reference to the modal 
        var modal;

        //THIS FUNCTION SHOULD BE easy to drop on a page, the class triggering the onclick and the modal name should be the main changes. 
        $(document).on('click', ".showZagentModal", function(){    
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            //modal ID goes here
            content: $('#zagentModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();

        //script related to a specific content block can be added like this
        //status modal
        if(target === "#newClientModal"){
            //scrpit for contentBlock1
            var client_stripe_id = "{{ $agency_company->stripe_id }}";
            
            if(client_stripe_id != "" || client_strip_id != null){
                $('#paymentDiv').find('.sectionInfo').html("");
                var cardButton = '<input type="radio" name="paymentMethodType" id="useCard"><label for="useCard">card ending in {{ $agency_company->pm_last_four }}</label>';
                $('#paymentMethodDiv').find('.formInput').prepend(cardButton);
            }
            

        }
        modal.open();
        });
    

        $('#buttonOne').hide();
        $('#buttonTwo').hide();
        $('#buttonThree').hide();
        var active_companies =  JSON.parse('<?php echo json_encode($active_companies); ?>');
        var pending_companies = JSON.parse('<?php echo json_encode($pending_companies); ?>');

        console.log(active_companies);
        console.log(pending_companies);

        function buildActiveCompanies(){
            var html = '<tr class="tableHeadings">'+
                            '<td></td>' +
                            '<td><button class="sorter showIconModal">name</button></td>' +
                            '<td><button class="sorter showIconModal">open</button></td>' +
                            '<td><button class="sorter showIconModal">critical</button></td>' +
                            '<td><button class="sorter showIconModal">policy#</button></td>' +
                            '<td><button class="sorter showIconModal">renewal</button></td>' +
                            '<td></td>' +
                        '</tr>';

            var i = 0;
            for(i; i < active_companies.length; i++){
                var randomCompanyImage = Math.floor(Math.random() * 9) + 1;
                if(active_companies[i].policy_number === null){
                    active_companies[i].policy_number = "-?-";
                }
                if(active_companies[i].renewal_date === null){
                    active_companies[i].renewal_date = "-?-";
                }
                /*
                if(active_companies[i].logo_location === null){

                }
                */
                /*
                <tr>
                    <td class="companyLogo"><div class="companyIcon" style="background-image:url('/images/default_logos/company5.png');"></div></td>
                    <td class="companyName"><span>really long company name here</span></td>
                    <td class="openClaims">12</td>
                    <td class="criticalClaims">2</td>
                    <td class="policyNum"><span>334453t34</span></td>
                    <td class="date"><span>03/10/2022</span></td>
                    <td><button class="smallIcon zagentEditClientModal" data-modalcontent="#editClientModal"></button></td>
                 </tr>
                 */
                html += "<tr class='companyListItem' data-companyid='"+active_companies[i].id+"'><td class='companyLogo'><div class='companyIcon' style=\"background-image:url('/images/default_logos/company5.png');\"></div></td>" +
                        "<td class='companyName'><span>" + active_companies[i].company_name + "</span></td>" +
                        "<td class='openClaims'>" + active_companies[i].open_injuries + "</td>" +
                        "<td class='criticalClaims'>" + active_companies[i].critical_injuries + "</td>" +
                        "<td class='policyNum'><span>" + active_companies[i].policy_number + "</span></td>" +
                        "<td class='date'><span>" + active_companies[i].renewal_date + "</span></td>" +
                        "<td><button class='smallIcon showZagentModal' data-modalcontent='#editClientModal'></button></td>" +
                        "<tr class='tablerowspace'><td></td></tr>"
                        "</tr>";
                console.log("html: " + html);    
            }
            
            $('#x1').find('tbody').html(html);
            console.log("final html: " + html);
        }

        function buildPendingCompanies(){
            var html = '<tr class="tableHeadings">' +
                '            <td></td>' +
                '            <td class="name"><button class="sorter showIconModal">name</button></td>' +
                '            <td class="injuries"><button class="sorter showIconModal">invited on</button></td>' +
                '            <td class="date"><button class="sorter showIconModal">owner email</button></td>' +
                '            <td></td>' +
                '        </tr>';
            var i = 0;
            for(i; i < pending_companies.length; i++){
                var randomCompanyImage = Math.floor(Math.random() * 9) + 1;
                /*
                var invited_on = pending_companies[i].invite_resent_on;
                if(invited_on === null){
                    invited_on = pending_companies[i].created_at;
                }*/

                html += "<tr><td class='companyLogo'><div class='companyIcon' style=\"background-image:url('/images/default_logos/company"+randomCompanyImage+".png');\"></div></td>" +
                        "<td class='companyName'><span>" + pending_companies[i].company_name + "</span></td>" +
                        "<td class='date'><span>" + pending_companies[i].invited_at + "</span></td>" +
                        "<td class='email'><span>" + pending_companies[i].owner.email + "</span></td>" +
                        "<td><button class='smallIcon showZagentModal' data-modalcontent='#editClientModal'></button></td>" +
                        "<tr class='tablerowspace'><td></td></tr>"
                        "</tr>";
            }
          
            $('#x2').find('tbody').html(html);
        }

        buildActiveCompanies();
        buildPendingCompanies();

        var premium_rate = '<?php echo $agency->premium_rate; ?>';

        $('#submitNewClient').on('click', function(){
           validateClient();
        });

        function validateClient(){
            var valid = true;
            $('.inputError').removeClass('show');
            console.log('validating');

            var company_name = $('#companyName').val();
            if(company_name === ""){
                valid = false;
                $('#companyName').parent().find('.inputError').addClass('show');
            }

            var contact_name = $('#contactName').val();
            if(contact_name === ""){
                valid = false;
                $('#contactName').parent().find('.inputError').addClass('show');
            }

            var contact_email = $('#contactEmail').val();
            if(contact_email === ""){
                valid = false;
                $('#contactEmail').parent().find('.inputError').addClass('show');
            }

            var renewal_month = $('#renewalMonth').val();
            if(renewal_month === ""){
                valid = false;
                $('#renewalMonth').parent().find('.inputError').addClass('show');
            }

            var renewal_day = $('#renewalDay').val();
            if(renewal_day === ""){
                valid = false;
                $('#renewalDay').parent().find('.inputError').addClass('show');
            }

            var premium = $('#premium').val();
            //TODO:remove non numeric non decimal characters
            //premium = premium.replace(/\D/g,'');
            if(premium === ""){
                $('#premium').parent().find('.inputError').addClass('show');
            }

            var file = $('#file').prop('files')[0];
            //check if file is set, if not check it to null and no undefined
            if(file === undefined){
                file = null;
            }
            console.log(file);
            if($('#agentPays').is(':checked')){
                var client_pays = 0;
            }else if($('#clientPays').is(':checked')){
                var client_pays = 1;
            }else{
                console.log('no pays');
                valid = false;
                $('#whoPaysError').addClass('show');
                //TODO: validate no selection
            }

            if(client_pays === 0){
                var has_card = $('#hasCard').val();
                if($('#useCard').is(":checked")){
                    var payment_method = "card";
                }else if($('#useCheck').is(":checked")){
                    var payment_method = "check";
                }else{
                    console.log('no card');
                    valid = false;
                    $('#paymentMethodError').addClass('show');
                    //TODO: validate no selection
                }
            }else{
                var has_card = null;
                var payment_method = null;
            }

            if(valid){

                var formData = new FormData();
                formData.append('_token', '<?php echo csrf_token(); ?>');
                formData.append('company_name', company_name);
                formData.append('contact_name', contact_name);
                formData.append('email', contact_email);
                formData.append('premium', premium);
                formData.append('renewal_day', renewal_day);
                formData.append('renewal_month', renewal_month);
                formData.append('payment_method', payment_method);
                formData.append('client_pays', client_pays);
                formData.append('file', file);
                formData.append('agency_id', '<?php echo $agency->id; ?>');

                $.ajax({
                    type: 'POST',
                    url: '<?php echo route('newClient'); ?>',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(data){
                        clearNewClientModal();
                        refreshCompanies();

                    },
                    error: function(data){
                        //alert('error!');
                        console.log(data);
                        handleServerValidation(data);

                    }

                });
            }


        }

        function clearNewClientModal(){
            addClientModal.close();
            //TODO: CLEAR OUT DATA FROM MODAL
        }

        function handleServerValidation(data) {
            if(data['status'] === 422) {
                var errors = (data['responseJSON']['errors']);
                console.log(errors);
                if (errors['client_pays']) {
                    $('#whoPaysError').html(errors['client_pays']).addClass('show');
                }
                if (errors['company_name']) {
                    $('#companyName').parent().find('.inputError').html(errors['company_name']).addClass('show');
                }
                if (errors['contact_name']) {
                    $('#contactName').parent().find('.inputError').html(errors['contact_name']).addClass('show');
                }
                if (errors['email']) {
                    $('#contactEmail').parent().find('.inputError').html(errors['email']).addClass('show');
                }
                if (errors['payment_method']) {
                    $('#paymentMethodError').html(errors['payment_method']).addClass('show');
                }
                if (errors['premium']) {
                    $('#premium').parent().find('.inputError').html(errors['premium']).addClass('show');
                }
                if (errors['renewal_day']) {
                    $('#renewalDay').parent().find('.inputError').html(errors['renewal_day']).addClass('show');
                }
                if (errors['renewal_month']) {
                    $('#renewalMonth').parent().find('.inputError').html(errors['renewal_month']).addClass('show');
                }
                if (errors['file']) {
                    $('#file').parent().find('.inputError').html(errors['file']).addClass('show');
                }
            }else{
                //TODO:: handle server errors
            }
        }

        $('#whoPaysDiv').on('click', function(){
            if($('#agentPays').is(':checked')){
                $('#paymentDiv').show();
            }else{
                $('#paymentDiv').hide();
            }

        });

        $('#activeCompanyTable').on('click', '.activeCompanyRow', function(e){
           if($(e.target).hasClass('editCompany') || $(e.target).hasClass('smallIcon')){
               var url = "/company";
           }else {
               var url = "/zenboard";
           }
            var company_id = $(this).data('id');
            $.ajax({
                type: 'POST',
                url: '<?php echo route('selectCompanyPost'); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    company_id: company_id
                },
                success: function(data){
                    console.log('success');
                    window.location.href = url;
                },
                error: function(data){
                    console.log(data);
                }
            });

        });


        $('#premium, #renewalMonth, #renewalDay').on('focusout', function(){
           getPrice();
        });

        $('#premium').on('input', function(){
            getPrice();
        });

        function getPrice(){
            var premium = $('#premium').val();
            premium = premium.replace(/[^0-9.]/g, '');
            var renewal_day = $('#renewalDay').val();
            renewal_day = renewal_day.replace(/\D/g,'');
            var renewal_month = $('#renewalMonth').val();
            renewal_month = renewal_month.replace(/\D/g,'');
            if(renewal_day !== "" && renewal_month !== "" && premium !== ""){
                var total_cost = returnPrice(premium, renewal_day, renewal_month);
                $('#finalCostTarget').html(total_cost);
                $('#finalCostSpan').show();
            }
        }

        function returnPrice(premium, renewal_day, renewal_month){

            var now = new Date();
            now.setHours("0", "0", "0", "0");

            var renewal = new Date(now.getFullYear(), renewal_month - 1, renewal_day);

            if(renewal.getTime() === now.getTime()){
                //console.log('equal');
            }else if(now.getTime() > renewal.getTime()){
                renewal = new Date(now.getFullYear() + 1, renewal_month - 1, renewal_day);
            }

            var difference_in_time = renewal.getTime() - now.getTime();

            var difference_in_days = Math.ceil(difference_in_time / (1000 * 3600 * 24));

            console.log('premium');
            console.log(premium);

            premium = Math.round(premium);

            //console.log(premium)


            var price = premium * (premium_rate * .01);
            console.log(price);

            if(renewal.getTime() !== now.getTime()){
                var price_per_day = price / 365;

                var price = price_per_day * difference_in_days;
            }


            //console.log(price);
            //console.log(price.toFixed(2));
            return price.toFixed(0);
        }

        function refreshCompanies(){
            $.ajax({
               type: 'POST',
               url: '<?php echo route("refreshCompaniesZagent"); ?>',
               data: {
                   _token: '<?php echo csrf_token(); ?>',
                   agency_id: '<?php echo $agency->id; ?>',
               },
                success: function(data){
                   console.log(data);
                   active_companies = data['activated'];
                   pending_companies = data['pending'];
                   buildActiveCompanies();
                   buildPendingCompanies();
                },
                error: function(data){
                    console.log(data);
                }
            });
        }

        refreshCompanies();

        $('body').on('click', 'tr.companyListItem', function(){
            var id = $(this).data('companyid');
            window.location.href = "/selectCompany/" + id;
        });
    </script>

@endsection
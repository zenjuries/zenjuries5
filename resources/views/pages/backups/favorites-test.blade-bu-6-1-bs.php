@extends('layouts.zen5_layout')
@section('maincontent')

<script>
    $(document).ready(function() {
        var stringPassedIn = "0000";
        var favArray = stringPassedIn.split("");

        if(favArray[3] === "1"){ 
            $('.setFavorite').toggleClass("set");
        }

        for(var i = 0; i < favArray.length; i++){
            if(favArray[i] === "1"){
                var string = "#fav";
                string += (i + 1);
                $(string).show();
            }
        } 

        function updateStringPassedIn(){
            stringPassedIn = favArray.toString().replaceAll(",", "");
            alert(stringPassedIn);
        }

        $('.setFavorite').on("click", function() {
            if(favArray[3] === "0")
            {
                $(this).toggleClass("set");
                $('#fav4').show();
                favArray[3] = "1";
                updateStringPassedIn();
            }
            else{
                $(this).toggleClass("set");
                $('#fav4').hide();
                favArray[3] = "0";
                updateStringPassedIn();
            }
        }) 

        $('.favRemove').on("click", function() {

            $(this.parentElement.parentElement).hide();
            var thisId = this.parentElement.parentElement.id;

            if(thisId === "fav1")
            {
                favArray[0] = "0";
                updateStringPassedIn();
            }
            else if(thisId === "fav2")
            {     
                favArray[1] = "0";
                updateStringPassedIn();
            }
            else if(thisId === "fav3")
            {
                favArray[2] = "0";
                updateStringPassedIn();
            }
            else if(thisId === "fav4")
            {
                $('.setFavorite').toggleClass("set");
                favArray[3] = "0";
                updateStringPassedIn();
            }
           
        }) 

        $('#favBtn1').on("click", function() {
            if(favArray[0] === "0")
            {
                $('#fav1').show();
                favArray[0] = "1";
                updateStringPassedIn();
            }
        }) 

        $('#favBtn12').on("click", function() {
            if(favArray[0] === "0")
            {
                $('#fav1').show();
                favArray[0] = "1";
                updateStringPassedIn();
            }
            if(favArray[1] === "0")
            {
                $('#fav2').show();
                favArray[1] = "1";
                updateStringPassedIn();
            }
        }) 
        
        $('#favBtn13').on("click", function() {
            if(favArray[0] === "0")
            {
                $('#fav1').show();
                favArray[0] = "1";
                updateStringPassedIn();
            }
            if(favArray[2] === "0")
            {
                $('#fav3').show();
                favArray[2] = "1";
                updateStringPassedIn();
            }
        })
    });
</script>

<div class="pageContent bgimage-bgheader bghaze-sky fade-in">
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                    <span>favorites test page</span>
                </div>
                <div class="pageHeader__subTitle">
                    <!-- using 'set' with the 'setFavorite' class will make it active.  toggle to make inactive-->
                    <span class="icon"><img src="images/icons/heart.png"></span><span class="subContent">set favorite 4.<button class="setFavorite"></button></span>
                </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/checkheart.png">
            </div>
        </div>
    </div>

    <div class="contentBlock noPad">
        <div class="container noPad">
            <div class="testPanel">
                <section class="sectionPanel">
                    <!-- buttons using a data attribute to hard set the status of the favorites -->
                    <div class="center" style="padding-top:20px;">
                        <button class="red" id="favBtn1">Favorites 1</button>

                        <button class="green" id="favBtn12">Favorites 1,2</button>

                        <button class="blue" id="favBtn13">Favorites 1,3</button>
                    </div>
                </section>

                <section class="sectionPanel" style="min-height:400px;">
                    <span class="sectionTitle"><div class="icon icon-bar-heart"></div> FAVORITES</span>
                    <!-- add/remove class 'showAll' to override favBox display:none (default style)-->
                    <div class="homeFavorites">
                        <section>
                            <div class="favBox" id="fav1"><a class="noLink favItem"><span class="favTitle">Favorite 1</span><span class="favRemove">X</span>
                            <span class="favContent">
                                <hr>
                            Lorem ipsum dolor sit amet, consectetur elit.
                            </span></a></div>

                            <div class="favBox" id="fav2"><a class="noLink favItem"><span class="favTitle">Favorite 2</span><span class="favRemove">X</span>
                            <span class="favContent">
                            <hr>
                            Sed tincidunt odio sit amet tempus pellentesque.
                            </span></a></div>
                            
                            <div class="favBox" id="fav3"><a class="noLink favItem"><span class="favTitle">Favorite 3</span><span class="favRemove">X</span>
                            <span class="favContent">
                            <hr>
                            Nunc blandit commodo augue, at malesuada leo.
                            </span></a></div>
                            
                            <div class="favBox" id="fav4"><a class="noLink favItem"><span class="favTitle">Favorite 4</span></span><span class="favRemove">X</span>
                            <span class="favContent">
                            <hr>
                            Sed scelerisque rutrum malesuada. Praesent mollis
                            </span></a></div>
                        </section>
                    </div>

                </section>
            </div>
        </div>
    </div>
</div>
   

@endsection
@extends('layouts.zen5_layout')
@section('maincontent')
	
<div class="pageContent bgimage-bgheader bghaze-peach fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>zenjuries help & support</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/bandaid.png"></span><span class="subContent">get help with zenjuries</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/help.png">
            </div> 
        </div>           
	</div>


	<div class="contentBlock lessPadding">
		<div class="container setMinHeight">

        <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-comments"></div>Communicate</span>
            <div class="sectionContent padded">
                <span class="sectionContentTitle">Chat with your Team</span>
                <p>Post messages to your team to keep everyone up to date.</p>

                <span class="sectionContentTitle">Sort Communications</span>
                <p>Use the controls below to find messages within certain date ranges.</p>

                <span class="sectionContentTitle">Search Communications</span>
                <p>Use the full text search to find messages with specific words or phrases.</p>
            </div>
         </section>

         <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-claimcosts"></div>Claim Costs</span>
            <div class="sectionContent padded">
                <span class="sectionContentTitle">Paid Values</span>
                <p>Paid Values represent money you've already spent towards this claim. This number will continue to rise, or stay the same, throughout the life of the claim.</p>

                <span class="sectionContentTitle">Reserve Values</span>
                <p>Reserve Values represent the money your insurance company expects to have to pay on a claim. Swift and efficient management will reduce the claim's reserve!</p>

                <span class="sectionContentTitle">Total Costs</span>
                <p>Total Costs are the paid costs of a claim plus the reserve costs of a claim. You can reduce this number by lowering the reserve.</p>
            </div>
         </section>

         <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-stethoscope"></div>Injury Details</span>
            <div class="sectionContent padded">
                <span class="sectionContentTitle">Additional Information</span>
                <p>Enter any additional information for Injury Submitter's injury, like their phone number and email address. You will need their email address entered for them to view their Zen Garden page.</p>

                <span class="sectionContentTitle">Notes & Claim number</span>
                <p>You can add notes to each employee's injury, and always make sure to fill in the Claim Number as soon as you have it.</p>
            </div>
         </section>

         <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-clinic"></div>Appointments</span>
            <div class="sectionContent padded">
                <span class="sectionContentTitle">Monitoring</span>
                <p>Use this section of the Tree of Communications to keep a record of Injury Submitter's medical appointments. Keeping this up-to-date will make sure that the whole team always knows what's going on with Injury Submitter!</p>

                <span class="sectionContentTitle">Updating</span>
                <p>Entering an appointment will notify the team and the injured employee (if their email address has been added). After an appointment happens, please update it with a summary of the results of the appointment (new findings about the injury, expected return to work date, etc) and enter a follow-up appointment if there is one.</p>
            </div>
         </section>

         <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-check"></div>To Do List</span>
            <div class="sectionContent padded">
                <span class="sectionContentTitle">Best Practices</span>
                <p>Review each item in the Guide, and complete every one to close Injury Submitter's injury in the most efficient manner. Some items just need verification, and some need information input. Complete them all to ensure that you've provided the best care possible.</p>

                <span class="sectionContentTitle">Custom Requests</span>
                <p>Click the 'Create a Custom Request" button at the top to use the custom request form. You can make your own requests to support this injury.</p>
            </div>
         </section>

         <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-teams"></div>Injury Team</span>
            <div class="sectionContent padded">
                <span class="sectionContentTitle">Team Members</span>
                <p>Team Members assigned to an injury should make sure they check on the tree of communications regularly and keep everything as up-to-date as possible. You can still add a new member to this team from the Teams page.</p>

                <span class="sectionContentTitle">Responsibilities</span>
                <p>Each member will have their own responsibilities based on their role on the team. Team Members will receive email communications asking them for claim information such as the Claim Number, etc.</p>

                <span class="sectionContentTitle">Insurance Adjuster</span>
                <p>Once an Adjuster has been assigned to the claim, please enter their contact info here to make it easy for your team members to contact them!</p>
            </div>
         </section>

         <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-paperclip"></div>Attachments</span>
            <div class="sectionContent padded">
                <span class="sectionContentTitle">Documents</span>
                <p>Use the buttons under each section to upload your documents that pertain to this injury. Allowed types are .pdf, .doc, .docx, .rtf, .txt, .eml</p>

                <span class="sectionContentTitle">Emails</span>
                <p>You can upload any emails you may have that you'd like to save with this injury. These have their own category so they are easier to find.</p>

                <span class="sectionContentTitle">Photos</span>
                <p>If you have any photos of the injury or the events surrounding the injury, upload those to the photo section.</p>
            </div>
         </section>
         
         <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-question"></div>Injury Submitter has been injured</span>
            <div class="sectionContent padded">
                <span class="sectionContentTitle">Now what do I do?</span>
                <p>You're doing great so far, quickly reporting the injury to Zenjuries is crucial for keeping costs low.
                    Now use Zenjuries to follow up with every aspect of the injury.
                    You'll be able to make sure Injury Submitter is happy and informed, and also be able to track communications, costs, and events.</p>

                <span class="sectionContentTitle">How does this work?</span>
                <p>Use the categories at the left to select the aspect of the injury you would like to manage. Included with each category is a quickView detail to allow for fast overviews.
                    Your goal is to move the claim along quickly (use the To-Do List as a guide!), and close out the injury while maintaining the happiness of the injured employee.</p>
            </div>
         </section>

         <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-progress-2"></div>Follow the Injury Progress</span>
            <div class="sectionContent padded">
                <span class="sectionContentTitle">Progress Tracker</span>
                <p>The Progress Tracker shows Injury Submitter's current status in the injury process, and lets you see what is up next. Green indicates a completed step Blue shows what step you are currently working on.</p>

                <span class="sectionContentTitle">Status Descriptions</span>
                <p>Each status description lets you know the details of each step in the process. Read them to see what you will have to do throughout the process.</p>
            </div>
         </section>

         <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-video-camera"></div>help videos</span>
            <div class="sectionContent">
                <div class="placeHolder">
                    Help articles 
                    <span>List faq, help articles, videos.</span>
                </div>                    
            </div>
         </section>
						
		</div>
	</div>


</div>

	


@endsection

@extends('layouts.zen5_layout')
@section('maincontent')
	
<div class="pageContent bgimage-bgheader bghaze-peach fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>zenjuries help & support</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/bandaid.png"></span><span class="subContent">get help with zenjuries</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/help.png">
            </div> 
        </div>           
	</div>


	<div class="contentBlock lessPadding">
		<div class="container setMinHeight">

        <section class="sectionPanel">
            <span class="sectionTitle"><div class="icon icon-lifebuoy"></div>need help</span>
            <div class="sectionContent">
                <div class="placeHolder">
                    Help articles 
                    <span>List faq, help articles, videos.</span>
                </div>                    
                <button class="yellow">request help</button>
            </div>
         </section>
						
		</div>
	</div>


</div>

	


@endsection

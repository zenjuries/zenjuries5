@extends('layouts.zen5_layout_simple')
@section('maincontent')

    <?php
    //.pdf, .doc, .docx, .rtf, .csv, .xls, .txt, or .eml'
    if($type != 'image'){
        $doc = \App\InjuryFile::where('id', $id)->first();
    }else{
        $doc = \App\InjuryImage::where('id', $id)->first();
    }
       
    ?>
<div class="center"><button onclick="history.back()">go back</button></div>
{{ logger($doc) }}
@if($type != "image")
<iframe src="/{{$doc->location}}?{{$date = str_replace(' ', '_', \Carbon\Carbon::now())}}" type="text/html" width="100%" height="100%" style="height:100vh"></iframe>
@else
<img src="{{$doc->location}}" width="100%">
@endif
<!--
@if($type === "file" && $use_iframe == true)
    <iframe src="/{{$doc->location}}?{{$date = str_replace(' ', '_', \Carbon\Carbon::now())}}" type="application/pdf" width="100%" height="100%" style="height:100vh"></iframe>
@elseif($type === "file" && $use_iframe == false)
    <div id="CSVTable"></div>
    <script>
    $(document).ready(function() {
        $('#CSVTable').CSVToTable('{{$doc->location}}');
    });
    </script>
@elseif($type === "image")
    <img src="{{$doc->location}}" width="100%">
@endif
-->
<div class="center"><button onclick="history.back()">go back</button></div>
<script>
   
</script>

@endsection
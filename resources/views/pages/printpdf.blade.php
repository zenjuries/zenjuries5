@extends('layouts.zen5_layout_print')
@section('maincontent')

<style>
    .centerPDF{width:max-content;margin:0 auto;}
</style>


<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.10.1/html2pdf.bundle.min.js" integrity="sha512-GsLlZN/3F2ErC5ifS5QtgpiJtWd43JWSuIgh7mbzZ8zBps+dvLusV+eNQATqgA/HdeKFVgA5v3S/cIrLF7QnIg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<div class="center"><button onclick="history.back()">go back</button></div>
<hr style="border-color:#00000020;">
<div class="downloadButton divCenter"><button class="gold hideMobile" id="downloadPDF"><div class="icon icon-file-pdf-o"></div> download pdf</button></div>

<div class="centerPDF" id="printPDF">
PDF CONTENT HERE

</div>

<hr style="border-color:#00000020;">
<div class="center"><button onclick="history.back()">go back</button></div>

<script>
var certificate = document.getElementById('printPDF');
var opt = {
  margin:       0,
  filename:     'newfile.pdf',
  image:        { type: 'jpeg', quality: 0.98 },
  html2canvas:  { scale: 1 },
  jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
};
$('#downloadPDF').on('click', function(){
    html2pdf().set(opt).from(certificate).save();
    });
</script>


@endsection
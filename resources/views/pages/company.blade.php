@extends('layouts.zen5_layout')
@section('maincontent')
<?php
use Carbon\Carbon;

$user = Auth::user();
$company = $user->getCompany();
$month = "";
$day = "";
if(!empty($company->renewal_date)){
    $date = new \Carbon\Carbon($company->renewal_date);
    $month = $date->month;
    $day = $date->day;
}
$carrier = \App\Carrier::where('id', $company->carrier_id)->first();

$team = \App\Team::where('id', $company->team_id)->first();
$owner_name = \App\User::where('id', $team->owner_id)->value('name');
$teamID = \App\Company::where('id', $user->company_id)->value('team_id');
//$isOwner = DB::table('team_users')->where('team_id', $teamID)->where('user_id', $user->id)->where('role', 'owner')->first();
if($team->owner_id === $user->id){
    $isOwner = true;
}else{
    $isOwner = NULL;
}

if($user->is_zagent && $user->zagent_id !== $company->id){
    $isOwner = true;
}

if($isOwner !== NULL){
    $teamUsers = DB::table('team_users')->where('team_id', $teamID)->where(function($query){
        $query->where('role', 'owner')->orWhere('role', 'employee')->orWhere('role', 'agent');
    })->get();
}

//BILLING
$intent = $company->createSetupIntent();

if($company->stripe_id === NULL){
    $has_card = false;
}else{$has_card = true;}


/*
$user = Auth::user();
$company = $user->getCompany();
$month = "";
$day = "";
if(!empty($company->renewal_date)){
    $date = new \Carbon\Carbon($company->renewal_date);
    $month = $date->month;
    $day = $date->day;
}
$carrier = \App\Carrier::where('id', $company->carrier_id)->first();

$team = \App\Team::where('id', $company->team_id)->first();
$owner_name = \App\User::where('id', $team->owner_id)->value('name');
$teamID = \App\Company::where('id', Auth::user()->getCompany()->id)->value('team_id');
$isOwner = DB::table('team_users')->where('team_id', $teamID)->
where('user_id', Auth::user()->id)->
where('role', 'owner')->first();

if(Auth::user()->is_zagent && Auth::user()->zagent_id !== $company->id){
    $isOwner = true;
}

if($isOwner !== NULL){
    $teamUsers = DB::table('team_users')->where('team_id', $teamID)->where(function($query){
        $query->where('role', 'owner')->orWhere('role', 'employee')->orWhere('role', 'agent');
    })->get();
}
*/
?>
<div class="pageContent bgimage-bgheader pagebackground13 fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>company settings</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/edit.png"></span><span class="subContent">edit your company settings</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/company.png">
            </div> 
        </div>           
	</div>


<script>
$(document).ready(function() {
  $('.toggleChat').click(function() {
    $('.messageListing').toggleClass('open');
  });
  $('.messagePost').click(function() {
    $(this).toggleClass('open');
  });  
});    
</script>

	<div class="contentBlock lessPadding">
		<div class="container setMinHeight">

            <section class="sectionPanel" style="display: none;">
                <span class="sectionTitle"><div class="icon icon-commenting"></div> messages</span>
                <div class="sectionNav"><a class="toggleChat">minimize</a> | <a>new message</a> | <a>filter</a></div>
                <div class="sectionContent">
                <section class="messageListing">
                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType resolvedPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">resolvedPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reopenedPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reopenedPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost chat">
                            <div class="messageContent">
                                <span class="messageType chatPost"><span class="messageIcon" style="background-color:purple;background-image:url('images/avatar_icons/avatar12.png');"></span></span>
                                <span class="messageTitle">chatPost</span>
                                <span class="messageBody">Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus. Morbi in dui quis erat iaculis interdum. Nam sit amet eros felis. Donec laoreet quam vulputate nisl pellentesque commodo.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">hide all replies</span>
                                <div class="messageControls">reply | collapse | edit </div>
                            </div>
                        </div>
                        <div class="messageReplyThread" style="display:none;">
                            <div class="messagePost chat">
                                <div class="messageContent">
                                    <span class="messageType chatPost"><span class="messageIcon" style="background-color:green;background-image:url('images/avatar_icons/avatar24.png');"></span></span>
                                    <span class="messageTitle">chatPost</span>
                                    <span class="messageBody">Morbi massa sapien, semper eget nisi nec, fringilla posuere eros. Sed aliquam lorem quis dolor venenatis laoreet.</span>
                                    <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                    <div class="messageControls">reply | delete | edit </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType firstReportPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">firstReportPost</span>
                                <span class="messageBody">Proin fringilla dapibus nisi at luctus. Donec posuere id magna ut ullamcorper. Duis pretium metus diam, id porta nisi varius et. Nulla facilisi. Morbi massa sapien, semper eget nisi nec, fringilla posuere eros. Sed aliquam lorem quis dolor venenatis laoreet. Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus. Morbi in dui quis erat iaculis interdum. Nam sit amet eros felis. Donec laoreet quam vulputate nisl pellentesque commodo.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType claimNumberPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">claimNumberPost</span>
                                <span class="messageBody">Proin fringilla dapibus nisi at luctus. Donec posuere id magna ut ullamcorper. Duis pretium metus diam, id porta nisi varius et. Nulla facilisi. Morbi massa sapien, semper eget nisi nec, fringilla posuere eros. Sed aliquam lorem quis dolor venenatis laoreet.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div> 
                    
                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reclassifiedPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reclassifiedPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost chat">
                            <div class="messageContent">
                                <span class="messageType diaryPost"><span class="messageIcon" style="background-color:brown;background-image:url('images/avatar_icons/avatar7.png');"></span></span>
                                <span class="messageTitle">diaryPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType valuePost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">valuePost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType systemPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">systemPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reportTimeGoodPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reportTimeGoodPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>    
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reportTimeBadPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reportTimeBadPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>    
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reportTimeOkayPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reportTimeOkayPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType finalCostPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">finalCostPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType adjusterPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">adjusterPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType notesPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">notesPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType photoPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">photoPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>    
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType filePost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">filePost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType newInjuryPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">newInjuryPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reserveBadPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reserveBadPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reserveGoodPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reserveGoodPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType reminderPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">reminderPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType newTaskPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">newTaskPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>    
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType completeTaskPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">completeTaskPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType taskReminderPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">taskReminderPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>                                
                        </div>
                    </div>

                    <div class="messageContainer">
                        <div class="messagePost">
                            <div class="messageContent">
                                <span class="messageType expiredTaskPost"><span class="messageIcon"></span></span>
                                <span class="messageTitle">expiredTaskPost</span>
                                <span class="messageBody">Quisque pellentesque libero eget sem tincidunt, at posuere ex efficitur. Donec vitae turpis eu turpis rutrum mollis at in orci. Vestibulum quis dui justo. Ut varius sit amet mi ac auctor. Sed vitae sollicitudin libero, at feugiat risus.</span>
                                <span class="messageDateTime">12 Sept 2022 | 9:34am</span>
                                <span class="messageReplies">no replies</span>
                                <div class="messageControls">reply | delete | edit </div>
                            </div>    
                        </div>
                    </div>

                </section>
                -->
                <!--
                <div class="placeHolder">
                    Message Area
                    <span>current alerts and messages go here</span>
                </div>-->
                </div>
            </section>
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-cogs"></div> administration</span>
                <div class="sectionContent">
                    @if($isOwner !== NULL)
                    The current company admin is <b id="currentOwnerName">{{$owner_name}}</b>.<br>
                    <span id="subEmailsEnabledSpan" @if($company->gets_unsubscribed_emails === 0) style="display: none" @endif >You currently <b>will</b> receive an email when users disable communications.</span><br>
			        <span id="subEmailsDisabledSpan" @if($company->gets_unsubscribed_emails === 1) style="display: none" @endif>You currently <b>will not</b> receive an email when users disable communications.</span><br>
                    <button class="cyan" id="changeAdmin">change admin info</button>
                    <button id="changeSubEmails" class="cyan">Change User Unsubscribed Emails</button>
                  
                    @else
                    The current company admin is <b id="currentOwnerName">{{$owner_name}}</b>.<br>
                    <span id="subEmailsEnabledSpan" @if($company->gets_unsubscribed_emails === 0) style="display: none" @endif >You currently <b>will</b> receive an email when users disable communications.</span><br>
			        <span id="subEmailsDisabledSpan" @if($company->gets_unsubscribed_emails === 1) style="display: none" @endif>You currently <b>will not</b> receive an email when users disable communications.</span><br>

                    @endif
                    <button class="cyan" id="downloadCert"><a style="text-decoration:none !important;color:white" href="/renderCert/{{$company->id}}">Download Certificate</a></button>
                </div>
            </section>
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-company"></div> company details</span>
                <div class="sectionContent">
                    company phone: <span id="currentCompanyPhone">{{ $company->company_phone }}</span><br>
                    mailing address: <span id="currentCompanyMailingAddress">{{ $company->mailing_address }}</span><br>
                    mailing address line 2: <span id="currentCompanyMailingAddressLine2">{{ $company->mailing_address_line_2 }}</span><br>
                    city: <span id="currentCompanyCity">{{ $company->mailing_city }}</span><br>
                    state: <span id="currentCompanyState">{{ $company->mailing_state }}</span><br>
                    zip code: <span id="currentCompanyZip">{{ $company->mailing_zip }}</span><br>
                             
                    <button class="cyan" id="changeCompanyInfo">edit company info</button>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-file-text"></div> policy info</span>
                <div class="sectionContent">
                    main class code: <span id="currentClassCode">{{ $company->class_code }}</span><br>
                    policy number: <span id="currentPolicyNumber">{{ $company->policy_number }}</span><br>
                    renewal date day: <span id="currentRenewalDay">{{ $day }}</span><br>
                    renewal date month: <span id="currentRenewalMonth">{{ $month }}</span><br>
                                  
                    <button class="cyan" id="changePolicyInfo">edit policy info</button>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-insurance"></div> carrier</span>
                <div class="sectionContent">
                    Carrier Name: <span id="currentCarrierName">{{(is_null($carrier) ? "" : $carrier->name)}}</span><br>
                    Carrier Phone: <span id="currentCarrierPhone">{{(is_null($carrier) ? "" : $carrier->phone)}}</span><br>
                    Carrier Email: <span id="currentCarrierEmail">{{ (is_null($carrier) ? "" : $carrier->email)}}</span><br>
                                       
                    <button class="cyan" id="changeCarrier">edit carrier</button>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-credit-card"></div> billing</span>
                <div class="sectionContent">
                        <span id="hasCard" {{($has_card ? "" : "style=display:none")}}>
                            {{$company->company_name}} is set to use the card ending in <b><span id="cardLast4">{{$company->pm_last_four}}</span></b>
                        </span>
                        <span id="noCard" {{($has_card ? "style=display:none" : "")}}>
                                No card on file for {{$company->company_name}}. Add one now?
                        </span>
                    <button class="cyan showCompanyModal" data-modalcontent="#billingModal">change billing settings</button>
                </div>
            </section>

		</div>
	</div>
</div>


<!-- company info modal -->
<div id="edit_companyinfo" class="zModal" style="display:none">
    <div class="modalContent">
        <div class="modalHeader"><span class="modalTitle">Edit Company Info</span></div>
        <div class="modalBody" style="min-width:260px;">
                <section class="sectionPanel">
                    <span class="sectionTitle"><div class="icon icon-company"></div> Update Company Information</span>
                    <div class="sectionContent">
                    <section class="formBlock dark">                          
                        <div class="formGrid">  
                            <div class="formInput">
                                <!-- input -->
                                <label for="companyPhone">company phone</label>
                                <input id="companyPhone" type="text" value="{{ $company->company_phone }}"/>
                                <span class="inputError">error</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="mailingAddress">mailing address</label>
                                <input id="mailingAddress" type="text" value="{{ $company->mailing_address }}"/>
                                <span class="inputError">error</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="mailingAddressLine2">mailing address line 2</label>
                                <input id="mailingAddressLine2" type="text" value="{{ $company->mailing_address_line_2 }}" />
                                <span class="inputError">error</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="mailingCity">mailing city</label>
                                <input id="mailingCity" type="text" value="{{ $company->mailing_city }}" />
                                <span class="inputError">error</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="mailingState">mailing state</label>
                                <input id="mailingState" type="text" value="{{ $company->mailing_state }}" />
                                <span class="inputError">error</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="mailingZip">mailing zip code</label>
                                <input id="mailingZip" type="text" value="{{ $company->mailing_zip }}" />
                                <span class="inputError">error</span>
                            </div>
                            <button id="submitCompanyInfo" class="cyan">submit</button>
                        </div>
                    </div>                    
                </section>           
        </div>
    </div> 
</div>

<!-- policy info modal -->
<div id="edit_policyinfo" class="zModal" style="display:none">
    <div class="modalContent">
        <div class="modalHeader"><span class="modalTitle">Edit Policy Info</span></div>
        <div class="modalBody" style="min-width:260px;">
                <section class="sectionPanel">
                    <span class="sectionTitle"><div class="icon icon-clipboard"></div> Update Policy Information</span>
                    <div class="sectionContent">
                    <section class="formBlock dark">                          
                        <div class="formGrid">  
                            <div class="formInput">
                                <!-- input -->
                                <label for="classCode">main class code</label>
                                <input id="classCode" type="text" value="{{ $company->class_code }}"/>
                                <span class="inputError">error</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="policyNumber">policy number</label>
                                <input id="policyNumber" type="text" value="{{ $company->policy_number }}"/>
                                <span class="inputError">error</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="renewalDay">renewal day</label>
                                <input id="renewalDay" type="text" value="{{ $day }}"/>
                                <span class="inputError">error</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="renewalMonth">renewal month</label>
                                <input id="renewalMonth" type="text" value="{{ $month }}"/>
                                <span class="inputError">error</span>
                            </div>
                            <button class="cyan" id="submitPolicyInfo">submit</button>
                        </div>
                    </section>
                    </div>                    
                </section>           
        </div>
    </div> 
</div>

<!-- zen carrier modal -->
<div id="edit_carrierinfo" class="zModal" style="display:none">
    <div class="modalContent">
        <div class="modalHeader"><span class="modalTitle">Edit Carrier Info</span></div>
        <div class="modalBody" style="min-width:260px;">
                <section class="sectionPanel">
                    <span class="sectionTitle"><div class="icon icon-insurance"></div>Select a ZenCarrier or add a new carrier</span>
                    <div class="sectionContent">
                    <section class="formBlock dark">                          
                        <div class="formGrid">  
                            <div class="formInput">
                                <label for="zenCarrierSelect">Select Zen Carrier</label>
                                <select id="zenCarrierSelect" name="zenCarrierSelect">
                                    <option value="false">None of the Above</option>
                                    <?php
                                    $zen_carriers = \App\ZenCarrier::get();
                                    foreach($zen_carriers as $zen_carrier){
                                        echo "<option value='$zen_carrier->id'";
                                        /*
                                        if($zen_carrier->id === $company->zen_carrier_id){
                                            echo " selected='selected' ";
                                        }
                                        */
                                        echo ">$zen_carrier->carrier_name</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="carrierName">carrier name</label>
                                <input id="carrierName" type="text" />
                                <span class="inputError">error</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="carrierPhone">carrier phone</label>
                                <input id="carrierPhone" type="text" />
                                <span class="inputError">error</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="carrierEmail">carrier email</label>
                                <input id="carrierEmail" type="text" />
                                <span class="inputError">error</span>
                            </div>
                            <button class="cyan" id="submitCarrierInfo">submit</button>
                        </div>
                    </section>
                    </div>                    
                </section>           
        </div>
    </div> 
</div>
@if ($isOwner !== NULL)
<!-- Admin modal -->
<div id="edit_admininfo" class="zModal" style="display:none">
    <div class="modalContent">
        <div class="modalHeader"><span class="modalTitle">Edit Admin</span></div>
        <div class="modalBody" style="min-width:260px;">
                <section class="sectionPanel">
                    <span class="sectionTitle"><div class="icon icon-user"></div>Change Admin Ownership for the Company</span>
                    <div class="sectionContent">
                    <section class="formBlock dark">                          
                        <div class="formGrid" style="flex-direction: column;align-items: center;">  
                            <div class="formInput">
                                <!-- input -->
                                <label for="ownerSelector">Select New Admin</label>
                                <select id="ownerSelector">
                                    @foreach($teamUsers as $teamUser)
                                        <?php 
                                        $this_user = DB::table('users')->where('id', $teamUser->user_id)->first();
                                        if($this_user->uses_squads == 1 && is_null($this_user->deleted_at)){
                                            echo "<option value=" . $this_user->id . ">" . $this_user->name . "</option>";
                                        } 
                                        ?>
                                    @endforeach
                                </select>
                            </div>
                            <div>
                                <button class="cyan" id="submitChangeOwner">submit</button>
                            </div>
                        </div>
                    </section>
                    </div>                    
                </section>           
        </div>
    </div> 
</div>

<!-- Admin Email Sub Modal -->
<div id="edit_adminEmailSub" class="zModal" style="display:none">
    <div class="modalContent">
        <div class="modalHeader"><span class="modalTitle">Edit Admin</span></div>
        <div class="modalBody" style="min-width:260px;">
                <section class="sectionPanel">
                    <span class="sectionTitle"><div class="icon icon-setemail"></div>Change Email Subscriptions</span>
                    <div class="sectionContent">
                    <section class="formBlock dark">                          
                        <div class="formGrid">  
                            <div class="formInput">
                                @if($company->gets_unsubscribed_emails === 0)
                                <!-- input -->
                                <span>Disable User to Unsubscribed from Emails</span>
                                <input id="companyEmailSub" type="hidden" value="1">
                                @else
                                <span>Enable User to Unsubscrib from Emails</span>
                                <input id="companyEmailSub" type="hidden" value="0">
                                @endif
                            </div>
                            <div class="formInput">
                                <button class="cyan" id="submitChangeSubEmails">submit</button>
                            </div>
                        </div>
                    </section>
                    </div>                    
                </section>           
        </div>
    </div> 
</div>
@endif

<!-- COMPANY MODALS -->
<div id="companyModal" class="zModal" style="display: none;">
    <!-- CONTENT FOR billingModal -->
    <div class="modalContent modalBlock" id="billingModal" style="display: none;">
        <div class="modalHeader"><span class="modalTitle">Update Billing Info</span></div>
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <section class="sectionPanel">
                    <span class="sectionTitle"><div class="icon icon-credit-card"></div><span style="opacity:.5">Update Billing Info for {{$company->company_name}}</span></span>
                    <div class="sectionContent">
                        <div id="updateCardDiv">
                            <span>Cardholder's Name</span><input id="updateCardName"><br>
                            <span>Card Details</span><br>
                            <!-- This div will be populated with card inputs by the stripe.js code -->
                            <div id="updateCardCardElement"></div>
                            <div id="updateCardErrors"></div>

                            <button id="updateCardSubmitButton" data-secret="{{$intent->client_secret}}">Update</button>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<!-- END COMPANY MODALS -->
<script>
    var modal;

    $('.showCompanyModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: false,
            //modal ID goes here
            content: $('#companyModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();

        //script related to a specific content block can be added like this
        //status modal
        if(target === "#billingModal"){

        }
        //modal ID goes here
        modal.open();
    });

    //STRIPE CODE
    $(document).ready(function(){
        const stripe = Stripe('<?php echo env("STRIPE_KEY"); ?>');
        const elements = stripe.elements();
        const cardElement = elements.create('card', {
            style: {
                base: {
                    iconColor: '#c4f0ff',
                    color: '#fff',
                    fontWeight: '500',
                    fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
                    fontSize: '16px',
                    fontSmoothing: 'antialiased',
                    ':-webkit-autofill': {
                        color: '#fce883',
                    },
                    '::placeholder': {
                        color: '#87BBFD',
                    },
                },
                invalid: {
                    iconColor: '#FFC7EE',
                    color: '#FFC7EE',
                },
            }});

        cardElement.mount('#updateCardCardElement');

        const cardHolderName = document.getElementById('updateCardName');
        const cardButton = document.getElementById('updateCardSubmitButton');
        const clientSecret = cardButton.dataset.secret;

        cardButton.addEventListener('click', async (e) => {
            $('#updateCardErrors').html("");
        const { setupIntent, error} = await stripe.confirmCardSetup(
            clientSecret, {
                payment_method:{
                    card: cardElement,
                    billing_details: {name: cardHolderName.value}
                }
            }
        );
        if (error) {
            $('#updateCardErrors').html(error.message);
            /*
            console.log(error);
            alert('error: ' + error.message);
            */
        } else {
            console.log(setupIntent['payment_method']);
            updatePaymentMethod(setupIntent['payment_method']);
        }
    });
    });

    function updatePaymentMethod(payment_method){
        $.ajax({
            type: 'POST',
            url: '<?php echo route('updateCompanyBilling'); ?>',
            data:{
                _token: '<?php echo csrf_token(); ?>',
                company_id: '<?php echo $company->id; ?>',
                payment_method: payment_method
            },
            success: function(data){
                showAlert("Billing Info Updated!", "confirm", 5);
                modal.close();
                $('#cardLast4').html(data);
                $('#noCard').hide();
                $('#hasCard').show();
            },
            error: function(data){
                console.log(data);
            }
        })
    }


    //each jBox modal needs to be created with this code block, using seperate variable names for each new modal created
	var changeCompanyInfoModal = new jBox('Modal', {
		attach: '#changeCompanyInfo', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#edit_companyinfo'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true,
		onOpen: function(event){
			
		},
		onCloseComplete: function(event){
			
		}
	});

    var changePolicyInfoModal = new jBox('Modal', {
		attach: '#changePolicyInfo', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#edit_policyinfo'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true,
		onOpen: function(event){
			
		},
		onCloseComplete: function(event){
			
		}
	});

    var changeCarrierInfoModal = new jBox('Modal', {
		attach: '#changeCarrier', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#edit_carrierinfo'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true,
		onOpen: function(event){
			
		},
		onCloseComplete: function(event){
			
		}
	});

    var changeAdminInfoModal = new jBox('Modal', {
		attach: '#changeAdmin', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#edit_admininfo'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true,
		onOpen: function(event){
			
		},
		onCloseComplete: function(event){
			
		}
	});

    var changeAdminEmailSubModal = new jBox('Modal', {
		attach: '#changeSubEmails', //id/class of the element that should open the modal
		addClass: 'zBox',
		content: $('#edit_adminEmailSub'), //id of the modal div itself
		isolateScroll: true,
		responsiveWidth: true,
		onOpen: function(event){
			
		},
		onCloseComplete: function(event){
			
		}
	});
	
	//*** CUSTOM CLOSE BUTTON CODE ***//
	//the variable storing each modal should be added to the modal array
    var modal_array = [];
	modal_array.push(changeCompanyInfoModal);
    modal_array.push(changePolicyInfoModal);
    modal_array.push(changeCarrierInfoModal);
    modal_array.push(changeAdminInfoModal);
    modal_array.push(changeAdminEmailSubModal);
	
	function closeModals(){
	    for(var i = 0; i < modal_array.length; i++){
	        modal_array[i].close();
	    }
	}
	
	$('.close-zModal').on('click', function(){
	    for(var i = 0; i < modal_array.length; i++){
	        modal_array[i].close();
	    }
	});

    //Submitting company info modal
    $('#submitCompanyInfo').on('click', function(){
        //company id
        var companyId = {{ $company->id }};
        //getting values from modal
        var companyPhone = $('#companyPhone').val();
        var companyMailingAddress = $('#mailingAddress').val();
        var companyMailingAddressLine2 = $('#mailingAddressLine2').val();
        var companyMailingCity = $('#mailingCity').val();
        var companyMailingState = $('#mailingState').val();
        var companyMailingZip = $('#mailingZip').val();

        var valid = true;

        //simple js validation
        if(companyPhone == ""){
            valid = false;
            $('#companyPhone').parent().find('.inputError').addClass('show');
        }

        if(companyMailingAddress == ""){
            valid = false;
            $('#companyMailingAddress').parent().find('.inputError').addClass('show');
        }

        if(companyMailingCity == ""){
            valid = false;
            $('#mailingCity').parent().find('.inputError').addClass('show');
        }

        if(companyMailingState == ""){
            valid = false;
            $('#mailingState').parent().find('.inputError').addClass('show');
        }

        if(companyMailingZip == ""){
            valid = false;
            $('#mailingZip').parent().find('.inputError').addClass('show');
        }

        if(valid){
            $.ajax({
                type: 'POST',
                url: '{{ route("updateCompanyInfo") }}',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    companyId: companyId,
                    phone: companyPhone,
                    mailingAddress: companyMailingAddress,
                    mailingAddressLine2: companyMailingAddressLine2,
                    mailingCity: companyMailingCity,
                    mailingState: companyMailingState,
                    mailingZip: companyMailingZip
        
                },
                success: function(){
                    updateCompanyInfoFields();
                    closeModals();
                    showAlert("Thanks for your update!", "confirm", 5);
                },
                error: function(){
                    showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
                }
        
            });
        }
        
    });

    //submit policy info modal
    $("#submitPolicyInfo").on('click', function(){
        //grab the company id
        var companyId = {{ $company->id }};
        //get the variables from the modal
        var classCode = $('#classCode').val();
        var policyNumber = $('#policyNumber').val();
        var renewalDay = $('#renewalDay').val();
        var renewalMonth = $('#renewalMonth').val();
        
        //validation variable
        var valid = true;

        if(classCode === "" || classCode === undefined){
            valid = false;
            $('#classCode').parent().find('.inputError').addClass('show');
        }

        if(policyNumber === "" || policyNumber === undefined){
            valid = false;
            $('#policyNumber').parent().find('.inputError').addClass('show');
        }

        if(renewalDay > 31 || renewalDay === "" || renewalDay === undefined){
            valid = false;
            $('#renewalDay').parent().find('.inputError').addClass('show');
        }

        if(renewalMonth > 12 || renewalMonth === "" || renewalMonth === undefined){
            valid = false;
            $('#renewalMonth').parent().find('.inputError').addClass('show');
        }

        if(valid === true){
            $.ajax({
                type: 'POST',
                url: '{{ route("updatePolicyInfo") }}',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    companyId: companyId,
                    classCode: classCode,
                    policyNumber: policyNumber,
                    renewalDay: renewalDay,
                    renewalMonth: renewalMonth
                },
                success: function(){
                    updatePolicyInfoFields();
                    closeModals();
                    showAlert("Thanks for your update!", "confirm", 5);
                },
                error: function(){
                    showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
                }
        
            });
        }

    });

    //submit carrier info
    $('#submitCarrierInfo').on('click', function(){
        var zen_carrier = $('#zenCarrierSelect').val();
        if(zen_carrier === "false"){
            var valid = true;
            var name = $('#carrierName').val();
            var phone = $('#carrierPhone').val();
            var email = $('#carrierEmail').val();
            if(phone === "" || phone === undefined){
                valid = false;
                $('#carrierPhone').parent().find('.inputError').addClass('show');
            }
            if(email === "" || email === undefined){
                valid = false;
                $('#carrierEmail').parent().find('.inputError').addClass('show');
            }
            if(name === "" || name === undefined){
                valid = false;
                $('#carrierName').parent().find('.inputError').addClass('show');
            }
            if(valid === true){
                addNewCarrier(name, phone, email);
            }
        }else{
            addZenCarrier(zen_carrier);
        }
    });

    function addNewCarrier(name, phone, email){
        $.ajax({
            type: 'POST',
            url: '{{ route("addNewCarrier") }}',
            data: {
                _token : '<?php echo csrf_token(); ?>',
                name: name,
                phone: phone,
                email: email,
                company_id: "{{ $company->id }}",
            },
            success: function(data){
                updateCarrierInfoFields(data);
                closeModals();
                showAlert("Thanks for your update!", "confirm", 5);
            },
            error: function(data){
                showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
            }
        });
    }
    
    function addZenCarrier(zencarrier_id){
        $.ajax({
            type: 'POST',
            url: '{{ route("selectZenCarrier") }}',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                company_id: '{{ $company->id }}',
                zencarrier_id: zencarrier_id
            },
            success: function(data){
                updateCarrierInfoFields(data);
                console.log(data);
                closeModals();
                showAlert("Thanks for your update!", "confirm", 5);
            },
            error: function(data){
                showAlert("Sorry, an error occurred. Please try again later.", "deny", 5);
            }
        });
    }

    $('#submitChangeOwner').on('click', function(){
        var oldOwnerId = '<?php echo Auth::user()->id; ?>';
        var newOwnerId = $('#ownerSelector').val();
        var teamId = '<?php echo $teamID; ?>';
        $.ajax({
            type: 'POST',
            url: '<?php echo route("changeCompanyOwner") ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>', 
                oldOwnerId: oldOwnerId, 
                newOwnerId: newOwnerId, 
                teamId: teamId
            },
            success: function(){
                location.reload(true);
            },
            error: function(data){
                var errorMsg = JSON.parse(data.responseText);
                showAlert("Sorry, an error occurred. Please try again later.", "deny", 5);
            }
        });
    });

    $('#submitChangeSubEmails').on('click', function(){
        var changeEmailSub = $('#companyEmailSub').val();
        $.ajax({
            type: 'POST',
            url: '<?php echo route("changeCompanyEmailSubscription") ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                changeEmailSub: changeEmailSub,
                companyId: '{{ $company->id }}'
            },
            success: function(){
                location.reload(true);
            },
            error: function(data){
                showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
            }
        });
    });
    
    function updateCompanyInfoFields(){
        var phone = $('#companyPhone').val();
        var address = $('#mailingAddress').val();
        var addressLine2 = $('#mailingAddressLine2').val();
        var city = $('#mailingCity').val();
        var state = $('#mailingState').val();
        var zip = $('#mailingZip').val();

        //sectionPanel on page
        $('#currentCompanyPhone').text(phone);
        $('#currentCompanyMailingAddress').text(address);
        $('#currentCompanyMailingAddressLine2').text(addressLine2);
        $('#currentCompanyMailingCity').text(city);
        $('#currentCompanyMailingState').text(state);
        $('#currentCompanyMailingZip').text(zip);
        
        //update input fields on update claim

        //remove errors from modal
        $('#companyPhone').parent().find('.inputError').removeClass('show');
        $('#companyMailingAddress').parent().find('.inputError').removeClass('show');
        $('#mailingCity').parent().find('.inputError').removeClass('show');
        $('#mailingState').parent().find('.inputError').removeClass('show');
        $('#mailingZip').parent().find('.inputError').removeClass('show');
    }

    function updatePolicyInfoFields(){
        var classCode = $('#classCode').val();
        var policyNumber = $('#policyNumber').val();
        var renewalDay = $('#renewalDay').val();
        var renewalMonth = $('#renewalMonth').val();

        //update section on page
        $('#currentClassCode').text(classCode);
        $('#currentPolicyNumber').text(policyNumber);
        $('#currentRenewalDay').text(renewalDay);
        $('#currentRenewalMonth').text(renewalMonth);

        //remove errors from modal
        $('#classCode').parent().find('.inputError').removeClass('show');
        $('#policyNumber').parent().find('.inputError').removeClass('show');
        $('#renewalDay').parent().find('.inputError').removeClass('show');
        $('#renewalMonth').parent().find('.inputError').removeClass('show');
    }  
    
    function updateCarrierInfoFields(carrier){
        $('#currentCarrierName').text(carrier['name']);
        $('#currentCarrierPhone').text(carrier['phone']);
        $('#currentCarrierEmail').text(carrier['email']);

        $('#carrierName').text("");
        $('#carrierPhone').text("");
        $('#carrierEmail').text("");

        $('#carrierName').parent().find('.inputError').removeClass('show');
        $('#carrierPhone').parent().find('.inputError').removeClass('show');
        $('#carrierEmail').parent().find('.inputError').removeClass('show');
    }
</script>
	


@endsection

@extends('layouts.zen5_layout')
@section('maincontent')

<style>
.companyTag{margin-left:12px;display:inline-block;border:1px solid black;border-radius:4px;padding:6px 12px;position:relative;width:200px;}
.companyTag span{text-align:right;position:absolute;right:46px;top:19px;color:white;white-space:noWrap;font-size:.8rem;width:160px;overflow:hidden;text-overflow:ellipsis;}
.companyTag img{position:absolute;right:12px;top:14px;height:28px;width:28px;}

ul.newPolicyholderList{list-style:none;}
ul.newPolicyholderList li{margin:2px 0;} 

#policyHolders input:checked + .etab-label, #policyHolders .etab-label:hover {background: #de970a;}
#policyHolders .etab-content{background: #7d6534;}
#policyHolders .etab-content ul {list-style:none;width:100%;padding:0;margin:0;}
#policyHolders .etab-content li {display:block;width:100%;background-color:transparent;}

#claimActivity input:checked + .etab-label, #claimActivity .etab-label:hover {background: #5cb905;}
#claimActivity .etab-content{background: #3e621e;}
#claimActivity .etab-content ul {list-style:none;width:100%;padding:0;margin:0;}
#claimActivity .etab-content li {display:block;width:100%;background-color:transparent;}

.pageControls{border:1px solid rgba(0, 0, 0, 0.64);margin:10px 16px;height:30px;background-color:rgba(0, 0, 0, 0.18);border-radius:4px;}
.pageControls span.claimsPageNumbers{color:white;font-size:12px;position:relative;top:5px;}
.pageControls .icon{height:26px;}
ul#claimContainer {list-style:none;margin:0;padding:0;}
ul#claimContainer ul{list-style:none;padding:0;margin:0;}
ul#claimContainer li{transition: all 0.35s;margin-bottom:0px;padding:0px;}
ul#claimContainer li .itemTitle{position:relative;width:100%;height:70px;background-color:black;border-top-left-radius:12px;border-top-right-radius:12px;}
ul#claimContainer li .itemTitle .itemEmployee{position:absolute;top:10px;left:10px;height:50px;}
ul#claimContainer li .itemTitle .itemEmployee img.userAvatar{width:50px;height:50px;top:0px;left:0px;}
ul#claimContainer li .itemTitle .itemEmployee:hover span.name{cursor:pointer;color:#7777ff;}
ul#claimContainer li .itemTitle .itemEmployee img{display:inline-block;margin-right:20px;}
ul#claimContainer li .itemTitle .itemEmployee span.name{position:absolute;width:100px;top:16px;left:60px;font-size:1rem;weight:900;white-space:noWrap;font-size:1rem;width:160px;overflow:hidden;text-overflow:ellipsis;}
ul#claimContainer li .itemTitle .itemCompany{position:absolute;top:2px;right:2px;}
ul#claimContainer li .itemBody{position:relative;}
ul#claimContainer li .itemBody .itemSeverity{position:absolute;top:-10px;right:5px;font-size:.7rem;}
ul#claimContainer li .itemBody .itemSeverity span.level{font-size:.8rem;}
ul#claimContainer li .itemBody .itemSeverity span.level.severe{color:red;}
ul#claimContainer li .itemBody .itemSeverity span.level.moderate{color:orange;}
ul#claimContainer li .itemBody .itemSeverity span.level.firstaid{color:yellow;}
ul#claimContainer li .itemBody .itemNewItems{cursor:pointer;width:max-content;padding:2px 10px;border-top-left-radius:21px;border-bottom-left-radius:21px;border-top-right-radius:4px;border-bottom-right-radius:4px;background-color:rgba(0,   0,   0, 0.12);margin-bottom:12px;}
ul#claimContainer li .itemBody .itemNewItems:hover{background-color:#ffffff35;}
ul#claimContainer li .itemBody .itemNewItems .icon{padding-right:12px;font-size:18px;position:relative;top:3px;left:-4px;}
ul#claimContainer .itemNum, .etabs .itemNum{color:white;font-size:16px;}
ul#claimContainer .itemNum0, .etabs .itemNum0{color:rgba(255, 255, 255, 0.58);font-size:16px;font-style:italic;}
ul#claimContainer .itemClickDesc, .etabs .itemClickDesc{color:white;font-size:12px;font-style:italic;}
ul#claimContainer .icon, .etabs .icon{display:inline-block;}
ul#claimContainer li .itemBody{width:100%;margin-top:10px;padding:10px 0 0 20px;border-top:1px solid black;}

ul.costlist{margin:-12px 0 15px 40px;padding:6px;background-color:rgba(73, 73, 73, 1);border-top-right-radius:6px;border-bottom-right-radius:6px;border-bottom-left-radius:6px;width:max-content;}
ul.costlist li{padding:0;margin:0;background-color:transparent;border:0;}
ul.costlist li:hover{background-color:transparent;border:0;}
ul.costlist li span{font-size:14px;padding-left:6px;color:#c1c1c1;}
ul.costlist li .icon{position:relative;top:3px;}
ul.costlist li.green .icon{color:#00a400;}
ul.costlist li.red .icon{color:#ce2a2a;}
.sectionControls{width:100%;height:100px;position:relative;margin-top:20px;}
.sectionControls .description{display:block;width:100%;padding:0 20px 0 20px;font-size:1.1rem;text-align:center;}
.dateSelector{width:100%;text-align:center;} 

@media screen and (max-width: 580px) {
    .sectionControls{margin-top:0px;}
    .dateSelector{width:100%;text-align:center;} 
}
</style>

<!-- ******* GET PHP DATA -->
<?php
$user = Auth::user();

if($user->type !== "internal"){
    $is_agent = true;
}else{
    $is_agent = false;
}

if($date !== NULL){
    $last_update = \Carbon\Carbon::createFromFormat("Y-m-d", $date)->startOfDay();
}else{
    $last_update = \Carbon\Carbon::today()->subWeek();
}

$injuries = $user->returnInjuryIDs(false);

$user_squad_ids = DB::table('squad_members')->where('user_id', Auth::user()->id)->pluck('squad_id');
$resolved_injuries = \App\Injury::join('users', 'users.id', '=', 'injuries.injured_employee_id')->join('companies', 'companies.id', '=', 'injuries.company_id')->whereIn('injuries.squad_id', $user_squad_ids)->where('injuries.resolved', 1)->where('injuries.resolved_at', '>', $last_update)
    ->select('injuries.*', 'users.name as injured_employee_name', 'users.picture_location as user_avatar', 'companies.company_name', 'companies.logo_location as company_logo')->get();

$new_user_companies = DB::table('external_users_companies')->where('external_users_companies.user_id', Auth::user()->id)->join('companies', 'companies.id', '=', 'external_users_companies.company_id')->where('companies.activated', 1)->where(function($query) use($last_update){
    $query->where('companies.registered_at', '>', $last_update)->orWhere('companies.created_at', '>', $last_update);
})->get();

?>

<div class="pageContent bgimage-bgheader pagebackground9 fade-in">
		<!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>weekly update</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="/images/icons/summary.png"></span><span class="subContent">get a summary week by week</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/date.png">
            </div> 
        </div>           
	</div>


	<div class="contentBlock lessPadding">
		<div class="container setMinHeight">

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-calendar"></div>Showing Updates since <b>{{$last_update->format("M d, Y")}}</b></span>
                <div class="sectionControls">
                    <span class="description">By default this page shows a <b>current weekly summary</b>, but you can use the date selector to view all activity from any date!</span>
                    
                    <div class="dateSelector">
                        <section class="formBlock dark">   
                            <div class="formGrid tight">
                                <!--<div id="injuryDate"></div>-->
                                <div class="formInput">
                                    choose date:
                                    <input type="date" id="weeklySummaryDatePicker" name="weeklySummaryDatePicker"
                                    pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}">
                                    <button class="cyan centered" id="updateDateButton">set start date</button>
                                </div>
                            </div>
                        </section> 
                    </div>       

                </div>

                <div class="divider">updates</div><br>

                <div class="flexPanel">
                    <div class="viewControl">
                        <div class="compactViewLeft animate__animated animate__bounceInLeft">
                            <span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/insurance.png">
                            <?php if(count($new_user_companies) === 0){echo "no new policy holders";}else if(count($new_user_companies) > 0){echo "new policy holders";}?>
                            </span>
                        </div>
                        <div class="compactViewExpand hideMobile">[ expand ]</div>
                        <div class="compactViewRight animate__animated animate__bounceInRight">
                            <ul>
                                <li><span class="value FG__green"><b><?=count($new_user_companies)?></b></span> <span class="icon"><img src="/images/icons/insurancenum.png"></span></li>
                            </ul>
                            <div class="compactViewX">[&nbsp;minimize&nbsp;]</div>
                        </div>
                    </div>
                    <div class="viewContent">
                        <div class="zen-actionPanel transparent tight">
                            <div class="sectionContent dynamicSize">
                                <b><?=count($new_user_companies)?> new policy holders since {{$last_update->format("M d, Y")}}.</b> 
                                <ul class="newPolicyholderList">
                                <?php
                                    foreach($new_user_companies as $company){
                                        $company_avatar = $company->logo_location;
                                        $randmin=1;$randmax=9;
                                        $company_rand = rand($randmin,$randmax);
                                        if(is_null($company_avatar)){
                                            $company_avatar = "/images/default_logos/company" . $company_rand . ".png";
                                        }
                                        if(is_null($company->registered_at)){
                                            $joined_on = $company->created_at;
                                        }else $joined_on = $company->registered_at;

                                        echo '<li data-company_id="' . $company->id . '"><img class="companyIcon" style="height:24px;width:24px;" src="' . $company_avatar . '"><span class="boldText">' . $company->company_name . '</span> - <span class="italicText">Joined on </span><span class="dateTime">' . $joined_on .'</span></li>';
                                    }
                                ?>
                                </ul>
                            </div>					
                        </div>
                    </div>				
                </div>


                <div class="flexPanel">
                    <div class="viewControl">
                        <div class="compactViewLeft animate__animated animate__bounceInLeft">
                            <span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/plus.png">
                            <?php if(count($resolved_injuries) === 0){echo "no new resolved claims";}else if(count($resolved_injuries) > 0){echo "new resolved claims";}?>
                            </span>
                        </div>
                        <div class="compactViewExpand hideMobile">[ expand ]</div>
                        <div class="compactViewRight animate__animated animate__bounceInRight">
                            <ul>
                                <li><span class="value FG__green"><b><?=count($resolved_injuries)?></b></span> <span class="icon"><img src="/images/icons/checkheart.png"></span></li>
                            </ul>
                            <div class="compactViewX">[&nbsp;minimize&nbsp;]</div>
                        </div>
                    </div>
                    <div class="viewContent">
                        <div class="zen-actionPanel transparent tight">
                            <div class="sectionContent dynamicSize">
                                <b><?=count($resolved_injuries)?> new resolved claims since {{$last_update->format("M d, Y")}}.</b> 
                                <ul class="newPolicyholderList">
                                    <?php
                                        foreach($resolved_injuries as $injury){
                                            $user_avatar = $injury->user_avatar;
                                            $company_avatar = $injury->company_logo;
                                            if(is_null($user_avatar)){
                                                $user_avatar = "/images/avatars/no_profile_image.jpg";
                                            }

                                            if(is_null($company_avatar)){
                                                $company_avatar = "/images/avatars/no_profile_image.jpg";
                                            }

                                            echo '<li class="resolvedClaimListItem" data-url="' . route('communicationLink', ['id' => $injury->id]) . '">
                                                    <div class="itemBlock">
                                                    <img class="avatarIcon" style="width:30px;height:30px;border-radius:50%;" src="' . $user_avatar . '">
                                                    <span class="boldText">
                                                    ' . $injury->injured_employee_name . '
                                                    </span> -
                                                    <div class="companyTag">
                                                    <img class="avatarIcon" src="
                                                    ' . $company_avatar . '
                                                    ">
                                                    <span class="boldText">
                                                    ' . $injury->company_name . '
                                                    </span>
                                                    </div> -
                                                    <span class="italicText">Resolved on </span>
                                                    <span class="dateTime">
                                                    ' . $injury->resolved_at . '
                                                    </span>
                                                    </span></div></li>';
                                                                    }
                                    ?>
                                </ul>
                            </div>					
                        </div>
                    </div>				
                </div>
                <div class="divider">activity</div><br>
                <!-- ******* CLAIM UPDATES -->
                    <ul id="claimContainer">default text
                    <?php

                    $injury_array = [];
                    $injury_count = 0;
                    foreach($injuries as $injury){

                        $company = \App\Company::where('id', $injury->company_id)->first();
                        $injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();
                        if($injured_employee === NULL){
                            continue;
                        }
                        //new tree posts
                        $tree_posts = \App\TreePost::where('injury_id', $injury->id)->where('created_at', '>', $last_update)->get();

                        $user_avatar = $injured_employee->picture_location;
                        $company_avatar = $company->logo_location;

                        if(is_null($user_avatar)){
                            $user_avatar = "/images/utility/avatar_default.png";
                        }

                        if(is_null($company_avatar)){
                            $company_avatar = "/images/utility/companylogo.png";
                        }

                        $show_claim = 0;

                        if($injury->claim_number !== NULL && $injury->claim_number !== ""){
                            $claim_number = $injury->claim_number;
                        }else{
                            $claim_number = "No Claim Number Added";
                        }

                        $severity_class = "firstaid";

                        if($injury->severity === "Moderate"){
                            $severity_class = "moderate";
                        }else if($injury->severity === "Severe"){
                            $severity_class = "severe";
                        }

                        $injury_date = \Carbon\Carbon::createFromFormat("m-d-Y g:i a", $injury->injury_date)->format('m-d-y h:i a');

                        $html = "<li>
                            <div class='sectionPanel gradient' style='max-width:1030px;' data-url='" . route('communicationLink', ['id' => $injury->id]) . "'>" .
                            "<div class='itemTitle'>" .
                            "<div class='itemEmployee'><img class='userAvatar' src='" . $user_avatar . "'> <span class='name'>$injured_employee->name </div>" .
                            "<div class='itemCompany'><div class='companyTag'><img class='avatarIcon' src='" . $company_avatar . "'><span class='boldText'>$company->company_name</span></div></div>" .
                            "</div><div class='itemBody'><span class='itemSeverity'><span class='level $severity_class'>$injury->severity</span> - $claim_number - $injury_date</span>";

                        if(count($tree_posts) === 0){
                            $html.= "<div class='itemNewItems zencareLink' data-injuryid='" . $injury->id . "'><div class='icon icon-comment'></div><span class='itemNum0'>No new posts</span></div>";
                        }else if(count($tree_posts) === 1){
                            $html.= "<div class='itemNewItems zencareLink' data-injuryid='" . $injury->id . "'><div class='icon icon-comment'></div><span class='itemNum'>1 new post</span> - <span class='itemClickDesc'>click to view!</span></div>";
                        }else{
                            $html.= "<div class='itemNewItems zencareLink' data-injuryid='" . $injury->id . "'><div class='icon icon-comments'></div><span class='itemNum'>" . count($tree_posts) . " new posts</span> - <span class='itemClickDesc'>click to view!</span></div>";
                        }

                        if(count($tree_posts) > 0){
                            $show_claim = 1;
                        }

                        $html.= "<div class='itemNewItems'>";

                        //new claim costs
                        $medical = \App\MedicalCosts::where('injury_id', $injury->id)->where('current', 1)->where('created_at', '>', $last_update)->first();
                        $legal = \App\LegalCosts::where('injury_id', $injury->id)->where('current', 1)->where('created_at', '>', $last_update)->first();
                        $indemnity = \App\IndemnityCosts::where('injury_id', $injury->id)->where('current', 1)->where('created_at', '>', $last_update)->first();
                        $misc = \App\MiscellaneousCosts::where('injury_id', $injury->id)->where('current', 1)->where('created_at', '>', $last_update)->first();
                        $reserve_medical = \App\ReserveMedicalCost::where('injury_id', $injury->id)->where('current', 1)->where('created_at', '>', $last_update)->first();
                        $reserve_legal = \App\ReserveLegalCost::where('injury_id', $injury->id)->where('current', 1)->where('created_at', '>', $last_update)->first();
                        $reserve_indemnity = \App\ReserveIndemnityCost::where('injury_id', $injury->id)->where('current', 1)->where('created_at', '>', $last_update)->first();
                        $reserve_misc = \App\ReserveMiscellaneousCost::where('injury_id', $injury->id)->where('current', 1)->where('created_at', '>', $last_update)->first();

                        if(!is_null($medical) || !is_null($legal) || !is_null($indemnity) || !is_null($misc) || !is_null($reserve_medical) ||
                            !is_null($reserve_legal) || !is_null($reserve_indemnity) || !is_null($reserve_misc)){

                            //$html.=
                            $claim_cost_html = "";
                            $claim_cost_count = 0;

                            if(!is_null($medical) && $medical->cost !== ""){
                                $claim_cost_html.= "<li class='green'><div class='icon icon-stop'></div><span class='costSpan'>New Medical Cost: <b>$" . $medical->cost . "</b></span></li>";
                                $claim_cost_count++;
                            }

                            if(!is_null($legal) && $legal->cost !== ""){
                                $claim_cost_html.= "<li class='green'><div class='icon icon-stop'></div><span class='costSpan'>New Legal Cost: <b>$" . $legal->cost . "</b></span></li>";
                                $claim_cost_count++;
                            }

                            if(!is_null($indemnity) && $indemnity->cost !== ""){
                                $claim_cost_html.= "<li class='green'><div class='icon icon-stop'></div><span class='costSpan'>New Indemnity Cost: <b>$" . $indemnity->cost . "</b></span></li>";
                                $claim_cost_count++;
                            }

                            if(!is_null($misc) && $misc->cost !== ""){
                                $claim_cost_html.= "<li class='green'><div class='icon icon-stop'></div><span class='costSpan'>New Misc Cost: <b>$" . $misc->cost . "</b></span></li>";
                                $claim_cost_count++;
                            }

                            if(!is_null($reserve_medical) && $reserve_medical->cost !== ""){
                                $claim_cost_html.= "<li class='red'><div class='icon icon-stop'></div><span class='costSpan'>New Reserve Medical Cost: <b>$" . $reserve_medical->cost . "</b></span></li>";
                                $claim_cost_count++;
                            }

                            if(!is_null($reserve_legal) && $reserve_legal->cost !== ""){
                                $claim_cost_html.= "<li class='red'><div class='icon icon-stop'></div><span class='costSpan'>New Reserve Legal Cost: <b>$" . $reserve_legal->cost . "</b></span></li>";
                                $claim_cost_count++;
                            }

                            if(!is_null($reserve_indemnity) && $reserve_indemnity->cost !== ""){
                                $claim_cost_html.= "<li class='red'><div class='icon icon-stop'></div><span class='costSpan'>New Reserve Indemnity Cost: <b>$" . $reserve_indemnity->cost . "</b></span></li>";
                                $claim_cost_count++;
                            }

                            if(!is_null($reserve_misc) && $reserve_misc->cost !== ""){
                                $claim_cost_html.= "<li class='red'><div class='icon icon-stop'></div><span class='costSpan'>New Reserve Misc Cost: <b>$" . $reserve_misc->cost . "</b></span></li>";
                                $claim_cost_count++;
                            }

                            $show_claim = 1;

                            $claim_cost_html.= "</ul>";
                            $claim_cost_html = "<div class='icon icon-claimcosts'></div><span class='itemNum'>$claim_cost_count new claim cost updates</span></div><ul class='costList'>" . $claim_cost_html;
                            $html.= $claim_cost_html;


                        }else{
                            $html.= "<div class='icon icon-claimcosts'></div><span class='itemNum0'>No changes to costs</span></div>";
                        }

                        //new appointments
                        $appointments = \App\Appointment::where('appointments.injury_id', $injury->id)->where('appointments.created_at', '>', $last_update)->where('appointments.cancelled', 0)
                            ->join('injuries_care_locations', 'appointments.location_id', '=', 'injuries_care_locations.id')->select('appointments.*', 'injuries_care_locations.name', 'injuries_care_locations.email', 'injuries_care_locations.phone')
                            ->get();

                        $html.= "<div class='itemNewItems'><div class='icon icon-clinic'></div>";

                        if(count($appointments) === 0){
                            $html.= "<span class='itemNum0'>No appointments</span></div>";
                        }else if(count($appointments) === 1){
                            $html.= "<span class='itemNum'>1 new appointment</span> - <span class='itemClickDesc'>click to view</span></div>";
                        }else{
                            $html.= "<span class='itemNum'>" . count($appointments) . " new appointments</span> - <span class='itemClickDesc'>click to view</span></div>";
                        }

                        $injury_docs = \App\InjuryFile::where('injury_id', $injury->id)->where('created_at', $injury->created_at)->get();
                        $injury_photos = \App\InjuryImage::where('injury_id', $injury->id)->where('created_at', $injury->created_at)->get();

                        if(count($injury_docs) > 0 || count($injury_photos) > 0){
                            $html.= "<div class='itemNewItems'><div class='icon icon-picture'></div><span class='itemNum'>New Files: " . count($injury_docs) . " New Photos: " . count($injury_photos) . "</span></div>";
                        }else $html.= "<div class='itemNewItems'><div class='icon icon-picture'></div><span class='itemNum0'>No new files/photos for this injury!</span></div>";

                        $html.= "</div></div></li>";

                        if($show_claim){
                            // echo $html;
                            $injury_array[$injury_count] = $html;
                            $injury_count++;
                        }

                    }
                    ?>
                    </ul>

         </section>
			
		</div>
	</div>
</div>


<script>
        injury_array = <?php echo json_encode($injury_array); ?>;

        var page_count = 0;

        var claim_count = injury_array.length;

        var current_page_number = 1;

        var max_page_number = Math.ceil((claim_count/10));


        $('.claimsPreviousButton').on('click', function(){
            if(page_count !== 0){
                page_count = page_count - 10;
                if(page_count < 0){
                    page_count = 0;
                }
                current_page_number--;
                writeClaims();
            }
        });

        $('.claimsNextButton').on('click', function(){
            if(current_page_number !== max_page_number){
                page_count = page_count + 10;
                current_page_number++;
                writeClaims();
            }
        });

        function writeClaims(){
            var html_string = "";

            for(var i = 0; i < 10; i++){
                if(injury_array[page_count + i] !== undefined){
                    html_string+= injury_array[page_count + i];
                }
            }

            $('.currentPage').html(current_page_number);
            $('.totalPages').html(max_page_number);
            $('#claimContainer').html(html_string);
        }

        writeClaims();

        console.log(injury_array);

        $('#resolvedInjuriesDiv').on('click', function(){
            $('#resolvedClaimsList').toggle();
            if($('#resolvedCountIcon').hasClass('icon-arrow-down')){
                $('#resolvedCountIcon').attr('class', 'icon icon-arrow-up');
            }else{
                $('#resolvedCountIcon').attr('class', 'icon-arrow-down');
            }
        });

        $('#policyHoldersDiv').on('click', function(){
            $('#policyHoldersList').toggle();
            if($('#policyHoldersDropdownIcon').hasClass('icon-arrow-down')){
                $('#policyHoldersDropdownIcon').attr('class', 'icon icon-arrow-up');
            }else{
                $('#policyHoldersDropdownIcon').attr('class', 'icon icon-arrow-down');
            }
        })

        $(".policyHolderListItem").on('click', function(){
            window.document.location = '/agentZenboard/' + $(this).data('company_id');
        });

        $('.resolvedClaimListItem').on('click', function(){
            var url = $(this).data('url');
            window.location.href = url;
        });

        $('#claimContainer').on('click', '.injuryDiv', function(){
            var url = $(this).data('url');
            window.location.href = url;
        });
        
        $('#updateDateButton').on('click', function(){

            var error_div = $('#setDateErrors');
            error_div.html("").hide();
            var new_date = $("#weeklySummaryDatePicker").val();

            if(new_date === ""){
                error_div.html("Please check to make sure you entered a valid date.").show();
            }else{
                window.location.href = "/weeklysummary/" + new_date;
            }
        });

        $('.zencareLink').on('click', function(){
            var injury_id = $(this).data('injuryid');
            if(injury_id !== null || injury_id !== "" || injury_id !== undefined){
                window.location.href = "/zencare/" + injury_id;
            }
        });

    </script>


@endsection

@extends('layouts.zen5_layout')
@section('maincontent')
    <?php


    $sorted_companies = Auth::user()->getCompanyListByAgency();
    $agency = Auth::user()->getAgency();
//    var_dump($sorted_companies);
    /*
    echo "<div class='agencyCompaniesDiv'>";
        echo "<h3>" . $agency . "</h3><br>";
            foreach($companies as $company){
                echo "<li class='companyListItem'><a href='/selectCompany/" . $company->id ."'>" . $company->company_name . "</a></li>";
            }
    echo "</div>";
}
    */


    ?>

<div class="pageContent bgimage-bgheader pagebackground5 fade-in">		
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                	<span>Policyholders</span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/company.png"></span><span class="subContent">manage <b>policyholders</b></span>
                    </div>
            </div>
            <div class="pageIcon animate__animated animate__bounceInRight">
                <img src="/images/icons/gears.png">
            </div> 
        </div>           
	</div>
    <div class="container noPad">

        <!--ACTIVE COMPANIES-->
        <div class="fixedPanel">
            <div class="barView">
                <div class="barViewLeft animate__animated animate__headShake">
                    <span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/company-active.png"> all active policyholders</span>
                </div>
                <div class="barViewRight animate__animated animate__fadeInRight animDelay6">
                    <!-- <ul> -->
                        <!-- total open injuries -->
                        <!-- <li><div class="setToolTip inline"><span class="toolTip">total open<br>injuries</span><span class="icon"><img src="images/icons/heart-simple.png"></span><span id="active__open" class="value FG__green">0</span></div></li> -->
                        <!-- total critical injuries -->
                        <!-- <li><div class="setToolTip inline"><span class="toolTip">total critical<br>injuries</span><span class="icon"><img src="images/icons/time-crit.png"></span><span id="active__critical" class="value FG__critical">0</span></div></li> -->
                        <!-- due renewals -->
                        <!-- <li><div class="setToolTip inline"><span class="toolTip">upcoming<br>renewals</span><span class="icon"><img src="images/icons/date-crit.png"></span><span id="renewal__critical" class="value FG__critical">0</span></div></li> -->
                    <!-- </ul> -->
                </div>
            </div>
            <div class="viewContent">
                <div class="zen-actionPanel transparent tight">
                    <div class="sectionContent">
                        <table class="allcompanyTable zenListTable tight">
                            <tbody id="allcompanyDropdown">
                                <tr class="allcompanyHeadings tableHeadings">
                                    <td></td>
                                    <td class="name"><button class="watchlistSorter sorter showIconModal">name</button></td>
                                    <td class="numvalue"><button class="watchlistSorter sorter showIconModal">open claims</button></td>
                                    <!-- <td class="numvalue"><button class="watchlistSorter sorter showIconModal">critical</button></td>
                                    <td class="policyNum"><button class="watchlistSorter sorter showIconModal">policy #</button></td>
                                    <td class="itemdate"><button class="watchlistSorter sorter showIconModal">renewal</button></td> -->
                                </tr>
                                <!-- replace content -->
                                <tr class="active">
                                    <td class='companyLogo'><div class='companyIcon' style="background-image:url('/images/default_logos/company1.png');"></div></td>
                                    <td class="name"><span>selected Company</span></td>
                                    <td class="numvalue"><span class="FG__green">23</span></td>
                                    <!-- <td class="numvalue"><span class="FG__red">4</span></td>
                                    <td class="policyNum"><span>34rt45h4</span></td>
                                    <td class="itemdate"><span>2/12/2023</span></td> -->
                                </tr>
                                <tr class="tablerowspace"><td></td></tr>
                                <tr>
                                    <td class='companyLogo'><div class='companyIcon' style="background-image:url('/images/default_logos/company6.png');"></div></td>
                                    <td class="name"><span>New Company Name</span></td>
                                    <td class="numvalue"><span class="FG__green">23</span></td>
                                    <!-- <td class="numvalue"><span class="FG__red">4</span></td>
                                    <td class="policyNum"><span>34rt45h4</span></td>
                                    <td class="itemdate"><span>2/12/2023</span></td> -->
                                </tr>
                                <tr class="tablerowspace"><td></td></tr>                                
                                <!-- end replace content -->                                
                            </tbody>
                        </table>
                    </div>					
                </div>
            </div>				
        </div> 

    </div> 

    <script>
        var companies = JSON.parse('<?php echo json_encode($sorted_companies, JSON_HEX_APOS); ?>');
        //console.log("Companies: " + companies);
        //THIS IS THE OVERALL FUNCTION THAT LOOPS THROUGH ALL THE COMPANIES AND BUILDS THE MAIN HTML BLOCK
        function buildTableArray(){
            var html = "";
            var i = 0;
            for(i; i < Object.keys(companies).length; i++){
                //console.log(companies[i]);
                html+= buildAgencyTable(companies[i]);
            }

            //console.log("html before allcompanyTable: " + html);
            $('.allcompanyTable').html(html);
            //console.log("after table container: " + html);
        }


        //THIS FUNCTION CREATES THE HTML FOR EACH AGENCY BLOCK, THIS IS WHAT YOU'LL BE CHANGING
        function buildAgencyTable(agency){
            console.log(agency[0].agency_name);

            //get the agency_name var
            if(agency[0].agency_name === ""){
                var agency_name = "Independent Companies";
            }else{
                var agency_name = agency[0].agency_name;
            }

            //generate the html for each section with the agency name; aside from agency_name this part is static but repeated
            var html = '<table class="allcompanyTable zenListTable tight">' +
                            '<tbody id="allcompanyDropdown">' +
                                '<tr class="allcompanyHeadings tableHeadings">' +
                                    '<td></td>' +
                                   '<td class="name"><button class="watchlistSorter sorter showIconModal">name</button></td>' +
                                    '<td class="numvalue"><button class="watchlistSorter sorter showIconModal">open claims</button></td>' +
                                    // '<td class="numvalue"><button class="watchlistSorter sorter showIconModal">critical</button></td>' +
                                    // '<td class="policyNum"><button class="watchlistSorter sorter showIconModal">policy #</button></td>' +
                                    // '<td class="itemdate"><button class="watchlistSorter sorter showIconModal">renewal</button></td>' +
                                '</tr>';
            
            
            //this generates the html for each company row and is appended to the html var
            var i = 0;
            for(i; i < Object.keys(agency).length; i++){
                if(agency[i].activated === 1){
                    var row_html = "<tr class='hoverLink companyListRow' data-id=" + agency[i].id + "><td class='companyLogo'><div class='companyIcon' style=\"background-image:url('" + agency[i].logo_location + "?{{ $date = str_replace(' ', '', \Carbon\Carbon::now()) }}" + "');\"></div></td><td class='companyName'>" + agency[i].company_name + "</td><td class='numvalue'>" + agency[i].open_claims + "</td><td class='numvalue'>" + "</td><td class='numvalue'>" + "</td><td class='numvalue'>" + "</td><td class='numvalue'>" + "</td></tr><tr class='tablerowspace'><td></td></tr>";
                    html+= row_html;
                }
            }
            //console.log("row_html: " + row_html);

            //this adds the closing table html
            html += "</tbody></table>";
            return html;

        }
        

        buildTableArray();
        //this redirects to zenboard with the selected company
        $('.allcompanyTable').on('click', '.companyListRow', function(){
           var id = $(this).data('id');
           console.log('id' + id);
           if (id != undefined){
              window.location.href = "/selectCompany/" + id;
            }
        });


    </script>
@endsection
	


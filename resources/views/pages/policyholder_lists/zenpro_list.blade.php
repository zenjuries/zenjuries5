@extends('layouts.zen5_layout')
@section('maincontent')
    <?php


    $sorted_companies = Auth::user()->getZenproCompanyListByAgency("active");

   // var_dump($sorted_companies);
    /*
    echo "<div class='agencyCompaniesDiv'>";
        echo "<h3>" . $agency . "</h3><br>";
            foreach($companies as $company){
                echo "<li class='companyListItem'><a href='/selectCompany/" . $company->id ."'>" . $company->company_name . "</a></li>";
            }
    echo "</div>";
}
    */


    ?>

<div class="pageContent bgimage-bgheader pagebackground5 fade-in">		
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                	<span>All clients</span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/company.png"></span><span class="subContent">manage your <b>clients</b></span>
                    </div>
            </div>
            <div class="pageIcon animate__animated animate__bounceInRight">
                <img src="/images/icons/zenpro2.png">
            </div> 
        </div>           
	</div>
    <div class="container noPad">

    
        <!--ACTIVE COMPANY 1
        <div class="flexPanel">
            <div class="viewControl">
                <div class="compactViewLeft animate__animated animate__headShake">
                    <span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/company-assigned.png"> agency1 name</span>
                </div>
                <div class="compactViewExpand">[ expand ]</div>
                <div class="compactViewRight animate__animated animate__fadeInRight animDelay8">
                    <ul>
                        <li><span class="icon"><img src="images/icons/heart-simple.png"></span><span id="active__open" class="value FG__green">0</span></li>
                        <li><span class="icon"><img src="images/icons/time-crit.png"></span><span id="active__critical" class="value FG__critical">0</span></li>
                    </ul>
                    <div class="compactViewX">[ minimize ]</div>
                </div>
            </div>
            <div class="viewContent">
                <div class="zen-actionPanel transparent tight">
                    <div class="sectionContent dynamicSize-nopad">
                        <table class="watchlistTable zenListTable tight" id="">
                            <tbody id="myWatchListDropdown">
                                <tr class="watchlistHeadings tableHeadings">
                                    <td></td>
                                    <td class="name"><button class="watchlistSorter sorter showIconModal">name</button></td>
                                    <td class="numvalue"><button class="watchlistSorter sorter showIconModal">open</button></td>
                                    <td class="numvalue"><button class="watchlistSorter sorter showIconModal">critical</button></td>
                                    <td class="policyNum"><button class="watchlistSorter sorter showIconModal">policy #</button></td>
                                    <td class="itemdate"><button class="watchlistSorter sorter showIconModal">renewal</button></td>
                                </tr>
                                <tr>
                                    <td class='companyLogo'><div class='companyIcon' style="background-image:url('/images/default_logos/company1.png');"></div></td>
                                    <td class="name"><span>New Company Name</span></td>
                                    <td class="numvalue"><span class="FG__green">23</span></td>
                                    <td class="numvalue"><span class="FG__red">4</span></td>
                                    <td class="policyNum"><span>34rt45h4</span></td>
                                    <td class="itemdate"><span>2/12/2023</span></td>
                                </tr>
                                <tr class="tablerowspace"><td></td></tr>
                                <tr>
                                    <td class='companyLogo'><div class='companyIcon' style="background-image:url('/images/default_logos/company6.png');"></div></td>
                                    <td class="name"><span>New Company Name</span></td>
                                    <td class="numvalue"><span class="FG__green">23</span></td>
                                    <td class="numvalue"><span class="FG__red">4</span></td>
                                    <td class="policyNum"><span>34rt45h4</span></td>
                                    <td class="itemdate"><span>2/12/2023</span></td>
                                </tr>
                                <tr class="tablerowspace"><td></td></tr>                                
                            </tbody>
                        </table>
                    </div>					
                </div>
            </div>				
        </div>

        <div class="flexPanel">
            <div class="viewControl">
                <div class="compactViewLeft animate__animated animate__headShake">
                    <span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/company-assigned.png"> agency2 name</span>
                </div>
                <div class="compactViewExpand">[ expand ]</div>
                <div class="compactViewRight animate__animated animate__fadeInRight animDelay8">
                    <ul>
                        <li><span class="icon"><img src="images/icons/heart-simple.png"></span><span id="active__open" class="value FG__green">0</span></li>
                        <li><span class="icon"><img src="images/icons/time-crit.png"></span><span id="active__critical" class="value FG__critical">0</span></li>
                    </ul>
                    <div class="compactViewX">[ minimize ]</div>
                </div>
            </div>
            <div class="viewContent">
                <div class="zen-actionPanel transparent tight">
                    <div class="sectionContent dynamicSize-nopad">
                        <table class="watchlistTable zenListTable tight" id="">
                            <tbody id="myWatchListDropdown">
                                <tr class="watchlistHeadings tableHeadings">
                                    <td></td>
                                    <td class="name"><button class="watchlistSorter sorter showIconModal">name</button></td>
                                    <td class="numvalue"><button class="watchlistSorter sorter showIconModal">open</button></td>
                                    <td class="numvalue"><button class="watchlistSorter sorter showIconModal">critical</button></td>
                                    <td class="policyNum"><button class="watchlistSorter sorter showIconModal">policy #</button></td>
                                    <td class="itemdate"><button class="watchlistSorter sorter showIconModal">renewal</button></td>
                                </tr>
                                <tr>
                                    <td class='companyLogo'><div class='companyIcon' style="background-image:url('/images/default_logos/company1.png');"></div></td>
                                    <td class="name"><span>New Company Name</span></td>
                                    <td class="numvalue"><span class="FG__green">23</span></td>
                                    <td class="numvalue"><span class="FG__red">4</span></td>
                                    <td class="policyNum"><span>34rt45h4</span></td>
                                    <td class="itemdate"><span>2/12/2023</span></td>
                                </tr>
                                <tr class="tablerowspace"><td></td></tr>
                                <tr>
                                    <td class='companyLogo'><div class='companyIcon' style="background-image:url('/images/default_logos/company6.png');"></div></td>
                                    <td class="name"><span>New Company Name</span></td>
                                    <td class="numvalue"><span class="FG__green">23</span></td>
                                    <td class="numvalue"><span class="FG__red">4</span></td>
                                    <td class="policyNum"><span>34rt45h4</span></td>
                                    <td class="itemdate"><span>2/12/2023</span></td>
                                </tr>
                                <tr class="tablerowspace"><td></td></tr>                                
                            </tbody>
                        </table>
                    </div>					
                </div>
            </div>				
        </div>        

        <div class="flexPanel active">
            <div class="viewControl">
                <div class="compactViewLeft animate__animated animate__headShake">
                    <span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/company-assigned.png"> agency3 name</span>
                </div>
                <div class="compactViewExpand">[ expand ]</div>
                <div class="compactViewRight animate__animated animate__fadeInRight animDelay8">
                    <ul>
                        <li><span class="icon"><img src="images/icons/heart-simple.png"></span><span id="active__open" class="value FG__green">0</span></li>
                        <li><span class="icon"><img src="images/icons/time-crit.png"></span><span id="active__critical" class="value FG__critical">0</span></li>
                    </ul>
                    <div class="compactViewX">[ minimize ]</div>
                </div>
            </div>
            <div class="viewContent">
                <div class="zen-actionPanel transparent tight">
                    <div class="sectionContent dynamicSize-nopad">
                        <table class="watchlistTable zenListTable tight" id="">
                            <tbody id="myWatchListDropdown">
                                <tr class="watchlistHeadings tableHeadings">
                                    <td></td>
                                    <td class="name"><button class="watchlistSorter sorter showIconModal">name</button></td>
                                    <td class="numvalue"><button class="watchlistSorter sorter showIconModal">open</button></td>
                                    <td class="numvalue"><button class="watchlistSorter sorter showIconModal">critical</button></td>
                                    <td class="policyNum"><button class="watchlistSorter sorter showIconModal">policy #</button></td>
                                    <td class="itemdate"><button class="watchlistSorter sorter showIconModal">renewal</button></td>
                                </tr>
                                <tr class="active">
                                    <td class='companyLogo'><div class='companyIcon' style="background-image:url('/images/default_logos/company1.png');"></div></td>
                                    <td class="name"><span>New Company Name</span></td>
                                    <td class="numvalue"><span class="FG__green">23</span></td>
                                    <td class="numvalue"><span class="FG__red">4</span></td>
                                    <td class="policyNum"><span>34rt45h4</span></td>
                                    <td class="itemdate"><span>2/12/2023</span></td>
                                </tr>
                                <tr class="tablerowspace"><td></td></tr>
                                <tr>
                                    <td class='companyLogo'><div class='companyIcon' style="background-image:url('/images/default_logos/company6.png');"></div></td>
                                    <td class="name"><span>New Company Name</span></td>
                                    <td class="numvalue"><span class="FG__green">23</span></td>
                                    <td class="numvalue"><span class="FG__red">4</span></td>
                                    <td class="policyNum"><span>34rt45h4</span></td>
                                    <td class="itemdate"><span>2/12/2023</span></td>
                                </tr>
                                <tr class="tablerowspace"><td></td></tr>                                
                            </tbody>
                        </table>
                    </div>					
                </div>
            </div>				
        </div>   
    --> 

       <div id="tableContainer" class="container noPad">
        <!-- start of the company block -->
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-company" id="companyName"></div> [company name here]</span>
                <div class="zen-actionPanel transparent tight">
                    <div class="sectionContent">
                        <table class="zenListTable tight" id="[companynameID]">
                            <tbody>
                            <tr class="tableHeadings">
                                <td></td>
                                <td><button class="sorter showIconModal">name</button></td>
                                <td><button class="sorter showIconModal">open claims</button></td>
                                <td></td>
                            </tr>
        
                            <!-- start of repeatable table area -->    
                            <tr class="hoverLink">
                                <td class="companyLogo"><div class="companyIcon"></div></td>
                                <td class="companyName"></td>
                                <td class="openClaims"></td>
                                <!--this next button links to the company edit page, for this listed company-->
                                <td><button class="smallIcon"></button></td>
                            </tr>
                            <tr class="tablerowspace"><td></td></tr>
                            <!-- end of repeatable table area -->
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        <!-- end of the company block -->
      </div>
    </div> 
   
    <div id="tableContainer"></div>

    <script>
        var companies = JSON.parse('<?php echo json_encode($sorted_companies, JSON_HEX_APOS); ?>');
        console.log("Companies: " + companies);

        //THIS IS THE OVERALL FUNCTION THAT LOOPS THROUGH ALL THE COMPANIES AND BUILDS THE MAIN HTML BLOCK
        function buildTableArray(){
            var html = "";
            var i = 0;
            for(i; i < Object.keys(companies).length; i++){
                console.log(companies[i]);
                html+= buildAgencyTable(companies[i]);
            }

            console.log("html before tablecontainer: " + html);
            $('#tableContainer').html(html);
            console.log("after table container: " + html);
        }
        var agencyArray;


        //THIS FUNCTION CREATES THE HTML FOR EACH AGENCY BLOCK, THIS IS WHAT YOU'LL BE CHANGING
        function buildAgencyTable(agency){
            console.log(agency[0].agency_name);
            agencyArray = agency;

            //get the agency_name var
            if(agency[0].agency_name === ""){
                var agency_name = "Independent Companies";
            }else{
                var agency_name = agency[0].agency_name;
            }

            //generate the html for each section with the agency name; aside from agency_name this part is static but repeated
            var html = '<section class="sectionPanel">' +
            '               <span class="sectionTitle"><div class="icon icon-company">' + agency_name + '</div></span>' +
            '               <div class="zen-actionPanel transparent tight">' +
            '                   <div class="sectionContent">' +
            '                       <table class="zenListTable tight" id="[companynameID]">' +        
            '                           <tbody>' +
            '                           <tr class="tableHeadings">' +
            '                               <td></td>' +
            '                                <td><button class="sorter showIconModal">name</button></td>' +
            '                                <td><button class="sorter showIconModal">open claims</button></td>' +
            '                                <td></td>' +
            '                           </tr>';
            
            //this generates the html for each company row and is appended to the html var
            var i = 0;
            for(i; i < Object.keys(agency).length; i++){
                console.log("angency " + Object.keys(agency));
                console.log("angency id" + agency);

                var randomCompanyImage = Math.floor(Math.random() * 9) + 1;
                var row_html = "<tr class='hoverLink companyListRow' data-id=" + agency[i].id + "><td class='companyLogo'><div class='companyIcon' style=\"background-image:url('" + agency[i].logo_location + "?{{ $date = str_replace(' ', '', \Carbon\Carbon::now()) }}" + "');\"></div></td><td class='companyName'>" + agency[i].company_name + "</td><td class='openClaims'>" + agency[i].open_claims + "</td><td><button class='smallIcon'></button></td></tr><tr class='tablerowspace'><td></td></tr>";
                html+= row_html;
            }
            console.log("row_html: " + row_html);

            //this adds the closing table html
            html += "</tbody></table></div></div></section>";
            return html;

        }
        

        buildTableArray();
        //this redirects to zenboard with the selected company
        $('#tableContainer').on('click', '.companyListRow', function(){
           var id = $(this).data('id');
           console.log('id' + id);
           if (id != undefined){
              window.location.href = "/selectCompany/" + id;
            }
        });


        $('.sorter').on('click', function(){

            var type = (this).innerText;
            var sort;
            if($(this).hasClass('down')){
                sort = "up";
                $('.sorter').removeClass('down').removeClass('up');
                $(this).removeClass('down').addClass('up');
            }else {
                sort = "down";
                $('.sorter').removeClass('down').removeClass('up');
                $(this).removeClass('up').addClass('down');
            }
            if(type == "name"){
                if(sort == "up"){
                    sortByNameAsc(agencyArray);
                }
                else if(sort === "down"){
                    sortByNameDesc(agencyArray);
                }
            }
            /*
            else if(type == "open claims"){
                console.log("open claims clicked");
                if(sort == "up"){
                    sortByOpenClaimsAsc(watchlist_injuries);
                }
                else if(sort === "down"){
                    sortByOpenClaimsDesc(watchlist_injuries);
                }
            }
            */
        });

        function sortByNameAsc(agency_array){
            function compare(a, b) {
                if(a.agency_name.toLowerCase() < b.agency_name.toLowerCase()){
                    return 1;
                }
                if(a.agency_name.toLowerCase() > b.agency_name.toLowerCase()){
                    return -1;
                }
                return 0;
            }
            (agency_array).sort(compare);
            buildAgencyTable(agency_array);
        }

        function sortByNameDesc(agency_array){
            function compare(a, b) {
                if(a.agency_name.toLowerCase() < b.agency_name.toLowerCase()){
                    return -1;
                }
                if(a.agency_name.toLowerCase() > b.agency_name.toLowerCase()){
                    return 1;
                }
                return 0;
            }
            (agency_array).sort(compare);
            buildAgencyTable(agency_array);
        }

        /*
        function sortByOpenClaimsAsc(injury_array){
            function compare(a, b){
                if(a.current_cost + a.reserve_cost < b.current_cost + b.reserve_cost){
                    return 1;
                }
                if(a.current_cost + a.reserve_cost > b.current_cost + b.reserve_cost){
                    return -1;
                }
                return 0;
            }
            watchlist_injuries.sort(compare);
            buildMyWatchListable(true);
        }

        function sortByOpenClaimsDesc(injury_array){
            function compare(a, b){
                if(a.current_cost < b.current_cost){
                    return -1;
                }
                if(a.current_cost > b.current_cost){
                    return 1;
                }
                return 0;
            }
            watchlist_injuries.sort(compare);
            buildMyWatchListable(true);
        }
        */

    </script>
@endsection
	


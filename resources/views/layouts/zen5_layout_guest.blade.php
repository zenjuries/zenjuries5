<?php
    $brandCompany = env('COMPANY_NAME', "Zenjuries");
    $brandID = env('BRAND_NAME', "default");
    $brandApp = env('BRAND_APP_NAME', "Zenjuries");
?>

<!DOCTYPE html>
<html>
<head> 
    <meta charset="UTF-8">
    <title><?php echo $brandApp ?></title>

    <!--ZenjuriesFont-->
    <link rel="stylesheet" href="{{URL::asset('/css/zenfontstyles.css?v='.filemtime(public_path('css/zenfontstyles.css'))) }}">
    <!--Animation-->
    <link rel="stylesheet" href="{{URL::asset('/css/animate.css') }}">
    <!--jBox-->
    <link rel="stylesheet" href="{{URL::asset('/css/jBox.all.min.css') }}">
    <!--branding-->
    <link rel="stylesheet" href="{{URL::asset("/branding/$brandID/css/branding.css") }}">
    <link rel="stylesheet" href="{{URL::asset("/branding/$brandID/css/themes.css") }}">
    <!--site styling-->
    <link rel="stylesheet" href="{{URL::asset('/css/variables.css') }}">
    <link rel="stylesheet" href="{{URL::asset('/css/zen5_style_default.css') }}">

    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>
    <script src="{{URL::asset('/js/jBox.all.min.js') }}"></script>
    <!-- jquerty input masks (date/time/phone/etc) -->
    <script src="/js/jquery.mask.min.js"></script>
    <!-- TODO:: ZENMODAL.JS? -->
    <script src="/js/wow.min.js"></script>
    <!-- STRIPE -->
    <script src="https://js.stripe.com/v3/"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <!-- Icons -->
    <link rel="apple-touch-icon" sizes="180x180" href="/branding/{{ $brandID }}/icon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/branding/{{ $brandID }}/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/branding/{{ $brandID }}/icon/favicon-16x16.png">
    <link rel="manifest" href="/branding/{{ $brandID }}/icon/site.webmanifest">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
</head>

<body>


@include('partials.zen5_topNavPublic')
<div class="mainContent">

    @yield('maincontent')

</div>

<script>
wow = new WOW(
        {
        boxClass:     'wow',      // default
        animateClass: 'animate__animated', // default
        offset:       0,          // default
        mobile:       true,       // default
        live:         true        // default
    }
    )
    wow.init();
</script>

<script>
$(document).ready(function(){
  $('.date').mask('00/00/0000');
  $('.time').mask('00:00:00');
  $('.date_time').mask('00/00/0000 00:00:00');
  $('.cep').mask('00000-000');
  $('.phone').mask('0000-0000');
  $('.phone_with_ddd').mask('(00) 0000-0000');
  $('.phone_us').mask('(000) 000-0000');
  $('.mixed').mask('AAA 000-S0S');
  $('.cpf').mask('000.000.000-00', {reverse: true});
  $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
  $('.money').mask('000.000.000.000.000,00', {reverse: true});
  $('.money2').mask("#.##0,00", {reverse: true});
  $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
    translation: {
      'Z': {
        pattern: /[0-9]/, optional: true
      }
    }
  });
  $('.ip_address').mask('099.099.099.099');
  $('.percent').mask('##0,00%', {reverse: true});
  $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
  $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
  $('.fallback').mask("00r00r0000", {
      translation: {
        'r': {
          pattern: /[\/]/,
          fallback: '/'
        },
        placeholder: "__/__/____"
      }
    });
  $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
});
</script>

</body>

</html>
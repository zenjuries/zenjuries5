<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Your zenGarden</title>


    <!-- Zenployers Style -->
    <link rel="stylesheet" href="https://www.zenployees.com/css/zp_style.css?<$date format='yyyy-MM-dd_HH:mm:ss'/$>">
    <!-- zenGarden Style -->
    <link rel="stylesheet" href="{{URL::asset('/css/zg_style.css') }}">
    <!--ZenjuriesFont-->
    <link rel="stylesheet" href="{{URL::asset('/css/zenfontstyles.css') }}">
    <!--Animation-->
    <link rel="stylesheet" href="https://www.zenployees.com/css/animate.css?<$date format='yyyy-MM-dd_HH:mm:ss'/$>">
    <!--jBox-->
    <link rel="stylesheet" href="https://www.zenployees.com/css/jBox.all.min.css?<$date format='yyyy-MM-dd_HH:mm:ss'/$>">

    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>
    <script src="https://www.zenployees.com/js/jBox.all.min.js"></script>

    <!-- Icons -->
    <link rel="apple-touch-icon" sizes="180x180" href="/icon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/icon/favicon-16x16.png">
    <link rel="manifest" href="/icon/site.webmanifest">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta content="width=device-width, initial-scale=1, user-scalable=0" name="viewport" />
</head>

<body>


@include('partials.zen5_zengarden_zp_blankNav')
<div class="mainContent">
    <div class="zAlertArea"></div>
    <input type="hidden" id="userID" value="{{Auth::user()->id}}">
    <input type="hidden" id="csrfToken" value="{{csrf_token()}}">
    @yield('maincontent')

</div>

<script src="{{URL::asset('/js/zp/colorpicker.js') }}"></script>
<script src="/js/zen-js/zengarden_modal.js"></script>
<script src="/js/zen-js/zengarden.js"></script>

<script>
    function showAlert(text, alert_class, display_time_in_seconds){
        var alert_div = document.createElement('div');
        var alert_html = '<div class="zAlertIcon ' + alert_class + '"><div class="icon"></div></div>' +
            '<div class="message">' + text + '</div>' +
            '<div class="dismiss"><div class="icon icon-times"></div></div>';
        $(alert_div)
            .addClass("zAlertDialogue open")
            .html(alert_html)
            .click(function(){
                $(this).remove();
            });

        //alert('showing alert');

        // to animate alert, add class of -open, -close, -exit to zAlertDialogue.  all animations are 1s

        $('.zAlertArea').append(alert_div);

        setTimeout(function(){
            $(alert_div).addClass("zAlertDialogue close");
            setTimeout(function(){
                $(alert_div).height(0).remove();
            }, 900);
            //$(alert_div).remove();
        }, (display_time_in_seconds * 900));

    }
</script>

</body>

</html>
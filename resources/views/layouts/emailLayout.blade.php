
<?php
    $brandApp = env('BRAND_APP_NAME');
	$brandCompany = env('COMPANY_NAME');
	$brandName = env('BRAND_NAME');
?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <title>{{ $brandApp }} Status Email</title>
</head>

<body bgcolor="#eee">
	<center>
	<table width="90%" align="center" cellspacing="0" cellpadding="0">
	    <tr><td>
	        <table cellpadding="0" cellspacing="0" style="max-width:550px;background-color:white;margin:0 auto;border:1px solid #dddddd;border-radius:8px;">
	            <tr>

	            	<td align="center" id="headerBG" bgcolor="#686868" style="text-align:center;border-top-left-radius:8px;border-top-right-radius:8px;background: rgb(104,104,104);background: -moz-linear-gradient(top, rgba(104,104,104,1) 0%, rgba(22,22,22,1) 100%);background: -webkit-linear-gradient(top, rgba(104,104,104,1) 0%,rgba(22,22,22,1) 100%);background: linear-gradient(to bottom, rgba(104,104,104,1) 0%,rgba(22,22,22,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#686868', endColorstr='#161616',GradientType=0 );">

			        <div style="text-align:center;padding:10px 0 0 0;"> 

					<img id="headerImage" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/header-logo.png" width="100%" style="width:100%;max-width:360px;">
					<span style="display:block;width:100%;font-size:2.2em;text-align:center;color:white;"><?php echo $headerTitle ?></span>

			        </div>
						
			        </td>
			    </tr>
	            <tr><td bgcolor="black" height="12px" padding="0" margin="0">&nbsp;</td></tr>
	            <tr><td>
		            <div style="width:90%;margin:5%;background-color:#ededed;border-radius:8px;">
		                    <div style="padding:20px;">
		            <!-- BODYFLAIR SWAPPING -->
		            @if($bodyFlair === "4adjuster")
		            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/assigned-adjuster.png" width="100%" style="width:100%;"><hr>
		            @elseif($bodyFlair === "4admin")
		            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/assigned-admin.png" width="100%" style="width:100%;"><hr>
		            @elseif($bodyFlair === "4navigator")
		            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/assigned-navigator.png" width="100%" style="width:100%;"><hr>	
		            @elseif($bodyFlair === "4agent")
		            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/assigned-agent.png" width="100%" style="width:100%;"><hr>		            	
		            @elseif($bodyFlair === "4claimmanager")
		            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/assigned-claimmanager.png" width="100%" style="width:100%;"><hr>
		            @elseif($bodyFlair === "4customerservice")
		            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/assigned-customerservice.png" width="100%" style="width:100%;"><hr>
		            @elseif($bodyFlair === "4doctor")
		            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/assigned-doctor.png" width="100%" style="width:100%;"><hr>
		            @elseif($bodyFlair === "4hr")
		            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/assigned-hr.png" width="100%" style="width:100%;"><hr>
		            @elseif($bodyFlair === "4other")
		            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/assigned-other.png" width="100%" style="width:100%;"><hr>
		            @elseif($bodyFlair === "4pt")
		            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/assigned-pt.png" width="100%" style="width:100%;"><hr>
		            @elseif($bodyFlair === "4safety")
		            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/assigned-safety.png" width="100%" style="width:100%;"><hr>
		            @elseif($bodyFlair === "4supervisor")
		            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/assigned-supervisor.png" width="100%" style="width:100%;"><hr>		            			            			            	
		            @endif		            

			                    <div style="padding-top:20px;padding-bottom:10px;font-family: 'Lato', sans-serif;font-size:14px;color:black;">
									<?php echo $bodyText; ?>
									<br><br><hr>
									<div style="text-align:center;font-size:16px;"><?php echo $linkText; ?></div>
			                    </div>
		                    </div>
	                    </div>
	            	</td>
	            </tr>
	            <tr>
		            <td>
			            <table width="100%" cellspacing="0" cellpadding="0">
				            <tr>
								<td width="50%"><img id="footerLogo" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/footer-logo.png" width="100%" style="width:100%;"></td>
								<td width="50%" align="right" style="padding-right:10px;padding-bottom:10px;">		        
						            <!-- FOOTERFLAIR SWAPPING -->
						            @if($footerFlair === "medic")
						            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/footer-medic.png" width="100%" style="width:100%;max-width:209px;">
						            @elseif($footerFlair === "firstreport")
						            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/footer-firstreport.png" width="100%" style="width:100%;max-width:209px;">
						            @elseif($footerFlair === "firstreportreminder")
						            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/footer-firstreportreminder.png" width="100%" style="width:100%;max-width:209px;">							@elseif($footerFlair === "reminder")
						            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/footer-reminder.png" width="100%" style="width:100%;max-width:209px;">
						            @elseif($footerFlair === "alert")
						            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/footer-alert.png" width="100%" style="width:100%;max-width:209px;">
						            @elseif($footerFlair === "alert2")
						            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/footer-alert2.png" width="100%" style="width:100%;max-width:209px;">
						            @elseif($footerFlair === "teams")
						            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/footer-teams.png" width="100%" style="width:100%;max-width:209px;">
						            @elseif($footerFlair === "zengarden")
						            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/footer-zengarden.png" width="100%" style="width:100%;max-width:209px;">
						            @elseif($footerFlair === "books")
						            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/cl{{ $brandName }}ient/email/images/footer-books.png" width="100%" style="width:100%;max-width:209px;">
						            @elseif($footerFlair === "super")
						            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/footer-super.png" width="100%" style="width:100%;max-width:209px;">
						            @elseif($footerFlair === "info")
						            	<img id="footerFlair" src="https://dev5.zenjuries.com/branding/{{ $brandName }}/email/images/footer-info.png" width="100%" style="width:100%;max-width:209px;">		
						            @endif
								</td>
				            </tr>
			            </table>    	    
		            </td>
		        </tr>



	        </table>
		</td></tr>
	</table>
	</center>

</body>

	
		

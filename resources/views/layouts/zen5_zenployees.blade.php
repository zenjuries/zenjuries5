<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/> <!--320-->
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Zenjuries</title>
    @include('partials.head-new')
    @yield('header')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>


<body>

<style>
    .zButton, .zButton a, .zButton div, .zButton button{cursor: hand !important;}
</style>

<div class="mainContent">
    <noscript>
        <br><br><br><br><br>
        <div style="text-align: center; background-color: lightcoral; font-size: 16pt; color: black; width: 100%">
            This site will not work properly without JavaScript. Please enable JavaScript to use Zenjuries.
        </div>
    </noscript>
    <div class="zAlertArea"></div>
    <!--
    <div id="noJavascript" style="text-align: center; background-color: lightcoral; font-size: 20pt; color: black">
        <br><br><br>
        This site will not work properly without Javascript. Please enable Javascript to use Zenjuries.
    </div>
    <script>
        document.getElementById('noJavascript').style.display = "none";
    </script>
    -->
    @yield('maincontent')
</div>

<script>
    function setActiveNav(){
        //populate with zengarden nav
    }

    function showAlert(text, alert_class, display_time_in_seconds){
        var alert_div = document.createElement('div');
        var alert_html = '<div class="zAlertIcon ' + alert_class + '"><div class="icon"></div></div>' +
            '<div class="message">' + text + '</div>' +
            '<div class="zButton tiny round dismiss "><button class="buttonColor red"><div class="icon icon-times"></div></button></div>';
        $(alert_div)
            .addClass("zAlertDialogue animated bounceInRight")
            .html(alert_html)
            .click(function(){
                $(this).remove();
            });

        //alert('showing alert');

        $('.zAlertArea').append(alert_div);

        setTimeout(function(){
            $(alert_div).addClass("animated slideRemove");
            setTimeout(function(){
                $(alert_div).height(0).remove();
            }, 2000);
            //$(alert_div).remove();
        }, (display_time_in_seconds * 1000));

    }

    $(document).ready(function(){
        setActiveNav();

        $(document).on('click', '.popupMessageTrigger', function(){
            console.log('clicked');
            var message = $(this).find('.popupMessage');
            if(message.css('display') == 'none'){
                console.log('message already hidden, showing');
                $('.popupMessage').hide();
                message.show();
            }else{
                console.log('message already showing, hide');
                $('.popupMessage').hide();
            }
            var new_class = $(this).data('applyclass');
            $(this).addClass(new_class);
        });


        $(document).on('focusout', '.popupMessageTrigger', function(){
            console.log('focusout');
            $('.popupMessage').hide();
        });

    });
</script>

@include('partials.zenfooter')

@yield('footer-new')

</body>
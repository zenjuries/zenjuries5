# emailPartials
-  Alert Text Email:
    file name: alertTextEmailPartial.blade.php
    description: used for verifying accounts, negative diary entries, and safety coordinator early injury emails
    date edited: 1/2/2019
    version: 2.2
-  Common Text Email:
    file name: commonTextEmailPartial.blade.php
    description: used for most emails
    date edited: 1/2/2019
    version: 2.2
-  Header Line 1:
    file name: headline1EmailPartial.blade.php
    description: Header line for emails
    date edited: 1/2/2019
    version: 2.2
-  Header Line 2:
    file name: headerline2.blade.php
    description: Header line for emails
    date edited: 1/2/2019
    version: 2.2
-  Welcome Box:
    file name: welcomeBoxEmailPartial.blade.php
    description: body text container for some emails
    date edited: 1/2/2019
    version: 2.2
-  Zenfucius Comment Email:
    file name: zenfuciusCommentEmailPartial.blade.php
    description: Body text container for some emails
    date edited: 1/2/2019
    version: 2.2
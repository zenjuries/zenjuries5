<?php
function checkForAndroidApp(){
    $usingApp = false;
    if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
        if($_SERVER['HTTP_X_REQUESTED_WITH'] === "com.zenjuries.zendev"){
            $usingApp = true;
        }
    }
    return $usingApp;
}

function checkForIPhoneApp(){
    $usingApp = false;
    if(!isset($_SERVER['HTTP_USER_AGENT']) || ((strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile/') !== false) && (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari/') == false))){
        $usingApp = true;
    }
    return $usingApp;
}

function setMobileSession($userType){
    if($userType == "android" || $userType == "iPhone"){
        session(['onMobile' => true]);
    }
}

function checkApps(){
    //userType will tell whether the user is on our iPhone app, Android app, or accessed the site through a browser
    if(checkForAndroidApp()){
        $userType = "android";
    }else if(checkForIPhoneApp()){
        $userType = "iPhone";
    }else{
        $userType = "browser";
    }
    setMobileSession($userType);
    return $userType;
}


?>

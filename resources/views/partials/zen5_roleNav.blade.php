<style>
.rolePanelContainer{width:100%;height:max-content;position:absolute;bottom:0px;left:0px;}
.rolePanelTab{font-size:.8rem;color:white;background-color:black;width:max-content;padding:6px 12px;border-top-right-radius:6px;border-top-left-radius:6px;box-shadow: 0px 0px 18px 0px rgba(0,0,0,0.75);-webkit-box-shadow: 0px 0px 12px 0px rgba(0,0,0,0.75);-moz-box-shadow: 0px 0px 12px 0px rgba(0,0,0,0.75);}
.rolePanelTab .icon{font-size:.8rem;color:yellow;position:relative;top:-1px;}
.rolePanelContent{background-color:black;width:100%;height:0px;box-shadow: 0px 0px 12px 0px rgba(0,0,0,0.75);-webkit-box-shadow: 0px 0px 12px 0px rgba(0,0,0,0.75);-moz-box-shadow: 0px 0px 12px 0px rgba(0,0,0,0.75);}
.rolePanelContent.show{height:max-content;min-height:50px;}
</style>
<!--
<div class="rolePanelContainer">
    <div class="rolePanelTab"><div class="icon icon-cog inline"></div> role: [role]</div>
    <div class="rolePanelContent"> -->

    <!-- display if role=dev
    dev panel
    @yield('devcontent')-->

    <!-- display if role=qa
    test panel
    @yield('testcontent')-->

   <!-- </div>
</div> -->
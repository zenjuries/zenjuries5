<div id="navbar">
    <div id="logo"><a href="/zengarden/home" style="background-image:url('https://www.zenployees.com/images/zengarden_dark_med.png');" class="invisible"></a></div>
    <div id="navbar-menu">
      <div id="menuLinks">
          <ul>
          <li id="diary"><a class="main" href="/zengarden/home"><span>Home</span></a></li>
          <li id="diary"><a class="main" href="/zengarden/diary"><span>Diary</span></a></li>
          <li id="schedule"><a class="main" href="/zengarden/schedule"><span>Schedule</span></a></li>
          <li id="team"><a class="main" href="/zengarden/myinfo"><span>My Info<span></a></li>
          <li id="team"><a class="main" href="/zengarden/team"><span>My Injury<span></a></li>		
          <li id="info"><a class="main" href="/zengarden/faq"><span>FAQ</span></a></li>
          @if(count(\App\Injury::where('injured_employee_id', Auth::user()->id)->get()) > 1)
          <li id="injuryList"><a class="main" href="/zengarden/zengardenInjuryList"><span>your injuries</span></a></li>
          @endif
          @if(Auth::user()->checkInjuredUserForInjuryTeams())
          <li id="zenboard"><a class="main zenboard" href="/zenboard"><span class="menuText">dashboard</span></a></li>
          @endif
          <li id="logout"><a class="login" href="/zengarden/logout"><span>LogOut</span></a></li>
          </ul>
      </div>
    </div>
  </div>
  
  
  <audio id="audioClick" src="/audio/click.mp3" autostart="false" preload="auto"></audio>
  <audio id="audioChime" src="/audio/chime.mp3" autostart="false" preload="auto"></audio>
  <audio id="audioSlide" src="/audio/qslide.mp3" autostart="false" preload="auto"></audio>
  <audio id="audioSlideClick" src="/audio/slideclick.mp3" autostart="false" preload="auto"></audio>
  <audio id="happyClick" src="/audio/happyclick2.mp3" autostart="false" preload="auto"></audio>
  <audio id="chirpClick" src="/audio/chirpclick.mp3" autostart="false" preload="auto"></audio>
  
  <div id="navbarMobile">
      <div class="hamburger hamburger--vortex no_highlights" onclick="PlaySlide()">
          <div class="hamburger-box">
              <div class="hamburger-inner"></div>
          </div>
      </div>
      <div id="mobileLogo" onclick="PlayChirpClick();"><a class="invisible no_highlights" href="/zengarden/home"><img class="animate__animated animate__bounceInRight" src="https://www.zenployees.com/images/zengarden_dark_med.png" height="100%"></a></div>
      <div id="slideMenu">
           <ul>
          <li onclick="PlayChirpClick();location.href='/zengarden/home';" class="seeTouch" id="mhome"><a class="main no_highlights" href="/zengarden/home" ><img class="mobileNavIcon" src="/images/clipart/zengarden-o.png"><span class="home">home</span></a></li>
          <li onclick="PlayChirpClick();location.href='/zengarden/diary';" class="seeTouch" id="mdiary"><a class="main no_highlights" href="/zengarden/diary"><img class="mobileNavIcon" src="/images/clipart/diary-o.png"><span class="diary">zen diary</span></a></li>
          <li onclick="PlayChirpClick();location.href='/zengarden/schedule';" class="seeTouch" id="mschedule"><a class="main no_highlights" href="/zengarden/schedule"><img class="mobileNavIcon" src="/images/clipart/schedule-o.png"><span class="scheduling">scheduling</span></a></li>
          <li onclick="PlayChirpClick();location.href='/zengarden/myinfo';" class="seeTouch" id="minfo"><a class="main no_highlights" href="/zengarden/myinfo"><img class="mobileNavIcon" src="/images/clipart/myinfo-o.png"><span class="info">info & profile</span></a></li>
          <li onclick="PlayChirpClick();location.href='/zengarden/team';" class="seeTouch" id="minfo"><a class="main no_highlights" href="/zengarden/team"><img class="mobileNavIcon" src="/images/clipart/medical-o.png"><span class="team">injury & team</span></a></li>
          <li onclick="PlayChirpClick();location.href='/zengarden/faq';" class="seeTouch" id="mfaq"><a class="main no_highlights" href="/zengarden/faq"><img class="mobileNavIcon" src="/images/clipart/help-o.png"><span class="faq">FAQ</span></a></li>
          @if(count(\App\Injury::where('injured_employee_id', Auth::user()->id)->get()) > 1)
          <li onclick="PlayChirpClick();location.href='/zengarden/zengardenInjuryList';" class="seeTouch" id="mfaq"><a class="main no_highlights" href="/zengarden/zengardenInjuryList"><img class="mobileNavIcon" src="/images/clipart/paperstack.png"><span class="faq">your injuries</span></a></li>
          @endif
          @if(Auth::user()->checkInjuredUserForInjuryTeams())
          <li onclick="location.href='/zenboard';" class="seeTouch" id="mzenboard"><a class="main no_highlights" href="/zenboard" ><img class="mobileNavIcon" src="/images/icons/zenjuries.png"><span class="zenboard">dashboard</span></a></li>
          @endif
          </ul>
      </div>
  </div>
  
  <script>
  const AudioContext = window.AudioContext || window.webkitAudioContext;
  const audioCtx = new AudioContext();
  function PlayClick() {
        var sound = document.getElementById("audioClick");
        sound.play()
    }
  function PlaySlide() {
        var sound = document.getElementById("audioSlide");
        sound.play()
    } 
  function PlayChime() {
        var sound = document.getElementById("audioChime");
        sound.play()
    }    
  function PlaySlideClick() {
        var sound = document.getElementById("audioSlideClick");
        sound.play()
    } 
  function PlayHappyClick() {
        var sound = document.getElementById("happyClick");
        sound.play()
    }    
  function PlayChirpClick() {
        var sound = document.getElementById("chirpClick");
        sound.play()
    }  
  </script>
  
  <script>
  // When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size
  window.onscroll = function() {scrollFunction()};
  
  function scrollFunction() {
    if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
      document.getElementById("navbar").style.height = "70px";
      document.getElementById("menuLinks").style.padding = "20px 5px";
      document.getElementById("logo").style.padding ="8px 0 8px 10px";
      document.getElementById("logo").style.width ="200px";
    } else {
      document.getElementById("navbar").style.height = "135px";
      document.getElementById("menuLinks").style.padding = "50px 20px";
      document.getElementById("logo").style.padding ="18px 0 18px 20px";
      document.getElementById("logo").style.width ="300px";
    }
  }
  </script>
  
  <script>
      var forEach=function(t,o,r){if("[object Object]"===Object.prototype.toString.call(t))for(var c in t)Object.prototype.hasOwnProperty.call(t,c)&&o.call(r,t[c],c,t);else for(var e=0,l=t.length;l>e;e++)o.call(r,t[e],e,t)};
  
      var hamburgers = document.querySelectorAll(".hamburger");
      if (hamburgers.length > 0) {
        forEach(hamburgers, function(hamburger) {
          hamburger.addEventListener("click", function() {
            this.classList.toggle("is-active");
          }, false);
        });
      }
      $('.hamburger').click(function(){
      $('#slideMenu').toggleClass('slide-out');
      });
  </script>
  
  <script>
  $(".seeTouch").click(function(){
    // $(".seeTouch").removeClass("touched");
    $(this).addClass("touched");
  })
  </script>
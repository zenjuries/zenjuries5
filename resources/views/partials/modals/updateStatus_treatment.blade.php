<div class="modalContent dark modalBlock" id="updateStatusModal_treatment" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/stethescope.png">update treatment</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                    <div id="update_status_treatmentDiv">
                        <div class="buttonCloud">
                            <button name="blah" id="hospitalButton" class="hospitalButton treatmentButton" data-status="Hospital"><div class="icon inline icon-ambulance"></div> Evaluation at Hospital</button>
                            <button name="blah" id="urgentcareButton" class="urgentcareButton treatmentButton" data-status="Urgent Care"><div class="icon inline icon-clinic"></div> Sent to Urgent Care</button>
                            <button name="blah" id="doctorButton" class="doctorButton treatmentButton" data-status="Doctor"><div class="icon inline icon-stethoscope"></div> Sent to see Doctor</button>
                            <button name="blah" id="skipButton" class="grey treatmentButton" data-status="treatmentSkip"><div class="icon inline icon-stethoscope"></div> Skip this step</button>
                        </div>
                        <p><b>Always set the date the employee's status changed, and add a note if helpful.</b> Setting this accurately will help provide you with data about how quickly your employee's return to work.</p>
                        <section class="formBlock dark">
                            <div class="formGrid">
                                <!--<div id="injuryDate"></div>-->
                                <div class="formInput">
                                <label for="status-date">date of update</label>
                                <input type="date" id="status-date-treatment" name="status-date"
                                    pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}">
                                </div>
                            </div>
                            
                            <textarea id="treatmentNote" style="width:100%;" placeholder="add a note" ></textarea><br><br>
                            
                        </section>
                        <div class="buttonArray" >
                                <button id="fillInBtnTreatment" class="red centered">select a status and set a date</button>
                                <button  id="submitStatusUpdateTreatment" class="cyan centered" required style="display:none;"><div class="icon inline icon-check-circle"></div> set updated status</button>
                        </div>
                    </div>                     
                
                </div>
            </section>
        </div>
    </div>
</div>

<script>
    //submit the update
$('#submitStatusUpdateTreatment').on('click', function(){
        var date = $('#status-date-treatment').val();
        var status = $('.treatmentButton.selected').data('status');
        var note = $('#treatmentNote').val();

        $.ajax({
            type: 'POST',
            url: '<?php echo route("updateEmployeeStatus"); ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                injury_id: {{ $injury->id }},
                status: status,
                date: date,
                note: note
            },
            success: function(data){
                $('#treatmentContainer').removeClass('doctor');
                $('#treatmentContainer').removeClass('skipStep');
                $('#treatmentContainer').removeClass('urgentcare');
                $('#treatmentContainer').removeClass('hospital');
                status = status.toLowerCase();
                if(status === "urgent care"){
                    status = "urgentcare";
                }else if(status === "treatmentSkip"){
                    status = "skipStep";
                }
                $('#treatmentContainer').addClass(status);
                getRecentPosts();
                getTreePosts();
                modal.close();

            },
            error: function(data){
                modal.close();
            }
        })

    });

    //sets selected class on eval buttons
    $('.treatmentButton').on('click', function(){
        $('.treatmentButton').removeClass('selected');
        $(this).addClass('selected');
    });

    //checks to see if a severity and date has been set
    $('#update_status_treatmentDiv').on('click', function(){
        console.log('running');
        if($('.treatmentButton').hasClass('selected')){
            console.log('button has class');
        }else{
            console.log('button does not have class');
        }




        if($('#status-date-treatment').val()){
            console.log('date has value');
        }else{
            console.log($('#status-date-treatment').val());
            console.log('date does not have value');
        }
        if($('.treatmentButton').hasClass('selected') && $('#status-date-treatment').val()){
            $('#fillInBtnTreatment').hide();
            $('#submitStatusUpdateTreatment').show();
        }else{
            $('#fillInBtnTreatment').show();
            $('#submitStatusUpdateTreatment').hide();
        }
    });
</script>
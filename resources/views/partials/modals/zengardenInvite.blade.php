
<div class="modalContent modalBlock dark" id="inviteZengardenModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/zengarden.png">invite to zengarden</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <section class="formBlock dark">
                    <div class="modalDescription">invite {{$injured_employee->name}} to the zengarden</div> 
                    @if(empty($injured_employee->email))
					<div class="formGrid">
                        <div class="formInput">
                            <label for="inviteZengardenEmail" id="inputEmailName">{{$injured_employee->name}}</label>
                            <div class="inputIcon mail"><input id="inviteZengardenEmail" name="inviteZengardenEmail"></div>    
                        </div>
                    </div>
                    @else
					<div class="formGrid">
                        <div class="formInput">
                            <label for="inviteZengardenEmail" id="nonInputEmailName">{{$injured_employee->name}}</label>
                            <!-- <span id="nonInputEmailName" class="name">{{$injured_employee->name}}</span> -->
                            <br>
                            <!-- <span id="nonInputEmail" class="email">{{$injured_employee->email}}</span> -->
                            <label id="nonInputEmail">{{$injured_employee->email}}</label>
                            <!-- <div class="inputIcon mail"><input id="inviteZengardenEmail" name="inviteZengardenEmail"></div>     -->
                        </div>
                    </div>
                    @endif

                    <div class="buttonArray">
                        <button class="olive centered" id="inviteToZengarden"><div class="icon icon-bamboo2"></div> invite!</button>
                    </div>                    
                </section>
            </section>
        </div>
    </div>
</div>

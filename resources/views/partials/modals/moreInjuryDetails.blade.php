<div class="modalContent modalBlock dark" id="moreInjuryDetailsModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/magnify.png"> full injury details</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
                <div class="zen-actionPanel"> 
                <table class="displayData" style="min-width:260px;">
                        <tr><td>date of injury</td><td><span class="injuryData date">{{$injury->injury_date}}</span></td></tr>
                    @if($injury->severity === "Severe")
                        <tr><td>severity</td><td><span id="severityOverviewText" class="injuryData severity critical">{{$injury->severity}}</span></td></tr>
                    @elseif($injury->severity === "Mild")
                        <tr><td>severity</td><td><span id="severityOverviewText" class="injuryData severity mild">{{$injury->severity}}</span></td></tr>
                    @else
                        <tr><td>severity</td><td><span id="severityOverviewText" class="injuryData severity moderate">{{$injury->severity}}</span></td></tr>
                    @endif
                        <tr><td>location(s)</td><td><span class="injuryData location">{{printLocations($injury)}}</span></td></tr>
                        <tr><td>type(s)</td><td><span class="injuryData type">{{printTypes($injury)}}</span></td></tr>
                        <tr><td>description</td><td><span id="injuryDetailsNotesText" class="injuryData description">{{$injury->notes}}</span></td></tr>
                        <tr><td>claim #</td><td><span class="injuryData claimnumber">{{$injury->claim_number ?: 'none entered'}}</span></td></tr>
                </table>
                </div>
            </section>
        </div>
    </div>
</div>

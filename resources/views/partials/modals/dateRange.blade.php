<?php 
$created_at = Auth::user()->getCompany()->created_at;
$created_at = \Carbon\Carbon::parse($created_at)->format('Y-m-d');
?>
<div class="modalContent modalBlock dark" id="dateRangeModal" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/date.png">date range</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
			<section class="formBlock dark">
				<div class="modalDescription">select a date range</div> 
					<!-- your code here-->
					<div class="formGrid">  
						<div class="formInput">
							<!-- input -->
							<label for="phone">from date</label>
							<input id="fromDate" type="date" value="{{$created_at}}"/>
						</div>
						<div class="formInput">
							<!-- input -->
							<label for="phone">to date</label>
							<input id="toDate" type="date" value="<?php echo date('Y-m-d'); ?>"/>
						</div>
					</div>
					
					<!-- end your code here--> 
					<div class="buttonArray">
						<button class="olive centered" id="submitDateRange"><div class="icon icon-calendar"></div> set range</button>
					</div>            
			</section>                 
		</section>  
	</div>
</div>

<script>

</script>
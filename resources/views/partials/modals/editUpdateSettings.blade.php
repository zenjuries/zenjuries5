<div class="modalContent modalBlock dark" id="editUpdateSettingsModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/email.png">email settings</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
                <section class="formBlock dark">
                    <div class="modalDescription">Subscribe to our <b>"Weekly Summary"</b> email to receive a brief overview of all your Zenjuries activity each Friday, <i>perfect for Agents who need to keep up with multiple injuries!</i><br>
                    <span class="FG__yellow">The weekly summary is also available at any time from your Zenboard page.</span></div> 
                    <h3>Current Setting: <span id="currentWeeklySummarySetting">
                        <?php
                        if($company->gets_unsubscribed_emails === 1){
                            echo "<b class='FG__green'>Subscribed</b>";
                        }else echo "<b class='FG__red'>Unsubscribed</b>";
                        ?>
                    </span></h3>

                    <div class="buttonArray">
                        <button id="subscribeWeeklyUpdate" class="weeklySummaryButton buttonColor green"><div class="icon icon-check-circle"></div>Enable Weekly Update</button>
                        <button id="unsubscribeWeeklyUpdate" class="weeklySummaryButton buttonColor red"><div class="icon icon-times-circle"></div>Disable Weekly Update.</button>
                    </div>                   
                </section>
            </section>
        </div>
    </div>
</div>

<script>
$('#subscribeWeeklyUpdate').on('click', function(){
    updateWeeklySummarySetting(1);
    $('#currentWeeklySummarySetting').html("<b class='FG__green'>Subscribed</b>");
    $(this).addClass('buttonColor').removeClass('buttonHover');
    $('#unsubscribeWeeklyUpdate').addClass('buttonHover').removeClass('buttonColor');
});

$('#unsubscribeWeeklyUpdate').on('click', function(){
    updateWeeklySummarySetting(0);
    $('#currentWeeklySummarySetting').html("<b class='FG__red'>Unsubscribed</b>");
    $(this).addClass('buttonColor').removeClass('buttonHover');
    $('#subscribeWeeklyUpdate').addClass('buttonHover').removeClass('buttonColor');
});

function updateWeeklySummarySetting(is_subscribed){
    var companyId = {{ $company->id }};
    var changeEmailSub = is_subscribed;
    console.log("companyid " + companyId);
    console.log("is_subscribed" + is_subscribed);
    console.log("change email sub" + changeEmailSub);
    $.ajax({
        'type': 'POST',
        'url': '{{ route("changeCompanyEmailSubscription") }}',
        'data': {
            _token: '<?php echo csrf_token(); ?>',
            company_id: {{ $company->id }},
            changeEmailSub: changeEmailSub
        },
        success: function (data){
                modal.close();
                showAlert("Email subscription updated!", "confirm", 5);
            },
        error: function (data){
            console.log(data);
            var errors = data.responseJSON;
            console.log(errors);
            showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
        }
    });
}

   // $('.mildinjury').on('click', function(){
    //     var mildInjury = $(this).data('mildinjury');
    //     // $('.mildInjury').toggleClass('selected');
    //     $(this).toggleClass('selected');
    // });

    // // $('.typeButton').on('click', function(){
    // //     $(this).toggleClass('selected');
    // // });

    // $('.moderateinjury').on('click', function(){
    //     var Severeinjury = $(this).data('moderateinjury');
    //     // $('.moderateInjury').toggleClass('selected');
    //     $(this).toggleClass('selected');
    // });

    // $('#resolvedinjury').on('click', function(){
    //     var resolvedInjury = $(this).data('resolvedinjury');
    //     //$('.resolvedInjury').toggleClass('selected');
    //     //var zen_carrier = $('#zenCarrierSelect').val();
    //     $(this).toggleClass('selected');
    // });

    // // $('.openInjury').on('click', function(){
    // //     var openInjury = $(this).data('openInjury');
    // //     $('.openInjury').toggleClass('selected');
    // // });

    // $('#openinjury').on('click', function(){
    //     var openInjury = $(this).data('openinjury');
    //     console.log("openInjury: " + openInjury);
    //     $(this).toggleClass('selected');
    // });

    // $('.bothinjury').on('click', function(){
    //     var bothInjury = $(this).data('bothinjury');
    //     $(this).toggleClass('selected');
    // });

    // $('.bothinjuryTwo').on('click', function(){
    //     var bothInjuryTwo = $(this).data('bothinjurytwo');
    //     $(this).toggleClass('selected');
    // });

    // $('.lossReportsStatusButton').on('click', function(){
    //     $('.lossReportsStatusButton').removeClass('selected');
    //     $(this).addClass('selected');
    // });

    // $('.locationButton').on('click', function(){
    //    var code = $(this).data('naiscscode');
    //    console.log(code);
    //    //this should toggle the class on the duplicate buttons (on the view all modal + other modals)
     //   $('.locationButton[data-naiscscode="' + code +'"]').toggleClass('selected');
    // });
</script>
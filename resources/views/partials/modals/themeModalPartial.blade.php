<div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/theme.png">change your theme</span></div>
    <div class="modalBody" style="min-width:360px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <div class="sectionContent">
                    <section class="formBlock dark">
                        <div class="themeSelector"> 
                            <ul>
                                
                                @foreach($themes as $key => $theme)
                                    @if($theme === '.' or $theme === '..') 
                                    @elseif(is_dir($path . '/' . $theme))
                                        @if($theme === $user_theme)
                                            <li class="selected">
                                        @else
                                            <li>
                                        @endif
                                            <div class="themeSelectorBg {{$theme}}" data-theme="{{$theme}}"> 
                                            <div class="themeTitleBar"><span class="themeName"></span></div>
                                            <span class="themeColor color1"></span>
                                            <span class="themeColor color2"></span>
                                            <span class="themeColor color3"></span>
                                            <span class="themeColor color4"></span>
                                        </div></li>
                                    @endif
                                @endforeach
                            </ul>   
                        </div>
                    </section>
                </div>                    
            </section>           
        </div>
    </div> 
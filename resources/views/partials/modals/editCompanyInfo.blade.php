<div class="modalContent modalBlock dark" id="editCompanyInfoModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/company.png">company info</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
                <section class="formBlock dark">
                    <div class="modalDescription">edit your company info.</div> 
					<div class="formGrid">  
						<div class="formInput">
							<label for="companyPhone">phone</label>
							<div class="inputIcon user"><input id="companyPhone" class="phone_us" type="tel" aria-label="Please enter your phone number" placeholder="ex. (111)-111-1111" value="{{ $company->company_phone }}"/></div>
							<span class="inputError">enter phone</span>
						</div>
						<div class="formInput">
							<label for="mailingAddress">address line 1</label>
							<div class="inputIcon address"><input id="mailingAddress" type="text" value="{{ $company->mailing_address }}"/></div>
							<span class="inputError">enter address</span>
						</div>
						<div class="formInput">
							<label for="MailingAddressLine2">address line 2</label>
							<div class="inputIcon address"><input id="mailingAddressLine2" type="text" value="{{ $company->mailing_address_line_2 }}"/></div>
						</div>
						<div class="formInput">
							<label for="mailingCity">city</label>
							<div class="inputIcon address"><input id="mailingCity" type="text" value="{{ $company->mailing_city }}"/></div>
							<span class="inputError">enter city</span>
						</div>

						<div class="formInput">
							<label for="mailingState">state</label>
							<div class="inputIcon address">
								<select id="mailingState" name="state"><option>{{ is_null($company->mailing_state) ? "choose state" : $company->mailing_state }}</option><option value="Alabama">Alabama</option><option value="Alaska">Alaska</option><option value="Arizona">Arizona</option><option value="Arkansas">Arkansas</option><option value="California">California</option><option value="Colorado">Colorado</option><option value="Connecticut">Connecticut</option><option value="Delaware">Delaware</option><option value="District of Columbia">District of Columbia</option><option value="Florida">Florida</option><option value="Georgia">Georgia</option><option value="Guam">Guam</option><option value="Hawaii">Hawaii</option><option value="Idaho">Idaho</option><option value="Illinois">Illinois</option><option value="Indiana">Indiana</option><option value="Iowa">Iowa</option><option value="Kansas">Kansas</option><option value="Kentucky">Kentucky</option><option value="Louisiana">Louisiana</option><option value="Maine">Maine</option><option value="Maryland">Maryland</option><option value="Massachusetts">Massachusetts</option><option value="Michigan">Michigan</option><option value="Minnesota">Minnesota</option><option value="Mississippi">Mississippi</option><option value="Missouri">Missouri</option><option value="Montana">Montana</option><option value="Nebraska">Nebraska</option><option value="Nevada">Nevada</option><option value="New Hampshire">New Hampshire</option><option value="New Jersey">New Jersey</option><option value="New Mexico">New Mexico</option><option value="New York">New York</option><option value="North Carolina">North Carolina</option><option value="North Dakota">North Dakota</option><option value="Northern Marianas Islands">Northern Marianas Islands</option><option value="Ohio">Ohio</option><option value="Oklahoma">Oklahoma</option><option value="Oregon">Oregon</option><option value="Pennsylvania">Pennsylvania</option><option value="Puerto Rico">Puerto Rico</option><option value="Rhode Island">Rhode Island</option><option value="South Carolina">South Carolina</option><option value="South Dakota">South Dakota</option><option value="Tennessee">Tennessee</option><option value="Texas">Texas</option><option value="Utah">Utah</option><option value="Vermont">Vermont</option><option value="Virginia">Virginia</option><option value="Virgin Islands">Virgin Islands</option><option value="Washington">Washington</option><option value="West Virginia">West Virginia</option><option value="Wisconsin">Wisconsin</option><option value="Wyoming">Wyoming</option></select>
							</div>	
							<span class="inputError">enter state</span>
						</div>

						<div class="formInput">
							<label for="mailingZip">zip</label>
							<div class="inputIcon address"><input id="mailingZip" type="text" value="{{ $company->mailing_zip }}"/></div>
							<span class="inputError">enter zip</span>
						</div>
					</div>
                    <div class="buttonArray">
					<button class="cyan centered" id="submitCompanyInfo"><div class="icon icon-check-circle"></div> update info</button>
                    </div>                   
                </section>
            </section>
        </div>
    </div>
</div>

<script>
$('#submitCompanyInfo').on('click', function(){
        //company id
        var companyId = {{ $company->id }};
        //getting values from modal
        var companyPhone = $('#companyPhone').val();
        var companyMailingAddress = $('#mailingAddress').val();
        var companyMailingAddressLine2 = $('#mailingAddressLine2').val();
        var companyMailingCity = $('#mailingCity').val();
        var companyMailingState = $('#mailingState').val();
        var companyMailingZip = $('#mailingZip').val();

        console.log("show variables");
        console.log(companyId);
        console.log(companyPhone);
        console.log(companyMailingAddress);
        console.log(companyMailingAddressLine2);
        console.log(companyMailingCity);
        console.log(companyMailingState);
        console.log(companyMailingZip);
        console.log("end show variables");

        var valid = true;

        //simple js validation
        if(companyPhone == "" || companyPhone.length < 14){
            valid = false;
            $('#companyPhone').parent().parent().find('.inputError').addClass('show');
        }
        else{
            $('#companyPhone').parent().parent().find('.inputError').removeClass('show');   
        }

        if(companyMailingAddress == ""){
            valid = false;
            $('#mailingAddress').parent().parent().find('.inputError').addClass('show');
        }
        else{
            $('#mailingAddress').parent().parent().find('.inputError').removeClass('show');   
        }

        if(companyMailingCity == ""){
            valid = false;
            $('#mailingCity').parent().parent().find('.inputError').addClass('show');
        }
        else{
            $('#mailingCity').parent().parent().find('.inputError').removeClass('show');   
        }

        if(companyMailingState == "choose state"){
            valid = false;
            $('#mailingState').parent().parent().find('.inputError').addClass('show');
        }
        else{
            $('#mailingState').parent().parent().find('.inputError').removeClass('show');   
        }

        if(companyMailingZip == ""){
            valid = false;
            $('#mailingZip').parent().parent().find('.inputError').addClass('show');
        }
        else{
            $('#mailingZip').parent().parent().find('.inputError').removeClass('show');   
        }

        if(valid){
            $.ajax({
                type: 'POST',
                url: '{{ route("updateCompanyInfo") }}',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    companyId: companyId,
                    phone: companyPhone,
                    mailingAddress: companyMailingAddress,
                    mailingAddressLine2: companyMailingAddressLine2,
                    mailingCity: companyMailingCity,
                    mailingState: companyMailingState,
                    mailingZip: companyMailingZip
        
                },
                success: function(){
                    updateCompanyInfoFields();
                    //closeModals();
                    modal.close();
                    showAlert("Thanks for your update!", "confirm", 5);
                },
                error: function(){
                    showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
                }
        
            });
        }
        
    });	

function updateCompanyInfoFields(){
    var phone = $('#companyPhone').val();
    var address = $('#mailingAddress').val();
    var addressLine2 = $('#mailingAddressLine2').val();
    var city = $('#mailingCity').val();
    var state = $('#mailingState').val();
    var zip = $('#mailingZip').val();

     //sectionPanel on page
     $('#currentCompanyPhone').text(phone);
     $('#currentCompanyMailingAdress').text(address);
     $('#currentCompanyMailingLine2').text(addressLine2);
     $('#currentCompanyMailingCity').text(city);
     $('#currentCompanyMailingState').text(state);
     $('#currentCompanyMailingZipcode').text(zip);
}	
</script>
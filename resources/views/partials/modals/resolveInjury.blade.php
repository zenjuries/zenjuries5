<div class="modalContent modalBlock dark" id="resolveInjuryModal" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/heart.png">resolve injury</span></div>
    <div class="modalBody" style="min-width:260px;">
        <section class="sectionPanel dark">
            <section class="formBlock dark">
                <div class="modalDescription">Is this injury ready to be resolved?</div>
                <br>
                <div class="divCenter">Please enter the official Claim Close Date for this claim. You can get this date from your carrier, or ask your insurance agent if you aren't sure.</div>
                <div class="formGrid">
                    <div class="formInput">
                        <label for="resolveDate">resolve date</label>
                        <input id="resolveDate" type="date"/>
                    </div> 
                </div>
            <div class="buttonArray">
                <button class="green centered" id="submitInjuryResolved"><div class="icon icon-heart"></div> resolve injury</button>
            </div>  
            <hr>
            <div class="divCenter">Resolve the injury without a date.<br>You will have to add this later on when you have it.</div>
            <br>
            <div class="buttonArray">
                <button class="orange centered" id="submitInjuryResolvednoDate"><div class="icon icon-heart"></div> resolve without date</button>
            </div> 
            </section>                 
        </section>  
    </div>
</div>
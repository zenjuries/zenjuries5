<div class="modalContent modalBlock dark" id="passwordModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/lock.png">update password</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <div class="sectionContent">
                    <section class="formBlock dark">                          
                        <div class="formGrid">  
                            <div class="formInput">
                                <!-- input -->
                                <label for="newPassword">new password</label>
                                <input id="newPassword" type="password" />
                                <span class="inputError">error</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="confirmNewPassword">confirm new password</label>
                                <input id="confirmNewPassword" type="password" />
                                <span class="inputError">error</span>
                            </div>
                            <div class="formInput">
                                <button class="cyan" id="submitPasswordChange">submit</button>
                            </div>
                        </div>
                    </section>
                </div>                    
            </section>           
        </div>
    </div> 
</div>

<script>
$('#submitPasswordChange').on('click', function(){
    var valid = true;

    var password = $('#newPassword').val();
    var password_confirm = $('#confirmNewPassword').val();

    if(password === "" || password_confirm === ""){
        valid = false;
        $('#newPassword').parent().find('.inputError').addClass('show');
        $('#confirmNewPassword').parent().find('.inputError').addClass('show');
    }

    if(password !== password_confirm){
        valid = false;
        $('#newPassword').parent().find('.inputError').addClass('show');
        $('#confirmNewPassword').parent().find('.inputError').addClass('show');
    }

    if(password.length < 6){
        valid = false;
        $('#newPassword').parent().find('.inputError').addClass('show');
        $('#confirmNewPassword').parent().find('.inputError').addClass('show');
    }
    if(valid === true){
        updatePassword(password, password_confirm);
    }
});

function updatePassword(password, password_confirmation){
    $.ajax({
        type: 'POST',
        url: '/zengarden/updatePassword',
        data: {
            _token: '<?php echo csrf_token(); ?>',
            user_id: {{ $user->id }},
            password: password,
            password_confirmation: password_confirmation
        },
        success: function(data){
            $('#passwordChangedAt').html(data);
            modal.close();
            $('#passwordModal').find('input').val("");
            showAlert("Your password has been updated!", "confirm", 5);
        },
        error: function(data){
            console.log(data.responseText);
            showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
        }
    })
}    
</script>    
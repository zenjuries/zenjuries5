<div class="modalContent modalBlock dark" id="popmessageModal2" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/<?php echo $popmessage2[0]?>.png"><?php echo $popmessage2[1]?></span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
			<section class="formBlock dark">
				<div class="modalDescription" id="popModalMessage2"><?php echo $popmessage2[2]?></div> 			
                <div class="buttonArray">
                    <button class="<?php echo $popmessage2[3]?> centered" id="<?php echo $popmessage2[4]?>"><div class="icon icon-<?php echo $popmessage2[5]?>"></div> <?php echo $popmessage2[6]?></button>
					<button class="<?php echo $popmessage2[7]?> centered" id="<?php echo $popmessage2[8]?>"><div class="icon icon-<?php echo $popmessage2[9]?>"></div> <?php echo $popmessage2[10]?></button>
                </div>            
			</section>                 
		</section>  
	</div>
</div>
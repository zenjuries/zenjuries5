<div class="modalContent modalBlock dark" id="sampleModal" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/company.png">sample title</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
			<section class="formBlock dark">
				<div class="modalDescription">something about this form</div> 
					<!-- your code here-->
					<div class="formGrid">  
						<div class="formInput">
							<!-- input -->
							<label for="email">phone</label>
							<div class="inputIcon phone"><input id="companyPhone"  class="phone_us" type="tel" aria-label="Please enter your phone number" placeholder="ex. (111)-111-1111"/></div>
							<span class="inputError" style="display:block !important;">numbers only</span>
						</div>
						<div class="formInput">
							<!-- input -->
							<label for="phone">address line 1</label>
							<div class="inputIcon address"><input id="mailingAddress" type="text" /></div>
							<span class="inputError" style="display:block !important;">--</span>
						</div>
						<div class="formInput">
							<!-- input -->
							<label for="email">data mask test (date)</label>
							<div class="inputIcon date"><input class="phone_us" id="test" type="text" data-mask="(000) 000-0000"/></div>
							<span class="inputError" style="display:block !important;">--</span>
						</div>
						<div class="formInput">
							<!-- input -->
							<label for="phone">city</label>
							<div class="inputIcon city"><input id="mailingCity" type="text"/></div>
							<span class="inputError" style="display:block !important;">--</span>
						</div>

						<div class="formInput">
							<label for="phone">state</label>
							<div class="inputIcon state">
								<select id="mailingState" name="state">
								<option value="default" selected>the default</option>    
								<option value="---">choose state</option><option value="Alabama">Alabama</option><option value="Alaska">Alaska</option><option value="Arizona">Arizona</option><option value="Arkansas">Arkansas</option><option value="California">California</option><option value="Colorado">Colorado</option><option value="Connecticut">Connecticut</option><option value="Delaware">Delaware</option><option value="District of Columbia">District of Columbia</option><option value="Florida">Florida</option><option value="Georgia">Georgia</option><option value="Guam">Guam</option><option value="Hawaii">Hawaii</option><option value="Idaho">Idaho</option><option value="Illinois">Illinois</option><option value="Indiana">Indiana</option><option value="Iowa">Iowa</option><option value="Kansas">Kansas</option><option value="Kentucky">Kentucky</option><option value="Louisiana">Louisiana</option><option value="Maine">Maine</option><option value="Maryland">Maryland</option><option value="Massachusetts">Massachusetts</option><option value="Michigan">Michigan</option><option value="Minnesota">Minnesota</option><option value="Mississippi">Mississippi</option><option value="Missouri">Missouri</option><option value="Montana">Montana</option><option value="Nebraska">Nebraska</option><option value="Nevada">Nevada</option><option value="New Hampshire">New Hampshire</option><option value="New Jersey">New Jersey</option><option value="New Mexico">New Mexico</option><option value="New York">New York</option><option value="North Carolina">North Carolina</option><option value="North Dakota">North Dakota</option><option value="Northern Marianas Islands">Northern Marianas Islands</option><option value="Ohio">Ohio</option><option value="Oklahoma">Oklahoma</option><option value="Oregon">Oregon</option><option value="Pennsylvania">Pennsylvania</option><option value="Puerto Rico">Puerto Rico</option><option value="Rhode Island">Rhode Island</option><option value="South Carolina">South Carolina</option><option value="South Dakota">South Dakota</option><option value="Tennessee">Tennessee</option><option value="Texas">Texas</option><option value="Utah">Utah</option><option value="Vermont">Vermont</option><option value="Virginia">Virginia</option><option value="Virgin Islands">Virgin Islands</option><option value="Washington">Washington</option><option value="West Virginia">West Virginia</option><option value="Wisconsin">Wisconsin</option><option value="Wyoming">Wyoming</option></select>
							</div>	
						</div>

						<div class="formInput">
							<!-- input -->
							<label for="phone">zip</label>
							<div class="inputIcon zip"><input id="mailingZip" type="text"/></div>
							<span class="inputError">numbers only</span>
						</div>

						<div class="formInput">
							<!-- input -->
							<label for="phone">pick a date</label>
							<input id="dateTest" type="date"/>
							<span class="inputError" style="display:block !important;">--</span>
						</div>
						<div class="formInput">
							<!-- input -->
							<label for="phone">city</label>
							<div class="inputIcon city"><input id="test2" type="text"/></div>
							<span class="inputError" style="display:block !important;">--</span>
						</div>

					</div>
					
					<div class="formGrid short">
						<div class="formInput">
							<input type="radio" name="r1" id="newClaim">
							<label for="newClaim">radio 1</label>

							<input type="radio" name="r1" id="preClaim">
							<label for="preClaim">radio 2</label>

							<input type="radio" name="r1" id="demoClaim">
							<label for="demoClaim" style="color:grey;">radio 3</label>
						</div>
					</div>

					<!-- end your code here--> 
					<div class="buttonArray">
						<button class="cyan centered" id="submitCompanyInfo"><div class="icon icon-retweet"></div> update</button>
					</div>            
			</section>                 
		</section>  
	</div>
</div>
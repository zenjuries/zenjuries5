
<div class="modalContent modalBlock dark" id="myInfoModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/user.png">edit your info</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <div class="sectionContent">
                    <section class="formBlock dark">                          
                        <div class="formGrid">  
                            <div class="formInput">
                                <!-- input -->
                                <label for="userName">name</label>
                                <input id="userName" type="text" value="{{ $user->name }}"/>
                                <span class="inputError">error</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="phone">phone</label>
                                <input id="phone" class="phone_us" type="tel" value="{{ (empty($user->phone) ? "" : $user->phone) }}"/>
                                <span class="inputError">error</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="email">email</label>
                                <input id="email" type="text" value="{{ $user->email }}"/>
                                <span class="inputError">error</span>
                            </div>
                            <div class="formInput" style="height:160px;">
                                <label for="description">description</label>
                                <textarea id="description" style="font-size:.8rem;width:220px;height:110px;" type="text" value="{{ (empty($user->description) ? "" : $user->description) }}">{{ (empty($user->description) ? "" : $user->description) }}</textarea>
                                <span style="display:block;font-size:.7rem;" id="description_char_count">MAX 140 characters</span>
                                <span class="inputError">error</span>
                            </div>
                        </div>
                        <div class="buttonArray">
                            <button class="cyan centered" id="submitUserInfo"><div class="icon icon-user"></div> update</button>
                        </div>  
                    </section>
                </div>                    
            </section>           
        </div>
    </div> 
</div>



<script>
let textArea = document.getElementById("description");
let characterCounter = document.getElementById("description_char_count");
let textString = textArea.innerText.length;
var maxNumOfChars = 140;
if(textString == 0){
    characterCounter.textContent = "MAX 140 characters";
}
else{
    characterCounter.textContent = textString + "/140";
}

if (textString > 140) {
    characterCounter.style.color = "red";
} else if (textString > 120) {
    characterCounter.style.color = "orange";
} else {
    characterCounter.style.color = "white";
}


const countCharacters = () => {
    let numOfEnteredChars = textArea.value.length;
    characterCounter.textContent = numOfEnteredChars + "/140";

      if (numOfEnteredChars > 140) {
        characterCounter.style.color = "red";
    } else if (numOfEnteredChars > 120) {
        characterCounter.style.color = "orange";
    } else {
        characterCounter.style.color = "white";
    }
};
textArea.addEventListener("input", countCharacters);
</script>



<script>
$('#submitUserInfo').on('click', function(){
let numOfEnteredChars = textArea.value.length;
var name = $('#userName').val();
var email = $('#email').val();
var phone = $('#phone').val();
var description = $('#description').val();
var valid = true;
if(name === ""){
    valid = false;
    $('#userName').parent().find('.inputError').addClass('show');
}
if(email === ""){
    valid = false;
    $('#email').parent().find('.inputError').addClass('show');
}
if(phone.length < 14){
    valid = false;
    $('#phone').parent().find('.inputError').addClass('show');
}
if(numOfEnteredChars > 140){
    valid = false;
    $('#description_char_count').parent().find('.inputError').addClass('show');
}

if(valid === true){
    updateInfo(name, email, phone, description);
}
});
 
function updateInfo(name, email, phone, description) {
        $.ajax({
            type: 'POST',
            url: '/zengarden/updateInfo',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                user_id: {{ $user->id }},
                name: name,
                email: email,
                phone: phone,
                description: description
            },
            success: function (data) {
                //update user info section
                $('#currentUserName').html(name);
                $('#currentUserEmail').html(email);
                $('#currentUserPhone').html(phone);
                if(description){
                    $('#currentUserDescription').html(description);
                }

                if(description == ""){
                    $('#currentUserDescription').html("no comments yet");
                }

                $('#userName').parent().find('.inputError').removeClass('show');
                $('#email').parent().find('.inputError').removeClass('show');
                $('#phone').parent().find('.inputError').removeClass('show');
                $('#description_char_count').parent().find('.inputError').removeClass('show');
                //update modal
                $('#name').html(name);
                $('#email').html(email);
                $('#phone').html(phone);
                if(description){
                    $('#description').html(description);
                }

                modal.close();
                showAlert("Info Updated!", "confirm", 5);
            },
            error: function (data) {
                console.log(data);
                var errors = data.responseJSON;
                console.log(errors);
                showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
            }
        });
}    
</script>
<div id="add_media_description" class="zModal" style="display:none">
    <div class="modalContent dark">
        <div class="modalHeader"><span class="modalTitle"><div class="icon inline icon-picture"></div> edit description</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                <section class="formBlock dark">                          
                    <div class="formGrid">  
                        <div class="formInput">
                            <!-- input -->
                            <label for="photo_description">description</label>
                            <textarea id="photo_description"></textarea>
                        </div>
                    </div>
                </div> 
                <div class="center"><button class="cyan">set description</button></div>                   
            </section>  
        </div>
    </div> 
</div>
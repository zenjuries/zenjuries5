<?php

    $company_id = Auth::user()->getCompany()->id;
    $company = \App\Company::where('id', $company_id)->first();?> 

<script src="{{URL::asset('/js/zp/colorpicker.js') }}"></script>

 <div class="modalContent modalBlock dark" id="companyColor" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/setcolor.png">company color</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark" style="padding:1rem;">         
                <div id="colorpicker"></div>
                <br>
                
                <div class="buttonArray">
                    <button class="centered" id="colorModalSave"><div class="icon icon-retweet"></div> save color</button>
                </div>  
            </section>
        </div>
    </div>
</div>


<script>

    var savedColor = <?php echo json_encode($company->logo_color); ?>;
    var defaultColor = "";

    if(savedColor === null || savedColor === ""){
        defaultColor = "#f00"
    }
    else{
        defaultColor = savedColor;
    }

    var colorPicker = new iro.ColorPicker("#colorpicker", {
    // Set the size of the color picker
    width: 280,
    // Set the initial color to pure red
    color: defaultColor
});

colorPicker.on('color:change', function(color) {
    // log the current color as a HEX string
    document.getElementById('colorModalSave').style.background=color.hexString;
    console.log(color.hexString);
});

$('#colorModalSave').on('click', function(){
        var color = colorPicker.color.hexString;
        console.log(color);
        $.ajax({
        type: 'POST',
            url: '/setCompanyColor',
            data: {
            _token: '<?php echo csrf_token(); ?>',
            company_id: {{ $company_id }},
            color: color,
            },
            success: function(){
                modal.close();
                //var avatar_number = $('#changeAvatar').find('a.selected').data('avatar');
                //setAvatarOnPage(color, avatar_number);
                console.log('success color save');
                $('.profileCompanyColor').css('background-color', color);
                $('.profileCompanyColor').css('border-color', color);
                $('#policyholderColorMobile').css('background-color', color);
                //$('.avatarMenu-btn').css('background-color', color);
                //createAvatar(color, avatar_number);
            },
            error: function(){
                showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
            }
    
        });
        
    });

</script>
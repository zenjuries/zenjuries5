<div class="modalContent modalBlock dark" id="add_user" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/user.png">add user</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark">             
            <section class="formBlock dark">
                <div class="modalDescription"><span style="font-size:1.4rem;font-weight:300;display:block;margin-bottom:12px;">Building a winning team means fielding the best players.</span>
                <span style="font-size:1rem;font-weight:300;display:block;text-align:justify;margin-bottom:10px;color:#bbb;">Please invite as many valuable assets as you think you might need.  There is no limit on how many players you can have available on your roster.</span>
                
                <span style="font-size:1rem;font-weight:300;display:block;text-align:justify;margin-bottom:10px;color:#bbb;">Once added, you can build teams by placing these individuals into the roles that you would like them to perform. They'll get an email containing a sign-up link.</span> 

                <span style="font-size:1rem;font-weight:300;display:block;text-align:justify;margin-bottom:10px;color:#bbb;">If they do have an account already (for example, if they're a doctor who works with multiple companies) they'll appear in your roster right away!</span></div> 
                <div class="formGrid">  
                    <div class="formInput">
                        <!-- input -->
                        <label for="email">user name</label>
                        <div class="inputIcon user"><input id="newuser" type="text" /></div>
                        <!--<span class="inputError show">Name Required</span>-->
                    </div>
                    <div class="formInput">
                        <!-- input -->
                        <label for="phone">user email</label>
                        <div class="inputIcon mail"><input id="email" type="text" /></div>
                        <!--<span class="inputError show">Email Required</span>-->
                    </div>
                </div> 
                <div class="buttonArray">
                    <button id="inviteNewUser" class="cyan centered"><div class="icon icon-user"></div> add new user</button>
                </div>                                       
            </section>                 
        </section>  
    </div> 
</div>
<script>
    $('#inviteNewUser').on('click', function(){
        var usertype = $(this).data('usertype');
        var company_id = $(this).data('companyid');

        addUser();
    });
    /*Creates a new user */
    function addUser(){
        var name = document.getElementById("newuser").value;
        var email = document.getElementById("email").value;
        var phone = null;
        var _token = '<?php echo csrf_token(); ?>';
        console.log(name);
        console.log(email);
        $.ajax({
            type: 'POST',
            url: '<?php echo route('addUser') ?>',
            data: {
                name: name,
                email: email,
                _token: _token
            },
            success: function (data) {
                showAlert("We've sent them an invitation! Once they finish creating their account you'll be able to add them to your team!", "confirm", 5);
                // loadRoster();
                modal.close();
                document.getElementById('email').value = "";
                document.getElementById('newuser').value = "";
                $('.userTypeButton').removeClass('selected');
                newUserType = "";
                $('#addUserModal').removeData('bs.modal');
            },
            error: function(data){
                var errorMsg = JSON.parse(data.responseText);
                if(typeof errorMsg['error'] == "undefined"){
                    errorMsg['error'] = "";
                }
                if(errorMsg['email'] !== undefined){
                    errorMsg['error']+= (errorMsg['email'] + "<br>");
                }
                if(errorMsg['type'] !== undefined){
                    errorMsg['error']+= (errorMsg['type'] + "<br>");
                }
                document.getElementById('addUserFormErrors').innerHTML = errorMsg['error'];
                document.getElementById('addUserFormErrors').style.display = "";
            }
        });
    }
</script>
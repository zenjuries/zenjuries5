<div class="modalContent modalBlock dark" id="renameTeamModal" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/team-bright.png">rename team</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
			<section class="formBlock dark">
				<div class="modalDescription">use this to rename your team</div> 
                    <div class="formGrid">
                        <div class="formInput">
                            <label for="updateTeamName">new name</label>
                            <div class="inputIcon user"><input id="updateTeamName" name="updateTeamName"></div>
                        </div>
                    </div>			
					<div class="buttonArray">
						<button class="cyan centered" id="submitNewTeamName"><div class="icon icon-retweet"></div> rename</button>
					</div>            
			</section>                 
		</section>  
	</div>
</div>
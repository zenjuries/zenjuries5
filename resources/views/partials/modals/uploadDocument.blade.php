<div class="modalContent modalBlock dark" id="uploadDocumentModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/myphoto.png">upload document</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <div class="sectionDescription">allowed file types:  .doc, .docx, .pdf, .txt</div> 
                <div class="divCenter"><input type="file" id="uploadDocInput"></div>
                <br>
                <div class="buttonArray">
                    <button class="cyan centered" id="uploadDoc"><div class="icon icon-retweet"></div> upload document</button>
                </div>             
            </section>
        </div>
	</div>
</div>
  
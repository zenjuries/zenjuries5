<div class="modalContent modalBlock" id="registerSuccess" style="display: none;">
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <div class="divCenter  animate__animated animate__pulse"><img src="/images/icons/welcome.png" style="width:100%;max-width:300px;"></div>
            <br>
            <div class="divCenter  animate__animated animate__pulse animate__delay-1s"><span style="color:lime;font-size:1.8rem;font-weight:900;">Successful Registration</span></div>
            <p>In a moment you will be directed to your dashboard...</p>
        </div>
    </div>
</div>
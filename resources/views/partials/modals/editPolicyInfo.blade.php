<div class="modalContent modalBlock dark" id="editPolicyInfoModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/insurance.png">policy information</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
            <!-- any content can be added in this area--> 

                <!-- below is a sample of form layout -->
                <section class="formBlock dark">
                    <div class="modalDescription">edit your policy info.</div> 
					<div class="formGrid">  
						<div class="formInput">
							<!-- input -->
							<label for="classCode">main class code</label>
							<div class="inputIcon number"><input id="classCode" type="text" value="{{ $company->class_code }}" /></div>
                            <span class="inputError">enter code</span>
                        </div>
						<div class="formInput">
							<!-- input -->
							<label for="policyNumber">policy number</label>
							<div class="inputIcon number"><input id="policyNumber" type="text" value="{{ $company->policy_number }}" /></div>
                            <span class="inputError">enter policy</span>
                        </div>
                    </div>
                    <div class="formGrid">
                        <div class="formInput">
                            <label for="claimNumberInput">Renewal Month / Day</label>
                            <div class="inputIcon month" style="display:inline-block;">
                                <select name="renewalMonth" id="renewalMonth">                
                                    <option value="{{ $month }}">{{$month}}</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select> 
                            </div>
                            <div class="inputIcon day" style="display:inline-block;"><input type="text" id="renewalDay" name="renewalDay" style="width:60px;" value="{{ $day }}"></div>
                            <span class="inputError">enter date</span>
                        </div>
                    </div>

                    <!-- make sure to always use the buttonArray for centered buttons and multi buttons-->
                    <div class="buttonArray">
                    <button class="cyan centered" id="submitPolicyInfo"><div class="icon icon-check-circle"></div> update info</button>
                    </div>                   
                </section>
                <!-- end of sample form -->

            <!--end of any added content -->
            </section>
        </div>
    </div>
</div>

<script>
function updatePolicyInfoFields(){
    var classCode = $('#classCode').val();
    var policyNumber = $('#policyNumber').val();
    var renewalDay = $('#renewalDay').val();
    var renewalMonth = $('#renewalMonth').val();
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    //update section on page
    $('#currentClassCode').text(classCode);
    $('#currentPolicyNumber').text(policyNumber);
    $('#currentRenewalDay').text(renewalDay);
    $('#currentRenewalMonth').text(months[renewalMonth - 1]);
}

$("#submitPolicyInfo").on('click', function(){
    //grab the company id
    var companyId = {{ $company->id }};
    //get the variables from the modal
    var classCode = $('#classCode').val();
    var policyNumber = $('#policyNumber').val();
    var renewalDay = $('#renewalDay').val();
    var renewalMonth = $('#renewalMonth').val();
    
    //validation variable
    var valid = true;

    if(classCode === "" || classCode === undefined){
        valid = false;
        $('#classCode').parent().parent().find('.inputError').addClass('show');
    }
    else{
        $('#classCode').parent().parent().find('.inputError').removeClass('show');
    }

    if(policyNumber === "" || policyNumber === undefined){
        valid = false;
        $('#policyNumber').parent().parent().find('.inputError').addClass('show');
    }
    else{
        $('#policyNumber').parent().parent().find('.inputError').removeClass('show');
    }

    if(renewalDay > 31 || renewalDay <= 0|| renewalDay === "" || renewalDay === undefined){
        valid = false;
        $('#renewalDay').parent().parent().find('.inputError').addClass('show');
    }
    else{
        $('#renewalDay').parent().parent().find('.inputError').removeClass('show');
    }

    if(renewalMonth > 12 || renewalMonth === "select" || renewalMonth === undefined){
        valid = false;
        $('#renewalMonth').parent().parent().find('.inputError').addClass('show');
    }

    if(valid === true){
        $.ajax({
            type: 'POST',
            url: '{{ route("updatePolicyInfo") }}',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                companyId: companyId,
                classCode: classCode,
                policyNumber: policyNumber,
                renewalDay: renewalDay,
                renewalMonth: renewalMonth
            },
            success: function (data){
                updatePolicyInfoFields();
                modal.close();
                showAlert("Policy info updated!", "confirm", 5);
            },
            error: function (data){
                console.log(data);
                var errors = data.responseJSON;
                console.log(errors);
                showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
            }
    
        });
    }

});
</script>
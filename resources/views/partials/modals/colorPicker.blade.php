<script src="{{URL::asset('/js/zp/colorpicker.js') }}"></script>

 <div class="modalContent modalBlock dark" id="changeColor" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/setcolor.png">set color</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark" style="padding:1rem;">         
                <div id="colorpicker"></div>
                <br>
                
                <div class="buttonArray">
                    <button class="centered" id="colorModalSave"><div class="icon icon-retweet"></div> save color</button>
                </div>  
            </section>
        </div>
    </div>
</div>


<script>
var colorPicker = new iro.ColorPicker("#colorpicker", {
    // Set the size of the color picker
    width: 280,
    // Set the initial color to pure red
    color: "#f00"
});

colorPicker.on('color:change', function(color) {
    // log the current color as a HEX string
    document.getElementById('colorModalSave').style.background=color.hexString;
    console.log(color.hexString);
});

$('#colorModalSave').on('click', function(){
        var color = colorPicker.color.hexString;
        $.ajax({
        type: 'POST',
            url: '/zengarden/setColor',
            data: {
            _token: '<?php echo csrf_token(); ?>',
            user_id: {{ $user->id }},
            color: color,
            },
            success: function(){
                modal.close();
                var avatar_number = $('#changeAvatar').find('a.selected').data('avatar');
                //setAvatarOnPage(color, avatar_number);
                console.log('success color save');
                $('.profileAvatarColor').css('background-color', color);
                $('.avatarMenu-btn').css('background-color', color);
                $('.profileAvatarColor').css('border-color', color);
                $('.avatarMenu-btn').css('border-color', color);
                createAvatar(color, avatar_number);
            },
            error: function(){
                showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
            }
    
        });
    });

</script>
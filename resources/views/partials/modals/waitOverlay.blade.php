<div class="modalContent modalBlock" id="waitspinnerModal" style="display: none;">
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <div class="waitSpinner">
                <div class="spinnerContainer">
                    <span class="spinner <?php echo $spinnercontent[0]?>"></span>
                    <div class="centerText animate__animated animate__pulse animate__infinite"><?php echo $spinnercontent[1]?></div>
                    <span class="topText"><?php echo $spinnercontent[2]?></span>
                    <span class="bottomText"><?php echo $spinnercontent[3]?></span>
                </div>
            </div>
        </div>
    </div>
</div>
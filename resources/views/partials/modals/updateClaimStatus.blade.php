<div class="modalContent dark modalBlock" id="updateClaimStatus" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/stop.png">reopen claim</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                    <div id="litigationDivContainer">
                        <div id="litigationDiv1" class="litigationDiv">
                            <p class="center">
                                <b>Are you sure you want to reopen this claim?</b><br>
                            </p>
                            <div class="buttonArray">
                                <button class="red" id="editClaimStatusReopen"><div class="icon inline icon-ban"></div> Reopen Claim</button>
                                <button onclick="modal.close()">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div class="modalContent modalBlock dark" id="show_team_templates" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/team-bw.png">select template</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
            <div id="templateList" class="buttonCloud">
                <button class="selectTeamTemplate">default team -2/10</button>
                <button class="selectTeamTemplate">front house team -5/10</button>
                <button class="selectTeamTemplate">warehouse team -3/10</button>
                <button class="selectTeamTemplate">office team -2/10</button>
                <button class="selectTeamTemplate">secondary team -2/10</button>
                <button class="selectTeamTemplate">team6 -4/10</button>
                <button class="selectTeamTemplate">team7 -2/10</button>
                <button class="selectTeamTemplate">team8 -5/10</button>
                <button class="selectTeamTemplate">team9 -4/10</button>
                <button class="selectTeamTemplate">team10 -6/10</button>
                <button class="selectTeamTemplate">team11 -3/10</button>
            </div>       
        </section>  
    </div>
</div>
<div class="modalContent modalBlock dark" id="addFinalCostModal" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/cash.png">final cost</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
			<section class="formBlock dark">
				<div class="modalDescription">log the final cost of this injury.</div>
                <div class="divCenter" id="finalCostText">Final <span class="FG_green">TOTAL</span>: ${{$injury->getPaidTotal()}} | Final <span class="FG_red">RESERVE</span>: ${{$injury->getReserveTotal()}}</div>
                <div class="divCenter" id="finalCostTotalText">Final <span class="FG_green">TOTAL</span> + <span class="FG_red">RESERVE</span>: ${{($injury->getPaidTotal()) + ($injury->getReserveTotal())}}</div> 
                    <div class="formGrid">
                        <div class="formInput">
                            <label for="updateTeamName">final cost</label>
                            <div class="inputIcon user"><input id="finalInjuryCost" name="finalInjuryCost" value=" ${{($injury->getPaidTotal()) + ($injury->getReserveTotal())}}"></div>
                        </div>
                    </div>			
					<div class="buttonArray">
						<button class="green centered" id="submitFinalCost"><div class="icon icon-retweet"></div> submit</button>
					</div>            
			</section>                 
		</section>  
	</div>
</div>
<div class="modalContent modalBlock dark" id="welcomeModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/zenjuries.png">welcome!</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <section class="formBlock dark">
                    <div class="modalDescription">Welcome to zenjuries 5.0.  The first thing you will notice is the update in the user interface.  We took all of your comments and suggestions and made everything as simple as possible, and easy to navigate.<br><br>
                The functions you know are all still in place, along with a few improvements.  Please be patient with us as we migrate over to this new platform, and if you have any questions or need help please email <a href="mailto:mm@zenjuries.com?Subject=Zenjuries%205.0%20Landing%20Help">McCrae Moorhouse</a> for assistance.</div> 
                    <div class="buttonArray">
                        <button class="cyan centered" onclick="modal.close()"><div class="icon icon-retweet"></div> continue</button>
                    </div>                    
                </section>
            </section>
        </div>
    </div>
</div>
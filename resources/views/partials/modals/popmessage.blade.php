<div class="modalContent modalBlock dark" id="popmessageModal" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/<?php echo $popmessage[0]?>.png"><?php echo $popmessage[1]?></span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
			<section class="formBlock dark">
				<div class="modalDescription" id="popModalMessage"><?php echo $popmessage[2]?></div> 			
                <div class="buttonArray">
                    <button class="<?php echo $popmessage[3]?> centered" id="<?php echo $popmessage[4]?>"><div class="icon icon-<?php echo $popmessage[5]?>"></div> <?php echo $popmessage[6]?></button>
                </div>            
			</section>                 
		</section>  
	</div>
</div>
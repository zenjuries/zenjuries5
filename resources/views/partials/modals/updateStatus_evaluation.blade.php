<div class="modalContent dark modalBlock" id="updateStatusModal_evaluation" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/time-crit.png">update evaluation</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                    <div id="update_status_evalDiv">
                        <div class="buttonCloud">
                            <button name="blah" id="firstaidButton" class="firstaidButton evalButton" data-status="Mild"><div class="icon inline icon-ambulance"></div> First aid</button>
                            <button name="blah" id="moderateButton" class="moderateButton evalButton" data-status="Moderate"><div class="icon inline icon-clinic"></div> Moderate injury</button>
                            <button name="blah" id="severeButton" class="severeButton evalButton" data-status="Severe"><div class="icon inline icon-stethoscope"></div> Severe injury</button>
                        </div>
                        <p><b>Always set the date the employee's status changed, and add a note if helpful.</b> Setting this accurately will help provide you with data about how quickly your employee's return to work.</p>
                        <section class="formBlock dark">
                            <div class="formGrid">
                                <!--<div id="injuryDate"></div>-->
                                <div class="formInput">
                                <label for="status-dateEval">date of update</label>
                                <input type="date" id="status-dateEval" name="status-dateEval"
                                    pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}">
                                </div>
                            </div>
                            <textarea id="evalNote" style="width:100%;" placeholder="add a note"></textarea><br><br>
                        </section>
                        <div class="buttonArray" >
                                <button id="fillInBtnEval" class="red centered">select a status and set a date</button>
                                <button  id="submitStatusUpdateEval" class="cyan centered" required style="display:none;"><div class="icon inline icon-check-circle"></div> set updated status</button>
                        </div>
                    </div>                    
                
                </div>
            </section>
        </div>
    </div>
</div>
<script>
    //submit the update
    $('#submitStatusUpdateEval').on('click', function(){
        var date = $('#status-dateEval').val();
        var status = $('.evalButton.selected').data('status');
        var note = $('#evalNote').val();

        $.ajax({
            type: 'POST',
            url: '<?php echo route("updateSeverity"); ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                injury_id: {{ $injury->id }},
                severity: status,
                date: date,
                note: note,
            },
            success: function(data){
                $('#evalContainer').removeClass('minor');
                $('#evalContainer').removeClass('moderate');
                $('#evalContainer').removeClass('severe');
                status = status.toLowerCase();
                if(status === "mild"){
                    status = "minor";
                }
                $('#evalContainer').addClass(status);
                getRecentPosts();
                getTreePosts();
                modal.close();

            },
            error: function(data){
                //add error
                modal.close();
            }
        })

    });

    //sets selected class on eval buttons
    $('.evalButton').on('click', function(){
        $('.evalButton').removeClass('selected');
        $(this).addClass('selected');
    });

    //checks to see if a severity and date has been set
    $('#update_status_evalDiv').on('click', function(){
        console.log('running');
        if($('.evalButton').hasClass('selected')){
            console.log('button has class');
        }else{
            console.log('button does not have class');
        }




        if($('#status-dateEval').val()){
            console.log('date has value');
        }else{
            console.log($('#status-dateEval').val());
            console.log('date does not have value');
        }
        if($('.evalButton').hasClass('selected') && $('#status-dateEval').val()){
            $('#fillInBtnEval').hide();
            $('#submitStatusUpdateEval').show();
        }else{
            $('#fillInBtnEval').show();
            $('#submitStatusUpdateEval').hide();
        }
    });
</script>


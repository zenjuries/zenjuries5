<div class="modalContent modalBlock dark" id="injuryAlertConfirmModal" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/alert.png">Injury Alert</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
			<section class="formBlock dark">
				<div class="modalDescription">Confirm Injury Alert has been delivered to your insurance carrier.</div> 
					<div class="buttonArray">
                        <button class="red centered" id="cancelTheInjuryAlert"><div class="icon icon-retweet"></div>cancel</button>
						<button class="green centered" id="submitTheInjuryAlert"><div class="icon icon-retweet"></div>confirm</button>
					</div>            
			</section>                 
		</section>  
	</div>
</div>
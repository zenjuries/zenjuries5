<!-- <script src="https://js.stripe.com/v3/"></script> -->
<?php
    use Carbon\Carbon;
    
    $company = Auth::user()->getCompany();
    
    $subscriptionStripe = ($company->subscribed('ZAgent_Monthly_Fee')) ? $company->subscription('ZAgent_Monthly_Fee')->asStripeSubscription() : null;
    
    $endDate = (!is_null($subscriptionStripe)) ? Carbon::parse($subscriptionStripe->current_period_end) : Carbon::now();
    $currentDate = Carbon::now();
    $subscriptionPeriodDaysLeft = $endDate->diffInDays($currentDate);

?>
<div id="showCancelSubscription" class="modalContent modalBlock dark" style="display:none">

    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/insurance.png">Subscription Cancelation</span></div>


    <div class="modalBody" style="min-width:390px;min-height:300px;">
        <section class="sectionPanel dark">
            <section class="formBlock dark">
                <br>
                <div class="center" style="padding-top:10px;">
                <!-- countdown classes:  attention, alert, critical -->
                    <div class="countdownContainer"><div class="countdown attention"><span id="daysLeft" class="countdownValue">{{$subscriptionPeriodDaysLeft}}</span><span class="countdownLabel">days</span><span class="countdownDesc">remaining</span></div></div>
                </div>
                <div class="modalDescription">Are you sure you want to cancel your subscription to Zenjuries. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus venenatis ante ut neque ornare, nec ultrices felis fringilla. </div>
                <br>
                <div id="policyNumberNotUploaded">
                    
                    
                    <br>
                    <div class="buttonArray">
                        <button class="red centered" id="cancelSubscription">Cancel Subscription</button>
                        <button class="green centered" id="stayBtn" onclick="modal.close()">Stick with Zenjuries</button>
                    </div>                    
                </div>
            </section>                 
        </section>  
    </div>
</div>

<script>


        $('#cancelSubscription').on('click', function(){
            $.ajax({
                type: 'POST',
                url: '{{ route("cancelSubscription") }}',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    companyId: <?php echo $company->id; ?>
        
                },
                success: function(){
                    // closeModals();
                    modal.close();
                    showAlert("Subscription Canceled!", "confirm", 5);
                },
                error: function(){
                    showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
                }
        
            });
        });


</script>

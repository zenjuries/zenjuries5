<div class="modalContent modalBlock dark" id="communicationModal" style="display: none;">
<div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/email.png">communication settings</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <section class="formBlock dark">
                    <div class="modalDescription">enable weekly updates</div>                          
                    <div class="formGrid short">
                        <div class="formInput"> 
                            <input class="green" id="weeklyUpdateEnabled" name="weeklyUpdate" type="radio"  value="1" @if($user->gets_weekly_summary) checked @endif/><label for="weeklyUpdateEnabled">enable</label>

                            <input class="red" id="weeklyUpdateDisabled" name="weeklyUpdate" type="radio" value="0" @if(!$user->gets_weekly_summary) checked @endif/><label for="weeklyUpdateDisabled">disable</label>
                        </div> 
                    </div>
                    <div class="modalDescription">enable priority 2 emails (major updates on claims and account)</div> 
                    <div class="formGrid short">   
                        <div class="formInput">
                            <input class="green" id="priority2SettingEnabled" type="radio" name="priority2Email" value="1" @if($user->gets_priority_2_emails) checked @endif/><label for="priority2SettingEnabled">enable</label>
                            
                            <input class="red" id="priority2SettingDisabled" type="radio" name="priority2Email" value="0" @if(!$user->gets_priority_2_emails) checked @endif/><label for="priority2SettingDisabled">disable</label>
                        </div>
                    </div>
                    <div class="modalDescription">enable priority 3 emails  (minor updates on claims and account)</div> 
                    <div class="formGrid short">  
                        <div class="formInput">
                            <input class="green" id="priority3SettingEnabled" type="radio" name="proprity3Email" value="1" @if($user->gets_priority_3_emails) checked @endif/><label for="priority3SettingEnabled">enable</label>

                            
                            <input class="red" id="priority3SettingDisabled" type="radio" name="proprity3Email" value="0" @if(!$user->gets_priority_3_emails) checked @endif/><label for="priority3SettingDisabled">disable</label>
                        </div>
                    </div>
                    <div class="buttonArray">
						<button class="cyan centered" id="submitCommunicationChange"><div class="icon icon-retweet"></div> update</button>
					</div>  
                </section>                  
            </section>           
        </div>
    </div> 
</div>

<script>
$('#submitCommunicationChange').on('click', function(){
    var weeklyUpdate = parseInt($("input[name='weeklyUpdate']:checked").val());
    var priority2Email = parseInt($("input[name='priority2Email']:checked").val());
    var priority3Email = parseInt($("input[name='proprity3Email']:checked").val());
    var valid = true;
    
    if(weeklyUpdate === ""){
        valid = false;
        $('#weeklyUpdateEnabled').parent().find('.inputError').addClass('show');
    }
    if(priority2Email === ""){
        valid = false;
        $('#priority2SettingEnabled').parent().find('.inputError').addClass('show');
    }
    if(priority3Email === ""){
        valid = false;
        $('#priority3SettingEnabled').parent().find('.inputError').addClass('show');
    }

    if(valid === true){
        $.ajax({
            type: 'POST',
            url: '{{ route("updateCommunicationSettings") }}',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                user_id: {{ $user->id }},
                weeklyUpdate: weeklyUpdate,
                priority2Email: priority2Email,
                priority3Email: priority3Email
            },
            success: function (data){
                console.log(data);
                if(weeklyUpdate === 1){
                    weeklyUpdate = "enabled";
                }else{
                    weeklyUpdate = "disabled";
                }
                if(priority2Email === 1){
                    priority2Email = "enabled";
                }else{
                    priority2Email = "disabled";
                }
                if(priority3Email === 1){
                    priority3Email = "enabled";
                }else{
                    priority3Email = "disabled";
                }
                //communication page section
                $('#currentWeeklySummary').html(weeklyUpdate);
                $('#currentPriority2Emails').html(priority2Email);
                $('#currentPriority3Emails').html(priority3Email);
                $('#alertSetting').text(data);
                $('#alertSettingMobile').text(data);


                //change navBar radioButtons
                var weeklyUpdateVar = document.getElementById("weeklyUpdateEnabledNav");
                var weeklyUpdateDisabledVar = document.getElementById("weeklyUpdateDisabledNav");
                if(weeklyUpdate == "Enabled"){
                    weeklyUpdateVar.checked = true
                    weeklyUpdateDisabledVar.checked = false
                }
                else{
                    weeklyUpdateDisabledVar.checked = true
                    weeklyUpdateVar.checked = false
                }

                var priority2SettingEnabledVar = document.getElementById("priority2SettingEnabledNav");
                var priority2SettingDisabledVar = document.getElementById("priority2SettingDisabledNav");
                if(priority2Email == "Enabled"){                    
                    priority2SettingEnabledVar.checked = true
                    priority2SettingDisabledVar.checked = false
                }
                else{
                    priority2SettingDisabledVar.checked = true
                    priority2SettingEnabledVar.checked = false
                }

                var priority3SettingEnabledVar = document.getElementById("priority3SettingEnabledNav");
                var priority3SettingDisabledVar = document.getElementById("priority3SettingDisabledNav");
                if(priority3Email == "Enabled"){
                    priority3SettingEnabledVar.checked = true
                    priority3SettingDisabledVar.checked = false
                }
                else{
                    priority3SettingDisabledVar.checked = true
                    priority3SettingEnabledVar.checked = false
                }

                modal.close();
                showAlert("Communication Settings Updated!", "confirm", 5);
            },
            error: function(data){
                console.log(data);
                var errors = data.responseJSON;
                console.log(errors);
                showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
            }
        })
    }
});
</script>    
<div class="modalContent modalBlock dark" id="nameTeamModal" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/team-bright.png">name team</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
			<section class="formBlock dark">
				<div class="modalDescription">name your team.</div> 
                    <div class="formGrid">
                        <div class="formInput">
                            <label for="teamName">new name</label>
                            <div class="inputIcon user"><input id="teamName" name="teamName"></div>
                        </div>
                    </div>			
					<div class="buttonArray">
						<button class="cyan centered" id="submitNewTeamName"><div class="icon icon-retweet"></div> set team name</button>
					</div>            
			</section>                 
		</section>  
	</div>
</div>
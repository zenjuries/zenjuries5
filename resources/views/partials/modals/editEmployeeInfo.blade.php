<div class="modalContent modalBlock dark" id="employeeInfoUpdateModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/user.png">update info</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <section class="formBlock dark">
                    <br>
					<div class="formGrid close">
                        <div class="formInput">
                            <label for="employeeEmailInput">employee name</label>
                            <div class="inputIcon user"><input id="employeeNameInput" type="email" name="employeeNameInput" value="{{$injured_employee->name}}"></div>
                        </div>
                        <div class="formInput">
                            <label for="employeeEmailInput">Employee Email</label>
                            <div class="inputIcon mail"><input id="employeeEmailInput" type="email" name="employeeEmailInput" value="{{$injured_employee->email}}"></div>
                            <span class="inputError"></span>
                        </div>
                        <div class="formInput">
                            <label for="employeePhoneInput">Employee Phone</label>
                            <div class="inputIcon phone"><input id="employeePhoneInput" class="phone_us" type="tel" name="employeePhoneInput" value="{{$injured_employee->phone}}"></div>
                            <span class="inputError"></span>
                        </div>
                    </div>
                    <div class="formGrid short">
                        <div class="formInput wide">
                            <label for="employeeSummaryInput">injury summary</label>
                            <div class="inputIcon user"><textarea id="textarea" class="modalTextarea">{{$injury->notes}}</textarea></div>
                        </div>
                    </div>
                    <hr>
                    <div class="formGrid close">
                        <div class="formInput">
                            <label for="adjusterNameInput">Adjuster Name</label>
                            <div class="inputIcon user"><input id="adjusterNameInput2" name="adjusterNameInput" value="{{$injury->adjuster_name}}"></div>
                            <span class="inputError">required</span>
                        </div>
                        <div class="formInput">
                            <label for="adjusterEmailInput">Adjuster Email</label>
                            <div class="inputIcon mail"><input id="adjusterEmailInput2" name="adjusterEmailInput" value="{{$injury->adjuster_email}}"></div>
                            <span class="inputError"></span>
                        </div>
                        <div class="formInput">
                            <label for="adjusterPhoneInput">Adjuster Phone</label>
                            <div class="inputIcon phone"><input id="adjusterPhoneInput2" class="phone_us" type="tel" name="adjusterPhoneInput" value="{{$injury->adjuster_phone}}"></div>
                            <span class="inputError"></span>
                        </div>
                    </div>                    
                    <div class="formGrid close">
                        <div class="formInput">
                            <label for="updateClaimNumberInput">Claim Number</label>
                            <div class="inputIcon number"><input id="updateClaimNumberInput" name="updateClaimNumberInput" value="{{$injury->claim_number}}"></div>
                        </div>
                        <div class="formInput">
                            <label for="updateConfirmClaimNumberInput">Confirm Claim Number</label>
                            <div class="inputIcon number"><input id="updateConfirmClaimNumberInput" name="updateConfirmClaimNumberInput" value="{{$injury->claim_number}}"></div>
                        </div>
                    </div>
                </section>

                <div class="buttonArray">
                    <button class="cyan centered" id="submitEmployeeEmail"><div class="icon icon-user"></div> update info</button>
                </div>

            </section>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#updateClaimNumberInput').on('input', function(){
            checkUpdateClaimMatch();
        })
        $('#updateConfirmClaimNumberInput').on('input', function(){
            checkUpdateClaimMatch();
        })
    });
    function checkUpdateClaimMatch(){
        var claimNumber = document.getElementById("updateClaimNumberInput");
        var confirmClaimNumber = document.getElementById("updateConfirmClaimNumberInput");
        if(claimNumber.value == "" && confirmClaimNumber.value == ""){
            claimNumber.className = '';
            confirmClaimNumber.className = '';
        }
        else if(claimNumber.value == confirmClaimNumber.value){
            claimNumber.className = 'match';
            confirmClaimNumber.className = 'match';
        }
        else{
            claimNumber.className = 'nomatch';
            confirmClaimNumber.className = 'nomatch';
        }
    }
</script>
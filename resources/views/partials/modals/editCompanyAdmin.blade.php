<div class="modalContent modalBlock dark" id="editCompanyAdminModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/zenisphere.png">company admin</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
                <section class="formBlock dark">
                    <div class="modalDescription">The current admin is <b>{{$owner_name}}.</b><br>You can assign a new admin by choosing them from the dropdown below.</div> 
					<div class="formGrid">
                        <div class="formInput">    
                            <div class="inputIcon user">
                                <select class="form-control" id="ownerSelector" style="min-width:200px;">
                                @foreach($teamUsers as $teamUser)
                                <?php $this_user = DB::table('users')->where('id', $teamUser->user_id)->first();
                                    if($this_user->uses_squads == 1 && is_null($this_user->deleted_at)){
                                        echo "<option value=" . $this_user->id . ">" . $this_user->name . "</option>";
                                    } ?>
                                @endforeach
                                </select>
                            </div>
                            <span class="inputError">error</span>
                        </div>
                    </div>
                    <div class="buttonArray">
                        <button class="cyan centered" id="changeAdmin"><div class="icon icon-check-circle"></div> change admin</button>
                    </div>                   
                </section>
            </section>
        </div>
    </div>
</div>

<script>
 $('#changeAdmin').on('click', function(){
    var oldOwnerId = '<?php echo Auth::user()->id; ?>';
    var newOwnerId = $('#ownerSelector').val();
    var teamId = '<?php echo $teamID; ?>';
    var newOwnerName = $("#ownerSelector option:selected").text();
    changeAdmin(oldOwnerId, newOwnerId, teamId);  
});  

function changeAdmin(oldOwnerId, newOwnerId, teamId){
    var _token = '<?php echo csrf_token(); ?>';
    $.ajax({
        type: 'POST',
        url: '<?php echo route('changeCompanyOwner') ?>',
        data: {_token: _token, oldOwnerId: oldOwnerId, newOwnerId: newOwnerId, teamId: teamId},

        success: function (data) {
                modal.close();
                showAlert("Admin Changed!", "confirm", 5);
            },
            error: function (data) {
                console.log(data);
                var errors = data.responseJSON;
                console.log(errors);
                showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
            }
    });
}
</script>
<div id="show_company" class="zModal" style="display:none">
    <div class="modalContent dark">
        <div class="modalHeader"><span class="modalTitle"><div class="icon inline icon-users"></div> [company name]</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="sectionPanel dark">
                <section class="formBlock dark">
                <p>Submitting this form will add a user to your company. If they don't have a zenjuries account, they'll get an email containing a sign-up link. If they do have an account already (for example, if they're a doctor who works with multiple companies) they'll appear in your roster right away!</p> 
                    <div class="formGrid">  
                        <div class="formInput">
                            <!-- input -->
                            <label for="email">user name</label>
                            <div class="inputIcon user"><input id="newuser" type="text" /></div>
                            <span class="inputError show">Name Required</span>
                        </div>
                        <div class="formInput">
                            <!-- input -->
                            <label for="phone">user email</label>
                            <div class="inputIcon mail"><input id="email" type="text" /></div>
                            <span class="inputError show">Email Required</span>
                        </div>
                    </div> 
                    <div class="buttonArray">
                        <button class="cyan centered">add new user</button>
                    </div>                                       
                </section>                 
            </section>  
        </div>
    </div> 
</div>
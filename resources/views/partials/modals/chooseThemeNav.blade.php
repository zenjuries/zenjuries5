<?php 
    $brandID = env('BRAND_NAME', "default");
    $path = public_path() . '/branding/' .$brandID . '/themes/';
    $themes = scandir($path);
    $user_theme = 'theme' . Auth::user()->theme_id;
?>
<div class="modalContent modalBlock dark" id="navThemeModal" style="display: none;">
@include('partials.modals.themeModalPartial') 
</div>


<script>
 $('.themeSelectorBg').on('click', function(){
        var theme = $(this).data('theme');
        theme = theme.replace('theme','');
        console.log(theme);
        changeTheme(theme);
    });
    
    function changeTheme(theme){
        $.ajax({
            type: 'POST',
            url: '/updateUserTheme',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                user_id: {{ $user->id }},
                theme: theme
            },
            success: function(){
                window.location.href = "/zenboard";
                //showAlert("Your settings have been updated!", "confirm", 5);
                //redirect back to settings page
            },
            error: function(){
                showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
            }
        })
    }    
</script>
<div class="modalContent modalBlock dark" id="zengardenOptOutModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/zengarden.png">invite to zengarden</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <section class="formBlock dark">
                    <div class="modalDescription">Opt out of the zengarden. If you choose this you will not be able to invite this employee to the zengarden</div> 
                    <div class="buttonArray">
                        <button class="red centered" id="zengardenOptOutSubmit"><div class="icon icon-bamboo2"></div> opt out</button>
                    </div>                    
                </section>
            </section>
        </div>
    </div>
</div>
<script>
    $('#zengardenOptOutSubmit').on('click', function(){
        $.ajax({
            type: 'POST',
            url: '<?php echo route('optOutZengarden') ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                injury_id: {{$injury_id}},
            },
            success: function(data){
                window.location.reload();
            },
            error: function(data){
                // alert('error');
            }
        });
    });
</script>

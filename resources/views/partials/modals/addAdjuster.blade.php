<div class="modalContent modalBlock dark" id="adjusterModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/agent1.png">add adjuster</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <section class="formBlock dark">
                    <div class="modalDescription">add the info for your adjuster.</div> 
					<div class="formGrid">
                        <div class="formInput">
                            <label for="adjusterNameInput">Adjuster Name</label>
                            <div class="inputIcon user"><input id="adjusterNameInput" name="adjusterNameInput" value="{{$injury->adjuster_name}}"></div>
                            <span class="inputError">required</span>
                        </div>
                        <div class="formInput">
                            <label for="adjusterEmailInput">Adjuster Email</label>
                            <div class="inputIcon mail"><input id="adjusterEmailInput" name="adjusterEmailInput" value="{{$injury->adjuster_email}}"></div>
                            <span class="inputError"></span>
                        </div>
                        <div class="formInput">
                            <label for="adjusterPhoneInput">Adjuster Phone</label>
                            <div class="inputIcon phone"><input id="adjusterPhoneInput" class="phone_us" type="tel" name="adjusterPhoneInput" value="{{$injury->adjuster_phone}}"></div>
                            <span class="inputError"></span>
                        </div>
                    </div>
                    <div class="buttonArray">
                        <button class="cyan centered" id="submitAdjusterInfo"><div class="icon icon-retweet"></div> submit</button>
                    </div>                    
                </section>
            </section>
        </div>
    </div>
</div>
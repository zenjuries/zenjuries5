<div class="modalContent dark modalBlock" id="updateStatusModal_resolution" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/greencheck.png">update resolution</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                    <div id="update_status_resolutionDiv">
                        <div class="buttonCloud">
                            <button name="blah" id="maxrecoveryButton" class="maxrecoveryButton resolutionButton" data-status="Maximum Recovery"><div class="icon inline icon-newheart"></div> Maximum Recovery</button>
                            <button name="blah" id="terminatedButton" class="terminatedButton resolutionButton" data-status="Terminated"><div class="icon inline icon-times"></div> Employee was Terminated</button>
                            <button name="blah" id="resignedButton" class="resignedButton resolutionButton" data-status="Resigned"><div class="icon inline icon-ban"></div> Resigned</button>
                            <button name="blah" id="deathButton" class="deathButton resolutionButton" data-status="Death"><div class="icon inline icon-skull"></div> Death</button>
                        </div>
                        <p><b>Always set the date the employee's status changed, and add a note if helpful.</b> Setting this accurately will help provide you with data about how quickly your employee's return to work.</p>
                        <section class="formBlock dark">
                            <div class="formGrid">
                                <!--<div id="injuryDate"></div>-->
                                <div class="formInput">
                                <label for="status-date-resolution">date of update</label>
                                <input type="date" id="status-date-resolution" name="status-date-resolution"
                                    pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}">
                                </div>
                            </div>
                            <textarea id="resolutionNote" style="width:100%;" placeholder="add a note"></textarea><br><br>
                        </section>
                        <div class="buttonArray" >
                                <button id="fillInBtnResolution" class="red centered">select a status and set a date</button>
                                <button  id="submitStatusUpdateResolution" class="cyan centered" required style="display:none;"><div class="icon inline icon-check-circle"></div> set updated status</button>
                        </div>
                    </div>                     
                
                </div>
            </section>
        </div>
    </div>
</div>
<script>
     //submit the update
$('#submitStatusUpdateResolution').on('click', function(){
        var date = $('#status-date-resolution').val();
        var status = $('.resolutionButton.selected').data('status');
        var note = $('#resolutionNote').val();

        $.ajax({
            type: 'POST',
            url: '<?php echo route("updateEmployeeStatus"); ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                injury_id: {{ $injury->id }},
                status: status,
                date: date,
                note: note,
            },
            success: function(data){
                $('#resolutionContainer').removeClass('death');
                $('#resolutionContainer').removeClass('terminated');
                $('#resolutionContainer').removeClass('resigned');
                $('#resolutionContainer').removeClass('maximumrecovery');
                status = status.toLowerCase();
                if(status === "maximum recovery"){
                    $('#resolutionContainer').addClass('maximumrecovery');
                }else{
                    $('#resolutionContainer').addClass(status);
                }
                getRecentPosts();
                getTreePosts();
                modal.close();
            },
            error: function(data){
                modal.close();
            }
        })

    });

    //sets selected class on eval buttons
    $('.resolutionButton').on('click', function(){
        $('.resolutionButton').removeClass('selected');
        $(this).addClass('selected');
    });

    //checks to see if a severity and date has been set
    $('#update_status_resolutionDiv').on('click', function(){
        console.log('running');
        if($('.resolutionButton').hasClass('selected')){
            console.log('button has class');
        }else{
            console.log('button does not have class');
        }




        if($('#status-date-resolution').val()){
            console.log('date has value');
        }else{
            console.log($('#status-date-resolution').val());
            console.log('date does not have value');
        }
        if($('.resolutionButton').hasClass('selected') && $('#status-date-resolution').val()){
            $('#fillInBtnResolution').hide();
            $('#submitStatusUpdateResolution').show();
        }else{
            $('#fillInBtnResolution').show();
            $('#submitStatusUpdateResolution').hide();
        }
    });
</script>

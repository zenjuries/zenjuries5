<div class="modalContent modalBlock dark" id="user_list" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/users.png">all users</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
			<div id="modalItemGrid" class="itemGrid userList">
				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar13.png');background-color:pink;"></div>
						<div class="userFirstName">Poindexter</div>
						<div class="userLastName">Harrington</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar6.png');background-color:blue;"></div>
						<div class="userFirstName">Rose</div>
						<div class="userLastName">Mellingsby</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar3.png');background-color:cyan;"></div>
						<div class="userFirstName">Patrick</div>
						<div class="userLastName">Smeenley</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar12.png');background-color:black;"></div>
						<div class="userFirstName">John</div>
						<div class="userLastName">Zwilinger</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar2.png');background-color:green;"></div>
						<div class="userFirstName">Karen</div>
						<div class="userLastName">Blasking</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar7.png');background-color:cyan;"></div>
						<div class="userFirstName">Leon</div>
						<div class="userLastName">Harmining</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar16.png');background-color:red;"></div>
						<div class="userFirstName">Sarah</div>
						<div class="userLastName">Limmonly</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar8.png');background-color:orange;"></div>
						<div class="userFirstName">Sho</div>
						<div class="userLastName">Kashoki</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar20.png');background-color:grey;"></div>
						<div class="userFirstName">Merlin</div>
						<div class="userLastName">Jasminbly</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar4.png');background-color:blue;"></div>
						<div class="userFirstName">Madeline</div>
						<div class="userLastName">Shillingsworth</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar23.png');background-color:yellow;"></div>
						<div class="userFirstName">Mary</div>
						<div class="userLastName">Jones</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar19.png');background-color:black;"></div>
						<div class="userFirstName">Herman</div>
						<div class="userLastName">Dessinger</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>
				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar13.png');background-color:pink;"></div>
						<div class="userFirstName">PoindexterVonBurgundy</div>
						<div class="userLastName">Harringtonsmithsonwillamstein</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar6.png');background-color:blue;"></div>
						<div class="userFirstName">Rose</div>
						<div class="userLastName">Mellingsby</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar3.png');background-color:cyan;"></div>
						<div class="userFirstName">Patrick</div>
						<div class="userLastName">Smeenley</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar12.png');background-color:black;"></div>
						<div class="userFirstName">John</div>
						<div class="userLastName">Zwilinger</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar2.png');background-color:green;"></div>
						<div class="userFirstName">Karen</div>
						<div class="userLastName">Blasking</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar7.png');background-color:cyan;"></div>
						<div class="userFirstName">Leon</div>
						<div class="userLastName">Harmining</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar16.png');background-color:red;"></div>
						<div class="userFirstName">Sarah</div>
						<div class="userLastName">Limmonly</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar8.png');background-color:orange;"></div>
						<div class="userFirstName">Sho</div>
						<div class="userLastName">Kashoki</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar20.png');background-color:grey;"></div>
						<div class="userFirstName">Merlin</div>
						<div class="userLastName">Jasminbly</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar4.png');background-color:blue;"></div>
						<div class="userFirstName">Madeline</div>
						<div class="userLastName">Shillingsworth</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar23.png');background-color:yellow;"></div>
						<div class="userFirstName">Mary</div>
						<div class="userLastName">Jones</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>

				<div class="itemGrid__item userContainer">
					<div class="userListUser showUserCard">
						<div class="userAvatar" style="background-image:url('/images/avatar_icons/avatar19.png');background-color:black;"></div>
						<div class="userFirstName">Herman</div>
						<div class="userLastName">Dessinger</div>
					</div>
					<a class="addToTeam" href="#"></a>
				</div>                    
			</div>
		</section>  
	</div>
</div>
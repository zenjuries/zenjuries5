<div class="modalContent modalBlock dark" id="add_zagent" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/agent1.png">add zagent</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark">             
            <section class="formBlock dark">
                <div class="modalDescription"><span style="font-size:1.4rem;font-weight:300;display:block;margin-bottom:12px;">Building a winning team means fielding the best players.</span> <span style="font-size:1rem;font-weight:300;display:block;text-align:justify;">Your agency is ripe with great players that you may want to add to your team building roster. <span style="font-size:1rem;font-weight:900;color:#ff3333;">Caution: Invitations sent from here will grant access to other agency clients and information. For agency personnel ONLY.</span></span></div> 
                <div class="formGrid">  
                    <div class="formInput">
                        <!-- input -->
                        <label for="email">zagent name</label>
                        <div class="inputIcon user"><input id="znewuser" type="text" /></div>
                        <!--<span class="inputError show">Name Required</span>-->
                    </div>
                    <div class="formInput">
                        <!-- input -->
                        <label for="phone">zagent email</label>
                        <div class="inputIcon mail"><input id="zemail" type="text" /></div>
                        <!--<span class="inputError show">Email Required</span>-->
                    </div>
                </div> 
                <div class="buttonArray">
                    <button id="inviteNewZagent" class="cyan centered"><div class="icon icon-user"></div> add new zagent</button>
                </div> 
                <div class="divCenter FG__grey"><i>With that, go build your winning team!</i></div>                                     
            </section>                 
        </section>  
    </div> 
</div>
<script>
    $('#inviteNewZagent').on('click', function(){
        var company_id = $(this).data('companyid');

        addZagent(company_id);
    });

    function addZagent(company_id){
        var name = document.getElementById("znewuser").value;
        var email = document.getElementById("zemail").value;
        var _token = '<?php echo csrf_token(); ?>';
        console.log(name);
        console.log(email);
        console.log(company_id);
        $.ajax({
            type: 'POST',
            url: '<?php echo route('myZagentInviteZagent') ?>',
            data: {
                company_id: company_id,
                name: name,
                email: email,
                _token: _token
            },
            success: function (data) {
                showAlert("We've sent them an invitation! Once they finish creating their account you'll be able to add them to your team!", "confirm", 5);
                // loadRoster();
                modal.close();
                document.getElementById('email').value = "";
                document.getElementById('newuser').value = "";
                $('.userTypeButton').removeClass('selected');
                $('#addUserModal').removeData('bs.modal');
            },
            error: function(data){
                var errorMsg = JSON.parse(data.responseText);
                if(typeof errorMsg['error'] == "undefined"){
                    errorMsg['error'] = "";
                }
                if(errorMsg['email'] !== undefined){
                    errorMsg['error']+= (errorMsg['email'] + "<br>");
                }
                if(errorMsg['type'] !== undefined){
                    errorMsg['error']+= (errorMsg['type'] + "<br>");
                }
                console.log(errorMsg['error']);
                
                //document.getElementById('addUserFormErrors').innerHTML = errorMsg['error'];
                //document.getElementById('addUserFormErrors').style.display = "";
            }
        });
    }
</script>
<div class="modalContent modalBlock dark" id="errorModal" style="display: none">
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
			<section class="formBlock dark">
                <div class="divCenter animate__animated animate__bounce animate__faster"><img src="/images/icons/<?php echo $errormessage[0]?>.png" style="width:100%;max-width:160px;"></div>
				<div class="center" id="errorMessage" style="padding-bottom:12px;margin-top:-10px;"><?php echo $errormessage[1]?></div> 
                <div class="errorCode center" id="errorCode">error code: <span class="error"><?php echo $errormessage[2]?></span></div>
                <br> 			
                <div class="buttonArray">
                    <button class="<?php echo $errormessage[3]?> centered" id="<?php echo $errormessage[4]?>"><div class="icon icon-<?php echo $errormessage[5]?>"></div> <?php echo $errormessage[6]?></button>
                </div>            
			</section>                 
		</section>  
	</div>
</div>
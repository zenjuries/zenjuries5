<div class="modalContent modalBlock dark" id="testingRequest" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/dashboard.png">Testing Request</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
            <!-- any content can be added in this area--> 

                <!-- below is a sample of form layout -->
                <section class="formBlock dark">
                    <div class="modalDescription">request testing for this page/function.</div> 
					<div class="formGrid">
                        <div class="formInput">
                        <label for="state">type of issue</label>
                            <div class="inputIcon check">
                                <select id="issue_type" name="state" style="width:212px;">
                                <option value="---">--select--</option>
                                <option value="visual">visual or UI</option>
                                <option value="text">text issue</option>
                                <option value="formatting">broken formatting</option>
                                <option value="better_format">formatting suggestion</option>
                                <option value="data">wrong data</option>
                                <option value="functionality">broken functionality</option>
                                <option value="wrong_code">wrong functionality</option>
                                <option value="better_idea">function suggestion</option>
                                <option value="minor_error">minor error</option>
                                <option value="critical_error">critical error</option>
                                <option value="security">security issue</option>
                                </select>
                            </div>
                        </div>
                        <div class="formInput">
                            <label for="adjusterEmailInput">Adjuster Email</label>
                            <div class="inputIcon mail"><input id="adjusterEmailInput" name="adjusterEmailInput" value=""></div>
                            <span class="inputError"></span>
                        </div>
                    </div>
                    <div class="formGrid" style="border:1px solid blue;">
                        <div class="formInput wide" style="border:1px solid green;">
                            <label for="adjusterPhoneInput">Description</label>
                            <div style="width:100%;border:1px solid red;"><textarea style="width:100%;" id="testDescription"></textarea></div>
                            <span class="inputError"></span>
                        </div>
                    </div>

                    <!-- make sure to always use the buttonArray for centered buttons and multi buttons-->
                    <div class="buttonArray">
                        <button class="grey centered" id="submitAdjusterInfo"><div class="icon icon-retweet"></div> input</button>
                    </div>                   
                </section>
                <!-- end of sample form -->

            <!--end of any added content -->
            </section>
        </div>
    </div>
</div>

<!-- add any js code you have specific to this modal below -->

<script>
/* any JS required for this modal */
</script>
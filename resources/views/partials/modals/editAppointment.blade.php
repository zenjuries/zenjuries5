<div class="modalContent modalBlock dark" id="editAppointmentModal" style="display: none">
    <div class="modalHeader"><span class="modalTitle" id="setAptTitle" style="display:auto;">edit Appointment</span></div>    
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
			<section class="formBlock dark">
				{{-- <div class="modalDescription">something about this form</div>  --}}
					<!-- your code here-->
					<div class="formGrid">  
						<div class="formInput">
							<!-- input -->
							<label for="location">Location Name</label>
							<div class="location"><input type="text" id="editAppointmentName"/></div>
						</div>
						<div class="formInput">
							<!-- input -->
							<label for="phone">Location Phone</label>
							<div class="inputIcon phone"><input id="editAppointmentPhone" class="phone_us" type="tel" aria-label="Please enter your phone number" placeholder="ex. (111)-111-1111"/></div>
							<span class="inputError">numbers only</span>
						</div>
						<div class="formInput">
							<!-- input -->
							<label for="email">Location Email</label>
							<div class="inputIcon email"><input id="editAppointmentEmail" type="email"/></div>
							<span class="inputError">--</span>
						</div>
                        <section class="formBlock dark">
                            <div class="sectionInfo">Select Date and Time</div>
                            <div class="formGrid">
                                <!--<div id="injuryDate"></div>-->
                                <div class="formInput">
                                <label for="datePickerPlaceholder">date/time</label>
                                <input type="datetime-local" id="editdatetime" name="editdatetime"
                                    pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}"/>
                                </div>
                            </div>
                        </section>
                        <input type="hidden" id="editAppointmentID">
                        <div class="formInput">
							<!-- input -->
							<label for="email">Notes</label>
							<div class="inputIcon userinfo"><input id="editAppointmentNote" type="text"/></div>
							<span class="inputError">--</span>
						</div>
					</div>
                    <div class="buttonArray">
						<button class="green" id="editApptBtnSave"><div class="icon icon-retweet"></div>update appointment</button>
					</div>           					
					<!-- end your code here--> 
			</section>                 
		</section>  
	</div>
</div>
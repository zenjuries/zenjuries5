<div class="modalContent modalBlock dark" id="getLossReportModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/lossreport.png">loss report</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
                <section class="formBlock dark">
                    <div class="modalDescription">view or download your loss reports.</div>
                    <div class="sectionInfo">
                        {{-- <span class="inputError show" id="dateError" style="top:5px;left:0px;">Date Required</span> --}}
                    </div> 
					<div class="formGrid">
                        <div class="formInput">
                            <label for="lossDate">from date</label>
                            <input type="date" id="lossDate" name="lossDate"
                                pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}" onblur="onDataChanged()">
                        </div>
                            <div class="formInput">
                            <label for="lossDateTwo">to date</label>
                            <input type="date" id="lossDateTwo" name="lossDateTwo"
                                pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}" onblur="onDataChanged()">
                        </div>
                    </div>
                    <div class="modalDescription">select filtering options</div>
                    <div class="buttonArray">
                        <button class="lossReportsType centered" data-type="mild">first aid</button>
                        <button class="lossReportsType centered" data-type="moderate">moderate/severe</button>
                        <button class="lossReportsType centered" data-type="both">both</button>
                    </div>
                    <div class="buttonArray">
                        <button class="lossReportsStatus centered" data-status="resolved">resolved</button>
                        <button class="lossReportsStatus centered" data-status="open">open</button>
                        <button class="lossReportsStatus centered" data-status="both">both</button>
                    </div>
                    <hr>
                    <div class="buttonArray">
                        <button class="gold" id="downloadLossReport"><a style="text-decoration:none !important;color:white" href="#"><div class="icon icon-cloud-download"></div> download loss report</a></button>
                    </div>                   
                </section>
            </section>
        </div>
    </div>
</div>

<script>
function getLossReports(){
    //document.getElementById('lossReportsDatesErrors').innerHTML = "";
    var startDate = lossDate.toLocaleString();
    var endDate = lossDateTwo.toLocaleString();
    var type = $('.lossReportsTypeButton').filter('.selected').data('type');
    var status = $('.lossReportsStatusButton').filter('.selected').data('status');
    console.log("type: " + type);
    console.log("status: " + status);
    console.log("start date " + startDate);
    console.log("start date " + endDate);
    
    //window.location = "/lossReportControls/" + startDate + "/" + endDate + "/" + type + "/" + status;
}

$('#downloadLossReport').on('click', function(e){
    var startDate = $('#lossDate').val();
    var endDate = $('#lossDateTwo').val();
    var type = $('.lossReportsType').filter('.selected').data('type');
    var status = $('.lossReportsStatus').filter('.selected').data('status');

    //convert date from 2022/01/01 to 2022-01-01
    startDate = startDate.replace(/\//g,'-');
    endDate = endDate.replace(/\//g,'-');
    var link = document.createElement('a');
    var generatedLink = "renderLossReport/" + startDate + '/' + endDate + '/' + type + '/' + status;
    link.href = generatedLink;
    // alert('clicking link');
    link.click();
});

    


    // console.log("status: " + status);
    // var btn = $('.downloadLossReport').children("a");
    

    // //e.stopPropagation();
    // btn.attr('href', generatedLink);
    // console.log(generatedLink);
    // btn.find('a').trigger('click');
    // //$('a')[0].click();
    // console.log(btn);


// $('#downloadLossReport').on('click', function(e){
//     var startDate = lossDate.toLocaleString();
//     var endDate = lossDateTwo.toLocaleString();
//     var type = $('.lossReportsType').filter('.selected').data('type');
//     var status = $('.lossReportsStatus').filter('.selected').data('status');
//     console.log("type: " + type);
//     console.log("status: " + status);
//     console.log("start date " + startDate);
//     console.log("start date " + endDate);
    
//     console.log("status: " + status);
//     var btn = $('.downloadLossReport').children("a");
//     var generatedLink = 'lossReport/' + startDate + '/' + endDate + '/' + type + '/' + status;
//     //e.stopPropagation();
//     btn.attr('href', generatedLink);
//     btn.trigger('click');

//     //window.location.href = "/lossReportControls" + startDate  + endDate + type + status;
//     //window.location.href = "lossReportControls/" + startDate + "/" + endDate + "/" + type + "/" + status;
//     //location.href='/lossReportControls/startDate/endDate/claimType/claimStatus';
//     window.location = 'lossReport/' + startDate + "/" + endDate + "/" + type + "/" + status; 
// }); 
    //document.getElementById('lossReportsDatesErrors').innerHTML = "";
//     var startDate = lossDate.toLocaleString();
//     var endDate = lossDateTwo.toLocaleString();
//     var type = $('.lossReportsType').filter('.selected').data('type');
//     var status = $('.lossReportsStatus').filter('.selected').data('status');
//     console.log("type: " + type);
//     console.log("status: " + status);
//     console.log("start date " + startDate);
//     console.log("start date " + endDate);
    
//     console.log("status: " + status);
//     var btn = $('#downloadLossReport').children("a");
//     var generatedLink = 'lossReport/' + startDate + '/' + endDate + '/' + claimType + '/' + claimStatus;
//     btn.attr('href', generatedLink);
//     btn.trigger('click');

//     //window.location.href = "/lossReportControls" + startDate  + endDate + type + status;
//     //window.location.href = "lossReportControls/" + startDate + "/" + endDate + "/" + type + "/" + status;
//     //location.href='/lossReportControls/startDate/endDate/claimType/claimStatus';
//     window.location = "lossReport/" + startDate + "/" + endDate + "/" + type + "/" + status;
// }

$('#viewReport').on('click', function(){
    getLossReports();
});

// $('#downloadLossReport').on('click', function(e){ 
//    downloadLossReports();
// });



$('.lossReportsType').on('click', function(){
    console.log("types clicked");
    $('.lossReportsType').removeClass('selected');
    $(this).addClass('selected');
});

$('.lossReportsStatus').on('click', function(){
    console.log("status clicked");
    $('.lossReportsStatus').removeClass('selected');
    $(this).addClass('selected');
});    
</script>
<?php
    $company = Auth::user()->getCompany()
?> 

<div class="modalContent modalBlock dark" id="uploadLogoModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/company-active.png">upload your logo</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <div class="sectionDescription">Make sure your logo is in a square format for best results.  A transparent PNG file looks best!</div> 
                <div class="divCenter"><input type="file" id="imageModalFileInput"></div>
                <div id="croppieDiv" style="width:300px;height:300px;margin:auto"></div>
                <br>
                <div class="buttonArray">
                    <button class="cyan centered" id="imageModalSave"><div class="icon icon-retweet"></div> save logo</button>
                </div>             
            </section>
        </div>
	</div>
</div>

<script src="{{ URL::asset('/js/croppie.js') }}"></script>
<link rel="stylesheet" href="{{URL::asset('/css/croppie.css')}}">

<script>
//PHOTO UPLOAD/CROPPIE
var croppie;

$('#imageModalFileInput').on('change', function(){
    var file = $(this).prop('files')[0];
    console.log(file);
    if(file.type === "image/png" || file.type === "image/jpeg"){
        console.log('in reader');
        var reader = new FileReader();
        reader.onload = function(e){
            $('#croppieDiv').html("");
            croppie = $('#croppieDiv').croppie({
                viewport: {
                    width: 150,
                    height: 150,
                    type: 'square'
                },
                boundary: {
                    width: 150,
                    height: 150
                },
                mouseWheelZoom:false
            });
            croppie.croppie('bind',{
                url: e.target.result,
                points: []
            });
        }
        reader.readAsDataURL(file);
    }else{
        showAlert("invalid filetype", "deny", 5);
    }

});

$('#imageModalSave').on('click', function(){
    console.log('saving');
    croppie.croppie('result', 'canvas', 'original').then(function (img) {
        uploadPhoto(img);
    });
});

//upload a custom photo, remove the selected class from icons
function uploadPhoto(img){
    $.ajax({
        type: 'POST',
        url: '/createCompanyLogo',
        data: {
            _token: '<?php echo csrf_token(); ?>',
            company_id: {{ $company->id }},
            img: img
        },
        success: function(data){
            console.log(data);
            modal.close();
            showAlert("Thanks for your upload!", "confirm", 5);
            var image_url = data + "?" + new Date().getTime();
            $('.profileCompanyColor').css('background-image', 'url(' + img + ')');
            $('#policyholderLogo').css('background-image', 'url(' + img + ')');
            $('#policyholderLogoMobileIcon').css('background-image', 'url(' + img + ')');
            
            //$('.avatarIcon').css('background-image', '').css('background-image', 'url(' + image_url + ')');
            //$('.profileAvatarColor').html('<div class="avatarIcon" style="background-image:url(' + img +')"></div>');
            //$('.iconSelector').find('a.selected').removeClass('selected');
            //$('.avatarIcon').css('background-image', '');
            //$('.avatarIcon').css('background-image', img);

        },
        failure: function(data){
            modal.close();
            showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
            console.log(data);
        }
    });
    
}  
</script>    
<div class="modalContent modalBlock dark" id="addSummaryModal" style="display: none">
<div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/edit.png">add summary</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <section class="formBlock dark"  style="padding:0 40px;">
                    <div class="modalDescription">Add any details you have from the appointment, such as doctor's notes, expected time until maximum medical improvement, etc.</div> 
                            <input type="hidden" id="addSummaryModalAppointmentID">
                            <div class="appointmentTitle center">Appointment on <span>2021-01-01 13:00:00</span></div>
                            <textarea placeholder="summary/notes" style="min-width:300px;max-width:800px;width:100%;height:120px;"></textarea>
  
                    <div class="buttonArray">
                        <button class="cyan centered" id="submitSummaryModal"><div class="icon icon-pencil-square-o"></div> submit summary</button>
                    </div>
                </section>
            </section>
        </div>
    </div>
</div>
<div id="add_media_links" class="zModal" style="display:none">
    <div class="modalContent dark">
        <div class="modalHeader"><span class="modalTitle"><div class="icon inline icon-link"></div> add media links</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                    <section class="formBlock dark">                          
                        <div class="formGrid"> 
                        <div class="formGrid short">
                            <div class="formInput">
                                <input type="radio" name="r1" id="r1">
                                <label for="r1">link</label>
                            </div> 
                            <div class="formInput">
                                <input type="radio" name="r1" id="r2">
                                <label for="r2">photo</label>
                            </div>    
                            <div class="formInput">    
                                <input type="radio" name="r1" id="r3">
                                <label for="r3">video</label>
                            </div>
                            <div class="formInput">    
                                <input type="radio" name="r1" id="r4">
                                <label for="r4">document</label>
                            </div>  
                            <div class="formInput">    
                                <input type="radio" name="r1" id="r5">
                                <label for="r5">collection</label>
                            </div>
                            <div class="formInput">    
                                <input type="radio" name="r1" id="r6">
                                <label for="r6">medical</label>
                            </div>                                                              
                        </div>                                
                            <div class="formInput">
                                <!-- input -->
                                <label for="media_url">URL of media</label>
                                <input id="media_url" />
                                <span class="inputError show">error</span>
                            </div>                                 
                            <div class="formInput">
                                <!-- input -->
                                <label for="media_description">description</label>
                                <textarea id="media_description"></textarea>
                            </div>
                        </div>
                    </section>
                </div> 
                <div class="center"><button class="cyan">add link</button></div>                   
            </section>
        </div>
    </div> 
</div>
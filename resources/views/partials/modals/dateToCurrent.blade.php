<div class="modalContent modalBlock dark" id="dateToCurrentModal" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/date.png">date selector</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
			<section class="formBlock dark">
				<div class="modalDescription">select a starting date</div> 
					<!-- your code here-->
					<div class="formGrid">  
						<div class="formInput">
							<!-- input -->
							<label for="phone">from date</label>
							<input id="fromDate" type="date"/>
						</div>
						<div class="formInput">
							<!-- input -->
							<label for="phone">to date</label>
							<input id="currentDate" type="input" value="today"/>
						</div>
					</div>
					
					<!-- end your code here--> 
					<div class="buttonArray">
						<button class="olive centered" id="submitDateRange"><div class="icon icon-calendar"></div> set date</button>
					</div>            
			</section>                 
		</section>  
	</div>
</div>
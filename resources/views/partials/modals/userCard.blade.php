
<?php
use Carbon\Carbon;
$user = Auth::user();
$theme = $user->zengarden_theme;
$theme = str_replace("theme", "", $theme);
$theme = strtolower($theme);

$brandID = env('BRAND_NAME');
$themeID = 'theme' . $user->theme_id;
?>
<div class="modalContent modalBlock transparent" id="user_card" style="display: none">
	<div class="modalBody" style="min-width:260px;">
<!-- if there is more than one user, display this block to navigate user cards-->
        <div id="displayMultipleUsers" class="buttonArray">
            <button class="centered small" id="prevUserCard"><div class="icon icon-arrow-left"></div> prev</button>
            <span style="display:inline-block;padding:0 12px;"><span id="currentUserCardShowing">1</span> of <span id="numberOfUserCards">2</span> users</span>
            <button class="centered small" id="nextUserCard">next <div class="icon icon-arrow-right"></div></button>
        </div>         
        <br>
<!--user card - add class "stacked" to userCard if more than 1 user-->
        <div id="cardContainer" class="animate__animated" data-role="">
            <div class="flippableCard">
                <div id="userCard" class="userCard">
                    <div class="cardFront">
                        <div id="themeContainer" class="header"><div class="headerBG themeBG" style="background-image:url('/branding/{{ $brandID }}/themes/{{ $themeID }}/dashboard.jpg');"></div></div>
                        <div class="cardZenRating menuWidget">
                                <div href="/zenrating" class="widget">	
                                    <span class="leftImage"><img src="/images/icons/zenrating.png"></span>
                                    <span class="rate lv2">73</span>
                                </div>
                        </div>                 
                        <div class="userCardAvatarContainer"><div class="userCardAvatar" style="background-color:#7777ff;background-image:url('/images/avatar_icons/avatar16.png');"></div></div>
                        
                        <div class="firstname">Madeline</div>
                        <div class="lastname">Smith</div>
                        <a class="flipCardL"></a> <a class="flipCardR"></a> <a class="flipCardX"></a>
                        <div class="cardFrontContent">
                        <hr>    
                        <div class="userPrimaryRole">Primary Role: <div class="displayRole inline role4"><div class="roleIcon"></div><div class="roleTxt FG__green"></div></div></div>
                        <fieldset class="centered" style="display:none;"><legend>Roles</legend>
                        <div class="roleDisplay">
                            <!-- roles can be hidden/exposed with the role ID 1-10 -->
                            <div id="role1" class="setToolTip inline"><div class="icon icon-exec FG__required"></div><span class="toolTip">chief<br>executive</span></div>
                            <div id="role2" class="setToolTip inline"><div class="icon icon-reuser FG__required"></div><span class="toolTip">human<br>resources</span></div>
                            <div id="role3" class="setToolTip inline"><div class="icon icon-zencircle FG__suggested"></div><span class="toolTip">insurance<br>adjuster</span></div>
                            <div id="role4" class="setToolTip inline"><div class="icon icon-injuryreport FG__suggested"></div><span class="toolTip">claim<br>manager</span></div>
                            <div id="role5" class="setToolTip inline"><div class="icon icon-exclamation-triangle FG__suggested"></div><span class="toolTip">safety<br>manager</span></div>
                            <div id="role6" class="setToolTip inline"><div class="icon icon-insurance FG__suggested"></div><span class="toolTip">work comp<br>agent</span></div>
                            <div id="role7" class="setToolTip inline"><div class="icon icon-hardhat FG__suggested"></div><span class="toolTip">employee<br>supervisor</span></div>
                            <div id="role8" class="setToolTip inline"><div class="icon icon-support FG__suggested"></div><span class="toolTip">service<br>agent</span></div>
                            <div id="role9" class="setToolTip inline"><div class="icon icon-doctor FG__optional"></div><span class="toolTip">medical<br>support</span></div>
                            <div id="role10" class="setToolTip inline"><div class="icon icon-adduser FG__optional"></div><span class="toolTip">other<br>user</span></div>                        
                        </div>    
                        </fieldset>
                        <fieldset class="centered"><legend>Teams</legend>
                        <!-- active teams is the current teams user is on.  All teams is the number of injuries the user was on a team with-->
                        <div class="teamDisplay"><!--active: <span class="onActiveTeams">5</span>  &nbsp;&nbsp;&nbsp;-->  total: <span class="onTotalTeams">13</span></div>
                        </fieldset>
                        <div class="cardElement email"><a href="#">marcopolo@anotherlongemailanemail.com</a></div>
                        <div class="cardElement phone">(345) 834-2345</div>
                        <div class="cardElement lastseen">last seen: 3 mar 2021 12:15pm</div>
                        </div>
                        <div class="mainButton">
                            <button id="sendCardMessage" class="orange small" style="display:none;"><div class="icon icon-comment"></div> message madeline</button>
                            <!-- add user to role buttons, display:none by default, use-case--when choosing user for a team -->
                            <button id="addChiefExecutive" class="roleRequired small">add as chief executive</button>
                            <button id="addHumanResources" class="roleRequired small">add as human resources</button>
                            <button id="addInsuranceAdjuster" class="roleSuggested small">add as insurance adjuster</button>
                            <button id="addClaimManager" class="roleSuggested small">add as claim manager</button>
                            <button id="addSafetyManager" class="roleSuggested small">add as safety manager</button>
                            <button id="addWCAgent" class="roleSuggested small">add as work comp agent</button>
                            <button id="addSupervisor" class="roleSuggested small">add as employee supervisor</button>
                            <button id="addServiceAgent" class="roleSuggested small">add as service agent</button>
                            <button id="addMedicalSupport" class="roleOptional small">add as medical support</button>
                            <button id="addOtherUser" class="roleOptional centered">add as other user</button>
                            <!-- use to remove from a team, display:none by default -->
                            <button id="removeFromTeam" class="red small" data-userid="">remove from team</button>
                        </div>
                    </div>
<style>
.cardTabs{}

</style>
<script>
function openTab(tabName) {
  var i;
  var x = document.getElementsByClassName("tab");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  document.getElementById(tabName).style.display = "block";
}
</script>
                    <div class="cardBackL">
                        <div class="cardBackContent">
                            <span class="cardTitle">Madeline</span>
                            <span class="cardSubTitle">Smith</span>
                            <hr>
                            <div class="cardTabs">
                                <div class="buttonArray">
                                    <button class="olive small" onclick="openTab('cardTab1')">info</button>
                                    <button class="olive small" onclick="openTab('cardTab2')">roles</button>
                                    <button class="olive small" onclick="openTab('cardTab3')">teams</button>
                                    <button class="olive small" onclick="openTab('cardTab4')">active</button>
                                </div>
                                <div id="cardTab1" class="tab">
                                    <h2>info</h2>
                                    <p>details on the user.</p>
                                </div>

                                <div id="cardTab2" class="tab" style="display:none">
                                    <h2>roles</h2>
                                    <p>roles this user is active in.</p>
                                </div>

                                <div id="cardTab3" class="tab" style="display:none">
                                    <h2>teams</h2>
                                    <p>all teams this user is on.</p>
                                </div>

                                <div id="cardTab4" class="tab" style="display:none">
                                    <h2>active</h2>
                                    <p>just the active teams.</p>
                                </div>

                            </div>
                            <div class="scrollingDivContent" style="display:none;">    
                                <fieldset><legend>Roles</legend>
                                <div>human resources, manager, other, supervisor</div>    
                                </fieldset>
                                <fieldset><legend>Team Templates</legend>
                                <div>team1, team2, team3, team4, team5, team6, team7</div>
                                </fieldset>
                                <fieldset><legend>Active Teams</legend>
                                <div><a>jim f's team</a>, <a>betty e's team</a>, <a>jeremy g's team</a>, <a>thomas t's team</a>, <a>helen o's team</a></div>
                                </fieldset>
                            </div>
                        </div>
                        <a class="flipCardbackL"></a>
                        <div class="mainButton">
                            <button class="olive small"><div class="icon icon-pencil"></div> edit user</button>
                            <button class="red small"><div class="icon icon-trash-o"></div> remove user</button>
                        </div>                        
                    </div> 

                    <div class="cardBackR">
                        <div class="cardBackContent">
                        <span class="cardTitle">Some Settings</span>
                        <hr>    
                        Etiam venenatis, lorem ac imperdiet hendrerit, orci elit imperdiet nisi, a convallis velit dolor ac tellus. Duis in arcu ultricies, elementum ante id, mattis nibh. Vivamus at odio est. In blandit ipsum efficitur ultrices vulputate. Nam mattis urna placerat mauris volutpat, et laoreet lectus dignissim. Proin neque enim, fermentum ut nisi in, gravida gravida augue. Vivamus aliquam, sem at fringilla aliquet, risus orci commodo orci, at luctus magna erat eu libero.</div>
                        <a class="flipCardbackR"></a>
                        <div class="mainButton">
                            <button class="cyan centered">settings</button>
                        </div>                        
                    </div>                 

                    <div class="cardBackX">
                        <div class="cardBackContent">
                        <span class="cardTitle">Injury Data</span>
                        <hr>    
                        Etiam venenatis, lorem ac imperdiet hendrerit, orci elit imperdiet nisi, a convallis velit dolor ac tellus. Duis in arcu ultricies, elementum ante id, mattis nibh. Vivamus at odio est. In blandit ipsum efficitur ultrices vulputate. Nam mattis urna placerat mauris volutpat, et laoreet lectus dignissim. Proin neque enim, fermentum ut nisi in, gravida gravida augue. Vivamus aliquam, sem at fringilla aliquet, risus orci commodo orci, at luctus magna erat eu libero.</div>
                        <a class="flipCardbackX"></a>
                        <div class="mainButton">
                            <button class="red centered">overview</button>
                        </div>                        
                    </div> 

                </div> 
            </div>
        </div>           
<!-- end card-->    


            <br>
            <div class="buttonArray">
                <button class="cyan centered showZenModal" id="addNewUserToRole" data-modalcontent="#add_user_to_team"><div class="icon icon-retweet"></div> add new user to role</button>
            </div> 
        </div>

</div>

<script>
$( ".flipCardL" ).click(function() {
    $(".cardBackL").css("display", "none");
    $(".cardBackR").css("display", "none");
    $(".cardBackX").css("display", "none");         
    $( ".userCard" ).toggleClass( "cardFlipL" );
    $(".cardBackL").css("display", "block");   
});
$( ".flipCardR" ).click(function() {
    $(".cardBackL").css("display", "none");  
    $(".cardBackR").css("display", "none");
    $(".cardBackX").css("display", "none");        
    $( ".userCard" ).toggleClass( "cardFlipR" );   
    $(".cardBackR").css("display", "block");    
});
$( ".flipCardX" ).click(function() {
    $(".cardBackL").css("display", "none");  
    $(".cardBackR").css("display", "none");
    $(".cardBackX").css("display", "none");        
    $( ".userCard" ).toggleClass( "cardFlipX" );   
    $(".cardBackX").css("display", "block");    
});
$( ".flipCardbackL" ).click(function() {
  $( ".userCard" ).toggleClass( "cardFlipL" );
});
$( ".flipCardbackR" ).click(function() {
  $( ".userCard" ).toggleClass( "cardFlipR" );
 });
$( ".flipCardbackX" ).click(function() {
  $( ".userCard" ).toggleClass( "cardFlipX" );
 });
 $(document).ready(function() {
  $('#demo-tooltip-above').jBox('Tooltip', {
    theme: 'TooltipDark'
  }); 
});
</script>
<div id="show_image" class="zModal" style="display:none">
    <div class="modalContent wide">
        <div class="modalHeader"><span class="modalTitle"><div class="icon inline icon-picture"></div> image title</span></div>
        <div class="modalBody" style="min-width:260px;">                
            <div class="buttonArray center">
                <button class="cyan centered" onclick="location.href='/fullscreen';">full screen</button>
                <button class="blue centered">edit description</button>
                <button class="red centered">delete</button>
            </div>
            <div class="showFullImage">
                <div class="fullImage" style="background-image:url('/images/utility/testimage-landscape.jpg');"></div>
                <div class="fullImageCaption">-- no caption --</div>

            </div>
        </div>
    </div> 
</div>

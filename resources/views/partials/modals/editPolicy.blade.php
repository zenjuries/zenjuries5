<div id="showInputPolicy" class="modalContent modalBlock dark" style="display:none">


    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/insurance.png">update your policy number</span></div>


    <div class="modalBody" style="min-width:260px;">
        <section class="sectionPanel dark">
            <section class="formBlock dark">

                <div class="formGrid">

                    <div class="formInput">
                        <label for="claimNumberInput">Policy Number</label>
                        <div class="inputIcon number"><input id="editClaimNumberInput" name="claimNumberInput"></div>
                        <span class="inputError"></span>
                    </div>

                    <div class="formInput">
                        <label for="confirmClaimNumberInput">Confirm Policy Number</label>
                        <div class="inputIcon number"><input id="editConfirmClaimNumberInput" name="confirmClaimNumberInput"></div>
                        <span class="inputError"></span>
                    </div>

                </div>

                <div class="formGrid">
                    <div class="formInput">
                        <label for="claimNumberInput">Renewal Month / Day</label>
                        <div class="inputIcon month" style="display:inline-block;">
                            <select name="claimNumberRenewalMonth" id="editClaimNumberRenewalMonth">
                                <option value="---">select</option>
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select> 
                        </div>
                        <div class="inputIcon day" style="display:inline-block;"><input type="text" id="editClaimNumberRenewalDay" name="claimNumberRenewalDay" style="width:60px;"></div>
                    </div>
                </div>                

                <div class="buttonArray">
                    <button class="grey centered" id="editSubmitBtn"><div class="icon icon-heart"></div> Input Policy #</button>
                </div> 

            </section>                 
        </section>  
    </div>

</div>

<script>
	$(document).ready(function() {
		  $('#editClaimNumberInput').on('input', function(){
			  checkEditMatch();
		  })
		  $('#editConfirmClaimNumberInput').on('input', function(){
			  checkEditMatch();
		  })
          $('#editClaimNumberRenewalMonth').on('input', function(){
            checkEditMatch();
          })
          $('#editClaimNumberRenewalDay').on('input', function(){
            checkEditMatch();
          })
	  });
	  function checkEditMatch(){
		    var claimNumber = document.getElementById("editClaimNumberInput");
		    var confirmClaimNumber = document.getElementById("editConfirmClaimNumberInput");
            var renewalMonth = document.getElementById("editClaimNumberRenewalMonth");
            var renewalDay = document.getElementById("editClaimNumberRenewalDay");
		    var submitPolicyBtn = document.getElementById("editSubmitBtn");
		    if(claimNumber.value == "" && confirmClaimNumber.value == ""){
		        claimNumber.className = '';
			    confirmClaimNumber.className = '';
		    }
		    else if(claimNumber.value == confirmClaimNumber.value){
			    claimNumber.className = 'match';
			    confirmClaimNumber.className = 'match';
		    }
		    else{
			    claimNumber.className = 'nomatch';
			    confirmClaimNumber.className = 'nomatch';
		    }

            if(claimNumber.value == confirmClaimNumber.value && renewalDay.value != "" && renewalDay.value > 0 && renewalDay.value <= 31 && renewalMonth.value != "---")
            {
                submitPolicyBtn.className = 'cyan centered';
			    submitPolicyBtn.innerHTML = '<div class="icon icon-heart"></div> Update Policy #';
            }
            else{
                submitPolicyBtn.className = 'grey centered';
			    submitPolicyBtn.innerHTML = '<div class="icon icon-heart"></div> Input Policy #';
            }
	    }
        $('#editSubmitBtn').on('click', function(){
            var policy_number = $('#editClaimNumberInput').val();
            var renewal_month = $('#editClaimNumberRenewalMonth').val();
            var renewal_day = $('#editClaimNumberRenewalDay').val();
            if(policy_number != "" && renewal_month != "---" && renewal_day > 0 && renewal_day <= 31)
            {
                $.ajax({
                    type: 'POST',
                    url: "{{ route('newPolicyNumber') }}",
                    data: {
                        _token: '<?php echo csrf_token(); ?>',
                        company_id : '<?php echo $company->id; ?>',
                        policy_number: policy_number,
                        renewal_day: renewal_day,
                        renewal_month: renewal_month,
                    },
                    success: function(data){
                        showAlert("Thanks for adding the policy number! You're all set to begin submitting claims!", "confirm", 5);
                        modal.close();
                    },
                    error: function(data){
                        showAlert("Sorry, there was a problem updating your policy number. Please try again later.", "deny", 5);
                    }
                })
            }
        
        });
  </script>
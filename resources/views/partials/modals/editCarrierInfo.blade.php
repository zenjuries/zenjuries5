<div class="modalContent modalBlock dark" id="editCarrierInfoModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/insurance.png">carrier info</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
            <!-- any content can be added in this area--> 

                <!-- below is a sample of form layout -->
                <section class="formBlock dark">
                    <div class="modalDescription">edit the info for your carrier.</div> 
					<div class="formGrid">
                        <div class="formInput">
                            <label for="zenCarrierSelect">Select Zen Carrier</label>
                            <select id="zenCarrierSelect" name="zenCarrierSelect">
                                <option value="false">None of the Above</option>
                                <?php
                                $zen_carriers = \App\ZenCarrier::get();
                                foreach($zen_carriers as $zen_carrier){
                                    echo "<option value='$zen_carrier->id'";
                                    /*
                                    if($zen_carrier->id === $company->zen_carrier_id){
                                        echo " selected='selected' ";
                                    }
                                    */
                                    echo ">$zen_carrier->carrier_name</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="formInput">
                            <!-- input -->
                            <label for="carrierName">carrier name</label>
                            <input id="carrierName" type="text" value="{{ is_null($carrier) ? "" : $carrier->name }}" />
                            <span class="inputError">enter name</span>
                        </div>
                        <div class="formInput">
                            <!-- input -->
                            <label for="carrierPhone">carrier phone</label>
                            <div class="inputIcon phone"><input id="carrierPhone"  class="phone_us" type="tel" aria-label="Please enter your phone number" placeholder="ex. (111)-111-1111" value="{{(is_null($carrier) ? "" : $carrier->phone)}}" /></div>
                            <span class="inputError">enter phone</span>
                        </div>
                        <div class="formInput">
                            <!-- input -->
                            <label for="carrierEmail">carrier email</label>
                            <input id="carrierEmail" type="text" value="{{(is_null($carrier) ? "" : $carrier->email)}}" />
                            <span class="inputError">enter email</span>
                        </div>
                    </div>

                    <!-- make sure to always use the buttonArray for centered buttons and multi buttons-->
                    <div class="buttonArray">
                        <button class="grey centered" id="submitCarrierInfo"><div class="icon icon-retweet"></div> submit</button>
                    </div>                   
                </section>
                <!-- end of sample form -->

            <!--end of any added content -->
            </section>
        </div>
    </div>
</div>

<script>
$('#submitCarrierInfo').on('click', function(){
    var zen_carrier = $('#zenCarrierSelect').val();
    if(zen_carrier === "false"){
        var valid = true;
        var name = $('#carrierName').val();
        var phone = $('#carrierPhone').val();
        var email = $('#carrierEmail').val();
        if(phone === "" || phone === undefined || phone.length < 14){
            valid = false;
            $('#carrierPhone').parent().parent().find('.inputError').addClass('show');
        }
        else{
            $('#carrierPhone').parent().parent().find('.inputError').removeClass('show');
        }
        if(email === "" || email === undefined){
            valid = false;
            $('#carrierEmail').parent().find('.inputError').addClass('show');
        }
        else{
            $('#carrierEmail').parent().find('.inputError').removeClass('show');
        }
        if(name === "" || name === undefined){
            valid = false;
            $('#carrierName').parent().find('.inputError').addClass('show');
        }
        else{
            $('#carrierName').parent().find('.inputError').removeClass('show');
        }
        if(valid === true){
            addNewCarrier(name, phone, email);
        }
    }else{
        addZenCarrier(zen_carrier);
    }
});

function addNewCarrier(name, phone, email){
    $.ajax({
        type: 'POST',
        url: '{{ route("addNewCarrier") }}',
        data: {
            _token : '<?php echo csrf_token(); ?>',
            name: name,
            phone: phone,
            email: email,
            company_id: "{{ $company->id }}",
        },
        success: function(data){
            updateCarrierInfoFields(data);
            modal.close();
            showAlert("Thanks for your update!", "confirm", 5);
        },
        error: function(data){
            console.log(data);
            var errors = data.responseJSON;
            console.log(errors);
            showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
        }
    });
}
    
function addZenCarrier(zencarrier_id){
    $.ajax({
        type: 'POST',
        url: '{{ route("selectZenCarrier") }}',
        data: {
            _token: '<?php echo csrf_token(); ?>',
            company_id: '{{ $company->id }}',
            zencarrier_id: zencarrier_id
        },
        success: function(data){
            updateCarrierInfoFields(data);
            console.log(data);
            modal.close();
            showAlert("Thanks for your update!", "confirm", 5);
        },
        error: function(data){
            showAlert("Sorry, an error occurred. Please try again later.", "deny", 5);
        }
    });
} 
    
function updateCarrierInfoFields(carrier){
    $('#currentCarrierName').text(carrier['name']);
    $('#currentCarrierPhone').text(carrier['phone']);
    $('#currentCarrierEmail').text(carrier['email']);

    $('#carrierName').text("");
    $('#carrierPhone').text("");
    $('#carrierEmail').text("");
}    
</script>
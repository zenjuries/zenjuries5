<script src="https://cdn.jsdelivr.net/npm/fuse.js/dist/fuse.js"></script>
<script src="/js/emoji_keyboard.js"></script>
<link rel="stylesheet" href="/css/emoji_keyboard.css">

<div class="modalContent modalBlock dark" id="treePostModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/message.png">communicate</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <section class="formBlock dark">
                    <div class="modalDescription">send a message to your team.</div>
					<div class="formGrid">  
						<div class="formInput" style="min-height:190px;">
                            <textarea id="treePostInput" style="min-width:300px;min-height:160px;"></textarea>
                            <textarea id="editTreePostInput" style="display:none;min-width:300px;min-height:160px;"></textarea>
                            <br>
                            <span class="inputError"></span>
                        </div>
                        <div class="buttonArray">
                            <button class="orange centered" id="submitTreePost"><div class="icon icon-comment"></div> send message</button>
                        </div>
                    </div>
                </section>
            </section>
        </div>
    </div>
</div>


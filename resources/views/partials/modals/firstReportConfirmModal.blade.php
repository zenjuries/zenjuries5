<div class="modalContent modalBlock dark" id="firstReportConfirmModal" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/edit.png">First Report</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
			<section class="formBlock dark">
				<div class="modalDescription">Confirm First Report of injury has been filed with your insurance carrier.</div> 
					<div class="buttonArray">
                        <button class="red centered" id="cancelTheFirstReport"><div class="icon icon-retweet"></div>cancel</button>
						<button class="green centered" id="submitTheFirstReport"><div class="icon icon-retweet"></div>confirm</button>
					</div>            
			</section>                 
		</section>  
	</div>
</div>
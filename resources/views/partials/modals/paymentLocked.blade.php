<script src="https://js.stripe.com/v3/"></script>
<?php
// preferably some optionals here and grabbing payment options from the env
    $company = Auth::user()->getCompany();
    $isCompanyActive = ($paymentRepo->checkIfActive(Auth::user()->getCompany()->id)) ? "true" : "false";
    if ($user->is_zenpro === 1) $isCompanyActive = 'true';
    if ($user->email == 'ally@roarkandsutton.com') $isCompanyActive = 'true';
    $pays = $company->pays();

    if($isCompanyActive != 'true' && $pays == true){
        $intent = $company->createSetupIntent([
            'customer' => $company->createOrGetStripeCustomer()->id,
            'payment_method_types' => ['us_bank_account', 'card'],
            'payment_method_options' => [
                'us_bank_account' => [
                'verification_method' => 'instant',
                // 'financial_connections' => ['permissions' => ['payment_method', 'balances']],
                ],
            ],
            'metadata' => [
                'customer' => $company->createOrGetStripeCustomer()->id,
                'company_id' => $company->id
            ]
        ]);
    }
?>
<div id="showPaymentLocked" class="modalContent modalBlock dark" style="display:none">

    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/insurance.png">Payment Error</span></div>

    <div class="modalBody" style="min-width:590px;min-height:600px;">
        <section class="sectionPanel dark">
            <section class="formBlock dark">
                <div class="center" style="padding-top:10px;">
                <!-- countdown classes:  attention, alert, critical -->
                    <div class="countdownContainer"><div class="countdown attention"><span id="daysLeft" class="countdownValue">3</span><span class="countdownLabel">days</span><span class="countdownDesc">remaining</span></div></div>
                </div>
                <div class="modalDescription">In order to use Zenjuries, please update your payment method. You will not be able to use the system until you have updated your payment method and have resumed your subscription.</div>
                <br>
                <div id="policyNumberNotUploaded">

                    <form id="payment-form"  style="width:100%;">
                        <div id="error-message">
                            <!-- Display error message to your customers here -->
                        </div>
                        <div class="formGrid">
                            <div id="payment-element" style="width:90%;">
                                <!-- Elements will create form elements here -->
                            </div>
                        </div>
                    </form>
                    <br>
                    <div class="buttonArray">
                        <button class="grey centered" id="newSubmitBtn"><div class="icon icon-shield"></div> Update & Resume Payments</button>
                    </div>
                    @if(env('APP_ENV') != 'production')
                    <button class="grey centered" onclick="modal.close()"><div class="icon icon-shield"></div> close dialogue </button>
                    @endif
                </div>
            </section>
        </section>
    </div>

</div>
@if($isCompanyActive != 'true' && $pays == true)
<script>
        $(document).ready(function(){
            const stripe = Stripe('<?php echo config('stripe.key'); ?>');

            const options = {
                clientSecret: '<?=$intent->client_secret?>',
                // Fully customizable with appearance API.
                appearance: {
                    theme: 'night'
                },
            };

            // Set up Stripe.js and Elements to use in checkout form, passing the client secret obtained in step 2
            const elements = stripe.elements(options);

            // Create and mount the Payment Element
            const paymentElement = elements.create('payment');
            paymentElement.mount('#payment-element');

        });
    // $('#newSubmitBtn').on('click', function(){
    //     var policy_number = $('#newClaimNumberInput').val();
    //     var renewal_month = $('#newClaimNumberRenewalMonth').val();
    //     var renewal_day = $('#newClaimNumberRenewalDay').val();

    //     if(policy_number != "" && renewal_month != "---" && renewal_day > 0 && renewal_day <= 31)
    //     {
    //         $.ajax({
    //             type: 'POST',
    //             url: "{{ route('newPolicyNumber') }}",
    //             data: {
    //                 _token: '<?php echo csrf_token(); ?>',
    //                 company_id : '<?php echo $company->id; ?>',
    //                 policy_number: policy_number,
    //                 renewal_day: renewal_day,
    //                 renewal_month: renewal_month,
    //             },
    //             success: function(data){
    //                 showAlert("Thanks for adding the policy number! You're all set to begin submitting claims!", "confirm", 5);
    //                 var completionText = document.getElementById("policyNumberUploaded");
    //                 completionText.style = "display:inline block";
    //                 modal.close();
    //             },
    //             error: function(data){
    //                 showAlert("Sorry, there was a problem updating your policy number. Please try again later.", "deny", 5);
    //             }
    //         })
    //     }

    // });

   </script>
@endif

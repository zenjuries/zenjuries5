<div class="modalContent dark modalBlock" id="updateStatusModal_rehab" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/heart.png">update rehab</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                    <div id="update_status_rehabDiv">
                        <div class="buttonCloud">
                            <button name="blah" id="lightdutyButton" class="lightdutyButton rehabButton" data-status="Light Duty"><div class="icon inline icon-rehab"></div> Assigned Light Duty</button>
                            <button name="blah" id="fulldutyButton" class="fulldutyButton rehabButton" data-status="Full Duty"><div class="icon inline icon-workman"></div> Assigned Full Duty</button>
                            <button name="blah" id="skipButton" class="grey rehabButton" data-status="rehabSkip"><div class="icon inline icon-stethoscope"></div> Skip this step</button>
                        </div>
                        <p><b>Always set the date the employee's status changed, and add a note if helpful.</b> Setting this accurately will help provide you with data about how quickly your employee's return to work.</p>
                        <section class="formBlock dark">
                            <div class="formGrid">
                                <!--<div id="injuryDate"></div>-->
                                <div class="formInput">
                                <label for="status-date-rehab">date of update</label>
                                <input type="date" id="status-date-rehab" name="status-date-rehab"
                                    pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}">
                                </div>
                            </div>
                            <textarea id="rehabNote" style="width:100%;" placeholder="add a note"></textarea><br><br>
                        </section>
                        <div class="buttonArray" >
                                <button id="fillInBtnRehab" class="red centered">select a status and set a date</button>
                                <button  id="submitStatusUpdateRehab" class="cyan centered" required style="display:none;"><div class="icon inline icon-check-circle"></div> set updated status</button>
                        </div>
                    </div>                     
                
                </div>
            </section>
        </div>
    </div>
</div>
<script>
    //submit the update
$('#submitStatusUpdateRehab').on('click', function(){
        var date = $('#status-date-rehab').val();
        var status = $('.rehabButton.selected').data('status');
        var note = $('#rehabNote').val();

        $.ajax({
            type: 'POST',
            url: '<?php echo route("updateEmployeeStatus"); ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                injury_id: {{ $injury->id }},
                status: status,
                date: date,
                note: note,
            },
            success: function(data){
                $('#rehabContainer').removeClass('fullduty');
                $('#rehabContainer').removeClass('lightduty');
                $('#rehabContainer').removeClass('skipStep');
                status = status.toLowerCase();
                status = status.replace(/ +/g, "");
                if(status === "rehabskip"){
                    status = 'skipStep';
                }
                $('#rehabContainer').addClass(status);
                getRecentPosts();
                getTreePosts();
                modal.close();

            },
            error: function(data){
                modal.close();
            }
        })

    });

    //sets selected class on eval buttons
    $('.rehabButton').on('click', function(){
        $('.rehabButton').removeClass('selected');
        $(this).addClass('selected');
    });

    //checks to see if a severity and date has been set
    $('#update_status_rehabDiv').on('click', function(){
        console.log('running');
        if($('.rehabButton').hasClass('selected')){
            console.log('button has class');
        }else{
            console.log('button does not have class');
        }




        if($('#status-date-rehab').val()){
            console.log('date has value');
        }else{
            console.log($('#status-date-rehab').val());
            console.log('date does not have value');
        }
        if($('.rehabButton').hasClass('selected') && $('#status-date-rehab').val()){
            $('#fillInBtnRehab').hide();
            $('#submitStatusUpdateRehab').show();
        }else{
            $('#fillInBtnRehab').show();
            $('#submitStatusUpdateRehab').hide();
        }
    });
</script>


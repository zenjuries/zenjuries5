<div class="modalContent modalBlock dark" id="addAppointmentModal" style="display: none">
    <div class="modalHeader">
        <span class="modalTitle" id="setAptTitle" style="display:none;">add an Appointment</span>
        <span class="modalTitle" id="setFollowTitle" style="display:auto;">set a Follow-Up</span>
    </div>    
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
			<section class="formBlock dark">
				{{-- <div class="modalDescription">something about this form</div>  --}}
					<!-- your code here-->
					<div class="formGrid">  
						<div class="formInput">
							<!-- input -->
							<label for="location">Location Name</label>
							<div class="location"><input type="text" id="addAppointmentName"/></div>
						</div>
						<div class="formInput">
							<!-- input -->
							<label for="phone">Location Phone</label>
							<div class="inputIcon phone"><input id="addAppointmentPhone" class="phone_us" type="tel" aria-label="Please enter your phone number" placeholder="ex. (111)-111-1111"/></div>
							<span class="inputError">numbers only</span>
						</div>
						<div class="formInput">
							<!-- input -->
							<label for="email">Location Email</label>
							<div class="inputIcon email"><input id="addAppointmentEmail" type="text"/></div>
							<span class="inputError">--</span>
						</div>
                        <section class="formBlock dark">
                            <div class="sectionInfo">Select Date and Time</div>
                            <div class="formGrid">
                                <!--<div id="injuryDate"></div>-->
                                <div class="formInput">
                                <label for="datePickerPlaceholder">date/time</label>
                                <input type="datetime-local" id="datetime" name="datetime"
                                    pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}"/>
                                </div>
                            </div>
                        </section>
                        <div class="formInput">
							<!-- input -->
							<label for="email">Notes</label>
							<div class="inputIcon userinfo"><input id="addAppointmentNote" type="text"/></div>
							<span class="inputError">--</span>
						</div>
					</div>
                    <div class="buttonArray">
						<button class="green" id="setApptBtn"><div class="icon icon-retweet"></div>set this appointment</button>
                        <button class="blue" id="setFollowBtn" style="display:none;"><div class="icon icon-retweet"></div>add follow-up</button>
					</div>           					
					<!-- end your code here--> 
			</section>                 
		</section>  
	</div>
</div>
<div class="modalContent modalBlock dark" id="claimNumberModal" style="display: none;">
<div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/insurancenum.png">add claim number</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <section class="formBlock dark">
                    <div class="modalDescription">enter your claim number.</div> 
					<div class="formGrid">
                        <div class="formInput">
                            <label for="claimNumberInput">Claim Number</label>
                            <div class="inputIcon number"><input id="claimNumberInput" name="claimNumberInput"></div>
                            <span class="inputError"></span>
                        </div>
                        <div class="formInput">
                            <label for="confirmClaimNumberInput">Confirm Claim Number</label>
                            <div class="inputIcon number"><input id="confirmClaimNumberInput" name="confirmClaimNumberInput"></div>
                            <span class="inputError"></span>
                        </div>
                    </div>
                    <div class="buttonArray">
                        <button class="grey centered" id="submitClaimNumber"><div class="icon icon-retweet"></div> Input Claim #</button>
                    </div>
                </section>
            </section>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#claimNumberInput').on('input', function(){
            checkClaimMatch();
        })
        $('#confirmClaimNumberInput').on('input', function(){
            checkClaimMatch();
        })
    });
    function checkClaimMatch(){
        var claimNumber = document.getElementById("claimNumberInput");
        var confirmClaimNumber = document.getElementById("confirmClaimNumberInput");
        var submitPolicyBtn = document.getElementById("submitClaimNumber");
        if(claimNumber.value == "" && confirmClaimNumber.value == ""){
            claimNumber.className = '';
            confirmClaimNumber.className = '';
            submitPolicyBtn.className = 'grey centered';
            submitPolicyBtn.innerHTML = '<div class="icon icon-retweet"></div> Input Claim #';
        }
        else if(claimNumber.value == confirmClaimNumber.value){
            claimNumber.className = 'match';
            confirmClaimNumber.className = 'match';
            submitPolicyBtn.className = 'cyan centered';
            submitPolicyBtn.innerHTML = '<div class="icon icon-retweet"></div> Update Claim #';
        }
        else{
            claimNumber.className = 'nomatch';
            confirmClaimNumber.className = 'nomatch';
            submitPolicyBtn.className = 'grey centered';
            submitPolicyBtn.innerHTML = '<div class="icon icon-retweet"></div> Input Claim #';
        }
    }
</script>
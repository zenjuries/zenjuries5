<?php
// preferably some optionals here and grabbing payment options from the env
    $pays = $company->pays();
    if ($pays){
        $intent = $company->createSetupIntent([
            'customer' => $company->createOrGetStripeCustomer()->id,
            'payment_method_types' => ['us_bank_account', 'card'],
            'payment_method_options' => [
                'us_bank_account' => [
                'verification_method' => 'instant',
                // 'financial_connections' => ['permissions' => ['payment_method', 'balances']],
                ],
            ],
            'metadata' => [
                'customer' => $company->createOrGetStripeCustomer()->id,
                'company_id' => $company->id
            ]
        ]);
    }
?>
<script>
$(document).ready(function() {

    var tab = 'billingaddress';

    $('.zenTabs').addClass(tab);
    $('.zenTabContent.' + tab + 'Tab').show();
    $('.zenTabs').on('click', 'li', function(){
            var tab_class = $(this).attr('class');
            var target = $(this).data('target');
            $('.zenTabs').removeClass().addClass('zenTabs ' + tab_class);
            $('.zenTabContent').hide();
            $('.zenTabContent.' + target).show();
    });

});
</script>
<script src="https://js.stripe.com/v3/"></script>

<div class="modalContent modalBlock dark" id="editBillingInfoModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/creditcard.png">billing info</span></div>
    <div class="modalBody" style="min-width:590px;min-height:520px;">
        <div class="modalContent">
            <div class="zenTabs" style="width:100%;">
                <ul class="twoTabs">
                    <li class="billingaddress" data-target="billingaddressTab"><a class="tabBillingaddress"><div class="icon icon-map-marker"></div><span>billing address</span></a></li>
                    <li class="billingtype" data-target="billingtypeTab"><a class="tabBillingtype"><div class="icon icon-credit-card"></div><span>billing method <span class="TXT__tiny">(stripe)</span></span></a></li>
                </ul>
            </div>

            <div class="zenTabContent billingaddressTab">
                <section class="sectionPanel dark">
                    <section class="formBlock dark">
                        <div class="modalDescription">edit your billing info.</div>
                        <div class="formGrid">
                            <div class="formInput">
                                <!-- input -->
                                <label for="billingAddress">address line 1</label>
                                <div class="inputIcon address"><input id="billingAddress" type="text" value="{{ $team->billing_address }}"/></div>
                                <span class="inputError">enter address</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="billingAddressLine2">address line 2</label>
                                <div class="inputIcon address"><input id="billingAddress2" type="text" value="{{ $team->billing_address_line_2 }}"/></div>
                                <span class="inputError">enter address 2</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="billingCity">city</label>
                                <div class="inputIcon address"><input id="billingCity" type="text" value="{{ $team->billing_city }}"/></div>
                                <span class="inputError">enter city</span>
                            </div>

                            <div class="formInput">
                                <label for="billingState">state</label>
                                <div class="inputIcon address">
                                    <select id="billingState" value="{{ $team->billing_state }}"><option>choose state</option><option value="Alabama">Alabama</option><option value="Alaska">Alaska</option><option value="Arizona">Arizona</option><option value="Arkansas">Arkansas</option><option value="California">California</option><option value="Colorado">Colorado</option><option value="Connecticut">Connecticut</option><option value="Delaware">Delaware</option><option value="District of Columbia">District of Columbia</option><option value="Florida">Florida</option><option value="Georgia">Georgia</option><option value="Guam">Guam</option><option value="Hawaii">Hawaii</option><option value="Idaho">Idaho</option><option value="Illinois">Illinois</option><option value="Indiana">Indiana</option><option value="Iowa">Iowa</option><option value="Kansas">Kansas</option><option value="Kentucky">Kentucky</option><option value="Louisiana">Louisiana</option><option value="Maine">Maine</option><option value="Maryland">Maryland</option><option value="Massachusetts">Massachusetts</option><option value="Michigan">Michigan</option><option value="Minnesota">Minnesota</option><option value="Mississippi">Mississippi</option><option value="Missouri">Missouri</option><option value="Montana">Montana</option><option value="Nebraska">Nebraska</option><option value="Nevada">Nevada</option><option value="New Hampshire">New Hampshire</option><option value="New Jersey">New Jersey</option><option value="New Mexico">New Mexico</option><option value="New York">New York</option><option value="North Carolina">North Carolina</option><option value="North Dakota">North Dakota</option><option value="Northern Marianas Islands">Northern Marianas Islands</option><option value="Ohio">Ohio</option><option value="Oklahoma">Oklahoma</option><option value="Oregon">Oregon</option><option value="Pennsylvania">Pennsylvania</option><option value="Puerto Rico">Puerto Rico</option><option value="Rhode Island">Rhode Island</option><option value="South Carolina">South Carolina</option><option value="South Dakota">South Dakota</option><option value="Tennessee">Tennessee</option><option value="Texas">Texas</option><option value="Utah">Utah</option><option value="Vermont">Vermont</option><option value="Virginia">Virginia</option><option value="Virgin Islands">Virgin Islands</option><option value="Washington">Washington</option><option value="West Virginia">West Virginia</option><option value="Wisconsin">Wisconsin</option><option value="Wyoming">Wyoming</option></select>
                                </div>
                                <span class="inputError">enter state</span>
                            </div>

                            <div class="formInput">
                                <!-- input -->
                                <label for="billingZip">zip</label>
                                <div class="inputIcon address"><input id="billingZip" type="text" value="{{ $team->billing_zip }}"/></div>
                                <span class="inputError">enter zip</span>
                            </div>
                        </div>
                        <div class="buttonArray">
                            <button class="cyan centered" id="submitBillingInfo"><div class="icon icon-retweet"></div> update billing</button>
                        </div>
                    </section>
                </section>
            </div>
            <div class="zenTabContent billingtypeTab">
                <section class="sectionPanel dark">
                    <section class="formBlock dark">
                        <div class="modalDescription">edit your billing method.</div>
                        <form id="payment-form">

                            <div id="error-message">
                                <!-- Display error message to your customers here -->
                            </div>
                            <div class="formGrid">
                                <div id="payment-element" style="width:90%;">
                                    <!-- Elements will create form elements here -->
                                </div>
                            </div>
                            <!-- end your code here-->

                            <div class="buttonArray mt-4">
                                <button class="cyan centered" id="submitPaymentInfo"><div class="icon icon-retweet"></div> update</button>
                            </div>
                        </form>
                    </section>
                </section>
            </div>
        </div>
    </div>
</div>

<script>
    const stripe = Stripe('<?php echo config('stripe.key'); ?>');
    const pays = '<?php echo $pays; ?>';
    if (pays){
        const options = {
            clientSecret: '<?=$intent->client_secret?>',
            // Fully customizable with appearance API.
            appearance: {
                theme: 'night'
            },
        };

        // Set up Stripe.js and Elements to use in checkout form, passing the client secret obtained in step 2
        const elements = stripe.elements(options);

        // Create and mount the Payment Element
        const paymentElement = elements.create('payment');
        paymentElement.mount('#payment-element');

        $('#submitPaymentInfo').on('click', async function(){
            const { setupIntent, error} = await stripe.confirmSetup({
                elements,
                confirmParams: {
                    return_url: '<?php echo route("policyHolderProfile"); ?>',
                }
            });
            if (error) {
                $('#error-message').html(error.message);
                modal.close();
                /*
                console.log(error);
                alert('error: ' + error.message);
                */
            }
        });
    }

    $('#submitBillingInfo').on('click', function(){
        //getting values from modal
        var billingAddress = $('#billingAddress').val();
        var billingAddressLine2 = $('#billingAddress2').val();
        var billingCity = $('#billingCity').val();
        var billingState = $('#billingState').val();
        var billingZip = $('#billingZip').val();
        var billingCountry = "US";

        console.log("show variables");
        console.log(billingAddress);
        console.log(billingAddressLine2);
        console.log(billingCity);
        console.log(billingState);
        console.log(billingZip);
        console.log("end show variables");

        var valid = true;

        if(billingAddress == ""){
            valid = false;
            $('#billingAddress').parent().parent().find('.inputError').addClass('show');
        }
        else{
            $('#billingAddress').parent().parent().find('.inputError').removeClass('show');
        }

        if(billingCity == ""){
            valid = false;
            $('#billingCity').parent().parent().find('.inputError').addClass('show');
        }
        else{
            $('#billingCity').parent().parent().find('.inputError').removeClass('show');
        }

        if(billingState == "choose state"){
            valid = false;
            $('#billingState').parent().parent().find('.inputError').addClass('show');
        }
        else{
            $('#billingState').parent().parent().find('.inputError').removeClass('show');
        }

        if(billingZip == ""){
            valid = false;
            $('#billingZip').parent().parent().find('.inputError').addClass('show');
        }
        else{
            $('#billingZip').parent().parent().find('.inputError').removeClass('show');
        }

        if(valid){
            // we need more checks
            $.ajax({
                type: 'POST',
                url: '{{ route("updateCompanyBillingInfo") }}',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    companyId: '<?php echo $company->id; ?>',
                    billingAddress: billingAddress,
                    billingAddressLine2: billingAddressLine2,
                    billingCity: billingCity,
                    billingState: billingState,
                    billingZip: billingZip
                },
                success: function(){
                    //closeModals();
                    modal.close();
                    showAlert("Thanks for your update!", "confirm", 5);
                },
                error: function(){
                    showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
                }

            });
        }


    });
</script>


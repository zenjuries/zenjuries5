<div id="showAddInputPolicy" class="modalContent modalBlock dark" style="display:none">


    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/insurance.png">add your policy number</span></div>


    <div class="modalBody" style="min-width:260px;">
        <section class="sectionPanel dark">
            <div class="center" style="padding:20px 0;">You can't use the zenjuries system without a policy number.  Add this number to start using zenjuries now!</div>
            <section class="formBlock dark">

                <div class="formGrid">

                    <div class="formInput">
                        <label for="claimNumberInput">Policy Number</label>
                        <div class="inputIcon number"><input id="claimNumberInput" name="claimNumberInput"></div>
                        <span class="inputError"></span>
                    </div>

                    <div class="formInput">
                        <label for="confirmClaimNumberInput">Confirm Policy Number</label>
                        <div class="inputIcon number"><input id="confirmClaimNumberInput" name="confirmClaimNumberInput"></div>
                        <span class="inputError"></span>
                    </div>

                </div>

                
                <div class="formGrid">
                    <div class="formInput">
                        <label for="claimNumberInput">Renewal Month / Day</label>
                        <div class="inputIcon month" style="display:inline-block;">
                            <select name="claimNumberRenewalMonth" id="claimNumberRenewalMonth">
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select> 
                        </div>
                        <div class="inputIcon day" style="display:inline-block;"><input type="text" id="claimNumberRenewalDay" name="claimNumberRenewalDay" style="width:60px;"></div>
                    </div>
                </div>  

                <div class="buttonArray">
                    <button class="cyan centered" id="submitPolicyNumber"><div class="icon icon-insurance"></div> add policy number</button>
                </div> 

            </section>                 
        </section>  
    </div>

</div>
<div class="modalContent dark modalBlock" id="stopforlitigationModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/stop.png">stop for litigation</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                    @if(!$injury->paused_for_litigation)
                        <div id="litigationDivContainer">
                            <div id="litigationDiv1" class="litigationDiv">
                                <p class="center">
                                    <b>Do you need to STOP this claim for litigations?</b><br>
                                    Once this claim has come under litigation, all communication with the Injured Employee must stop, and you will not be able to modify the injury in Zenjuries.<br>
                                    <b>This can not be easily undone!</b>
                                </p>
                                <div class="buttonArray">
                                    <button class="red" id="claimStopButton1"><div class="icon inline icon-ban"></div> STOP for litigation</button>
                                    <button onclick="modal.close()">Cancel</button>
                                </div>
                            </div>

                            <div id="litigationDiv2" class="litigationDiv" style="display: none">
                                <p>
                                    Are you SURE you want to stop this claim for litigation?<br>
                                    You will be unable to easily reverse this or make any changes to the claim once its done.
                                </p>
                                <div class="buttonArray">
                                <button class="red" id="claimStopButton2"> Yes, STOP claim</button>
                                <button onclick="modal.close()">Cancel</button>
                                </div>
                            </div>

                            <div id="litigationDiv3" class="litigationDiv" style="display:none">
                                <p>
                                    Are you REALLY SURE you want to stop this claim for litigation?<br>
                                    All activity for this claim in Zenjuries will stop. You will have to contact Zenjuries support to unpause the claim.<br>
                                </p>
                                <div class="buttonArray">
                                <button class="red" id="confirmClaimStopButton">Confirm litigation</button>
                                <button onclick="modal.close()"> Cancel</button>
                                </div>
                            </div>
                        </div>
                    @else
                        <h3 class="FG__red">This claim has been stopped for litigation! You will be unable to modify anything in Zenjuries, but the claim is still available to view. If this claim was stopped by mistake, please contact support@zenjuries.com to reverse this.</h3>
                    @endif
                </div>
            </section>
        </div>
    </div>
</div>
<div class="modalContent modalBlock dark" id="completeInjuryModal" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/heart.png">complete injury</span></div>
    <div class="modalBody" style="min-width:260px;">
        <section class="sectionPanel dark">
            <section class="formBlock dark">
                <div class="modalDescription">complete this injury</div>
                <br>
                <div class="divCenter">Please enter the official Claim Close Date for this claim. You can get this date from your carrier, or ask your insurance agent if you aren't sure.</div>
                <div class="formGrid">
                    <div class="formInput">
                        <label for="resolveDate">resolve date</label>
                        <input id="resolveDateNewDate" type="date"/>
                    </div> 
                </div>
	            <div class="buttonArray">
	                <button class="green centered" id="submitInjuryResolvedNewDate"><div class="icon icon-heart"></div> resolve injury</button>
	            </div>  
            </section>                 
        </section>  
    </div>
</div>
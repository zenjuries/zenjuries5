<?php
if(Auth::user()->is_zagent === 1){
    $user_agency = Auth::user()->getAgency();
    //$user_agency_company = \App\Company::where('is_agency', 1)->where('agency_id', $user_agency->id)->first();
    $user_companies = $user_agency->getCompanies("activated");
}
?>
<div class="modalContent modalBlock dark" id="selectPolicyholderModal" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/company.png">policyholders</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
            <div class="modalDescription"><span style="font-size:1.2rem;font-weight:300;display:block;margin-bottom:12px;">Select one of your policyholders below.</span></div>
            <div id="teamList" class="buttonCloud selectPolicyholder">
                @if(Auth::user()->is_zagent === 1)
                @foreach($user_companies as $company)
                <button class="companySelectButton" data-id="{{$company->id}}" data-company="true"><div class="policyholderLogo" style="background-image:url('{{$company->logo_location}}')"></div> <span>{{$company->company_name}}</span></button>
                @endforeach
                @endif
                <!-- 
                <button><div class="policyholderLogo" style="background-image:url('images/default_logos/company1.png')"></div> <span>policyholder name1</span></button>
                <button><div class="policyholderLogo" style="background-image:url('images/default_logos/company2.png')"></div> <span>policyholder longer name2</span></button>
                <button><div class="policyholderLogo" style="background-image:url('images/default_logos/company3.png')"></div> <span>policyholder3</span></button>
                <button><div class="policyholderLogo" style="background-image:url('images/default_logos/company4.png')"></div> <span>pholder4</span></button>
                <button><div class="policyholderLogo" style="background-image:url('images/default_logos/company5.png')"></div> <span>policyholder num5</span></button>
                <button><div class="policyholderLogo" style="background-image:url('images/default_logos/company6.png')"></div> <span>policyholder6</span></button>
                <button><div class="policyholderLogo" style="background-image:url('images/default_logos/company7.png')"></div> <span>policyholder7</span></button>
                 -->
            </div>       
        </section>                    
    </div> 
</div>
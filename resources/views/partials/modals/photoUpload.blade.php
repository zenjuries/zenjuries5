

    <?php
    $value = config('constants.file_storage_limit');
    ?>
    <div class="sectionContent">

        <section class="formBlock dark">
            <div class="formElement" style="display: none">
                <button class="cyan showFileModal" data-modalcontent="#uploadImageModal">upload an image</button>
            </div>

            <div class="formGrid photos">

                <!-- upload bar- change percent on .uploadSpace for bar length, add 'warning | critical' on .uploadSpaceBar for yellow and red coloring.  change .remainingSpace value for current values.-->
                <div class="uploadSpaceIndicator" style="display:none"><div class="uploadSpaceBar critical"><div class="uploadSpace" style="width:0%;"></div></div><div class="remainingSpace">0k of 50000</div></div>

                <div id="photoTarget">

                </div>
                <!-- uploaded images should create a new formMedia div below.  Clicking on the image divs should open a modal with image manip features (delete, show full screen, etc)-->
                <!-- replicate this div block to add new images.  Use background-image: to display the added image
                hide or show mediaCaptionSummary if data exists.  Clicking mediaCaption will open the add description modal
                mediaSelectOverlay provides the dark background up top so you can see the info, clickable area to open modal
                mediaInfo should show a filename and how much memory the image is.
                mediaDelete will remove the photo from the list.
                -->


                <!--
                <div class="formMedia">
                    <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2016/08/29/08/55/work-1627703_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                </div>
                <div class="formMedia">
                    <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2017/10/22/22/16/business-2879465_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                </div>
                <div class="formMedia">
                    <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2018/01/17/07/06/laptop-3087585_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                </div>
                <div class="formMedia">
                    <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2017/04/25/22/28/despaired-2261021_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary" style="display:none;">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                </div>
                <div class="formMedia">
                    <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2016/08/29/08/55/work-1627703_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                </div>
                -->
            </div>
        </section>
    </div>


{{-- <section class="sectionPanel">
    <span class="sectionTitle"><div class="icon icon-picture"></div> include images</span>
    <div class="sectionContent">

        <section class="formBlock dark">
            <div class="formElement">
                <button class="cyan">upload an image</button>
            </div>
            <hr>
            <div class="formGrid">
                <!-- uploaded images should create a new formImage div below.  Clicking on the image divs should open a modal with image manip features (delete, show full screen, etc)-->
                <!-- replicate this div block to add new images.  Use background-image: to display the added image
                <div class="formImage">
                    <div class="uploadedImage" style="background-image:url('https://cdn.pixabay.com/photo/2018/01/17/07/06/laptop-3087585_960_720.jpg');"></div>
                </div>
                -->
                <div class="formImage">
                    <div class="uploadedImage" style="background-image:url('https://cdn.pixabay.com/photo/2017/04/25/22/28/despaired-2261021_960_720.jpg');"></div>
                </div>
                <div class="formImage">
                    <div class="uploadedImage" style="background-image:url('https://cdn.pixabay.com/photo/2016/08/29/08/55/work-1627703_960_720.jpg');"></div>
                </div>
                <div class="formImage">
                    <div class="uploadedImage" style="background-image:url('https://cdn.pixabay.com/photo/2017/10/22/22/16/business-2879465_960_720.jpg');"></div>
                </div>
                <div class="formImage">
                    <div class="uploadedImage" style="background-image:url('https://cdn.pixabay.com/photo/2018/01/17/07/06/laptop-3087585_960_720.jpg');"></div>
                </div>
                <div class="formImage">
                    <div class="uploadedImage" style="background-image:url('https://cdn.pixabay.com/photo/2017/04/25/22/28/despaired-2261021_960_720.jpg');"></div>
                </div>
                <div class="formImage">
                    <div class="uploadedImage" style="background-image:url('https://cdn.pixabay.com/photo/2016/08/29/08/55/work-1627703_960_720.jpg');"></div>
                </div>
                <div class="formImage">
                    <div class="uploadedImage" style="background-image:url('https://cdn.pixabay.com/photo/2017/10/22/22/16/business-2879465_960_720.jpg');"></div>
                </div>
                <div class="formImage">
                    <div class="uploadedImage" style="background-image:url('https://cdn.pixabay.com/photo/2018/01/17/07/06/laptop-3087585_960_720.jpg');"></div>
                </div>
                <div class="formImage">
                    <div class="uploadedImage" style="background-image:url('https://cdn.pixabay.com/photo/2017/04/25/22/28/despaired-2261021_960_720.jpg');"></div>
                </div>
                <div class="formImage">
                    <div class="uploadedImage" style="background-image:url('https://cdn.pixabay.com/photo/2016/08/29/08/55/work-1627703_960_720.jpg');"></div>
                </div>
                <div class="formImage">
                    <div class="uploadedImage" style="background-image:url('https://cdn.pixabay.com/photo/2017/10/22/22/16/business-2879465_960_720.jpg');"></div>
                </div>
            </div>
        </section>
    </div>
</section> --}}

<section class="sectionPanel" style="display: none" id="mediaLinksDiv">
    <span class="sectionTitle"><div class="icon icon-link"></div> add external media links</span>
    <div class="sectionContent">
        <section class="formBlock dark">
            <div class="formElement">
                <button id="addMedia" class="cyan">upload media link</button>
            </div>
            <hr>
            <div class="formGrid mmlinks">
                <div class="formMedia">
                    <div class="uploadedMedia"><span class="mediaType"></span><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">some link on the net</span><div class="mediaSelectOverlay link" onclick="location.href='#';"></div><span class="mediaDelete"></span><span class="mediaEdit"></span></div>
                </div>
                <div class="formMedia">
                    <div class="uploadedMedia"><span class="mediaType photo"></span><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very </div><span class="mediaInfo">link to some photo</span><div class="mediaSelectOverlay photo" onclick="location.href='#';"></div><span class="mediaDelete"></span><span class="mediaEdit"></span></div>
                </div>
                <div class="formMedia">
                    <div class="uploadedMedia"><span class="mediaType video"></span><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">some video somewhere</span><div class="mediaSelectOverlay video" onclick="location.href='#';"></div><span class="mediaDelete"></span><span class="mediaEdit"></span></div>
                </div>
                <div class="formMedia">
                    <div class="uploadedMedia"><span class="mediaType doc"></span><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">some document link</span><div class="mediaSelectOverlay doc" onclick="location.href='#';"></div><span class="mediaDelete"></span><span class="mediaEdit"></span></div>
                </div>
                <div class="formMedia">
                    <div class="uploadedMedia"><span class="mediaType coll"></span><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very </div><span class="mediaInfo">link to some collection of docs</span><div class="mediaSelectOverlay coll" onclick="location.href='#';"></div><span class="mediaDelete"></span><span class="mediaEdit"></span></div>
                </div>
                <div class="formMedia">
                    <div class="uploadedMedia"><span class="mediaType med"></span><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><div class="mediaSelectOverlay med" onclick="location.href='#';"></div><span class="mediaInfo">link to something medical</span><span class="mediaDelete"></span><span class="mediaEdit"></span></div>
                </div>
            </div>
        </section>
    </div>
</section>
{{-- begin new code for injury selections --}}


<div id="fileModal" class="zModal" style="display:none">
    <!-- upload Image modal -->
    <div class="modalContent modalBlock wide" id="uploadImageModal" style="display: none">
        <div class="modalHeader"><span class="modalTitle"><div class="icon inline icon-picture"></div> upload image</span></div>
        <div class="modalBody" style="min-width:260px;">
               <section class="formBlock dark">
                   <div class="formInput">
                      <label for="photoUpload">Choose Photo:</label>
                      <input id="photoUpload" type="file">
                      <span class="inputError"></span>
                   </div>
                   <div class="formInput">
                       <label for="photoNameInput">Name:</label>
                       <input id="photoNameInput" name="photoNameInput">
                       <span class="inputError"></span>
                   </div>
                   <div class="formInput">
                       <label for="photoUploadDescription">Description:</label>
                       <textarea id="photoUploadDescription"></textarea>
                   </div>
                    <div class="formInput">
                        <span>File upload limit: {{$value * 1000}}K</span>
                    </div>
                    <br />
                   <button id="submitUpload">Upload</button>
               </section>
        </div>
    </div>


    <!-- show image modal -->
    <div class="modalContent modalBlock wide" id="showImageModal" style="display: none">
        <div class="modalHeader"><span class="modalTitle"><div class="icon inline icon-picture"></div> image title</span></div>
        <div class="modalBody" style="min-width:260px;">
            <div class="buttonArray center">
                <button id="fullscreenButton" class="cyan centered"><a>full screen</a></button>
                <button class="blue centered editDescription" data-id="">edit description</button>
                <button class="red centered deletePhoto" data-id="">delete</button>
            </div>
            <div class="showFullImage">
                <div class="fullImage" style="background-image:url('/images/utility/testimage-landscape.jpg');"></div>
                <div class="fullImageCaption">-- no caption --</div>

            </div>
        </div>
    </div>

    <!-- media description modal -->
    <div class="modalContent modalBlock dark" id="mediaDescriptionModal" style="display: none">
        <div class="modalHeader"><span class="modalTitle"><div class="icon inline icon-picture"></div> edit description</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                    <section class="formBlock dark">
                        <div class="formGrid">
                            <div class="formInput">
                                <!-- input -->
                                <input type="hidden" id="descriptionID">
                                <label for="photo_description">description</label>
                                <textarea id="photo_description"></textarea>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="center"><button id="setDescription" class="cyan">set description</button></div>
            </section>
        </div>
    </div>

    <!-- add media link modal -->
    <div class="modalContent modalBlock dark" id="mediaLinkModal" style="display: none">
        <div class="modalHeader"><span class="modalTitle"><div class="icon inline icon-link"></div> add media links</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                    <section class="formBlock dark">
                        <div class="formGrid">
                            <div class="formGrid short">
                                <div class="formInput">
                                    <input type="radio" name="r1" id="r1">
                                    <label for="r1">link</label>
                                </div>
                                <div class="formInput">
                                    <input type="radio" name="r1" id="r2">
                                    <label for="r2">photo</label>
                                </div>
                                <div class="formInput">
                                    <input type="radio" name="r1" id="r3">
                                    <label for="r3">video</label>
                                </div>
                                <div class="formInput">
                                    <input type="radio" name="r1" id="r4">
                                    <label for="r4">document</label>
                                </div>
                                <div class="formInput">
                                    <input type="radio" name="r1" id="r5">
                                    <label for="r5">collection</label>
                                </div>
                                <div class="formInput">
                                    <input type="radio" name="r1" id="r6">
                                    <label for="r6">medical</label>
                                </div>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="media_url">URL of media</label>
                                <input id="media_url" />
                                <span class="inputError show">error</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="media_description">description</label>
                                <textarea id="media_description"></textarea>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="center"><button class="cyan">add link</button></div>
            </section>
        </div>
    </div>

</div>

<script>

    $('body').on('click','.showFileModal', function(){
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#fileModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        console.log(target);
        if(target === "#showImageModal"){
            var url = $(this).parent().css('background-image');
            var id = $(this).parent().data('id');
            console.log(url);
            $('.fullImage').css('background-image', url);
            $('#fullscreenButton').find('a').attr('href', '/zencare/fullscreen/image/' + id);
            $('#showImageModal').find('.editDescription').data('id', id);
            $('#showImageModal').find('.deletePhoto').data('id', id);


        }else if(target === "#mediaDescriptionModal"){
            var id = $(this).closest('.formMedia').find('.uploadedMedia').data('id');
            var description = $(this).closest('.formMedia').find('.mediaCaptionSummary').html();
            console.log(id);
            console.log(description);
            $('#descriptionID').val(id);
            $('#photo_description').val(description);
        }

        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
        modal.open();
    });

    $('#submitUpload').on('click', function(){
       uploadPhoto();
    });

    function uploadPhoto(){
        
        var upload_errors = $('#photoUpload').parent().find('.inputError')
        var file = document.getElementById('photoUpload').files[0];
        if(file === undefined || file === ""){
            upload_errors.html("Photo Required").addClass('show');
            return false;
        }

        if(file.type == "image/png" || file.type == "image/jpeg"){
            var name = $('#photoNameInput').val();
            var description = $('#photoUploadDescription').val();
            var formData = new FormData();
            formData.append('photo', file);
            formData.append('name', name);
            formData.append('description', description);
            formData.append('injury_id', '<?php echo $injury->id; ?>');
            formData.append('_token', '<?php echo csrf_token(); ?>');

            $.ajax({
                type: 'POST',
                url: '<?php echo route('addInjuryPhoto'); ?>',
                data: formData,
                contentType: false,
                processData: false,
                success: function(data){
                    showAlert('file uploaded!', "confirm", 5);
                    modal.close();
                    getTreePosts();
                    getInjuryPhotos();
                    
                },
                error: function(data){
                    //alert('error');
                    console.log(data)
                }
            })
        }else{
            showAlert("Sorry, please make sure your image is a png or a jpeg.", "deny", 5);
            upload_errors.html("Invalid File Type").addClass('show');
        }
    }

    var photos;
    getInjuryPhotos();
    
    function getInjuryPhotos(){
        $.ajax({
            type: 'POST',
            url: '<?php echo route('getInjuryPhotos'); ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                injury_id: '<?php echo $injury->id; ?>'
            },
            success: function(data){
                photos = data;
                console.log("photos from getInjuryPotos" + photos);
                console.log(JSON.stringify(photos));
                writePhotos();
            },
            error: function(data){
                alert('error');
                console.log(data);
            }
        })
    }

    function writePhotos(){
        /* uploaded images should create a new formMedia div below.  Clicking on the image divs should open a modal with image manip features (delete, show full screen, etc)-->
        <!-- replicate this div block to add new images.  Use background-image: to display the added image
        hide or show mediaCaptionSummary if data exists.  Clicking mediaCaption will open the add description modal
        mediaSelectOverlay provides the dark background up top so you can see the info, clickable area to open modal
        mediaInfo should show a filename and how much memory the image is.
        mediaDelete will remove the photo from the list.

       <div class="formMedia">
                    <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2017/04/25/22/28/despaired-2261021_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                </div>
                */
        var html = "";
        for(var i = 0; i < photos.length; i++){
            if(photos[i].name == null){
                photos[i].name = "Injury Photo";
            }
            if(photos[i].description == null){
                photos[i].description = "";
            }
            html += '<div class="formMedia">'+
                        '<div class="uploadedMedia" data-id="'+photos[i].id+'" style="background-image:url(\''+photos[i].location+'?{{ $date = str_replace(' ', "_", \Carbon\Carbon::now()) }}\');">'+
                            '<div class="mediaSelectOverlay showImage showFileModal" data-modalcontent="#showImageModal"></div>'+
                            '<div class="mediaCaptionSummary">'+photos[i].description+'</div>'+
                            '<span class="mediaInfo">231k | '+photos[i].name+'</span>'+
                            '<span class="mediaDelete deletePhoto" data-id="' + photos[i].id + '"></span>'+
                            '<span class="mediaCaption"></span>'+
                        '</div>'+
                    '</div>';
            /*
            html += '<li data-id="'+photos[i].id+'"><a href="'+photos[i].location+'" target="_blank">' +
                        '<span class="documentIcon"><img src="'+photos[i].location+'?{{ $date = str_replace(' ', "_", \Carbon\Carbon::now()) }}"></span>' +
                        '<span class="documentTitle">'+photos[i].name+'</span>' +
                        '<span class="documentDesc">'+photos[i].description+'</span></a></li>';
            
            html+= '<div class="formMedia" data-id="' + photos[i].id + '">' +
                        '<div class="uploadedMedia" data-id="' + photos[i].id + '" style="background-image:url(' + photos[i].location + '?{{ $date = str_replace(' ', "_", \Carbon\Carbon::now()) }});"><div class="mediaSelectOverlay showFileModal" data-modalcontent="#showImageModal"></div>' +
                        '<div class="mediaCaptionSummary">' + photos[i].description +'</div><span class="mediaInfo">' + photos[i].name + '</span><span class="mediaDelete deletePhoto" data-id="' + photos[i].id + '"></span><span class="mediaCaption showFileModal" data-modalcontent="#mediaDescriptionModal" data-id="' + photos[i].id + '"></span></div>' +
                   '</div>';
                   */
        }
        html += '<div class="formMedia">' +
                    '<div class="uploadedMedia newPhoto showFileModal" data-modalcontent="#uploadImageModal" id="newPhotoUpload">' +
                        '<span style="width:100%;text-align:center;top:30px;">Add new</span>' +
                        '<span style="width:100%;text-align:center;top:48px;font-size:1.6rem;">photo</span>'+
                        '<div style="width:100%;text-align:center;margin:0 auto;position:absolute;top:90px;">' +
                            '<div style="font-size:2.6rem" class="icon inline icon-file-image-o"></div>' +
                            '<div style="font-size:1.6rem;top:-5px;" class="icon inline icon-plus"></div>' +
                        '</div>'+
                    '</div>'+
                '</div>';
        console.log('writing photo list to #photoGrid');
        $('#photoGrid').html(html);

    }

    $('body').on('click', '.deletePhoto', function(){
        var id = $(this).data('id');
        $.ajax({
            type: 'POST',
            url: '<?php echo route('deleteInjuryPhoto'); ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                photo_id: id
            },
            success: function(data){
                showAlert("photo deleted!", "confirm", 5);
                getInjuryPhotos();
                getTreePosts();
                if(modal != null){
                    modal.close();
                }
            },
            error: function(data){
                showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
            }
        })
    //}
    });

    $('.editDescription').on('click', function(){
       var id = $(this).data('id');
       modal.close();
       $('.mediaCaption[data-id="' + id +'"').click();
    });

    $('#setDescription').on('click', function(){
       var id = $('#descriptionID').val();
       var description = $('#photo_description').val();
       $.ajax({
           type: 'POST',
           url: '<?php echo route('updatePhotoDescription'); ?>',
           data: {
               _token: '<?php echo csrf_token(); ?>',
               photo_id: id,
               description: description
           },
           success:function(data){
               showAlert('description updated!', "confirm", 5);
               modal.close();
               getInjuryPhotos();
           }
       })
    });



</script>
<div class="modalContent dark modalBlock" id="updateStatusModal_recovery" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/plus.png">update recovery</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                    <div id="update_status_recoveryDiv">
                        <div class="buttonCloud">
                            <button name="blah" id="homeButton" class="recoverhomeButton recoveryButton" data-status="Home"><div class="icon inline icon-ambulance"></div> Recovering at home</button>
                            <button name="blah" id="skipButton" class="grey recoveryButton" data-status="recoverySkip"><div class="icon inline icon-stethoscope"></div> Skip this step</button>
                        </div>
                        <p><b>Always set the date the employee's status changed, and add a note if helpful.</b> Setting this accurately will help provide you with data about how quickly your employee's return to work.</p>
                        <section class="formBlock dark">
                            <div class="formGrid">
                                <!--<div id="injuryDate"></div>-->
                                <div class="formInput">
                                <label for="status-date-recovery">date of update</label>
                                <input type="date" id="status-date-recovery" name="status-date-recovery"
                                    pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}">
                                </div>
                            </div>
                            
                            <textarea id="recoveryNote" style="width:100%;" placeholder="add a note"></textarea><br><br>
                        </section>
                        <div class="buttonArray" >
                                <button id="fillInBtnRecovery" class="red centered">select a status and set a date</button>
                                <button  id="submitStatusUpdateRecovery" class="cyan centered" required style="display:none;"><div class="icon inline icon-check-circle"></div> set updated status</button>
                        </div>
                    </div>    
                
                </div>
            </section>
        </div>
    </div>
</div>
<script>
//submit the update
$('#submitStatusUpdateRecovery').on('click', function(){
        var date = $('#status-date-recovery').val();
        var status = $('.recoveryButton.selected').data('status');
        var note = $('#recoveryNote').val();
        $.ajax({
            type: 'POST',
            url: '<?php echo route("updateEmployeeStatus"); ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                injury_id: {{ $injury->id }},
                status: status,
                date: date,
                note: note,
            },
            success: function(data){
                $('#recoveryContainer').removeClass('home');
                $('#recoveryContainer').removeClass('skipStep');
                status = status.toLowerCase();
                if(status === "recoveryskip"){
                    status = "skipStep";
                }
                $('#recoveryContainer').addClass(status);
                getRecentPosts();
                getTreePosts();
                modal.close();
            },
            error: function(data){
                modal.close();
            }
        })

    });

    //sets selected class on eval buttons
    $('.recoveryButton').on('click', function(){
        $('.recoveryButton').removeClass('selected');
        $(this).addClass('selected');
    });

    //checks to see if a severity and date has been set
    $('#update_status_recoveryDiv').on('click', function(){
        console.log('running');
        if($('.recoveryButton').hasClass('selected')){
            console.log('button has class');
        }else{
            console.log('button does not have class');
        }




        if($('#status-date-recovery').val()){
            console.log('date has value');
        }else{
            console.log($('#status-date-recovery').val());
            console.log('date does not have value');
        }
        if($('.recoveryButton').hasClass('selected') && $('#status-date-recovery').val()){
            $('#fillInBtnRecovery').hide();
            $('#submitStatusUpdateRecovery').show();
        }else{
            $('#fillInBtnRecovery').show();
            $('#submitStatusUpdateRecovery').hide();
        }
    });
</script>


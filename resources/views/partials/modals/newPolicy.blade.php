<div id="showNewPolicy" class="modalContent modalBlock dark" style="display:none">

    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/insurance.png">submit your policy</span></div>


    <div class="modalBody" style="min-width:260px;">
        <section class="sectionPanel dark">
            <section class="formBlock dark">
                <div class="center" style="padding-top:10px;">
                <!-- countdown classes:  attention, alert, critical -->
                    <div class="countdownContainer"><div class="countdown attention"><span id="daysLeft" class="countdownValue">30</span><span class="countdownLabel">days</span><span class="countdownDesc">remaining</span></div></div>
                </div>
                <div class="modalDescription">In order to use Zenjuries, we need to have your policy number and uploaded policy.  You have the above number of days to obtain and upload that information.  After this time, you will not be able to use the system until you have updated your policy number and uploaded your policy.</div>

                <div id="policyNumberNotUploaded">
                    <div class="formGrid">
                        <div class="formInput">
                            <label for="claimNumberInput">Policy Number</label>
                            <div class="inputIcon number"><input id="newClaimNumberInput" name="claimNumberInput"></div>
                            <span class="inputError"></span>
                        </div>
                        <div class="formInput">
                            <label for="confirmClaimNumberInput">Confirm Policy Number</label>
                            <div class="inputIcon number"><input id="newConfirmClaimNumberInput" name="confirmClaimNumberInput"></div>
                            <span class="inputError"></span>
                        </div>
                    </div>

                    <div class="formGrid">
                        <div class="formInput">
                            <label for="claimNumberInput">Renewal Month / Day</label>
                            <div class="inputIcon month" style="display:inline-block;">
                                <select name="claimNumberRenewalMonth" id="newClaimNumberRenewalMonth">
                                    <option value="---">select</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select> 
                            </div>
                            <div class="inputIcon day" style="display:inline-block;"><input type="text" id="newClaimNumberRenewalDay" name="claimNumberRenewalDay" style="width:60px;"></div>
                        </div>
                    </div>  

                    <div class="buttonArray">
                        <button class="grey centered" id="newSubmitBtn"><div class="icon icon-shield"></div> Input Policy #</button>
                    </div>                    
                </div>

                <div id="policyNumberUploaded" style="display:none">
                    <div class="modalDescription"><div class="icon icon-shield FG__cyan inline"></div> <span id="policyNumber">445345tr3</span>
                    <br>Thank you for submitting your policy number  Please upload your policy to complete your requirements.</div>                
                </div>

                <div id="policyNotUploadedBtn" style="display:none">
                    <div class="modalDescription">Use the button below to upload your policy in pdf format.</div> 
                    <div class="buttonArray">
                        <button class="blue centered" id="submitPolicy"><div class="icon icon-cloud-upload"></div> upload policy</button>
                    </div>                
                </div>

                <div id="policyUploaded" style="display:none">
                    <div class="modalDescription"><div class="icon icon-cloud-upload FG__blue"></div> Thanks!  Your policy has been uploaded and saved to your account.</div>                
                </div>

            </section>                 
        </section>  
    </div>

</div>

<script>
	$(document).ready(function() {
		   $('#newClaimNumberInput').on('input', function(){
			    checkNewMatch();
		   })
		   $('#newConfirmClaimNumberInput').on('input', function(){
		        checkNewMatch();
		   })
           $('#newClaimNumberRenewalMonth').on('input', function(){
                checkNewMatch();
           })
           $('#newClaimNumberRenewalDay').on('input', function(){
                checkNewMatch();
          })
	   });
	   function checkNewMatch(){
		    var claimNumber = document.getElementById("newClaimNumberInput");
		    var confirmClaimNumber = document.getElementById("newConfirmClaimNumberInput");
            var renewalMonth = document.getElementById("newClaimNumberRenewalMonth");
            var renewalDay = document.getElementById("newClaimNumberRenewalDay");
		    var submitPolicyBtn = document.getElementById("newSubmitBtn");
            if(claimNumber.value == "" && confirmClaimNumber.value == ""){
		        claimNumber.className = '';
			    confirmClaimNumber.className = '';
		    }
		    else if(claimNumber.value == confirmClaimNumber.value){
			    claimNumber.className = 'match';
			    confirmClaimNumber.className = 'match';
		    }
		    else{
			    claimNumber.className = 'nomatch';
			    confirmClaimNumber.className = 'nomatch';
		    }

            if(claimNumber.value == confirmClaimNumber.value && renewalDay.value != "" && renewalDay.value > 0 && renewalDay.value <= 31 && renewalMonth.value != "---")
            {
                submitPolicyBtn.className = 'cyan centered';
			    submitPolicyBtn.innerHTML = '<div class="icon icon-shield"></div> Update Policy #';
            }
            else{
                submitPolicyBtn.className = 'grey centered';
			    submitPolicyBtn.innerHTML = '<div class="icon icon-shield"></div> Input Policy #';
            }
	    }
        $('#newSubmitBtn').on('click', function(){
            var policy_number = $('#newClaimNumberInput').val();
            var renewal_month = $('#newClaimNumberRenewalMonth').val();
            var renewal_day = $('#newClaimNumberRenewalDay').val();
            
            if(policy_number != "" && renewal_month != "---" && renewal_day > 0 && renewal_day <= 31)
            {
                $.ajax({
                    type: 'POST',
                    url: "{{ route('newPolicyNumber') }}",
                    data: {
                        _token: '<?php echo csrf_token(); ?>',
                        company_id : '<?php echo $company->id; ?>',
                        policy_number: policy_number,
                        renewal_day: renewal_day,
                        renewal_month: renewal_month,
                    },
                    success: function(data){
                        showAlert("Thanks for adding the policy number! You're all set to begin submitting claims!", "confirm", 5);
                        var completionText = document.getElementById("policyNumberUploaded");
                        completionText.style = "display:inline block";
                        modal.close();
                    },
                    error: function(data){
                        showAlert("Sorry, there was a problem updating your policy number. Please try again later.", "deny", 5);
                    }
                })
            }
        
        });
        $('#policyNotUploadedBtn').on('click', function(){
            var policyUploadText = document.getElementById("policyUploaded");
            policyUploadText.style = "display:inline block";
            


        });
        
   </script>

<div class="modalContent modalBlock dark" id="uploadPhotosModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/myphoto.png">upload photos</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <div class="divCenter">Click <span style="display:inline-block;padding:2px;background-color:#333;border-radius:2px;">+ add photo</span> to upload images for review.  You can upload up to <b>eight (8)</b> images.</div>
                <br>
                <div class="formGrid photos thumbnails"> 
                    <div class="formMedia"><div class="photo" style="background-image:url('https://picsum.photos/100');"></div></div>  
                    <div class="formMedia"><div class="photo" style="background-image:url('https://picsum.photos/200');"></div></div>
                    <div class="formMedia"><div class="photo" style="background-image:url('https://picsum.photos/300');"></div></div> 
                    <div class="formMedia"><div class="photo" style="background-image:url('https://picsum.photos/100');"></div></div>  
                    <div class="formMedia"><div class="photo" style="background-image:url('https://picsum.photos/200');"></div></div> 
                    <div class="formMedia"><div class="photo" style="background-image:url('https://picsum.photos/300');"></div></div> 
                    <div class="formMedia"><div class="photo" id="upload_link"><span>+ add photo</span></div></div>  
                    <input id="upload" type="file" style="display:none;"/>                          
                </div> 
                <br><br>
                <div class="buttonArray">
                    <button class="cyan centered" id="imageModalSave"><div class="icon icon-photo"></div> upload images</button>
                </div>             
            </section>
        </div>
	</div>
</div>

<script>
$(function(){
    $("#upload_link").on('click', function(e){
        e.preventDefault();
        $("#upload:hidden").trigger('click');
    });
});
</script>   
<div class="modalContent dark modalBlock" id="updateCostsModal" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/cash.png">update claims costs</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="sectionPanel dark">
                <section class="formBlock dark">
                    <div class="formGrid">
                        <div class="formInput">
                            <label for="paidMedicalInput" class="FG__green TXT__bold">Paid Medical</label>
                            <div class="inputIcon cost"><input type=[number] id="paidMedicalInput" value="{{(is_null($costs['paid_medical']) ? "0" : $costs['paid_medical'])}}"></div>
                        </div>
                        <div class="formInput">
                            <label for="paidIndemnityInput" class="FG__green TXT__bold">Paid Indemnity</label>
                            <div class="inputIcon cost"><input type=[number] id="paidIndemnityInput" value="{{(is_null($costs['paid_indemnity']) ? "0" : $costs['paid_indemnity'])}}"></div>
                        </div>
                        <div class="formInput">
                            <label for="paidLegalInput" class="FG__green TXT__bold">Paid Legal</label>
                            <div class="inputIcon cost"><input type=[number] id="paidLegalInput" value="{{(is_null($costs['paid_legal']) ? "0" : $costs['paid_legal'])}}"></div>
                        </div>
                        <div class="formInput">
                            <label for="paidMiscInput" class="FG__green TXT__bold">Paid Miscellaneous</label>
                            <div class="inputIcon cost"><input type=[number] id="paidMiscInput" value="{{(is_null($costs['paid_misc']) ? "0" : $costs['paid_misc'])}}"></div>
                        </div>
                    </div>
                    <div class="formGrid">
                        <div class="formInput">
                            <label for="reserveMedicalInput" class="FG__red TXT__bold">Reserve Medical</label>
                            <div class="inputIcon cost"><input type=[number] id="reserveMedicalInput" value="{{(is_null($costs['reserve_medical']) ? "0" : $costs['reserve_medical'])}}"></div>
                        </div>
                        <div class="formInput">
                            <label for="reserveIndemnityInput" class="FG__red TXT__bold">Reserve Indemnity</label>
                            <div class="inputIcon cost"><input type=[number] id="reserveIndemnityInput" value="{{(is_null($costs['reserve_indemnity']) ? "0" : $costs['reserve_indemnity'])}}"></div>
                        </div>
                        <div class="formInput">
                            <label for="reserveLegalInput" class="FG__red TXT__bold">Reserve Legal</label>
                            <div class="inputIcon cost"><input type=[number] id="reserveLegalInput" value="{{(is_null($costs['reserve_legal']) ? "0" : $costs['reserve_legal'])}}"></div>
                        </div>
                        <div class="formInput">
                            <label for="reserveMiscInput" class="FG__red TXT__bold"2>Reserve Miscellaneous</label>
                            <div class="inputIcon cost"><input type=[number] id="reserveMiscInput" value="{{(is_null($costs['reserve_misc']) ? "0" : $costs['reserve_misc'])}}"></div>
                        </div>
                    </div>
                </section>
                <div class="divCenter">
                    <button id="submitClaimCostModal">Submit</button>
                </div>
            </section>

    </div>
</div>
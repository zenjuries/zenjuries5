<style>
    .accountTypeTag{position:absolute;top:-23px;right:0px;}
</style>
<script>
  $( function() {
  $(document).on('input', '.spinnerFix', function() {
    
    var allowedDigits = 2;
    var length = $(this).val().length;
    var max = parseInt($(this).attr("max"));
    
    // appears there is a bug in the latest version of Chrome. When there are multiple decimals,
    // the value of a number type field can no longer be accessed. For now, all we can do is clear it
    // when that happens
    if($(this).val() == ""){
      $(this).val("");
    }
    
    if ($(this).val().indexOf('.') != -1) {
      allowedDigits = 3;
    }
   	
    if(length > allowedDigits){
      $(this).val($(this).val().substring(1));
 	}
    
    if($(this).val() > max && max > 0){
      $(this).val($(this).val().substring(1));
    }
  });  
});
</script>

<div class="modalContent modalBlock dark" id="newClientModal" style="display: none;">

    <div id="sendingInvite" class="waitModalOverlay" style="display: none"><!-- must be none/table -->
        <div class="waitModalCenter">
            <div class="waitSpinner">
                <div class="spinnerContainer">
                    <span class="spinner green"></span>
                    <div class="centerText animate__animated animate__pulse animate__infinite animate__slow">sending<br>invite...</div>
                </div>
            </div>
        </div>
    </div> 

    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/insurance.png">add client</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent"> 
            <section class="sectionPanel dark">
                <!--tag the page if it's grandfathered or not -->
                @if($agency->new_client)
                <div id="isZenPro" class="accountTypeTag"><span class="FG__blue">zenPro .v5</span> account</div>
                @else
                <div id="isGrandfathered" class="accountTypeTag">subscription <span class="FG__green">active</span></div>
                @endif 
                <!--4 inputs for both account types, validated -->        
                <section class="formBlock dark">
                    <br>
                    <div class="divCenter">All fields are required.</div>
                    <br>
                    <div class="formGrid">
                        <div class="formInput">
                            <!-- input -->
                            <label for="companyName">Company Name</label>
                            <div class="inputIcon company"><input id="companyName" type="text" /></div>
                            <span class="inputError">required</span>
                        </div>
                        <div class="formInput">
                            <!-- input -->
                            <label for="contactName">Contact Name</label>
                            <div class="inputIcon user"><input id="contactName" type="text" /></div>
                            <span class="inputError">required</span>
                        </div>
                        <div class="formInput">
                            <!-- input -->
                            <label for="contactEmail">Contact Email</label>
                            <div class="inputIcon mail"><input id="contactEmail" type="text" /></div>
                            <span class="inputError">required</span>
                        </div>
                        <div class="formInput" style="text-align:center;">
                            <label for="claimNumberInput">Policy Renewal Mo/Day</label>
                            <div class="inputIcon month" style="display:inline-block;">
                                <select name="renewalMonth" id="renewalMonth">                
                                    <option value="select">select</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select> 
                            </div>
                            <div class="inputIcon day" style="display:inline-block;"><input class="hideSpinners spinnerFix" type="number" min="1" max="31" value="01" id="renewalDay" name="renewalDay" style="width:60px;"></div>
                            <span class="inputError">enter date</span>

                        </div>
                    </div>
                    <div class="formGrid">
                        <div class="formInput">
                            <!-- input -->
                            <label for="premium">Work Comp Annual Premium</label>
                            <div class="inputIcon cost"><input class="hideSpinners" id="premium" placeholder="in dollars" type="number" min="0"/></div>
                            <span class="inputError">required (must be a number)</span>
                        </div>
                        <div class="formInput">
                            <!-- input -->
                            <label for="upload">Upload Work Comp Policy</label>
                            <input  id="file" type="file"/>
                            <span class="inputError">required</span>
                        </div>
                        <!--Only show this if it's an new v5 account -->
                        @if($agency->new_client)                        
                        <span id="finalCostSpan" style="display: none">The cost for this account will be $<span id="finalCostTarget"></span>.</span>
                        <div class="modalDescription">Who will pay for this account? (required)
                            <span class="inputError" style="top:5px;left:0px;" id="whoPaysError">required</span>
                        </div>
                        <div class="formGrid short" id="whoPaysDiv">
                            <div class="formInput">
                            <input type="radio" name="whoPaysButton" id="agentPays" checked="checked">
                            <label for="whoPaysButton">Agency</label>
                            </div>
                            <div class="formInput">
                            <input type="radio" name="whoPaysButton" id="clientPays">
                            <label for="whoPaysButton">Client</label>
                            </div>
                        </div>
                        <div id="paymentDiv" style="display: none">
                            <input type="hidden" id="hasCard" value="1}">
                            <div class="sectionInfo">You don't have a payment method on file. <a href="/paymentMethod">Click here</a> to add a card, or choose from an option below.
                            <span class="inputError" style="top:5px;left:0px;" id="paymentMethodError">required</span>
                            </div>
                            <div class="formGrid short" id="paymentMethodDiv">
                                <div class="formInput">
                                    <!-- DEPRICATED
                                    <input style="display:none;" type="radio" name="paymentMethodType" id="useCheck">
                                    <label style="display:none;" for="useCheck">Pay With Check</label>
                                    -->
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="formElement">
                            <div class="buttonArray">
                                <button class="cyan centered hideMe" id="submitNewClient"><div class="icon icon-user"></div> submit new client</button>
                                <button class="red centered" id="incomplete"><div class="icon icon-alert"></div> please complete all fields</button>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#companyName').on('input', function(){
            checkForCompletion();
        });
        $('#contactName').on('input', function(){
            checkForCompletion();
        });
        $('#contactEmail').on('input', function(){
            checkForCompletion();
        });
        $('#renewalMonth').on('input', function(){
            checkForCompletion();
        });
        $('#renewalDay').on('input', function(){
            checkForCompletion();
        });
        $('#premium').on('input', function(){
            checkForCompletion();
        });
    });

    function checkForCompletion(){
        var companyName = document.getElementById("companyName").value;
        var contactName = document.getElementById("contactName").value;
        var contactEmail = document.getElementById("contactEmail").value;
        var renewalMonth = document.getElementById("renewalMonth").value;
        var renewalDay = document.getElementById("renewalDay").value;
        var premiumValue = document.getElementById("premium").value;

        if(premiumValue != "" && companyName != "" && contactName != "" && contactEmail != "" && renewalMonth != "select" && renewalDay != ""){
            $('#submitNewClient').removeClass('hideMe');
            $('#incomplete').addClass('hideMe');
        }
        else{
            $('#submitNewClient').addClass('hideMe');
            $('#incomplete').removeClass('hideMe');
        }
    }


</script>

<!-- |||||| the following code goes on the page showing modals |||||| -->

<!-- code to activate modals.  Keys on class, and data-modalcontent.  Can be used anywhere, not just buttons.-->
<button class="showZenModal" data-modalcontent="#**MODAL_NAME**"><div class="icon icon-bamboo2"></div> launch modal</button>

<!-- code that pulls in all of the modals used on the page.  Make sure the modals are the right format, 
have the right associated JS on their page, and you don't have too many or not enough divs-- any formatting 
issue will break ALL modals-->
<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
    @include('partials.modals.**MODAL_NAME**')                      
</div>

<!-- script that selects and shows the correct modal when clicked-->
<script>
    $('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>

<!-- |||||| the following code goes on the modals themselves |||||| -->

<div class="modalContent modalBlock dark" id="**MODAL_NAME**" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/**IMAGE_NAME**.png">**MODAL_TITLE**</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
            <!-- any content can be added in this area--> 

                <!-- below is a sample of form layout -->
                <section class="formBlock dark">
                    <div class="modalDescription">add the info for your adjuster.</div> 
					<div class="formGrid">
                        <div class="formInput">
                            <label for="adjusterNameInput">Adjuster Name</label>
                            <div class="inputIcon user"><input id="adjusterNameInput" name="adjusterNameInput" value="{{$injury->adjuster_name}}"></div>
                            <span class="inputError">required</span>
                        </div>
                        <div class="formInput">
                            <label for="adjusterEmailInput">Adjuster Email</label>
                            <div class="inputIcon mail"><input id="adjusterEmailInput" name="adjusterEmailInput" value="{{$injury->adjuster_email}}"></div>
                            <span class="inputError"></span>
                        </div>
                        <div class="formInput">
                            <label for="adjusterPhoneInput">Adjuster Phone</label>
                            <div class="inputIcon phone"><input id="adjusterPhoneInput" class="phone_us" type="tel" name="adjusterPhoneInput" value="{{$injury->adjuster_phone}}"></div>
                            <span class="inputError"></span>
                        </div>
                    </div>

                    <!-- make sure to always use the buttonArray for centered buttons and multi buttons-->
                    <div class="buttonArray">
                        <button class="grey centered" id="submitAdjusterInfo"><div class="icon icon-retweet"></div> input</button>
                    </div>                   
                </section>
                <!-- end of sample form -->

            <!--end of any added content -->
            </section>
        </div>
    </div>
</div>

<!-- add any js code you have specific to this modal below -->

<script>
/* any JS required for this modal */
</script>
<div class="modalContent modalBlock dark" id="deleteTeamModal" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/stop.png"> Delete Team</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
			<section class="formBlock dark">
				<div class="modalDescription">Are you sure you want to delete this team?</div> 			
                <div class="buttonArray">
                    <button class="green centered" id="confirmDeleteTeam"><div class="icon icon-check"></div> delete</button>
                </div>            
			</section>                 
		</section>  
	</div>
</div>
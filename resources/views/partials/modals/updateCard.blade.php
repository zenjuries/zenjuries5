<?php
$company = Auth::user()->getCompany();
$intent = $company->createSetupIntent();

if($company->pm_last_four !== NULL){
    $last_four = $company->pm_last_four;
}else{
    $last_four = "****";
}
?>

<div id="updateCardDiv">
    <span>Cardholder's Name</span><input id="updateCardName"><br>
    <span>Card Details</span><br>
    <!-- This div will be populated with card inputs by the stripe.js code -->
    <div id="updateCardCardElement"></div>
    <div id="updateCardErrors"></div>

    <button id="updateCardSubmitButton" data-secret="{{$intent->client_secret}}">Update</button>

</div>


<script src="https://js.stripe.com/v3/"></script>

<script>

    $(document).ready(function(){
        const stripe = Stripe('pk_test_9LkP3b5ldWrkpHxmNyvYt8Pk');

        const elements = stripe.elements();
        const cardElement = elements.create('card');

        cardElement.mount('#updateCardCardElement');

        const cardHolderName = document.getElementById('updateCardName');
        const cardButton = document.getElementById('updateCardSubmitButton');
        const clientSecret = cardButton.dataset.secret;

        cardButton.addEventListener('click', async (e) => {
            $('#updateCardErrors').html("");
        const { setupIntent, error} = await stripe.confirmCardSetup(
            clientSecret, {
                payment_method:{
                    card: cardElement,
                    billing_details: {name: cardHolderName.value}
                }
            }
        );
        if (error) {
            $('#updateCardErrors').html(error.message);
            /*
            console.log(error);
            alert('error: ' + error.message);
            */
        } else {
            alert('success!');
            console.log(setupIntent);
            updateCard(setupIntent['payment_method']);
        }
    });
    });

    function updateCard(payment_method){
        $.ajax({
            type: 'POST',
            url: '<?php echo route("updateCard"); ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                company_id: '<?php echo $company->id; ?>',
                payment_method: payment_method
            },
            success: function(data){
                alert('success!');
            },
            error: function(data){
                alert('error');
                console.log(data);
            }
        })
    }


</script>
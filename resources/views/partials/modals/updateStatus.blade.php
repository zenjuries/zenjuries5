<div class="modalContent dark modalBlock" id="updateStatusModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/users.png">update status</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                    <div id="employeeStatusDiv">
                        <div class="buttonCloud">
                            <button name="blah" id="hospitalButton" class="hospitalButton" data-status="Evaluation at Hospital"><div class="icon inline icon-ambulance"></div> Evaluation at Hospital</button>
                            <button name="blah" id="urgentcareButton" class="urgentcareButton" data-status="Sent to Urgent Care"><div class="icon inline icon-clinic"></div> Sent to Urgent Care</button>
                            <button name="blah" id="doctorButton" class="doctorButton" data-status="Sent to see Doctor"><div class="icon inline icon-stethoscope"></div> Sent to see Doctor</button>
                            <button name="blah" id="recoverhomeButton" class="recoverhomeButton" data-status="Recovering at Home"><div class="icon inline icon-home"></div> Recovering at Home</button>
                            <button name="blah" id="lightdutyButton" class="lightdutyButton" data-status="Assigned Light Duty"><div class="icon inline icon-rehab"></div> Assigned Light Duty</button>
                            <button name="blah" id="fulldutyButton" class="fulldutyButton" data-status="Assigned Full Duty"><div class="icon inline icon-workman"></div> Assigned Full Duty</button>
                            <button name="blah" id="maxrecoveryButton" class="maxrecoveryButton" data-status="Maximum Recovery"><div class="icon inline icon-newheart"></div> Maximum Recovery</button>
                            <button name="blah" id="terminatedButton" class="terminatedButton" data-status="Employee was Terminated"><div class="icon inline icon-times"></div> Employee was Terminated</button>
                            <button name="blah" id="resignedButton" class="resignedButton" data-status="Resigned"><div class="icon inline icon-ban"></div> Resigned</button>
                            <button name="blah" id="deathButton" class="deathButton" data-status="Death"><div class="icon inline icon-skull"></div> Death</button>
                        </div>
                        <!--
                        <div class="formInput formBlock dark">
                            <label for="employeeStatusInput">Select Employee Status</label>
                            <select name="employeeStatusInput" id="employeeStatusInput">
                                <option value="Hospital">Evaluation at Hospital</option>
                                <option value="Urgent Care">Sent to Urgent Care</option>
                                <option value="Family Practice">Sent to see Doctor</option>
                                <option value="Home">Recovering at Home</option>
                                <option value="Light Duty">Assigned Light Duty</option>
                                <option value="Full Duty">Assigned Full Duty</option>
                                <option value="Maximum Medical Improvement" selected>Maximum Recovery</option>
                                <option value="Terminated">Employee was Terminated</option>
                                <option value="Resigned">Resigned</option>
                            </select>
                        </div>
                        -->
                        <p><b>Always set the date the employee's status changed.</b> Setting this accurately will help provide you with data about how quickly your employee's return to work.</p>
                        <section class="formBlock dark">
                            <div class="formGrid">
                                <!--<div id="injuryDate"></div>-->
                                <div class="formInput">
                                <label for="status-date">date of update</label>
                                <input type="date" id="status-date" name="status-date"
                                    pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}">
                                </div>
                            </div>
                        </section>
                        <!--
                        <div id="taskListStatusDiv" class="statusDateDiv formBlock dark">
                            <span><span class="statusDateError"></span></span>
                            <p class="white"><b>Always set the date the employee's status changed.</b> Setting this accurately will help provide you with data about how quickly your employee's return to work.</p>
                            <div class="divCenter"><input type="text" id="taskListStatusDate" class="statusEffectiveDate"></div>
                        </div>
                        -->
                        <div class="buttonArray" >
                                <button id="fillInBtn" class="red centered">select a status and set a date</button>
                                <button  id="submitStatusUpdate" class="cyan centered" required style="display:none;"><div class="icon inline icon-check-circle"></div> set updated status</button>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


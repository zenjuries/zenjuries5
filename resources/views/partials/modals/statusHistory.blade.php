<div class="modalContent dark modalBlock" id="statusHistoryModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/magnify.png">status history</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                    <ul id="statusHistoryContainer">
                    this will show the history of every status change with notes.
                    </ul>
                </div>
            </section>
        </div>
    </div>
</div>


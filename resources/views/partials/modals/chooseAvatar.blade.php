<div class="modalContent modalBlock dark" id="changeAvatar" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/user.png">change avatar</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <ul class="iconSelector">
                    <li><a data-avatar="avatar1" style="background-image: url('/images/avatar_icons/avatar1.png')"></a></li>
                    <li><a data-avatar="avatar2" style="background-image: url('/images/avatar_icons/avatar2.png')"></a></li>
                    <li><a data-avatar="avatar3" style="background-image: url('/images/avatar_icons/avatar3.png')"></a></li>
                    <li><a data-avatar="avatar4" style="background-image: url('/images/avatar_icons/avatar4.png')"></a></li>
                    <li><a data-avatar="avatar5" style="background-image: url('/images/avatar_icons/avatar5.png')"></a></li>
                    <li><a data-avatar="avatar6" style="background-image: url('/images/avatar_icons/avatar6.png')"></a></li>
                    <li><a data-avatar="avatar7" style="background-image: url('/images/avatar_icons/avatar7.png')"></a></li>
                    <li><a data-avatar="avatar8" style="background-image: url('/images/avatar_icons/avatar8.png')"></a></li>
                    <li><a data-avatar="avatar9" style="background-image: url('/images/avatar_icons/avatar9.png')"></a></li>
                    <li><a data-avatar="avatar10" style="background-image: url('/images/avatar_icons/avatar10.png')"></a></li>
                    <li><a data-avatar="avatar11" style="background-image: url('/images/avatar_icons/avatar11.png')"></a></li>
                    <li><a data-avatar="avatar12" style="background-image: url('/images/avatar_icons/avatar12.png')"></a></li>
                    <li><a data-avatar="avatar13" style="background-image: url('/images/avatar_icons/avatar13.png')"></a></li>
                    <li><a data-avatar="avatar14" style="background-image: url('/images/avatar_icons/avatar14.png')"></a></li>
                    <li><a data-avatar="avatar15" style="background-image: url('/images/avatar_icons/avatar15.png')"></a></li>
                    <li><a data-avatar="avatar16" style="background-image: url('/images/avatar_icons/avatar16.png')"></a></li>
                    <li><a data-avatar="avatar17" style="background-image: url('/images/avatar_icons/avatar17.png')"></a></li>
                    <li><a data-avatar="avatar18" style="background-image: url('/images/avatar_icons/avatar18.png')"></a></li>
                    <li><a data-avatar="avatar19" style="background-image: url('/images/avatar_icons/avatar19.png')"></a></li>
                    <li><a data-avatar="avatar20" style="background-image: url('/images/avatar_icons/avatar20.png')"></a></li>
                    <li><a data-avatar="avatar21" style="background-image: url('/images/avatar_icons/avatar21.png')"></a></li>
                    <li><a data-avatar="avatar22" style="background-image: url('/images/avatar_icons/avatar22.png')"></a></li>
                    <li><a data-avatar="avatar23" style="background-image: url('/images/avatar_icons/avatar23.png')"></a></li>
                    <li><a data-avatar="avatar24" style="background-image: url('/images/avatar_icons/avatar24.png')"></a></li>
                    <li><a data-avatar="avatar25" style="background-image: url('/images/avatar_icons/avatar25.png')"></a></li>
                    <li><a data-avatar="avatar26" style="background-image: url('/images/avatar_icons/avatar26.png')"></a></li>
                    <li><a data-avatar="avatar27" style="background-image: url('/images/avatar_icons/avatar27.png')"></a></li>
                    <li><a data-avatar="avatar28" style="background-image: url('/images/avatar_icons/avatar28.png')"></a></li>
                    <li><a data-avatar="avatar29" style="background-image: url('/images/avatar_icons/avatar29.png')"></a></li>
                    <li><a data-avatar="avatar30" style="background-image: url('/images/avatar_icons/avatar30.png')"></a></li>
                    <li><a data-avatar="avatar31" style="background-image: url('/images/avatar_icons/avatar31.png')"></a></li>
                    <li><a data-avatar="avatar32" style="background-image: url('/images/avatar_icons/avatar32.png')"></a></li>
                    <li><a data-avatar="avatar33" style="background-image: url('/images/avatar_icons/avatar33.png')"></a></li>
                    <li><a data-avatar="avatar34" style="background-image: url('/images/avatar_icons/avatar34.png')"></a></li>
                    <li><a data-avatar="avatar35" style="background-image: url('/images/avatar_icons/avatar35.png')"></a></li>
                    <li><a data-avatar="avatar36" style="background-image: url('/images/avatar_icons/avatar36.png')"></a></li>
                    <li><a data-avatar="avatar37" style="background-image: url('/images/avatar_icons/avatar37.png')"></a></li>
                    <li><a data-avatar="avatar38" style="background-image: url('/images/avatar_icons/avatar38.png')"></a></li>
                    <li><a data-avatar="avatar39" style="background-image: url('/images/avatar_icons/avatar39.png')"></a></li>
                    <li><a data-avatar="avatar40" style="background-image: url('/images/avatar_icons/avatar40.png')"></a></li>
                    <li><a data-avatar="avatar41" style="background-image: url('/images/avatar_icons/avatar41.png')"></a></li>
                    <li><a data-avatar="avatar42" style="background-image: url('/images/avatar_icons/avatar42.png')"></a></li>
                    <li><a data-avatar="avatar43" style="background-image: url('/images/avatar_icons/avatar43.png')"></a></li>
                    <li><a data-avatar="avatar44" style="background-image: url('/images/avatar_icons/avatar44.png')"></a></li>
                    <li><a data-avatar="avatar45" style="background-image: url('/images/avatar_icons/avatar45.png')"></a></li>
                    <li><a data-avatar="avatar46" style="background-image: url('/images/avatar_icons/avatar46.png')"></a></li>
                    <li><a data-avatar="avatar47" style="background-image: url('/images/avatar_icons/avatar47.png')"></a></li>
                    <li><a data-avatar="avatar48" style="background-image: url('/images/avatar_icons/avatar48.png')"></a></li>
                    <li><a data-avatar="avatar49" style="background-image: url('/images/avatar_icons/avatar49.png')"></a></li>
                    <li><a data-avatar="avatar50" style="background-image: url('/images/avatar_icons/avatar50.png')"></a></li>
                    <li><a data-avatar="avatar51" style="background-image: url('/images/avatar_icons/avatar51.png')"></a></li>
                    <li><a data-avatar="avatar52" style="background-image: url('/images/avatar_icons/avatar52.png')"></a></li>
                    <li><a data-avatar="avatar53" style="background-image: url('/images/avatar_icons/avatar53.png')"></a></li>
                    <li><a data-avatar="avatar54" style="background-image: url('/images/avatar_icons/avatar54.png')"></a></li>
                    <li><a data-avatar="avatar55" style="background-image: url('/images/avatar_icons/avatar55.png')"></a></li>
                </ul> 
                <div class="buttonArray">
                    <button class="cyan centered" id="iconModalSave"><div class="icon icon-retweet"></div> save icon</button>
                </div> 
            </section>      
        </div>
    </div> 
</div>

<script>
function setStartingAvatar(){
    var current_avatar_number = $('#currentAvatarNumber').val();
    if(typeof current_avatar_number !== 'undefined'){
        $('#changeAvatar').find('a[data-avatar="' + current_avatar_number +'"').addClass('selected');
    }
}

setStartingAvatar();

$('#iconModalSave').on('click', function(){
    var avatar = $('.iconSelector').find('a.selected').data('avatar');
    var avatar_number = avatar.replace('avatar', '');

    var color = $('.profileAvatarColor').css('background-color');
    console.log('color is set');
    createAvatar(color, avatar);

});

function createAvatar(color, icon_number){
    console.log('in create avatar');
    console.log(color);
    var canvas = document.createElement("canvas");
    canvas.width = "128";
    canvas.height = "128";

    var ctx = canvas.getContext("2d");
    ctx.fillStyle = color;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    //making sure the avatar background color and the avatar div have the same color
    $('.profileAvatarColor').css('background-color', color);

    var img = document.createElement("img");
    img.setAttribute("src", "/images/avatar_icons/" + icon_number + ".png");
    //img.style.color = color;

    img.onload = function(){
        ctx.drawImage(img, 0, 0);
        img = canvas.toDataURL('image/png');
        $.ajax({
            type: 'POST',
            url: '/zengarden/createAvatar',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                user_id: {{ $user->id }},
                img: img,
                avatar_number: icon_number,
                color: color
            },
            success: function(data){
                console.log('createAvatar');
                modal.close();
                setAvatarOnPage(color, icon_number);
                console.log(data);
            },
            error: function(data){
                alert('error');
                console.log(data);
            }
        })

    }
}

function setAvatarOnPage(color, avatar_number){
    console.log(color);
    $('.profileAvatarColor').css('background-color', color);
    if($('.iconSelector').find('a.selected').length > 0){
        $('.avatarIcon').css('background-image', "url('/images/avatar_icons/" + avatar_number + ".png')");
        $('.avatarMenu-btn').css('background-color', color);
        $('.avatarMenu-btn').css('background-image', "url('/images/avatar_icons/" + avatar_number + ".png')");   
    }

}

$('.iconSelector').on('click', 'a', function(){
    $('.iconSelector').find('a').removeClass('selected');
    $(this).addClass('selected');
});    
</script>


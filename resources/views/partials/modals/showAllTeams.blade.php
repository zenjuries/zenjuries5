<div class="modalContent modalBlock dark" id="show_teams" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/team.png">select team</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
            <div id="teamList" class="buttonCloud">
                <button class="selectTeam">default team -2/10</button>
                <button class="selectTeam">front house team -5/10</button>
                <button class="selectTeam">warehouse team -3/10</button>
                <button class="selectTeam">office team -2/10</button>
                <button class="selectTeam">secondary team -2/10</button>
                <button class="selectTeam">team6 -4/10</button>
                <button class="selectTeam">team7 -2/10</button>
                <button class="selectTeam">team8 -5/10</button>
                <button class="selectTeam">team9 -4/10</button>
                <button class="selectTeam">team10 -6/10</button>
                <button class="selectTeam">team11 -3/10</button>
            </div>       
        </section>  
        <div class="buttonArray">
            <button id="showInjuryTeams" class="cyan centered">Show Injury Teams</button>
            <button id="showCompanyTeams" class="red centered" style="display: none">Show Teams</button>
        </div>                    
    </div> 
</div>
<div class="modalContent modalBlock dark" id="pushalertsModal" style="display: none;">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/mobile.png"> mobile notifications</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <section class="formBlock dark">
                    <div class="modalDescription">base notifications</div>                          
                    <div class="formGrid short">
                        <div class="formInput"> 
                            <input class="green" id="basePushEnabled" name="basePush" type="radio" value="1" @if($user->notification_priority_1) checked @endif/><label for="basePushEnabled">enable</label>

                            <input class="red" id="basePushDisabled" name="basePush" type="radio" value="0" @if(!$user->notification_priority_1) checked @endif/><label for="basePushDisabled">disable</label>
                        </div> 
                    </div>
                    <div class="modalDescription">additional notifications</div> 
                    <div class="formGrid short">   
                        <div class="formInput">
                            <input class="green" id="additionalPushEnabled" type="radio" name="additionalPush" value="1" @if($user->notification_priority_2) checked @endif/><label for="additionalPushEnabled">enable</label>
                            
                            <input class="red" id="additionalPushDisabled" type="radio" name="additionalPush" value="0" @if(!$user->notification_priority_2) checked @endif/><label for="additionalPushDisabled">disable</label>
                        </div>
                    </div>
                    <div class="modalDescription">all communications</div> 
                    <div class="formGrid short">  
                        <div class="formInput">
                            <input class="green" id="allcommsPushEnabled" type="radio" name="allcommsPush" value="1" @if($user->notification_priority_3) checked @endif/><label for="allcommsPushEnabled">enable</label>

                            
                            <input class="red" id="allcommsPushDisabled" type="radio" name="allcommsPush" value="0" @if(!$user->notification_priority_3) checked @endif/><label for="allcommsPushDisabled">disable</label>
                        </div>
                    </div>
                    <div class="buttonArray">
						<button class="cyan centered" id="submitPushSettings"><div class="icon icon-mobile"></div> set notifications</button>
					</div>  
                </section>                  
            </section>           
        </div>
    </div> 
</div>

<script>
$('#submitPushSettings').on('click', function(){
    var basePushVar = parseInt($("input[name='basePush']:checked").val());
    var additionalPushVar = parseInt($("input[name='additionalPush']:checked").val());
    var allcommsPushVar = parseInt($("input[name='allcommsPush']:checked").val());
    var valid = true;
    
    if(basePushVar === ""){
        valid = false;
        $('#basePushEnabled').parent().find('.inputError').addClass('show');
    }
    if(additionalPushVar === ""){
        valid = false;
        $('#additionalPushEnabled').parent().find('.inputError').addClass('show');
    }
    if(allcommsPushVar === ""){
        valid = false;
        $('#allcommsPushEnabled').parent().find('.inputError').addClass('show');
    }

    if(valid === true){
        $.ajax({
            type: 'POST',
            url: '{{ route("updatePushNotificationSettings") }}',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                user_id: {{ $user->id }},
                basePush: basePushVar,
                additionalPush: additionalPushVar,
                allcommsPush: allcommsPushVar
            },
            success: function (data){
                if(basePushVar === 1){
                    basePushVar = "enabled";
                }else{
                    basePushVar= "disabled";
                }
                if(additionalPushVar === 1){
                    additionalPushVar = "enabled";
                }else{
                    additionalPushVar = "disabled";
                }
                if(allcommsPushVar === 1){
                    allcommsPushVar = "enabled";
                }else{
                    allcommsPushVar = "disabled";
                }
                //communication page section
                $('#basePushNotifications').html(basePushVar);
                $('#additionalPushNotifications').html(additionalPushVar);
                $('#allPushNotifications').html(allcommsPushVar)
                modal.close();
                showAlert("Communication Settings Updated!", "confirm", 5);
            },
            error: function(data){
                console.log(data);
                var errors = data.responseJSON;
                console.log(errors);
                showAlert("Sorry, something went wrong. Please try again later.", "deny", 5);
            }
        })
    }
});
</script>    

    
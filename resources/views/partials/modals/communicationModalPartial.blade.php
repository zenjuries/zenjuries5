<div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/email.png">communication settings</span></div>
    <div class="modalBody" style="min-width:260px;">
        <div class="modalContent">
            <section class="sectionPanel dark"> 
                <section class="formBlock dark">
                    <div class="modalDescription">enable weekly updates</div>                          
                    <div class="formGrid short">
                        <div class="formInput"> 
                            <input class="green" id="weeklyUpdateEnabled" name="weeklyUpdate" type="radio"  value="1" @if($user->gets_weekly_summary) checked @endif/><label for="weeklyUpdateEnabled">enable</label>

                            <input class="red" id="weeklyUpdateDisabled" name="weeklyUpdate" type="radio" value="0" @if(!$user->gets_weekly_summary) checked @endif/><label for="weeklyUpdateDisabled">disable</label>
                        </div> 
                    </div>
                    <div class="modalDescription">enable priority 2 emails (major updates on claims and account)</div> 
                    <div class="formGrid short">   
                        <div class="formInput">
                            <input class="green" id="priority2SettingEnabled" type="radio" name="priority2Email" value="1" @if($user->gets_priority_2_emails) checked @endif/><label for="priority2SettingEnabled">enable</label>
                            
                            <input class="red" id="priority2SettingDisabled" type="radio" name="priority2Email" value="0" @if(!$user->gets_priority_2_emails) checked @endif/><label for="priority2SettingDisabled">disable</label>
                        </div>
                    </div>
                    <div class="modalDescription">enable priority 3 emails  (minor updates on claims and account)</div> 
                    <div class="formGrid short">  
                        <div class="formInput">
                            <input class="green" id="priority3SettingEnabled" type="radio" name="proprity3Email" value="1" @if($user->gets_priority_3_emails) checked @endif/><label for="priority3SettingEnabled">enable</label>

                            
                            <input class="red" id="priority3SettingDisabled" type="radio" name="proprity3Email" value="0" @if(!$user->gets_priority_3_emails) checked @endif/><label for="priority3SettingDisabled">disable</label>
                        </div>
                    </div>
                    <div class="buttonArray">
						<button class="cyan centered" id="submitCommunicationChange"><div class="icon icon-retweet"></div> update</button>
					</div>  
                </section>                  
            </section>           
        </div>
    </div> 
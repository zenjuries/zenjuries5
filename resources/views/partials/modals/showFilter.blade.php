<div class="modalContent modalBlock dark" id="show_filter" style="display: none">
    <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/filter.png">filter results</span></div>
	<div class="modalBody" style="min-width:260px;">
		<section class="sectionPanel dark"> 
            <section class="formBlock dark">
            <p>select items below to filter</p> 
                <div class="formTable">  
                    <div class="formTable__cell w50 filterList">
                        <div><input type="checkbox" name="a"><label for="email">a filter item</label></div>
                        <div><input type="checkbox" name="a"><label for="email">something here</label></div>
                        <div><input type="checkbox" name="a"><label for="email">small</label></div>
                        <div><input type="checkbox" name="a"><label for="email">bigger thing</label></div>
                        <div><input type="checkbox" name="a"><label for="email">really big</label></div>
                        <div><input type="checkbox" name="a"><label for="email">hello</label></div>

                    </div>

                    <div class="formTable__cell w50 filterList">
                        <div><input type="checkbox" name="c"><label for="phone">another item</label></div>
                        <div><input type="checkbox" name="c"><label for="phone">a banana</label></div>
                        <div><input type="checkbox" name="c"><label for="phone">click me</label></div>
                        <div><input type="checkbox" name="c"><label for="phone">a thing here</label></div>
                        <div><input type="checkbox" name="c"><label for="phone">do this</label></div>
                        <div><input type="checkbox" name="c"><label for="phone">here it is</label></div>

                    </div>                                           
                </div>
                <br>
                <div class="buttonArray">
                    <button class="filter centered">set filter</button><button class="red centered">clear</button>
                </div>                                       
            </section>                 
        </section>  
    </div> 
</div>

<script>
var number_per_page = 10;
var page_count = 0;
var current_page = 1;
var max_page_number = 0;
var injuries = null;
$(document).ready(function () {
    $.ajax({
        type: 'POST',
		url: '<?php echo route('getCaseloadInjuries'); ?>',
        async: false,
		data: {
			_token: '<?php echo csrf_token(); ?>',
		},
		success: function(data){
		    console.log(data);
            injuries = data;
            buildCaseloadTable(false);
        },
        error: function(jqxhr, status, exception){
            console.log(jqxhr);
        }
    });
});

function buildCaseloadTable(revert_to_page_one){
    var heading_html = $('#myCaseListHeading').prop('outerHTML');
    html = heading_html;
    /*
    var html = '<tr id="myCaseListHeading" class="tableHeadings">' +
					'<td></td>' +
					'<td class="name"><button class="caseloadSorter sorter showIconModal">name</button></td>' +
					'<td class="injurydate"><button class="caseloadSorter sorter showIconModal">date</button></td>' +
					'<td class="severity"><button class="caseloadSorter sorter showIconModal">severity</button></td>' +
					'<td class="claimNumber"><button class="caseloadSorter sorter showIconModal">claim number</button></td>' +
                    '<td class="policyholder"><button class="caseloadSorter sorter showIconModal">policyholder</button></td>' +
					'<td class="mood"><button class="caseloadSorter sorter showIconModal">mood</button></td>' +
				'</tr>';
    */
    //PAGINATION
    if(revert_to_page_one === true){
        current_page = 1;
    }
    var results_count = injuries.length;
    //calculate how many results to show per page
    var page_length = number_per_page;
     //if there are less results than the default number_per_page, set it to the results count
    if(page_length > results_count){
        page_length = results_count;
    }

            //i will contain the number of messages needed to skip through to get to the current page
            //for example, if on page 2, we'll want to skip to the 11th message to display it
            //so i will equal 10 if on the 2nd page
    i = 0;
    for (var j = 1; j < current_page; j++){
        //add the number shown per page to i each time we advance a page
        i = i + number_per_page;
        //add to the page length
        page_length = page_length + number_per_page;
        //make sure page_length doesn't exceed the number of messages
        if(page_length > results_count){
            page_length = results_count;
        }
    }

    page_count = Math.ceil(results_count / number_per_page);
	$('#myCaseloadpageInfoSpan').html("page " + current_page + " of " + page_count);
    
    for(i; i < page_length; i++){
        var claim_number = injuries[i].claim_number;
        if(claim_number === null){
            claim_number = "-enter #-";
        }

        //mood
        var mood = injuries[i].mood;
        if(mood === null){
            mood = "unknown";
        }else{
            mood = "lv" + mood;
        }

        var mood_label = injuries[i].mood;
        if(mood_label === null){
            mood_label = "-";
		}
        var mood_color = "";
        var mood_string ='<div class="moodVisual scale60"><div class="currentMood"></div></div>';

        if(mood_label === '10'){
            mood_label = "10 - Awesome";
            mood_color = "mood-10";
        }else if(mood_label === '9'){
            mood_label = "9 - Terrific";
            mood_color = "mood-9";
        }else if(mood_label === '8'){
            mood_label = "8 - Great";
            mood_color = "mood-8";
        }else if(mood_label === '7'){
            mood_label = "7 - Good";
            mood_color = "mood-7";
        }else  if(mood_label === '6'){
            mood_label = "6 - Ok";
            mood_color = "mood-6";
        }else if(mood_label === '5'){
            mood_label = "5 - Alright";
            mood_color = "mood-5";
        }else if(mood_label === '4'){
            mood_label = "4 - Uncomfortable";
            mood_color = "mood-4";
        }else if(mood_label === '3'){
            mood_label = "3 - Not Good";
            mood_color = "mood-3";
        }else if(mood_label === '2'){
            mood_label = "2 - Unwell";
            mood_color = "mood-2";
        }else if(mood_label === '1'){
            mood_label = "1 - Bad";
            mood_color = "mood-1";
        }else if(mood_label === '0'){
            mood_label = "0 - Terrible";
            mood_color = "mood-0";
        }else if(mood_label === "x"){
            mood_label = "Dead";
            mood_color = "mood-x";
        }else{
            mood_label = "Unknown";
            mood_color = "unknown";
            mood_string = "-?-";
        }

        var duration_color = '';
        if(injuries[i].days >= 30){
            duration_color = 'critical';
        }else if(injuries[i].days >= 10 && injuires[i].days < 30){
            duration_color = 'alert';
        }

        html += '<tr class="injuryRow" data-id="'+injuries[i].id+'">'+
        '<td><div class="progressMeter '+duration_color+' small inline status'+injuries[i].status_number+'"><svg viewBox="0 0 100 100"><circle class="pm" r="50%" cx="50%" cy="50%"></circle><div class="overlay"><div class="avatar" style="background-image:url(' + injuries[i].user_pic_location +'); background-color: ' + injuries[i].user_avatar_color + '"></div></div></svg></div></td>' +
        '<td class="name"><span>' + injuries[i].user_name + '</span></td>' +
        '<td class="injurydate"><span>' + injuries[i].injury_date + '</span></td>' +
        '<td class="severity"><span class="'+injuries[i].severity+'"></span></td>' +
        '<td class="claim"><span>' + claim_number + '</span></td>' +
        '<td class="policyholder"><span>' + injuries[i].company_name + '</span></td>' +
        '<td class="mood myMood '+mood_color+'"><span>'+ mood_string +'</span></td>' +
        '<tr class="tablerowspace"><td></td></tr>';
    }

    $('#caseloadInjuryDropdown').html(html);


}

$('#caseloadInjuryDropdown').on('click', '.sorter', function() {
    var type = $(this).parent().attr('class');
    if ($(this).hasClass('down')) {
        var sort = "up";
        $('.sorter').removeClass('down').removeClass('up');
        $(this).removeClass('down').addClass('up');
    } else {
        sort = "down";
        $('.sorter').removeClass('down').removeClass('up');
        $(this).removeClass('up').addClass('down');
    }


    console.log(type);
    if (type === "name") {
        if (sort === "up") {
            sortInjuriesByNameAsc(injuries);
        } else if (sort === "down") {
            sortInjuriesByNameDesc(injuries);
        }
    } else if (type === "injurydate") {
        if (sort === "up") {
            sortByDateAsc(injuries);
        } else if (sort === "down") {
            sortByDateDesc(injuries);
        }
    } else if (type === "severity") {
        if (sort === "up") {
            sortBySeverityAsc(injuries);
        } else if (sort === "down") {
            sortBySeverityDesc(injuries);
        }
    } else if (type === "mood") {
        if (sort === "up") {
            sortByMoodAsc(injuries);
        } else if (sort === "down") {
            sortByMoodDesc(injuries);
        }
    } else if (type === "claimNumber") {
        if (sort === "up") {
            sortByClaimNumAsc(injuries);
        } else if (sort === "down") {
            sortByClaimNumDesc(injuries);
        }
    } else if (type === "policyholder") {
        if (sort === "up") {
            sortByPolicyholderAsc(injuries);
        } else if (sort === "down") {
            sortByPolicyholderDesc(injuries);
        }
    } 

});

function setButtonClasses(){
    if(current_page === 1){
        //first page
        $('.pagePrev').attr('class', 'pagePrev ghost');
        $('.pageNext').attr('class', 'pageNext');
    }else if(current_page === page_count){
        //last page
        $('.pagePrev').attr('class', 'pagePrev');
        $('.pageNext').attr('class', 'pageNext ghost');
    }else{
        //middle pages
        $('.pagePrev').attr('class', 'pagePrev');
        $('.pageNext').attr('class', 'pageNext');
    }
}

$('.pagePrev').on('click', function(){
    if(!$(this).hasClass('ghost')){
        var cur_page = current_page;
        cur_page = cur_page - 1;
        if(cur_page > 0){
            current_page = cur_page;
            buildCaseloadTable(false);
        }
        setButtonClasses();
	}
});

$('.pageNext').on('click', function(){
    if(!$(this).hasClass('ghost')){
        var cur_page = current_page;
        cur_page = cur_page + 1;
        if(cur_page <= page_count){
            current_page = cur_page;
            buildCaseloadTable(false);
        }
        setButtonClasses();
	}
});

function sortInjuriesByNameDesc(injury_array){

function compare(a, b) {
    if(a.user_name.toLowerCase() < b.user_name.toLowerCase()){
        return -1;
    }
    if(a.user_name.toLowerCase() > b.user_name.toLowerCase()){
        return 1;
    }

    return 0;
}

injury_array.sort(compare);
buildCaseloadTable(true);
}

function sortInjuriesByNameAsc(injury_array){

function compare(a, b) {
    if(a.user_name.toLowerCase() < b.user_name.toLowerCase()){
        return 1;
    }
    if(a.user_name.toLowerCase() > b.user_name.toLowerCase()){
        return -1;
    }

    return 0;
}

injury_array.sort(compare);
buildCaseloadTable(true);
}

function sortByDateDesc(injury_array){
function compare(a, b){
    var a = new Date(a.injury_date);
    var b = new Date(b.injury_date);
    if(a < b){
        return -1;
    }
    if(a > b){
        return 1;
    }
    return 0;
}
injury_array.sort(compare);
buildCaseloadTable(true);
}

function sortByDateAsc(injury_array){
function compare(a, b){
    var a = new Date(a.injury_date);
    var b = new Date(b.injury_date);
    if(a < b){
        return 1;
    }
    if(a > b){
        return -1;
    }
    return 0;
}
injury_array.sort(compare);
buildCaseloadTable(true);
}

function sortByCurrentCostDesc(injury_array){
function compare(a, b){
    if(a.current_cost < b.current_cost){
        return -1;
    }
    if(a.current_cost > b.current_cost){
        return 1;
    }
    return 0;
}
injury_array.sort(compare);
buildCaseloadTable(true);
}

function sortByCurrentCostAsc(injury_array){
function compare(a, b){
    if(a.current_cost < b.current_cost){
        return 1;
    }
    if(a.current_cost > b.current_cost){
        return -1;
    }
    return 0;
}
injury_array.sort(compare);
buildCaseloadTable(true);
}

function sortByReserveCostDesc(injury_array){
function compare(a, b){
    if(a.reserve_cost < b.reserve_cost){
        return -1;
    }
    if(a.reserve_cost > b.reserve_cost){
        return 1;
    }
    return 0;
}
injury_array.sort(compare);
console.log(injury_array);
buildCaseloadTable(true);
}

function sortByReserveCostAsc(injury_array){
function compare(a, b){
    if(a.reserve_cost < b.reserve_cost){
        return 1;
    }
    if(a.reserve_cost > b.reserve_cost){
        return -1;
    }
    return 0;
}
injury_array.sort(compare);
console.log(injury_array);
buildCaseloadTable(true);
}

function sortBySeverityDesc(injury_array){
function compare(a, b){
    if(a.severity < b.severity){
        return -1;
    }
    if(a.severity > b.severity){
        return 1;
    }
    return 0;
}
injury_array.sort(compare);
console.log('sort by severity desc');
console.log(injury_array);
buildCaseloadTable(true);
}

function sortBySeverityAsc(injury_array){
function compare(a, b){
    if(a.severity < b.severity){
        return 1;
    }
    if(a.severity > b.severity){
        return -1;
    }
    return 0;
}
injury_array.sort(compare);
console.log('sort by severity asc');
console.log(injury_array);
buildCaseloadTable(true);
}

function sortByProgressDesc(injury_array){
function compare(a, b){
    if(a.employee_status == null){
        return -1;
    }
    else if(b.employee_status == null){
        return 1;
    }
    else if(a.employee_status < b.employee_status){
        return 1;
    }
    else if(a.employee_status > b.employee_status){
        return -1;
    }
}
injury_array.sort(compare);
console.log('sort by progress desc');
console.log(injury_array);
buildCaseloadTable(true);
}

function sortByProgressAsc(injury_array){
function compare(a, b){
    if(a.employee_status == null){
        return 1;
    }
    else if(b.employee_status == null){
        return -1;
    }
    else if(a.employee_status < b.employee_status){
        return -1;
    }
    else if(a.employee_status > b.employee_status){
        return 1;
    }
}
injury_array.sort(compare);
console.log('sort by progress asc');
console.log(injury_array);
buildCaseloadTable(true);
}

function sortByClaimNumberDesc(injury_array){
function compare(a, b){
    a = a.claim_number;
    b = b.claim_number;
    if(a === null){
        a = 0;
    }
    if(b === null){
        b = 0;
    }
    if(a < b){
        return -1;
    }
    if(a > b){
        return 1;
    }
    return 0;
}

injury_array.sort(compare);
console.log(injury_array);
buildCaseloadTable(true);
}

function sortByClaimNumberAsc(injury_array){
function compare(a, b){
    a = a.claim_number;
    b = b.claim_number;
    if(a === null){
        a = 0;
    }
    if(b === null){
        b = 0;
    }
    if(a < b){
        return 1;
    }
    if(a > b){
        return -1;
    }
    return 0;
}
injury_array.sort(compare);
console.log('injury array - ' + injury_array);
buildCaseloadTable(true);
}

function sortByStatusDesc(injury_array){
function compare(a, b){
    if(a.status_number < b.status_number){
        return -1;
    }
    if(a.status_number > b.status_number){
        return 1;
    }
    return 0;
}
injury_array.sort(compare);
buildCaseloadTable(true);
}

function sortByStatusAsc(injury_array){
function compare(a, b){
    if(a.status_number < b.status_number){
        return 1;
    }
    if(a.status_number > b.status_number){
        return -1;
    }
    return 0;
}
injury_array.sort(compare);
buildCaseloadTable(true);
}

function sortByMoodDesc(injury_array){
function compare(a, b){
    if(a.mood < b.mood){
        return -1;
    }
    if(a.mood > b.mood){
        return 1;
    }
    return 0;
}
injury_array.sort(compare);
buildCaseloadTable(true);
}

function sortByMoodAsc(injury_array){
function compare(a, b){
    if(a.mood < b.mood){
        return 1;
    }
    if(a.mood > b.mood){
        return -1;
    }
    return 0;
}
injury_array.sort(compare);
buildCaseloadTable(true);
}

function sortByStateDesc(injury_array){
function compare(a, b){
    if(a.resolved < b.resolved){
        return -1;
    }
    if(a.resolved > b.resolved){
        return 1;
    }
    return 0;
}
injury_array.sort(compare);
buildCaseloadTable(true);
}

function sortByStateAsc(injury_array){
function compare(a, b){
    if(a.resolved < b.resolved){
        return 1;
    }
    if(a.resolved > b.resolved){
        return -1;
    }
    return 0;
}
injury_array.sort(compare);
buildCaseloadTable(true);
}

//use this function to deterimine what the colspan for filler table rows should be, based on how many columns are currently visible
function getColspanValue(){
var count = 2;
$('.visibilityToggle').each(function(){
    if(!$(this).hasClass('hide')){
        count++;
    }
});
return count;
}

function getInjuryIndexById(id){
for(var i = 0; i < injuries.length; i++){
    if(id == injuries[i].id){
        return i;
    }
}
return 0;
}

function sortByClaimNumDesc(injury_array){
function compare(a, b){
    a = a.claim_number;
    b = b.claim_number;
    if(a === null){
        a = 0;
    }
    if(b === null){
        b = 0;
    }
    if(a < b){
        return -1;
    }
    if(a > b){
        return 1;
    }
    return 0;
}
injury_array.sort(compare);
buildCaseloadTable(true);
}

function sortByClaimNumAsc(injury_array){
function compare(a, b){
    a = a.claim_number;
    b = b.claim_number;
    if(a === null){
        a = 0;
    }
    if(b === null){
        b = 0;
    }
    if(a < b){
        return 1;
    }
    if(a > b){
        return -1;
    }
    return 0;
}
injury_array.sort(compare);
buildCaseloadTable(true);
}

function sortByPolicyholderDesc(injury_array){
function compare(a, b){
    if(a.company_name.toLowerCase() < b.company_name.toLowerCase()){
        return -1;
    }
    if(a.company_name.toLowerCase() > b.company_name.toLowerCase()){
        return 1;
    }

    return 0;
}
injury_array.sort(compare);
buildCaseloadTable(true);
}

function sortByPolicyholderAsc(injury_array){
function compare(a, b){
    if(a.company_name.toLowerCase() < b.company_name.toLowerCase()){
        return 1;
    }
    if(a.company_name.toLowerCase() > b.company_name.toLowerCase()){
        return -1;
    }

    return 0;
}
injury_array.sort(compare);
buildCaseloadTable(true);
}


$('#caseloadInjuryDropdown').on('click', '.injuryRow', function(){
    var id = $(this).data('id');
    window.location.href = "/zencare/" + id;
});

</script>
<script>
    var unresolved_injuries = null;
	var unresolved_page_count = 0;
	var unresolved_current_page = 1;
	var unresolved_number_per_page = 10;
	var unresolved_max_page_number = 0;
    getUnresolvedList();

$('.zenListTable').on('click', '.injuryRow', function(){
    var id = $(this).data('id');
    window.location.href = "/zencare/" + id;
});

function setButtonClasses(){
 if(unresolved_current_page === 1){
     //first page
     $('#prevUnresolved').attr('class', 'pagePrev ghost');
     $('#nextUnresolved').attr('class', 'pageNext');
 }else if(unresolved_current_page === unresolved_page_count){
     //last page
     $('#prevUnresolved').attr('class', 'pagePrev');
     $('#nextUnresolved').attr('class', 'pageNext ghost');
 }else{
     //middle pages
     $('#prevUnresolved').attr('class', 'pagePrev');
     $('#nextUnresolved').attr('class', 'pageNext');
 }
}

$('#prevUnresolved').on('click', function(){
 if(!$(this).hasClass('ghost')){
     var cur_page = unresolved_current_page;
     cur_page = cur_page - 1;
     if(cur_page > 0){
        unresolved_current_page = cur_page;
         buildUnresolvedTable(false);
     }
     setButtonClasses();
 }
});

$('#nextUnresolved').on('click', function(){
 if(!$(this).hasClass('ghost')){
     var cur_page = unresolved_current_page;
     cur_page = cur_page + 1;
     if(cur_page <= unresolved_page_count){
        unresolved_current_page = cur_page;
         buildUnresolvedTable(false);
     }
     setButtonClasses();
 }
});
$('.unresolvedTable').on('click', '.unresolvedSorter', function(){
	var type = $(this).parent().attr('class');
	if($(this).hasClass('down')){
		var sort = "up";
        $('.unresolvedSorter').removeClass('down').removeClass('up');
		$(this).removeClass('down').addClass('up');
	}else{
		sort = "down";
        $('.unresolvedSorter').removeClass('down').removeClass('up');
		$(this).removeClass('up').addClass('down');
	}


    alert(type);
	if(type === "name"){
        if(sort === "up"){
            sortInjuriesByNameAsc(unresolved_injuries);
        }else if(sort === "down"){
            sortInjuriesByNameDesc(unresolved_injuries);
        }
	}else if(type === "date"){
        if(sort === "up"){
            sortByDateAsc(unresolved_injuries);
        }else if(sort === "down"){
            sortByDateDesc(unresolved_injuries);
        }
	}else if(type === "total"){
        if(sort === "up"){
            sortByCurrentCostAsc(unresolved_injuries);
        }else if(sort === "down"){
            sortByCurrentCostDesc(unresolved_injuries);
        }
	}else if(type === "severity"){
        if(sort === "up"){
            sortBySeverityAsc(unresolved_injuries);
        }else if(sort === "down"){
            sortBySeverityDesc(unresolved_injuries);
        }
	}else if(type === "claim"){
        if(sort === "up"){
            sortByClaimNumberAsc(unresolved_injuries);
        }else if(sort === "down"){
            sortByClaimNumberDesc(unresolved_injuries);
        }
	}else if(type === "mood"){
        if(sort === "up"){
            sortByMoodAsc(unresolved_injuries);
        }else if(sort === "down"){
            sortByMoodDesc(unresolved_injuries);
        }
	}else if(type === "resolved"){
		if(sort === "up"){
			sortByStateAsc(unresolved_injuries);
        }else if(sort === "down"){
			sortByStateDesc(unresolved_injuries);
        }
    }
    else if(type === "progress"){
        if(sort === "up"){
            sortByProgressAsc(unresolved_injuries);
        }else if(sort === "down"){
            sortByProgressDesc(unresolved_injuries);
        }
    }
});

function getUnresolvedList(){
	$.ajax({
		type: 'POST',
		url: '<?php echo route('getUnresolvedInjuryList'); ?>',
		data: {
			_token: '<?php echo csrf_token(); ?>',
			company_id: '<?php echo Auth::user()->getCompany()->id ?>'
		},
		success: function(data){
			unresolved_injuries = data;
            console.log('from call: ' + JSON.stringify(unresolved_injuries));
            if(unresolved_injuries.length <= unresolved_number_per_page){
                $('#nextUnresolved').addClass('ghost');
            }else{
                $('#nextUnresolved').removeClass('ghost');
            }
			sortByStateDesc(unresolved_injuries);
            //showList(injuries);
                if(unresolved_injuries.length > 0){
                    buildUnresolvedTable(true);
                }
                else{
                    $('#prevUnresolved').hide();
                    $('#nextUnresolved').hide();
                    $('.unresolvedSorter').hide();
                    $('#unresolvedPageInfoSpan').html("You are not currently on any injury teams.");
                }
			},
        error: function(jqxhr, status, exception){
            console.log(jqxhr);
        }
	});
}

	function buildUnresolvedTable(revert_to_page_one){
        //console.log("injuries length: " + unresolved_injuries);
        var unresolved_critical_count = 0;
        var unresolved_warning_count = 0;
        var unresolved_ok_count = 0;
		    var i = 0;
            var l = unresolved_injuries.length;
            var html = "";
            var valid = true;
            var colspan = getColspanValue();
            
            //.log("length is: " + l);
            //SEARCH
			//TODO: HOOK UP SEARCH
			var search_string = "";
            //var search_string = $("#claimSearchBar").val();
            var results_array = [];
            if(search_string !== ""){
                for(i = 0; i < l; i++){
                    var show_this_row = false;
                    if(unresolved_injuries[i]['user_name'] !== null && unresolved_injuries[i]['user_name'].toLowerCase().indexOf(search_string.toLowerCase()) !== -1){
                        show_this_row = true;
                    }

                    if(unresolved_injuries[i]['injury_date'] !== null && unresolved_injuries[i]['injury_date'].indexOf(search_string) !== -1){
                        show_this_row = true;
                    }

                    if(unresolved_injuries[i]['claim_number'] !== null && unresolved_injuries[i]['claim_number'].indexOf(search_string) !== -1){
                        show_this_row = true;
                    }
                    if(show_this_row === true){
                        results_array.push(unresolved_injuries[i]);
                    }
                }

            }else{
                results_array = unresolved_injuries;
            }


            //PAGINATION
            if(revert_to_page_one === true){
                unresolved_current_page = 1;
            }
            var results_count = results_array.length;
            //calculate how many results to show per page
            var page_length = unresolved_number_per_page;
            //if there are less results than the default number_per_page, set it to the results count
            if(page_length > results_count){
                page_length = results_count;
            }

            //i will contain the number of messages needed to skip through to get to the current page
            //for example, if on page 2, we'll want to skip to the 11th message to display it
            //so i will equal 10 if on the 2nd page
            i = 0;
            for (var j = 1; j < unresolved_current_page; j++){
                //add the number shown per page to i each time we advance a page
                i = i + unresolved_number_per_page;
                //add to the page length
                page_length = page_length + unresolved_number_per_page;
                //make sure page_length doesn't exceed the number of messages
                if(page_length > results_count){
                    page_length = results_count;
                }
            }

            unresolved_page_count = Math.ceil(results_count / unresolved_number_per_page);
			$('#unresolvedPageInfoSpan').html("page " + unresolved_current_page + " of " + unresolved_page_count);
            ////.log(page_count + " = " + results_count + " / " + number_per_page );

            var heading_html = $('.unresolvedHeadings').prop('outerHTML');
            html = heading_html;

            for(var x=0; x < l; x++){
                if(results_array[x].days >= 30 && results_array[x].resolved === 0){
                    unresolved_critical_count++;
                }else if(results_array[x].days >= 10 && results_array[i].days < 30 && results_array[x].resolved === 0){
                    unresolved_warning_count++;
                }else if(results_array[x].days < 10 && results_array[x].resolved === 0){
                    unresolved_ok_count++;
                }
            }

            for(i; i < page_length; i++){
                var duration_color = '';
                if(results_array[i].days >= 30){
                    duration_color = 'critical';
                }else if(results_array[i].days >= 10 && results_array[i].days < 30){
                    duration_color = 'alert';
                }else if(results_array[i].days < 10){
                }
                //total costs
                var current_cost = results_array[i].reserve_cost +  results_array[i].current_cost;
                if(current_cost === 0 || current_cost === null){
                    current_cost = "$-enter cost-";
                }else{
                    current_cost = "$" + current_cost;
                }

                //mood
                var mood = results_array[i].mood;
                if(mood === null){
                    mood = "unknown";
                }else{
                    mood = "lv" + mood;
                }

                var mood_label = results_array[i].mood;
                if(mood_label === null){
                    mood_label = "-";
				}
                var mood_color = "";
                var mood_string ='<div class="moodVisual scale60"><div class="currentMood"></div></div>';

                if(mood_label === '10'){
                    mood_label = "10 - Awesome";
                    mood_color = "mood-10";
                }else if(mood_label === '9'){
                    mood_label = "9 - Terrific";
                    mood_color = "mood-9";
                }else if(mood_label === '8'){
                    mood_label = "8 - Great";
                    mood_color = "mood-8";
                }else if(mood_label === '7'){
                    mood_label = "7 - Good";
                    mood_color = "mood-7";
                }else  if(mood_label === '6'){
                    mood_label = "6 - Ok";
                    mood_color = "mood-6";
                }else if(mood_label === '5'){
                    mood_label = "5 - Alright";
                    mood_color = "mood-5";
                }else if(mood_label === '4'){
                    mood_label = "4 - Uncomfortable";
                    mood_color = "mood-4";
                }else if(mood_label === '3'){
                    mood_label = "3 - Not Good";
                    mood_color = "mood-3";
                }else if(mood_label === '2'){
                    mood_label = "2 - Unwell";
                    mood_color = "mood-2";
                }else if(mood_label === '1'){
                    mood_label = "1 - Bad";
                    mood_color = "mood-1";
                }else if(mood_label === '0'){
                    mood_label = "0 - Terrible";
                    mood_color = "mood-0";
                }else if(mood_label === "x"){
                    mood_label = "Dead";
                    mood_color = "mood-x";
                }else{
                    mood_label = "Unknown";
                    mood_color = "unknown";
                    mood_string = "-?-";
                }

                //employee status
                if(results_array[i].employee_status === "Hospital"){
                    var employee_status = "hospital";
                    var employee_status_title = "Hospital";
                }else if(results_array[i].employee_status === "Urgent Care"){
                    var employee_status = "urgentcare";
                    var employee_status_title = "Urgent Care";
                }else if(results_array[i].employee_status === "Family Practice"){
                    var employee_status = "doctor";
                    var employee_status_title = "Family Practice";
                }else if(results_array[i].employee_status === "Home"){
                    var employee_status = "home";
                    var employee_status_title = "Home";
                }else if(results_array[i].employee_status === "Light Duty"){
                    var employee_status = "lightduty";
                    var employee_status_title = "Light Duty";
                }else if(results_array[i].employee_status === "Full Duty"){
                    var employee_status = "fullduty";
                    var employee_status_title = "Full Duty";
                }else if(results_array[i].employee_status === "Maximum Medical Improvement"){
                    var employee_status = "max";
                    var employee_status_title = "Maximum Medical Improvement";
                }else if(results_array[i].employee_status === "Terminated"){
                    var employee_status = "terminated";
                    var employee_status_title = "Terminated";
                }else if(results_array[i].employee_status === "Resigned"){
                    var employee_status = "resigned";
                    var employee_status_title = "Resigned";
                }else{
                    var employee_status = "unknown";
                    var employee_status_title = "Unknown";
                }
				
                var severity_color = '';
                if(results_array[i].severity === "First Aid"){
                    severity_color = "var(--severityMild)";
                }else if(results_array[i].severity === "Moderate"){
                    severity_color = "var(--severityModerate)";
                }else{
                    severity_color = "var(--severitySevere)";
                }

				html = html + '<tr class="injuryRow" data-id=' + results_array[i].id +'>'
                            + '<td><div class="progressMeter '+duration_color+' small inline status'+results_array[i].status_number+'"><svg viewBox="0 0 100 100"><circle r="50%" cx="50%" cy="50%"></circle><div class="overlay"><div class="avatar" style="background-image:url(' + results_array[i].user_pic_location +'); background-color: ' + results_array[i].user_avatar_color + '"></div></div></svg></div></td>'
							+ '<td class="name"><span>' + results_array[i].user_name + '</span></td>'
							+ '<td class="injurydate"><span>' + results_array[i].injury_date + '</span></td>'
                            + '<td class="severity"><span class="'+results_array[i].severity+'"></span></td>'
                            + '<td class="total"><span>' + current_cost + '</span></td>'
                            + '<td class="progress"><span>' + employee_status + '</span></td>'
							+ '<td class="mood myMood '+mood_color+'"><span>'+ mood_string +'</span></td>'
							+ '<tr class="tablerowspace"><td></td></tr>';
            }
        
			$('#unresolvedInjuryDropdown').html(html);
            $('#unresolved__ok').html(unresolved_ok_count);
            $('#unresolved__alert').html(unresolved_warning_count);
            $('#unresolved__critical').html(unresolved_critical_count);

	}

//use this function to deterimine what the colspan for filler table rows should be, based on how many columns are currently visible
function getColspanValue(){
    var count = 2;
    $('.visibilityToggle').each(function(){
        if(!$(this).hasClass('hide')){
            count++;
        }
    });
    return count;
}

function sortInjuriesByNameDesc(injury_array){

    function compare(a, b) {
        if(a.user_name.toLowerCase() < b.user_name.toLowerCase()){
            return -1;
        }
        if(a.user_name.toLowerCase() > b.user_name.toLowerCase()){
            return 1;
        }

        return 0;
}

injury_array.sort(compare);
buildUnresolvedTable(true);
}

function sortInjuriesByNameAsc(injury_array){

    function compare(a, b) {
        if(a.user_name.toLowerCase() < b.user_name.toLowerCase()){
            return 1;
        }
        if(a.user_name.toLowerCase() > b.user_name.toLowerCase()){
            return -1;
        }

        return 0;
    }

    injury_array.sort(compare);
    buildUnresolvedTable(true);
}

function sortByDateDesc(injury_array){
    function compare(a, b){
        var a = new Date(a.injury_date);
        var b = new Date(b.injury_date);
        if(a < b){
            return -1;
        }
        if(a > b){
            return 1;
        }
        return 0;
    }
    injury_array.sort(compare);
    buildUnresolvedTable(true);
}

function sortByDateAsc(injury_array){
    function compare(a, b){
        var a = new Date(a.injury_date);
        var b = new Date(b.injury_date);
        if(a < b){
            return 1;
        }
        if(a > b){
            return -1;
        }
        return 0;
    }
    injury_array.sort(compare);
    buildUnresolvedTable(true);
}

function sortByCurrentCostDesc(injury_array){
    function compare(a, b){
        if(a.current_cost < b.current_cost){
            return -1;
        }
        if(a.current_cost > b.current_cost){
            return 1;
        }
        return 0;
    }
    injury_array.sort(compare);
    buildUnresolvedTable(true);
}

function sortByCurrentCostAsc(injury_array){
    function compare(a, b){
        if(a.current_cost + a.reserve_cost < b.current_cost + b.reserve_cost){
            return 1;
        }
        if(a.current_cost + a.reserve_cost > b.current_cost + b.reserve_cost){
            return -1;
        }
        return 0;
    }
    injury_array.sort(compare);
    buildUnresolvedTable(true);
}

function sortBySeverityDesc(injury_array){
    function compare(a, b){
        if(a.severity < b.severity){
            return -1;
        }
        if(a.severity > b.severity){
            return 1;
        }
        return 0;
    }
    injury_array.sort(compare);
    //.log('sort by severity desc');
    //.log(injury_array);
    buildUnresolvedTable(true);
}

function sortBySeverityAsc(injury_array){
    function compare(a, b){
        if(a.severity < b.severity){
            return 1;
        }
        if(a.severity > b.severity){
            return -1;
        }
        return 0;
    }
    injury_array.sort(compare);
    //.log('sort by severity asc');
    //.log(injury_array);
    buildUnresolvedTable(true);
}

function sortByClaimNumberDesc(injury_array){
    function compare(a, b){
        a = a.claim_number;
        b = b.claim_number;
        if(a === null){
            a = 0;
        }
        if(b === null){
            b = 0;
        }
        if(a < b){
            return -1;
        }
        if(a > b){
            return 1;
        }
        return 0;
    }

    injury_array.sort(compare);
    //.log(injury_array);
    buildUnresolvedTable(true);
}

function sortByClaimNumberAsc(injury_array){
    function compare(a, b){
        a = a.claim_number;
        b = b.claim_number;
        if(a === null){
            a = 0;
        }
        if(b === null){
            b = 0;
        }
        if(a < b){
            return 1;
        }
        if(a > b){
            return -1;
        }
        return 0;
    }
    injury_array.sort(compare);
    //.log('injury array - ' + injury_array);
    buildUnresolvedTable(true);
}

function sortByStatusDesc(injury_array){
    function compare(a, b){
        if(a.status_number < b.status_number){
            return -1;
        }
        if(a.status_number > b.status_number){
            return 1;
        }
        return 0;
    }
    injury_array.sort(compare);
    buildUnresolvedTable(true);
}

function sortByStatusAsc(injury_array){
    function compare(a, b){
        if(a.status_number < b.status_number){
            return 1;
        }
        if(a.status_number > b.status_number){
            return -1;
        }
        return 0;
    }
    injury_array.sort(compare);
    buildUnresolvedTable(true);
}

function sortByMoodDesc(injury_array){
    function compare(a, b){
        if(a.mood < b.mood){
            return -1;
        }
        if(a.mood > b.mood){
            return 1;
        }
        return 0;
    }
    injury_array.sort(compare);
    buildUnresolvedTable(true);
}

function sortByMoodAsc(injury_array){
    function compare(a, b){
        if(a.mood < b.mood){
            return 1;
        }
        if(a.mood > b.mood){
            return -1;
        }
        return 0;
    }
    injury_array.sort(compare);
    buildUnresolvedTable(true);
}

function sortByStateDesc(injury_array){
    function compare(a, b){
        if(a.resolved < b.resolved){
            return -1;
        }
        if(a.resolved > b.resolved){
            return 1;
        }
        return 0;
    }
    injury_array.sort(compare);
    buildUnresolvedTable(true);
}

function sortByStateAsc(injury_array){
    function compare(a, b){
        if(a.resolved < b.resolved){
            return 1;
        }
        if(a.resolved > b.resolved){
            return -1;
        }
        return 0;
    }
    injury_array.sort(compare);
    buildUnresolvedTable(true);
}


function sortByProgressDesc(injury_array){
    function compare(a, b){
        if(a.employee_status == null){
            return -1;
        }
        else if(b.employee_status == null){
            return 1;
        }
        else if(a.employee_status < b.employee_status){
            return 1;
        }
        else if(a.employee_status > b.employee_status){
            return -1;
        }
    }
    injury_array.sort(compare);
    buildUnresolvedTable(true);
}

function sortByProgressAsc(injury_array){
    function compare(a, b){
        if(a.employee_status == null){
            return 1;
        }
        else if(b.employee_status == null){
            return -1;
        }
        else if(a.employee_status < b.employee_status){
            return -1;
        }
        else if(a.employee_status > b.employee_status){
            return 1;
        }
    }
    injury_array.sort(compare);
    buildUnresolvedTable(true);
}

    
</script>
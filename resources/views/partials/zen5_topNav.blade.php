<?php
$user = Auth::user();
$theme = $user->zengarden_theme ?: 'ocean';
$theme = str_replace("theme", "", $theme);
$theme = strtolower($theme);
$agency_type = NULL;
$agent = false;
$user_is_zagent = false;
$homeLink = "";
/*
//check if user is zagent
if(Auth::user()->zagent_id !== NULL){
	$user_is_zagent = true;
}
//check for zenpro
if(Auth::user()->type !== "internal"){
        $agent = true;
}
if($user_is_zagent){
	$agent = false;
	$homeLink = "zagent";
}
if($agent){
	$user_is_zagent = false;
	$homeLink = "zenpro";
}
*/
if($user->is_zenpro){
	$user_is_zagent = false;
	$homeLink = "zenpro";
	$agent = true;
}elseif($user->zagent_id !== NULL && $user->is_zenpro === 0){
	$user_is_zagent = true;
	$homeLink = "zagent";
}

if($user->gets_priority_3_emails === 1 && $user->gets_priority_2_emails === 1){
    $alert_settings = "full";

}elseif($user->gets_priority_3_emails === 0 && $user->gets_priority_2_emails === 1){
    $alert_settings = "partial";

}elseif($user->gets_priority_3_emails === 1 && $user->gets_priority_2_emails === 0){
    $alert_settings = "partial";

}else{
	$alert_settings = "none";
}

if(str_contains(Auth::user()->photo_url, 'public')){
	Auth::user()->photo_url = 'images/avatar_icons/avatar0.png';
}
//echo "<pre>user_is_agent: " . $user_is_zagent . " agent: " . $agent . " homelink: " . $homeLink;
?>
<div id="navbar">
	<div id="logo"><a href="/<?php echo $homeLink?>"> </a></div>
	<div id="policyholderSelector">
		@if(!$user_is_zagent && !$agent)
		<div id="policyholderCompany" class="policyholderButton noButton">
		@endif
		@if($agent)
		<div id="policyholderCompany" class="policyholderButton" onclick="location.href='/zenpro_list';">
		@endif
		@if($user_is_zagent)
		<div id="policyholderCompany" class="policyholderButton" onclick="location.href='/zagent_list';">
		@endif
			<div id="policyholderLogo" style="background-image:url({{Auth::user()->getCompany()->logo_location}}?{{ $date = str_replace(' ', '', \Carbon\Carbon::now()) }})"></div>
			<div id="policyholderName">{{Auth::user()->getCompany()->company_name}}</div>
		</div>
	</div>

	<div id="navbar-menu">
		<div id="menuLinks">
			<ul>
			<li id="zenboard"><a class="main zenboard" href="/zenboard"><span class="menuText">dashboard</span></a></li>
			<li id="zenisphere"><a class="main zenisphere" href="/zenisphere"><span class="menuText">users/teams</span></span></a></li>
			<li id="zencare"><a class="main zencare" href="/zencarelist"><span class="menuText">injury list</span></span></a></li>
			@if($user_is_zagent || $agent)
			<li id="profile"><a class="main company" href="/policyholder_profile"><span class="menuText">profile<span></a></li>
			@endif
			@if($agent)	
			<li id="policyholders"><a class="main company" href="/zenpro_list"><span class="menuText">policyholders<span></a></li>
			@endif
			@if($user_is_zagent)	
			<li id="policyholders"><a class="main company" href="/zagent_list"><span class="menuText">policyholders<span></a></li>			
			@endif
			@if($agent)	
			<li id="landing"><a class="main company" href="/zenpro"><span class="menuText">zenPro<span></a></li>
			@endif
			@if($user_is_zagent)	
			<li id="landing"><a class="main company" href="/zagent"><span class="menuText">zagent<span></a></li>			
			@endif		
			</ul>
		</div>	
	</div>
	
<!-- set the status of the user.  Will show icon on the upper right.  userType superadmin/admin/agent/pro -->
	<div id="loggedInStatus" class="userType"></div>
<!-- set the avatarMenu class to add 'injured' to set the user avatar with a RED border, indicating they have an injury -->	
	<div class="avatarMenu @if(Auth::user()->checkInjuredUserForInjuryTeams()) injured @endif" id="avatarMenuBtn">
		<div class="avatarMenu-btn" style="background-color: {{Auth::user()->avatar_color}};background-image:url({{Auth::user()->photo_url}});"></div>
		<div class="avatarMenu-name">{{ Auth::user()->getFirstName(); }}</div>
	</div>	
	<div id="avatarSlideMenu">
		<div class="widgetDock">
			<!-- <a class="menuWidget">
				<div href="/zenrating" class="widget">	
					<span class="leftImage"><img src="/images/icons/zenrating.png"></span>
					<span class="rate lv2">73</span>
				</div>
			</a> -->
			<a class="menuWidget showZenNavModal" data-modalcontent="#communicationModalNav">
				<div class="widget">
					<span class="label">my alerts</span>
					<span class="data" id="alertSetting">{{ $alert_settings }}</span>
					<span class="rightImage"><img src="/images/icons/alert.png"></span>
				</div>
			</a>
			<!--
			<a href="/myinfo" class="menuWidget">
				<div class="widget">
					<span class="label">messages</span>
					<span class="data"><b>0</b> new</span>
					<span class="rightImage"><img src="/images/icons/message.png"></span>
				</div>
			</a>
			-->
			<a href="/myinfo" class="menuWidget">
				<div class="widget">
					<span class="label">my info</span>
					<span class="data">update</span>
					<span class="rightImage"><img src="/images/icons/gear.png"></span>
				</div>
			</a>
			<a class="menuWidget showZenNavModal" data-modalcontent="#navThemeModal">
				<div class="widget">
					<span class="label onClickEvent">theme</span>
					<span class="data onClickEvent {{ $themeID }} themeName"></span>
					<span class="rightImage onClickEvent"><img src="/images/icons/theme.png"></span>
				</div>
			</a>
			@if(Auth::user()->checkInjuredUserForInjuryTeams()) 
			<a href="/zengarden/home" class="menuWidget">
				<div class="widget">
					<span class="label">my injury</span>
					<span class="data">zengarden</span>
					<span class="rightImage"><img src="/images/icons/zengarden.png"></span>
				</div>
			</a>
			@endif
			<a href="/logout" class="menuWidget last">
				<div class="widget">
					<span class="label">log out</span>
					<span class="data">goodbye</span>
					<span class="rightImage"><img src="/images/icons/power2.png"></span>
				</div>
			</a>		
		</div>								
	</div> 
</div>

<div id="navbarMobile">
	<div class="hamburger hamburger--vortex no_highlights">
		<div class="hamburger-btn">
			<div class="hamburger-inner"></div>
		</div>
	</div>
	
	<div id="mobileLogo"><a class="no_highlights animate__animated animate__bounceInRight" href="/zenboard"></a></div>
	<div id="slideMenu">
		<div class="menuWidget" id="policyholderColorMobile" style="padding:9px 0;background-color:{{Auth::user()->getCompany()->logo_color}}">
			<div id="policyholderLogoMobileIcon" style="background-image:url({{Auth::user()->getCompany()->logo_location}}?{{ $date = str_replace(' ', '', \Carbon\Carbon::now()) }})"></div><span style="width:200px;overflow:hidden;text-overflow:ellipsis;display:inline-block;padding-left:32px;">{{Auth::user()->getCompany()->company_name}}</span>
		</div>
		<ul>
          <li onclick="location.href='/zenboard';" class="seeTouch" id="mzenboard"><a class="main no_highlights" href="/zenboard" ><img class="mobileNavIcon" src="/images/icons/zenjuries.png"><span class="zenboard">dashboard</span></a></li>
          <li onclick="location.href='/zenisphere';" class="seeTouch" id="mzenisphere"><a class="main no_highlights" href="/zenisphere"><img class="mobileNavIcon" src="/images/icons/zenisphere.png"><span class="zenisphere">users/teams</span></a></li>
          <li onclick="location.href='/zencarelist';" class="seeTouch" id="mzencare"><a class="main no_highlights" href="/zencarelist"><img class="mobileNavIcon" src="/images/icons/stethescope.png"><span class="zencare">injury list</span></a></li>
          <li onclick="location.href='/newinjury';" class="seeTouch" id="mnewinjury"><a class="main no_highlights" href="/newinjury"><img class="mobileNavIcon" src="/images/icons/plus.png"><span class="newinjury">new injury</span></a></li>

		  <!-- next two should only be viewable by company admin/agents/pros-->
		  @if($user_is_zagent || $agent)
		  <li onclick="location.href='/policyholder_profile';" class="seeTouch" id="mprofile"><a class="main no_highlights" href="/policyholder_profile"><img class="mobileNavIcon" src="/images/icons/userinfo.png"><span class="profile">profile</span></a></li>
			@endif
			@if($agent)	
			<li onclick="location.href='/zenpro_list';" class="seeTouch" id="mpolicyholders"><a class="main no_highlights" href="/zenpro_list"><img class="mobileNavIcon" src="/images/icons/users.png"><span class="policyholders">policyholders</span></a></li>
			@endif
			@if($user_is_zagent)	
			<li onclick="location.href='/zagent_list';" class="seeTouch" id="mpolicyholders"><a class="main no_highlights" href="/zagent_list"><img class="mobileNavIcon" src="/images/icons/users.png"><span class="policyholders">policyholders</span></a></li>			
			@endif
			@if($agent)	
			<li onclick="location.href='/zenpro';" class="seeTouch" id="mpolicyholders"><a class="main no_highlights" href="/zenpro"><img class="mobileNavIcon" src="/images/icons/zenpro.png"><span class="policyholders">zenpro</span></a></li>
			@endif
			@if($user_is_zagent)	
			<li onclick="location.href='/zagent';" class="seeTouch" id="mpolicyholders"><a class="main no_highlights" href="/zagent"><img class="mobileNavIcon" src="/images/icons/agent1.png"><span class="policyholders">zagent</span></a></li>			
			@endif
        </ul>
	</div>

	<!-- set the avatarMenu class to add 'injured' to set the user avatar with a RED border, indicating they have an injury -->	
	<div class="avatarMenu mobile @if(Auth::user()->checkInjuredUserForInjuryTeams()) injured @endif">
		<div class="avatarMenu-btn" style="background-image:url({{Auth::user()->photo_url}});"></div>
	</div>	
	<div id="avatarSlideMenuMobile">
		<div class="menuWidget" style="padding:3px 0;background-color:{{Auth::user()->avatar_color}};">
			<span class="textLine1">{{ Auth::user()->getFirstName(); }}</span>
			<span class="textLine2">{{ explode(' ',trim($user->name))[1] }}</span>
		</div>
		<!-- <a class="menuWidget">
			<div href="/zenrating" class="widget">	
				<span class="leftImage"><img src="/images/icons/zenrating.png"></span>
				<span class="rate lv2">73</span>
			</div>
		</a> -->
		<a class="menuWidget showZenNavModal" data-modalcontent="#communicationModalNav">
			<div class="widget">
				<span class="label">my alerts</span>
				<span class="data" id="alertSettingMobile">{{ $alert_settings }}</span>
				<span class="rightImage"><img src="/images/icons/alert.png"></span>
			</div>
		</a>
		<!--
		<a href="/myinfo" class="menuWidget">
			<div class="widget">
				<span class="label">messages</span>
				<span class="data"><b>0</b> new</span>
				<span class="rightImage"><img src="/images/icons/message.png"></span>
			</div>
		</a>
		-->
		<a href="/myinfo" class="menuWidget">
			<div class="widget">
				<span class="label">my info</span>
				<span class="data">update</span>
				<span class="rightImage"><img src="/images/icons/gear.png"></span>
			</div>
		</a>
		<a class="menuWidget showZenNavModal" data-modalcontent="#navThemeModal">
			<div class="widget">
				<span class="label onClickEvent">theme</span>
				<span class="data onClickEvent {{ $themeID }} themeName"></span>
				<span class="rightImage onClickEvent"><img src="/images/icons/theme.png"></span>
			</div>
		</a>
		@if(Auth::user()->checkInjuredUserForInjuryTeams()) 
		<a href="/zengarden/home" class="menuWidget">
			<div class="widget">
				<span class="label">my injury</span>
				<span class="data">zengarden</span>
				<span class="rightImage"><img src="/images/icons/zengarden.png"></span>
			</div>
		</a>
		@endif
	</div>
</div>
<div id="zenNavModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
	@include('partials.modals.setCommunicationNav')
    @include('partials.modals.chooseThemeNav')                
</div>

<script>
    $('.showZenNavModal').on('click', function(){
		console.log('open modal button clicked');
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenNavModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
		console.log('opening ' + target);
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });

// When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("navbar").style.height = "70px";
    document.getElementById("menuLinks").style.padding = "20px 5px";
    document.getElementById("menuLinks").style.fontSize = ".75rem";	
    document.getElementById("logo").style.padding ="8px 0 8px 10px";
    document.getElementById("logo").style.width ="200px";
	document.getElementById("avatarMenuBtn").style.transform ="scale(.6)";
	document.getElementById("avatarMenuBtn").style.top ="-6px";		
	document.getElementById("avatarSlideMenu").style.top ="70px";
	document.getElementById("policyholderCompany").style.fontSize =".6rem";
	document.getElementById("policyholderCompany").style.left ="110px";
	document.getElementById("policyholderCompany").style.padding ="2px 8px 2px 16px";	
	document.getElementById("policyholderLogo").style.width ="12px";
	document.getElementById("policyholderLogo").style.height ="12px";	
	document.getElementById("policyholderName").style.top ="0px";
	alert("scroll-1");			
  } else {
    document.getElementById("navbar").style.height = "135px";
    document.getElementById("menuLinks").style.padding = "50px 20px";
    document.getElementById("menuLinks").style.fontSize = "1rem";	
    document.getElementById("logo").style.padding ="18px 0 18px 20px";
    document.getElementById("logo").style.width ="300px";
	document.getElementById("avatarMenuBtn").style.transform ="scale(1)";
	document.getElementById("avatarMenuBtn").style.top ="30px";		
	document.getElementById("avatarSlideMenu").style.top ="135px";
	document.getElementById("policyholderCompany").style.fontSize =".9rem";
	document.getElementById("policyholderCompany").style.left ="86px";
	document.getElementById("policyholderCompany").style.padding ="3px 10px 3px 18px";	
	document.getElementById("policyholderLogo").style.width ="22px";
	document.getElementById("policyholderLogo").style.height ="22px";
	document.getElementById("policyholderName").style.top ="-4px";	
	alert("scroll-2");		
  }
}
</script>

  <script>
    var forEach=function(t,o,r){if("[object Object]"===Object.prototype.toString.call(t))for(var c in t)Object.prototype.hasOwnProperty.call(t,c)&&o.call(r,t[c],c,t);else for(var e=0,l=t.length;l>e;e++)o.call(r,t[e],e,t)};

    var hamburgers = document.querySelectorAll(".hamburger");
    if (hamburgers.length > 0) {
      forEach(hamburgers, function(hamburger) {
        hamburger.addEventListener("click", function() {
          this.classList.toggle("is-active");
        }, false);
      });
    }
	$('.hamburger').click(function(){
	$('#slideMenu').toggleClass('slide-out');	
	});

    var avatarMenus = document.querySelectorAll(".avatarMenu");
    if (avatarMenus.length > 0) {
      forEach(avatarMenus, function(avatarMenu) {
        avatarMenu.addEventListener("click", function() {
          this.classList.toggle("is-active");
        }, false);
      });
    }
	$('.avatarMenu').click(function(){
	$('#avatarSlideMenuMobile').toggleClass('slide-out')
	$('#avatarSlideMenu').toggleClass('slide-out')
	$('#mobileLogo').toggleClass('slide-over')
	$('.avatarMenu').toggleClass('slide-over');	
	});	
  </script>

<script>
var parent = document.getElementById('container1');
var child = document.getElementById('container2');
child.style.paddingRight = child.offsetWidth - child.clientWidth + "px";
</script>

<script>
  $(".seeTouch").click(function(){
    // $(".seeTouch").removeClass("touched");
    $(this).addClass("touched");
  })
  </script>


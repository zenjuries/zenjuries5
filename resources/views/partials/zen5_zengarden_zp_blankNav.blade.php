<div id="navbar">
    <div id="logo"><a style="background-image:url('https://www.zenployees.com/images/zengarden_dark_med.png');" class="invisible"></a></div>
    <div id="navbar-menu">
        <div id="menuLinks">
            <ul>
                <li id="zenboard"><a class="main zenboard" href="/zenboard"><span class="menuText">dashboard</span></a></li>
                <li id="logout"><a class="login" href="/zengarden/logout"><span>LogOut</span></a></li>
            </ul>
        </div>
    </div>
</div>

<div id="navbarMobile">
    <div class="hamburger hamburger--vortex">
        <div class="hamburger-box">
            <div class="hamburger-inner"></div>
        </div>
    </div>
    <div id="mobileLogo"><img class="animate__animated animate__bounceInRight" src="https://www.zenployees.com/images/zengarden_dark_med.png" height="100%"></div>
    <div id="slideMenu">
        <ul>
            <li onclick="location.href='/zenboard';" class="seeTouch" id="mzenboard"><a class="main no_highlights" href="/zenboard" ><img class="mobileNavIcon" src="/images/icons/zenjuries.png"><span class="zenboard">dashboard</span></a></li>
            <li id="mlogout"><a class="login" href="/zengarden/logout"><span><div class="icon icon-power-off txt__grey"></div> LogOut</span></a></li>
        </ul>
    </div>
</div>

<script>
    // When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
            document.getElementById("navbar").style.height = "70px";
            document.getElementById("menuLinks").style.padding = "20px 5px";
            document.getElementById("logo").style.padding ="8px 0 8px 10px";
            document.getElementById("logo").style.width ="200px";
        } else {
            document.getElementById("navbar").style.height = "135px";
            document.getElementById("menuLinks").style.padding = "50px 20px";
            document.getElementById("logo").style.padding ="18px 0 18px 20px";
            document.getElementById("logo").style.width ="300px";
        }
    }
</script>

<script>
    var forEach=function(t,o,r){if("[object Object]"===Object.prototype.toString.call(t))for(var c in t)Object.prototype.hasOwnProperty.call(t,c)&&o.call(r,t[c],c,t);else for(var e=0,l=t.length;l>e;e++)o.call(r,t[e],e,t)};

    var hamburgers = document.querySelectorAll(".hamburger");
    if (hamburgers.length > 0) {
        forEach(hamburgers, function(hamburger) {
            hamburger.addEventListener("click", function() {
                this.classList.toggle("is-active");
            }, false);
        });
    }
    $('.hamburger').click(function(){
        $('#slideMenu').toggleClass('slide-out');
    });
</script>

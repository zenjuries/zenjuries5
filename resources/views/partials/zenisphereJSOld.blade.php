<?php
$is_zagent = \App\Company::where('id', Auth::user()->company_id)->value('is_agency');
$zagency_name = "Agency";
$agency_type = "ZAgent";
if(!is_null(Auth::user()->zagent_id)){
    $company = \App\Company::where('id', Auth::user()->zagent_id)->first();
    $zagency_name = $company->company_name;
    $agency_type = \App\Agency::where('id', $company->agency_id)->value('agency_type');
}

$company_id = Auth::user()->getCompany()->id;

$load_squad = \App\Squad::where('company_id', $company_id)->where('injury_squad', 0)->value('id');

$company = \App\Company::where('id', $company_id)->first();
$has_zenpro = $company->has_zenpro;
if($has_zenpro){
    $zenpro_id = $company->getZenproId();
}else $zenpro_id = NULL;

?>
<script> /*** GLOBAL VARS ***/
        //the squad var will hold each squadmembers user id for easy saving/loading
    //squad will be for the active team apge
    var squad = {
            id: null,//we can check this to determine if the team has been saved yet or not
            name: null,
            injury_team: 0,
            chief_executive: [],
            chief_navigator: [],
            claim_manager: [],
            work_comp_agent: [],
            customer_service_agent: [],
            safety_manager: [],
            supervisor: [],
            doctor: [],
            physical_therapist: [],
            other: [],
            edited: false, //indicates whether the team has been edited since being loaded (to determine which buttons to show)
            avatar_location: null, //location of the team's current avatar
            avatar_color: null, //the outline color
            background_color: null, //the image's background color
            icon_color: null, //the image's icon color
            avatar_number: null //the image's icon number
        };
    //this will be for the new squad on the template page
    var new_squad = {
        id: null,//we can check this to determine if the team has been saved yet or not
        name: null,
        injury_team: 0,
        chief_executive: [],
        chief_navigator: [],
        claim_manager: [],
        work_comp_agent: [],
        customer_service_agent: [],
        safety_manager: [],
        supervisor: [],
        doctor: [],
        physical_therapist: [],
        other: [],
        avatar_location: null, //location of the team's current avatar
        avatar_color: null, //the outline color
        background_color: null, //the image's background color
        icon_color: null, //the image's icon color
        avatar_number: null //the image's icon number
    };

    var squad_array = [];

    //the roster array will store each available user
    var roster_array = [];

    var information_mode = false;

    var userlist_current_page = 1;
    var userlist_page_total = null;
    var userlist_number_per_page = 7;

    var teamlist_current_page = 1;
    var teamlist_page_total = null;
    var teamlist_number_per_page = 7;

    var team_userlist_current_page = 1;
    var team_userlist_page_total = null;
    //var team_userlist_number_per_page = 7;

    var has_zenpro = '<?php echo $has_zenpro; ?>';
    var zenpro_id = '<?php echo $zenpro_id; ?>';

    /*
    These two variables are for adding a user to a role from the user card, then the user list
    setUserToSquad will either be "active" or "template"
    setUserRle will be the role and if template is set add "new_"
    */
    var setUserToSquad = "";
    var setUserToRole = "";

    //global for the users cards to not go over the total length of stacked cards
    var amountOfStackedCards = 0;

    function addZenpro(){
        //since the zenpro should be added automatically we don't want it to change the squad.edited variable
        if(has_zenpro === 1){
            var squad_edited = squad.edited;
            var zenpro_userindex = getUserIndexById(zenpro_id);
            addUserToSquad(zenpro_userindex, 'other');
            squad.edited = squad_edited;
        }
    }

    function updateTeamListPageNumber(){
        var teamPageNumberSpan = $('#loadSquadModal').find('.teamListPagerContainer').find('.pageNumber');
        teamPageNumberSpan.html("<span>showing<br>page</span>" + teamlist_current_page + " of " + teamlist_page_total);
    }

    $(document).ready(function () {
        $('#subSideNavWrapper').on('click', '.createNewTeamSideButton', function(){
            if($(this).hasClass('selected')){
                $('#loadSquadArrow').css('display', 'none');
                $('#newTeamArrow').css('display', 'block');
            }
        });
        $('#teamPanelHeader .loadSquadModalLoadButton').on('click', function(){
            $('#loadSquadArrow').css('display', 'block');
            $('#newTeamArrow').css('display', 'none');
        });
    });

    $(document).ready(function(){
        // PAGE SETUP
        loadRoster(true);

        getSquadList("premade");

        //EVENT LISTENERS

        $('#userListPrevPage').on('click', function(){
            if(userlist_current_page > 1){
                userlist_current_page = userlist_current_page - 1;
            }
            writeRosterList();
        });

        $('#userListNextPage').on('click', function(){
            //console.log(userlist_page_total);
            //console.log(userlist_current_page);
            if(userlist_page_total !== null && userlist_current_page < userlist_page_total){
                userlist_current_page = userlist_current_page + 1;
            }
            writeRosterList();
        });

        $('#teamListPrevPage').on('click', function(){
            if(teamlist_current_page > 1){
                teamlist_current_page = teamlist_current_page - 1;
                $('#loadSquadModal').find('.teamInfoPanelDefault').show();
                $('#loadSquadModal').find('#teamInfoPanelShowSelected').hide();
                $('#teamListUserInfoPanel').hide();
                $('#loadSquadModal').find('.teamUsers').show();
                updateTeamListPageNumber();
            }
            writeSquadListContents();
        });

        $('#teamListNextPage').on('click', function(){
            if(teamlist_page_total !== null && teamlist_current_page < teamlist_page_total){
                teamlist_current_page = teamlist_current_page + 1;
                $('#loadSquadModal').find('.teamInfoPanelDefault').show();
                $('#loadSquadModal').find('#teamInfoPanelShowSelected').hide();
                $('#teamListUserInfoPanel').hide();
                $('#loadSquadModal').find('.teamUsers').show();
                updateTeamListPageNumber();
            }
            writeSquadListContents();
        });

        $('#teamUserListPrevPage').on('click', function(){
            if(team_userlist_current_page > 1){
                team_userlist_current_page = team_userlist_current_page - 1;
                var squad_index = $('.loadSquadListItem.selected').data('squadindex');
                writeTeamUserListContents(squad_index);
                //updateTeamUserListPageNumber();
            }

        });

        $('#teamUserListNextPage').on('click', function(){
            if(team_userlist_page_total !== null && team_userlist_current_page < team_userlist_page_total){
                team_userlist_current_page = team_userlist_current_page + 1;
                var squad_index = $('.loadSquadListItem.selected').data('squadindex');
                writeTeamUserListContents(squad_index);
                //updateTeamUserListPageNumber();
            }
        });

        $('.teamCircleButton').on('click', function(){
            $('.teamCircleButton').removeClass('selected');
            $(this).addClass('selected');
            var role = $(this).data('role');
            //console.log(role);
            showUserInfo(role);
        });

        $('.addExistingRole').on('click', function(){
            var role = $(this).data('role');
            $('.teamCircleButton').removeClass('selected');
            $('.teamCircleButton[data-role="' + role + '"]').addClass('selected');
            showUserModal();
        });

        $('.addNewRole').on('click', function(){
            var role = $(this).data('role');
            $('.teamCircleButton').removeClass('selected');
            $('.teamCircleButton[data-role="' + role + '"]').addClass('selected');
            showUserModal();
        });

        $('#rosterUserList').on('click', '.userListItem', function(){
            var roster_index = $(this).data('index');
            //showUserInfoModal(roster_index);
            $('.userListItem').removeClass('selected');
            $(this).addClass('selected');
            $('#userInfoDefaultDiv').hide();
            $('#userInfoPanelModal').show();
            showIndividualUserInfo(roster_array[roster_index].id, "userInfoPanelModal");

        });

        $('#addUserFromUserlistButton').on('click', function(){
            var role = $('.teamCircleButton.selected').data('role');
            var roster_index = $('.userListItem.selected').data('index');
            if(role !== undefined && roster_index !== undefined){
                addUserToSquad(roster_index, role);
                $('#modal-fullscreen').modal('hide');
            }
        });

        $('#loadSquadSideButton').on('click', function(){
            $('#loadSquadModal').modal('show');
        });

        $('#teamInfoNameModal').on('click', function(){
            var squad_index = $('.loadSquadListItem.selected').data('squadindex');
            //alert(squad_index);
            //console.log(squad_array);
            var squad_id = squad_array[squad_index].id;
            var squad_name = squad_array[squad_index].squad_name;
            //console.log(squad_id);
            //console.log(squad_name);
            document.getElementById('renameTeamOldName').innerHTML = squad_name;
            $('#renameTeamModalID').val(squad_id);
            //$('#loadSquadModal')
            $('#renameTeamModal').modal('show');
            //alert(squad_id);
        });

        $('#saveNewTeamButton').on('click', function(){
            clearAvatarBuilder();
            launchConfigureTeamModal(null, true);
        });

        $('#setTeamIconSideButton').on('click', function(){
            if(squad.id !== null){
                launchConfigureTeamModal(squad.id, false);
            }
        });

        $("#modalOverwriteSquadButton").on('click', function(){
            overwriteCurrentSquad();
        });

        $('#loadSquadTeamList').on('click', '.squadListItem', function(){
            var squad_id = $(this).data('squadid');
            loadSquad(squad_id, 'active');
        });

        $(document).on('click', '.userSquadListItem', function(){
            var squad_id = $(this).data('teamid');
            loadSquad(squad_id, 'active');
            $('#modal-fullscreen').modal('hide');
            $('#singleUserModalFullscreen').modal('hide');
            $('#loadSquadModal').modal('hide');
        });

        $('#createNewTeamSideButton').on('click', function(){
            $('#loadSquadModal').find('.teamInfoPanelDefault').show();
            $('#loadSquadModal').find('#teamInfoPanelShowSelected').hide();
            $('.loadSquadListItem').removeClass('selected');
            resetSquad();
            $('.teamCircleID').hide();
            $('.roleFilled').hide();
            $('.roleEmpty').show();
            addZenpro();
        });

        $('#viewUsersSideButton').on('click', function(){
            showUserModalViewOnly();
        });

        $('#toggleInfoModeButton').on('click', function(){
            toggleInfoMode();
        });

        $('#inviteNewUserSideButton').on('click', function(){
            $('#addUserModal').modal('show');
            //$('#descCircle').hide();
        });

        $('#overwriteTeamButton').on('click', function(){
            if(validateSquad(false)){
                document.getElementById('overwriteModalSquadName').innerHTML = squad.name;
                $('#overwriteModal').modal('show');
            }
        });

        $('#startOverButton').on('click', function(){
            squad.chief_executive = [];
            squad.chief_navigator = [];
            squad.claim_manager = [];
            squad.work_comp_agent = [];
            squad.customer_service_agent = [];
            squad.safety_manager = [];
            squad.supervisor = [];
            squad.doctor = [];
            squad.physical_therapist = [];
            squad.other = [];
            squad.edited = true;
            resetRoleCircles();
            showUserInfo("chief_executive");
            addZenpro();
            ////console.log(squad);
        });

        $('#renameTeamButton').on('click', function(){
            if(squad.id !== null){
                launchConfigureTeamModal(squad.id, true);
            }
        });

        $('#renameTeamModalSubmitButton').on('click', function(){
            renameSquad();
        });

        //**** USER CONTROLS ****/

        $('#userInfoPanel').on('click', '.userInfoNudgeButton', function(){
            var user_id = $(this).closest(".userControls").data('userid');
            var company_id = {{$company_id}};
            if(user_id !== "" && user_id !== undefined && company_id !== undefined){
                sendNudge(user_id, company_id);
            }
        });


        $(document).on('click', '.userInfoNudgeButtonModal', function(){
            var user_index = $(this).data('userindex');
            var company_id = {{$company_id}};
            if(user_index !== "" && user_index !== undefined && company_id !== undefined){
                sendNudge(roster_array[user_index].id, company_id);
            }
        });

        $(document).on('click', '.userInfoChatButtonModal', function(){
            var user_index = $(this).data('userindex');
            var name_string = "To: " + roster_array[user_index].name;
            if(user_index !== "" && user_index !== undefined){
                document.getElementById('messageTeamMemberName').innerHTML = name_string;
                document.getElementById('messageTeamMemberRecipientId').value = roster_array[user_index].id;
                $('#messageTeamMemberModal').modal('show');
            }
        });

        $(document).on('click', '.userInfoDeleteButtonModal', function(){
            var user_index = $(this).data('userindex');
            if(user_index !== undefined && user_index !== ""){
                var name = roster_array[user_index].name;
                var id = roster_array[user_index].id;
                var zagent_id = roster_array[user_index].zagent_id;
                launchDeleteUserModal(id, name, zagent_id);
            }
        });

        $(document).on('click', '.addUserFromUserlistButton', function(){
            var user_index = $(this).data('userindex');
            var role = $('.teamCircleButton.selected').data('role');
            if(role !== undefined && user_index !== undefined){
                addUserToSquad(user_index, role);
                $('#modal-fullscreen').modal('hide');
                $('#singleUserModalFullscreen').modal('hide');
                $('#loadSquadModal').modal('hide');

            }
        });

        $('#userInfoPanel').on('click', '.userInfoChatButton', function(){
            var user_index = $(this).closest(".userControls").data('userindex');
            var name_string = "To: " + roster_array[user_index].name;
            if(user_index !== "" && user_index !== undefined){
                document.getElementById('messageTeamMemberName').innerHTML = name_string;
                document.getElementById('messageTeamMemberRecipientId').value = roster_array[user_index].id;
                $('#messageTeamMemberModal').modal('show');
            }
        });

        $('#userInfoChatButtonModal').on('click', function(){
            var user_index = $('#currentUserIndexModal').val();
            var name_string = "To: " + roster_array[user_index].name;
            if(user_index !== "" && user_index !== undefined){
                document.getElementById('messageTeamMemberName').innerHTML = name_string;
                document.getElementById('messageTeamMemberRecipientId').value = roster_array[user_index].id;
                $('#messageTeamMemberModal').modal('show');
            }
        });

        $('#userInfoPanel').on('click', '.userInfoRemoveButton', function(){
            var user_id = $(this).closest(".userControls").data('userid');
            var user_role = $(this).closest(".userControls").data('userrole');
            removeUserFromRole(user_role, user_id);
        });


        $('#userInfoPanel').on('click', '.userControlRemove', function(){
            var user_id = $(this).closest(".roleUser").data('userid');
            var user_role = $(this).closest(".roleUser").data('userrole');
            removeUserFromRole(user_role, user_id);
        });

        $('.roleDetailPanel').on('click', '.userControlRemove', function(){
            //alert('clicked');
            var user_id = $(this).closest('.roleUser').data('userid');
            var user_role = $(this).closest('.roleUser').data('userrole');
            removeUserFromRole(user_role, user_id);
        });

        $('#userInfoPanel').on('click', '.userInfoDeleteButton', function(){
            var user_index = $(this).closest("userControls").data('userindex');
            if(user_index !== undefined && user_index !== ""){
                var name = roster_array[user_index].name;
                var id = roster_array[user_index].id;
                var zagent_id = roster_array[user_index].zagent_id;
                launchDeleteUserModal(id, name, zagent_id);
            }

        });

        $('#userInfoDeleteButtonModal').on('click', function(){
            var user_index = $('#currentUserIndexModal').val();
            if(user_index !== undefined && user_index !== ""){
                var name = roster_array[user_index].name;
                var id = roster_array[user_index].id;
                var zagent_id = roster_array[user_index].zagent_id;
                launchDeleteUserModal(id, name, zagent_id);
            }

        });

        $('#deleteUserSubmitButton').on('click', function(){
            var userid = document.getElementById('deleteUserModalId').value;
            var zagent_id = document.getElementById('deleteUserModalZagent').value;
            var user_name = document.getElementById('deleteUserModalName').innerHTML;
            var company_id = {{$company_id}};
            console.log("HI USERESUBMIT");
            if(zagent_id == company_id){
                deleteZAgentZUser(userid, user_name);
            }else{
                removeUser(userid);
            }
            //removeUser(userid);
        });

        $('.zenfuciusUser').on('click', function(){
            $('#helpModal').modal('show');
        });

        //hide elements of the avatar builder if user is on Internet Explorer and FireFox
        if(checkForInternetExplorer()){
            handleInternetExplorer();
        }
    });

    //*** ROSTER FUNCTIONS ***/

    //load the company's employees that should be added to the roster
    //load_squad indicates whether the load squad function should also be called (true or false)
    function loadRoster(load_squad){
        var _token = '<?php echo csrf_token(); ?>';
        $.ajax({
            type: 'GET',
            url: '<?php echo route('getSquadRoster') ?>',
            data: {
                companyID: {{$company_id}},
                _token: _token
            },
            success: function (data) {
                console.log(data);
                generateRosterArray(data, load_squad);
            },
            error: function (data) {
                //alert('error');
            }
        });
    }

    //Sort the employees returned by loadRoster(), loadSquad() if load_squad == true
    function generateRosterArray(employees, load_squad){
        roster_array = employees;
        // WE WANT TO SORT THE ARRAY ALPHABETICALLY
        Array.prototype.sortOn = function(key){
            this.sort(function(a, b){
                if(a[key] < b[key]){
                    return -1;
                }else if(a[key] > b[key]){
                    return 1;
                }
                return 0;
            });
        }

        roster_array.sortOn("name");
        var user_count = roster_array.length;
        var page_number = Math.ceil(user_count / 7);
        //console.log("page number = " + page_number);
        userlist_page_total = page_number;
        //add the users to the roster
        writeRosterList();
        if(load_squad){
            loadSquadToStart();
        }else{
            showButtons();
        }
        //alert('roster loaded');
        //console.log(roster_array);

    }

    function writeRosterList(){
        //THIS WILL WRITE THE ROSTER ARRAY TO THE ROSTER
        var list_html = "";

        var modal_list_html = "";
        //we just want to write the first group of users
        var i = 0;
        userlist_page_total = Math.ceil(roster_array.length / userlist_number_per_page);
        //7 users per page
        var page_length = roster_array.length;
        //if(page_length > roster_array.length){
        //    page_length = roster_array.length;
        //}
        for (var l = 1; l < userlist_current_page; l++){
            i = i + userlist_number_per_page;
            page_length = page_length + userlist_number_per_page;
            if(page_length > roster_array.length){
                page_length = roster_array.length;
            }
        }
        $('#userListPageCount').html("<span>showing<br>page</span> " + userlist_current_page + " of " + userlist_page_total);
        ////console.log("userlist i = " + i);
        ////console.log(i);

        for(i; i < page_length; i++){

             list_html += '<div class="itemGrid__item userContainer" data-index="' + i + '">' + 
					'<div class="userListUser showUserCard showZenModal" data-index="'+ i +'" data-modalcontent="#user_card">' +
						'<div class="userAvatar" style="background-image:url(\'' + roster_array[i].picture_location + '\');background-color:blue;"></div>' +
						'<div class="userFirstName">' +  roster_array[i].name +  '</div>' +
						'<div class="userLastName">' + roster_array[i].email + '</div></div>' +
				'<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div></div>';

            modal_list_html += '<div class="itemGrid__item userContainer addToRoster" data-index="' + i + '">' + 
					'<div class="userListUser">' +
						'<div class="userAvatar" data-id="'+ roster_array[i].id +'" style="background-image:url(\'' + roster_array[i].picture_location + '\');background-color:blue;"></div>' +
						'<div class="userFirstName">' +  roster_array[i].name +  '</div>' +
						'<div class="userLastName">' + roster_array[i].email + '</div></div>' +
				'<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div></div>';
            

        }
        document.getElementById('userGrid').innerHTML = list_html;

        document.getElementById("modalItemGrid").innerHTML = list_html;

        document.getElementById("addUserGrid").innerHTML = modal_list_html;

        //console.log(list_html);
        
        $('#userInfoDefaultDiv').show();
        $('#userInfoPanelModal').hide();
    }

    /*** SQUAD FUNCTIONS ***/

    function writeSquadListContents(){
        console.log('writeSquadListContents');
        var squad_html = "";
        var i = 0;
        if(squad_array.length === 0){
            squad_html += "No Injury Teams at this time!";
        }else{
            for(i; i < squad_array.length; i++){
                squad_html += "<button data-teamId='"+squad_array[i].id+"' data-squadindex='" + i + "' class='selectTeam'>"+squad_array[i].squad_name+"</button>";
            }
        }
        document.getElementById('teamList').innerHTML = squad_html;
        //$('#loadSquadModal').find('.teamInfoPanelDefault').show();
        //$('#loadSquadModal').find('#teamInfoPanelShowSelected').hide();
    }
    function writeTemplateModalContent(){
        var squad_html = "";
        var i = 0;
        for(i; i < squad_array.length; i++){
            squad_html += "<button data-teamId='"+squad_array[i].id+"' data-squadindex='" + i + "' class='selectTeamTemplate'>"+squad_array[i].squad_name+"</button>";
        }
        document.getElementById('templateList').innerHTML = squad_html;
    }

    $('body').on('click', '.selectTeam', function(){
        var squad_id = $(this).data('teamid');
        //alert(squad_id);
        modal.close();
        //showAllTeamsModal.close();
        loadSquad(squad_id, 'active');
    });

    $('body').on('click', '.selectTeamTemplate', function(){
        var squad_id = $(this).data('teamid');
        //alert(squad_id);
        modal.close();
        //showAllTeamsModal.close();
        loadSquad(squad_id, 'template');
    });

    function getSingleUserSquads(squad_members, member_id){
        var position_array = [];
        for(var i = 0; i < squad_members.length; i++){
            if(squad_members[i].user_id === member_id){
                position_array.push(squad_members[i].position_id);
            }
        }
        return position_array;
    }

    $('#loadSquadTeamList').on('click', '.loadSquadListItem', function(){
        $('#teamListUserInfoPanel').hide();
        $('#loadSquadModal').find('.teamUsers').show();
        $('#loadSquadControlsDiv').show();
        $('.loadSquadListItem').removeClass('selected');
        $(this).addClass('selected');
        var squad_index = $(this).data('squadindex');
        var squad_id = squad_array[squad_index].id;
        $('#renameTeamOldName').html(squad_array[squad_index].squad_name);
        $('#renameTeamModalInput').val(squad_array[squad_index].squad_name);
        $('#teamInfoNameModal').attr('data-squadid', squad_id);
        team_userlist_current_page = 1;
        writeTeamUserListContents(squad_index);
    });

    function updateTeamUserListPageNumber(){
        var teamUserPageNumberSpan = $('#loadSquadModal').find('.rosterListPager').find('.pageNumberBottom');
        teamUserPageNumberSpan.html("<span>showing page</span> " + team_userlist_current_page + " of " + team_userlist_page_total);
    }

    function sortTeamUserList(user_id_array, squad_member_array){
        var chief_executive_array = [];
        var human_resources_array = [];
        var claim_manager_array = [];
        var work_comp_agent_array = [];
        var service_agent_array = [];
        var other_array = [];
        var doctor_array = [];
        var physical_therapist_array = [];
        var supervisor_array = [];
        var safety_manager_array = [];
        var sorted_user_id_array = [];
        for(var i = 0; i < user_id_array.length; i++){
            var user_id = user_id_array[i];
            /* POSITION IDS
                   1 = Chief Exec
                   2 = Doctor
                   3 = Physical Therapist
                   4 = Adjuster
                   5 = Agent
                   6 = Insurance Customer Service
                   7 = Safety Coordinator
                   8 = Supervisor
                   9 = HR
                   10 = Other
               */
            var position_array = getSingleUserSquads(squad_member_array, user_id);
            if($.inArray(1, position_array) !== -1){
                chief_executive_array.push(user_id);

            }else if($.inArray(9, position_array) !== -1){
                human_resources_array.push(user_id);

            }else if($.inArray(5, position_array) !== -1){
                work_comp_agent_array.push(user_id);

            }else if($.inArray(6, position_array) !== -1){
                service_agent_array.push(user_id);

            }else if($.inArray(8, position_array) !== -1){
                supervisor_array.push(user_id);

            }else if($.inArray(7, position_array) !== -1){
                safety_manager_array.push(user_id);

            }else if($.inArray(4, position_array) !== -1){
                claim_manager_array.push(user_id);

            }else if($.inArray(2, position_array) !== -1){
                doctor_array.push(user_id);

            }else if($.inArray(3, position_array) !== -1){
                physical_therapist_array.push(user_id);

            }else if($.inArray(10, position_array) !== -1){
                other_array.push(user_id);
            }
        }
        if(chief_executive_array.length > 0){
            for(var i = 0; i < chief_executive_array.length; i++){
                sorted_user_id_array.push(chief_executive_array[i]);
            }
        }

        if(human_resources_array.length > 0){
            for(var i = 0; i < human_resources_array.length; i++){
                sorted_user_id_array.push(human_resources_array[i]);
            }
        }

        if(work_comp_agent_array.length > 0){
            for(var i = 0; i < work_comp_agent_array.length; i++){
                sorted_user_id_array.push(work_comp_agent_array[i]);
            }
        }

        if(service_agent_array.length > 0){
            for(var i = 0; i < service_agent_array.length; i++){
                sorted_user_id_array.push(service_agent_array[i]);
            }
        }

        if(supervisor_array.length > 0){
            for(var i = 0; i < supervisor_array.length; i++){
                sorted_user_id_array.push(supervisor_array[i]);
            }
        }

        if(safety_manager_array.length > 0){
            for(var i = 0; i < safety_manager_array.length; i++){
                sorted_user_id_array.push(safety_manager_array[i]);
            }
        }

        if(claim_manager_array.length > 0){
            for(var i = 0; i < claim_manager_array.length; i++){
                sorted_user_id_array.push(claim_manager_array[i]);
            }
        }

        if(doctor_array.length > 0){
            for(var i = 0; i < doctor_array.length; i++){
                sorted_user_id_array.push(doctor_array[i]);
            }
        }

        if(physical_therapist_array.length > 0){
            for(var i = 0; i < physical_therapist_array.length; i++){
                sorted_user_id_array.push(physical_therapist_array[i]);
            }
        }

        if(other_array.length > 0){
            for(var i = 0; i < other_array.length; i++){
                sorted_user_id_array.push(other_array[i]);
            }
        }
        //console.log("sorted_user_id_array");
        return sorted_user_id_array;

    }

    function writeTeamUserListContents(squad_index){
        $('#loadSquadModal').find('.teamInfoPanelDefault').hide();
        $('#loadSquadModal').find('#teamInfoPanelShowSelected').show();
        //squad_array[i].avatar_location
        $('#loadTeamModalIcon').css("background-image", "url(" + squad_array[squad_index].avatar_location + ")");
        var user_html = "";
        var squad_members = squad_array[squad_index].members;
        $('#teamInfoNameModal').html(squad_array[squad_index].squad_name);
        //console.log('user list');
        //console.log(squad_members);
        var i = 0;
        var l = squad_members.length;
        var user_id_array = [];
        //var teamUserListArray = [];
        for(i; i < l; i++){
            /*
            var user_object = {
                user_id: squad_members[i].user_id,
                position_id: squad_members[i].position_id
            };
            */

            var user_id = squad_members[i].user_id;
            if($.inArray(user_id, user_id_array) == -1){
                user_id_array.push(user_id);
            }
        }


        /*
        var i = 0;
        var page_length = 7;
        for (var j = 1; j < teamlist_current_page; j++){
            i = i + 7;
            page_length = page_length + 7;
            if(page_length > squad_array.length){
                page_length = squad_array.length;
            }
        }
        */
        var page_length = 10;
        i = 0;
        //console.log('about to enter j for loop');
        //console.log(team_userlist_current_page);
        for (var j = 1; j < team_userlist_current_page; j++){
            //console.log("j: " + j);
            i = i + 10;
            page_length = page_length + 10;
        }
        if(page_length > user_id_array.length){
            page_length = user_id_array.length;
        }
        //console.log("about to enter user_id_array loop");
        //console.log("i = " + i);
        //console.log("page_length = " + page_length);
        //console.log(user_id_array);
        user_id_array = sortTeamUserList(user_id_array, squad_members);
        for(i; i < page_length; i++){
            var user_color = "required";
            var user_id = user_id_array[i];
            //var position_id = squad_members[i].position_id;
            var position_array = getSingleUserSquads(squad_members, user_id);
            var position_id = position_array[0];
            /* POSITION IDS
                1 = Chief Exec
                2 = Doctor
                3 = Physical Therapist
                4 = Adjuster
                5 = Agent
                6 = Insurance Customer Service
                7 = Safety Coordinator
                8 = Supervisor
                9 = HR
                10 = Other
            */

            if(position_id === 1 || position_id === 9){
                user_color = "required";
            }else if(position_id === 2 || position_id === 3 || position_id === 10){
                user_color = "optional";
            }else{
                user_color = "suggested";
            }
            //required, suggested, optional
            var member_index = getUserIndexById(user_id);
            var member = roster_array[member_index];
            //console.log("member_index + member");
            //console.log(user_id);
            //console.log(member_index);
            //console.log(member);
            //console.log(roster_array);
            if(member.phone !== "" && member.phone !== null){
                var phone = member.phone;
            }else var phone = "None Entered";


            ////console.log('position array');
            ////console.log(position_array);
            var position_string = "";
            for(var j = 0; j < position_array.length; j++){
                switch (position_array[j]){
                    case 1:
                        position_string += " Chief Executive,";
                        user_role_string = "- chief executive";
                        break;
                    case 2:
                        position_string += " Doctor,";
                        user_role_string = "- doctor";
                        break;
                    case 3:
                        position_string += " Physical Therapist,";
                        user_role_string = "- physical therapist";
                        break;
                    case 4:
                        position_string += " Claim Manager,";
                        user_role_string = "- claim manager";
                        break;
                    case 5:
                        position_string += " Work Comp Agent,";
                        user_role_string = "- work comp agent";
                        break;
                    case 6:
                        position_string += " Customer Service Agent,";
                        user_role_string = "- customer service agent";
                        break;
                    case 7:
                        position_string += " Safety Manager,";
                        user_role_string = "- safety manager";
                        break;
                    case 8:
                        position_string += " Supervisor,";
                        user_role_string = "- supervisor";
                        break;
                    case 9:
                        position_string += " Chief Navigator,";
                        user_role_string = "- chief navigator";
                        break;
                    case 10:
                        position_string += " Other,";
                        user_role_string = "- other";
                        break;
                }
            }
            if(position_array.length > 1){
                user_role_string = "- multiple roles";
            }
            if(position_string !== ""){
                // //console.log(position_string);
                position_string = position_string.slice(0, -1);
                ////console.log(position_string);
            }

            var info_string = "Name: " + member.name + "<br>" +
                "Email: " + member.email + "<br>" +
                "Phone: " + phone + "<br>" +
                "Position(s):" + position_string;

            
            user_html += '<li class="userListItem ' + user_color + '" data-index="' + member_index + '">' +
                '<a class="userListUser">' +
                '<div class="userAvatar" style="background-image:url(\'' + member.picture_location + '\');"></div>' +
                '<div class="userName">' + member.name + "</div>" +
                '<div class="userEmail">' + member.email + "</div>" +
                '<div class="zButton row userActions">' +
                '<button class="buttonHover blue userControlInfo popupMessageTrigger" data-applyclass="newclass"><div class="icon icon-info-circle"></div><span>info</span><span class="popupMessage up" style="display:none">' + info_string + '</span></button>' +
                '<button class="buttonHover cyan userControlNudge"><div class="icon icon-hand-o-right"></div><span>nudge</span></button>' +
                /*'<button class="buttonHover orange userControlChat"><div class="icon icon-comments"></div><span>chat</span></button>' +*/
                '<button class="buttonHover red userControlDelete"><div class="icon icon-removeuser"></div><span>delete</span></button>' +
                '</div>' +
                '</a>' +
                '<span class="userRole ' + user_color + '">' + user_role_string + '</span>' +
                '</li>';

            console.log(user_html);
        }



        //<div class="buttonSingle small round"><button class="buttonGlow blue"><div class="icon icon-info-circle"></div></button><span class="popupMessage up">This is the info message</span></div>


        var page_number = Math.ceil(user_id_array.length / 10);
        team_userlist_page_total = page_number;
        //document.getElementById('').innerHTML = team_userlist_page_total;
        document.getElementById('loadSquadUserList').innerHTML = user_html;
        updateTeamUserListPageNumber();
    }

    /*
    $('loadSquadUserList').on('click', '.userControlInfo', function(){
	        });
    */

    /*
    $('#loadSquadUserList').on('click', '.userControlInfo', function(){
	    var parent_div = $(this).parent();

	    var span = parent_div.find('.popupMessage').toggle();
	    var user_index = $(this).parents('.userListItem').data('index');
	    //alert(user_index);
    });
    */

    $('#loadSquadUserList').on('click', '.userControlNudge', function(){
        var user_index = $(this).parents('.userListItem').data('index');
        var user = roster_array[user_index];
        var company_id = {{$company_id}};
        sendNudge(user.id, company_id);
    });

    $('#userInfoPanel').on('click', '.userControlNudge', function(){
        var user_index = $(this).parents('.roleUser').data('userid');
        var company_id = {{$company_id}};
        // //console.log("user index " + user_index);
        // //console.log("user " + user);
        sendNudge(user_index, company_id);
    });

    $(".roleDetailPanel").on('click', '.userControlNudge', function(){
        var user_index = $(this).parents('.roleUser').data('userid');
        var company_id = {{$company_id}};
        sendNudge(user_index, company_id);
    });

    $('#loadSquadUserList').on('click', '.userControlChat', function(){
        var user_index = $(this).parents('.userListItem').data('index');
        var name_string = "To: " + roster_array[user_index].name;
        if(user_index !== "" && user_index !== undefined){
            document.getElementById('messageTeamMemberName').innerHTML = name_string;
            document.getElementById('messageTeamMemberRecipientId').value = roster_array[user_index].id;
            $('#messageTeamMemberModal').modal('show');
        }
    });

    $('#userInfoPanel').on('click', '.userControlChat', function(){
        var user_index = $(this).parents('.roleUser').data('userid');
        var user_name = $(this).parents('.roleUser').data('name');
        var name_string = "To: " + user_name;
        //console.log("user index " + user_index);
        //console.log("name_string " + name_string);
        if(user_index !== "" && user_index !== undefined){
            document.getElementById('messageTeamMemberName').innerHTML = name_string;
            document.getElementById('messageTeamMemberRecipientId').value = user_index;
            $('#messageTeamMemberModal').modal('show');
        }
    });

    $('.roleDetailPanel').on('click', '.userControlChat', function(){
        var user_id = $(this).parents('.roleUser').data('userid');
        var user_name = $(this).parents('.roleUser').data('name');
        var name_string = "To: " + user_name;
        if(user_id !== "" && user_id !== undefined){
            document.getElementById('messageTeamMemberName').innerHTML = name_string;
            document.getElementById('messageTeamMemberRecipientId').value = user_id;
            $('#messageTeamMemberModal').modal('show');
        }
    });

    $('#loadSquadUserList').on('click', '.userControlDelete', function(){
        var user_index = $(this).parents('.userListItem').data('index');
        if(user_index !== undefined && user_index !== ""){
            var name = roster_array[user_index].name;
            var id = roster_array[user_index].id;
            var zagent_id = roster_array[user_index].zagent_id;
            launchDeleteUserModal(id, name, zagent_id);
        }
        /*
        var user_index = $(this).closest("userControls").data('userindex');
            if(user_index !== undefined && user_index !== ""){
                var name = roster_array[user_index].name;
                var id = roster_array[user_index].id;
                var zagent_id = roster_array[user_index].zagent_id;
                launchDeleteUserModal(id, name, zagent_id);
            }
           */
    });

    $('#userInfoPanel').on('click', '.userControlDelete', function(){
        var user_index = $(this).parents('.roleUser').data('userid');
        if(user_index !== undefined && user_index !== ""){
            var name = $(this).parents('.roleUser').data('name');
            var id = $(this).parents('.roleUser').data('userid');
            var zagent_id = user_index = $(this).parents('.roleUser').data('zagentid');
            launchDeleteUserModal(id, name, zagent_id);
        }
    });

    $('.roleDetailPanel').on('click', '.userControlDelete', function(){
        var user_id = $(this).parents('.roleUser').data('userid');
        if(user_id !== undefined && user_id !== ""){
            var name = $(this).parents('.roleUser').data('name');
            var zagent_id = $(this).parents('.roleUser').data('zagentid');
            launchDeleteUserModal(user_id, name, zagent_id);
        }
    });

    //write the company's squads to the Load Squad modal
    function getSquadList(squad_type){
        var _token = '<?php echo csrf_token(); ?>';
        /*
        if(squad_type !== "injury"){
            $('#showInjuryTeams').show();
            $('#showPremadeTeams').hide();
        }else{
            $('#showInjuryTeams').hide();
            $('#showPremadeTeams').show();
        }
        */
        resetSquadCircle();
        $.ajax({
            type: 'POST',
            url: '<?php echo route('getTeamList') ?>',
            data: {_token: _token,
                company_id: {{$company_id}},
                squad_type: squad_type
            },
            success: function(data){
                console.log(data);
                squad_array = data;
                var page_number = Math.ceil(squad_array.length / 7);
                teamlist_page_total = page_number;
                //console.log(squad_array);
                writeSquadListContents();
                if(squad_type === "premade"){
                    writeTemplateModalContent();
                }
            },
            error: function(data){
                // alert('error');
            }
        });
    }

    //get the squad_members.user_id for each member of the currently selected squad
    function loadSquad(squad_id, squad_type) {
        console.log('load squad');
        console.log(squad_id);
        var _token = '<?php echo csrf_token(); ?>';
        $.ajax({
            type: 'GET',
            url: '<?php echo route('loadSquad'); ?>',
            data: {
                squadID: squad_id,
                _token: _token
            },
            success: function (data) {

                if(squad_type === 'active'){
                    resetSquadCircle();
                    setSquadMembers(data);
                }else{
                    resetTemplateCircle();
                    setTemplateSquadMembers(data);
                }

            }
        });
    }

    //add the current members of the squad to the squad object and add their avatars to the team circle
    function setSquadMembers(member_array){
        resetSquadObject();
        console.log(JSON.stringify(squad));
        console.log('member_array');
        console.log(member_array);
        ////console.log(squad);
        var l = member_array.length;
        if(l) {
            squad.name = member_array[0].squad_name;
            squad.avatar_location = member_array[0].avatar_location;
            squad.avatar_color = member_array[0].avatar_color;
            squad.background_color = member_array[0].background_color;
            squad.id = member_array[0].squad_id;
            squad.injury_team = member_array[0].injury_squad;
            console.log('squad: ' + JSON.stringify(squad));
            var missingTeamMates = "";
            for (var i = 0; i < l; i++) {

                var position_id = member_array[i].position_id;
                var user_id = member_array[i].user_id;
                var user_index = getUserIndexById(user_id);
                if(user_index !== false){
                    switch (position_id) {
                        case 1:
                            squad.chief_executive[squad.chief_executive.length] = user_id;
                            addUserToCircle('chief_executive');
                            break;
                        case 2:
                            squad.doctor[squad.doctor.length] = user_id;
                            addUserToCircle('doctor');
                            break;
                        case 3:
                            squad.physical_therapist[squad.physical_therapist.length] = user_id;
                            addUserToCircle('physical_therapist');
                            break;
                        case 4:
                            squad.claim_manager[squad.claim_manager.length] = user_id;
                            addUserToCircle('claim_manager');
                            break;
                        case 5:
                            squad.work_comp_agent[squad.work_comp_agent.length] = user_id;
                            addUserToCircle('work_comp_agent');
                            break;
                        case 6:
                            squad.customer_service_agent[squad.customer_service_agent.length] = user_id;
                            addUserToCircle('customer_service_agent');
                            break;
                        case 7:
                            squad.safety_manager[squad.safety_manager.length] = user_id;
                            addUserToCircle('safety_manager');
                            break;
                        case 8:
                            squad.supervisor[squad.supervisor.length] = user_id;
                            addUserToCircle('supervisor');
                            break;
                        case 9:
                            squad.chief_navigator[squad.chief_navigator.length] = user_id;
                            addUserToCircle('chief_navigator');
                            break;
                        case 10:
                            squad.other[squad.other.length] = user_id;
                            addUserToCircle('other');
                    }
                }
                $('#currentTeamTitle').html(squad.name);
            }
        }
        
        console.log(JSON.stringify(squad));
    }

    function setTemplateSquadMembers(member_array){
        resetNewSquadObject();
        console.log(JSON.stringify(new_squad));
        console.log(member_array);
        var l = member_array.length;
        if(l) {
            new_squad.name = member_array[0].squad_name;
            new_squad.avatar_location = member_array[0].avatar_location;
            new_squad.avatar_color = member_array[0].avatar_color;
            new_squad.background_color = member_array[0].background_color;
            new_squad.id = member_array[0].squad_id;
            new_squad.injury_team = member_array[0].injury_squad;
            console.log('new_squad: ' + JSON.stringify(new_squad));
            var missingTeamMates = "";
            for (var i = 0; i < l; i++) {

                var position_id = member_array[i].position_id;
                var user_id = member_array[i].user_id;
                var user_index = getUserIndexById(user_id);
                if(user_index !== false){
                    switch (position_id) {
                        case 1:
                            new_squad.chief_executive[new_squad.chief_executive.length] = user_id;
                            addUserToTemplateCircle('new_chief_executive', 'chief_executive');
                            break;
                        case 2:
                            new_squad.doctor[new_squad.doctor.length] = user_id;
                            addUserToTemplateCircle('new_medical_support', 'doctor');
                            break;
                        case 3:
                            new_squad.physical_therapist[new_squad.physical_therapist.length] = user_id;
                            addUserToTemplateCircle('new_physical_therapist', 'physical_therapist');
                            break;
                        case 4:
                            new_squad.claim_manager[new_squad.claim_manager.length] = user_id;
                            addUserToTemplateCircle('new_claim_manager', 'claim_manager');
                            break;
                        case 5:
                            new_squad.work_comp_agent[new_squad.work_comp_agent.length] = user_id;
                            addUserToTemplateCircle('new_work_comp_agent', 'work_comp_agent');
                            break;
                        case 6:
                            new_squad.customer_service_agent[new_squad.customer_service_agent.length] = user_id;
                            addUserToTemplateCircle('new_service_agent', 'customer_service_agent');
                            break;
                        case 7:
                            new_squad.safety_manager[new_squad.safety_manager.length] = user_id;
                            addUserToTemplateCircle('new_safety_manager', 'safety_manager');
                            break;
                        case 8:
                            new_squad.supervisor[new_squad.supervisor.length] = user_id;
                            addUserToTemplateCircle('new_employee_supervisor', 'supervisor');
                            break;
                        case 9:
                            new_squad.chief_navigator[new_squad.chief_navigator.length] = user_id;
                            addUserToTemplateCircle('new_human_resources', 'chief_navigator');
                            break;
                        case 10:
                            new_squad.other[new_squad.other.length] = user_id;
                            addUserToTemplateCircle('new_other_users', 'other');
                    }
                }
                $('#currentTeamTitle').html(new_squad.name);
            }
        }
        
        console.log(JSON.stringify(new_squad));
    }

    function resetSquadCircle(){
        var roles_array = [
            'chief_executive', 
            'safety_manager', 
            'supervisor', 
            'physical_therapist', 
            'doctor', 
            'chief_navigator', 
            'claim_manager', 
            'work_comp_agent', 
            'customer_service_agent', 
            'other'
        ];

        for(var i = 0; i < roles_array.length; i++){
            switch(roles_array[i]){
                case 'physical_therapist':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-rehab"></div>' +
                        '<div class="addTeamUser"><button data-title="Physical Therapist" data-modalcontent="#add_user_to_team" data-roleType="roleOptional" data-role="4" class="twoline roleOptional showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> physical<br>therapist</button></div>' +
						'<div class="teamMemberIcon templateDelay4"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'doctor':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-doctor"></div>' +
                        '<div class="addTeamUser"><button  data-title="Medical Support" data-modalcontent="#add_user_to_team" data-roleType="roleOptional" data-role="5" class="twoline roleOptional showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> medical<br>support</button></div>' +
						'<div class="teamMemberIcon templateDelay5"><div class="helpAvatar" style="background-color:var(--roleOptional);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'chief_executive':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-exec"></div>' +
                        '<div class="addTeamUser"><button data-title="Chief Executive" data-modalcontent="#add_user_to_team" data-roleType="roleRequired" data-role="1" class="twoline roleRequired showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> chief<br>executive</button></div>' +
						'<div class="teamMemberIcon templateDelay1"><div class="helpAvatar" style="background-color:var(--roleRequired);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'claim_manager':
                    document.getElementById(roles_array[i]).innerHTML ='<div class="icon teamRoleTypeIcon icon-injuryreport"></div>'+
                        '<div class="addTeamUser"><button data-title="Claim Manager" data-modalcontent="#add_user_to_team" data-roleType="roleSuggested" data-role="7" class="twoline roleSuggested showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> claim<br>manager</button></div>' +
					    '<div class="teamMemberIcon templateDelay9"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'work_comp_agent':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-insurance"></div>'+
                        '<div class="addTeamUser"><button data-title="Work Comp Agent" data-modalcontent="#add_user_to_team" data-roleType="roleSuggested" data-role="8" class="twoline roleSuggested showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> work comp<br>agent</button></div>'+
						'<div class="teamMemberIcon templateDelay8"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'customer_service_agent':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-support"></div>'+
                        '<div class="addTeamUser"><button data-title="Service Agent" data-modalcontent="#add_user_to_team" data-roleType="roleSuggested" data-role="9" class="twoline roleSuggested showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> service<br>agent</button></div>'+
						'<div class="teamMemberIcon templateDelay7"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'safety_manager':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-exclamation-triangle"></div>'+
                        '<div class="addTeamUser"><button data-title="Safety Manager" data-modalcontent="#add_user_to_team" data-roleType="roleSuggested" data-role="2" class="twoline roleSuggested showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> safety<br>manager</button></div>' +
						'<div class="teamMemberIcon templateDelay2"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'supervisor':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-hardhat"></div>'+
                        '<div class="addTeamUser"><button data-title="Employee Supervisor" data-modalcontent="#add_user_to_team" data-roleType="roleSuggested" data-role="3" class="twoline roleSuggested showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> employee<br>supervisor</button></div>'+
					    '<div class="teamMemberIcon templateDelay3"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'chief_navigator':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-reuser"></div>'+
                        '<div class="addTeamUser"><button data-title="Human Resources" data-modalcontent="#add_user_to_team" data-roleType="roleRequired" data-role="6" class="twoline roleRequired showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> human<br>resources</button></div>' +
						'<div class="teamMemberIcon templateDelay10"><div class="helpAvatar" style="background-color:var(--roleRequired);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'other':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-adduser"></div>'+
                        '<div class="addTeamUser"><button data-title="Other Users" data-modalcontent="#add_user_to_team" data-roleType="roleOptional" data-role="10" class="twoline roleOptional showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> other<br>users</button></div>' +
					    '<div class="teamMemberIcon templateDelay6"><div class="helpAvatar" style="background-color:var(--roleOptional);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    
            }
        }
    }

    $('#resetTemplate').on('click', function(){
        $('#currentTeamTemplateTitle').html('template title');
        resetTemplateCircle();
    });

    function resetTemplateCircle(){
        resetNewSquadObject();
        var roles_array = [
            'new_chief_executive', 
            'new_safety_manager', 
            'new_employee_supervisor', 
            'new_physical_therapist', 
            'new_medical_support', 
            'new_human_resources', 
            'new_claim_manager', 
            'new_work_comp_agent', 
            'new_service_agent', 
            'new_other_users'
        ];

        for(var i = 0; i < roles_array.length; i++){
            switch(roles_array[i]){
                case 'new_physical_therapist':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-rehab"></div>' +
                        '<div class="addTeamUser"><button data-title="Physical Therapist" data-modalcontent="#add_user_to_team" data-roleType="roleOptional" data-role="4" class="twoline roleOptional showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> physical<br>therapist</button></div>' +
						'<div class="teamMemberIcon templateDelay4"><div class="helpAvatar" style="background-color:var(--roleOptional);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'new_medical_support':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-doctor"></div>' +
                        '<div class="addTeamUser"><button  data-title="Medical Support" data-modalcontent="#add_user_to_team" data-roleType="roleOptional" data-role="5" class="twoline roleOptional showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> medical<br>support</button></div>' +
						'<div class="teamMemberIcon templateDelay5"><div class="helpAvatar" style="background-color:var(--roleOptional);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'new_chief_executive':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-exec"></div>' +
                        '<div class="addTeamUser"><button data-title="Chief Executive" data-modalcontent="#add_user_to_team" data-roleType="roleRequired" data-role="1" class="twoline roleRequired showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> chief<br>executive</button></div>' +
						'<div class="teamMemberIcon templateDelay1"><div class="helpAvatar" style="background-color:var(--roleRequired);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'new_claim_manager':
                    document.getElementById(roles_array[i]).innerHTML ='<div class="icon teamRoleTypeIcon icon-injuryreport"></div>'+
                        '<div class="addTeamUser"><button data-title="Claim Manager" data-modalcontent="#add_user_to_team" data-roleType="roleSuggested" data-role="7" class="twoline roleSuggested showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> claim<br>manager</button></div>' +
					    '<div class="teamMemberIcon templateDelay9"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'new_work_comp_agent':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-insurance"></div>'+
                        '<div class="addTeamUser"><button data-title="Work Comp Agent" data-modalcontent="#add_user_to_team" data-roleType="roleSuggested" data-role="8" class="twoline roleSuggested showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> work comp<br>agent</button></div>'+
						'<div class="teamMemberIcon templateDelay8"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'new_service_agent':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-support"></div>'+
                        '<div class="addTeamUser"><button data-title="Service Agent" data-modalcontent="#add_user_to_team" data-roleType="roleSuggested" data-role="9" class="twoline roleSuggested showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> service<br>agent</button></div>'+
						'<div class="teamMemberIcon templateDelay7"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'new_safety_manager':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-exclamation-triangle"></div>'+
                        '<div class="addTeamUser"><button data-title="Safety Manager" data-modalcontent="#add_user_to_team" data-roleType="roleSuggested" data-role="2" class="twoline roleSuggested showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> safety<br>manager</button></div>' +
						'<div class="teamMemberIcon templateDelay2"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'new_employee_supervisor':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-hardhat"></div>'+
                        '<div class="addTeamUser"><button data-title="Employee Supervisor" data-modalcontent="#add_user_to_team" data-roleType="roleSuggested" data-role="3" class="twoline roleSuggested showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> employee<br>supervisor</button></div>'+
					    '<div class="teamMemberIcon templateDelay3"><div class="helpAvatar" style="background-color:var(--roleSuggested);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'new_human_resources':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-reuser"></div>'+
                        '<div class="addTeamUser"><button data-title="Human Resources" data-modalcontent="#add_user_to_team" data-roleType="roleRequired" data-role="6" class="twoline roleRequired showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> human<br>resources</button></div>' +
						'<div class="teamMemberIcon templateDelay10"><div class="helpAvatar" style="background-color:var(--roleRequired);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    break;
                case 'new_other_users':
                    document.getElementById(roles_array[i]).innerHTML = '<div class="icon teamRoleTypeIcon icon-adduser"></div>'+
                        '<div class="addTeamUser"><button data-title="Other Users" data-modalcontent="#add_user_to_team" data-roleType="roleOptional" data-role="10" class="twoline roleOptional showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> other<br>users</button></div>' +
					    '<div class="teamMemberIcon templateDelay6"><div class="helpAvatar" style="background-color:var(--roleOptional);background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i]).removeClass('showUserCard showZenModal');
                        $('#' + roles_array[i]).removeAttr('data-modalcontent');
                    
            }
        }
    }

    function resetSquad(){
        $('#loadSquadSideButton').removeClass('selected');
        $('#createNewTeamSideButton').addClass('selected');
        resetSquadObject();
        resetRoleCircles();
        //resetRoleCircles();
        $('.teamCircleTitle').hide();
        $('#newTeamTitleDiv').show();
        if(information_mode){
            toggleInfoMode();
        }
        $('#setTeamIconSideButton').hide();
        $('#teamIconDisplay').css("background-image", "url('/images/avatars/teamIcons/noTeam.png')");
        $('#roleCheckIcon').hide();
        $('#teamNameTitle').html("New Team");


    }

    //clears the squad object to a blank slate (new team)
    function resetSquadObject(){
        squad.id = null;
        squad.name = null;
        squad.injury_team = 0;
        squad.chief_executive = [];
        squad.chief_navigator = [];
        squad.claim_manager = [];
        squad.work_comp_agent = [];
        squad.customer_service_agent = [];
        squad.safety_manager = [];
        squad.supervisor = [];
        squad.doctor = [];
        squad.physical_therapist = [];
        squad.other = [];
        squad.edited = false;
        squad.avatar_location = null;
        squad.avatar_color = null;
        squad.background_color = null;
        squad.icon_color = null;
        squad.avatar_number = null;

    }

    function resetNewSquadObject(){
        new_squad.id = null;
        new_squad.name = null;
        new_squad.injury_team = 0;
        new_squad.chief_executive = [];
        new_squad.chief_navigator = [];
        new_squad.claim_manager = [];
        new_squad.work_comp_agent = [];
        new_squad.customer_service_agent = [];
        new_squad.safety_manager = [];
        new_squad.supervisor = [];
        new_squad.doctor = [];
        new_squad.physical_therapist = [];
        new_squad.other = [];
        new_squad.edited = false;
        new_squad.avatar_location = null;
        new_squad.avatar_color = null;
        new_squad.background_color = null;
        new_squad.icon_color = null;
        new_squad.avatar_number = null;
    }

    //clears the role circles
    function resetRoleCircles(){
        resetChiefExecutiveCircle();
        resetChiefNavigatorCircle();
        resetClaimManagerCircle();
        resetWorkCompAgentCircle();
        resetCustomerServiceAgentCircle();
        resetOtherCircle();
        resetDoctorCircle();
        resetPhysicalTherapistCircle();
        resetSupervisorCircle();
        resetSafetyManagerCircle();
        $('.teamCircleButton').css('borderColor', '#4d4d4d');//reset the borders to their default colors
        showButtons();
    }

    function resetChiefExecutiveCircle(){
        document.getElementById('chiefExecutiveRole').innerHTML = '<div class="noRole"><div class="icon icon-exec"></div><span class="label">chief<br>executive</span></div> ' +
            '<div class="setRole"><span class="icon icon-exec"></span></div>';
    }

    function resetChiefNavigatorCircle(){
        document.getElementById('chiefNavigatorRole').innerHTML = '<div class="noRole"><div class="icon icon-reuser"></div><span class="label">human<br>resources</span></div> ' +
            '<div class="setRole"><span class="icon icon-reuser"></span></div>'
    }

    function resetClaimManagerCircle(){
        document.getElementById('claimManagerRole').innerHTML = '<div class="noRole"><div class="icon icon-injuryreport"></div><span class="label">claim<br>manager</span></div> ' +
            '<div class="setRole"><span class="icon icon-injuryreport"></span></div>';
    }

    function resetWorkCompAgentCircle(){
        document.getElementById('workCompAgentRole').innerHTML = '<div class="noRole"><div class="icon icon-insurance"></div><span class="label">work comp<br>agent</span></div>';
    }

    function resetCustomerServiceAgentCircle(){
        document.getElementById('customerServiceAgentRole').innerHTML = '<div class="noRole"><div class="icon icon-support"></div><span class="label">service<br>agent</span></div>';
    }

    function resetOtherCircle(){
        document.getElementById('otherRole').innerHTML = '<div class="noRole"><div class="icon icon-users"></div><span class="label">other<br>users</span></div>';
    }

    function resetDoctorCircle(){
        document.getElementById('doctorRole').innerHTML = '<div class="noRole"><div class="icon icon-doctor"></div><span class="label">employee\'s<br>doctor</span></div>';
    }

    function resetPhysicalTherapistCircle(){
        document.getElementById('physicalTherapistRole').innerHTML = '<div class="noRole"><div class="icon icon-rehab"></div><span class="label">physical<br>therapist</span></div>';
    }

    function resetSupervisorCircle(){
        document.getElementById('supervisorRole').innerHTML = '<div class="noRole"><div class="icon icon-hardhat"></div><span class="label">employee\'s<br>supervisor</span></div>';
    }

    function resetSafetyManagerCircle(){
        document.getElementById('safetyManagerRole').innerHTML = '<div class="noRole"><div class="icon icon-exclamation-triangle"></div><span class="label">safety<br>manager</span></div>';
    }

    //This function handles checking if the user is on internet explore or firefox
    //the addtion of firefox was added later thus why the function name does not say firefox
    function checkForInternetExplorer(){
        var ua = window.navigator.userAgent;
        var is_ie = /MSIE|Trident/.test(ua);
        var gecko = ua.indexOf('Gecko/');
        if (is_ie || gecko > 0) {
            return true;
        }else{
            return false;

        }
    }
    function handleInternetExplorer(){
        $('.zCodeAvatarBuilder').hide();
    }

    /* AJAX call to submit squad */
    function saveNewSquad(){
        var _token = '<?php echo csrf_token(); ?>';
        var valid = validateSquad(true);
        var squad_name = $('#configureTeamNameInput').val();
        if(checkForInternetExplorer()){
            if(valid){
                $.ajax({
                    type: 'POST',
                    url: '<?php echo route("postNewSquadOld") ?>',
                    data: {
                        chiefExecutiveArray: squad.chief_executive,
                        doctorArray: squad.doctor,
                        therapistArray: squad.physical_therapist,
                        claimManagerArray: squad.claim_manager,
                        agentArray: squad.work_comp_agent,
                        customerServiceArray: squad.customer_service_agent,
                        safetyCoordinatorArray: squad.safety_manager,
                        supervisorArray: squad.supervisor,
                        chiefNavigatorArray: squad.chief_navigator,
                        otherArray: squad.other,
                        squadName: squad_name,
                        avatar_location: squad.avatar_location,
                        background_color: squad.background_color,
                        avatar_color: squad.avatar_color,
                        avatar_number: squad.avatar_number,
                        icon_color: squad.icon_color,
                        _token: _token
                    },
                    success: function(data){
                        getSquadList("premade");
                        loadSquad(data, 'active');
                        showAlert("Your team has been saved!", "confirm", 5);
                        $('#teamNameModalInput').val("");
                        $('#teamNameModal').modal('hide');
                        //alert('success');

                    }
                });
            }
        }else{
            if(valid) {
                // //console.log("set new team avatar preview");
                var background_color = getCurrentBackgroundColor();
                var outline_color = getCurrentOutlineColor();
                var icon_color = getCurrentIconColor();
                var avatar_number = $('.avatarListItem.selected').find('a').data('avatar');
                var canvas = document.createElement("canvas");
                canvas.width="256";
                canvas.height = "256";
                var ctx = canvas.getContext( "2d" );
                //draw the diamond
                drawCanvasDiamond(ctx, 8, background_color, outline_color);

                var img = document.createElement("img");
                //img.setAttribute("src", "/images/avatars/avatar_icons/" + avatar_number + ".png");
                img.src = 'data:image/svg+xml;base64,' + window.btoa(document.getElementById('teamIcon' + avatar_number).outerHTML);
                img.onload = function() {
                    ////console.log("setNewTeamAvatarPreview img onload");
                    ctx.drawImage(img, 64, 64, 128, 128);
                    //ctx.drawImage(mask, 0, 0);
                    var icon = canvas.toDataURL('image/png');
                    img = canvas.toDataURL('image/png');
                    $('.avatarPreview').find('span').css('background-image', 'url(' + img + ')');
                    //$('.avatarPreview').css('background-color', outline_color);
                    squad.avatar_location = img;
                    squad.background_color = background_color;
                    squad.avatar_color = outline_color;
                    squad.icon_color = icon_color;
                    squad.avatar_number = avatar_number;

                    ////console.log('valid');
                    ////console.log(squad.avatar_location);
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo route("postNewSquad") ?>',
                        data: {
                            chiefExecutiveArray: squad.chief_executive,
                            doctorArray: squad.doctor,
                            therapistArray: squad.physical_therapist,
                            claimManagerArray: squad.claim_manager,
                            agentArray: squad.work_comp_agent,
                            customerServiceArray: squad.customer_service_agent,
                            safetyCoordinatorArray: squad.safety_manager,
                            supervisorArray: squad.supervisor,
                            chiefNavigatorArray: squad.chief_navigator,
                            otherArray: squad.other,
                            squadName: squad_name,
                            avatar_location: squad.avatar_location,
                            background_color: squad.background_color,
                            avatar_color: squad.avatar_color,
                            avatar_number: squad.avatar_number,
                            icon_color: squad.icon_color,
                            _token: _token
                        },
                        success: function(data){
                            getSquadList("premade");
                            loadSquad(data, 'active');
                            showAlert("Your team has been saved!", "confirm", 5);
                            $('#teamNameModalInput').val("");
                            $('#teamNameModal').modal('hide');
                            //alert('success');

                        }
                    });

                }
            }
        }

    }

    $('#saveEditedTeam').on('click', function(){
        overwriteCurrentSquad();
    });

    /* Overwrite an old squad instead of creating a new one */
    function overwriteCurrentSquad(){
        var _token = '<?php echo csrf_token(); ?>';
        var valid = validateSquad(true);
        if(squad.id !== null && valid == true) {
            $.ajax({
                type: 'POST',
                url: '<?php echo route('editSquad') ?>',
                data: {
                    chiefExecutiveArray: squad.chief_executive,
                    doctorArray: squad.doctor,
                    therapistArray: squad.physical_therapist,
                    claimManagerArray: squad.claim_manager,
                    agentArray: squad.work_comp_agent,
                    customerServiceArray: squad.customer_service_agent,
                    safetyCoordinatorArray: squad.safety_manager,
                    supervisorArray: squad.supervisor,
                    chiefNavigatorArray: squad.chief_navigator,
                    otherArray: squad.other,
                    squadName: squad.name,
                    squad_id: squad.id,
                    _token: _token
                },
                success: function (data) {
                    showAlert("Your team has been updated!", "confirm", 5);
                    modal.close();
                    squad.edited = false;
                    loadSquad(squad.id, 'active');
                 
                }
            });
        }
    }


    function validateSquad(show_error_messages){
        var error_message = "";
        var valid = true;
        if(squad.chief_executive[0] == null || squad.chief_executive[0] == ""){
            error_message += "The position of Chief Executive is required. You must fill this position before you can save the team.<br>";
            valid = false;
        }
        if(squad.chief_navigator[0] == null || squad.chief_navigator[0] == ""){
            error_message += "The position of Chief Navigator is required. You must fill this position before you can save the team.<br>";
            valid = false;
        }
        //document.getElementById('teamErrors').innerHTML = error_message;
        if(show_error_messages){
            ////console.log(error_message);
        }
        return valid;
    }

    //** Page Functions **//

    //determine which contextual controls should be displayed.
    //should be called any time a condition can change (adding/removing a team member, loading a new team, saving a team)
    function showButtons(){
        $('.teamControlsButton').hide();
        //button cases
        var valid_squad = validateSquad();

        if(squad.id !== null){
            var preexisting_squad = true;
        }else{
            var preexisting_squad = false;
        }

        if(valid_squad){//show the Save as New button
            document.getElementById('saveNewTeamButton').style.display = "";
        }else{
            $('#saveNewTeamButton').removeClass('buttonGlow blue');
        }

        if(valid_squad && squad.edited){
            $('#saveNewTeamButton').addClass('buttonGlow blue');
        }

        if(valid_squad && preexisting_squad && squad.edited){//show the Overwrite Team button
            document.getElementById('overwriteTeamButton').style.display = "";
            $('#overwriteTeamButton').addClass('buttonGlow blue');
        }else{
            $('#overwriteTeamButton').removeClass('buttonGlow blue');
        }

        if( squad.chief_executive !== null ||
            squad.chief_navigator !== null ||
            squad.claim_manager !== null ||
            squad.work_comp_agent !== null ||
            squad.customer_service_agent !== null ||
            squad.safety_manager !== null ||
            squad.supervisor !== null ||
            squad.doctor !== null ||
            squad.physical_therapist !== null ||
            squad.other !== null ) {
            //if any position on the team is filled, show the "Clear Circle" button
            document.getElementById('startOverButton').style.display = "";
        }

        if(preexisting_squad){//show Re-Name Team and Delete Team buttons
            document.getElementById('renameTeamButton').style.display = "";
            document.getElementById('deleteTeamButton').style.display = "";
        }
    }

    //add the user's avatar to the squad circle
    function addUserToCircle(role){
        console.log(role);
        var userArray = [];
        if(squad[role].length > 1){
            for(var i = 0; i < squad[role].length; i++){
                var index = getUserIndexById(squad[role][i]);
                userArray.push(index);
            }
        }
        switch(role){
            case 'physical_therapist':
                if(squad[role].length === 1){
                    var roster_index = getUserIndexById(squad[role][0]);
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-rehab"></div>' +
                    '<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> physical<br>therapist</button>' +
                    '<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole optional">physical therapist</span></div>' +
                    '<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-rehab"></div>' +
                        '<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> physical<br>therapist</button>' +
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole optional">physical therapist</span></div>' +
                        '<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+role).parent().addClass('multiUser');   
                }
                $("#"+role).addClass('showZenModal showUserCard');
                $('#'+role).attr('data-modalcontent', '#user_card');
                break;
            case 'doctor':
                if(squad[role].length === 1){
                    var roster_index = getUserIndexById(squad[role][0]);
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-doctor"></div>' +
														'<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> medical<br>support</button>' +
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole optional">medical support</span></div>' +
														'<div class="teamMemberIcon teamDelay5"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-doctor"></div>' +
                        '<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> medical<br>support</button>' +
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole optional">medical support</span></div>' +
                        '<div class="teamMemberIcon teamDelay5"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+role).parent().addClass('multiUser');
                }
                $("#"+role).addClass('showZenModal showUserCard');
                $('#'+role).attr('data-modalcontent', '#user_card');
                break;
            case 'chief_executive':
                if(squad[role].length === 1){
                    var roster_index = getUserIndexById(squad[role][0]);
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-exec"></div>' +
														'<button class="twoline roleRequired showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> chief<br>executive</button>' +
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole required">chief executive</span></div>' +
														'<div class="teamMemberIcon teamDelay1"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-exec"></div>' +
                        '<button class="twoline roleRequired showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> chief<br>executive</button>' +
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole required">chief executive</span></div>' +
                        '<div class="teamMemberIcon teamDelay1"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+role).parent().addClass('multiUser');
                }
                $("#"+role).addClass('showZenModal showUserCard');
                $('#'+role).attr('data-modalcontent', '#user_card');
                break;
            case 'claim_manager':
                if(squad[role].length === 1){
                    var roster_index = getUserIndexById(squad[role][0]);
                    document.getElementById(role).innerHTML ='<div class="icon teamRoleTypeIcon icon-injuryreport"></div>'+
														'<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> claim<br>manager</button>' +
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole suggested">claim manager</span></div>' +
														'<div class="teamMemberIcon teamDelay9"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(role).innerHTML ='<div class="icon teamRoleTypeIcon icon-injuryreport"></div>'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> claim<br>manager</button>' +
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole suggested">claim manager</span></div>' +
                        '<div class="teamMemberIcon teamDelay9"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+role).parent().addClass('multiUser');
                }
                $("#"+role).addClass('showZenModal showUserCard');
                $('#'+role).attr('data-modalcontent', '#user_card');
                break;
            case 'work_comp_agent':
                if(squad[role].length === 1){
                    var roster_index = getUserIndexById(squad[role][0]);
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-insurance"></div>'+
														'<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> work comp<br>agent</button>'+
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole suggested">work comp agent</span></div>'+
														'<div class="teamMemberIcon teamDelay8"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-insurance"></div>'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> work comp<br>agent</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole suggested">work comp agent</span></div>'+
                        '<div class="teamMemberIcon teamDelay8"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+role).parent().addClass('multiUser');
                }
                $("#"+role).addClass('showZenModal showUserCard');
                $('#'+role).attr('data-modalcontent', '#user_card');
                break;
            case 'customer_service_agent':
                if(squad[role].length === 1){
                    var roster_index = getUserIndexById(squad[role][0]);
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-support"></div>'+
														'<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> service<br>agent</button>'+
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole suggested">service agent</span></div>'+
														'<div class="teamMemberIcon teamDelay7"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-support"></div>'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> service<br>agent</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole suggested">service agent</span></div>'+
                        '<div class="teamMemberIcon teamDelay7"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+role).parent().addClass('multiUser');
                }
                $("#"+role).addClass('showZenModal showUserCard');
                $('#'+role).attr('data-modalcontent', '#user_card');
                break;
            case 'safety_manager':
                if(squad[role].length === 1){
                    var roster_index = getUserIndexById(squad[role][0]);
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-exclamation-triangle"></div>'+
														'<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> safety<br>manager</button>'+
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole suggested">safety manager</span></div>'+
														'<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-exclamation-triangle"></div>'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> safety<br>manager</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole suggested">safety manager</span></div>'+
                        '<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+role).parent().addClass('multiUser');
                }
                $("#"+role).addClass('showZenModal showUserCard');
                $('#'+role).attr('data-modalcontent', '#user_card');
                break;
            case 'supervisor':
                if(squad[role].length === 1){
                    var roster_index = getUserIndexById(squad[role][0]);
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-hardhat"></div>'+
														'<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> employee<br>supervisor</button>'+
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole suggested">employee supervisor</span></div>'+
														'<div class="teamMemberIcon teamDelay3"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-hardhat"></div>'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> employee<br>supervisor</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole suggested">employee supervisor</span></div>'+
                        '<div class="teamMemberIcon teamDelay3"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+role).parent().addClass('multiUser');
                }
                $("#"+role).addClass('showZenModal showUserCard');
                $('#'+role).attr('data-modalcontent', '#user_card');
                break;
            case 'chief_navigator':
                console.log('role is chief nav');
                if(squad[role].length === 1){
                    var roster_index = getUserIndexById(squad[role][0]);
                    console.log('adding new chief nav');
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-reuser"></div>'+
														'<button class="twoline roleRequired showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> human<br>resources</button>'+
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole required">human resources</span></div>'+
														'<div class="teamMemberIcon teamDelay10"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-reuser"></div>'+
                        '<button class="twoline roleRequired showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> human<br>resources</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole required">human resources</span></div>'+
                        '<div class="teamMemberIcon teamDelay10"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+role).parent().addClass('multiUser');
                }
                $("#"+role).addClass('showZenModal showUserCard');
                $('#'+role).attr('data-modalcontent', '#user_card');
                break;
            case 'other':
                if(squad[role].length === 1){
                    var roster_index = getUserIndexById(squad[role][0]);
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-adduser"></div>'+
														'<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> other<br>users</button>'+
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole optional">other user(s)</span></div>'+
														'<div class="teamMemberIcon teamDelay6"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(role).innerHTML = '<div class="icon teamRoleTypeIcon icon-adduser"></div>'+
                        '<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> other<br>users</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+ userArray +'">Multiple Users</span><span class="teamUserRole optional">other user(s)</span></div>'+
                        '<div class="teamMemberIcon teamDelay6"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+role).parent().addClass('multiUser');
                }
                $("#"+role).addClass('showZenModal showUserCard');
                $('#'+role).attr('data-modalcontent', '#user_card');
                
        }
    }

    function addUserToTemplateCircle(id, role){
        var userArray = [];
        if(new_squad[role].length > 1){
            for(var i = 0; i < new_squad[role].length; i++){
                var index = getUserIndexById(new_squad[role][i]);
                userArray.push(index);
            }
        }
        switch(id){
            case 'new_physical_therapist':
                if(new_squad[role].length === 1){
                    var roster_index = getUserIndexById(new_squad[role][0]);
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-rehab"></div>' +
                    '<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> physical<br>therapist</button>' +
                    '<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole optional">physical therapist</span></div>' +
                    '<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-rehab"></div>' +
                        '<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> physical<br>therapist</button>' +
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole optional">physical therapist</span></div>' +
                        '<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+id).parent().addClass('multiUser');   
                }
                $("#"+id).addClass('showZenModal showUserCard');
                $('#'+id).attr('data-modalcontent', '#user_card');
                break;
            case 'new_medical_support':
                if(new_squad[role].length === 1){
                    var roster_index = getUserIndexById(new_squad[role][0]);
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-doctor"></div>' +
														'<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> medical<br>support</button>' +
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole optional">medical support</span></div>' +
														'<div class="teamMemberIcon teamDelay5"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-doctor"></div>' +
                        '<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> medical<br>support</button>' +
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole optional">medical support</span></div>' +
                        '<div class="teamMemberIcon teamDelay5"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+id).parent().addClass('multiUser');
                }
                $("#"+id).addClass('showZenModal showUserCard');
                $('#'+id).attr('data-modalcontent', '#user_card');
                break;
            case 'new_chief_executive':
                if(new_squad[role].length === 1){
                    var roster_index = getUserIndexById(new_squad[role][0]);
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-exec"></div>' +
														'<button class="twoline roleRequired showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> chief<br>executive</button>' +
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole required">chief executive</span></div>' +
														'<div class="teamMemberIcon teamDelay1"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-exec"></div>' +
                        '<button class="twoline roleRequired showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> chief<br>executive</button>' +
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole required">chief executive</span></div>' +
                        '<div class="teamMemberIcon teamDelay1"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+id).parent().addClass('multiUser');
                }
                $("#"+id).addClass('showZenModal showUserCard');
                $('#'+id).attr('data-modalcontent', '#user_card');
                break;
            case 'new_claim_manager':
                if(new_squad[role].length === 1){
                    var roster_index = getUserIndexById(new_squad[role][0]);
                    document.getElementById(id).innerHTML ='<div class="icon teamRoleTypeIcon icon-injuryreport"></div>'+
														'<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> claim<br>manager</button>' +
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole suggested">claim manager</span></div>' +
														'<div class="teamMemberIcon teamDelay9"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(id).innerHTML ='<div class="icon teamRoleTypeIcon icon-injuryreport"></div>'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> claim<br>manager</button>' +
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole suggested">claim manager</span></div>' +
                        '<div class="teamMemberIcon teamDelay9"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+id).parent().addClass('multiUser');
                }
                $("#"+id).addClass('showZenModal showUserCard');
                $('#'+id).attr('data-modalcontent', '#user_card');
                break;
            case 'new_work_comp_agent':
                if(new_squad[role].length === 1){
                    var roster_index = getUserIndexById(new_squad['work_comp_agent'][0]);
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-insurance"></div>'+
														'<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> work comp<br>agent</button>'+
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole suggested">work comp agent</span></div>'+
														'<div class="teamMemberIcon teamDelay8"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-insurance"></div>'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> work comp<br>agent</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole suggested">work comp agent</span></div>'+
                        '<div class="teamMemberIcon teamDelay8"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+id).parent().addClass('multiUser');
                }
                $("#"+id).addClass('showZenModal showUserCard');
                $('#'+id).attr('data-modalcontent', '#user_card');
                break;
            case 'new_service_agent':
                if(new_squad[role].length === 1){
                    var roster_index = getUserIndexById(new_squad[role][0]);
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-support"></div>'+
														'<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> service<br>agent</button>'+
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole suggested">service agent</span></div>'+
														'<div class="teamMemberIcon teamDelay7"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-support"></div>'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> service<br>agent</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole suggested">service agent</span></div>'+
                        '<div class="teamMemberIcon teamDelay7"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+id).parent().addClass('multiUser');
                }
                $("#"+id).addClass('showZenModal showUserCard');
                $('#'+id).attr('data-modalcontent', '#user_card');
                break;
            case 'new_safety_manager':
                if(new_squad[role].length === 1){
                    var roster_index = getUserIndexById(new_squad[role][0]);
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-exclamation-triangle"></div>'+
														'<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> safety<br>manager</button>'+
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole suggested">safety manager</span></div>'+
														'<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-exclamation-triangle"></div>'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> safety<br>manager</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole suggested">safety manager</span></div>'+
                        '<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+id).parent().addClass('multiUser');
                }
                $("#"+id).addClass('showZenModal showUserCard');
                $('#'+id).attr('data-modalcontent', '#user_card');
                break;
            case 'new_employee_supervisor':
                if(new_squad[role].length === 1){
                    var roster_index = getUserIndexById(new_squad[role][0]);
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-hardhat"></div>'+
														'<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> employee<br>supervisor</button>'+
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole suggested">employee supervisor</span></div>'+
														'<div class="teamMemberIcon teamDelay3"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-hardhat"></div>'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> employee<br>supervisor</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole suggested">employee supervisor</span></div>'+
                        '<div class="teamMemberIcon teamDelay3"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+id).parent().addClass('multiUser');
                }
                $("#"+id).addClass('showZenModal showUserCard');
                $('#'+id).attr('data-modalcontent', '#user_card');
                break;
            case 'new_human_resources':
                console.log('role is chief nav');
                if(new_squad[role].length === 1){
                    var roster_index = getUserIndexById(new_squad[role][0]);
                    console.log('adding new chief nav');
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-reuser"></div>'+
														'<button class="twoline roleRequired showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> human<br>resources</button>'+
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole required">human resources</span></div>'+
														'<div class="teamMemberIcon teamDelay10"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-reuser"></div>'+
                        '<button class="twoline roleRequired showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> human<br>resources</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole required">human resources</span></div>'+
                        '<div class="teamMemberIcon teamDelay10"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+id).parent().addClass('multiUser');
                }
                $("#"+id).addClass('showZenModal showUserCard');
                $('#'+id).attr('data-modalcontent', '#user_card');
                break;
            case 'new_other_users':
                if(new_squad[role].length === 1){
                    var roster_index = getUserIndexById(new_squad[role][0]);
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-adduser"></div>'+
														'<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> other<br>users</button>'+
														'<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole optional">other user(s)</span></div>'+
														'<div class="teamMemberIcon teamDelay6"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
                }else{
                    document.getElementById(id).innerHTML = '<div class="icon teamRoleTypeIcon icon-adduser"></div>'+
                        '<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> other<br>users</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+ userArray +'">Multiple Users</span><span class="teamUserRole optional">other user(s)</span></div>'+
                        '<div class="teamMemberIcon teamDelay6"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                        $("#"+id).parent().addClass('multiUser');
                }
                $("#"+id).addClass('showZenModal showUserCard');
                $('#'+id).attr('data-modalcontent', '#user_card');
                
        }
    }

    //adds the user to the squad object in the proper role, updates squad.edited, adds user to team circle via addUserToCircle()
    function addUserToSquad(roster_index, role){
        //if they're already in that role then we don't want to add them again
        var user_in_role = false;
        var user_id = roster_array[roster_index].id;
        for(var i = 0; i < squad[role].length; i++){
            if(squad[role][i] === user_id){
                user_in_role = true;
                break;
            }
        }
        if(user_in_role === false){
            squad[role][squad[role].length] = user_id;
            addUserToCircle(role);
            squad.edited = true;
            showButtons();
            if(information_mode === false){
                showUserInfo(role);
            }
            // //console.log(squad);
        }
    }

    //return the roster_array index for a user_id
    function getUserIndexById(id){
        var user_index = false;
        // alert(roster_array.length);
        for(var i = 0; i < roster_array.length; i++){
            if(roster_array[i].id == id){
                user_index = i;
                break;
            }
        }
        return user_index;
    }

    function getSquadIndexById(id){
        var squad_index = false;
        for(var i = 0; i < squad_array.length; i++){
            if(squad_array[i].id == id){
                squad_index = i;
                break;
            }
        }
        return squad_index;
    }

    function toggleInfoMode(){
        if(information_mode === false){
            information_mode = true;
            $('#toggleInfoModeButton').addClass('selected');//add selected to the toggle button
            $('.teamCircleTitle').hide();
            $('#infoModeTitleDiv').show();
            $('#userInfoPanel').hide();
            $('#roleInfoPanel').show();
        }else{
            information_mode = false;
            $('#toggleInfoModeButton').removeClass('selected');
            $('#roleInfoPanel').hide();
            $('#userInfoPanel').show();
            $('#infoModeTitleDiv').hide();
            if(squad.id !== null){
                $('#loadTeamTitleDiv').show();
            }else{
                $('#newTeamTitleDiv').show();
            }
        }
    }

    function showUserInfoModal(roster_index){
        $('#currentUserIndexModal').val(roster_index);

        var user = roster_array[roster_index];

        document.getElementById('userInfoNameModal').innerHTML = user.name;
        document.getElementById('userInfoEmailModal').innerHTML = user.email;
        $('#userInfoEmailModal').attr('href','mailto:' + user.email);
        document.getElementById('userInfoPhoneModal').innerHTML = user.phone;
        //document.getElementById('userInfoDescriptionModal').innerHTML = user.description;
        if(user.description == "" || user.description == null){
            document.getElementById('userInfoDescriptionModal').innerHTML = "No additional info for this user";
        }else{
            document.getElementById('userInfoDescriptionModal').innerHTML = user.description;
        }
        //$('#teamIconDisplay').css("background-image", "url('/images/avatars/teamIcons/noTeam.png')");
        $('#userInfoUserIcon').css("background-image", "url('" + user.picture_location + "')");


        var userTeamListHTML = "";
        var i = 0;
        var l = user.company_squad_list.length;
        for(i; i < l; i++){

            userTeamListHTML+= "<li data-teamid='" + user.company_squad_list[i].id + "' class='userSquadListItem'><a><div class='teamIcon' style='background-image:url(\"" + user.company_squad_list[i].avatar_location + "\")'></div><span>" + user.company_squad_list[i].squad_name + "</span></a></li>";
        }
        document.getElementById('userInfoTeamListModal').innerHTML = userTeamListHTML;

    }

    function showUserInfo(role){
        var role_category = "required";
        switch(role){
            case "chief_executive":
                $('#detailHeaderColor').attr("class", "iconBg roleRedbg");
                $('#detailHeaderIcon').attr("class", "icon icon-exec");
                $('#roleDetailTitle').html("Chief Executive");
                break;

            case "chief_navigator":
                $('#detailHeaderColor').attr("class", "iconBg roleRedbg");
                $('#detailHeaderIcon').attr("class", "icon icon-reuser");
                $('#roleDetailTitle').html("Chief Navigator");

                break;

            case "claim_manager":
                $('#detailHeaderColor').attr("class", "iconBg roleOrangebg");
                $('#detailHeaderIcon').attr("class", "icon icon-injuryreport");
                $('#roleDetailTitle').html("Claim Manager");
                role_category = "suggested";
                break;

            case "work_comp_agent":
                $('#detailHeaderColor').attr("class", "iconBg roleOrangebg");
                $('#detailHeaderIcon').attr("class", "icon icon-insurance");
                $('#roleDetailTitle').html("Work Comp Agent");
                role_category = "suggested";

                break;

            case "customer_service_agent":
                $('#detailHeaderColor').attr("class", "iconBg roleOrangebg");
                $('#detailHeaderIcon').attr("class", "icon icon-support");
                $('#roleDetailTitle').html("Customer Service Agent");
                role_category = "suggested";

                break;

            case "safety_manager":
                $('#detailHeaderColor').attr("class", "iconBg roleOrangebg");
                $('#detailHeaderIcon').attr("class", "icon icon-exclamation-triangle");
                $('#roleDetailTitle').html("Safety Manager");
                role_category = "suggested";

                break;

            case "supervisor":
                $('#detailHeaderColor').attr("class", "iconBg roleOrangebg");
                $('#detailHeaderIcon').attr("class", "icon icon-hardhat");
                $('#roleDetailTitle').html("Supervisor");
                role_category = "suggested";

                break;

            case "doctor":
                $('#detailHeaderColor').attr("class", "iconBg roleGreenbg");
                $('#detailHeaderIcon').attr("class", "icon icon-doctor");
                $('#roleDetailTitle').html("Doctor");
                role_category = "optional";

                break;

            case "physical_therapist":
                $('#detailHeaderColor').attr("class", "iconBg roleGreenbg");
                $('#detailHeaderIcon').attr("class", "icon icon-rehab");
                $('#roleDetailTitle').html("Physical Therapist");
                role_category = "optional";

                break;

            case "other":
                $('#detailHeaderColor').attr("class", "iconBg roleGreenbg");
                $('#detailHeaderIcon').attr("class", "icon icon-users");
                $('#roleDetailTitle').html("Other");
                role_category = "optional";

                break;
        }
        //get the role panel div
        //var rolePanel = $('.uniqueDetails[data-role="' + role + '"]');
        $('.roleDetailDiv').hide();
        var rolePanel = $('.roleDetailDiv[data-role="' + role + '"]');

        rolePanel.show();

        //var role_users contains an array of user IDs for each user in that role
        var role_users = squad[role];

        //check if the role is currently filled
        if(role_users.length > 0) {
            rolePanel.find('.roleEmpty').hide();
            rolePanel.find('.roleFilled').show();
           
            var user_html = "";
            for(var i = 0; i < role_users.length; i++){
                //var user =
                ////console.log("user");
                ////console.log(user);
                var user_id = role_users[i];
                var roster_index = getUserIndexById(user_id);
                var user = roster_array[roster_index];
                user_html += '<li class="' + role_category + ' roleUser" data-zagentID="' + user.zagent_id + '" data-userID="' + user.id + '" data-name="'+ user.name +'" data-userRole="' + role + '" data-userIndex="' + roster_index + '">' +
                    '<a class="userListUser">' +
                    '<div class="userAvatar" style="background-image:url(' + user.picture_location + ')"></div>' +
                    '<div class="userName">' + 	user.name + '</div>' +
                    '<div class="userEmail">' + user.email + '</div>' +
                    '<div class="zButton row userActions">' +
                    '<button class="buttonHover blue userControlInfo">' +
                    '<div class="icon icon-info-circle"></div><span>info</span>' +
                    '</button>' +
                    '<button class="buttonHover cyan userControlNudge">' +
                    '<div class="icon icon-hand-o-right"></div><span>nudge</span>' +
                    '</button>' +
                    /*'<button class="buttonHover orange userControlChat">' +
                        '<div class="icon icon-comments"></div><span>chat</span>' +
                    '</button>' + */
                    '<button class="buttonHover red userControlDelete">' +
                    '<div class="icon icon-removeuser"></div><span>delete</span>' +
                    '</button>' +
                    '</div>' +
                    '</a>' +

                    '<div class="zButton"><button class="userControlRemove"><div class="icon icon-times"></div>Remove from Role</button></div>' +

                    '</li>';
            }
            rolePanel.find('ul').html(user_html);
            
        }else{
            rolePanel.find('.roleFilled').hide();
            rolePanel.find('.roleEmpty').show();
            /*
            $('#roleCheckIcon').hide();
            $('#userInfoPanel').hide();
            $('#assignedUsersHeader').hide();
            rolePanel.find('.roleDetail').show();
            rolePanel.find('.roleDetailSummary').hide();
            rolePanel.find('.role_exists').hide();
            rolePanel.find('.no_role_selected').show();
            */
        }
        $(".uniqueDetails").hide();
        rolePanel.show();
    }

    /**** USER CONTROLS ****/
    $('#userInfoPanel').on('click', '.openControls', function(){
        var role = $(this).data('userrole');
        var id = $(this).data('userid');
        $('.userControls[data-userRole="' + role + '"][data-userID="' + id + '"]').toggle();
    });

    function sendNudge(userID, companyID) {
        var _token = '<?php echo csrf_token(); ?>';
        $.ajax({
            type: 'POST',
            url: '<?php echo route('pingAgent') ?>',
            data: {
                user_id: userID,
                company_id: companyID,
                _token: _token
            },
            success: function (data) {
                showAlert("They'll get a communication asking them to login!", "confirm", 5);
            }
        })
    }

    function removeUserFromRole(role, user_id){
        ////console.log(squad[role]);
        ////console.log(squad);
        for(var i = 0; i < squad[role].length; i++){
            if(squad[role][i] === user_id){
                squad[role].splice(i, 1);
                break;
            }
        }
        squad.edited = true;
        ////console.log(squad
        ////console.log(squad[role]);
        ////console.log(squad);
        if(squad[role].length > 0){
            //handle the circle avatar
            addUserToCircle(role);
            showUserInfo(role);
        }else{
            switch (role){
                case "chief_executive":
                    resetChiefExecutiveCircle();
                    break;

                case "chief_navigator":
                    resetChiefNavigatorCircle();
                    break;

                case "claim_manager":
                    resetClaimManagerCircle();
                    break;

                case "work_comp_agent":
                    resetWorkCompAgentCircle();
                    break;

                case "customer_service_agent":
                    resetCustomerServiceAgentCircle();
                    break;

                case "safety_manager":
                    resetSafetyManagerCircle();
                    break;

                case "supervisor":
                    resetSupervisorCircle();
                    break;

                case "doctor":
                    resetDoctorCircle();
                    break;

                case "physical_therapist":
                    resetPhysicalTherapistCircle();
                    break;

                case "other":
                    resetOtherCircle();
                    break;
            }

            $('.teamCircleButton[data-role="' + role + '"]').css('borderColor', '#4d4d4d');

            showUserInfo(role);
        }
        showButtons();
    }

    /* Delete a regular user */
    function removeUser(userID){
        var _token = '<?php echo csrf_token(); ?>';
        $.ajax({
            type: 'POST',
            url: '<?php echo route('removeUser') ?>',
            data: {
                user_id: userID,
                company_id: {{$company_id}},
                _token: _token
            },
            success: function (data) {
                $('#deleteUserModal').modal('hide');
                showAlert("User removed! You can add them again whenever you want.", "confirm", 5);
                clearDeletedUser();
            },
            error: function (data){
                ////console.log(data);
                var errorMsg = JSON.parse(data.responseText);
                document.getElementById('deleteUserAlerts').innerHTML = '<div class="alert alert-danger alert-dismissible" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    errorMsg['error'] + "</div>";
            }
        });
    }

    /* Delete a ZAgent ZUser */
    function deleteZAgentZUser(user_id, user_name){
        var _token = '<?php echo csrf_token(); ?>';
        $.ajax({
            type: 'POST',
            url: '<?php echo route('deleteZagentZuser') ?>',
            data: {
                user_id: user_id,
                company_id: {{$company_id}},
                _token: _token
            },
            success: function (data) {
                $('#deleteUserModal').modal('hide');
                showAlert(user_name + " deleted! If you want to add them again, just send them another invite!", "confirm", 5);
                clearDeletedUser();
            },
            error: function (data){
                var response_array = JSON.parse(data.responseText);
                ////console.log(response_array);
                if(response_array.hasOwnProperty('error')){
                    document.getElementById('deleteUserAlerts').innerHTML = '<div class="alert alert-danger alert-dismissible" role="alert">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                        response_array['error'] + "</div>";
                }else {
                    //var l = Object.getOwnPropertyNames(response_array).length;
                    getZagentList(user_id);
                    var l = response_array['length'];
                    var i;
                    var teamString = "";
                    for (i = 0; i < l; i++) {
                        teamString += "<li>" + response_array[i].company_name + "'s team " + response_array[i].squad_name + "</li>";
                    }
                    document.getElementById('deleteZagentTeamListing').innerHTML = teamString;
                    document.getElementById('deleteZagentModalName').innerHTML = user_name;
                    document.getElementById('deleteZagentModalOldUserId').value = user_id;
                    $('#deleteUserModal').modal('hide');
                    $('#deleteZagentModal').modal('show');
                }
            }
        });
    }

    function replaceZagentZuser(old_user_id, new_user_id, old_user_name){
        var _token = '<?php echo csrf_token(); ?>';
        $.ajax({
            type: 'POST',
            url: '<?php echo route('replaceZagentZuser') ?>',
            data: {_token: _token,
                old_user_id: old_user_id,
                new_user_id: new_user_id},
            success: function(data){
                deleteZAgentZUser(old_user_id, old_user_name);
            },
            error: function(data){
                showAlert('Sorry, there was an error. Please try again later.', "deny", 5);
            }
        });
    }

    $('#deleteZagentAgentListing').on('click', '.zagentListItem', function(){
        $('#deleteZagentModal').modal('hide');
        var new_user_id = $(this).data('user');
        var old_user_id = document.getElementById('deleteZagentModalOldUserId').value;
        var user_name = document.getElementById('deleteZagentModalName').innerHTML;
        replaceZagentZuser(old_user_id, new_user_id, user_name);
    });

    function getZagentList(user_id){
        //getZagentList
        var _token = '<?php echo csrf_token(); ?>';
        $.ajax({
            type: 'POST',
            url: '<?php echo route('getZagentList') ?>',
            data: {_token: _token, company_id: {{$company_id}}},
            success: function(data){
                ////console.log(data);
                var agent_array = data;
                var i;
                var l = agent_array.length;
                var agent_string = "";
                for(i = 0; i < l; i++){
                    if(agent_array[i].id != user_id) {
                        agent_string += "<li class='zagentListItem' data-user='" + agent_array[i].id + "'>" + agent_array[i].name + "</li>";
                    }
                }
                document.getElementById('deleteZagentAgentListing').innerHTML = agent_string;
            }
        });
    }

    function launchDeleteUserModal(userid, name, zagent_id){
        document.getElementById('deleteUserAlerts').innerHTML = "";
        document.getElementById('deleteUserModalName').innerHTML = name;
        document.getElementById('deleteUserModalId').value = userid;
        if(zagent_id == null){
            zagent_id = "null";
        }
        document.getElementById('deleteUserModalZagent').value = zagent_id;
        $('#deleteUserModal').modal('show');
    }

    function clearDeletedUser(){
        loadRoster();
        if(squad.id !== null){
            loadSquad(squad.id, 'active');
        }else{
            resetSquad();
        }
    }

    /* INVITES */
    var newUserType = "";

    $('.userTypeButton').on('click', function(){
        $('.userTypeButton').removeClass('selected');
        $(this).addClass('selected');
        newUserType = $(this).data('classification');
    })

    

    function renameSquad(squad_name, squad_id) {
        if (squad_name !== "" && squad_name !== undefined && squad_id !== null) {
            var _token = '<?php echo csrf_token(); ?>';
            $.ajax({
                type: 'POST',
                url: '<?php echo route('renameSquad') ?>',
                data: {
                    //companyID: companyID,
                    squad_name: squad_name,
                    squad_id: squad_id,
                    _token: _token
                },
                success: function (data) {
                    //loadSquad(squad.id);
                    getSquadList("premade");
                    if(squad_id == squad.id){
                        $('#teamNameTitle').html(squad_name);
                    }
                    //$('#renameTeamModal').modal('hide');
                    showAlert("Your team has been renamed!", "confirm", 5);
                }
            });

        }
    }

    function checkForInjury(){
        var injury = '<?php echo (isset($injury) ? $injury : "false")?>';
        if(injury !== "false"){
            var injury_squad_id = '<?php echo (isset($injury_squad_id) ? $injury_squad_id : "false"); ?>';
            if(injury_squad_id !== "false"){
                loadSquad(injury_squad_id, 'active');
                return true;
            }
        }
        return false;
    }

    function loadSquadToStart(){
        var injury = checkForInjury();
        if(injury === false){
            var squad_id = '<?php echo (isset($load_squad) ? $load_squad : "false")?>';
            if(squad_id !== "false"){
                loadSquad(squad_id, 'active');
            }
        }
    }

    $('#loadSquadUserList').on('click', '.userControlInfo', function(){
        var user_index = $(this).closest('.userListItem').data('index');
        var user_id = roster_array[user_index].id;
        showIndividualUserInfo(user_id, "singleUserModalContentDiv");
    });

    $('#userInfoPanel').on('click', '.userControlInfo', function(){
        var user_id = $(this).closest('.roleUser').data('userid');
        showIndividualUserInfo(user_id, "singleUserModalContentDiv");
    });

    $(".roleDetailPanel").on('click', '.userControlInfo', function(){
        var user_id = $(this).closest('.roleUser').data('userid');
        ////console.log('clicked');
        ////console.log(user_id);
        showIndividualUserInfo(user_id, "singleUserModalContentDiv");
    });

    $('#teamListUserInfoPanel').on('click', '.individualUserInfoBackButton', function(){
        $('#teamListUserInfoPanel').hide();
        $('#loadSquadModal').find('.teamUsers').show();
    });

    function showIndividualUserInfo(user_id, target_div){
        var user_index = getUserIndexById(user_id);
        var user = roster_array[user_index];
        if(user.description == "" || user.description == null){
            var description = "No additional info for this user";
        }else{
            var description = user.description;
        }
        var role = $('.teamCircleButton.selected').data('role');
        if(role !== "" && role !== undefined){
            var role_string = "";
            switch(role){
                case "chief_executive":
                    role_string = "Chief Executive";
                    break;

                case "chief_navigator":
                    role_string = "Chief Navigator";
                    break;

                case "claim_manager":
                    role_string = "Claim Manager";
                    break;

                case "work_comp_agent":
                    role_string = "Work Comp Agent";
                    break;

                case "customer_service_agent":
                    role_string = "Customer Service Agent";
                    break;

                case "safety_manager":
                    role_string = "Safety Manager";
                    break;

                case "supervisor":
                    role_string = "Supervisor";
                    break;

                case "doctor":
                    role_string = "Doctor";
                    break;

                case "physical_therapist":
                    role_string = "Physical Therapist";
                    break;

                case "other":
                    role_string = "Other";
                    break;

            }
            var role_button_html = '<div data-role="' + role + '" data-userindex="' + user_index + '"  class="addUserFromUserlistButton zButton small center" style="margin-top: 10px !important;">' +
                '<button class="buttonHover slate"><div class="icon icon-plusheart"></div>' +
                '<span class="addUserFromUserlistLabel"> Add to ' + role_string + '</span></button>' +
                '</div>';
        }else{
            var role_button_html = "";
        }
        var user_teamlist_HTML = "";
        var i = 0;
        var l = user.company_squad_list.length;
        for(i; i < l; i++){
            user_teamlist_HTML+= "<li data-teamid='" + user.company_squad_list[i].id + "' class='userSquadListItem'><a><div class='teamIcon' style='background-image:url(\"" + user.company_squad_list[i].avatar_location + "\")'></div><span>" + user.company_squad_list[i].squad_name + "</span></a></li>";
        }

        var html = '<div class="userPanel">' +
            '<input val="' + user_index + '" class="userPanelCurrentUserIndex" style="display: none">' +
            '<div class="userPanelHeader">' +

            '<div class="zButton small individualUserInfoBackButton" style="display: none"><button class= buttonHover red"><div class="icon icon-sign-in"></div> go back</button></div>' +

            '<div class="userIconContainer"><div class="userIcon" style="background-image:url(\'' + user.picture_location + '\'"></div></div>' +
            '<div class="userName userInfoNameModal">' + user.name + '</div>' +
            '</div>' +
            '<div class="zButton row small center">' +
            '<button data-userindex="' + user_index + '" class="userInfoNudgeButtonModal buttonHover cyan"><div class="icon icon-hand-o-right"></div> nudge</button>' +
            /*'<button data-userindex="' + user_index + '"  class="userInfoChatButtonModal buttonHover orange"><div class="icon icon-comments"></div> chat</button>' +*/
            '<button data-userindex="' + user_index + '"  class="userInfoDeleteButtonModal buttonHover red"><div class="icon icon-removeuser"></div> delete</button>' +
            '</div>' +
            role_button_html +
            '<ul class="userInfo">' +
            '<li class="userEmail">' +
            '<a href="mailto:' + user.email + '" class="userInfoEmailModal">' + user.email + '</a>' +
            '</li>' +
            '<li class="userPhone">' +
            '<span id="userInfoPhoneModal">' + user.phone + '</span>' +
            '</li>' +
            '<li class="userInfo">' +
            '<span class="infoTitle">User Notes</span>' +
            '<span class="userInfoDescriptionModal">' + description + '</span>' +
            '</li>' +
            '<li class="userTeams">' +
            '<span class="infoTitle">Team Assignements</span>' +
            '<ul class="teamList userInfoTeamListModal">' +
            user_teamlist_HTML +
            '</ul>' +
            '</li>' +
            '</ul>' +
            '</div>';

        var team_modal_open = "false";
        if($('#loadSquadModal').hasClass('show')){
            ////console.log('modal has class');
            team_modal_open = "true";
            $('#loadSquadModal').modal('hide');
        }else{
            ////console.log("modal has no class");
        }
        $('#reopenTeamOnClose').val(team_modal_open);
        //$('#singleUserFullscreenModal').data("teammodalopen", team_modal_open);
        ////console.log($('#singleUserFullscreenModal').attr("data-teammodalopen"));

        if(target_div === "singleUserModalContentDiv"){
            document.getElementById(target_div).innerHTML = html;
            $('#singleUserModalFullscreen').modal('show');
            $('#singleUserModalFullscreen').find('.userPanel').show();
        }else if(target_div === "teamListUserInfoPanel"){
            // alert('team target div');
            document.getElementById(target_div).innerHTML = html;
            $('#loadSquadModal').find('.teamUsers').hide();
            $('#teamListUserInfoPanel').show();
            $('#teamListUserInfoPanel').find('.userPanel').show();
            $('#teamListUserInfoPanel').find('.individualUserInfoBackButton').show();
        }else if(target_div === "userInfoPanelModal"){
            document.getElementById(target_div).innerHTML = html;
            $('#userInfoPanelModal').find('.userPanel').show();
        }

    }

    $('#singleUserModalFullscreen').on('hidden.bs.modal', function(){
        //var team_modal_open = $("#singleUserModalFullscreen").data("teammodalopen");
        var team_modal_open = $('#reopenTeamOnClose').val();
        ////console.log(team_modal_open);
        if(team_modal_open === "true"){
            ////console.log('opening load squad modal');
            $('#loadSquadModal').modal('show');
        }else{
            ////console.log('not opening load squad modal');
        }
    });

    function showUserModal(){
        var role = $('.teamCircleButton.selected').data('role');
        $('#userInfoPanelModal').hide();
        $('.userListItem').removeClass('selected');
        $('#userInfoDefaultDiv').show();
        if(role === undefined || role === null){
            document.getElementById('modalFullscreenCurrentActionSpan').innerHTML = "View Users.";
            $('#addUserFromUserlistButton').hide();
        }else{
            switch (role){
                case "chief_executive":
                    document.getElementById('modalFullscreenCurrentActionSpan').innerHTML = "Click user to add them to the Chief Executive role.";
                    $('#addUserFromUserlistLabel').html("Add Chief Executive");
                    break;

                case "chief_navigator":
                    document.getElementById('modalFullscreenCurrentActionSpan').innerHTML = "Click user to add them to the Chief Navigator role.";
                    $('#addUserFromUserlistLabel').html("Add Chief Navigator");
                    break;

                case "claim_manager":
                    document.getElementById('modalFullscreenCurrentActionSpan').innerHTML = "Click user to add them to the Claim Manager role.";
                    $('#addUserFromUserlistLabel').html("Add Claim Manager");
                    break;

                case "work_comp_agent":
                    document.getElementById('modalFullscreenCurrentActionSpan').innerHTML = "Click user to add them to the Work Comp Agent role.";
                    $('#addUserFromUserlistLabel').html("Add Work Comp Agent");
                    break;

                case "customer_service_agent":
                    document.getElementById('modalFullscreenCurrentActionSpan').innerHTML = "Click user to add them to the Customer Service Agent role.";
                    $('#addUserFromUserlistLabel').html("Add Customer Service Agent");
                    break;

                case "safety_manager":
                    document.getElementById('modalFullscreenCurrentActionSpan').innerHTML = "Click user to add them to the Safety Manager role.";
                    $('#addUserFromUserlistLabel').html("Add Safety Manager");
                    break;

                case "supervisor":
                    document.getElementById('modalFullscreenCurrentActionSpan').innerHTML = "Click user to add them to the Supervisor role.";
                    $('#addUserFromUserlistLabel').html("Add Supervisor");
                    break;

                case "doctor":
                    document.getElementById('modalFullscreenCurrentActionSpan').innerHTML = "Click user to add them to the Doctor role.";
                    $('#addUserFromUserlistLabel').html("Add Doctor");
                    break;

                case "physical_therapist":
                    document.getElementById('modalFullscreenCurrentActionSpan').innerHTML = "Click user to add them to the Physical Therapist role.";
                    $('#addUserFromUserlistLabel').html("Add Physical Therapist");
                    break;

                case "other":
                    document.getElementById('modalFullscreenCurrentActionSpan').innerHTML = "Click user to add them to the Other role.";
                    $('#addUserFromUserlistLabel').html("Add Other User");
                    break;
            }
            $('#addUserFromUserlistButton').show();
        }
        $("#modal-fullscreen").modal('show');
    }

    function showUserModalViewOnly(){
        var role = $('.teamCircleButton.selected').data('role');
        $('#userInfoPanelModal').hide();
        $('#userInfoDefaultDiv').show();
        $('#addUserFromUserlistButton').hide();
        document.getElementById('modalFullscreenCurrentActionSpan').innerHTML = "View Users.";
        $('.userListItem').removeClass('selected');
        $("#modal-fullscreen").modal('show');
        
    }



    //***** AVATAR CODE ******

    function launchConfigureTeamModal(squad_id, edit_name_boolean){
        var current_squad_id = $('#configureTeamID').val();
        //handle loading a different team
        if(current_squad_id != squad_id && squad_id !== null){
            ////console.log("loading new squad");
            ////console.log(current_squad_id);
            ////console.log(squad_id);
            var squad = squad_array[getSquadIndexById(squad_id)];
            ////console.log(squad);
            setAvatarImage(squad.avatar_location, squad.avatar_color, squad.background_color, squad.avatar_icon_color, squad.avatar_number, false);
            $('#configureTeamID').val(squad.id);
            $('#configureTeamNameDisplay').html(squad.squad_name);
            $('#configureTeamNameInput').val("").attr("placeholder", squad.squad_name);
            $('#saveAvatar').attr("class", "ghost");
            $('#resetAvatar').attr("class", "ghost");
            if(edit_name_boolean === true){
                $('#configureTeamNameDisplay').hide();
                $('#configureTeamNameEdit').show();
                $('#avatarModal').modal('show');
                //$('#configureTeamNameInput').focus();
            }else{
                $('#configureTeamNameDisplay').show();
                $('#configureTeamNameEdit').hide();
                $('#avatarModal').modal('show');
            }
            //prep the modal for a new team being created
        }else if(squad_id === null){
            $('#configureTeamID').val("newteam");
            $('#saveAvatar').attr("class", "buttonCover green");
            $('#resetAvatar').attr("class", "ghost");
            $('#configureTeamNameDisplay').hide();
            $('#configureTeamNameEdit').show();
            createRandomAvatar(16, 16, 16, 16);
            $('#avatarModal').modal('show');

            //don't looad a different team, just show what you've got
        }else{
            $('#avatarModal').modal('show');
        }
    }

    $('#loadSquadModalEditButton').on('click', function(){
        var squad_id = $('.loadSquadListItem.selected').data('squadid');
        $('#loadSquadModal').modal('hide');
        launchConfigureTeamModal(squad_id, false);
    });


    $('#configureTeamNameDisplay').on('click', function(){
        $(this).hide();
        $('#configureTeamNameEdit').show();
        //$('#saveAvatar').attr("class", "buttonHover green"); SH - Save button fix so users don't think the button is disabled.
        $('#saveAvatar').attr("class", "buttonCover green");
        $('#resetAvatar').attr("class", "buttonHover brick");
    });

    /*
    $('#configureTeamNameInput').on('keyup', function(){
        $('#saveAvatar').attr("class", "buttonHover green");
        $('#resetAvatar').attr("class", "buttonHover brick");
    });
    */
    /*
    function setColors(colorset, gridID, activeColor) {
        ////console.log("set colors");
        var arrayLength = colorset.length;
        var grid = $('#' + gridID).find('.colorCell').toArray();
        $('#' + gridID).find('.active').removeClass('active');
        for (var i = 0; i < arrayLength;) {
            //var selector = 'cell' + (i+1);
            //var cell = document.getElementById(selector).style.backgroundColor = colors[i].hex;
            grid[i].style.backgroundColor = "";
            grid[i].style.backgroundColor = colorset[i].hex;
            if (grid[i].style.backgroundColor == activeColor){
                grid[i].className += ' active'; //please note space
                grid[i].click();
            }
            i++;
        }
    }
    */

    var foregrounds16 = [
        {name: '', hex: '#FF0000'},
        {name: '', hex: '#8B0000'},
        {name: '', hex: '#FF4500'},
        {name: '', hex: '#FF8C00'},
        {name: '', hex: '#FFD700'},
        {name: '', hex: '#808000'},
        {name: '', hex: '#32CD32'},
        {name: '', hex: '#20B2AA'},
        {name: '', hex: '#1E90FF'},
        {name: '', hex: '#0000CD'},
        {name: '', hex: '#FF00FF'},
        {name: '', hex: '#9400D3'},
        {name: '', hex: '#FF69B4'},
        {name: '', hex: '#A0522D'},
        {name: '', hex: '#778899'},
        {name: '', hex: '#2F4F4F'}
    ];

    //setColors(foregrounds16, 'avatarBackgroundGrid', "rgb(50, 205, 50)");
    //setColors(foregrounds16, 'avatarOutlineGrid', "rgb(50, 205, 50)");
    //setColors(foregrounds16, 'avatarIconColorGrid', null);

    $('.avatarListItem a').on('click', function(){
        $('#saveAvatar').parent().removeClass('ghost');
        var avatar_number = $(this).data('avatar');
        $('.avatarListItem.selected').removeClass('selected');
        $(this).parent('li').addClass('selected');
        //$('#saveAvatar').attr("class", "buttonHover green"); SH - Save button fix
        $('#saveAvatar').attr("class", "buttonCover green");
        $('#resetAvatar').attr("class", "buttonHover brick");
        updatePreviewImage();
        //setPreviewImage(avatar_number);
    });

    function setPreviewImage(avatar_number){
        $('#previewImage').find('span').css("background-image", "url(/images/avatars/avatar_icons/" + avatar_number + ".png)");
    }

    $('#saveAvatar').on('click', function(){
        var squad_id = $('#configureTeamID').val();
        if(squad_id !== null && squad_id !== "newteam"){
            //alert(squad_id);
            var squad = getSquadIndexById(squad_id);
            var new_name = $('#configureTeamNameInput').val();
            var avatar_number = $('.avatarListItem.selected').find('a').data('avatar');
            if(new_name !== squad.squad_name){
                // alert('rename');
                renameSquad(new_name, squad_id);
            }else{
                ////console.log(new_name);
                ////console.log(squad.squad_name);
            }
            if(avatar_number){
                updateAvatarIcon(avatar_number, squad_id);
            }
        }else if(squad_id === "newteam"){
            //alert('newteam');
            setNewTeamAvatarPreview(false);
            saveNewSquad();
        }

        /*
        var avatar_number = $('.avatarListItem.selected').find('a').data('avatar');
        if(avatar_number){
            if(squad.id !== null){
                updateAvatarIcon(avatar_number);
            }else{
                setNewTeamAvatarPreview(false);
                swapTwoModals('avatarModal', 'teamNameModal');
                $('#avatarModal').modal('hide');
                $('#teamNameModal').modal('show');
            }
        }else{
            $.bootstrapGrowl('Please select an avatar first!',{
                offset: {from: 'top', amount: 200},
                align: 'center',
                type: 'warning',
            });
        }
        */
    });

    //**** drawCanvasDiamond() will draw the diamond with border color for team avatar icons
    //buffer_pixels represents the number of pixels that the corners of the diamond should be offset from the border of the canvas.
    //this is necessary so that the points of the corners are not rendered outside of the canvas.
    //if the canvas size changes, play with different values for buffer_pixels
    // until you find one that renders the diamond completely within the borders of the canvas (but as close to them as possible)
    function drawCanvasDiamond(context, buffer_pixels, background_color, outline_color){
        var canvas_height = context.canvas.height;
        var canvas_width = context.canvas.width;
        context.beginPath();
        //top corner
        context.moveTo(canvas_width / 2, canvas_height - buffer_pixels);
        //right corner
        context.lineTo(canvas_width - buffer_pixels, canvas_height / 2);
        //bottom corner
        context.lineTo(canvas_width / 2, buffer_pixels);
        //left side
        context.lineTo(buffer_pixels, canvas_width / 2);
        context.closePath();
        context.fill();
        context.fillStyle = background_color;
        context.fill();
        context.lineWidth = 12;
        context.strokeStyle = outline_color;
        context.stroke();

    }

    function updateAvatarIcon(avatar_number, squad_id){
        ////console.log("update avatar icon");
        var _token = '<?php echo csrf_token(); ?>';

        //get the background and outline colors from the color pickers
        var outline_color = getCurrentOutlineColor();
        var background_color = getCurrentBackgroundColor();
        var icon_color = getCurrentIconColor();
        //var icon_color = getCurrent

        //create the canvas, set the height and width, get the context
        var canvas = document.createElement("canvas");
        canvas.width="256";
        canvas.height = "256";
        var ctx = canvas.getContext( "2d" );
        //draw the diamond
        drawCanvasDiamond(ctx, 8, background_color, outline_color);

        var img = document.createElement("img");
        img.src = 'data:image/svg+xml;base64,' + window.btoa(document.getElementById('teamIcon' + avatar_number).outerHTML);
        //img.setAttribute("src", "/images/avatars/avatar_icons/" + avatar_number + ".png");
        img.onload = function() {
            ctx.drawImage(img, 64, 64, 128, 128);
            //ctx.drawImage(mask, 0, 0);
            var icon = canvas.toDataURL('image/png');
            $.ajax({
                type: 'POST',
                url: '<?php echo route('updateSquadImage') ?>',
                data:{squad_id: squad_id, image: icon, outline_color: outline_color, background_color: background_color, icon_color: icon_color, avatar_number: avatar_number, _token: _token},
                success: function(data){
                    showAlert("Your new icon is saved!", "confirm", 5);

                    //$('#previewImage').find('span').css('background-image', '');
                    $('#previewImage').find('span').css('background-image', 'url(' + icon + ')');
                    if(squad_id == squad.id){
                        $('#teamIconDisplay').css("background-image", "url(" + icon + ")");
                    }
                    getSquadList("premade");

                    //
                    //$('#previewImage').css('background-color', outline_color);

                },
                error: function(data){
                    showAlert("Sorry, an error occurred. Please try again later.", "deny", 5);
                }
            });
        }
    }

    function updatePreviewImage(){
        //get the background and outline colors from the color pickers
        var outline_color = getCurrentOutlineColor();
        var background_color = getCurrentBackgroundColor();
        var icon_color = getCurrentIconColor();
        //var icon_color = getCurrent
        var avatar_number = $('.avatarListItem.selected').find('a').data('avatar');
        if(avatar_number !== undefined){
            //create the canvas, set the height and width, get the context
            var canvas = document.createElement("canvas");
            canvas.width="256";
            canvas.height = "256";
            var ctx = canvas.getContext( "2d" );
            //draw the diamond
            drawCanvasDiamond(ctx, 8, background_color, outline_color);

            var img = document.createElement("img");
            img.src = 'data:image/svg+xml;base64,' + window.btoa(document.getElementById('teamIcon' + avatar_number).outerHTML);
            //img.setAttribute("src", "/images/avatars/avatar_icons/" + avatar_number + ".png");
            img.onload = function() {
                ctx.drawImage(img, 64, 64, 128, 128);
                //ctx.drawImage(mask, 0, 0);
                var icon = canvas.toDataURL('image/png');
                $('#previewImage').find('span').css('background-image', 'url(' + icon + ')');
            }
        }

    }

    function setAvatarImage(image_location, outline_color, background_color, icon_color, avatar_number, load_head_icon){
        ////console.log("set avatar image");
        ////console.log(image_location);
        ////console.log(outline_color);
        ////console.log(background_color);
        var preview_image = $('#previewImage');
        preview_image.html("<span></span>");
        if(image_location !== null){
            //draw the new one
            preview_image.find('span').css('background-image', 'url(' + image_location + ')');
            if(load_head_icon === true){
                $('#teamIconDisplay').css("background-image", "url(" + image_location + ")");
            }
        }else{
            if(load_head_icon === true){
                $('#teamIconDisplay').css("background-image", "url('/images/avatars/teamIcons/noTeam.png')");
            }
        }
        if(outline_color !== null){
            //preview_image.css('background-color', outline_color);
            $('#avatarOutlineGrid .colorCell').each(function(index, el){
                if($(this).css('background-color') == outline_color){
                    $(this).click();
                }
            });
        }else{
            //preview_image.css('background-color', 'grey');
        }
        if(background_color !== null){
            $('#avatarBackgroundGrid .colorCell').each(function(index, el){
                if($(this).css('background-color') == background_color){
                    $(this).click();
                }
            });
        }
        if(icon_color !== null){
            $('#avatarIconColorGrid .colorCell').each(function(index, el){
                if($(this).css('background-color') == icon_color){
                    $(this).click();
                }
            });
        }else{
            $('#avatarIconColorGrid').find('.colorCell').removeClass('active');
        }
        if(avatar_number !== null){
            $('.avatarListItem').find('a[data-avatar="' + avatar_number + '"]').click();
        }else{
            $('.avatarListItem').removeClass('selected');
        }
    }

    $('#avatarBackgroundGrid').on('click', '.colorCell', function(){
        $('#avatarBackgroundGrid').find('.colorCell.active').removeClass('active');
        $(this).addClass('active');
        var backgroundColor = $('#avatarBackgroundGrid').find('.colorCell.active').css('background-color');
        $('.avatarListItem a').css('background-color', backgroundColor);
       // $('#saveAvatar').attr("class", "buttonHover green"); SH - save icon fix
       $('#saveAvatar').attr("class", "buttonCover green");
        $('#resetAvatar').attr("class", "buttonHover brick");
        updatePreviewImage();
    });

    $('#avatarOutlineGrid').on('click', '.colorCell', function(){
        $("#avatarOutlineGrid").find('.colorCell.active').removeClass('active');
        $(this).addClass('active');
        var outlineColor = $(this).css('background-color');
        document.getElementById('avatarOutlineColorStyleBlock').innerHTML = ".avatarListItem.selected a{border:6px solid " + outlineColor + "}";
        //$('#saveAvatar').attr("class", "buttonHover green"); SH - save button fix
        $('#saveAvatar').attr("class", "buttonCover green");
        $('#resetAvatar').attr("class", "buttonHover brick");
        updatePreviewImage();
    });

    $('#avatarIconColorGrid').on('click', '.colorCell', function(){
        $('#avatarIconColorGrid').find('.colorCell.active').removeClass('active');
        $(this).addClass('active');
        var svg_color = $(this).css('background-color');
        $('.cls-1').attr('style', "fill:" + svg_color);
        //$('#saveAvatar').attr("class", "buttonHover green"); SH - save button fix
        $('#saveAvatar').attr("class", "buttonCover green");
        $('#resetAvatar').attr("class", "buttonHover brick");
        updatePreviewImage();
    });

    function getCurrentOutlineColor(){
        ////console.log("get current outline color");
        var outline_color = $('#avatarOutlineGrid').find('.colorCell.active').css('background-color');
        return outline_color;
    }

    function getCurrentBackgroundColor(){
        //console.log("get currrent background color");
        var background_color = $('#avatarBackgroundGrid').find('.colorCell.active').css('background-color');
        return background_color;
    }

    function getCurrentIconColor(){
        var icon_color = $('#avatarIconColorGrid').find('.colorCell.active').css('background-color');
        return icon_color;
    }

    function createRandomAvatar(background_color_count, outline_color_count, icon_color_count, icon_count){
        var minimum = 1;
        var background_number = Math.floor(Math.random() * (background_color_count - minimum + 1)) + minimum;
        var outline_number = Math.floor(Math.random() * (outline_color_count - minimum + 1)) + minimum;
        var icon_color_number = Math.floor(Math.random() * (icon_color_count - minimum + 1)) + minimum;
        var icon_number = Math.floor(Math.random() * (icon_count - minimum + 1)) + minimum;
        var background_selectors = $('#avatarBackgroundGrid .colorCell');
        var outline_selectors = $('#avatarOutlineGrid .colorCell');
        var icon_color_selectors = $('#avatarIconColorGrid .colorCell');
        var image_selectors = $('.avatarListItem a');
        background_selectors[background_number - 1].click();
        outline_selectors[outline_number - 1].click();
        icon_color_selectors[icon_color_number - 1].click();
        image_selectors[icon_number - 1].click();
        updatePreviewImage();
    }

    $('#randomizeAvatarButton').on('click', function(){
        createRandomAvatar(16, 16, 16, 16);
    });

    function clearAvatarBuilder(){
        $('#configureTeamID').val("");
        $('#configureTeamNameDisplay').html("click to add name");
        $('#configureTEamNameInput').val("").attr("placeholder", "click to add name");
        $('#avatarBackgroundGrid').find('.colorCell.active').removeClass('active');
        $('#avatarOutlineGrid').find('.colorCell.active').removeClass('active');
        $('#avatarIconColorGrid').find('.colorCell.active').removeClass('active');
        $('#avatarModal').find('.avatarListItem.selected').removeClass('selected');
    }

    $('#resetAvatar').on('click', function(){
        var team_id = $('#configureTeamID').val();
        if(team_id !== ""){
            clearAvatarBuilder();
            launchConfigureTeamModal(team_id, false);
        }
    });

    function setNewTeamAvatarPreview(random){
        //console.log("set new team avatar preview");
        if(random === true){
            createRandomAvatar(16, 16, 16, 16);
        }
        var background_color = getCurrentBackgroundColor();
        var outline_color = getCurrentOutlineColor();
        var icon_color = getCurrentIconColor();
        var avatar_number = $('.avatarListItem.selected').find('a').data('avatar');
        var canvas = document.createElement("canvas");
        canvas.width="256";
        canvas.height = "256";
        var ctx = canvas.getContext( "2d" );
        //draw the diamond
        drawCanvasDiamond(ctx, 8, background_color, outline_color);

        var img = document.createElement("img");
        //img.setAttribute("src", "/images/avatars/avatar_icons/" + avatar_number + ".png");
        img.src = 'data:image/svg+xml;base64,' + window.btoa(document.getElementById('teamIcon' + avatar_number).outerHTML);
        img.onload = function() {
            //console.log("setNewTeamAvatarPreview img onload");
            ctx.drawImage(img, 64, 64, 128, 128);
            //ctx.drawImage(mask, 0, 0);
            var icon = canvas.toDataURL('image/png');
            img = canvas.toDataURL('image/png');
            $('.avatarPreview').find('span').css('background-image', 'url(' + img + ')');
            //$('.avatarPreview').css('background-color', outline_color);
            squad.avatar_location = img;
            squad.background_color = background_color;
            squad.avatar_color = outline_color;
            squad.icon_color = icon_color;
            squad.avatar_number = avatar_number;
        }
    }

    $('#editNewTeamIconButton').on('click', function(){
        swapTwoModals('teamNameModal', 'avatarModal');
        /*
        $('#teamNameModal').modal('hide');
        $('#avatarModal').modal('show').focus();
        */
    });

    function swapTwoModals(old_modal_id, new_modal_id){
        $('#' + old_modal_id).on('hidden.bs.modal', function(){
            $('#' + new_modal_id).modal('show');
            $('#' + old_modal_id).off();
        });
        $('#' + old_modal_id).modal('hide');
    }


    $('#userListGridView').on('click', function(){
        var selected_user_index = $('.userListItem.selected').data('index');
        //alert(selected_user_index);
        var userList = $('#modal-fullscreen').find('.userList');
        if(userList.hasClass("list")){
            userList.removeClass('list').addClass('grid');
            userlist_current_page = 1;
            userlist_number_per_page = 20;
            $(this).removeClass('ctrlGrid').addClass('ctrlList');
        }else{
            userList.removeClass('grid').addClass('list');
            userlist_current_page = 1;
            userlist_number_per_page = 7;
            $(this).removeClass('ctrlList').addClass('ctrlGrid');
        }
        writeRosterList();
        setSelectedUser(selected_user_index);
    });


    $('#teamListViewToggle').on('click', function(){
        var selected_squad_id = $('.loadSquadListItem.selected').data('squadid');
        var teamList = $('#loadSquadModal').find('.teamList');
        if(teamList.hasClass("list")){
            //switch to grid view
            teamList.removeClass('list').addClass('grid');
            teamlist_current_page = 1;
            teamlist_number_per_page = 20;
            $(this).removeClass('ctrlGrid').addClass('ctrlList');
        }else{
            //switch to list view
            teamList.removeClass('grid').addClass('list');
            teamlist_current_page = 1;
            teamlist_number_per_page = 7;
            $(this).removeClass('ctrlList').addClass('ctrlGrid');
        }
        writeSquadListContents();
        setSelectedSquad(selected_squad_id);
    });

    function setSelectedSquad(selected_squad_id){
        if(selected_squad_id !== null && selected_squad_id !== undefined){
            var selected_element = $('.loadSquadListItem[data-squadid="' + selected_squad_id + '"]');
            //check if the element exists
            if(selected_element.length){
                selected_element.click();
            }else{
                for(i = 0; i < teamlist_page_total; i++){
                    //loop through each page and click the selected element when you find it
                    $('#teamListNextPage').click();
                    selected_element = $('.loadSquadListItem[data-squadid="' + selected_squad_id + '"]');
                    if(selected_element.length){
                        selected_element.click();
                        break;
                    }
                }
            }
        }

    }

    function setSelectedUser(selected_user_index){
        if(selected_user_index !== null && selected_user_index !== undefined){
            var selected_element = $('.userListItem[data-index="' + selected_user_index + '"]');
            //check if the element exists
            if(selected_element.length){
                selected_element.click();
            }else{
                for(i = 0; i < userlist_page_total; i++){
                    $('#userListNexPage').click();
                    selected_element = $('.userListItem[data-index="' + selected_user_index + '"]');
                    if(selected_element.length){
                        selected_element.click();
                        break;
                    }
                }
            }
        }
    }

    function setTeamInfoGraphic(){
        var teamInfoGraphic = $('.teamCircleID');
        if(squad.id !== null){
            teamInfoGraphic.find('.teamName').html(squad.name);
            var member_count = getTotalUserOnTeamCount();
            //set member count
            if(member_count === 1){
                teamInfoGraphic.find('.teamMemberCount').html("(1 member)");
            }else{
                teamInfoGraphic.find('.teamMemberCount').html("(" + member_count + " members)");
            }
            //set team type
            //alert(squad.injury_team);
            if(squad.injury_team === 1){
                teamInfoGraphic.find('.teamType').html("Injury Team");
            }else{
                teamInfoGraphic.find('.teamType').html("Pre-Made Team");
            }
            teamInfoGraphic.find('#teamIconDisplay').css('background-image', "url(" + squad.avatar_location + ")");
            teamInfoGraphic.show();
        }else{
            teamInfoGraphic.hide();
        }

    }

    function getTotalUserOnTeamCount(){
        var count = 0;
        count+= squad.chief_executive.length;
        count+= squad.chief_navigator.length;
        count+= squad.claim_manager.length;
        count+= squad.work_comp_agent.length;
        count+= squad.customer_service_agent.length;
        count+= squad.safety_manager.length;
        count+= squad.supervisor.length;
        count+= squad.doctor.length;
        count+= squad.physical_therapist.length;
        count+= squad.other.length;
        return count;
    }

    //INJURY TEAM CODE
    $('#showInjuryTeams').on('click', function(){
        //re-load the squads, this time only with injury squads
        $('#showInjuryTeams').hide();
        $('#showCompanyTeams').show();
        getSquadList("injury");
    });

    $('#showCompanyTeams').on('click', function(){
        $('#showInjuryTeams').show();
        $('#showCompanyTeams').hide();
        getSquadList("premade");
    });

    $('#showPremadeTeams').on('click', function(){
        //re-load the squads, this time only with pre-made squads
        getSquadList("premade");
        //swap out the buttons
        $('#showPremadeTeams').hide();
        $('#showInjuryTeams').show();
        $('#teamListTitle').html("Premade Teams");
        //reset the current page
        teamlist_current_page = 1;
    });

    //DELETE TEAM CODE

    $('#deleteTeamButton').on('click', function(){
        $('#deleteSquadModalID').val(squad.id);
        $('#deleteSquadModalName').html(squad.name);
        $('#deleteSquadModalErrors').html("").hide();
        $('#deleteSquadModal').modal('show');
    });

    $('#deleteSquadSubmitButton').on('click', function(){
        //console.log('delete button clicked');
        var squad_id = $('#deleteSquadModalID').val();
        //console.log(squad_id);
        //console.log('calling delete team');
        deleteTeam(squad_id);
    });

    function deleteTeam(squad_id){
        //console.log('ajax');
        $.ajax({
            type: 'POST',
            url: '<?php echo route("deleteSquad"); ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                squad_id: squad_id
            },
            success: function(){
                $('#deleteSquadModal').modal('hide');
                showAlert("Team Deleted!", "confirm", 5);
                getSquadList("premade");
                $('#createNewTeamSideButton').click();
            },
            error: function(data){
                showAlert("Team Could Not Be Deleted", "deny", 5);
                $('#deleteSquadModalErrors').html(JSON.parse(data.responseText)).show();
            }

        });
    }

    //invite ZAgents
    $('#inviteNewZAgentSideButton').on('click', function(){
        $('#confirmAddZAgentDiv').show();
        $('#addZAgentDiv').hide();
        $('#addZAgentModal').modal('show');
    });

    $('#continueAddingZAgentButton').on('click', function(){
        $('#confirmAddZAgentDiv').hide();
        $('#addZAgentDiv').show();
    });

    $('#submitZAgentButton').on('click', function(){
        inviteZAgent();
    });

    function inviteZAgent(){
        var email = $('#addZAgentEmail').val();
        var company_id = '<?php echo Auth::user()->zagent_id; ?>';
        if(company_id !== "" && company_id !== undefined && company_id !== null){
            var _token = '<?php echo csrf_token(); ?>';
            $.ajax({
                type: 'POST',
                url: '<?php echo route("myZagentInviteZagent") ?>',
                data: {
                    email: email,
                    company_id: company_id,
                    _token: _token
                },
                success: function (data) {
                    //console.log(data);
                    var response = data;
                    //if the email is taken, show an error
                    if(response['error'] !== undefined){
                        var errorField = document.getElementById('addZAgentErrors');
                        errorField.innerHTML = response['error'];
                        errorField.style.display = "";
                        //if the email is available, show a success message and reset the modal
                    }else if(response['email']){
                        showAlert("Your invitation has been sent!", "confirm", 5);
                        $('#addZAgentModal').modal('hide');
                        $('#addZAgentName').val("");
                        $('#addZAgentEmail').val("");
                    }
                },
                error: function (data) {
                    var errorField = document.getElementById('addZAgentErrors');
                    if(data.status == 500){
                        errorField.innerHTML = "Sorry, an error occurred. " +
                            "Please try again later. We apologize for any inconvenience.";
                        errorField.style.display = "";
                    }else {
                        var error = JSON.parse(data.responseText);
                        if (error['email'] !== undefined) {
                            errorField.innerHTML = error['email'];
                            errorField.style.display = "";
                        }
                    }
                }
            })
        }
    }

    $('body').on('click', '.showAddUserToTeam', function(){
        //get the data attributes
        var title = $(this).data('title');
        var roleType = $(this).data('roletype');
        var role = $(this).data('role');
        //set the title
        $('#modalTitle').html(title);

        //remove all role types
        $('#modalTitle').parent('span').removeClass('roleRequired');
        $('#modalTitle').parent('span').removeClass('roleSuggested');
        $('#modalTitle').parent('span').removeClass('roleOptional');

        //add role type of button clicked
        $('#modalTitle').parent('span').addClass(roleType);

        //save role to hidden input
        $('#modalTitle').attr('role', role);
        
    });

    $('body').on('click', '.addToRoster', function(){
        //get the role
        var role = $('#modalTitle').text();
        role = role.toLowerCase();
        role = role.replace(/ /g,"_");
        //get user index
        var user_index = $(this).data('index');
        //var user = getUserIndexById(user_index);
        var user = roster_array[user_index];
        if($('#manageTeamsTab').css('display') == 'none'){
            //alert('adding to active');
            //adjusting for naming diffs
            if(role === 'other_users'){
                role = 'other';
            }else if(role === "employee_supervisor"){
                role = 'supervisor';
            }else if(role === "medical_support"){
                role = 'doctor';
            }else if(role === "service_agent"){
                role = 'customer_service_agent';
            }else if(role === 'human_resources'){
                role = 'chief_navigator';
            }
            squad[role].push(user.id);
            squad.edited = true;
            console.log(JSON.stringify(squad));
            switch(role){
                case 'physical_therapist':
                    addUserToCircle(role);
                    //alert('physical_therapist');
                    break;
                case 'doctor':
                    addUserToCircle(role);
                    //alert('doctor');
                    break;
                case 'chief_executive':
                    addUserToCircle(role);
                    //alert('chief_executive');
                    break;
                case 'claim_manager':
                    addUserToCircle(role);
                    //alert('claim_manager');
                    break;
                case 'work_comp_agent':
                    addUserToCircle(role);
                    //alert('work_comp_agent');
                    break;
                case 'customer_service_agent':
                    addUserToCircle(role);
                    //alert('customer_service_agent');
                    break;
                case 'safety_manager':
                    addUserToCircle(role);
                    //alert('safety_manager');
                    break;
                case 'supervisor':
                    addUserToCircle(role);
                    //alert('supervisor');
                    break;
                case 'chief_navigator':
                    addUserToCircle(role);
                    //alert('chief_navigator');
                    break;
                case 'other':
                    addUserToCircle(role);
                    
            }
        }else{
            addToNewRoster(user_index, role);
        }
        modal.close();
    });

    function addToNewRoster(user_index, role){
        var user = roster_array[user_index];
        //setUserNewSquad(user);
        console.log(user);
        switch(role){
            case 'physical_therapist':
                setUserNewSquad(user, 'physical_therapist');
                if(new_squad['physical_therapist'].length === 1){
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-rehab"></div>' +
                        '<input type="hidden" class="user_index" value="'+user_index+'">'+
                        '<button class="twoline roleOptional showZenModal" style="display:none;"><div class="icon inline icon-plus"></div> physical<br>therapist</button>' +
                        '<div class="teamUser"><span class="teamUserName">'+user.name+'</span><span class="teamUserRole optional">physical therapist</span></div>' +
                        '<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:'+user.background_color+';background-image:url(\''+user.picture_location+'\');"></div></div>';
                }else{
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-rehab"></div>' +
                        '<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> physical<br>therapist</button>' +
                        '<div class="teamUser"><span class="teamUserName" data-id="'+new_squad['physical_therapist']+'">Multiple Users</span><span class="teamUserRole optional">physical therapist</span></div>' +
                        '<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                }
                //$('#new_'+role).addClass('showUserCard');
                break;
            case 'medical_support':
                setUserNewSquad(user, 'doctor');
                if(new_squad['doctor'].length === 1){
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-doctor"></div>' +
                        '<input type="hidden" class="user_index" value="'+user_index+'">'+
                        '<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> medical<br>support</button>' +
                        '<div class="teamUser"><span class="teamUserName">'+user.name+'</span><span class="teamUserRole optional">medical support</span></div>' +
                        '<div class="teamMemberIcon teamDelay5"><div class="userAvatar" style="background-color:'+user.background_color+';background-image:url(\''+user.picture_location+'\');"></div></div>';
                }else{
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-doctor"></div>' +
                        '<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> medical<br>support</button>' +
                        '<div class="teamUser"><span class="teamUserName" data-id="'+new_squad['doctor']+'">Multiple Users</span><span class="teamUserRole optional">medical support</span></div>' +
                        '<div class="teamMemberIcon teamDelay5"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                }
                //$('#new_'+role).addClass('showUserCard');
                break;
            case 'chief_executive':
                setUserNewSquad(user, 'chief_executive');
                if(new_squad['chief_executive'].length === 1){
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-exec"></div>' +
                        '<input type="hidden" class="user_index" value="'+user_index+'">'+
                        '<button class="twoline roleRequired showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> chief<br>executive</button>' +
                        '<div class="teamUser"><span class="teamUserName">'+user.name+'</span><span class="teamUserRole required">chief executive</span></div>' +
                        '<div class="teamMemberIcon teamDelay1"><div class="userAvatar" style="background-color:'+user.background_color+';background-image:url(\''+user.picture_location+'\');"></div></div>';
                }else{
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-exec"></div>' +
                        '<button class="twoline roleRequired showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> chief<br>executive</button>' +
                        '<div class="teamUser"><span class="teamUserName" data-id="'+new_squad['chief_executive']+'">Multiple Users</span><span class="teamUserRole required">chief executive</span></div>' +
                        '<div class="teamMemberIcon teamDelay1"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                }
                //$('#new_'+role).addClass('showUserCard');
                break;
            case 'claim_manager':
                setUserNewSquad(user, 'claim_manager');
                if(new_squad['claim_manager'].length === 1){
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-injuryreport"></div>'+
                        '<input type="hidden" class="user_index" value="'+user_index+'">'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> claim<br>manager</button>' +
                        '<div class="teamUser"><span class="teamUserName">'+user.name+'</span><span class="teamUserRole suggested">claim manager</span></div>' +
                        '<div class="teamMemberIcon teamDelay9"><div class="userAvatar" style="background-color:'+user.background_color+';background-image:url(\''+user.picture_location+'\');"></div></div>';
                }else{
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-injuryreport"></div>'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> claim<br>manager</button>' +
                        '<div class="teamUser"><span class="teamUserName" data-id="'+new_squad['claim_manager']+'">Multiple Users</span><span class="teamUserRole suggested">claim manager</span></div>' +
                        '<div class="teamMemberIcon teamDelay9"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                }
                //$('#new_'+role).addClass('showUserCard');
                break;
            case 'work_comp_agent':
                setUserNewSquad(user, 'work_comp_agent');
                if(new_squad['work_comp_agent'].length === 1){
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-insurance"></div>'+
                        '<input type="hidden" class="user_index" value="'+user_index+'">'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> work comp<br>agent</button>'+
                        '<div class="teamUser"><span class="teamUserName">'+user.name+'</span><span class="teamUserRole suggested">work comp agent</span></div>'+
                        '<div class="teamMemberIcon teamDelay8"><div class="userAvatar" style="background-color:'+user.background_color+';background-image:url(\''+user.picture_location+'\');"></div></div>';
                }else{
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-insurance"></div>'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> work comp<br>agent</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+new_squad['work_comp_agent']+'">Multiple Users</span><span class="teamUserRole suggested">work comp agent</span></div>'+
                        '<div class="teamMemberIcon teamDelay8"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                }
                //$('#new_'+role).addClass('showUserCard');
                break;
            case 'service_agent':
                setUserNewSquad(user, 'service_agent');
                if(new_squad['customer_service_agent'].length === 1){
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-support"></div>'+
                        '<input type="hidden" class="user_index" value="'+user_index+'">'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> service<br>agent</button>'+
                        '<div class="teamUser"><span class="teamUserName">'+user.name+'</span><span class="teamUserRole suggested">service agent</span></div>'+
                        '<div class="teamMemberIcon teamDelay7"><div class="userAvatar" style="background-color:'+user.background_color+';background-image:url(\''+user.picture_location+'\');"></div></div>';
                }else{
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-support"></div>'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> service<br>agent</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+new_squad['service_agent']+'">Multiple Users</span><span class="teamUserRole suggested">service agent</span></div>'+
                        '<div class="teamMemberIcon teamDelay7"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                }
                //$('#new_'+role).addClass('showUserCard');
                break;
            case 'safety_manager':
                setUserNewSquad(user, 'safety_manager');
                if(new_squad['safety_manager'].length === 1){
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-exclamation-triangle"></div>'+
                        '<input type="hidden" class="user_index" value="'+user_index+'">'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> safety<br>manager</button>'+
                        '<div class="teamUser"><span class="teamUserName">'+user.name+'</span><span class="teamUserRole suggested">safety manager</span></div>'+
                        '<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:'+user.background_color+';background-image:url(\''+user.picture_location+'\');"></div></div>';
                }else{
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-exclamation-triangle"></div>'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> safety<br>manager</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+new_squad['safety_manager']+'">Multiple Users</span><span class="teamUserRole suggested">safety manager</span></div>'+
                        '<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                }
                //$('#new_'+role).addClass('showUserCard');
                break;
            case 'employee_supervisor':
                setUserNewSquad(user, 'supervisor');
                if(new_squad['supervisor'].length === 1){
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-hardhat"></div>'+
                        '<input type="hidden" class="user_index" value="'+user_index+'">'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> employee<br>supervisor</button>'+
                        '<div class="teamUser"><span class="teamUserName">'+user.name+'</span><span class="teamUserRole suggested">employee supervisor</span></div>'+
                        '<div class="teamMemberIcon teamDelay3"><div class="userAvatar" style="background-color:'+user.background_color+';background-image:url(\''+user.picture_location+'\');"></div></div>';
                }else{
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-hardhat"></div>'+
                        '<button class="twoline roleSuggested showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> employee<br>supervisor</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+new_squad['supervisor']+'">Multiple Users</span><span class="teamUserRole suggested">employee supervisor</span></div>'+
                        '<div class="teamMemberIcon teamDelay3"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                }
                //$('#new_'+role).addClass('showUserCard');
                break;
            case 'human_resources':
                setUserNewSquad(user, 'chief_navigator');
                if(new_squad['chief_navigator'].length === 1){
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-reuser"></div>'+
                        '<input type="hidden" class="user_index" value="'+user_index+'">'+
                        '<button class="twoline roleRequired showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> human<br>resources</button>'+
                        '<div class="teamUser"><span class="teamUserName">'+user.name+'</span><span class="teamUserRole required">human resources</span></div>'+
                        '<div class="teamMemberIcon teamDelay10"><div class=" userAvatar" style="background-color:'+user.background_color+';background-image:url(\''+user.picture_location+'\');"></div></div>';
                }else{
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-reuser"></div>'+
                        '<button class="twoline roleRequired showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> human<br>resources</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+new_squad['chief_navigator']+'">Multiple Users</span><span class="teamUserRole required">human resources</span></div>'+
                        '<div class="teamMemberIcon teamDelay10"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                }
                //$('#new_'+role).addClass('showUserCard');
                break;
            case 'other_users':
                setUserNewSquad(user, 'other');
                if(new_squad['other'].length === 1){
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-adduser"></div>'+
                        '<input type="hidden" class="user_index" data-id="'+user_index+'">'+
                        '<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> other<br>users</button>'+
                        '<div class="teamUser"><span class="teamUserName">'+user.name+'</span><span class="teamUserRole optional">other user(s)</span></div>'+
                        '<div class="teamMemberIcon teamDelay6"><div class="userAvatar" style="background-color:'+user.background_color+';background-image:url(\''+user.picture_location+'\');"></div></div>';
                }else{
                    document.getElementById('new_'+role).innerHTML = '<div class="icon teamRoleTypeIcon icon-adduser"></div>'+
                        '<button class="twoline roleOptional showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> other<br>users</button>'+
                        '<div class="teamUser"><span class="teamUserName" data-id="'+ userArray +'">Multiple Users</span><span class="teamUserRole optional">other user(s)</span></div>'+
                        '<div class="teamMemberIcon teamDelay6"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
                }
                //$('#new_'+role).addClass('showUserCard');
                    
        }
        console.log(JSON.stringify(new_squad));
        modal.close();
        //showAddUserToTeamModal.close();
        //$('#new_'+role).attr('userindex', user_index);
        $('#new_'+role).addClass('showUserCard');
        $('#new_'+role).addClass('showZenModal');
        $('#new_'+role).attr('data-modalcontent', '#user_card');
        
        var valid = validateNewSquad();
        if(valid){
            $('#saveTeam').removeClass('ghost');
            $('#saveTeam').addClass('showZenModal');
            $('#saveTeam').attr('data-modalcontent', '#popmessageModal');
        }

        //$('#new_'+role).attr('id', user_index);
    }

    function setUserNewSquad(user, role){
        switch (role) {
            case 'physical_therapist':
                new_squad.physical_therapist[new_squad.physical_therapist.length] = user.id;
                break;
            case 'doctor':
                new_squad.doctor[new_squad.doctor.length] = user.id;
                break;
            case 'chief_executive':
                new_squad.chief_executive[new_squad.chief_executive.length] = user.id;
                break;
            case 'claim_manager':
                new_squad.claim_manager[new_squad.claim_manager.length] = user.id;
                break;
            case 'work_comp_agent':
                new_squad.work_comp_agent[new_squad.work_comp_agent.length] = user.id;
                break;
            case 'service_agent':
                new_squad.customer_service_agent[new_squad.customer_service_agent.length] = user.id;
                break;
            case 'safety_manager':
                new_squad.safety_manager[new_squad.safety_manager.length] = user.id;
                break;
            case 'supervisor':
                new_squad.supervisor[new_squad.supervisor.length] = user.id;
                break;
            case 'chief_navigator':
                new_squad.chief_navigator[new_squad.chief_navigator.length] = user.id;
                break;
            case 'other':
                new_squad.other[new_squad.other.length] = user.id;
        }
    }

    $('#saveTeam').on('click', function(){
        //console.log("new squad: " + JSON.stringify(new_squad));
        var templateName = $('#currentTeamTemplateTitle').text();
        $('#popModalMessage').html('Save "'+templateName+'" as a template?');
        
    });

    $('#confirmSaveTemplate').on('click', function(){
        console.log("new squad: " + JSON.stringify(new_squad));
        
        var valid = validateNewSquad();
        if(valid){
            saveNewSquad();
        }
        
    });

    $('body').on('click', '.showUserCard', function(){
        console.log('usercard Clicked');
        if($(this).hasClass('teamSetPanel')){
            //if showUserCard also has teamSetPanel that means its from the active teams pannel
            user_role = $(this).children('div').attr('id');
            if(user_role != undefined){
                getUsersForUserCard(user_role, squad);
            }
        }else{
            //else means its the team templates pannel
            console.log('from team template pannel');
            var user_role = $(this).attr('id');
            console.log("user role: " + user_role);
            //make sure role is set before showing the card
            if(user_role != undefined){
                user_role = user_role.substring(4);
                //send new squad object instead of squad
                getUsersForUserCard(user_role, new_squad);
            }else{
                var roster_index = $(this).data('index');
                showUserCardForManagement(roster_index);
            }
        }
        /*
        
        */
        //console.log($(this));
        
    });

    function showUserCardForManagement(roster_index){
        console.log('roster_index: ' + roster_index);
        var user = roster_array[roster_index];
        //split the last name and first name
        var name = user.name;
        name = name.split(' ');

        var description = "";
        if(user.description === null || user.description === ""){
            description = "not entered";
        }else{
            description = user.description;
        }
        var html = '';

        html += '<div class="flippableCard" style="display: block">'+
                    '<div class="userCard">'+
                        '<div class="cardFront">' +
                            '<div class="header '+user.zengarden_theme+'"><div class="headerBG themeBG"></div></div>' +
                            '<div class="cardZenRating menuWidget">' +
                                '<div href="/zenrating" class="widget">' +
                                    '<span class="leftImage"><img src="/images/icons/zenrating.png"></span>'+
                                    '<span class="rate lv2">73</span>'+
                                '</div>'+
                            '</div>'+
                            '<div class="userCardAvatarContainer"><div class="userCardAvatar" style="background-color:'+user.background_color+';background-image:url(\''+user.picture_location+'\');"></div></div>' +
                            '<div class="firstname">'+name[0]+'</div>' +
                            '<div class="lastname">'+name[1]+'</div>'+                 
                            '<div class="cardFrontContent">' +
                                '<hr>' +
                                '<fieldset class="centered"><legend>Description</legend>'+
                                    '<div class="descriptionDisplay">'+description+'</div>'+
                                '</fieldset>'+
                                '<div class="cardElement email"><a href="mailto:'+user.email+'">'+user.email+'</a></div>'+
                            '</div>'+
                            '<div class="mainButton">';
                            if(user.is_zagent === 0){
                               html+= '<button class="removeUserFromTeam red centered showZenModal" data-modalcontent="#popmessageModal2" data-remove="user" data-userid="'+user.id+'" data-name="'+name[0]+' '+name[1]+'">delete user</button>';
                            }else if(user.is_zagent === 1){
                                @if(Auth::user()->is_zagent)
                                    html += '<button class="removeUserFromTeam red centered showZenModal" data-modalcontent="#popmessageModal2" data-remove="agent" data-userid="'+user.id+'" data-name="'+name[0]+' '+name[1]+'">delete zagent</button>';
                                @endif
                            }
                    html+= '</div>' +
                        '</div>'+
                    '</div>'+
                '</div>';

        $('#addNewUserToRole').hide();
        $('#displayMultipleUsers').hide();
        $('#cardContainer').html(html);
    }

    function getUsersForUserCard(user_role, current_squad){
        console.log('user_role: ' + user_role);
        
        //fixed different names between both pannels
        if(user_role === 'other_users'){
            user_role = 'other';
        }else if(user_role === "employee_supervisor"){
            user_role = 'supervisor';
        }else if( user_role === "medical_support"){
            user_role = 'doctor';
        }else if(user_role === "service_agent"){
            user_role = 'customer_service_agent';
        }else if(user_role === 'human_resources'){
            user_role = 'chief_navigator';
        }
        switch(user_role){
            case 'chief_executive':
                console.log(current_squad.chief_executive);
                if(current_squad.chief_executive != 0){
                    addUsersToUserCard(current_squad.chief_executive, user_role, current_squad);
                }
                break;
            case 'chief_navigator':
                console.log(current_squad.chief_navigator);
                if(current_squad.chief_navigator != 0){
                    addUsersToUserCard(current_squad.chief_navigator, user_role, current_squad);
                }
                break;
            case 'claim_manager':
                console.log(current_squad.claim_manager, user_role, current_squad);
                if(current_squad.claim_manager != 0){
                    addUsersToUserCard(current_squad.claim_manager, user_role, current_squad);
                }
                break;
            case 'work_comp_agent':
                console.log(current_squad.work_comp_agent);
                if(current_squad.work_comp_agent != 0){
                    addUsersToUserCard(current_squad.work_comp_agent, user_role, current_squad);
                }
                break;
            case 'customer_service_agent':
                console.log(current_squad.customer_service_agent);
                if(current_squad.customer_service_agent != 0){
                    addUsersToUserCard(current_squad.customer_service_agent, user_role, current_squad);
                }
                break;
            case 'safety_manager':
                console.log(current_squad.safety_manager);
                if(current_squad.safety_manager != 0){
                    addUsersToUserCard(current_squad.safety_manager, user_role, current_squad);
                }
                break;
            case 'supervisor':
                console.log(current_squad.supervisor);
                if(current_squad.supervisor != 0){
                    addUsersToUserCard(current_squad.supervisor, user_role, current_squad);
                }
                break;
            case 'doctor':
                console.log(current_squad.doctor);
                if(current_squad.doctor != 0){
                    addUsersToUserCard(current_squad.doctor, user_role, current_squad);
                }
                break;
            case 'physical_therapist':
                console.log(current_squad.physical_therapist);
                if(current_squad.physical_therapist != 0){
                    addUsersToUserCard(current_squad.physical_therapist, user_role, current_squad);
                }
                break;
            case 'other':
                console.log(current_squad.other);
                if(current_squad.other != 0){
                    addUsersToUserCard(current_squad.other, user_role, current_squad);
                }

        }
        //showUserCardModal.open();
    }

    function addUsersToUserCard(user_array, role, current_squad){
        amountOfStackedCards = user_array.length - 1;
        $('#numberOfUserCards').html(amountOfStackedCards + 1);
        $('#currentUserCardShowing').html(1);
        //alert(role);
        var html = "";
        //if more than 1 user are in the role show the next and back buttonss
        if(user_array.length > 1){
            $('#displayMultipleUsers').css('display', 'block');
        }else{
            $('#displayMultipleUsers').css('display', 'none');
        }
        //each user will get their own flipable card
        for(var i = 0; i < user_array.length; i++){
            var user_index = getUserIndexById(user_array[i]);
            var user = roster_array[user_index];
            
            var display = '';
            //split the last name and first name
            var name = user.name;
            name = name.split(' ');

            var description = "";
            if(user.description === null || user.description === ""){
                description = "not entered";
            }else{
                description = user.description;
            }

            if(i != 0){
                display = 'none';
            }else{
                display = 'block';
            }

            html += '<div class="flippableCard" data-cardnumber="'+i+'" style="display: '+display+'">';
            //if there is more than 1 user add stacked class to the usercard
            if(user_array.length > 1){
                html += '<div class="userCard stacked">';
            }else{
                html += '<div class="userCard">';
            }

            html += '<div class="cardFront">' +
                        '<div class="header '+user.zengarden_theme+'"><div class="headerBG themeBG"></div></div>' +
                        '<div class="cardZenRating menuWidget">' +
                                '<div href="/zenrating" class="widget">' +
                                    '<span class="leftImage"><img src="/images/icons/zenrating.png"></span>'+
                                    '<span class="rate lv2">73</span>'+
                                '</div>'+
                        '</div>'+
                        '<div class="userCardAvatarContainer"><div class="userCardAvatar" style="background-color:'+user.background_color+';background-image:url(\''+user.picture_location+'\');"></div></div>' +
                        '<div class="firstname">'+name[0]+'</div>' +
                        '<div class="lastname">'+name[1]+'</div>';
                       
                        //html += '<a class="flipCardL"></a> <a class="flipCardR"></a> <a class="flipCardX"></a>';
                        
                        html += '<div class="cardFrontContent">' +
                        '<hr>' +
                        /*
                        TODO: ADD
                        */
                        '<fieldset class="centered"><legend>Role</legend>'+
                        '<div class="roleDisplay">';
                            /*<!-- roles can be hidden/exposed with the role ID 1-10 -->*/
                            if(role === 'chief_executive'){
                                html += '<div id="role1" class="setToolTip inline"><div class="icon icon-exec FG__required"></div><span class="toolTip">chief<br>executive</span></div>';
                            }else if(role === 'chief_navigator'){
                                html += '<div id="role2" class="setToolTip inline"><div class="icon icon-reuser FG__required"></div><span class="toolTip">human<br>resources</span></div>';
                            }else if(role === 'physical_therapist'){
                                html += '<div id="role3" class="setToolTip inline"><div class="icon icon-rehab FG__suggested" style="display:block;"></div><span class="toolTip">physical<br>therapist</span></div>';
                            }else if(role === 'claim_manager'){
                                html += '<div id="role4" class="setToolTip inline"><div class="icon icon-injuryreport FG__suggested"></div><span class="toolTip">claim<br>manager</span></div>';
                            }else if(role === 'safety_manager'){
                                html += '<div id="role5" class="setToolTip inline"><div class="icon icon-exclamation-triangle FG__suggested"></div><span class="toolTip">safety<br>manager</span></div>';
                            }else if(role === 'work_comp_agent'){
                                html += '<div id="role6" class="setToolTip inline"><div class="icon icon-insurance FG__suggested" style="display:inline-block;"></div><span class="toolTip">work comp<br>agent</span></div>';
                            }else if(role === 'supervisor'){
                                html += '<div id="role7" class="setToolTip inline"><div class="icon icon-hardhat FG__suggested"></div><span class="toolTip">employee<br>supervisor</span></div>';
                            }else if(role === 'customer_service_agent'){
                                html += '<div id="role8" class="setToolTip inline"><div class="icon icon-support FG__suggested"></div><span class="toolTip">service<br>agent</span></div>';
                            }else if(role === 'doctor'){
                                html += '<div id="role9" class="setToolTip inline"><div class="icon icon-doctor FG__optional"></div><span class="toolTip">medical<br>support</span></div>';
                            }else if(role === 'other'){
                                html += '<div id="role10" class="setToolTip inline"><div class="icon icon-adduser FG__optional"></div><span class="toolTip">other<br>user</span></div>';
                            }
                html+=  '</div>' +    
                        '</fieldset>'+
                        '<fieldset class="centered" style="display:none"><legend>Description</legend>'+
                            '<div class="descriptionDisplay">'+description+'</div>'+
                        '</fieldset>'+
                        /*
                        <fieldset class="centered"><legend>Teams</legend>
                        <!-- active teams is the current teams user is on.  All teams is the number of injuries the user was on a team with-->
                        <div class="teamDisplay">active: <span class="onActiveTeams">5</span>  &nbsp;&nbsp;&nbsp;  total: <span class="onTotalTeams">13</span></div>
                        </fieldset>
                        */
                        '<div class="cardElement email"><a href="mailto:'+user.email+'">'+user.email+'</a></div>';
                        if(user.phone){
                            html+= '<div class="cardElement phone">'+user.phone+'</div>';
                        }else{
                            html+= '<div class="cardElement phone">no phone number added</div>';
                        }
                html+='</div>'+
                    '<div class="mainButton">'+
                        '<button class="removeUserFromTeam red centered showZenModal" data-modalcontent="#popmessageModal2" data-remove="team" data-userid="'+user.id+'" data-name="'+name[0] + " " + name[1]+'" data-role="'+role+'">remove from team</button>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>';
            

        }
        console.log(html);
        $('#cardContainer').html(html);
        
        setUserToRole = role;
        //$('#addNewUserToRole').attr('data-role', role);
        if(current_squad === squad){
            //$('#addNewUserToRole').attr('data-squadtype', 'squad');
            setUserToSquad = "active";
        }else if(current_squad === new_squad){
            //$('#addNewUserToRole').attr('data-squadtype', 'new_squad');
            setUserToSquad = "template";
        }
    }

    $('#renameTeam').on('click', function(){
        if($('#manageTeamsTab').css('display') == 'none'){
            var title = $('#currentTeamTitle').text();
            $('#currentName').text(title);
        }else{
            var title = $('#currentTeamTemplateTitle').text();
            $('#currentName').text(title);
        }
       // renameTeamModal.open();
    });

    $('#submitNewTeamName').on('click', function(){
        var newName = $('#updateTeamName').val();
        //alert(newName);
        if($('#manageTeamsTab').css('display') == 'none'){
            squad.name = newName;
            $('#currentTeamTitle').text(newName);
        }else{
            new_squad.name = newName;
            $('#currentTeamTemplateTitle').text(newName);
        }
        modal.close();

        //renameTeamModal.close();
    });

    function validateNewSquad(show_error_messages){
        var error_message = "";
        var valid = true;
        if(new_squad.chief_executive[0] == null || new_squad.chief_executive[0] == ""){
            error_message += "The position of Chief Executive is required. You must fill this position before you can save the team.<br>";
            valid = false;
        }
        if(new_squad.chief_navigator[0] == null || new_squad.chief_navigator[0] == ""){
            error_message += "The position of Chief Navigator is required. You must fill this position before you can save the team.<br>";
            valid = false;
        }
        //document.getElementById('teamErrors').innerHTML = error_message;
        if(show_error_messages){
            ////console.log(error_message);
        }
        return valid;
    }

    function saveNewSquad(){
        var _token = '<?php echo csrf_token(); ?>';
        var valid = validateSquad(true);
        var squad_name = $('#currentTeamTemplateTitle').text();
        if(valid){
            $.ajax({
                type: 'POST',
                url: '<?php echo route("postNewSquad") ?>',
                data: {
                    chiefExecutiveArray: new_squad.chief_executive,
                    doctorArray: new_squad.doctor,
                    therapistArray: new_squad.physical_therapist,
                    claimManagerArray: new_squad.claim_manager,
                    agentArray: new_squad.work_comp_agent,
                    customerServiceArray: new_squad.customer_service_agent,
                    safetyCoordinatorArray: new_squad.safety_manager,
                    supervisorArray: new_squad.supervisor,
                    chiefNavigatorArray: new_squad.chief_navigator,
                    otherArray: new_squad.other,
                    squadName: new_squad.name,
                    avatar_location: new_squad.avatar_location,
                    background_color: new_squad.background_color,
                    avatar_color: new_squad.avatar_color,
                    avatar_number: new_squad.avatar_number,
                    icon_color: new_squad.icon_color,
                    _token: _token
                },
                success: function(data){
                    getSquadList("premade");
                    loadSquad(data, 'active');
                    showAlert("Your team has been saved!", "confirm", 5);
                    $("ul[data-target='activeTeamsTab']").trigger( "click" ); 
                    modal.close();
                }
            });
        }
    }

    $('#addNewUserToRole').on('click', function(){
        modal.close();
        var title = "";
        var roleType = "";
        if(setUserToRole === 'chief_executive'){
            title = "Chief Executive";
            roleType = "roleRequired";
        }else if(setUserToRole === 'chief_navigator'){
            title = "Human Resources";
            roleType = "roleRequired";
        }else if(setUserToRole === 'physical_therapist'){
            title = "Physical Therapist";
            roleType = "roleOptional";
        }else if(setUserToRole === 'claim_manager'){
            title = "Claim Manager";
            roleType = "roleSuggested";
        }else if(setUserToRole === 'safety_manager'){
            title = "Safety Manager";
            roleType = "roleSuggested";
        }else if(setUserToRole === 'work_comp_agent'){
            title = "Work Comp Agent";
            roleType = "roleSuggested";
        }else if(setUserToRole === 'supervisor'){
            title = "Supervisor";
            roleType = "roleSuggested";
        }else if(setUserToRole === 'customer_service_agent'){
            title = "Customer Service Agent";
            roleType = "roleSuggested";
        }else if(setUserToRole === 'doctor'){
            title = "Medical Support";
            roleType = "roleOptional";
        }else if(setUserToRole === 'other'){
            title = "Other";
            roleType = "roleOptional";
        }
        //remove all role types
        $('#modalTitle').parent('span').removeClass('roleRequired');
        $('#modalTitle').parent('span').removeClass('roleSuggested');
        $('#modalTitle').parent('span').removeClass('roleOptional');
        
        //add the role type color for the title and set the title
        $('#modalTitle').parent('span').addClass(roleType);
        $('#modalTitle').text(title);
        $('#hiddenUserRoleSettings').attr('data-setmultiusers', true);
    });

    $('#nextUserCard').on('click', function(){
        var cardIndex = $('.flippableCard[style*="block"]').data('cardnumber');
        var currentCard = $('.flippableCard[data-cardnumber="'+cardIndex+'"]');
        var nextCard = $('.flippableCard[data-cardnumber="'+(cardIndex + 1)+'"]');
        console.log(nextCard);
        if(nextCard != undefined || nextCard != "" || nextCard != null){
            if(cardIndex + 1 <= amountOfStackedCards){
                currentCard.hide();
                nextCard.css('display', 'block');
                $('#currentUserCardShowing').html(parseInt($('#currentUserCardShowing').text()) + 1);
            }
        }
    });

    $('#prevUserCard').on('click', function(){
        console.log('prev clicked');
        var cardIndex = $('.flippableCard[style*="block"]').data('cardnumber');
        var currentCard = $('.flippableCard[data-cardnumber="'+cardIndex+'"]');
        var prevCard = $('.flippableCard[data-cardnumber="'+(cardIndex - 1)+'"]');
        //console.log(nextCard);
        if(prevCard != undefined || prevCard != "" || prevCard != null){
            console.log('prev card is set');
            if(cardIndex - 1 >= 0){
                console.log('cardIndex is grater than 0');
                currentCard.hide();
                prevCard.css('display', 'block');
                $('#currentUserCardShowing').html(parseInt($('#currentUserCardShowing').text()) - 1);
            }
        }
    });

    $('.cancelDeletion').on('click', function(){
        modal.close();
    });

    $('body').on('click', '.removeUserFromTeam', function(){
        var id = $(this).data('userid');
        var name = $(this).data('name');
        var remove = $(this).data('remove');
        
        //$('#user_card').hide();

        if(remove === "user" || remove === "agent"){
            $('#popModalMessage2').text('Are you sure you want to permanently delete ' + name + '?');
        }else if(remove === "team"){
            var role = $(this).data('role');
            $('#deleteUser').attr('data-role', role);
            $('#popModalMessage2').text('Are you sure you want to remove ' + name + ' from this team?');
        }
        $('#deleteUser').attr('data-userid', id);
        $('#deleteUser').attr('data-remove', remove);
        console.log('id: ' + id + " name: " + name + " remove: " + remove);
    });

    $('body').on('click', '#deleteUser', function(){
        var id = $(this).data('userid');
        var remove = $(this).data('remove');
        var role = $(this).data('role');
        if(remove === "user"){
           deleteUser(id);
        }else if(remove === "team"){
            removeUserFromRole(role, id);
        }else if(remove === "agent"){
            console.log('user_id: ' + id);
            deleteZagent(id);
        }
    });

    function deleteUser(id){
        var _token = '<?php echo csrf_token(); ?>';
        var userID = id;
        $.ajax({
            type: 'POST',
            url: '<?php echo route('removeUser') ?>',
            data: {
                user_id: userID,
                company_id: '<?php echo Auth::user()->company_id; ?>',
                _token: _token
            },
            success: function (data) {
                //$('#deleteUserModal').modal('hide');
                showAlert("User removed! You can add them again whenever you want.", "confirm", 5);
                location.reload();
            },
            error: function (data){
                ////console.log(data);
                var errorMsg = JSON.parse(data.responseText);
                console.log(errorMsg);
                showAlert("Error Occured Plese try again later", "deny", 5);
            }
        });
    }

    function deleteZagent(id){
        var _token = '<?php echo csrf_token(); ?>';
        var user_id = id;
        $.ajax({
            type: 'POST',
            url: '<?php echo route('deleteZagentZuser') ?>',
            data: {
                user_id: user_id,
                company_id: {{$company_id}},
                _token: _token
            },
            success: function (data) {
                showAlert("User removed! You can add them again whenever you want.", "confirm", 5);
                location.reload();
            },
            error: function (data){
                var response_array = JSON.parse(data.responseText);
                console.log(response_array);
                showAlert("Error Occured Plese try again later", "deny", 5);
                modal.close();
            }
        });
    }

    function removeUserFromRole(role, user_id){
        ////console.log(squad[role]);
        ////console.log(squad);
        for(var i = 0; i < squad[role].length; i++){
            if(squad[role][i] === user_id){
                squad[role].splice(i, 1);
                break;
            }
        }
        squad.edited = true;
        console.log(JSON.stringify(squad));
        overwriteCurrentSquad();
    }

</script>

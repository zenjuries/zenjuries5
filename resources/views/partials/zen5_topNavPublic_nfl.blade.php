<div id="navbar">
  <div id="logo"><a href="/" style="background-image:url('/images/nflpa/zen-nflpa-logo.png');" class="noLink"></a></div>
  <div id="navbar-menu">
	<div id="menuLinks">
		<ul>
		<li id="dashboard"><a class="main zenboard" href="/dashboard_nfl"><span class="menuText">dashboard</span></a></li>
		<li id="injuries"><a class="main zencare" href="/injurylist_nfl"><span class="menuText">my injuries</span></span></a></li>
		<li id="team"><a class="main zenisphere" href="/injuryteams_nfl"><span class="menuText">my injury team</span></span></a></li> 
    <li id="zencare"><a class="main" href="/help_nfl"><span class="menuText">help / benefits</span></span></a></li>   
		</ul>	
	</div>	
  </div>
 
</div>

<div id="navbarMobile">
	<div class="hamburger hamburger--vortex">
		<div class="hamburger-btn">
			<div class="hamburger-inner"></div>
		</div>
	</div>
	<div id="mobileLogo"><img class="animate__animated animate__bounceInRight" src="/images/nflpa/zen-nflpa-logo.png" height="100%"></div>
	<div id="slideMenu">
 		<ul>
		<li id="mzenjuries"><a class="main" href="https://sales.zenjuries.com"><div class="icon icon-home txt__white"></div> <span>zenboard</span></a></li>
		</ul>
	</div>	
</div>

<script>
// When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("navbar").style.height = "70px";
    document.getElementById("menuLinks").style.padding = "20px 5px";
    document.getElementById("logo").style.padding ="8px 0 8px 10px";
    document.getElementById("logo").style.width ="200px";
  } else {
    document.getElementById("navbar").style.height = "135px";
    document.getElementById("menuLinks").style.padding = "50px 20px";
    document.getElementById("logo").style.padding ="18px 0 18px 20px";
    document.getElementById("logo").style.width ="300px";
  }
}
</script>

  <script>
    var forEach=function(t,o,r){if("[object Object]"===Object.prototype.toString.call(t))for(var c in t)Object.prototype.hasOwnProperty.call(t,c)&&o.call(r,t[c],c,t);else for(var e=0,l=t.length;l>e;e++)o.call(r,t[e],e,t)};

    var hamburgers = document.querySelectorAll(".hamburger");
    if (hamburgers.length > 0) {
      forEach(hamburgers, function(hamburger) {
        hamburger.addEventListener("click", function() {
          this.classList.toggle("is-active");
        }, false);
      });
    }
	$('.hamburger').click(function(){
	$('#slideMenu').toggleClass('slide-out');	
	});

    var avatarMenus = document.querySelectorAll(".avatarMenu");
    if (avatarMenus.length > 0) {
      forEach(avatarMenus, function(avatarMenu) {
        avatarMenu.addEventListener("click", function() {
          this.classList.toggle("is-active");
        }, false);
      });
    }
	$('.avatarMenu').click(function(){
	$('#avatarSlideMenuMobile').toggleClass('slide-out')
	$('#avatarSlideMenu').toggleClass('slide-out')
	$('#mobileLogo').toggleClass('slide-over')
	$('.avatarMenu').toggleClass('slide-over');	
	});	
  </script>

<script>
var parent = document.getElementById('container1');
var child = document.getElementById('container2');
child.style.paddingRight = child.offsetWidth - child.clientWidth + "px";
</script>
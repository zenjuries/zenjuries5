<?php
$user = Auth::user();
$theme = $user->zengarden_theme;
$theme = str_replace("theme", "", $theme);
$theme = strtolower($theme);
$agency_type = NULL;
$agent = false;
$user_is_zagent = false;

//check if user is zagent
if(Auth::user()->zagent_id !== NULL){
	$user_is_zagent = true;
}
//check for zenpro
if(Auth::user()->type !== "internal"){
        $agent = true;
}
if($user_is_zagent){
	$agent = false;
}
if($agent){
	$user_is_zagent = false;
}

if($user->gets_priority_3_emails === 1 && $user->gets_priority_2_emails === 1){
    $alert_settings = "full";

}elseif($user->gets_priority_3_emails === 0 && $user->gets_priority_2_emails === 1){
    $alert_settings = "partial";

}elseif($user->gets_priority_3_emails === 1 && $user->gets_priority_2_emails === 0){
    $alert_settings = "partial";

}else{
	$alert_settings = "none";
}

?>
<!-- initialize sound -->
<script>
  const AudioContext = window.AudioContext || window.webkitAudioContext;
  const audioCtx = new AudioContext();
  function PlayClick() {
        var sound = document.getElementById("audioClick");
        sound.play()
    }
  function PlaySlide() {
        var sound = document.getElementById("audioSlide");
        sound.play()
    } 
  function PlayChime() {
        var sound = document.getElementById("audioChime");
        sound.play()
    }    
  function PlaySlideClick() {
        var sound = document.getElementById("audioSlideClick");
        sound.play()
    } 
  function PlayHappyClick() {
        var sound = document.getElementById("happyClick");
        sound.play()
    }    
  function PlayChirpClick() {
        var sound = document.getElementById("chirpClick");
        sound.play()
    }  
</script>

<audio id="audioClick" src="/audio/click.mp3" autostart="false" preload="auto"></audio>
<audio id="audioChime" src="/audio/chime.mp3" autostart="false" preload="auto"></audio>
<audio id="audioSlide" src="/audio/qslide.mp3" autostart="false" preload="auto"></audio>
<audio id="audioSlideClick" src="/audio/slideclick.mp3" autostart="false" preload="auto"></audio>
<audio id="happyClick" src="/audio/happyclick2.mp3" autostart="false" preload="auto"></audio>
<audio id="chirpClick" src="/audio/chirpclick.mp3" autostart="false" preload="auto"></audio>

<div id="navbar">
	<div id="logo"><a href="/zenboard" class="noLink"></a></div>
	<div id="policyholderSelector">
        @if(!$user_is_zagent && !$agent)
        <div id="policyholderCompany" class="policyholderButton noButton">
        @endif
        @if($agent)
        <div id="policyholderCompany" class="policyholderButton" onclick="location.href='/policyholders';">
        @endif
        @if($user_is_zagent)
        <div id="policyholderCompany" class="policyholderButton" onclick="location.href='/zagent';">
        @endif
			<div id="policyholderLogo" style="background-image:url('/images/utility/companylogo.png');"></div>
			<div id="policyholderName">{{Auth::user()->getCompany()->company_name}}</div>
        </div>
    </div>

  <div id="navbar-menu">
	<div id="menuLinks">
		<ul>
		<li id="zenboard"><a class="main zenboard" href="/zenboard"><span class="menuText">dashboard</span></a></li>
		<li id="zenisphere"><a class="main zenisphere" href="/zenisphere"><span class="menuText">users/teams</span></span></a></li>
		<li id="zencare"><a class="main zencare" href="/zencarelist"><span class="menuText">injury list</span></span></a></li>
		@if($user_is_zagent || $agent)
		<li id="profile"><a class="main company" href="/policyholder_profile"><span class="menuText">profile<span></a></li>
		@endif
		@if($agent)	
		<li id="policyholders"><a class="main company" href="/policyholders"><span class="menuText">policyholders<span></a></li>
		@endif
		@if($user_is_zagent)	
		<li id="policyholders"><a class="main company" href="/zagent"><span class="menuText">policyholders<span></a></li>			
		@endif	
		</ul>	
	</div>	
  </div>



  <div class="avatarMenu" id="avatarMenuBtn">
		<div class="avatarMenu-btn" style="background-color: {{Auth::user()->avatar_color}};background-image:url({{Auth::user()->photo_url}});"></div>
	</div>	
	<div id="avatarSlideMenu">
		<div class="widgetDock">
			<a class="menuWidget">
				<div href="/zenrating" class="widget">	
					<span class="leftImage"><img src="/images/icons/zenrating.png"></span>
					<span class="rate lv2">73</span>
				</div>
			</a>
			<a class="menuWidget openUserSettingsModal" data-modalcontent="#alertsModal">
				<div class="widget">
					<span class="label">alerts</span>
					<span class="data" id="alertSetting">{{ $alert_settings }}</span>
					<span class="rightImage"><img src="/images/icons/alert.png"></span>
				</div>
			</a>
			<a href="/myinfo" class="menuWidget">
				<div class="widget">
					<span class="label">my info</span>
					<span class="data">settings</span>
					<span class="rightImage"><img src="/images/icons/gear.png"></span>
				</div>
			</a>
			<a class="menuWidget last openUserSettingsModal" data-modalcontent="#userThemeModal">
				<div class="widget">
					<span class="label onClickEvent">theme</span>
					<span class="data onClickEvent">{{ $theme }}</span>
					<span class="rightImage onClickEvent"><img src="/images/icons/theme.png"></span>
				</div>
			</a>
		</div>								
	</div> 
</div>

<div id="navbarMobile">
	<div class="hamburger hamburger--vortex no_highlights" onclick="PlaySlide()">
		<div class="hamburger-btn">
			<div class="hamburger-inner"></div>
		</div>
	</div>
	<!--<div id="policyholderLogoButton" style="background-image:url('/images/utility/companylogo.png');"></div>-->
	<div id="mobileLogo" onclick="PlayChirpClick();"><a class="invisible no_highlights" href="/zenboard"><img class="animate__animated animate__bounceInRight" height="100%"></a></div>
	<div id="slideMenu">
		<ul>
          <li onclick="PlayChirpClick();location.href='/zenboard';" class="seeTouch" id="mzenboard"><a class="main no_highlights" href="/zenboard" ><img class="mobileNavIcon" src="/images/icons/zenjuries.png"><span class="zenboard">dashboard</span></a></li>
          <li onclick="PlayChirpClick();location.href='/zenisphere';" class="seeTouch" id="mzenisphere"><a class="main no_highlights" href="/zenisphere"><img class="mobileNavIcon" src="/images/icons/zenisphere.png"><span class="zenisphere">users/teams</span></a></li>
          <li onclick="PlayChirpClick();location.href='/zencarelist';" class="seeTouch" id="mzencare"><a class="main no_highlights" href="/zencarelist"><img class="mobileNavIcon" src="/images/icons/stethescope.png"><span class="zencare">injury list</span></a></li>
          <li onclick="PlayChirpClick();location.href='/newinjury';" class="seeTouch" id="mnewinjury"><a class="main no_highlights" href="/newinjury"><img class="mobileNavIcon" src="/images/icons/plus.png"><span class="newinjury">new injury</span></a></li>
		  <!-- next two should only be viewable by company admin/agents/pros-->
		  <li onclick="PlayChirpClick();location.href='/policyholder_profile';" class="seeTouch" id="mprofile"><a class="main no_highlights" href="/policyholder_profile"><img class="mobileNavIcon" src="/images/icons/userinfo.png"><span class="profile">profile</span></a></li>
          <li onclick="PlayChirpClick();location.href='/policyholders';" class="seeTouch" id="mpolicyholders"><a class="main no_highlights" href="/policyholders"><img class="mobileNavIcon" src="/images/icons/users.png"><span class="policyholders">policyholders</span></a></li>
        </ul>
	</div>

	<div class="avatarMenu mobile">
		<div class="avatarMenu-btn" style="background-image:url({{Auth::user()->photo_url}});" onclick="PlaySlide()"></div>
	</div>	
	<div id="avatarSlideMenuMobile">
		<div class="widgetDock">
			<a class="menuWidget" onclick="PlayClick()">
				<div href="/zenrating" class="widget">	
					<span class="leftImage"><img src="/images/icons/zenrating.png"></span>
					<span class="rate lv2">73</span>
				</div>
			</a>
			<a class="menuWidget" onclick="PlayClick()">
				<div class="widget openUserSettingsModal" data-modalcontent="#alertsModal">
					<span class="label">alerts</span>
					<span class="data" id="alertSetting">{{ $alert_settings }}</span>
					<span class="rightImage"><img src="/images/icons/alert.png"></span>
				</div>
			</a>
			<a href="/myinfo" class="menuWidget" onclick="PlayClick()">
				<div class="widget">
					<span class="label">my info</span>
					<span class="data">settings</span>
					<span class="rightImage"><img src="/images/icons/gear.png"></span>
				</div>
			</a>
			<a class="menuWidget last openUserSettingsModal" data-modalcontent="#userThemeModal" onclick="PlayClick()">
				<div class="widget">
					<span class="label">theme</span>
					<span class="data">{{ $theme }}</span>
					<span class="rightImage"><img src="/images/icons/theme.png"></span>
				</div>
			</a>
		</div>
	</div>
</div>
<!-- Modal -->
<div id="userSettingsModal" class="zModal" style="display:none">
	<!-- THEME MODAL -->
	<div class="modalContent dark modalBlock" id="userThemeModal" style="display:none">
        <div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/theme.png">Zenjuries theme</span></div>
        <div class="modalBody" style="min-width:360px;">
            <section class="sectionPanel dark">
                <div class="modalDescription">choose a nice theme for zenjuries.</div>
				<div class="sectionContent">
					<section class="formBlock dark">                          
						<div class="formGrid">
							<div class="settingsButtonArray">
								<div class="buttonSet" style="background-image:url('/images/themes/bamboo.jpg');background-position:left bottom;">
									<span class="themeLabelbg themeColor-bbg-Bamboo">bamboo</span>
									<button class="themeColor-bg-Bamboo themeButton" data-theme="themeBamboo">set bamboo</button>
								</div>
								<div class="buttonSet" style="background-image:url('/images/themes/beach.jpg');background-position:center bottom;">
									<span class="themeLabelbg themeColor-bbg-Beach">beach</span>
									<button class="themeColor-bg-Beach themeButton" data-theme="themeBeach">set beach</button>
								</div>		
								<div class="buttonSet" style="background-image:url('/images/themes/redleaf.jpg');background-position:right bottom;">
									<span class="themeLabelbg themeColor-bbg-Redleaf">redleaf</span>
									<button class="themeColor-bg-Redleaf themeButton" data-theme="themeRedleaf">set redleaf</button>
								</div>
								<div class="buttonSet" style="background-image:url('/images/themes/ocean.jpg');background-position:left bottom;">
									<span class="themeLabelbg themeColor-bbg-Ocean">ocean</span>
									<button class="themeColor-bg-Ocean themeButton" data-theme="themeOcean">set ocean</button>
								</div>
								<div class="buttonSet" style="background-image:url('/images/themes/sand.jpg');background-position:left bottom;">
									<span class="themeLabelbg themeColor-bbg-Sand">sand</span>
									<button class="themeColor-bg-Sand themeButton" data-theme="themeSand">set sand</button>
								</div>
								<div class="buttonSet" style="background-image:url('/images/themes/zenstone.jpg');background-position:right bottom;">
									<span class="themeLabelbg themeColor-bbg-Zenstone">zenstone</span>
									<button class="themeColor-bg-Zenstone themeButton" data-theme="themeZenstone">set zenstone</button>
								</div>	
								<div class="buttonSet" style="background-image:url('/images/themes/lotus.jpg');background-position:right bottom;background-position-y:230px;">
									<span class="themeLabelbg themeColor-bbg-Lotus">lotus</span>
									<button class="themeColor-bg-Lotus themeButton" data-theme="themeLotus">set lotus</button>
								</div>
								<div class="buttonSet" style="background-image:url('/images/themes/island.jpg');background-position:left bottom;background-position-y:270px;">
									<span class="themeLabelbg themeColor-bbg-Serenity">serenity</span>
									<button class="themeColor-bg-Serenity themeButton" data-theme="themeSerenity">set serenity</button>
								</div>		
								<div class="buttonSet" style="background-image:url('/images/themes/autumn.jpg');background-position:center bottom;">
									<span class="themeLabelbg themeColor-bbg-Autumn">autumn</span>
									<button class="themeColor-bg-Autumn themeButton" data-theme="themeAutumn">set autumn</button>
								</div>	
								<div class="buttonSet" style="background-image:url('/images/themes/sky.jpg');background-position:center bottom;">
									<span class="themeLabelbg themeColor-bbg-Sky">sky</span>
									<button class="themeColor-bg-Sky themeButton" data-theme="themeSky">set sky</button>
								</div>
								<div class="buttonSet" style="background-image:url('/images/themes/purplehaze.jpg');background-position:center bottom;background-position-y:170px;">
									<span class="themeLabelbg themeColor-bbg-Purplehaze">purplehaze</span>
									<button class="themeColor-bg-Purplehaze themeButton" data-theme="themePurplehaze">set purplehaze</button>
								</div>												
							</div>
					</section>
				</div>                    
            </section>           
        </div>
    </div> 

	<!-- ALERTS MODALS -->

		<div id="alertsModal" class="modalContent dark modalBlock" style="display: none">
			<div class="modalHeader"><span class="modalTitle"><img class="modalIcon" src="/images/icons/alert.png">set alerts</span></div>
			<div class="modalBody" style="min-width:260px;">
				<section class="sectionPanel dark">
					<div class="modalDescription">Zenjuries can send you email alerts to keep you updated on your claims.<br> Please select what level of alerts you'd like to receive!</div>
						<div class="sectionContent">
							<section class="formBlock dark">
								<div class="settingsButtonArray">
									<div class="buttonSet">
										<button class="alertsButton {{$alert_settings === "full" ? "green" : ""}}" data-alerts="full"><div class="icon icon-bell"></div>  maximum alerts</button>
										<button class="alertsButton {{$alert_settings === "partial" ? "cyan" : ""}}" data-alerts="partial"><div class="icon icon-bell-o"></div>  critical alerts only</button>
										<button class="alertsButton {{$alert_settings === "none" ? "red" : ""}}" data-alerts="none"><div class="icon icon-bell-slash-o"></div>  no alerts</button>
									</div>
								</div>
							</section>
						</div>
					</div>		
				</section>
			</div>
		</div>

	</div>

</div>

<script>
	//modal globalVar to contain the reference to the modal 
	var modal;

	$('.openUserSettingsModal').on('click', function(){
			//initialize the modal
			modal = new jBox('Modal', {
				addClass: 'zBox',
				//modal ID goes here
				content: $('#userSettingsModal'),
				isolateScroll: true
			});
			//get the content for the modal
			var target = $(this).data('modalcontent');
			//hide all content blocks
			$('.modalBlock').hide();
			//show the target
			$(target).show();

			modal.open();
	});

	$('.themeButton').on('click', function(){
        var new_theme = $(this).data('theme');
        changeTheme(new_theme);
    });
    
    function changeTheme(theme){
        $.ajax({
            type: 'POST',
            url: '/zengarden/updateZengardenTheme',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                user_id: {{ Auth::user()->id }},
                theme: theme
            },
            success: function(){
                location.reload();
                //showAlert("Your settings have been updated!", "confirm", 5);
                //redirect back to settings page
            },
            error: function(){
                showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
            }
        })
    }

	$('.alertsButton').on('click', function(){
		var setting = $(this).data('alerts');
		updateAlertSettings(setting);
		$('.alertsButton').removeClass('green cyan red');
		if(setting === "partial"){
			$(this).addClass('cyan');
		}else if(setting === "full"){
			$(this).addClass('green');
		}else if(setting === "none"){
			$(this).addClass('red');
		}
	});
	
	function updateAlertSettings(alert_setting){
		$.ajax({
			type: 'POST',
			url: '/zengarden/updateAlertSettings',
			data: {
				_token: '<?php echo csrf_token(); ?>',
				user_id: {{ $user->id }},
				setting: alert_setting
			},
			success: function(){
				modal.close();
				$('#alertSetting').text(alert_setting);
				showAlert("Your settings have been updated!", "confirm", 5);
			},
			error: function(){
				modal.close();
				showAlert("Sorry, something went wrong. Please try again later", "deny", 5);
			}
		});
	}

// When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("navbar").style.height = "70px";
    document.getElementById("menuLinks").style.padding = "20px 5px";
    document.getElementById("menuLinks").style.fontSize = ".8rem";	
    document.getElementById("logo").style.padding ="8px 0 8px 10px";
    document.getElementById("logo").style.width ="200px";
	document.getElementById("avatarMenuBtn").style.top ="0px";
	document.getElementById("avatarMenuBtn").style.width ="48px";
	document.getElementById("avatarMenuBtn").style.height ="48px";		
	document.getElementById("avatarSlideMenu").style.top ="70px";
	document.getElementById("policyholderCompany").style.fontSize =".6rem";
	document.getElementById("policyholderCompany").style.left ="110px";
	document.getElementById("policyholderCompany").style.padding ="2px 8px 2px 16px";	
	document.getElementById("policyholderLogo").style.width ="12px";
	document.getElementById("policyholderLogo").style.height ="12px";	
	document.getElementById("policyholderName").style.top ="0px";			
  } else {
    document.getElementById("navbar").style.height = "135px";
    document.getElementById("menuLinks").style.padding = "50px 20px";
    document.getElementById("menuLinks").style.fontSize = "1rem";	
    document.getElementById("logo").style.padding ="18px 0 18px 20px";
    document.getElementById("logo").style.width ="300px";
	document.getElementById("avatarMenuBtn").style.top ="28px";
	document.getElementById("avatarMenuBtn").style.width ="54px";
	document.getElementById("avatarMenuBtn").style.height ="54px";		
	document.getElementById("avatarSlideMenu").style.top ="135px";
	document.getElementById("policyholderCompany").style.fontSize =".9rem";
	document.getElementById("policyholderCompany").style.left ="86px";
	document.getElementById("policyholderCompany").style.padding ="3px 10px 3px 18px";	
	document.getElementById("policyholderLogo").style.width ="22px";
	document.getElementById("policyholderLogo").style.height ="22px";
	document.getElementById("policyholderName").style.top ="-4px";			
  }
}
</script>

  <script>
    var forEach=function(t,o,r){if("[object Object]"===Object.prototype.toString.call(t))for(var c in t)Object.prototype.hasOwnProperty.call(t,c)&&o.call(r,t[c],c,t);else for(var e=0,l=t.length;l>e;e++)o.call(r,t[e],e,t)};

    var hamburgers = document.querySelectorAll(".hamburger");
    if (hamburgers.length > 0) {
      forEach(hamburgers, function(hamburger) {
        hamburger.addEventListener("click", function() {
          this.classList.toggle("is-active");
        }, false);
      });
    }
	$('.hamburger').click(function(){
	$('#slideMenu').toggleClass('slide-out');	
	});

    var avatarMenus = document.querySelectorAll(".avatarMenu");
    if (avatarMenus.length > 0) {
      forEach(avatarMenus, function(avatarMenu) {
        avatarMenu.addEventListener("click", function() {
          this.classList.toggle("is-active");
        }, false);
      });
    }
	$('.avatarMenu').click(function(){
	$('#avatarSlideMenuMobile').toggleClass('slide-out')
	$('#avatarSlideMenu').toggleClass('slide-out')
	$('#mobileLogo').toggleClass('slide-over')
	$('.avatarMenu').toggleClass('slide-over');	
	});	
  </script>

<script>
var parent = document.getElementById('container1');
var child = document.getElementById('container2');
child.style.paddingRight = child.offsetWidth - child.clientWidth + "px";
</script>

<script>
  $(".seeTouch").click(function(){
    // $(".seeTouch").removeClass("touched");
    $(this).addClass("touched");
  })
  </script>
<script>
    var watchlist_injuries = null;
	var watchlist_page_count = 0;
	var watchlist_current_page = 1;
	var watchlist_number_per_page = 10;
    getMyWatchList();

$('.zenListTable').on('click', '.injuryRow', function(){
    var id = $(this).data('id');
    window.location.href = "/zencare/" + id;
});

function setWatchListButtonClasses(){
    if(watchlist_current_page === 1){
        //.log('first');
        //first page
        $('#prevMyWatchList').attr('class', 'pagePrev ghost');
        $('#nextMyWatchList').attr('class', 'pageNext');
    }else if(watchlist_current_page === watchlist_page_count){
        //.log('last');
        //last page
        $('#prevMyWatchList').attr('class', 'pagePrev');
        $('#nextMyWatchList').attr('class', 'pageNext ghost');
    }else{
        //.log('middle');
        //middle pages
        $('#prevMyWatchList').attr('class', 'pagePrev');
        $('#nextMyWatchList').attr('class', 'pageNext');
    }
}

$('#prevMyWatchList').on('click', function(){
 if(!$(this).hasClass('ghost')){
     var cur_page = watchlist_current_page;
     cur_page = cur_page - 1;
     if(cur_page > 0){
        watchlist_current_page = cur_page;
         buildMyWatchListable(false);
     }
     setWatchListButtonClasses();
 }
});

$('#nextMyWatchList').on('click', function(){
    if(!$(this).hasClass('ghost')){
        var cur_page = watchlist_current_page;
        cur_page = cur_page + 1;
        if(cur_page <= watchlist_page_count){
            watchlist_current_page = cur_page;
            buildMyWatchListable(false);
        }
        setWatchListButtonClasses();
    }
});

$('.watchlistTable').on('click', '.watchlistSorter', function(){
	var type = $(this).parent().attr('class');
	if($(this).hasClass('down')){
		var sort = "up";
        $('.watchlistSorter').removeClass('down').removeClass('up');
		$(this).removeClass('down').addClass('up');
	}else {
		sort = "down";
        $('.watchlistSorter').removeClass('down').removeClass('up');
		$(this).removeClass('up').addClass('down');
	}
	if(type === "name"){
        if(sort === "up"){
            sortWatchlistInjuriesByNameAsc(watchlist_injuries);
        }else if(sort === "down"){
            sortWatchlistInjuriesByNameDesc(watchlist_injuries);
        }
	}else if(type === "date"){
        if(sort === "up"){
            sortWatchlistByDateAsc(watchlist_injuries);
        }else if(sort === "down"){
            sortWatchlistByDateDesc(watchlist_injuries);
        }
	}else if(type === "total"){
        if(sort === "up"){
            sortWatchlistByCurrentCostAsc(watchlist_injuries);
        }else if(sort === "down"){
            sortWatchlistByCurrentCostDesc(watchlist_injuries);
        }
	}else if(type === "severity"){
        if(sort === "up"){
            sortWatchlistBySeverityAsc(watchlist_injuries);
        }else if(sort === "down"){
            sortWatchlistBySeverityDesc(watchlist_injuries);
        }
	}else if(type === "claim"){
        if(sort === "up"){
            sortWatchlistByClaimNumberAsc(watchlist_injuries);
        }else if(sort === "down"){
            sortWatchlistByClaimNumberDesc(watchlist_injuries);
        }
	}else if(type === "mood"){
        if(sort === "up"){
            sortWatchlistByMoodAsc(watchlist_injuries);
        }else if(sort === "down"){
            sortWatchlistByMoodDesc(watchlist_injuries);
        }
	}else if(type === "resolved"){
		if(sort === "up"){
			sortWatchlistByStateAsc(watchlist_injuries);
        }else if(sort === "down"){
			sortWatchlistByStateDesc(watchlist_injuries);
        }
    }
    else if(type === "progress"){
        if(sort === "up"){
            sortWatchlistByProgressAsc(watchlist_injuries);
        }else if(sort === "down"){
            sortWatchlistByProgressDesc(watchlist_injuries);
        }
    }
});

	function getMyWatchList(){
		$.ajax({
				type: 'POST',
				url: '<?php echo route('getCriticalInjuryList'); ?>',
				data: {
				    _token: '<?php echo csrf_token(); ?>',
				},
				success: function(data){
				    watchlist_injuries = data;
                    if(watchlist_injuries.length <= watchlist_number_per_page){
                        $('#nextMyWatchList').addClass('ghost');
                    }else{
                        $('#nextMyWatchList').removeClass('ghost');
                    }
				    //.log('injuries: ' + watchlist_injuries);
                    //showList(injuries);
                    if(watchlist_injuries.length > 0){
                        buildMyWatchListable(true);
                    }
                    else{
                        $('#prevMyWatchList').hide();
                        $('#nextMyWatchList').hide();
                        $('.watchlistSorter').hide();
                        $('#myWatchListpageInfoSpan').html("There are no watchlist injuries to display.");
                    }
				},
                error: function(jqxhr, status, exception){
                    //.log(jqxhr);
                }
			});
	}

	function buildMyWatchListable(revert_to_page_one){
        var watchlist_max_page_number = 0;
        var watchlist_critical_count = 0;
        var watchlist_warning_count = 0;
		var i = 0;
            var l = watchlist_injuries.length;
            var html = "";
            var valid = true;
            var colspan = getColspanValue();
            //SEARCH
			//TODO: HOOK UP SEARCH
			var search_string = "";
            //var search_string = $("#claimSearchBar").val();
            var results_array = [];
            if(search_string !== ""){
                for(i = 0; i < l; i++){
                    var show_this_row = false;
                    if(watchlist_injuries[i]['user_name'] !== null && watchlist_injuries[i]['user_name'].toLowerCase().indexOf(search_string.toLowerCase()) !== -1){
                        show_this_row = true;
                    }

                    if(watchlist_injuries[i]['injury_date'] !== null && watchlist_injuries[i]['injury_date'].indexOf(search_string) !== -1){
                        show_this_row = true;
                    }

                    if(watchlist_injuries[i]['claim_number'] !== null && watchlist_injuries[i]['claim_number'].indexOf(search_string) !== -1){
                        show_this_row = true;
                    }
                    if(show_this_row === true){
                        results_array.push(watchlist_injuries[i]);
                    }
                }

            }else{
                results_array = watchlist_injuries;
            }

            //.log(results_array);

            //PAGINATION
            if(revert_to_page_one === true){
                watchlist_current_page = 1;
            }
            var results_count = results_array.length;
            //calculate how many results to show per page
            var page_length = watchlist_number_per_page;
            //if there are less results than the default number_per_page, set it to the results count
            if(page_length > results_count){
                page_length = results_count;
            }

            //i will contain the number of messages needed to skip through to get to the current page
            //for example, if on page 2, we'll want to skip to the 11th message to display it
            //so i will equal 10 if on the 2nd page
            i = 0;
            for (var j = 1; j < watchlist_current_page; j++){
                //add the number shown per page to i each time we advance a page
                i = i + watchlist_number_per_page;
                //add to the page length
                page_length = page_length + watchlist_number_per_page;
                //make sure page_length doesn't exceed the number of messages
                if(page_length > results_count){
                    page_length = results_count;
                }
            }

            watchlist_page_count = Math.ceil(results_count / watchlist_number_per_page);
			$('#myWatchListpageInfoSpan').html("page " + watchlist_current_page + " of " + watchlist_page_count);
            //console.log(page_count + " = " + results_count + " / " + number_per_page );

            var heading_html = $('.watchlistHeadings').prop('outerHTML');
            //.log(heading_html);
            html = heading_html;

            for(var w = 0; w < l; w++){
                if(results_array[w].days >= 30){
                        watchlist_critical_count++;
                }else if(results_array[w].days >= 10 && results_array[i].days < 30){
                        watchlist_warning_count++;
                }else if(results_array[w].days < 10){
                }
            }

            for(i; i < page_length; i++){
                var duration_color = '';
                if(results_array[i].days >= 30){
                    duration_color = 'critical';
                }else if(results_array[i].days >= 10 && results_array[i].days < 30){
                    duration_color = 'alert';
                }else if(results_array[i].days < 10){
                    continue;
                }
                //total costs
                var current_cost = results_array[i].reserve_cost +  results_array[i].current_cost;
                if(current_cost === 0 || current_cost === null){
                    current_cost = "$-enter cost-";
                }else{
                    current_cost = "$" + current_cost;
                }

                //mood
                var mood = results_array[i].mood;
                if(mood === null){
                    mood = "unknown";
                }else{
                    mood = "lv" + mood;
                }

                var mood_label = results_array[i].mood;
                if(mood_label === null){
                    mood_label = "-";
				}
                var mood_color = "";
                var mood_string ='<div class="moodVisual scale60"><div class="currentMood"></div></div>';

                if(mood_label === '10'){
                    mood_label = "10 - Awesome";
                    mood_color = "mood-10";
                }else if(mood_label === '9'){
                    mood_label = "9 - Terrific";
                    mood_color = "mood-9";
                }else if(mood_label === '8'){
                    mood_label = "8 - Great";
                    mood_color = "mood-8";
                }else if(mood_label === '7'){
                    mood_label = "7 - Good";
                    mood_color = "mood-7";
                }else  if(mood_label === '6'){
                    mood_label = "6 - Ok";
                    mood_color = "mood-6";
                }else if(mood_label === '5'){
                    mood_label = "5 - Alright";
                    mood_color = "mood-5";
                }else if(mood_label === '4'){
                    mood_label = "4 - Uncomfortable";
                    mood_color = "mood-4";
                }else if(mood_label === '3'){
                    mood_label = "3 - Not Good";
                    mood_color = "mood-3";
                }else if(mood_label === '2'){
                    mood_label = "2 - Unwell";
                    mood_color = "mood-2";
                }else if(mood_label === '1'){
                    mood_label = "1 - Bad";
                    mood_color = "mood-1";
                }else if(mood_label === '0'){
                    mood_label = "0 - Terrible";
                    mood_color = "mood-0";
                }else if(mood_label === "x"){
                    mood_label = "Dead";
                    mood_color = "mood-x";
                }else{
                    mood_label = "Unknown";
                    mood_color = "unknown";
                    mood_string = "-?-";
                }

                //employee status
                if(results_array[i].employee_status === "Hospital"){
                    var employee_status = "hospital";
                    var employee_status_title = "Hospital";
                }else if(results_array[i].employee_status === "Urgent Care"){
                    var employee_status = "urgentcare";
                    var employee_status_title = "Urgent Care";
                }else if(results_array[i].employee_status === "Family Practice"){
                    var employee_status = "doctor";
                    var employee_status_title = "Family Practice";
                }else if(results_array[i].employee_status === "Home"){
                    var employee_status = "home";
                    var employee_status_title = "Home";
                }else if(results_array[i].employee_status === "Light Duty"){
                    var employee_status = "lightduty";
                    var employee_status_title = "Light Duty";
                }else if(results_array[i].employee_status === "Full Duty"){
                    var employee_status = "fullduty";
                    var employee_status_title = "Full Duty";
                }else if(results_array[i].employee_status === "Maximum Medical Improvement"){
                    var employee_status = "max";
                    var employee_status_title = "Maximum Medical Improvement";
                }else if(results_array[i].employee_status === "Terminated"){
                    var employee_status = "terminated";
                    var employee_status_title = "Terminated";
                }else if(results_array[i].employee_status === "Resigned"){
                    var employee_status = "resigned";
                    var employee_status_title = "Resigned";
                }else{
                    var employee_status = "unknown";
                    var employee_status_title = "Unknown";
                }

                if(results_array[i].severity === "Mild"){
                    results_array[i].severity = "First Aid";
                }
				
                var severity_color = '';
                if(results_array[i].severity === "First Aid"){
                    severity_color = "var(--severityMild)";
                }else if(results_array[i].severity === "Moderate"){
                    severity_color = "var(--severityModerate)";
                }else{
                    severity_color = "var(--severitySevere)";
                }

                var duration_color = '';
                if(results_array[i].days >= 30){
                    duration_color = 'critical';
                }else if(results_array[i].days >= 10 && results_array[i].days < 30){
                    duration_color = 'alert';
                }else if(results_array[i].days < 10){
                }

				html = html + '<tr class="injuryRow" data-id=' + results_array[i].id +'>'
                            + '<td><div class="progressMeter '+duration_color+' small inline status'+results_array[i].status_number+'"><svg viewBox="0 0 100 100"><circle class="pm" r="50%" cx="50%" cy="50%"></circle><div class="overlay"><div class="avatar" style="background-image:url(' + results_array[i].user_pic_location +'); background-color: ' + results_array[i].user_avatar_color + '"></div></div></svg></div></td>'
							+ '<td class="name"><span>' + results_array[i].user_name + '</span></td>'
							+ '<td class="injurydate"><span>' + results_array[i].injury_date + '</span></td>'
                            + '<td class="severity"><span class="'+results_array[i].severity+'"></span></td>'
                            + '<td class="total"><span>' + current_cost + '</span></td>'
                            + '<td class="progress"><span>' + employee_status + '</span></td>'
							+ '<td class="mood myMood '+mood_color+'"><span>'+ mood_string +'</span></td>'
							+ '<tr class="tablerowspace"><td></td></tr>';

            }

			$('#myWatchListDropdown').html(html);
            $('#watchList__alert').html(watchlist_warning_count);
            $('#watchList__critical').html(watchlist_critical_count);

	}

	//use this function to deterimine what the colspan for filler table rows should be, based on how many columns are currently visible
	function getColspanValue(){
        var count = 2;
        $('.visibilityToggle').each(function(){
            if(!$(this).hasClass('hide')){
                count++;
            }
        });
        return count;
    }

function sortWatchlistInjuriesByNameDesc(injury_array){

    function compare(a, b) {
        if(a.user_name.toLowerCase() < b.user_name.toLowerCase()){
            return -1;
        }
        if(a.user_name.toLowerCase() > b.user_name.toLowerCase()){
            return 1;
        }

        return 0;
}

watchlist_injuries.sort(compare);
buildMyWatchListable(true);
}

function sortWatchlistInjuriesByNameAsc(injury_array){
    function compare(a, b) {
        if(a.user_name.toLowerCase() < b.user_name.toLowerCase()){
            return 1;
        }
        if(a.user_name.toLowerCase() > b.user_name.toLowerCase()){
            return -1;
        }

        return 0;
    }

    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistByDateDesc(injury_array){
    function compare(a, b){
        var a = new Date(a.injury_date);
        var b = new Date(b.injury_date);
        if(a < b){
            return -1;
        }
        if(a > b){
            return 1;
        }
        return 0;
    }
    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistByDateAsc(injury_array){
    function compare(a, b){
        var a = new Date(a.injury_date);
        var b = new Date(b.injury_date);
        if(a < b){
            return 1;
        }
        if(a > b){
            return -1;
        }
        return 0;
    }
    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistByCurrentCostDesc(injury_array){
    function compare(a, b){
        if(a.current_cost < b.current_cost){
            return -1;
        }
        if(a.current_cost > b.current_cost){
            return 1;
        }
        return 0;
    }
    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistByCurrentCostAsc(injury_array){
    function compare(a, b){
        if(a.current_cost + a.reserve_cost < b.current_cost + b.reserve_cost){
            return 1;
        }
        if(a.current_cost + a.reserve_cost > b.current_cost + b.reserve_cost){
            return -1;
        }
        return 0;
    }
    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistBySeverityDesc(injury_array){
    function compare(a, b){
        if(a.severity < b.severity){
            return -1;
        }
        if(a.severity > b.severity){
            return 1;
        }
        return 0;
    }
    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistBySeverityAsc(injury_array){
    function compare(a, b){
        if(a.severity < b.severity){
            return 1;
        }
        if(a.severity > b.severity){
            return -1;
        }
        return 0;
    }
    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistByClaimNumberDesc(injury_array){
    function compare(a, b){
        a = a.claim_number;
        b = b.claim_number;
        if(a === null){
            a = 0;
        }
        if(b === null){
            b = 0;
        }
        if(a < b){
            return -1;
        }
        if(a > b){
            return 1;
        }
        return 0;
    }

    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistByClaimNumberAsc(injury_array){
    function compare(a, b){
        a = a.claim_number;
        b = b.claim_number;
        if(a === null){
            a = 0;
        }
        if(b === null){
            b = 0;
        }
        if(a < b){
            return 1;
        }
        if(a > b){
            return -1;
        }
        return 0;
    }
    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistByStatusDesc(injury_array){
    function compare(a, b){
        if(a.status_number < b.status_number){
            return -1;
        }
        if(a.status_number > b.status_number){
            return 1;
        }
        return 0;
    }
    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistByStatusAsc(injury_array){
    function compare(a, b){
        if(a.status_number < b.status_number){
            return 1;
        }
        if(a.status_number > b.status_number){
            return -1;
        }
        return 0;
    }
    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistByMoodDesc(injury_array){
    function compare(a, b){
        if(a.mood < b.mood){
            return -1;
        }
        if(a.mood > b.mood){
            return 1;
        }
        return 0;
    }
    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistByMoodAsc(injury_array){
    function compare(a, b){
        if(a.mood < b.mood){
            return 1;
        }
        if(a.mood > b.mood){
            return -1;
        }
        return 0;
    }
    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistByStateDesc(injury_array){
    function compare(a, b){
        if(a.resolved < b.resolved){
            return -1;
        }
        if(a.resolved > b.resolved){
            return 1;
        }
        return 0;
    }
    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistByStateAsc(injury_array){
    function compare(a, b){
        if(a.resolved < b.resolved){
            return 1;
        }
        if(a.resolved > b.resolved){
            return -1;
        }
        return 0;
    }
    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistByProgressDesc(injury_array){
    function compare(a, b){
        if(a.employee_status == null){
            return -1;
        }
        else if(b.employee_status == null){
            return 1;
        }
        else if(a.employee_status < b.employee_status){
            return 1;
        }
        else if(a.employee_status > b.employee_status){
            return -1;
        }
    }
    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

function sortWatchlistByProgressAsc(injury_array){
    function compare(a, b){
        if(a.employee_status == null){
            return 1;
        }
        else if(b.employee_status == null){
            return -1;
        }
        else if(a.employee_status < b.employee_status){
            return -1;
        }
        else if(a.employee_status > b.employee_status){
            return 1;
        }
    }
    watchlist_injuries.sort(compare);
    buildMyWatchListable(true);
}

    
</script>
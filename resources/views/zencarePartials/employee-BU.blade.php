<?php
$severity = strtolower($injury->severity);
if($severity === "mild"){
    $severity = "minor";
}

$locationsArray = $injury->getLocations();
$optOutOfZenGardenNum = $injury->zengarden_opt_out;


?>
<!--
TODO: UPDATE status, update severity
-->


<div class="itemGrid">
    <div class="itemGrid__item thirds fixedHeight__380">
        <!--
        <div class="zen-actionPanel transparent">
            <div class="panelTitle full"><div class="icon inline icon-thumbs-up"></div> current mood</div>
            <div class="panelElement fixedHeight__230">
                <table class="displayData">

                    @if(!is_null($latest_injury_post))
                    <tr><td>injury<span class="moodnumber txt-mood-5">{{$latest_injury_post->mood}}</span><span class="datetime">{{$latest_injury_post->created_at->toDateTimeString()}}</span></td><td>
                            <div class="myMood mood-{{$latest_injury_post->mood}}">
                                <div class="moodVisual smaller">
                                    <div class="currentMood"></div>
                                </div>
                            </div>
                        </td></tr>
                    @endif
                    @if(!is_null($latest_claim_post))
                    <tr><td>experience<span class="moodnumber txt-mood-7">{{$latest_claim_post->mood}}</span><span class="datetime">{{$latest_claim_post->created_at->toDateTimeString()}}</span><span></span></td><td>
                            <div class="myMood mood-{{$latest_claim_post->mood}}">
                                <div class="moodVisual smaller">
                                    <div class="currentMood"></div>
                                </div>
                            </div>
                        </td></tr>
                        <tr><td>notes from<span class="firstname">{{$injured_employee->getFirstName()}}</span></td><td>{{count(\App\DiaryPost::where('injury_id', $injury->id)->where('note', '!=', '')->where('note', "!=", NULL)->get())}}</td></tr>
                    @endif
                </table>
                @if(is_null($latest_injury_post) && is_null($latest_claim_post))
                <table class="displayData">
                    <tr><td colspan="2" style="height:232px;position:relative;">
                            <div class="placeholderImage" style="background-image:url('/images/icons/hmm-trans.png');background-size:36%;">
                                <div class="center" style="position:absolute;top:-2px;width:100%;color:red;font-size:1rem;">No mood data for {{$injured_employee->getFirstName()}}</div>
                                <div class="center" style="position:absolute;top:20px;width:100%;color:grey;font-size:.8rem;">Send an invitation for the Zengarden<br>so they can update you with how they are doing!</div>
                            </div>
                        </td></tr> 
                </table>               
                <div class="buttonArray stick2bottom">
                    <button class="center olive showZencareModal" data-modalcontent="#inviteZengardenModal"><div class="icon icon-bamboo2"></div> Zengarden invitation</button>
                </div>   
                @endif              
            </div>
        </div>-->

        <div class="zen-actionPanel transparent">
        <div class="panelTitle full"><div class="icon inline icon-insurance"></div> claim details</div>
            <div class="panelElement">
                <table class="displayData tight">
                    <tr><td>date of injury</td><td><span class="injuryData truncate date">{{$injury->injury_date}}</span></td></tr>
                    <tr><td>severity</td><td class="severity"><span id="severityOverviewText" class="injuryData truncate {{$injury->severity}}"></span></td></tr>
                    <tr><td>description</td><td><span id="descriptionOverviewText" class="injuryData truncate description">{{$injury->notes}}</span></td></tr>
                    <tr><td>claim #</td><td><span id="overviewClaimNumberText" class="injuryData truncate claimnumber">{{$injury->claim_number ?: 'none entered'}}</span></td></tr>
                    <tr><td>adjuster name</td><td><span id="adjustername2" class="injuryData truncate adjuster">{{$injury->adjuster_name ?: 'none entered'}}</span></td></tr>
                    <tr><td>adjuster email</td><td><span id="adjusteremail2" class="injuryData truncate email"><a href="mailto:{{$injury->adjuster_email}}?subject=contact%20from%20zenjuries">{{$injury->adjuster_email ?: 'none entered'}}</a></span></td></tr>
                    <tr><td>adjuster phone</td><td><span id="adjusterphone2" class="injuryData truncate phonenumber"><a href="tel:{{$injury->adjuster_phone}}">{{$injury->adjuster_phone ?: 'none entered'}}</a></span></td></tr>
                </table>   
            </div>
        </div>
        <div class="buttonArray stick2bottom" style="bottom:60px;">
            <button class="center blue showZencareModal" data-modalcontent="#employeeInfoUpdateModal"><div class="icon icon-pencil"></div> update</button>&nbsp;
            <button class="center purple showZencareModal" data-modalcontent="#moreInjuryDetailsModal"><div class="icon icon-search"></div> view</button>&nbsp;
            <button class="center red showZencareModal" data-modalcontent="#stopforlitigationModal"><div class="icon icon-ban"></div> stop</button>
        </div>         
    </div>

    <div class="itemGrid__item thirds first fixedHeight__380"><!--increments of 15%-->
        <!--
        <div class="statusProgressionMeter">
            <div class="bar" style="width:0%;"></div>
            <ul>
                <li><div id="stateHospital" class="indicator"><div class="bobble"><div class="icon icon-ambulance"></div></div><div class="bobblelabel">hospital</div></div></li>
                <li><div id="stateUrgentCare" class="indicator"><div class="bobble"><div class="icon icon-clinic"></div></div><div class="bobblelabel">urgentCare</div></div></li>
                <li><div id="stateDoctor" class="indicator"><div class="bobble"><div class="icon icon-stethoscope"></div></div><div class="bobblelabel">at doctor</div></div></li>
                <li><div id="stateHome" class="indicator"><div class="bobble"><div class="icon icon-home"></div></div><div class="bobblelabel">at home</div></div></li>
                <li><div id="stateLightDuty" class="indicator"><div class="bobble"><div class="icon icon-rehab"></div></div><div class="bobblelabel">light duty</div></div></li>
                <li><div id="stateFullDuty" class="indicator"><div class="bobble"><div class="icon icon-workman"></div></div><div class="bobblelabel">full duty</div></div></li>
                <li><div id="stateRecovered" class="indicator"><div class="bobble"><div class="icon icon-newheart"></div></div><div class="bobblelabel">resolved</div></div></li>
            </ul>
            <div id="statusTerminated" class="statusOverlay terminated">employee terminated</div>
            <div id="statusResigned" class="statusOverlay resigned">employee resigned</div>
            <div id="statusDeath" class="statusOverlay deceased">employee deceased</div>
            <div id="statusResolved" class="statusOverlay resolved">claim resolved</div>
        </div>
        -->
        <div class="zen-actionPanel transparent" style="position:relative;top:-30px;">
            <div id="statusProgressCircle" class="progressMeter status{{ $state }}" style="width:140px;height:140px;background-color:#242424;">
                <svg viewBox="0 0 100 100">
                    <circle class="pm" r="50%" cx="50%" cy="50%"></circle>
                    <div class="overlay"><div class="avatar" style="background-color:{{$injured_employee->avatar_color}};background-image:url('{{$injured_employee->photo_url}}');"></div></div>
                </svg>
            </div>
            <div class="panelElement">
                <table class="displayData center">
                    <tr><td><span id="overviewEmployeeName" class="fullname">{{$injured_employee->name}}</span></td></tr>
                    <tr><td><span id="overviewEmployeeEmail" class="employeeEmail"><a href="mailto:{{$injured_employee->email}}?subject=email%20from%20zenjuries">{{$injured_employee->email ?: 'no email'}}</a></span>
                            <span id="overviewEmployeePhone" class="employeePhone"><a href="tel:{{$injured_employee->phone}}">{{$injured_employee->phone ?: 'no phone'}}</a></span>
                    </td></tr>
                    <!--
                    <tr><td><button class="showZencareModal blue" data-modalcontent="#updateStatusModal">Status: <span id="statusSpan">{{$injury->employee_status ?? 'none set'}}</span></button>
                    <span style="display:block;font-size:.65rem;letter-spacing:.1rem;color:grey;">(click to update status)</span></td></tr>
                    -->
                </table>
            </div>            
        </div>
        <div class="statusProgression">
            <div class="statusProgressionTitle">
                <span class="statusName">{{ $injured_employee->getFirstName(); }}'s progression <span class="statusNextup"> - next up [ <span id="nextStatus">rehab</span> ]</span></span>
               
                <span class="finalStatus resolved"></span>
                <span class="finalStatus resigned"></span>
                <span class="finalStatus terminated"></span>
                <span class="finalStatus deceased"></span>
                <span class="statusHistory"><button class="micro black showZencareModal" data-modalcontent="#statusHistoryModal"><div class="icon icon-history"></div> status history</button></span></div>
            
            <ul>
                <li><!-- minor, moderate, severe  -->
                    <a id="evalContainer" class="indicator evaluation {{ $severity }} showZencareModal" data-modalcontent="#updateStatusModal_evaluation">
                    <div class="statusStepLabel">evaluation</div>
                    <div class="statusIcon"></div>
                    <div class="statusIconLabel"></div>
                    <div class="multiStep" >
                        <ul>
                            @if($severity === "minor")
                            <li><div class="multiIndicator minor"></div></li>
                            @elseif($severity === "moderate")
                            <li><div class="multiIndicator moderate"></div></li>
                            @else
                            <li><div class="multiIndicator severe"></div></li>
                            @endif
                        </ul>
                    </div>
                    </a>
                </li>
                <li><!-- firstaid, doctor, urgentcare, hospital  -->
                    <a id="treatmentContainer" class="indicator treatment urgentcare showZencareModal" data-modalcontent="#updateStatusModal_treatment">
                    <div class="statusStepLabel">treatment</div>
                    <div class="statusIcon"></div>
                    <div class="statusIconLabel"></div>
                    <div class="multiStep">
                        <ul id="treatmentIndicatorList">
                            <li><div class="multiIndicator firstaid"></div></li>
                            <li><div class="multiIndicator doctor"></div></li>
                            <li><div class="multiIndicator urgentcare"></div></li>
                            <li><div class="multiIndicator hospital"></div></li>
                        </ul>
                    </div>
                </a></li>
                <li><a id="recoveryContainer" class="indicator recovery skipStep showZencareModal" data-modalcontent="#updateStatusModal_recovery">
                    <div class="statusStepLabel">recovery</div>
                    <div class="statusIcon"></div>
                    <div class="statusIconLabel"></div>
                    <div class="multiStep">
                        <ul id="recoveryIndicatorList">
                            <li><div class="multiIndicator icu"></div></li>
                            <li><div class="multiIndicator bedrest"></div></li>
                            <li><div class="multiIndicator home"></div></li>
                            <li><div class="multiIndicator work"></div></li>
                        </ul>
                    </div>                    
                </a></li>
                <li><a id="rehabContainer" class="indicator rehab showZencareModal" data-modalcontent="#updateStatusModal_rehab">
                    <div class="statusStepLabel">rehab</div>
                    <div class="statusIcon"></div>
                    <div class="statusIconLabel"></div>
                    <div class="multiStep">
                        <ul id="rehabIndicatorList">
                            <li><div class="multiIndicator therapy"></div></li>
                            <li><div class="multiIndicator lightduty"></div></li>
                            <li><div class="multiIndicator fullduty"></div></li>
                        </ul>
                    </div>                      
                </a></li>
                <li><a id="resolutionContainer" class="indicator resolution showZencareModal" data-modalcontent="#updateStatusModal_resolution">
                    <div class="statusStepLabel">resolution</div>
                    <div class="statusIcon"></div>
                    <div class="statusIconLabel"></div>
                    <div class="multiStep">
                        <ul id="resolutionIndicatorList">
                            <li><div class="multiIndicator fullrecovery"></div></li>
                            <li><div class="multiIndicator terminated"></div></li>
                            <li><div class="multiIndicator resigned"></div></li>
                            <li><div class="multiIndicator deceased"></div></li>
                        </ul>
                    </div>                      
                </a></li>
            </ul>
        </div>         
    </div>

    <div class="itemGrid__item thirds fixedHeight__380">
        <div class="zen-actionPanel transparent">
            <div class="panelTitle full"><div class="icon inline icon-injury"></div> injury details</div>
            <div class="panelElement">
            <div class="injuryLocationPicker small">
                <div class="pickerContainer">
                    <!--injuryGuy layers.  style "each div with "display:block" to make red area show.-->
                    <div class="layer base"></div>
                    <div id="layerSystematic" class="layer systematic"></div>
                    <div id="layerHead" class="layer head"></div>
                    <div id="layerNeck" class="layer neck"></div>
                    <div id="layerUpper" class="layer upper"></div>
                    <div id="layerRightArm" class="layer r-arm"></div>
                    <div id="layerLeftArm" class="layer l-arm"></div>
                    <div id="layerTrunk" class="layer trunk"></div>
                    <div id="layerLower" class="layer lower"></div>
                    <div id="layerRightLeg" class="layer r-leg"></div>
                    <div id="layerLeftLeg" class="layer l-leg"></div>
                </div>
            </div>
                <table class="displayData tight">
                    <tr><td>location(s)</td><td><span class="injuryData truncate location">{{printLocations($injury)}}</span></td></tr>
                    <tr><td>type(s)</td><td><span class="injuryData truncate type">{{printTypes($injury)}}</span></td></tr>
                </table>   
            </div>
        </div>
    </div>
</div>



<div class="itemGrid">
    <div class="itemGrid__item thirds fixedHeight__200">
        <div class="zen-actionPanel transparent">
            <!-- TODO: should add a state for resolved claims -->
            <div class="panelTitle full"><div class="icon inline icon-history"></div> time in progress</div>
            <div class="panelElement">
                <div class="timeInProgress">
                    <div class="progressMeter status{{calculateProgressChart($injury)}}" style="width:125px;height:125px;background-color:#104f8d;">
                        <svg viewBox="0 0 100 100">
                            <circle class="pm" r="50%" cx="50%" cy="50%"></circle>
                            <div class="overlay"><span class=largeNumber>{{ $injury_duration }}d</span></div>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--
        <div class="feedbackRate">
                barFG classes = user|base|team
            <div class="barBG"><div class="barFG {{$post_chart_array['setting']}}"><div class="rate">{{$post_chart_array['diary_count']}}/{{$post_chart_array['chat_count']}}</div><div class="total"><span>{{$post_chart_array['total_posts']}}</span> messages total</div></div></div>
        </div>-->
    <div class="itemGrid__item thirds fixedHeight__200">
        <div class="zen-actionPanel transparent">
            <div class="panelTitle full"><div class="icon inline icon-thumbs-up"></div>zengarden</div>
            <div class="panelElement fixedHeight__200">
            @if($optOutOfZenGardenNum === 0)
                <table class="displayData">
                    @if(!is_null($latest_injury_post))
                    <tr><td>injury<span class="moodnumber txt-mood-5">{{$latest_injury_post->mood}}</span><span class="datetime">{{$latest_injury_post->created_at->toDateTimeString()}}</span></td><td>
                            <div class="myMood mood-{{$latest_injury_post->mood}}">
                                <div class="moodVisual smaller">
                                    <div class="currentMood"></div>
                                </div>
                            </div>
                        </td></tr>
                    @endif
                    @if(!is_null($latest_claim_post))
                    <tr><td>experience<span class="moodnumber txt-mood-7">{{$latest_claim_post->mood}}</span><span class="datetime">{{$latest_claim_post->created_at->toDateTimeString()}}</span><span></span></td><td>
                            <div class="myMood mood-{{$latest_claim_post->mood}}">
                                <div class="moodVisual smaller">
                                    <div class="currentMood"></div>
                                </div>
                            </div>
                        </td></tr>
                        <tr><td>notes from<span class="firstname">{{$injured_employee->getFirstName()}}</span></td><td>{{count(\App\DiaryPost::where('injury_id', $injury->id)->where('note', '!=', '')->where('note', "!=", NULL)->get())}}</td></tr>
                    @endif
                </table>
                @if(!is_null($latest_claim_post) || !is_null($latest_injury_post))
                <div class="buttonArray stick2bottom">
                    <button class="center red"><div class="icon icon-bamboo2"></div>Opt Out of Zengarden</button>
                </div> 
                @endif
                @if(is_null($latest_injury_post) && is_null($latest_claim_post))
                <table class="displayData">
                    <tr><td colspan="2" style="height:232px;position:relative;">
                            <div class="placeholderImage" style="background-image:url('/images/icons/hmm-trans.png');background-size:36%;">
                                <div class="center" style="position:absolute;top:-2px;width:100%;color:red;font-size:1rem;">No mood data for {{$injured_employee->getFirstName()}}</div>
                                <div class="center" style="position:absolute;top:20px;width:100%;color:grey;font-size:.8rem;">Send an invitation for the Zengarden<br>so they can update you with how they are doing!</div>
                            </div>
                        </td></tr> 
                </table>               
                <div class="buttonArray stick2bottom">
                    <button class="center olive showZencareModal" data-modalcontent="#inviteZengardenModal"><div class="icon icon-bamboo2"></div> Zengarden invitation</button>
                    <button class="center red showZencareModal" data-modalcontent="#zengardenOptOutModal"><div class="icon icon-bamboo2"></div>Opt Out of Zengarden</button>
                </div>   
                @endif
            @else
            <table class="displayData">
                    <tr><td colspan="2" style="height:232px;position:relative;">
                            <div class="placeholderImage" style="background-image:url('/images/icons/hmm-trans.png');background-size:36%;">
                                <div class="center" style="position:absolute;top:-2px;width:100%;color:red;font-size:1rem;">Opted out of Zengarden</div>
                                <div class="center" style="position:absolute;top:20px;width:100%;color:grey;font-size:.8rem;">Employee will not be invited to the Zengarden</div>
                            </div>
                        </td></tr> 
                </table>   
            @endif           
            </div>
        </div>
    </div>

    <div class="itemGrid__item thirds fixedHeight__200">
        <div class="zen-actionPanel transparent">
            <div class="panelTitle full"><div class="icon inline icon-commenting-o"></div> recent communication</div>
            <div class="panelElement">
                <table id="recentComms" class="displayData tight">
                    <!--
                    <tr><td>adjuster name</td><td><span id="adjustername" class="injuryData truncate adjuster">{{$injury->adjuster_name ?: 'none entered'}}</span></td></tr>
                    <tr><td>adjuster email</td><td><span id="adjusteremail" class="injuryData truncate email"><a href="mailto:{{$injury->adjuster_email}}?subject=contact%20from%20zenjuries">{{$injury->adjuster_email ?: 'none entered'}}</a></span></td></tr>
                    <tr><td>adjuster phone</td><td><span id="adjusterphone" class="injuryData truncate phonenumber"><a href="tel:{{$injury->adjuster_phone}}">{{$injury->adjuster_phone ?: 'none entered'}}</a></span></td></tr>
                    -->
                </table>
                <br><br>
                <!--
                <div class="buttonArray">
                    <button class="center cyan showZencareModal" data-modalcontent="#adjusterModal"><div class="icon icon-agent"></div> edit adjuster</button>
                </div> 
                -->


                <div class="trendGraph" style="display:none;">
                    @if($recent_mood_array[0] != '-?-')
                    <!-- barFG classes = bg-mood-1 -> bg-mood-10 -->
                    
                    <div id="moodChart" class="chart" style="">
                        <div class="barBG"><div class="barFG a1 bg-mood-{{$recent_mood_array[0]}}"></div></div>
                        <div class="barBG"><div class="barFG a2 bg-mood-{{$recent_mood_array[1]}}"></div></div>
                        <div class="barBG"><div class="barFG a3 bg-mood-{{$recent_mood_array[2]}}"></div></div>
                        <div class="barBG"><div class="barFG a4 bg-mood-{{$recent_mood_array[3]}}"></div></div>
                        <div class="barBG"><div class="barFG a5 bg-mood-{{$recent_mood_array[4]}}"></div></div>
                    </div>
                    @else
                    <div id="moodChartDefault" class="placeholderImage actionChart" style="background-image:url('/images/icons/barchart-trans.png');"><span style="position:relative;top:50px;">No Mood Data... yet.</span></div>
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function(){
        var status_history = <?php echo json_encode($status_history) ?>;
        console.log(status_history);
        @if($injury->resolved === 1)
        var status = "Resolved";
        @else
        var status = <?php echo json_encode($employeeStatus); ?>;
        @endif
        
        displayStatus(status_history);
        displayStatusHistory(status_history);
    });
    /******* New status update code *******/
    function displayStatus(status_history){
        resetStatusClasses();
        for(var i = 0; i < status_history.length; i++){
            console.log('running display status');
            var status = status_history[i].status;
            console.log(status);
            if(status === "Urgent Care" || status === "Hospital" || status === "Doctor" || status === "First Aid"){
                resertTreatment();
                var container = $('#treatmentContainer');
                switch(status){
                    case 'Urgent Care':
                        container.addClass('urgentcare');
                        break;
                    case 'Hospital':
                        container.addClass('hospital');
                        break;
                    case 'Doctor':
                        contianter.addClass('doctor');
                        break;
                    case 'First Aid':
                        container.addClass('firstaid');
                        break;
                }
                
            }

            if(status === "Home"){
                resetRecovery();
                console.log('status is Home, adding to recovery container');
                $('#recoveryContainer').addClass('home');
            }

            if(status === "Light Duty"){
                resetRehab();
                $('rehabContainer').addClass('lightduty');
            }else if(status === "Full Duty"){
                resetRehab();
                $('#rehabContainer').addClass('fullduty');
            }
            if(status === "Maximum Medical Improvement" || status === "Terminated" || status === "Resigned" || status === "Death"){
                resetResolution();
                var container = $('#resolutionContainer');
                switch(status){
                    case "Maximum Medical Improvement":
                        container.addClass('fullrecovery');
                        break;
                    case "Terminated":
                        container.addClass('terminated');
                        break;
                    case "Resigned":
                        container.addClass('resigned');
                        break;
                    case "Death":
                        container.addClass('deceased');
                        break;
                }
            }
        }
    }

    function displayStatusHistory(status_history){
        console.log(JSON.stringify(status_history));
        var treatmentArray = [];
        var treatmentHtml = '';
        var recoveryArray = [];
        var recoveryHtml = '';
        var rehabArray = [];
        var rehabHtml = '';
        var resolutionArray = [];
        var resolutionHtml = '';
        var historyHtml = '';
        for(var i = 0; i < status_history.length; i++){
            historyHtml += "<li>Status changed to <b>" + status_history[i].status + "</b> on <b>" + status_history[i].status_changed_date + "</b></li>";
            status = status_history[i].status;
            status = status.toLowerCase().replace(/ +/g, "");
            console.log('status: ' + status);
            //html for each status update here
            switch(status){
                case 'urgentcare':
                case 'hospital':
                case 'doctor':
                case 'firstaid':
                case 'treatmentskip':
                    if(treatmentArray.length >= 4){
                        treatmentArray.shift();
                    }
                    treatmentArray.push(status);
                    break;
                case 'home':
                case 'recoveryskip':
                    if(recoveryArray.length >= 4){
                        recoveryArray.shift();
                    }
                    recoveryArray.push(status);
                    break;
                case 'lightduty':
                case 'fullduty':
                case 'rehabskip':
                    if(rehabArray.length >= 4){
                        rehabArray.shift();
                    }
                    rehabArray.push(status);
                    break;
                case 'fullrecovery':
                case 'terminated':
                case 'deceased':
                case 'resigned':
                    if(resolutionArray.length >= 4){
                        resolutionArray.shift();
                    }
                    resolutionArray.push(status);
                    break;

            }
        }
        for(var i = 0; i < treatmentArray.length; i++){
            treatmentHtml += '<li><div class="multiIndicator '+treatmentArray[i]+'"></div></li>';
        }
        $('#treatmentIndicatorList').html(treatmentHtml);

        for(var i = 0; i < recoveryArray.length; i++){
            recoveryHtml += '<li><div class="multiIndicator '+recoveryArray[i]+'"></div></li>';
        }
        $('#recoveryIndicatorList').html(recoveryHtml);

        for(var i = 0; i < rehabArray.length; i++){
            rehabHtml += '<li><div class="multiIndicator '+rehabArray[i]+'"></div></li>';
        }
        $('#rehabIndicatorList').html(rehabHtml);

        for(var i = 0; i < resolutionArray.length; i++){
            resolutionHtml += '<li><div class="multiIndicator '+resolutionArray[i]+'"></div></li>';
        }
        $('#resolutionIndicatorList').html(resolutionHtml);
        console.log(treatmentArray);
        console.log(recoveryArray);
        console.log(rehabArray);
        console.log(resolutionArray);
        $('#statusHistoryContainer').html(historyHtml);
    }

    function resetStatusClasses(){
        resertTreatment();
        resetRecovery();
        resetRehab();
        resetResolution();

    }

    function resertTreatment(){
        $('#treatmentContainer').removeClass('urgentcare');
        $('#treatmentContainer').removeClass('hospital');
        $('#treatmentContainer').removeClass('doctor');
        $('#treatmentContainer').removeClass('firstaid');
        $('#treatmentContainer').removeClass('skipStep');
    }
    function resetRecovery(){
        $('#recoveryContainer').removeClass('home');
        $('#recoveryContainer').removeClass('skipStep');
    }
    function resetRehab(){
        $('rehabContainer').removeClass('lightduty');
        $('#rehabContainer').removeClass('fullduty');
        $('#rehabContainer').removeClass('skipStep');
    }
    function resetResolution(){
        $('#resolutionContainer').removeClass('fullrecovery');
        $('#resolutionContainer').removeClass('terminated');
        $('#resolutionContainer').removeClass('resigned');
        $('#resolutionContainer').removeClass('deceased');
    }
    /***** End new status update code *****/
    function displayProgressBar(state){
        var bar = $('.statusProgressionMeter').find('.bar');
        var hospital = $('#stateHospital');
        var urgentCare = $('#stateUrgentCare');
        var doctor = $('#stateDoctor');
        var home = $('#stateHome');
        var lightDuty = $('#stateLightDuty');
        var fullDuty = $('#stateFullDuty');
        var recovered = $('#stateRecovered');

        //remove all the on classes for rebuilding
        hospital.removeClass('on');
        urgentCare.removeClass('on');
        doctor.removeClass('on');
        home.removeClass('on');
        lightDuty.removeClass('on');
        fullDuty.removeClass('on');
        recovered.removeClass('on');
        $('#statusTerminated').hide();
        $('#statusResigned').hide();
        $('#statusDeath').hide();
        $('#statusResolved').hide();

        switch(state){
            case "Hospital":
                bar.css('width', '0%');
                hospital.addClass('on');
                break;
            case "Urgent Care":
                bar.css('width', '15%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                break;
            case "Family Practice":
                bar.css('width', '30%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                break;
            case "Home":
                bar.css('width', '45%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                break;
            case "Light Duty":
                bar.css('width', '60%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                lightDuty.addClass('on');
                break;
            case "Full Duty":
                bar.css('width', '75%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                lightDuty.addClass('on');
                fullDuty.addClass('on');
                break;
            case "Maximum Medical Improvement":
                bar.css('width', '85%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                lightDuty.addClass('on');
                fullDuty.addClass('on');
                recovered.addClass('on');
                break;
            case "Terminated":
                $('#statusTerminated').show();
                bar.css('width', '85%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                lightDuty.addClass('on');
                fullDuty.addClass('on');
                recovered.addClass('on');
                break;
            case "Resigned":
                $('#statusResigned').show();
                bar.css('width', '85%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                lightDuty.addClass('on');
                fullDuty.addClass('on');
                recovered.addClass('on');
                break;
            case "Death":
                $('#statusDeath').show();
                bar.css('width', '85%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                lightDuty.addClass('on');
                fullDuty.addClass('on');
                recovered.addClass('on');
                break;
            case "Resolved":
                console.log(state);
                $('#statusResolved').show();
                bar.css('width', '85%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                lightDuty.addClass('on');
                fullDuty.addClass('on');
                recovered.addClass('on');
                break;

        }
    }
</script>

<script>

    //check which injury areas to show red
    var locations = <?php echo json_encode($locationsArray); ?>;
    locations = JSON.stringify(locations);
    locations = locations.replace(/[^a-zA-Z ]/g, "");
    locations = locations.toLowerCase();

    if(locations.includes("body system") || locations.includes("nonclassifiable") || locations.includes("multiple parts") || locations.includes("body parts")
    || locations.includes("death")){
        var findVar = document.getElementById("layerSystematic");
        findVar.style = "display:block";
    }

    if(locations.includes("head") || locations.includes("brain") || locations.includes("ear(s)") || locations.includes("ear(s) external")
    || locations.includes("lef ear external") || locations.includes("right ear external") || locations.includes("ear(s) internal") || locations.includes("left ear internal") ||
    locations.includes("right ear internal") || locations.includes("eye(s)") || locations.includes("left eye") || locations.includes("right eye") ||
    locations.includes("face") || locations.includes("jaw") || locations.includes("mouth") || locations.includes("nose") ||
    locations.includes("face, multi-part") || locations.includes("face, NEC") || locations.includes("scalp") || locations.includes("skull") ||
    locations.includes("head, multi-part") || locations.includes("head, NEC")){
        var findVar = document.getElementById("layerHead");
        findVar.style = "display:block";
    }

    if(locations.includes("neck")){
        var findVar = document.getElementById("layerNeck");
        findVar.style = "display:block";
    }

    if(locations.includes("right arm") || locations.includes("upper right arm") || locations.includes("right elbow") || locations.includes("right forearm")
    || locations.includes("right wrist") || locations.includes("right hand") || locations.includes("right fingers, thumb") || locations.includes("right lower extremities") || locations.includes("right arm lower extremities")){
        var findVar = document.getElementById("layerRightArm");
        findVar.style = "display:block";
    }

    if(locations.includes("left arm") || locations.includes("upper left arm") || locations.includes("left elbow") || locations.includes("left forearm")
    || locations.includes("left wrist") || locations.includes("left hand") || locations.includes("left fingers, thumb") || locations.includes("left lower extremities") || locations.includes("left arm lower extremities")){
        var findVar = document.getElementById("layerLeftArm");
        findVar.style = "display:block";
    }

    if(locations.includes("trunk") || locations.includes("abdomen") || locations.includes("back") || locations.includes("chest")
    || locations.includes("hips, both") || locations.includes("left hip") || locations.includes("right hip") || locations.includes("shoulders, both") || 
    locations.includes("left shoulder") || locations.includes("right shoulder") || locations.includes("trunk, multi-part") || locations.includes("trunk, NEC")){
        var findVar = document.getElementById("layerTrunk");
        findVar.style = "display:block";
    }

    if(locations.includes("arms") || locations.includes("upper arms") || locations.includes("elbows") || locations.includes("forearms")
    || locations.includes("arms, multi-part") || locations.includes("arms, NEC") || locations.includes("wrists") || locations.includes("hands") || 
    locations.includes("fingers, thumbs") || locations.includes("upper extremities") || locations.includes("upper extremities, NEC")){
        var findVar = document.getElementById("layerLeftArm");
        findVar.style = "display:block";
        var findVar2 = document.getElementById("layerRightArm");
        findVar2.style = "display:block";
    }

    if(locations.includes("left leg") || locations.includes("left knee") || locations.includes("left shin") || locations.includes("left ankle")
    || locations.includes("left foot") || locations.includes("left toes") || locations.includes("lower extremities, left") || locations.includes("left leg lower extremities")){
        var findVar = document.getElementById("layerLeftLeg");
        findVar.style = "display:block";
    }

    if(locations.includes("right leg") || locations.includes("right knee") || locations.includes("right shin") || locations.includes("right ankle")
    || locations.includes("right foot") || locations.includes("right toes") || locations.includes("lower extremities, right") || locations.includes("right leg lower extremities")){
        var findVar = document.getElementById("layerRightLeg");
        findVar.style = "display:block";
    }

    if(locations.includes("both legs") || locations.includes("both knees") || locations.includes("lower legs") || locations.includes("legs, multi-part")
    || locations.includes("both ankles") || locations.includes("both feet") || locations.includes("all toes") || locations.includes("lower extremities, multi") || locations.includes("lower extremities, NEC")){
        var findVar = document.getElementById("layerLeftLeg");
        findVar.style = "display:block";
        var findVar2 = document.getElementById("layerRightLeg");
        findVar2.style = "display:block";
    }

</script>

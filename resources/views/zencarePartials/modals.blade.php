<div id="zencareModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
    @include('partials.modals.updateCosts')     
    @include('partials.modals.updateClaimStatus')      
    @include('partials.modals.updateStatus')    
    @include('partials.modals.postMessage')
    @include('partials.modals.claimNumber')
    @include('partials.modals.addAdjuster')
    @include('partials.modals.editEmployeeInfo')
    @include('partials.modals.addAppointment') 
    @include('partials.modals.editAppointment') 
    @include('partials.modals.addAppointmentSummary')    
    @include('partials.modals.resolveInjury')
    @include('partials.modals.stopforlitigation')  
    @include('partials.modals.zengardenInvite')
    @include('partials.modals.moreInjuryDetails')
    @include('partials.modals.addFinalCost')
    @include('partials.modals.completeInjury')     
    @include('partials.modals.injuryAlertConfirmModal') 
    @include('partials.modals.firstReportConfirmModal') 
    @include('partials.modals.updateStatus_evaluation')
    @include('partials.modals.updateStatus_treatment')
    @include('partials.modals.updateStatus_recovery')
    @include('partials.modals.updateStatus_rehab')
    @include('partials.modals.updateStatus_resolution')
    @include('partials.modals.statusHistory')
    @include('partials.modals.zengardenOptOutModal')
    @include('partials.modals.photoUpload')

</div>

<script>
    $('.showZencareModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zencareModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();

        //script related to a specific content block can be added like this
        //status modal
        if(target === "#updateStatusModal"){

        }else if(target === "#treePostModal"){

        }else if(target === "#addAppointmentModal"){
            console.log("hitting else if");
            var apptType = $(this).attr("data-apptType");
            console.log(apptType);
            if($(this).attr("data-apptType") === "new"){
                console.log("apptType is new");
              $('#setAptTitle').show();
              $('#setFollowTitle').hide();
            }else{
                console.log("apptype is follow")
              $('#setAptTitle').hide();
              $('#setFollowTitle').show();
            }

            // setTimeout(function(){
            //     datepickerDefault = new MtrDatepicker({
            //         target: "zDateTime",
            //         animation: true,
            //         timestamp: new Date().getTime(),

            //     });
            // }, 1)
        }else if(target === "#editAppointmentModal"){
            var appointment_id = $(this).data('appointmentid');
            var parent = $(this).closest('.apptCard');
            var datetime = parent.find('.apptTimeFormatted').val();
            //var datetime = $(this).data('editdatetime'); //parent.find('.apptTimeFormatted').val();
            console.log("date in modals " + datetime);
            // setTimeout(function(){
            //     console.log(datetime);
            //     datepickerDefault = new MtrDatepicker({
            //         target: "zDateTimeEditAppointment",
            //         animation: true,
            //         timestamp: new Date().getTime(),
            //         // timestamp: datetime,

            //     });
            // }, 1);

            //datepickerDefault.timestamp =

            $('#editAppointmentName').val(parent.find('.apptDoctor').html());
            $('#editAppointmentPhone').val(parent.find('.apptPhone').html());
            $('#editAppointmentEmail').val(parent.find('.apptEmail').find('a').html());
            $('#editAppointmentNote').val(parent.find('.apptDescription').html());
            $('#editAppointmentID').val(appointment_id);
            console.log("appointmentid in modals2: " + appointment_id);
        }else if(target === "#addSummaryModal"){
            $('#addSummaryModalAppointmentID').val($(this).data('appointmentid'));
        }
        //modal ID goes here
        modal.open();
    });

</script>
<?php
$future_appointments = DB::table('appointments')->join('injuries_care_locations', 'appointments.location_id', '=', 'injuries_care_locations.id')->where('appointments.injury_id', $injury->id)->where('appointments.cancelled', 0)->where('appointments.appointment_time', '>', \Carbon\Carbon::now())
    ->select('appointments.*', 'injuries_care_locations.name', 'injuries_care_locations.email', 'injuries_care_locations.phone')->get();

//$past_appointments = \App\Appointment::where('injury_id', $injury->id)->where('cancelled', 0)->whereDate('appointment_time', '<', \Carbon\Carbon::now())->get();

$past_appointments = DB::table('appointments')->join('injuries_care_locations', 'appointments.location_id', '=', 'injuries_care_locations.id')->where('appointments.injury_id', $injury->id)->where('appointments.cancelled', 0)->where('appointments.appointment_time', '<=', \Carbon\Carbon::now())
    ->select('appointments.*', 'injuries_care_locations.name', 'injuries_care_locations.email', 'injuries_care_locations.phone')->get();
?>


<div class="itemGrid" style="width:80%;margin:0 auto;">
    <div class="itemGrid__item half max">
        <div class="zen-actionPanel transparent">
            <div class="panelTitle full"><div class="icon inline icon-calendar-plus-o"></div> new appointments</div>
            <div class="zAppointments" id="newAppointmentList">
                <div class="myAppointments noList">
                    <span class="default">no new appointments</span>
                </div>
                <div class="myAppointments hasList">
                    @foreach($future_appointments as $apt)
                        <div class="apptCard" data-appointmentid="{{$apt->id}}">
                            <div class="apptHeader">
                                <div class="apptIcon"><div class="icon inline icon-clock"></div></div>
                                <div class="apptTime">{{\Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $apt->appointment_time)->format("M j Y / g:ia")}}</div>
                            </div>
                            <input type="hidden" class="apptTimeFormatted" value="{{$apt->appointment_time}}">
                            <div class="apptBody">
                                <div class="apptDoctor">{{$apt->name}}</div>
                                <div class="apptPhone">{{$apt->phone}}</div>
                                <div class="apptEmail">
                                    <a href="mailto:{{$apt->email}}">
                                        {{$apt->email}}</a></div>
                                <div class="apptDescription">{{$apt->description}}</div>
                                <div class="apptSummary">{{$apt->summary}}</div>
                                <div class="apptControls">
                                    <button data-appointmentid="{{$apt->id}}" class="cyan addSummaryModal showZencareModal" data-modalcontent="#addSummaryModal">+ summary</button>
                                    <button data-appointmentid="{{$apt->id}}" class="blue addAppointmentModal showZencareModal" data-modalcontent="#addAppointmentModal" data-apptType="followup">+ follow-up</button>
                                </div>
                                <div class="apptEdit">
                                    <button data-appointmentid="{{$apt->id}}" class="gold editAppointmentModal showZencareModal" data-modalcontent="#editAppointmentModal"><div class="icon inline icon-pencil"></div> edit</button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="buttonArray">
                    <button class="green showZencareModal" data-modalcontent="#addAppointmentModal" data-apptType="new">add new appointment</button>
                </div>

            </div>
        </div>
    </div>
    <div class="itemGrid__item half max">
        <div class="zen-actionPanel transparent">
            <div class="panelTitle full"><div class="icon inline icon-calendar-check-o"></div> past appointments</div>
            <div class="zAppointments" id="newAppointmentList">
                <div class="pastAppointments noList">
                    <span class="default">no new appointments</span>
                </div>
                <div class="pastAppointments hasList">
                    <!--appointment card-->
                    @foreach($past_appointments as $apt)
                        <div class="apptCard" data-appointmentid="{{$apt->id}}">
                            <div class="apptHeader">
                                <div class="apptIcon"><div class="icon inline icon-clock"></div></div>
                                <div class="apptTime">{{\Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $apt->appointment_time)->format("M j Y / g:ia")}}</div>
                            </div>
                            <input type="hidden" class="apptTimeFormatted" value="{{$apt->appointment_time}}">
                            <div class="apptBody">
                                <div class="apptDoctor">{{$apt->name}}</div>
                                <div class="apptPhone">{{$apt->phone}}</div>
                                <div class="apptEmail">
                                    <a href="mailto:{{$apt->email}}">
                                        {{$apt->email}}</a></div>

                                <div class="apptDescription">{{$apt->description}}</div>
                                <div class="apptSummary">{{$apt->summary}}</div>
                                <div class="apptControls">
                                    <button data-appointmentid="{{$apt->id}}" class="cyan addSummaryModal showZencareModal" data-modalcontent="#addSummaryModal">+ summary</button>
                                    <button data-appointmentid="{{$apt->id}}" class="blue addAppointmentModal showZencareModal" data-modalcontent="#addAppointmentModal">+ follow-up</button>
                                </div>
                                <div class="apptEdit">
                                    <button data-appointmentid="{{$apt->id}}" class="gold editAppointmentModal showZencareModal" data-modalcontent="#editAppointmentModal"><div class="icon inline icon-pencil"></div> edit</button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <!-- end appointment card-->
                </div>
            </div>
        </div>
    </div>
</div>
<div style="width:80%;margin:0 auto;margin-bottom:40px; display: none">
    <div class="panelTitle full"><div class="icon inline icon-clinic"></div> facilities</div>
    <div class="panelElement">
        <button class="cyan">add facility</button>
    </div>
</div>

<script>

</script>
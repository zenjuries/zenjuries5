<?php


?>
<!--
TODO: UPDATE status, update severity
-->


<div class="itemGrid">
    <div class="itemGrid__item thirds">
        <div class="zen-actionPanel transparent">
            <div class="panelTitle full"><div class="icon inline icon-thumbs-up"></div> current mood</div>
            <div class="panelElement fixedHeight__230">
                <table class="displayData">

                    @if(!is_null($latest_injury_post))
                    <tr><td>injury<span class="moodnumber txt-mood-5">{{$latest_injury_post->mood}}</span><span class="datetime">{{$latest_injury_post->created_at->toDateTimeString()}}</span></td><td>
                            <div class="myMood mood-{{$latest_injury_post->mood}}">
                                <div class="moodVisual smaller">
                                    <div class="currentMood"></div>
                                </div>
                            </div>
                        </td></tr>
                    @endif
                    @if(!is_null($latest_claim_post))
                    <tr><td>experience<span class="moodnumber txt-mood-7">{{$latest_claim_post->mood}}</span><span class="datetime">{{$latest_claim_post->created_at->toDateTimeString()}}</span><span></span></td><td>
                            <div class="myMood mood-{{$latest_claim_post->mood}}">
                                <div class="moodVisual smaller">
                                    <div class="currentMood"></div>
                                </div>
                            </div>
                        </td></tr>
                        <tr><td>notes from<span class="firstname">{{$injured_employee->getFirstName()}}</span></td><td>{{count(\App\DiaryPost::where('injury_id', $injury->id)->where('note', '!=', '')->where('note', "!=", NULL)->get())}}</td></tr>
                    @endif
                </table>
                @if(is_null($latest_injury_post) && is_null($latest_claim_post))
                <table class="displayData">
                    <tr><td colspan="2" style="height:232px;position:relative;">
                            <div class="placeholderImage" style="background-image:url('/images/icons/hmm-trans.png');background-size:36%;">
                                <div class="center" style="position:absolute;top:-2px;width:100%;color:red;font-size:1rem;">No mood data for {{$injured_employee->getFirstName()}}</div>
                                <div class="center" style="position:absolute;top:20px;width:100%;color:grey;font-size:.8rem;">Send an invitation for the Zengarden<br>so they can update you with how they are doing!</div>
                            </div>
                        </td></tr> 
                </table>               
                <div class="buttonArray stick2bottom">
                    <button class="center olive showZencareModal" data-modalcontent="#inviteZengardenModal"><div class="icon icon-bamboo2"></div> Zengarden invitation</button>
                </div>   
                @endif              
            </div>
        </div>
    </div>

    <div class="itemGrid__item thirds first">
        <div class="statusProgressionMeter">
            <div class="bar" style="width:0%;"><!--increments of 15%--></div>
            <ul>
                <li><div id="stateHospital" class="indicator"><div class="bobble"><div class="icon icon-ambulance"></div></div><div class="bobblelabel">hospital</div></div></li>
                <li><div id="stateUrgentCare" class="indicator"><div class="bobble"><div class="icon icon-clinic"></div></div><div class="bobblelabel">urgentCare</div></div></li>
                <li><div id="stateDoctor" class="indicator"><div class="bobble"><div class="icon icon-stethoscope"></div></div><div class="bobblelabel">at doctor</div></div></li>
                <li><div id="stateHome" class="indicator"><div class="bobble"><div class="icon icon-home"></div></div><div class="bobblelabel">at home</div></div></li>
                <li><div id="stateLightDuty" class="indicator"><div class="bobble"><div class="icon icon-rehab"></div></div><div class="bobblelabel">light duty</div></div></li>
                <li><div id="stateFullDuty" class="indicator"><div class="bobble"><div class="icon icon-workman"></div></div><div class="bobblelabel">full duty</div></div></li>
                <li><div id="stateRecovered" class="indicator"><div class="bobble"><div class="icon icon-newheart"></div></div><div class="bobblelabel">resolved</div></div></li>
            </ul>
            <div id="statusTerminated" class="statusOverlay terminated">employee terminated</div>
            <div id="statusResigned" class="statusOverlay resigned">employee resigned</div>
            <div id="statusDeath" class="statusOverlay deceased">employee deceased</div>
            <div id="statusResolved" class="statusOverlay resolved showZencareModal" data-modalcontent="#updateClaimStatus">claim resolved</div>
        </div>
        <div class="zen-actionPanel transparent" style="position:relative;top:-10px;">
            <div id="statusProgressCircle" class="progressMeter status{{ $state }}" style="width:100px;height:100px;background-color:#242424;">
                <svg viewBox="0 0 100 100">
                    <circle class="pm" r="50%" cx="50%" cy="50%"></circle>
                    <div class="overlay"><div class="avatar" style="background-color:{{$injured_employee->avatar_color}};background-image:url('{{$injured_employee->photo_url}}');"></div></div>
                </svg>
            </div>
            <div class="panelElement">
                <table class="displayData center">
                    <tr><td><span id="overviewEmployeeName" class="fullname">{{$injured_employee->name}}</span></td></tr>
                    <tr><td><span id="overviewEmployeeEmail" class="employeeEmail"><a href="mailto:{{$injured_employee->email}}?subject=email%20from%20zenjuries">{{$injured_employee->email ?: 'no email'}}</a></span>
                            <span id="overviewEmployeePhone" class="employeePhone"><a href="tel:{{$injured_employee->phone}}">{{$injured_employee->phone ?: 'no phone'}}</a></span>
                    </td></tr>
                    <tr><td><button class="showZencareModal blue" data-modalcontent="#updateStatusModal">Status: <span id="statusSpan">{{$injury->employee_status ?? 'none set'}}</span></button>
                    <span style="display:block;font-size:.65rem;letter-spacing:.1rem;color:grey;">(click to update status)</span></td></tr>
                </table>
            </div>
        </div>
    </div>

    <div class="itemGrid__item thirds">
        <div class="zen-actionPanel transparent">
            <div class="panelTitle full"><div class="icon inline icon-injury"></div> injury details</div>
            <div class="panelElement fixedHeight__230">
                <table class="displayData tight fixedHeight__200">
                    <tr><td>date of injury</td><td><span class="injuryData truncate date">{{$injury->injury_date}}</span></td></tr>
                    <tr><td>severity</td><td class="severity"><span id="severityOverviewText" class="injuryData truncate {{$injury->severity}}"></span></td></tr>
                    <tr><td>location(s)</td><td><span class="injuryData truncate location">{{printLocations($injury)}}</span></td></tr>
                    <tr><td>type(s)</td><td><span class="injuryData truncate type">{{printTypes($injury)}}</span></td></tr>
                    <tr><td>description</td><td><span id="descriptionOverviewText" class="injuryData truncate description">{{$injury->notes}}</span></td></tr>
                    <tr><td>claim #</td><td><span id="overviewClaimNumberText" class="injuryData truncate claimnumber">{{$injury->claim_number ?: 'none entered'}}</span></td></tr>
                </table>
                <div class="buttonArray stick2bottom">
                    <button class="center blue showZencareModal" data-modalcontent="#employeeInfoUpdateModal"><div class="icon icon-pencil"></div> update</button>&nbsp;
                    <button class="center purple showZencareModal" data-modalcontent="#moreInjuryDetailsModal"><div class="icon icon-search"></div> view</button>&nbsp;
                    <button class="center red showZencareModal" data-modalcontent="#stopforlitigationModal"><div class="icon icon-ban"></div> stop</button>
                </div>    
            </div>
            
        </div>
    </div>

    <div class="itemGrid__item thirds short">
        <div class="zen-actionPanel transparent">
            <!-- TODO: should add a state for resolved claims -->
            <div class="panelTitle full"><div class="icon inline icon-history"></div> time in progress</div>
            <div class="panelElement">
                <div class="timeInProgress">
                    <div class="progressMeter status{{calculateProgressChart($injury)}}" style="width:125px;height:125px;background-color:#104f8d;">
                        <svg viewBox="0 0 100 100">
                            <circle class="pm" r="50%" cx="50%" cy="50%"></circle>
                            <div class="overlay"><span class=largeNumber>{{ $injury_duration }}d</span></div>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="itemGrid__item thirds short">
        <div class="zen-actionPanel transparent">
            <div class="panelTitle full"><div class="icon inline icon-commenting-o"></div> feedback rate</div>
            <div class="panelElement">
                <div class="feedbackRate">
                    <!-- barFG classes = user|base|team-->
                    <div class="barBG"><div class="barFG {{$post_chart_array['setting']}}"><div class="rate">{{$post_chart_array['diary_count']}}/{{$post_chart_array['chat_count']}}</div><div class="total"><span>{{$post_chart_array['total_posts']}}</span> messages total</div></div></div>
                </div>
            </div>
        </div>
    </div>

    <div class="itemGrid__item thirds short">
        <div class="zen-actionPanel transparent">
            <div class="panelTitle full"><div class="icon inline icon-agent"></div> injury adjuster</div>
            <div class="panelElement">
                <table class="displayData tight">
                    <tr><td>adjuster name</td><td><span id="adjustername" class="injuryData truncate adjuster">{{$injury->adjuster_name ?: 'none entered'}}</span></td></tr>
                    <tr><td>adjuster email</td><td><span id="adjusteremail" class="injuryData truncate email"><a href="mailto:{{$injury->adjuster_email}}?subject=contact%20from%20zenjuries">{{$injury->adjuster_email ?: 'none entered'}}</a></span></td></tr>
                    <tr><td>adjuster phone</td><td><span id="adjusterphone" class="injuryData truncate phonenumber"><a href="tel:{{$injury->adjuster_phone}}">{{$injury->adjuster_phone ?: 'none entered'}}</a></span></td></tr>
                </table>
                <br><br>
                <div class="buttonArray">
                    <button class="center cyan showZencareModal" data-modalcontent="#adjusterModal"><div class="icon icon-agent"></div> edit adjuster</button>
                </div> 


                <div class="trendGraph" style="display:none;">
                    @if($recent_mood_array[0] != '-?-')
                    <!-- barFG classes = bg-mood-1 -> bg-mood-10 -->
                    
                    <div id="moodChart" class="chart" style="">
                        <div class="barBG"><div class="barFG a1 bg-mood-{{$recent_mood_array[0]}}"></div></div>
                        <div class="barBG"><div class="barFG a2 bg-mood-{{$recent_mood_array[1]}}"></div></div>
                        <div class="barBG"><div class="barFG a3 bg-mood-{{$recent_mood_array[2]}}"></div></div>
                        <div class="barBG"><div class="barFG a4 bg-mood-{{$recent_mood_array[3]}}"></div></div>
                        <div class="barBG"><div class="barFG a5 bg-mood-{{$recent_mood_array[4]}}"></div></div>
                    </div>
                    @else
                    <div id="moodChartDefault" class="placeholderImage actionChart" style="background-image:url('/images/icons/barchart-trans.png');"><span style="position:relative;top:50px;">No Mood Data... yet.</span></div>
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    //var state = {{$state}};
    $(document).ready(function(){
        @if($injury->resolved === 1)
        var state = "Resolved";
        @else
        var state = <?php echo json_encode($employeeStatus); ?>;
        @endif
        displayProgressBar(state);
    });
    function displayProgressBar(state){
        var bar = $('.statusProgressionMeter').find('.bar');
        var hospital = $('#stateHospital');
        var urgentCare = $('#stateUrgentCare');
        var doctor = $('#stateDoctor');
        var home = $('#stateHome');
        var lightDuty = $('#stateLightDuty');
        var fullDuty = $('#stateFullDuty');
        var recovered = $('#stateRecovered');

        //remove all the on classes for rebuilding
        hospital.removeClass('on');
        urgentCare.removeClass('on');
        doctor.removeClass('on');
        home.removeClass('on');
        lightDuty.removeClass('on');
        fullDuty.removeClass('on');
        recovered.removeClass('on');
        $('#statusTerminated').hide();
        $('#statusResigned').hide();
        $('#statusDeath').hide();
        $('#statusResolved').hide();

        switch(state){
            case "Hospital":
                bar.css('width', '0%');
                hospital.addClass('on');
                break;
            case "Urgent Care":
                bar.css('width', '15%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                break;
            case "Family Practice":
                bar.css('width', '30%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                break;
            case "Home":
                bar.css('width', '45%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                break;
            case "Light Duty":
                bar.css('width', '60%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                lightDuty.addClass('on');
                break;
            case "Full Duty":
                bar.css('width', '75%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                lightDuty.addClass('on');
                fullDuty.addClass('on');
                break;
            case "Maximum Medical Improvement":
                bar.css('width', '85%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                lightDuty.addClass('on');
                fullDuty.addClass('on');
                recovered.addClass('on');
                break;
            case "Terminated":
                $('#statusTerminated').show();
                bar.css('width', '85%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                lightDuty.addClass('on');
                fullDuty.addClass('on');
                recovered.addClass('on');
                break;
            case "Resigned":
                $('#statusResigned').show();
                bar.css('width', '85%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                lightDuty.addClass('on');
                fullDuty.addClass('on');
                recovered.addClass('on');
                break;
            case "Death":
                $('#statusDeath').show();
                bar.css('width', '85%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                lightDuty.addClass('on');
                fullDuty.addClass('on');
                recovered.addClass('on');
                break;
            case "Resolved":
                console.log(state);
                $('#statusResolved').show();
                bar.css('width', '85%');
                hospital.addClass('on');
                urgentCare.addClass('on');
                doctor.addClass('on');
                home.addClass('on');
                lightDuty.addClass('on');
                fullDuty.addClass('on');
                recovered.addClass('on');
                break;

        }
    }
</script>

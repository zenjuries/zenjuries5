<?php
 if(session('company_id')){
    $company_id = session('company_id');
}else{
    //EDITED - TYLER
    $company_id = Auth::user()->getCompany()->id;
}
$brandID = env('BRAND_NAME');
?>
<div class="simpleGrid">
    <div class="zen-actionPanel">
        <div class="teamStats">
            <div class="teamTitle"><span id="currentTeamTitle">{{$injured_employee->getFirstName()}}'s team</span></div>
            <div class="buttonArray" style="position:relative;top:40px;z-index:999;">
                <button class="black centered" onclick="location.href='/zenisphere';"><div class="icon icon-cog"></div> edit team</button>
            </div>
            <!-- mission and effectiveness both have classes lv1- lv7 as a range. -->
            <div class="mission"><span>mission status:</span><div class="missionBar lv7"><div class="barBGContainer"><div class="barBG"></div></div><div class="barOverlay"></div></div></div>
            <div class="effectiveness"><span>effectiveness:</span><div class="effectiveBar lv7"><div class="barBGContainer"><div class="barBG"></div></div><div class="barOverlay"></div></div></div>
        </div>
        <br>
        <div class="rosterBox">
            <div class="teamRosterContainer">
                <div class="teamRoster activeTeam">
                <div class="fullTeamRoster" id="fullTeamLayout">
					<div class="teamRosterBG"><div class="circleOfCare">
									<!-- avatar image should be filled in here.  I have color set as well, because Im using raw icons. color setting should not be necessary --
									<div class="avatarContainer"><div class="userAvatar" style="background-color:#4444ff;background-image:url('/images/avatar_icons/avatar29.png');"></div></div>
									<div class="firstName">Joanna</div>
									<div class="lastName">Perkins</div>
									-->
						<div class="teamManagementControls">
							<button id="saveEditedTeam"><div class="icon icon-floppy-o"></div> save</button><br>
						</div>
					</div></div>

					<div class="teamRosterLeft">
						<div class="teamElementsLeft">

							<div class="teamElement">
								<div class="element tp1">
									<div class="teamSetPanel tp1 showUserCard">
										<div id="chief_executive" class="teamSetContent">
											<div class="icon teamRoleTypeIcon icon-exec"></div>
											<button class="twoline roleRequired showZencareModal" data-modalcontent="" style="display:none;"><div class="icon inline icon-plus"></div> chief<br>executive</button>
											<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole required">chief executive</span></div>
											<div class="teamMemberIcon teamDelay1"><div class="userAvatar" style="background-color:#ff77ff;background-image:url('/images/avatar_icons/avatar11.png');"></div></div>
										</div>
									</div>							
								</div>
							</div>

										<div class="teamElement">
											<div class="element tp2">
												<div class="teamSetPanel tp2 showUserCard">
													<div id="safety_manager" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-exclamation-triangle"></div>
														<button class="twoline roleSuggested showZencareModal" data-modalcontent="" style="display:none;"><div class="icon inline icon-plus"></div> safety<br>manager</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole suggested">safety manager</span></div>
														<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:#347766;background-image:url('/images/avatar_icons/avatar6.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp3">
												<div class="teamSetPanel tp3 showUserCard">
													<div id="supervisor" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-hardhat"></div>
														<button class="twoline roleSuggested showZencareModal" data-modalcontent="" style="display:none;"><div class="icon inline icon-plus"></div> employee<br>supervisor</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole suggested">employee supervisor</span></div>
														<div class="teamMemberIcon teamDelay3"><div class="userAvatar" style="background-color:#77eeff;background-image:url('/images/avatar_icons/avatar2.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp4">
												<div class="teamSetPanel tp4 showUserCard">
													<div id="physical_therapist" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-rehab"></div>
														<button class="twoline roleOptional showZencareModal" data-modalcontent="" style="display:none;"><div class="icon inline icon-plus"></div> physical<br>therapist</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole optional">physical therapist</span></div>
														<div class="teamMemberIcon teamDelay2"><div class="userAvatar" style="background-color:#77ffff;background-image:url('/images/avatar_icons/avatar9.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp5">
												<div class="teamSetPanel tp5 showUserCard">
													<div id="doctor" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-doctor"></div>
														<button class="twoline roleOptional showZencareModal" data-modalcontent="" style="display:none;"><div class="icon inline icon-plus"></div> medical<br>support</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole optional">medical support</span></div>
														<div class="teamMemberIcon teamDelay5"><div class="userAvatar" style="background-color:#1177ff;background-image:url('/images/avatar_icons/avatar18.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>

								<div class="teamRosterRight">
									<div class="teamElementsRight">

										<div class="teamElement">
											<div class="element tp6">
												<div class="teamSetPanel tp6 showUserCard">
													<div id="chief_navigator" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-reuser"></div>
														<button class="twoline roleRequired showZencareModal" data-modalcontent="" style="display:none;"><div class="icon inline icon-plus"></div> human<br>resources</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole required">human resources</span></div>
														<div class="teamMemberIcon teamDelay10"><div class="userAvatar" style="background-color:#dd7733;background-image:url('/images/avatar_icons/avatar28.png');"></div></div>
													</div>
												</div>							
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp7">
												<div class="teamSetPanel tp7 showUserCard">
													<div id="claim_manager" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-injuryreport"></div>
														<button class="twoline roleSuggested showZencareModal" data-modalcontent="" style="display:none;"><div class="icon inline icon-plus"></div> claim<br>manager</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole suggested">claim manager</span></div>
														<div class="teamMemberIcon teamDelay9"><div class="userAvatar" style="background-color:#117744;background-image:url('/images/avatar_icons/avatar15.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp8">
												<div class="teamSetPanel tp8 showUserCard">
													<div id="work_comp_agent" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-insurance"></div>
														<button class="twoline roleSuggested showZencareModal" data-modalcontent="" style="display:none;"><div class="icon inline icon-plus"></div> work comp<br>agent</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole suggested">work comp agent</span></div>
														<div class="teamMemberIcon teamDelay8"><div class="userAvatar" style="background-color:#cc77ff;background-image:url('/images/avatar_icons/avatar22.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp9">
												<div class="teamSetPanel tp9 showUserCard">
													<div id="customer_service_agent" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-support"></div>
														<button class="twoline roleSuggested showZencareModal" data-modalcontent="" style="display:none;"><div class="icon inline icon-plus"></div> service<br>agent</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole suggested">service agent</span></div>
														<div class="teamMemberIcon teamDelay7"><div class="userAvatar" style="background-color:#11ccff;background-image:url('/images/avatar_icons/avatar16.png');"></div></div>
													</div>
												</div>
											</div>
										</div>

										<div class="teamElement">
											<div class="element tp10">
												<div class="teamSetPanel tp10 showUserCard">
													<div id="other" class="teamSetContent">
														<div class="icon teamRoleTypeIcon icon-adduser"></div>
														<button class="twoline roleOptional showZencareModal" data-modalcontent="" style="display:none;"><div class="icon inline icon-plus"></div> other<br>users</button>
														<div class="teamUser"><span class="teamUserName" data-id="">not set</span><span class="teamUserRole optional">other user(s)</span></div>
														<div class="teamMemberIcon teamDelay6"><div class="userAvatar" style="background-color:#5511ff;background-image:url('/images/avatar_icons/avatar8.png');"></div></div>
													</div>
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>
                </div>
            </div>
        </div>       
    </div>
</div>

<script>

    var squad_id = {{$squad->id}};
    //squad will be for the active team apge
    var squad = {
        id: null,//we can check this to determine if the team has been saved yet or not
        name: null,
        injury_team: 0,
        chief_executive: [],
        chief_navigator: [],
        claim_manager: [],
        work_comp_agent: [],
        customer_service_agent: [],
        safety_manager: [],
        supervisor: [],
        doctor: [],
        physical_therapist: [],
        other: [],
        edited: false, //indicates whether the team has been edited since being loaded (to determine which buttons to show)
        avatar_location: null, //location of the team's current avatar
        avatar_color: null, //the outline color
        background_color: null, //the image's background color
        icon_color: null, //the image's icon color
        avatar_number: null //the image's icon number
    };

    //this will hold an object of each user for the cards;
    var user_card_array = [];

    var squad_array = [];

    var userlist_current_page = 1;
    var userlist_page_total = null;
    var userlist_number_per_page = 7;

    $(document).ready(function(){
        // PAGE SETUP
        loadRoster(true);
    });

    function loadRoster(load_squad){
        var _token = '<?php echo csrf_token(); ?>';
        $.ajax({
            type: 'GET',
            url: '<?php echo route('getSquadRoster') ?>',
            data: {
                companyID: {{$company_id}},
                _token: _token
            },
            success: function (data) {
                console.log(data);
                generateRosterArray(data, load_squad);
            },
            error: function (data) {
                //alert('error');
            }
        });
    }

    function generateRosterArray(employees, load_squad){
        roster_array = employees;
        // WE WANT TO SORT THE ARRAY ALPHABETICALLY
        Array.prototype.sortOn = function(key){
            this.sort(function(a, b){
                if(a[key] < b[key]){
                    return -1;
                }else if(a[key] > b[key]){
                    return 1;
                }
                return 0;
            });
        }

        roster_array.sortOn("name");
        var user_count = roster_array.length;
        var page_number = Math.ceil(user_count / 7);
        //console.log("page number = " + page_number);
        userlist_page_total = page_number;
        //add the users to the roster
        writeRosterList();
        if(load_squad){
            loadSquadToStart();
        }else{
            //showButtons();
        }
    }

    function checkForInjury(){
        var injury = '<?php echo (isset($injury) ? $injury : "false")?>';
        if(injury !== "false"){
            var injury_squad_id = '<?php echo (isset($injury_squad_id) ? $injury_squad_id : "false"); ?>';
            if(injury_squad_id !== "false"){
                loadSquad(injury_squad_id, 'active');
                return true;
            }
        }
        return false;
    }

    function loadSquadToStart(){
        loadSquad(squad_id, 'active');
        /*
        var injury = checkForInjury();
        if(injury === false){
            var squad_id = '<?php echo (isset($load_squad) ? $load_squad : "false")?>';
            if(squad_id !== "false"){
                loadSquad(squad_id, 'active');
            }
        }
        */
    }

     //get the squad_members.user_id for each member of the currently selected squad
     function loadSquad(squad_id, squad_type) {
        console.log('load squad');
        console.log(squad_id);
        var _token = '<?php echo csrf_token(); ?>';
        $.ajax({
            type: 'GET',
            url: '<?php echo route('loadSquad'); ?>',
            data: {
                squadID: squad_id,
                _token: _token
            },
            success: function (data) {
                console.log(data);
                resetSquadCircle();
                setSquadMembers(data);
            }
        });
    }

    function writeRosterList(){
        //THIS WILL WRITE THE ROSTER ARRAY TO THE ROSTER
        var list_html = "";

        var modal_list_html = "";
        //we just want to write the first group of users
        var i = 0;
        userlist_page_total = Math.ceil(roster_array.length / userlist_number_per_page);
        //7 users per page
        var page_length = roster_array.length;
        //if(page_length > roster_array.length){
        //    page_length = roster_array.length;
        //}
        for (var l = 1; l < userlist_current_page; l++){
            i = i + userlist_number_per_page;
            page_length = page_length + userlist_number_per_page;
            if(page_length > roster_array.length){
                page_length = roster_array.length;
            }
        }

        for(i; i < page_length; i++){

            modal_list_html += '<div class="itemGrid__item userContainer addToRoster" data-index="' + i + '">' + 
					                '<div class="userListUser">' +
						                '<div class="userAvatar" data-id="'+ roster_array[i].id +'" style="background-image:url(\'' + roster_array[i].picture_location + '\');background-color:blue;"></div>' +
						                '<div class="userFirstName">' +  roster_array[i].name +  '</div>' +
						                '<div class="userLastName">' + roster_array[i].email + '</div>'+
                                    '</div>' +
				                    '<div class="veterancy"><span class="onActiveTeams">2</span> | <span class="onTotalTeams">30</span></div>'+
                              '</div>';
            

        }
        document.getElementById("addUserGrid").innerHTML = modal_list_html;

        //console.log(list_html);
        
        $('#userInfoDefaultDiv').show();
        $('#userInfoPanelModal').hide();
    }

    function setSquadMembers(member_array){
        //setting the defualt role objects
        var chief_executive = {id: 'chief_executive', name: 'chief_executive', icon: 'icon-exec', title: 'Chief Executive', role: 1, roleType: 'required', displayName: 'chief executive'};
        var safety_manager = {id: 'safety_manager', name: 'safety_manager', icon: 'icon-exclamation-triangle', title: 'Safety Manager', role: 2, roleType: 'suggested', displayName: 'safety manager'};
        var supervisor = {id: 'supervisor', name: 'supervisor', icon: 'icon-hardhat', title: 'Employee Supervisor', role: 3, roleType: 'suggested', displayName: 'employee supervisor'};
        var physical_therapist = {id: 'physical_therapist', name: 'physical_therapist', icon: 'icon-rehab', title: 'Physical Therapist', role: 4, roleType: 'optional', displayName: 'physical therapist'};
        var doctor = {id: 'doctor', name: 'doctor', icon: 'icon-doctor', title: 'Medical Support', role: 5, roleType: 'optional', displayName: 'medical support'};
        var chief_navigator = {id: 'chief_navigator', name: 'chief_navigator', icon: 'icon-reuser', title: 'Human Resources', role: 6, roleType: 'required', displayName: 'human resources'};
        var claim_manager = {id: 'claim_manager', name: 'claim_manager', icon: 'icon-injuryreport', title: 'Claim Manager', role: 7, roleType: 'suggested', displayName: 'claim manager'};
        var work_comp_agent = {id: 'work_comp_agent', name: 'work_comp_agent', icon: 'icon-insurance', title: 'Work Comp Agent', role: 8, roleType: 'suggested', displayName: 'work comp agent'};
        var customer_service_agent = {id: 'customer_service_agent', name: 'customer_service_agent', icon: 'icon-support', title: 'Service Agent', role: 9, roleType: 'suggested', displayName: 'service agent'};
        var other = {id: 'other', name: 'other', icon: 'icon-adduser', title: 'Other User', role: 10, roleType: 'optional', displayName: 'other users'};
        resetSquadObject();
        console.log(JSON.stringify(squad));
        console.log('member_array');
        console.log(member_array);
        ////console.log(squad);
        var l = member_array.length;
        if(l) {
            squad.name = member_array[0].squad_name;
            squad.avatar_location = member_array[0].avatar_location;
            squad.avatar_color = member_array[0].avatar_color;
            squad.background_color = member_array[0].background_color;
            squad.id = member_array[0].squad_id;
            squad.injury_team = member_array[0].injury_squad;
            for (var i = 0; i < l; i++) {
                var position_id = member_array[i].position_id;
                var user_id = member_array[i].user_id;
                var user_index = getUserIndexById(user_id);
                if(user_index !== false){
                    switch (position_id) {
                        case 1:
                            squad.chief_executive[squad.chief_executive.length] = user_id;
                            console.log('sending chief');
                            addUserToCircle(chief_executive);
                            break;
                        case 2:
                            squad.doctor[squad.doctor.length] = user_id;
                            console.log('sending doctor');
                            addUserToCircle(doctor);
                            break;
                        case 3:
                            squad.physical_therapist[squad.physical_therapist.length] = user_id;
                            console.log('sending pt');
                            addUserToCircle(physical_therapist);
                            break;
                        case 4:
                            squad.claim_manager[squad.claim_manager.length] = user_id;
                            console.log('sending claim manager');
                            addUserToCircle(claim_manager);
                            break;
                        case 5:
                            squad.work_comp_agent[squad.work_comp_agent.length] = user_id;
                            console.log('sending comp agent');
                            addUserToCircle(work_comp_agent);
                            break;
                        case 6:
                            squad.customer_service_agent[squad.customer_service_agent.length] = user_id;
                            console.log('sending service agent');
                            addUserToCircle(customer_service_agent);
                            break;
                        case 7:
                            squad.safety_manager[squad.safety_manager.length] = user_id;
                            console.log('sending safety manager');
                            addUserToCircle(safety_manager);
                            break;
                        case 8:
                            squad.supervisor[squad.supervisor.length] = user_id;
                            console.log('sending supervisor');
                            addUserToCircle(supervisor);
                            break;
                        case 9:
                            squad.chief_navigator[squad.chief_navigator.length] = user_id;
                            console.log('sending hr');
                            addUserToCircle(chief_navigator);
                            break;
                        case 10:
                            squad.other[squad.other.length] = user_id;
                            console.log('sending other');
                            addUserToCircle(other);
                    }
                }
                $('#currentTeamTitle').html(squad.name);
            }
        }
        
        //console.log(JSON.stringify(squad));
    }

    function resetSquadCircle(){
        //setting the defualt role objects
        var chief_executive = {id: 'chief_executive', icon: 'icon-exec', title: 'Chief Executive', role: 1, roleType: 'roleRequired', displayName: 'chief<br>executive'};
        var safety_manager = {id: 'safety_manager', icon: 'icon-exclamation-triangle', title: 'Safety Manager', role: 2, roleType: 'roleSuggested', displayName: 'safety<br>manager'};
        var supervisor = {id: 'supervisor', icon: 'icon-hardhat', title: 'Employee Supervisor', role: 3, roleType: 'roleSuggested', displayName: 'employee<br>supervisor'};
        var physical_therapist = {id: 'physical_therapist', icon: 'icon-rehab', title: 'Physical Therapist', role: 4, roleType: 'roleOptional', displayName: 'physical<br>therapist'};
        var doctor = {id: 'doctor', icon: 'icon-doctor', title: 'Medical Support', role: 5, roleType: 'roleOptional', displayName: 'medical<br>support'};
        var chief_navigator = {id: 'chief_navigator', icon: 'icon-reuser', title: 'Human Resources', role: 6, roleType: 'roleRequired', displayName: 'human<br>resources'};
        var claim_manager = {id: 'claim_manager', icon: 'icon-injuryreport', title: 'Claim Manager', role: 7, roleType: 'roleSuggested', displayName: 'claim<br>manager'};
        var work_comp_agent = {id: 'work_comp_agent', icon: 'icon-insurance', title: 'Work Comp Agent', role: 8, roleType: 'roleSuggested', displayName: 'work comp<br>agent'};
        var customer_service_agent = {id: 'customer_service_agent', icon: 'icon-support', title: 'Service Agent', role: 9, roleType: 'roleSuggested', displayName: 'service<br>agent'};
        var other = {id: 'other', icon: 'icon-adduser', title: 'Other User', role: 10, roleType: 'roleOptional', displayName: 'other<br>users'};

        //putting all the default role objs in an array to loop through
        var roles_array = [chief_executive, safety_manager, supervisor, physical_therapist, doctor, chief_navigator, claim_manager, work_comp_agent, customer_service_agent, other];
        for(var i = 0; i < roles_array.length; i++){
            console.log('reset Circle: ' + roles_array[i].title);
            document.getElementById(roles_array[i].id).innerHTML = '<div class="icon teamRoleTypeIcon '+roles_array[i].icon+'"></div>' +
                        '<div class="addTeamUser"><button data-title="'+roles_array[i].title+'" data-modalcontent="#add_user_to_team" data-roleType="'+roles_array[i].roleType+'" data-role="'+roles_array[i].role+'" class="twoline '+roles_array[i].roleType+' showAddUserToTeam showZencareModal"><div class="icon inline icon-plus"></div> '+roles_array[i].displayName+'</button></div>' +
						'<div class="teamMemberIcon templateDelay'+roles_array[i].role+'"><div class="helpAvatar" style="background-color:var(--'+roles_array[i].roleType+');background-image:url(\'/images/icons/question2.png\');"></div></div>';
                        $('#' + roles_array[i].id).removeClass('showUserCard showZencareModal');
                        $('#' + roles_array[i].id).removeAttr('data-modalcontent');
                        $('#' + roles_array[i].id).parent().removeClass('multiUser');
        }
    }

    $('body').on('click', '.addToRoster', function(){
        var role = $('#modalTitle').text();
        var user_index = $(this).data('index');
        addUserToSquad(user_index, role);
    });

    function addUserToSquad(user_index, role){
        //setting the defualt role objects
        var chief_executive = {id: 'chief_executive', name: 'chief_executive', icon: 'icon-exec', title: 'Chief Executive', role: 1, roleType: 'required', displayName: 'chief executive'};
        var safety_manager = {id: 'safety_manager', name: 'safety_manager', icon: 'icon-exclamation-triangle', title: 'Safety Manager', role: 2, roleType: 'suggested', displayName: 'safety manager'};
        var supervisor = {id: 'supervisor', name: 'supervisor', icon: 'icon-hardhat', title: 'Employee Supervisor', role: 3, roleType: 'suggested', displayName: 'employee supervisor'};
        var physical_therapist = {id: 'physical_therapist', name: 'physical_therapist', icon: 'icon-rehab', title: 'Physical Therapist', role: 4, roleType: 'optional', displayName: 'physical therapist'};
        var doctor = {id: 'doctor', name: 'doctor', icon: 'icon-doctor', title: 'Medical Support', role: 5, roleType: 'optional', displayName: 'medical support'};
        var chief_navigator = {id: 'chief_navigator', name: 'chief_navigator', icon: 'icon-reuser', title: 'Human Resources', role: 6, roleType: 'required', displayName: 'human resources'};
        var claim_manager = {id: 'claim_manager', name: 'claim_manager', icon: 'icon-injuryreport', title: 'Claim Manager', role: 7, roleType: 'suggested', displayName: 'claim manager'};
        var work_comp_agent = {id: 'work_comp_agent', name: 'work_comp_agent', icon: 'icon-insurance', title: 'Work Comp Agent', role: 8, roleType: 'suggested', displayName: 'work comp agent'};
        var customer_service_agent = {id: 'customer_service_agent', name: 'customer_service_agent', icon: 'icon-support', title: 'Service Agent', role: 9, roleType: 'suggested', displayName: 'service agent'};
        var other = {id: 'other', name: 'other', icon: 'icon-adduser', title: 'Other User', role: 10, roleType: 'optional', displayName: 'other users'};
        //get the role
        //var role = $('#modalTitle').text();
        role = role.toLowerCase();
        role = role.replace(/ /g,"_");
        //get user index
        //var user_index = $(this).data('index');
        //var user = getUserIndexById(user_index);
        var user = roster_array[user_index];
            //adjusting for naming diffs
            if(role === 'other_users'){
                role = 'other';
            }else if(role === "employee_supervisor"){
                role = 'supervisor';
            }else if(role === "medical_support"){
                role = 'doctor';
            }else if(role === "service_agent"){
                role = 'customer_service_agent';
            }else if(role === 'human_resources'){
                role = 'chief_navigator';
            }
            squad[role].push(user.id);
            squad.edited = true;
            console.log(JSON.stringify(squad));
            switch(role){
            case 'physical_therapist':
                addUserToCircle(physical_therapist);
                break;
            case 'doctor':
                addUserToCircle(doctor);
                break;
            case 'chief_executive':
                addUserToCircle(chief_executive);
                break;
            case 'claim_manager':
                addUserToCircle(claim_manager);
                break;
            case 'work_comp_agent':
                addUserToCircle(work_comp_agent);
                break;
            case 'customer_service_agent':
                addUserToCircle(customer_service_agent);
                break;
            case 'safety_manager':
                addUserToCircle(safety_manager);
                break;
            case 'supervisor':
                addUserToCircle(supervisor);
                break;
            case 'chief_navigator':
                addUserToCircle(chief_navigator);
                break;
            case 'other':
                addUserToCircle(other);      
        }

        modal.close();
    }

    //add the user's avatar to the squad circle
    function addUserToCircle(role, template = false){
        console.log(role);
        var userArray = [];
        var squad_role = role.name;
        //account for 

        console.log('template is false');
        var use_squad = squad;
        /*
        console.log('squad role: ' + squad_role);
        console.log('template_squad: ' + JSON.stringify(template_squad));
        console.log('role: ' + JSON.stringify(role));
        console.log('role id: ' + use_squad[squad_role]);
        */
        if(use_squad[squad_role].length > 1){
            for(var i = 0; i < use_squad[squad_role].length; i++){
                var index = getUserIndexById(use_squad[squad_role][i]);
                userArray.push(index);
            }
        }
        //console.log('user_array: ' + userArray);
        //console.log('use_squad: ' + JSON.stringify(use_squad));
        if(use_squad[squad_role].length === 1){
            var roster_index = getUserIndexById(use_squad[squad_role][0]);
            console.log('role.id: ' + role.id);
            document.getElementById(role.id).innerHTML = '<div class="icon teamRoleTypeIcon '+role.icon+'"></div>' +
            '<button class="twoline '+role.roleType+' showZencareModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> '+role.displayName+'</button>' +
            '<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole '+role.roleType+'">'+role.displayName+'</span></div>' +
            '<div class="teamMemberIcon teamDelay'+role.role+'"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
        }else{
            console.log('role.id: ' + role.id);
            document.getElementById(role.id).innerHTML = '<div class="icon teamRoleTypeIcon '+role.icon+'"></div>' +
            '<button class="twoline '+role.roleType+' showZencareModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div>  '+role.displayName+'</button>' +
            '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole '+role.roleType+'"> '+role.displayName+'</span></div>' +
            '<div class="teamMemberIcon teamDelay'+role.role+'"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/icons/zenjuries.png\');"></div></div>';
            $("#"+role.id).parent().addClass('multiUser');   
        }
        $("#"+role.id).addClass('showZencareModal showUserCard');
        $('#'+role.id).attr('data-modalcontent', '#user_card');
    }

    //return the roster_array index for a user_id
    function getUserIndexById(id){
        var user_index = false;
        // alert(roster_array.length);
        for(var i = 0; i < roster_array.length; i++){
            if(roster_array[i].id == id){
                user_index = i;
                break;
            }
        }
        return user_index;
    }

    //clears the squad object to a blank slate (new team)
    function resetSquadObject(){
        squad.id = null;
        squad.name = null;
        squad.injury_team = 0;
        squad.chief_executive = [];
        squad.chief_navigator = [];
        squad.claim_manager = [];
        squad.work_comp_agent = [];
        squad.customer_service_agent = [];
        squad.safety_manager = [];
        squad.supervisor = [];
        squad.doctor = [];
        squad.physical_therapist = [];
        squad.other = [];
        squad.edited = false;
        squad.avatar_location = null;
        squad.avatar_color = null;
        squad.background_color = null;
        squad.icon_color = null;
        squad.avatar_number = null;

    }

    //displays the correct title and color for adding a user to the team
    $('body').on('click', '.showAddUserToTeam', function(){
        console.log('add user clicked');
        //get the data attributes
        var title = $(this).data('title');
        var roleType = $(this).data('roletype');
        var role = $(this).data('role');
        //set the title
        $('#modalTitle').html(title);

        //remove all role types
        $('#modalTitle').parent('span').removeClass('roleRequired');
        $('#modalTitle').parent('span').removeClass('roleSuggested');
        $('#modalTitle').parent('span').removeClass('roleOptional');

        //add role type of button clicked
        $('#modalTitle').parent('span').addClass(roleType);

        //save role to hidden input
        $('#modalTitle').attr('role', role);
        
    });

    //adding the user info to the userCardModal
    $('body').on('click', '.showUserCard', function(){
        var use_squad = squad;
        if($(this).hasClass('teamSetPanel')){
            //if showUserCard also has teamSetPanel that means its from the active teams pannel
            user_role = $(this).children('div').attr('id');
            if(user_role != undefined){
                console.log("user role: " + user_role);
                getUsersForUserCard(user_role, use_squad);
            }
        }else{
            //else means its the team templates pannel
            console.log('from team template pannel');
            var user_role = $(this).attr('id');
            console.log("user role: " + user_role);
            //make sure role is set before showing the card
            if(user_role != undefined){
                console.log('using getUsersForUserCard from template');
                user_role = user_role.substring(9);
                //send new squad object instead of squad
                getUsersForUserCard(user_role, use_squad);
            }else{
                console.log('using showUserCardForManagment');
                var roster_index = $(this).data('index');
                getUsersForUserCard("", "", roster_index);
            }
        }
    });
    
    function getUsersForUserCard(user_role, selected_squad, roster_index = ""){
        //var user_role = user_role.replace("template_", "");
        //reset the array
        user_card_array = [];
        console.log('getting user info');
        //fixed different names between both pannels
        if(user_role === 'other_users'){
            user_role = 'other';
        }else if(user_role === "employee_supervisor"){
            user_role = 'supervisor';
        }else if( user_role === "medical_support"){
            user_role = 'doctor';
        }else if(user_role === "service_agent"){
            user_role = 'customer_service_agent';
        }else if(user_role === 'human_resources'){
            user_role = 'chief_navigator';
        }
        console.log('using a squad: ' + selected_squad[user_role]);
        if(selected_squad[user_role] !== undefined){
            console.log(selected_squad[user_role]);
            for(var i = 0; i < selected_squad[user_role].length; i++){
                var user_index = getUserIndexById(selected_squad[user_role][i]);
                user_card_array.push(roster_array[user_index]);
                console.log('user card array: ' + user_card_array);
            }
            buildUserCardModalHeader(user_role);
        }else if(roster_index !== ""){
            user_card_array = [];
            var user = roster_array[roster_index];
            user_card_array.push(user);
            $('#displayMultipleUsers').hide();
            buildUserCardModalHeader("", false);
            
        }
    }

    function buildUserCardModalHeader(role, showbuttons = true){
        var showbuttons = showbuttons;
        if($('#cardContainer').data('role') !== ""){
            $('#cardContainer').data('role', role);
        }else{
            $('#cardContainer').attr('data-role', role);
            $('#addNewUserToRole').attr('data-role', role);
            console.log('role for card: ' + role);
        }

        if(user_card_array.length < 1 || user_card_array.length === 1){
            $('#displayMultipleUsers').hide();
        }else{
            $('#displayMultipleUsers').show();
            $('#nextUserCard').attr('data-userindex', 1);
            $('#prevUserCard').attr('data-userindex', -1);
            //testing the prev and next button attr not changing on changing cards
            if($('#nextUserCard').data('userindex') > 1){
                $('#nextUserCard').data('userindex', 1);
            }
            if($('#prevUserCard').data('userindex') > -1){
                $('#prevUserCard').data('userindex', -1);
            }
            console.log($('#nextUserCard').data('userindex'));
            console.log($('#prevUserCard').data('userindex'));
            $('#prevUserCard').addClass('ghost');
            $('#currentUserCardShowing').text('1');
            $('#numberOfUserCards').text(user_card_array.length);
        }
        if($('#addNewUserToRole').data('modaltitle') === "" || $('#addNewUserToRole').data('modaltitle') === undefined){
            $('#addNewUserToRole').attr('data-modaltitle', role);
        }else{
            $('#addNewUserToRole').data('modaltitle', role);
        }
        showUserOnCard(0, showbuttons);
        
    }
    //TODO: finish adding users to cards and displaying them
    function showUserOnCard(index, showbuttons = true){
        console.log('showbuttons: ' + showbuttons);
        $('#userCard').attr('class', 'userCard');
        var user = user_card_array[index];
        if(showbuttons === false){
            $('#removeFromTeam').hide();
            $('#addNewUserToRole').hide();
            //console.log('hiding the remove from team button');
        }else{
            $('#removeFromTeam').show();
            $('#addNewUserToRole').show();
            //console.log('not hiding the remove from team button');
        }
        console.log(user);
        if(user.theme_id === undefined){
            user.theme_id = 1;
        }
        var user_name = user.name.split(' ');
        $('.headerBG.themeBG').css('background-image', 'url("/branding/{{ $brandID }}/themes/theme'+user.theme_id+'/dashboard.jpg")');
        $('.firstname').text(user_name[0]);
        $('.lastname').text(user_name[1]);
        $('.onTotalTeams').text(user.company_squad_list.length);

        //email
        $('.cardElement.email').html('<a href="mailto:'+user.email+'">'+user.email+'</a>');
        if(user.phone){
            $('.cardElement.phone').text(user.phone);
        }else{
            $('.cardElement.phone').text('no phone entered');
        }
        console.log('user_id: ' + user.id);
        
        //$('#removeFromTeam').show();
        $('#removeFromTeam').attr('data-userid', user.id);
        
    }

    $('body').on('click', '#addNewUserToRole', function(){
        var title = $(this).data('modaltitle');
        var roleType = 'roleSuggested';
        if(title === 'chief_executive'){
            title = 'Chief Executive';
            roleType = 'roleRequired';
        }else if(title === 'chief_navigator'){
            title = 'Human Resources';
            roleType = 'roleRequired';
        }else if(title === 'claim_manager'){
            title = 'Claim Manager';
        }else if(title === 'work_comp_agent'){
            title = 'Work Comp Agent';
        }else if(title === 'customer_service_agent'){
            title = 'Service Agent';
        }else if(title === 'safety_manager'){
            title = 'Safety Manager';
        }else if(title === 'supervisor'){
            title = 'Supervisor';
        }else if(title === 'doctor'){
            title = 'Medical Support';
            roleType = 'roleOptional';
        }else if(title === 'physical_therapist'){
            title = 'Physical Therapist';
            roleType = 'roleOptional';
        }else if(title === 'other'){
            title = 'Other';
            roleType = 'roleOptional';
        }
        console.log('modal title: ' + title);
        console.log('modal role: ' + roleType);
        $('#modalTitle').parent().removeClass();
        $('#modalTitle').parent().addClass(roleType);
        $('#modalTitle').text(title);
        console.log('modal title changed');
    });

    $('body').on('click', '#prevUserCard', function(){
        if(!$(this).hasClass('ghost')){
            var index = $(this).data('userindex');
            if(index < user_card_array.length && index >= 0){
                showUserOnCard(index);
                if(index === 0){
                    $(this).addClass('ghost');
                }
                $('#nextUserCard').removeClass('ghost');
                $(this).data('userindex', index - 1);
                $('#nextUserCard').data('userindex', index + 1);
                $('#currentUserCardShowing').text(index + 1);
            }
        }
    });

    $('body').on('click', '#nextUserCard', function(){
        console.log('next clicked');
        if(!$(this).hasClass('ghost')){
            var index = $(this).data('userindex');
            console.log("length: " + user_card_array.length)
            console.log("index: " + index);
            if(index === user_card_array.length - 1){
                $(this).addClass('ghost');
            }
            if(index < user_card_array.length ){
                /*
                console.log("length: " + user_card_array.length)
                console.log("index: " + index);
                */
                $('#prevUserCard').removeClass('ghost');
                showUserOnCard(index);
                $(this).removeClass('ghost');
                $('#prevUserCard').data('userindex', index -1);
                $(this).data('userindex', index + 1);
                $('#currentUserCardShowing').text(index + 1);
            }
        }
    });

    $('body').on('click', '#removeFromTeam', function(){
        //for whatever reason jquery was not getting the role attribute so vanilla js to the rescue
        var role = document.getElementById("cardContainer").getAttribute('data-role');
        var id = $(this).data('userid');
        var chief_executive = {id: 'chief_executive', name: 'chief_executive', icon: 'icon-exec', title: 'Chief Executive', role: 1, roleType: 'required', displayName: 'chief</br>executive'};
        var safety_manager = {id: 'safety_manager', name: 'safety_manager', icon: 'icon-exclamation-triangle', title: 'Safety Manager', role: 2, roleType: 'suggested', displayName: 'safety</br>manager'};
        var supervisor = {id: 'supervisor', name: 'supervisor', icon: 'icon-hardhat', title: 'Employee Supervisor', role: 3, roleType: 'suggested', displayName: 'employee supervisor'};
        var physical_therapist = {id: 'physical_therapist', name: 'physical_therapist', icon: 'icon-rehab', title: 'Physical Therapist', role: 4, roleType: 'optional', displayName: 'physical</br>therapist'};
        var doctor = {id: 'doctor', name: 'doctor', icon: 'icon-doctor', title: 'Medical Support', role: 5, roleType: 'optional', displayName: 'medical support'};
        var chief_navigator = {id: 'chief_navigator', name: 'chief_navigator', icon: 'icon-reuser', title: 'Human Resources', role: 6, roleType: 'required', displayName: 'human</br>resources'};
        var claim_manager = {id: 'claim_manager', name: 'claim_manager', icon: 'icon-injuryreport', title: 'Claim Manager', role: 7, roleType: 'suggested', displayName: 'claim</br>manager'};
        var work_comp_agent = {id: 'work_comp_agent', name: 'work_comp_agent', icon: 'icon-insurance', title: 'Work Comp Agent', role: 8, roleType: 'suggested', displayName: 'work comp</br>agent'};
        var customer_service_agent = {id: 'customer_service_agent', name: 'customer_service_agent', icon: 'icon-support', title: 'Service Agent', role: 9, roleType: 'suggested', displayName: 'service</br>agent'};
        var other = {id: 'other', name: 'other', icon: 'icon-adduser', title: 'Other User', role: 10, roleType: 'optional', displayName: 'other</br>users'};

        var role_array = [chief_executive, safety_manager, supervisor, physical_therapist, doctor, chief_navigator, claim_manager, work_comp_agent, customer_service_agent, other];
        for(var i = 0; i < role_array.length; i++){
            if(role_array[i].id === role){
                removeUserFromRole(role_array[i], id);
            }
        }

    });

    function removeUserFromRole(role, user_id){
        console.log('removing user from : ' + role.id);
        console.log('removing user with the id: ' + user_id);
        userArray = [];
        for(var i = 0; i < squad[role.id].length; i++){
            if(squad[role.id][i] === user_id){
                squad[role.id].splice(i, 1);
                break;
            }
        }
        squad.edited = true;
        if(squad[role.id].length > 1){
            for(var i = 0; i < squad[role.id].length; i++){
                var index = getUserIndexById(squad[role.id][i]);
                userArray.push(index);
            }
        }
        if(squad[role.id].length === 1){
            var roster_index = getUserIndexById(squad[role.id][0]);
            document.getElementById(role.id).innerHTML = '<div class="icon teamRoleTypeIcon '+role.icon+'"></div>' +
            '<button class="twoline '+role.roleType+' showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div> '+role.displayName+'</button>' +
            '<div class="teamUser"><span class="teamUserName" data-id="'+roster_index+'">'+roster_array[roster_index].name+'</span><span class="teamUserRole '+role.roleType+'">'+role.displayName+'</span></div>' +
            '<div class="teamMemberIcon teamDelay'+role.role+'"><div class="userAvatar" style="background-color:'+roster_array[roster_index].background_color+';background-image:url(\''+roster_array[roster_index].picture_location+'\');"></div></div>';
            $("#"+role.id).parent().removeClass('multiUser');
        }else if(squad[role.id].length > 1){
            document.getElementById(role.id).innerHTML = '<div class="icon teamRoleTypeIcon '+role.icon+'"></div>' +
            '<button class="twoline '+role.roleType+' showZenModal" data-modalcontent="#user_card" style="display:none;"><div class="icon inline icon-plus"></div>  '+role.displayName+'</button>' +
            '<div class="teamUser"><span class="teamUserName" data-id="'+userArray+'">Multiple Users</span><span class="teamUserRole '+role.roleType+'"> '+role.displayName+'</span></div>' +
            '<div class="teamMemberIcon teamDelay'+role.role+'"><div class="userAvatar" style="background-color:#ffffff;background-image:url(\'/images/zenjuries-logo.jpg\');"></div></div>';
            $("#"+role.id).parent().addClass('multiUser');   
        }else{
            document.getElementById(role.id).innerHTML = '<div class="icon teamRoleTypeIcon '+role.icon+'"></div>' +
                    '<div class="addTeamUser"><button data-title="'+role.title+'" data-modalcontent="#add_user_to_team" data-roleType="'+role.roleType+'" data-role="'+role.role+'" class="twoline '+role.roleType+' showAddUserToTeam showZenModal"><div class="icon inline icon-plus"></div> '+role.displayName+'</button></div>' +
					'<div class="teamMemberIcon templateDelay'+role.role+'"><div class="helpAvatar" style="background-color:var(--'+role.roleType+');background-image:url(\'/images/icons/question2.png\');"></div></div>';
                    $('#' + role.id).removeClass('showUserCard showZenModal');
                    $('#' + role.id).removeAttr('data-modalcontent');
                    $('#' + role.id).parent().removeClass('multiUser');
        }
        
        modal.close();
    }

    $('#saveEditedTeam').on('click', function(){
        overwriteCurrentSquad();
    });

    /* Overwrite an old squad instead of creating a new one */
    function overwriteCurrentSquad(){
        var _token = '<?php echo csrf_token(); ?>';
        var valid = validateSquad(true);
        if(squad.id !== null && valid == true) {
            $.ajax({
                type: 'POST',
                url: '<?php echo route('editSquad') ?>',
                data: {
                    chiefExecutiveArray: squad.chief_executive,
                    doctorArray: squad.doctor,
                    therapistArray: squad.physical_therapist,
                    claimManagerArray: squad.claim_manager,
                    agentArray: squad.work_comp_agent,
                    customerServiceArray: squad.customer_service_agent,
                    safetyCoordinatorArray: squad.safety_manager,
                    supervisorArray: squad.supervisor,
                    chiefNavigatorArray: squad.chief_navigator,
                    otherArray: squad.other,
                    squadName: squad.name,
                    squad_id: squad.id,
                    _token: _token
                },
                success: function (data) {
                    showAlert("Your team has been updated!", "confirm", 5);
                    modal.close();
                    squad.edited = false;
                    loadSquad(squad.id, 'active');
                 
                }
            });
        }
    }


    function validateSquad(show_error_messages){
        var error_message = "";
        var valid = true;
        if(squad.chief_executive[0] == null || squad.chief_executive[0] == ""){
            error_message += "The position of Chief Executive is required. You must fill this position before you can save the team.<br>";
            valid = false;
        }
        if(squad.chief_navigator[0] == null || squad.chief_navigator[0] == ""){
            error_message += "The position of Chief Navigator is required. You must fill this position before you can save the team.<br>";
            valid = false;
        }
        //document.getElementById('teamErrors').innerHTML = error_message;
        if(show_error_messages){
            ////console.log(error_message);
        }
        return valid;
    }

</script>
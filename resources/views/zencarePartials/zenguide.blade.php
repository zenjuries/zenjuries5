<div class="itemGrid" id="topParent">
    <div class="itemGrid__item half max">
        <div class="zen-actionPanel transparent">
            <div class="panelTitle clean">goals to attain</div>
            <div class="panelElement">
                <!-- progressMeter classes = timeLv1 -> timeLv7 -->
                <!-- TASKS
                    Injury Alert Delivered
                    Official First Report Delivered
                    Claim Number Added
                    Adjuster Info Added
                    Employee Email Added
                    Employee Status Added
                    Resolve Injury
                    Add Final Costs


                -->
                <table id="goalsToAttainTable">
                    @if(!empty($injury_alert->action_taken) && !empty($injury->full_injury_report_delivered) && !empty($injury->claim_number) && !empty($injury->adjuster_name) && !empty($injured_employee->email) && $injury->resolved !== 0 && !empty($injury->employee_status) && !empty($injury->final_cost) && (!empty($medical_total) || !empty($legal_total) || !empty($indemnity_total) || !empty($misc_total))) 
                    <tr class="noAction" id="goalsCompleteText">
                        <td class="icon"><div class="icon icon-check-circle FG__green"></div></td>
                        <td class="longlabel">Great job, all goals complete</td>
                    </tr>
                    @else
                        <tr class="noAction" id="goalsCompleteTextNotCompletedOnLoad" style="display:none">
                            <td class="icon"><div class="icon icon-check-circle FG__green"></div></td>
                            <td class="longlabel">Great job, all goals complete</td>
                        </tr>                   
                    @endif
                    @if(empty($injury_alert->action_taken))
                    <!-- FIRST REPORT (remove from list when complete)-->
                    <tr class="hoverAction showZencareModal" data-modalcontent="#injuryAlertConfirmModal" id="taskListInjuryAlert" >
                        <td class="icon"><div class="progressMeter timeLv7 mini">
                                <svg viewBox="0 0 100 100"><circle class="pm" r="50%" cx="50%" cy="50%"></circle></svg></div></td>
                        <td class="longlabel">Has the Injury Alert been delivered to your insurance carrier? Click here to confirm.</td>
                    </tr>
                    <tr id="spacerInjuryAlert" class="tableRowSpacer"></tr>
                    @endif
                    @if(empty($injury->full_injury_report_delivered))
                    <!-- OFFICIAL FIRST REPORT (remove from list when complete) -->
                    <tr class="hoverAction showZencareModal" data-modalcontent="#firstReportConfirmModal" id="taskListOfficialReport" >
                        <td class="icon"><div class="progressMeter timeLv7 mini">
                                <svg viewBox="0 0 100 100"><circle class="pm" r="50%" cx="50%" cy="50%"></circle></svg></div></td>
                        <td class="longlabel">Has the Official First Report of Injury been filed with your insurance carrier? Click here to confirm.</td>
                    </tr>
                    <tr id="spacerOfficialReport" class="tableRowSpacer"></tr>
                    @endif
                    @if(empty($injury->claim_number))
                    <!-- Claim number (remove from list when complete) -->
                    <tr class="hoverAction showZencareModal" data-modalcontent="#claimNumberModal" id="taskListClaimNumber" >
                        <td class="icon"><div class="progressMeter timeLv7 mini">
                                <svg viewBox="0 0 100 100"><circle class="pm" r="50%" cx="50%" cy="50%"></circle></svg></div></td>
                        <td class="longlabel">Do you have a claim number yet? Add it here.</td>
                    </tr>
                    <tr id="spacerClaimNumber" class="tableRowSpacer"></tr>
                    @endif
                    @if(empty($injury->adjuster_name))
                    <!-- Adjuster (remove from list when complete) -->
                    <tr class="hoverAction showZencareModal" data-modalcontent="#adjusterModal" id="taskListAdjuster" >
                        <td class="icon"><div class="progressMeter timeLv5 mini">
                                <svg viewBox="0 0 100 100"><circle class="pm" r="50%" cx="50%" cy="50%"></circle></svg></div></td>
                        <td class="longlabel">Has an adjuster been assigned to your claim? Add it here.</td>
                    </tr>
                    <tr id="spacerAdjuster" class="tableRowSpacer"></tr>
                    @endif
                    @if(empty($injured_employee->email))
                    <!-- Employee Email (remove from list when complete) -->
                    <tr class="hoverAction showZencareModal" data-modalcontent="#employeeInfoUpdateModal" id="taskListEmployeeEmail" >
                        <td class="icon"><div class="progressMeter timeLv3 mini">
                                <svg viewBox="0 0 100 100"><circle class="pm" r="50%" cx="50%" cy="50%"></circle></svg></div></td>
                        <td class="longlabel">Add injured employee's email.  The more information from the injured employee, the better.</td>
                    </tr>
                    <tr id="spacerEmployeeEmail" class="tableRowSpacer"></tr>
                    @endif
                    @if(empty($medical_total) && empty($legal_total) && empty($indemnity_total) && empty($misc_total))
                    <!-- Update Claim Costs (remove from list when complete, add to ongoing) -->
                    <tr class="hoverAction showZencareModal" data-modalcontent="#updateCostsModal" id="taskListClaimCosts" >
                        <td class="icon"><div class="progressMeter timeLv1 mini">
                                <svg viewBox="0 0 100 100"><circle class="pm" r="50%" cx="50%" cy="50%"></circle></svg></div></td>
                        <td class="longlabel">Every time there is a change, update the claim costs!</td>
                    </tr>
                    <tr id="spacerClaimCosts" class="tableRowSpacer"></tr>
                    @endif
                    @if(empty($injury->employee_status))
                    <!-- Employee Status (remove from list when complete, add to ongoing) -->
                    <tr class="hoverAction showZencareModal" data-modalcontent="#updateStatusModal" id="taskListEmployeeStatus" >
                        <td class="icon"><div class="progressMeter timeLv1 mini">
                                <svg viewBox="0 0 100 100"><circle class="pm" r="50%" cx="50%" cy="50%"></circle></svg></div></td>
                        <td class="longlabel">Has the employee's status changed? Be sure to update it when it does!</td>
                    </tr>
                    <tr id="spacerEmployeeStatus" class="tableRowSpacer"></tr>
                    @endif
                    @if($injury->resolved === 0)
                    <!-- Resolve Injury (remove from list when complete) -->
                    <tr class="hoverAction showZencareModal" data-modalcontent="#resolveInjuryModal" id="taskListResolve" >
                        <td class="icon"><div class="progressMeter timeLv1 mini">
                                <svg viewBox="0 0 100 100"><circle class="pm" r="50%" cx="50%" cy="50%"></circle></svg></div></td>
                        <td class="longlabel">Ready to resolve the injury?</td>
                    </tr>
                    <tr id="spacerInjuryResolved" class="tableRowSpacer"></tr>
                    @endif
                    @if(empty($injury->final_cost))
                    <!-- Final Costs (remove from list when complete) -->
                    <tr class="hoverAction showZencareModal" data-modalcontent="#addFinalCostModal" id="taskListFinalCost" >
                        <td class="icon"><div class="progressMeter timeLv1 mini">
                                <svg viewBox="0 0 100 100"><circle class="pm" r="50%" cx="50%" cy="50%"></circle></svg></div></td>
                        <td class="longlabel">Add the Final Costs for the resolved injury here.</td>
                    </tr>
                    @endif
                </table>
            </div>
            <div style="height:30px;"></div>
            <div class="panelTitle clean">ongoing goals</div>
            <div class="panelElement">
                <!-- placeholder default text, shows until a repeatable goal has been completed. -->
                <!--<div>No repeatable goals yet.</div>-->
                <table>
                    <tr class="hoverAction showZencareModal" data-modalcontent="#updateStatusModal" id="taskListEmployeeStatusRepeat" @if(empty($injury->employee_status)) style="display: none" @endif>
                        <td class="icon"><div class="icon icon-undo FG__cyan"></div></td>
                        <td class="longlabel">Update the injured employee's status</td>
                    </tr>
                    <tr class="tableRowSpacer" @if(empty($injury->employee_status)) style="display: none" @endif></tr>
                    <tr class="hoverAction showZencareModal" data-modalcontent="#updateCostsModal" id="taskListClaimCostsRepeat" @if(empty($medical_total) && empty($legal_total) && empty($indemnity_total) && empty($misc_total)) style="display: none" @endif>
                        <td class="icon"><div class="icon icon-undo FG__cyan"></div></td>
                        <td class="longlabel">Update the claims costs information</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="itemGrid__item half max">
        <div class="zen-actionPanel transparent">
            <div class="panelTitle clean">goals met</div>
            <div class="panelElement">
                <table>
                    <!-- Injury Alert -->
                    <tr class="noAction" id="injuryAlertDelivered" @if(empty($injury_alert->action_taken))style="display: none" @endif>
                        <td class="icon"><div class="icon icon-check FG__green"></div></td>
                        <td class="longlabel">You delivered the injury alert to your carrier</td>
                    </tr>
                    <!-- Official First Report -->
                    <tr class="noAction" id="fullInjuryReportDelivered" @if(empty($injury->full_injury_report_delivered))style="display: none" @endif>
                        <td class="icon"><div class="icon icon-check FG__green"></div></td>
                        <td class="longlabel">You filed the official first report</td>
                    </tr>
                    <!-- Claim Number -->
                    <tr class="noAction" id="claimNumberAdded" @if(empty($injury->claim_number))style="display: none" @endif>
                        <td class="icon"><div class="icon icon-check FG__green"></div></td>
                        <td class="longlabel">You added the claim number - {{$injury->claim_number}}</td>
                    </tr>
                    <!-- Adjuster Info -->
                    <tr class="noAction" id="adjusterInfoCompleted" @if(empty($injury->adjuster_name)) style="display: none" @endif>
                        <td class="icon"><div class="icon icon-check FG__green"></div></td>
                        <td class="longlabel">You added the adjuster info</td>
                    </tr>
                    <!-- Employee Email -->
                    <tr class="noAction" id="employeeInfoCompleted" @if(empty($injured_employee->email)) style="display: none" @endif>
                        <td class="icon"><div class="icon icon-check FG__green"></div></td>
                        <td class="longlabel">You added the employee info</td>
                    </tr>
                    <!-- Injury Resolved -->
                    <tr class="noAction" id="injuryResolved" @if($injury->resolved === 0)style="display: none" @endif>
                        <td class="icon"><div class="icon icon-check FG__green"></div></td>
                        <td class="longlabel">You resolved the injury</td>
                    </tr>
                    <!-- Final Costs -->
                    <tr class="noAction" id="finalCostsUpdated" @if(empty($injury->final_cost)) style="display: none" @endif>
                        <td class="icon"><div class="icon icon-check FG__green"></div></td>
                        <td class="longlabel">You updated the final costs</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
   
</script>
<div class="zen-actionPanel transparent">
    <div class="costsHeader">
        <table>
            <tr>
                <td><span class="hLabel">TOTAL PAID TO DATE:</span></td><td></td>
                <td><span class="hValue" id="claimCostsPaidTotal">${{$injury->getPaidTotal()}}</span></td><td></td>
            </tr>
            <tr>
                <td><span class="hLabel">RESERVE TO DATE:</span></td><td></td>
                <td><span class="hValue" id="claimCostsReserveTotal">${{$injury->getReserveTotal()}}</span></td><td></td>
            </tr>
            <tr>
                <td><span class="hLabel">TOTAL COST:</td><td></td>
                <td><span class="hValue" id="claimCostsPaidPlusReserveTotal">${{($injury->getPaidTotal()) + ($injury->getReserveTotal())}}</span></td><td></td>
            </tr>
        </table>
        <div class="updateCosts"><button class="green showZencareModal" data-modalcontent="#updateCostsModal"><div class="icon icon-money"></div> update costs</button></div>
    </div>

    <div class="costs-chart">
        <table>
            <tr class="costTitles">
                <td></td>
                <td><span>medical</span></td>
                <td><span>indemnity</span></td>
                <td><span>legal</span></td>
                <td><span>misc</span></td>
            </tr>
            <tr>
                <td><span class="ctypeLabel reserve">reserve</span></td>
                <td><span class="cValue reserve" id="medicalReserve">{{(is_null($costs['reserve_medical']) ? "N/A" : '$' . $costs['reserve_medical'])}}</span></td>
                <td><span class="cValue reserve" id="indemnityReserve">{{(is_null($costs['reserve_indemnity']) ? "N/A" : '$' . $costs['reserve_indemnity'])}}</span></td>
                <td><span class="cValue reserve" id="legalReserve">{{(is_null($costs['reserve_legal']) ? "N/A" : '$' . $costs['reserve_legal'])}}</span></td>
                <td><span class="cValue reserve" id="miscReserve">{{(is_null($costs['reserve_misc']) ? "N/A" : '$' . $costs['reserve_misc'])}}</span></td>
            </tr>
            <tr>
                <td></td>
                <td><div class="barBG"><div class="barFG" id="medicalBar" style="height:{{ $medical_percent }}%"></div></div></td>
                <td><div class="barBG"><div class="barFG" id="indemnityBar" style="height:{{ $indemnity_percent }}%"></div></div></td>
                <td><div class="barBG"><div class="barFG" id="legalBar" style="height:{{ $legal_percent }}%"></div></div></td>
                <td><div class="barBG"><div class="barFG" id="miscBar" style="height:{{ $misc_percent }}%"></div></div></td>
            </tr>
            <tr>
                <td><span class="ctypeLabel paid">paid</span></td>
                <td><span class="cValue paid" id="medicalPaid">{{(is_null($costs['paid_medical']) ? "N/A" : '$' . $costs['paid_medical'])}}</span></td>
                <td><span class="cValue paid" id="indemnityPaid">{{(is_null($costs['paid_indemnity']) ? "N/A" : '$' . $costs['paid_indemnity'])}}</span></td>
                <td><span class="cValue paid" id="legalPaid">{{(is_null($costs['paid_legal']) ? "N/A" : '$' . $costs['paid_legal'])}}</span></td>
                <td><span class="cValue paid" id="miscPaid">{{(is_null($costs['paid_misc']) ? "N/A" : '$' . $costs['paid_misc'])}}</span></td>
            </tr>
        </table>
        <div class="line top"></div>
        <div class="line bottom"></div>
    </div>

    <div class="darkChart padded">
        total costs over time
        <canvas id="myChart"></canvas>
    </div>

</div>

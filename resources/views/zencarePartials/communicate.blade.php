<section class="sectionPanel transparent">
    
    <div class="sectionNav">
        <div class="buttonArray inline">
            <button class="orange showZencareModal" data-modalcontent="#treePostModal"><div class="icon icon-comment"></div> new message</button>
            <!--<button class="purple"><div class="icon icon-filter"></div> filter</button>-->
            <button class="grey toggleChat"><div class='icon icon-ellipsis-v'></div> max</button>
        </div>
    </div>
    <div class="sectionContent">
        <section class="messageListing">

        </section>
        <!--
        <div class="placeHolder">
            Message Area
            <span>current alerts and messages go here</span>
        </div>-->
    </div>
</section>

<script>
    var tree_posts = null;

    console.log(tree_posts);

    $('.toggleChat').click(function() {
        $('.messageListing').toggleClass('open');
        if($(this).html("<div class='icon icon-ellipsis-v'></div> max")){
            $(this).html("<div class='icon icon-ellipsis-h'></div> min");
        }else{
            $(this).html("<div class='icon icon-ellipsis-v'></div> max");
        }
    });

    $('.messageListing').on('click', '.messagePost', function() {
        $(this).toggleClass('open');
    });

    function getTreePosts(){
        $.ajax({
            type: 'POST',
            url: '<?php echo route('getTreePosts'); ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                injury_id: '<?php echo $injury->id; ?>'
            },
            success: function(data){
                tree_posts = data.reverse();
                console.log('printing tree posts');
                console.log(tree_posts);
                buildTreePosts();
            }
        })
    }

    function buildTreePosts(){
        var i = 0;
        var html = "";
        for(i; i < tree_posts.length; i++){
            if(tree_posts[i].user_id != null){
                var addedByUser = "Submitted by " + tree_posts[i].user_id;
            }else{
                var addedByUser = "";
            }

            
            var postTitle = "";
            var type = tree_posts[i].type;
            var body = tree_posts[i].body;
                if (type == "resolvedPost") {
                    postTitle = "Injury Resolved";

                } else if (type == "reopenedPost") {
                    postTitle = "Injury Reopened";

                } else if (type == "chatPost") {
                    postTitle = "Message from " + tree_posts[i].user_id;

                } else if (type == "firstReportPost") {
                    if(body.includes('Report')) {
                        postTitle = "First Report of Injury";
                    }else if(body.includes('Alert')){
                        postTitle = "Injury Alert Delivered";
                    }
                } else if (type == "claimNumberPost") {
                    postTitle = "Claim Number";

                } else if (type == "reclassifiedPost") {
                    if(body.includes('litigation')) {
                        postTitle = "Injury Litigated";
                    }else if(body.includes('Severity')){
                        postTitle = "Injury Severity Update";
                    }
                } else if (type == "diaryPost") {
                    postTitle = "Diary Post";

                } else if (type == "valuePost") {
                    postTitle = "Claim Cost Updated";

                } else if (type == "systemPost") {
                    if(body.includes('Note')) {
                        postTitle = "New Injury Note";
                    }
                    else if(body.includes('note')) {
                        postTitle = "New Injury Note";
                    }
                    else if(body.includes('appointment')){
                        if(body.includes('deleted')){
                            postTitle = "Appointment Deleted";
                        }else if(body.includes('new appointment')){
                            postTitle = "New Appointment Scheduled";
                        }
                    }
                    else if(body.includes('suffered')){
                        postTitle = "New Injury Reported";
                    }
                    else if(body.includes('new Care Location')){
                        postTitle = "New Care Location";
                    }
                } else if (type == "reportTimeGoodPost") {
                    postTitle = "Fast Claim Report";

                } else if (type == "reportTimeBadPost") {
                    postTitle = "Slow Claim Report";

                } else if (type == "reportTimeOkayPost") {
                    postTitle = "Medium Claim Report";
                    //NEW TYPES

                } else if (type == "employeeStatusPost") {
                    if(body.includes('Status')) {
                        postTitle = "Employee Status Updated";
                    }else if(body.includes('info')){
                        postTitle = "Employee Info Updated";
                    }
                } else if (type == "finalCostPost") {
                    postTitle = "Final Cost";

                } else if (type == "adjusterPost") {
                    postTitle = "Adjuster Info Added";
                } else if (type == "notesPost")  {
                    postTitle = 'Adjuster Notes Added'

                } else if (type == "photoPost") {
                    if(body.includes('added')){
                        postTitle = "Photo Uploaded";
                    }
                    else if(body.includes('deleted')){
                        postTitle = "Photo Deleted";
                    }
                } else if (type == "filePost") {
                    if(body.includes('added')) {
                        postTitle = "File Uploaded";
                    }else if(body.includes('deleted')){
                        postTitle = "File Deleted";
                    }
                } else if (type == "newInjuryPost") {
                    if(tree_posts[i].user_id){
                        postTitle = "New Injury Reported by " + tree_posts[i].user_id;
                    }else{
                        postTitle = "New Injury Reported";
                    }

                } else if (type == "reserveBadPost") {
                    postTitle = "Reserve Difference";

                } else if (type == "reserveGoodPost") {
                    postTitle = "Reserve Difference";

                } else if (type == "reminderPost") {
                    postTitle = "Zenjuries Reminder";

                } else if (type == "newTaskPost") {
                    postTitle = "New Request";

                } else if (type == "completeTaskPost") {
                    postTitle = "Completed Request";

                } else if (type == "taskReminderPost") {
                    postTitle = "Request Reminder";

                } else if (type == "expiredTaskPost") {
                    postTitle = "Expired Request";

                }else{
                    postTitle = "No Title";
                }
                
            html += '<div class="messageContainer">' +
                '                <div class="messagePost">' +
                '                    <div class="messageContent">' +
                '                        <span class="messageType ' + tree_posts[i].type +'"><span class="messageIcon" style="background-image:url('+tree_posts[i].profile_image+')"></span></span>' +
                '                        <span class="messageTitle">' + postTitle +'</span>' +
                '                        <span class="messageBody">' + tree_posts[i].body +' ' + '<br />' + addedByUser +'</span>' +
                '                        <span class="messageDateTime">' + tree_posts[i].formatted_date + '</span>' +
               // '                        <span class="messageReplies">no replies</span>' +
                '                        <div class="messageControls">' +
                '                            <div class="buttonArray inline small">' +
               // '                                <button class="orange"><div class="icon icon-commenting"></div> reply</button>' +
               // '                                <button class="cyan"><div class="icon icon-pencil-1"></div> edit</button>' +
               // '                                <button class="red"><div class="icon icon-times"></div> delete</button>' +
                '                            </div>' +
                '                        </div>' +
                '                    </div>' +
                '                </div>' +
                '            </div>';
        }
        $('.messageListing').html(html);
    }

    getTreePosts();

    /*
    <textarea id="treePostInput">

                        </textarea><br>
                        <span class="inputError"></span>
                        </div>
                        <button id="submitTreePost">Submit</button
     */


</script>
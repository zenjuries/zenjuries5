@extends('layouts.zen5_layout_simple')
@section('maincontent')
<?php
Auth::logout();
$brandCompany = env('COMPANY_NAME');
$brandID = env('BRAND_NAME');
$requirePayment = env('REQUIRE_PAYMENTS'); /* true, false */
$isBranded = env('THEME'); /* default, branded */
$brandApp = env('BRAND_APP_NAME'); /* app brand */
?>
<style>
.loginPageContainer{height:100vh;display:grid;place-items:center;}	
.login-panel {
    position:relative;
    width:100%;
    max-width:400px;
    height:500px;
    overflow:hidden;
    border-radius:8px;
    box-shadow:0px 0px 14px 3px black;
    padding:40px;
    background-color: rgb(54,54,54);
    align-self:center;
    justify-self:center;
}
.loginLinks{width:100%;text-align:center;color:#777;}
.loginLinks a{color:grey;letter-spacing:.07rem;text-transform:uppercase;text-decoration:none;font-size:.7rem;}
.loginLinks a:hover{color:white;}
@media (max-width: 576px) {
	.loginPageContainer{background-color:rgb(54,54,54);}
    .login-panel{background-color:transparent;box-shadow:none;}
}
.rememberme{position:absolute;right:10px;bottom:10px;line-height:1.4rem;color:grey;letter-spacing:.05rem;text-transform:uppercase;text-decoration:none;font-size:.7rem}

#loginlogo{
width:200px;height:200px;
background-size:auto;
background-repeat:no-repeat;
background-position:center center;
}

.messageOverlay{position:absolute;top:0px;left:0px;width:100%;}
.alert-text ul{margin:0;padding:0;list-style:none;width:100%;text-align:center;}
.alert-text li{display:block;color:red;}
.loginMessageContainer{width:100%;text-align:center;padding:0 2rem;position:absolute;top:10px;left:0px;z-index:999;}
.loginMessage{border:1px solid red;background-color:#dd000040;border-radius:12px;padding:20px;color:white;font-weight:300;font-size:1rem;}
</style>
 
           
<div class="loginPageContainer">	   

<div class="loginMessageContainer" style="display:none;"><div class="loginMessage"><span>
    
</span></div></div>
		
	<div class="login-panel">
		<div style="width:200px;height:200px;margin:0 auto;margin-bottom:20px;">
            @if($isBranded === "branded")
                <div id="loginlogo" style="background-image:url('/branding/{{ $brandID }}/images/logo_stacked_dark.png');background-size:contain;">		
                </div>
            @else
                <div id="loginlogo">
                <script>document.getElementById('loginlogo').style.backgroundImage = "url('/images/gif/zenjuries-anim.gif?v=" + new Date().valueOf() + "')"</script>					
                </div>
            @endif
		</div>

        <form id="zenployeesLoginForm" role="form" method="POST" action="/login" id="">
            @csrf
            <section class="formBlock dark">
                <div class="formGrid">  
                    <div class="formInput" style="height:60px;">
                        <!-- input -->
                        <label for="email">login email</label>
                        <div class="inputIcon mail"><input id="email" name="email" type="email" style="width:200px;"/></div>
                    </div>
                    <div class="formInput" style="height:60px;">
                        <!-- input -->
                        <label for="password">password</label>
                        <div class="inputIcon password"><input id="password" name="password" type="password" style="width:200px;" /></div>
                    </div>
                </div>
                <br>
                <div class="formGrid"> 
                    <div class="buttonArray" >
                        <button id="submitLoginForm" class="clientColor1 centered" >login to {{ $brandApp }}</button>
                    </div>
                    <div class="loginLinks">
                @if($requirePayment === true)
                    <a href="/agencyRegister">register</a> |
                @endif 
                    <a href="{{ url('forgot-password') }}">reset password</a></div>
                </div>
                <div class="rememberme">
                    <div class="formInput">remember me <input type="checkbox" name="remember"></div>
                </div>
            </section> 
        </form>


        <div class="messageOverlay">
     <!-- login errors -->
    <!-- you can change this html however you want, it only shows if there are errors when you submit the form. -->
    @if (count($errors) > 0)
	
        <div class="alert-text">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        
    @endif
 	    </div>        
         
	</div> 	 	
</div>


<script>
$('#submitLogin').on('click', function(){
    $('#zenployeesLoginForm').submit();
});

//submit the form by pressing enter while focus is on either field
$('#zenployeesLoginForm').on('keyup', function(event){
    //get the key code for the key pressed, 13 is the code for the enter key
    var key = event.which;
    if(key == 13){
        $('#submitLogin').click();
    }

});
</script>

@endsection
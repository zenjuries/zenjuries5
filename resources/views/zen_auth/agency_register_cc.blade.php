@extends('layouts.zen5_layout_guest')
@section('maincontent')
<?php
    $company = new \App\Company();
    $intent = $company->createSetupIntent();

    //arrays for error popup [icon image, error description, error code, button color, button ID, button icon, button label]
    $error1 = array("uhoh", "error message here", "zen233", "red", "dismiss", "exclamation-circle", "dismiss");
    $errormessage = $error1;

    $brandCompany = env('COMPANY_NAME');
    $brandID = env('BRAND_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $requirePayment = env('REQUIRE_PAYMENTS'); /* true, false */   
?>

<script src="https://js.stripe.com/v3/"></script>

<div class="pageContent bgimage-bgheader pagebackground16 fade-in">		
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                    <span><b>{{ $brandCompany }}</b> registration</span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="/images/icons/edit.png"></span><span class="subContent">register now for <b>{{ $brandApp }}</b></span>
                    </div>
            </div>
            <div class="pageIcon animate__animated animate__bounceInRight">
                <img src="/images/icons/userinfo.png">
            </div> 
        </div>           
    </div>

    <div style="height:10px;"></div>
    <div class="contentBlock noPad">
        <div class="container noPad">

            <section class="sectionPanel">
                <div class="zen-actionPanel noHover transparent tight">                
                    <div class="sectionContent">
                        Zenjuries Plan $500 / Monthly includes: <b>Injury & Claim management</b>--easily manage all of your injury claims.<b> Policyholder management</b>--manage your policy-holders using Zenjuries intuitive claim system <b>ZenPro</b>--A Zenpro will personally assist with every claim.
                        <div class="center" style="padding:20px 0;">Every field is required.  Fill out all information, enter your credit card information, and click the submit registration button below.</div>
                        
                        <section class="formBlock dark">
                            <div class="formGrid">  
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="agency_name">agency name</label>
                                    <div class="inputIcon check"><input id="agency_name" type="text" /></div>
                                    <span class="inputError">required field</span>
                                </div>
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="phone">phone</label>
                                    <div class="inputIcon phone"><input id="phone" type="tel" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" name="PhoneIn" aria-label="Please enter your phone number" placeholder="ex. 1(111)-111-1111"/></div>
                                    <span class="inputError">required field</span>
                                </div>
                            </div>   
                            <hr>                       
                            <div class="formGrid">  
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="street_address">street address</label>
                                    <div class="inputIcon address"><input id="street_address" type="text" /></div>
                                    <span class="inputError">required field</span>
                                </div>
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="city">city</label>
                                    <div class="inputIcon city"><input id="city" type="text" /></div>
                                    <span class="inputError">required field</span>
                                </div>
                            </div>
                            <div class="formGrid">  
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="state">state</label>
                                    <div class="inputIcon pointer">
                                        <select id="state" name="state" style="width:212px;">
                                        <option value="---">choose state</option><option value="Alabama">Alabama</option><option value="Alaska">Alaska</option><option value="Arizona">Arizona</option><option value="Arkansas">Arkansas</option><option value="California">California</option><option value="Colorado">Colorado</option><option value="Connecticut">Connecticut</option><option value="Delaware">Delaware</option><option value="District of Columbia">District of Columbia</option><option value="Florida">Florida</option><option value="Georgia">Georgia</option><option value="Guam">Guam</option><option value="Hawaii">Hawaii</option><option value="Idaho">Idaho</option><option value="Illinois">Illinois</option><option value="Indiana">Indiana</option><option value="Iowa">Iowa</option><option value="Kansas">Kansas</option><option value="Kentucky">Kentucky</option><option value="Louisiana">Louisiana</option><option value="Maine">Maine</option><option value="Maryland">Maryland</option><option value="Massachusetts">Massachusetts</option><option value="Michigan">Michigan</option><option value="Minnesota">Minnesota</option><option value="Mississippi">Mississippi</option><option value="Missouri">Missouri</option><option value="Montana">Montana</option><option value="Nebraska">Nebraska</option><option value="Nevada">Nevada</option><option value="New Hampshire">New Hampshire</option><option value="New Jersey">New Jersey</option><option value="New Mexico">New Mexico</option><option value="New York">New York</option><option value="North Carolina">North Carolina</option><option value="North Dakota">North Dakota</option><option value="Northern Marianas Islands">Northern Marianas Islands</option><option value="Ohio">Ohio</option><option value="Oklahoma">Oklahoma</option><option value="Oregon">Oregon</option><option value="Pennsylvania">Pennsylvania</option><option value="Puerto Rico">Puerto Rico</option><option value="Rhode Island">Rhode Island</option><option value="South Carolina">South Carolina</option><option value="South Dakota">South Dakota</option><option value="Tennessee">Tennessee</option><option value="Texas">Texas</option><option value="Utah">Utah</option><option value="Vermont">Vermont</option><option value="Virginia">Virginia</option><option value="Virgin Islands">Virgin Islands</option><option value="Washington">Washington</option><option value="West Virginia">West Virginia</option><option value="Wisconsin">Wisconsin</option><option value="Wyoming">Wyoming</option></select>
                                    </div>
                                    <span class="inputError">required field</span>
                                </div>
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="zipcode">zipcode</label>
                                    <div class="inputIcon zip"><input id="zipcode" type="text" /></div>
                                    <span class="inputError">required field</span>
                                </div>                                
                            </div>
                            <hr>
                            <div class="formGrid">  
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="contact_name">contact name</label>
                                    <div class="inputIcon user"><input id="contact_name" type="text" /></div>
                                    <span class="inputError">required field</span>
                                </div>
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="email_address">email address</label>
                                    <div class="inputIcon mail"><input id="email_address" type="email" /></div>
                                    <span class="inputError">required field</span>
                                </div>
                            </div>
                            <hr>
                            
                            <div class="formGrid"> 
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="password">password</label>
                                    <div class="inputIcon password"><input id="password" type="password" style="width:200px;"/></div>
                                    <span class="inputError">required field</span>
                                    <!-- input -->
                                    <label for="password_confirmation">confirm password</label>
                                    <div class="inputIcon password"><input id="password_confirmation" type="password" style="width:200px;" /></div>
                                    <span class="inputError">required field</span>                                    
                                </div>
                            </div>                                                                                                               
                            <br><br><hr>
                            <div class="formGrid short">
                                <div class="formInput">
                                    <input type="checkbox" name="tos" id="tos">
                                    <label for="tos">I agree to the <a href="javascript:void(0);" class="showZenModal" data-modalcontent="#tosModal" style="color:cyan;">terms of service</a>.</label>
                                </div>
                            </div>
                            <br><br><hr> 
                            <div id="updateCardDiv" style="max-width:500px;height:260px;padding:1rem;margin:0 auto;border:1px solid black;border-radius:8px;background-color:#ffffff05;">
                                <section class="formBlock dark">                          
                                    <div class="formGrid">  
                                        <div class="formInput" style="width:100%;">
                                        <label for="updateCardName">cardholder's name</label>
                                        <div class="inputIcon user"><input id="updateCardName" style="width:100%;"></div><br><br>
                                        <label for="updateCardCardElement" style="margin-bottom:10px;">card details</label>
                                            
                                            <!-- This div will be populated with card inputs by the stripe.js code -->
                                            <div class="stripeOverride" id="updateCardCardElement"></div>
                                            <div id="updateCardErrors"></div>
                                            <span style="display:block;font-weight:300;font-size:.8rem;width:100%;text-align:center;">(use numbers only to input card number and month/year/cvc)</span><br>
                                            <div class="buttonArray">
                                                <button id="fillInBtn" class="red centered">please fill in all fields to complete</button>
                                                <button id="updateCardSubmitButton" class="green centered" data-secret="{{$intent->client_secret}}" style="display:none;">Submit Registration</button>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </section>    
                    </div>
                </div>
            </section>


        <div style="height:160px;">
            <div class="buttonCloud small devButtons">
                <button class="showZenModal" data-modalcontent="#errorModal">error</button>
                <button class="showZenModal" data-modalcontent="#registerSuccess">success</button>
			</div>
		</div>

        </div>
    </div>
</div>


    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
	@include('partials.modals.tos')
	@include('partials.modals.errorModal')
	@include('partials.modals.registerSuccess')      
</div>    
<script>
    $('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>
<script>

    $(document).ready(function(){
        const stripe = Stripe('<?php echo env("STRIPE_KEY"); ?>');
        
        /*
        const appearance = {
            theme: 'stripe'
            }; 
        */
        
        const elements = stripe.elements();
        
        const cardElement = elements.create('card', {
                style: {
                base: {
                    iconColor: '#c4f0ff',
                    color: '#fff',
                    fontWeight: '500',
                    fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
                    fontSize: '16px',
                    fontSmoothing: 'antialiased',
                    ':-webkit-autofill': {
                    color: '#fce883',
                    },
                    '::placeholder': {
                    color: '#87BBFD',
                    },
                },
                invalid: {
                    iconColor: '#FFC7EE',
                    color: '#FFC7EE',
                },
                }});

        cardElement.mount('#updateCardCardElement');

        const cardHolderName = document.getElementById('updateCardName');
        const cardButton = document.getElementById('updateCardSubmitButton');
        const clientSecret = cardButton.dataset.secret;

        cardButton.addEventListener('click', async (e) => {
            $('#updateCardErrors').html("");
        const { setupIntent, error} = await stripe.confirmCardSetup(
            clientSecret, {
                payment_method:{
                    card: cardElement,
                    billing_details: {name: cardHolderName.value}
                }
            }               
        );                
        if (error) {
            $('#updateCardErrors').html(error.message);
            /*
            console.log(error);
            alert('error: ' + error.message);
            */
        } else {
            alert('success!');
            console.log(setupIntent);
            submitAgencyRegistration(setupIntent['payment_method']);
        }
        });
        

        var isCardInfoComplete = false;
        cardElement.on('change', function(event){
            if(event.complete){
                //turn on boolean
                isCardInfoComplete = true;
            }
            else{
                //turn off boolean
                isCardInfoComplete = false;
            }
            checkMatch(isCardInfoComplete);
        });

        //checkMatch code
        $('#agency_name').on('input', function(){
            checkMatch(isCardInfoComplete);
        });
        $('#phone').on('input', function(){
            checkMatch(isCardInfoComplete);
        });
        $('#street_address').on('input', function(){
            checkMatch(isCardInfoComplete);
        });
        $('#city').on('input', function(){
            checkMatch(isCardInfoComplete);
        });
        $('#state').on('input', function(){
            checkMatch(isCardInfoComplete);
        });
        $('#zipcode').on('input', function(){
            checkMatch(isCardInfoComplete);
        });
        $('#contact_name').on('input', function(){
            checkMatch(isCardInfoComplete);
        });
        $('#email_address').on('input', function(){
            checkMatch(isCardInfoComplete);
        });
        $('#password').on('input', function(){
            checkMatch(isCardInfoComplete);
        });
        $('#password_confirmation').on('input', function(){
            checkMatch(isCardInfoComplete);
        });
        $('#updateCardName').on('input', function(){
            checkMatch(isCardInfoComplete);
        });
        $('#tos').on('input', function(){
            checkMatch(isCardInfoComplete);
        });
        $('#eulaAgree').on('click', function(){
            var tosCheckBox = document.getElementById("tos");
            tosCheckBox.checked = true;
            checkMatch(isCardInfoComplete);
            modal.close();
        });
    });

    function checkMatch(isCardInfoComplete){
        var agencyNameVar = document.getElementById("agency_name");
        var phoneVar = document.getElementById("phone");
        var streetAddressVar = document.getElementById("street_address");
        var cityVar = document.getElementById("city");
        var stateVar = document.getElementById("state");
        var zipCodeVar = document.getElementById("zipcode");
        var nameVar = document.getElementById("contact_name");
        var emailVar = document.getElementById("email_address");
        var passwordVar = document.getElementById("password");
        var passwordConfirmVar = document.getElementById("password_confirmation");
        var cardNameVar = document.getElementById("updateCardName");
        var fillInBtn = document.getElementById("fillInBtn");
        var submitBtn = document.getElementById("updateCardSubmitButton");
        var tosCheckBox = document.getElementById("tos");

        if(passwordVar.value == "" && passwordConfirmVar.value == ""){
            passwordVar.className = '';
            passwordConfirmVar.className = '';
        }
        else if(passwordVar.value == passwordConfirmVar.value && passwordVar.value.length >= 8){
            passwordVar.className = 'match';
            passwordConfirmVar.className = 'match';
        }
        else{
            passwordVar.className = 'nomatch';
            passwordConfirmVar.className = 'nomatch';
        }

        //get isCardInfoComplete boolean value here?
        if(passwordVar.value == passwordConfirmVar.value && passwordVar.value.length >= 8 && agencyNameVar.value != "" && phoneVar.value.length >= 14 && streetAddressVar.value != "" && cityVar.value != "" && stateVar.value != "---" && zipCodeVar.value != "" && nameVar.value != "" && emailVar.value != "" && cardNameVar.value != "" && isCardInfoComplete == true && tosCheckBox.checked){
            submitBtn.style = "display:inline";
            fillInBtn.style = "display:none";
        }
        else{
            submitBtn.style = "display:none";
            fillInBtn.style = "display:inline";
        }
    }

    function submitAgencyRegistration(payment_method){
        var agency_name = $('#agency_name').val();
        var street_address = $('#street_address').val();
        var city = $('#city').val();
        var state = $('#state').val();
        var zipcode = $('#zipcode').val();
        var phone = $('#phone').val();
        var contact_name = $('#contact_name').val();
        var email_address = $('#email_address').val();
        var password = $('#password').val();
        var password_confirmation = $('#password_confirmation').val();

        phone = phone.replace(/\D/g,'');

        $.ajax({
            type: 'POST',
            url: '<?php echo route("registerZAgent"); ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                agency_name: agency_name,
                street_address: street_address,
                city: city,
                state: state,
                zipcode: zipcode,
                phone: phone,
                contact_name: contact_name,
                email_address: email_address,
                password: password,
                password_confirmation: password_confirmation,
                payment_method: payment_method,
            }, success: function(data){
                window.location.replace('/zagent')
            }, error: function(data){
                console.log(data);
                alert('error');
            }
        });
    }

</script>




@endsection
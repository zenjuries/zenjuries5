@extends('layouts.zen5_layout_guest')
@section('maincontent')
    <?php
    Auth::logout();

    $company = \App\Company::where('id', $company_id)->first();
    $user = \App\User::where('password_token', $token)->first();
    $policy_submission = $company->getPolicySubmission();

    if(!is_null($policy_submission) && $policy_submission->client_pays === 1){
        $client_pays = 1;
    }else{
        $client_pays = 0;
    }

    $intent = $company->createSetupIntent();

    //arrays for error popup [icon image, error description, error code, button color, button ID, button icon, button label]
    $error1 = array("uhoh", "error message here", "zen233", "red", "dismiss", "exclamation-circle", "dismiss");
    $errormessage = $error1;

    //arrays for wait spinner overlay [spinner color, center message, top message, lower message]
    $waitspinner1 = array("green", "one<br>moment", " ", "registering...");
    $spinnercontent = $waitspinner1;  
    
    $brandCompany = env('COMPANY_NAME');
    $brandID = env('BRAND_NAME');
    $brandApp = env('BRAND_APP_NAME');
    $requirePayment = env('REQUIRE_PAYMENTS'); /* true, false */      
    ?>

<div class="pageContent bgimage-bgheader pagebackground17 fade-in">		
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                    <span>{{ $brandCompany }} <b>registration</b> form</span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="/images/icons/edit.png"></span><span class="subContent">register for <b>{{ $brandApp }}</b></span>
                    </div>
            </div>
            <div class="pageIcon animate__animated animate__bounceInRight">
                <img src="/images/icons/userinfo.png">
            </div> 
        </div>           
    </div>

    <div style="height:10px;"></div>
    <div class="contentBlock noPad">
        <div class="container noPad">

            <section class="sectionPanel">
                <div class="sectionContent">
                    <div class="center" style="padding:20px 0;">Every field is required.  Fill out all information, enter your credit card information (if required), and click the submit registration button below.</div>
                    <section class="formBlock dark">

                        <input type="hidden" id="user_id" value="{{$user->id}}">
                        <input type="hidden" id="company_id" value="{{$company->id}}">
                        <input type="hidden" id="pw_token" value="{{$token}}">

                        <div class="formGrid" >  
                            <div class="formInput">
                                <!-- input -->
                                <label for="name">name</label>
                                <div class="inputIcon user"><input id="name" type="text" style="width:100%" required value="{{$user->name}}"/></div>
                                <span class="inputError">required field</span>
                            </div>
                        </div>
                        <hr>
                        <div class="formGrid">  
                            <div class="formInput">
                                <!-- input -->
                                <label for="companyName">company name</label>
                                <div class="inputIcon check"><input id="companyName" type="text" required value="{{$company->company_name}}"/></div>
                                <span class="inputError">required field</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="email">company phone</label>
                                    <div class="inputIcon phone"><input id="companyPhone"  class="phone_us" type="tel" aria-label="Please enter your phone number" placeholder="ex. (111)-111-1111"/></div>
                                    <span class="inputError">numbers only</span>
                            </div>
                        </div>
                        <div class="formGrid">  
                            <div class="formInput">
                                <!-- input -->
                                <label for="street_address">street address</label>
                                <div class="inputIcon address"><input id="street_address" type="text" /></div>
                                <span class="inputError">required field</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="city">city</label>
                                <div class="inputIcon city"><input id="city" type="text" /></div>
                                <span class="inputError">required field</span>
                            </div>
                        </div>
                        <div class="formGrid">  
                            <div class="formInput">
                                <!-- input -->
                                <label for="state">state</label>
                                    <div class="inputIcon pointer">
                                        <select id="state" name="state" style="width:213px;">
                                        <option value="---">choose state</option><option value="Alabama">Alabama</option><option value="Alaska">Alaska</option><option value="Arizona">Arizona</option><option value="Arkansas">Arkansas</option><option value="California">California</option><option value="Colorado">Colorado</option><option value="Connecticut">Connecticut</option><option value="Delaware">Delaware</option><option value="District of Columbia">District of Columbia</option><option value="Florida">Florida</option><option value="Georgia">Georgia</option><option value="Guam">Guam</option><option value="Hawaii">Hawaii</option><option value="Idaho">Idaho</option><option value="Illinois">Illinois</option><option value="Indiana">Indiana</option><option value="Iowa">Iowa</option><option value="Kansas">Kansas</option><option value="Kentucky">Kentucky</option><option value="Louisiana">Louisiana</option><option value="Maine">Maine</option><option value="Maryland">Maryland</option><option value="Massachusetts">Massachusetts</option><option value="Michigan">Michigan</option><option value="Minnesota">Minnesota</option><option value="Mississippi">Mississippi</option><option value="Missouri">Missouri</option><option value="Montana">Montana</option><option value="Nebraska">Nebraska</option><option value="Nevada">Nevada</option><option value="New Hampshire">New Hampshire</option><option value="New Jersey">New Jersey</option><option value="New Mexico">New Mexico</option><option value="New York">New York</option><option value="North Carolina">North Carolina</option><option value="North Dakota">North Dakota</option><option value="Northern Marianas Islands">Northern Marianas Islands</option><option value="Ohio">Ohio</option><option value="Oklahoma">Oklahoma</option><option value="Oregon">Oregon</option><option value="Pennsylvania">Pennsylvania</option><option value="Puerto Rico">Puerto Rico</option><option value="Rhode Island">Rhode Island</option><option value="South Carolina">South Carolina</option><option value="South Dakota">South Dakota</option><option value="Tennessee">Tennessee</option><option value="Texas">Texas</option><option value="Utah">Utah</option><option value="Vermont">Vermont</option><option value="Virginia">Virginia</option><option value="Virgin Islands">Virgin Islands</option><option value="Washington">Washington</option><option value="West Virginia">West Virginia</option><option value="Wisconsin">Wisconsin</option><option value="Wyoming">Wyoming</option></select>
                                    </div>
                                    <span class="inputError">required field</span>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="zipcode">zipcode</label>
                                    <div class="inputIcon zip"><input id="zipcode" type="text" /></div>
                                    <span class="inputError">required field</span>
                            </div>
                        </div>
                        <hr>
                        <div class="center" style="padding:20px 0;">Passsword requirement is minimum 8 characters.  Please create secure passwords!</div>
                        <div class="formGrid"> 
                         
                            <div class="formInput" style="height:160px;">
                                <!-- input -->
                                <label for="password">password</label>
                                <div class="inputIcon password"><input id="password" type="password" style="width:200px;"/></div>
                                <span class="inputError">required field</span>
                                <!-- input -->
                                <label for="password_confirmation">confirm password</label>
                                <div class="inputIcon password"><input id="password_confirmation" type="password" required style="width:200px;" /></div>
                                <span class="inputError">required field</span>
                                <input id="invitationToken" type="hidden" value="">
                            </div>
                        </div>
                        <hr> 
                        <div class="formGrid short">
                            <div class="formInput">
                                <input type="checkbox" name="tos" id="tos">
                                <label for="tos">I agree to the <a href="javascript:void(0);" class="showZenModal" data-modalcontent="#tosModal" style="color:cyan;">terms of service</a>.</label>
                            </div>
                        </div>
                        <br><br><hr> 
                    </section>
                    <section class="formBlock dark">
                        <div class="formGrid">
                            @if($client_pays)
                                <div class="center">The cost for Zenjuries subscription is <span class="FG__lime">${{$policy_submission->final_cost}}</span>,
                                    which renews on your policy's Renewal Date. Please enter your card details below.
                                    <div style="max-width:500px;height:260px;padding:1rem;margin:20px auto;border:1px solid black;border-radius:8px;background-color:#ffffff05;">

                                        <span class="inputError" id="paymentError" style="top:5px;left:0px;"></span>

                                        <div class="formInput" style="width:100%;">
                                            <label for="updateCardName">cardholder's name</label>
                                            <div class="inputIcon user"><input id="updateCardName" style="width:100%;"></div><br><br>

                                            <!-- This div will be populated with card inputs by the stripe.js code -->
                                        
                                            <div class="stripeOverride" id="updateCardCardElement"></div>
                                            <div id="updateCardErrors"></div>
                                            <span style="display:block;font-weight:300;font-size:.8rem;width:100%;text-align:center;">(use numbers only to input card number and month/year/cvc)</span><br>
                                            <div class="buttonArray">
                                                <button id="fillInBtn" class="red centered">please fill in all fields first</button>
                                            @if($client_pays)
                                                <button id="submitNewClientWithPayment" data-secret="{{$intent->client_secret}}" class="green centered" style="display:none;">Submit Registration</button>
                                            @endif
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                            @else
                                <div class="buttonArray">
                                    <button id="submitNewClient" class="centered red">please fill in all fields first</button>
                                </div>
                                @if($requirePayment)
                                <ul class="sectionHelpPanel blue">
                                    <li class="sectionPanelIcon"><img class="wow animateanimated animateswing animate__repeat-2" src="/images/icons/creditcard.png"></li>
                                    <li style="text-align:center;">Your Zenjuries subscription has been provided free of charge by <b class="FG__yellow">{{$company->getAgency()->name}}</b>!</li>
                                </ul>
                                @endif
                            @endif
                        </div>
                    </section>
                </div>
            </section>
        </div>
    </div>
</div>

    <div style="height:160px;">
        <div class="buttonCloud small devButtons">
        <button class="showZenModalLocked" data-modalcontent="#waitspinnerModal"><div class="icon icon-magic"></div> register in progress---</button>
        <button class="showZenModal" data-modalcontent="#errorModal"><div class="icon icon-magic"></div> error---</button>
        <button class="showZenModal" data-modalcontent="#registerSuccess"><div class="icon icon-magic"></div> success---</button>
        </div>
    </div>

<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
	@include('partials.modals.tos')
    @include('partials.modals.waitOverlay') 
	@include('partials.modals.errorModal')
	@include('partials.modals.registerSuccess')
</div>    
<script>
    $('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>
<script>
    $('.showZenModalLocked').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true,
            closeButton: false,
            closeOnClick: false
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>

<script>
    $(document).ready(function() {
        var client_pays = '<?php echo $client_pays; ?>';

        if(client_pays === "1"){
            const stripe = Stripe('<?php echo env("STRIPE_KEY"); ?>');

            const elements = stripe.elements();
            const cardElement = elements.create('card', {
				  style: {
				    base: {
				      iconColor: '#c4f0ff',
				      color: '#fff',
				      fontWeight: '500',
				      fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
				      fontSize: '16px',
				      fontSmoothing: 'antialiased',
				      ':-webkit-autofill': {
				        color: '#fce883',
				      },
				      '::placeholder': {
				        color: '#87BBFD',
				      },
				    },
				    invalid: {
				      iconColor: '#FFC7EE',
				      color: '#FFC7EE',
				    },
				  }});

            cardElement.mount('#updateCardCardElement');

            const cardHolderName = document.getElementById('updateCardName');
            const cardButton = document.getElementById('submitNewClientWithPayment');
            const clientSecret = cardButton.dataset.secret;

            cardButton.addEventListener('click', async (e) => {
                $('button[data-modalcontent="#waitspinnerModal"]').click();
                var valid = validateNewClient();
                if(valid) {
                    $('#updateCardErrors').html("");
                    const {setupIntent, error} = await
                    stripe.confirmCardSetup(
                        clientSecret, {
                            payment_method: {
                                card: cardElement,
                                billing_details: {name: cardHolderName.value}
                            }
                        }
                    );
                    if (error) {
                        modal.close();
                        console.log(error);
                        $('#updateCardErrors').html(error.message);

                    } else {
                        console.log(setupIntent);
                        submitNewClient(setupIntent['payment_method']);
                        // submitAgencyRegistration(setupIntent['payment_method']);
                    }
                }
            });

            var isCardInfoComplete = false;
            cardElement.on('change', function(event){
                if(event.complete){
                    //turn on boolean
                    isCardInfoComplete = true;
                }
                else{
                    //turn off boolean
                    isCardInfoComplete = false;
                }
                checkMatchWithPayment(isCardInfoComplete);
            });


            $('#companyName').on('input', function(){
                checkMatchWithPayment(isCardInfoComplete);
            })
            $('#name').on('input', function(){
                checkMatchWithPayment(isCardInfoComplete);
            })
            $('#companyPhone').on('input', function(){
                checkMatchWithPayment(isCardInfoComplete);
            })
            $('#street_address').on('input', function(){
                checkMatchWithPayment(isCardInfoComplete);
            })
            $('#city').on('input', function(){
                checkMatchWithPayment(isCardInfoComplete);
            })
            $('#state').on('input', function(){
                checkMatchWithPayment(isCardInfoComplete);
            })
            $('#zipcode').on('input', function(){
                checkMatchWithPayment(isCardInfoComplete);
            })
            $('#password').on('input', function(){
                checkMatchWithPayment(isCardInfoComplete);
            })
            $('#password_confirmation').on('input', function(){
                checkMatchWithPayment(isCardInfoComplete);
            })
            $('#updateCardName').on('input', function(){
                checkMatchWithPayment(isCardInfoComplete);
            })
            $('#tos').on('input', function(){
                checkMatchWithPayment(isCardInfoComplete);
            });
            $('#eulaAgree').on('click', function(){
                var tosCheckBox = document.getElementById("tos");
                tosCheckBox.checked = true;
                checkMatchWithPayment(isCardInfoComplete);
                modal.close();
            });
        }else{
            $('#companyName').on('input', function(){
                checkMatch();
            })
            $('#name').on('input', function(){
                checkMatch();
            })
            $('#companyPhone').on('input', function(){
                checkMatch();
            })
            $('#street_address').on('input', function(){
                checkMatch();
            })
            $('#city').on('input', function(){
                checkMatch();
            })
            $('#state').on('input', function(){
                checkMatch();
            })
            $('#zipcode').on('input', function(){
                checkMatch();
            })
            $('#password').on('input', function(){
                checkMatch();
            })
            $('#password_confirmation').on('input', function(){
                checkMatch();
            })
            $('#tos').on('input', function(){
                checkMatch();
            });
            $('#eulaAgree').on('click', function(){
                var tosCheckBox = document.getElementById("tos");
                tosCheckBox.checked = true;
                checkMatch();
                modal.close();
            });
        }

        $('#submitNewClient').on('click', function () {
            if($(this).hasClass('red')){
                return;
            }
            var valid = validateNewClient();
            if (valid) {
                $('button[data-modalcontent="#waitspinnerModal"]').click();
                submitNewClient(null);
            }
        });

        function submitNewClient(payment_method = null) {
            var company_name = $('#companyName').val();
            var name = $('#name').val();
            var password = $('#password').val();
            var password_confirmation = $('#password_confirmation').val();
            var company_phone = $('#companyPhone').val();
            var mailing_street = $('#street_address').val();
            var city = $('#city').val();
            var state = $('#state').val();
            var zip = $('#zipcode').val();

            var company_id = $('#company_id').val();
            var user_id = $('#user_id').val();
            var pw_token = $('#pw_token').val();

            company_phone = company_phone.replace(/\D/g,'');

            $.ajax({
                type: 'POST',
                url: '<?php echo route('verifyAccountNewPolicyHolder'); ?>',
                data: {
                    _token: '<?php echo csrf_token(); ?>',
                    name: name,
                    //email: email,
                    password: password,
                    password_confirmation: password_confirmation,
                    company_phone: company_phone,
                    mailing_street: mailing_street,
                    city: city,
                    state: state,
                    zip: zip,
                    payment_method: payment_method,
                    company_id: company_id,
                    user_id: user_id,
                    pw_token: pw_token
                },
                success: function () {
                    modal.close();
                    $('button[data-modalcontent="#registerSuccess"]').click();
                    setTimeout(() => {
                        window.location.replace('/');
                    }, 5000);
                },
                error: function (data) {
                    console.log(data);
                    modal.close();
                    $('#errorCode').html(data['responseJSON']['errors']['message']);
                    $('button[data-modalcontent="#errorModal"]').click();
                }
            })
        }

        function validateNewClient() {
            $('.inputError').removeClass('show');
            var valid = true;

            var company_name = $('#companyName').val();
            if (company_name === "") {
                valid = false;
                $('#companyName').parent().find('.inputError').addClass("show");
            }
            var name = $('#name').val();
            if (name === "") {
                valid = false;
                $('#name').parent().find('.inputError').addClass("show");
            }

            var password = $('#password').val();
            var password_confirmation = $('#password_confirmation').val();
            var company_phone = $('#companyPhone').val();
            var mailing_street = $('#street_address').val();
            var city = $('#city').val();
            var state = $('#state').val();
            var zip = $('#zipcode').val();

            //validate here
            return valid;
        }
    });
/*
    $("input").on("keyup", function(){
        console.log('triggering');
        if($("input:empty").length === 0){
            console.log(true);
        }else{
            console.log(false);
        }
    });
    */

    function checkMatchWithPayment(isCardInfoComplete){
        var company_name = $('#companyName').val();
        var name = $('#name').val();     
        var company_phone = $('#companyPhone').val();
        var mailing_street = $('#street_address').val();
        var city = $('#city').val();
        var state = $('#state').val();
        var zip = $('#zipcode').val();
        var password = document.getElementById("password");
        var password_confirmation = document.getElementById("password_confirmation");
        var submitBtn = document.getElementById("submitNewClientWithPayment"); 
        var fillInBtn = document.getElementById("fillInBtn");
        var cardName =  $('#updateCardName').val();
        var tosCheckBox = document.getElementById("tos");
        
        if(password.value == "" && password_confirmation.value == ""){
            password.className = '';
            password_confirmation.className = '';
        }
        else if(password.value == password_confirmation.value && password.value.length >= 8){
            password.className = 'match';
            password_confirmation.className = 'match';
        }
        else{
            password.className = 'nomatch';
            password_confirmation.className = 'nomatch';
        }
            
        if(password.value == password_confirmation.value && password.value.length >= 8 && company_name != "" && name != "" && company_phone.length >= 14 && mailing_street != "" && city != "" && state != "---" && zip != "" && cardName != "" && isCardInfoComplete == true && tosCheckBox.checked){
            submitBtn.style = "display:inline";
            fillInBtn.style = "display:none";
        }
        else{
            submitBtn.style = "display:none";
            fillInBtn.style = "display:inline";
        }   
    }

    function checkMatch(){
        console.log('in check match');
        var company_name = $('#companyName').val();
        var name = $('#name').val();     
        var company_phone = $('#companyPhone').val();
        var mailing_street = $('#street_address').val();
        var city = $('#city').val();
        var state = $('#state').val();
        var zip = $('#zipcode').val();
        var tos = $('#tos');
        var password = document.getElementById("password");
        var password_confirmation = document.getElementById("password_confirmation");
        var submitBtn = document.getElementById("submitNewClient");   
        
        if(password.value == "" && password_confirmation.value == ""){
            password.className = '';
            password_confirmation.className = '';
        }
        else if(password.value == password_confirmation.value && password.value.length >= 8){
            password.className = 'match';
            password_confirmation.className = 'match';
        }
        else{
            password.className = 'nomatch';
            password_confirmation.className = 'nomatch';
        }

        if(password.value == password_confirmation.value && password.value.length >= 8 && company_name != "" && name != "" && company_phone.length >= 14 && mailing_street != "" && city != "" && state != "---" && zip != "" && tos.is(":checked")){
            //submitBtn.style = "display:inline";
            submitBtn.classList.remove('red');
            submitBtn.classList.add('cyan');
            submitBtn.innerHTML = "Submit Registration";
        }
        else{
            if(submitBtn.classList.contains('cyan')){
                submitBtn.classList.remove('cyan');
                submitBtn.classList.add('red');
                submitBtn.innerHTML = "please fill in all fields first";
            }
        }   
    }

</script>

@endsection    
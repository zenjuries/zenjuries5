@extends('layouts.zen5_layout_simple')
@section('maincontent')


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {!! csrf_field() !!}

                        <!-- E-Mail Address -->
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        {{ $errors->first('email') }}
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- Send Password Reset Link Button -->
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-envelope"></i>Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<script>


/*

<div>RESET PASSWORD</div>
<div><input id="resetPasswordUserEmail"></div>
<div><button id="resetPasswordBtn">reset password</button></div>


    $('#resetPasswordUserEmail').on('keyup', function(event){
        var key = event.which;
        if(key == 13){
            $('#resetPasswordBtn').click();
        }
    });

    $('#resetPasswordBtn').on('click', function(){
        var userEmail = $("#resetPasswordUserEmail").val();

        if(userEmail != ""){
            console.log("check if email is valid/exists in system and then")
            console.log("Reset this email:    " + userEmail);
        }
    });
*/    
//submit the form by pressing enter while focus is on either field
//$('#zenployeesLoginForm').on('keyup', function(event){
    //get the key code for the key pressed, 13 is the code for the enter key
//    var key = event.which;
//    if(key == 13){
//        $('#submitLogin').click();
//    }
//});
</script>
@endsection
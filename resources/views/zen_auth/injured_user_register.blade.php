@extends('layouts.zen5_layout_guest')
@section('maincontent')
    <?php
	    
            if(isset($_GET['invitation'])){
                $invitation = \App\Invitation::where('token', $_GET['invitation'])->first();
            }else $invitation = NULL;

/*
            $invite_repo = new \App\Repositories\InvitationRepository();
            $invite_repo->inviteNewUser("Buck Allen", "ch+buckallen@zenjuries.com", 43, 1, "agent");
*//*
            $user = \App\User::where('email', 'ch+buckallen@zenjuries.com')->first();
            $user->setRandomAvatar();*/
    //App::make('files')->link(storage_path('app/public'), public_path('storage'));

    //arrays for error popup [icon image, error description, error code, button color, button ID, button icon, button label]
    $error1 = array("uhoh", "error message here", "zen233", "red", "dismiss", "exclamation-circle", "dismiss");
    $errormessage = $error1;
    //arrays for wait spinner overlay [spinner color, center message, top message, lower message]
    $waitspinner1 = array("blue", "one<br>moment", " ", "registering...");
    $spinnercontent = $waitspinner1; 
    ?>

<div class="pageContent bgimage-bgheader pagebackground18 fade-in">		
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                    <span>zenjuries <b>registration</b> form</span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="/images/icons/edit.png"></span><span class="subContent">register for <b>zenjuries</b></span>
                    </div>
            </div>
            <div class="pageIcon animate__animated animate__bounceInRight">
                <img src="/images/icons/userinfo.png">
            </div> 
        </div>           
    </div>

    <div style="height:10px;"></div>
    <div class="contentBlock noPad">
        <div class="container noPad">
        @if($invitation == NULL)
            <h1>Invalid Token</h1>
            <br><br>
            <p class="center">Sorry!  Either your registration token is invalid, or you have already registered.<br><br>If you have any questions please contact support@zenjuries.com.</p><br><br>
        @else
            <section class="sectionPanel">
            <div class="zen-actionPanel noHover transparent tight">                
                <div class="sectionContent">
                    <div class="center" style="padding:20px 0;">Every field is required.  Fill out all information, and click the submit registration button below.</div>
                    <section class="formBlock dark">                          
                        <div class="center" style="padding:20px 0;">Passsword requirement is minimum 8 characters.  Please create secure passwords!</div>
                        <div class="formGrid">  
                            <div class="formInput" style="height:160px;">
                                <!-- input -->
                                <label for="password">password</label>
                                <div class="inputIcon password"><input id="password" type="password" style="width:200px;"/></div>
                                <span class="inputError">required field</span>
                                <!-- input -->
                                <label for="password_confirmation">confirm password</label>
                                <div class="inputIcon password"><input id="password_confirmation" type="password" required style="width:200px;" /></div>
                                <span class="inputError">required field</span>
                                <input id="invitationToken" type="hidden" value="{{$invitation->token}}">
                                <input id="invitationEmail" type="hidden" value="{{$invitation->email}}">
                            </div>
                        </div>
                        <br><br><hr> 
                        <div class="formGrid short">
                            <div class="formInput">
                                <input type="checkbox" name="tos" id="tos">
                                <label for="tos">I agree to the <a href="#" class="showZenModal" data-modalcontent="#tosModal" style="color:cyan;">terms of service</a>.</label>
                            </div>
                        </div>
                        <br><br><hr> 
                        <div class="formGrid"> 
                            <div class="buttonArray" >
                                <button id="fillInBtn" class="red centered">please fill in all fields first</button>
                                <button id="submitInjuredUser" class="green centered" required style="display:none;">Submit Registration</button>
                            </div>
                        </div>
                    </section>
                    <div style="height:40px;"></div>
                </div>
            </div>
            </section>

        @endif
        </div>
    </div>
</div> 

<div style="height:160px;">
        <div class="buttonCloud small devButtons">
        <button class="showZenModalLocked" data-modalcontent="#waitspinnerModal"><div class="icon icon-magic"></div> register in progress---</button>
        <button class="showZenModal" data-modalcontent="#errorModal"><div class="icon icon-magic"></div> error---</button>
        <button class="showZenModal" data-modalcontent="#registerSuccess"><div class="icon icon-magic"></div> success---</button>
        </div>
    </div>

<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
	@include('partials.modals.tos')
    @include('partials.modals.waitOverlay') 
	@include('partials.modals.errorModal')
	@include('partials.modals.registerSuccess')      
</div>    
<script>
    $('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>
<script>
    $('.showZenModalLocked').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true,
            closeButton: false,
            closeOnClick: false
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>

    <script>
         $(document).ready(function() {
            $('#password').on('input', function(){
                checkMatch();
            })
            $('#password_confirmation').on('input', function(){
                checkMatch();
            })
            $('#tos').on('input', function(){
                checkMatch();
            });
            $('#eulaAgree').on('click', function(){
                var tosCheckBox = document.getElementById("tos");
                tosCheckBox.checked = true;
                checkMatch();
                modal.close();
            });
        });
        function checkMatch(){
            var passwordVar = document.getElementById("password");
            var passwordConfirmVar = document.getElementById("password_confirmation");
            var submitBtn = document.getElementById("submitInjuredUser");
            var fillInBtn = document.getElementById("fillInBtn");
            var tosCheckBox = document.getElementById("tos");
            if(passwordVar.value == "" && passwordConfirmVar.value == ""){
                passwordVar.className = '';
                passwordConfirmVar.className = '';
            }
            else if(passwordVar.value == passwordConfirmVar.value && passwordVar.value.length >= 8){
                passwordVar.className = 'match';
                passwordConfirmVar.className = 'match';
            }
            else{
                passwordVar.className = 'nomatch';
                passwordConfirmVar.className = 'nomatch';
            }

            if(passwordVar.value == passwordConfirmVar.value && passwordVar.value.length >= 8 && tosCheckBox.checked){
                submitBtn.style = "display:inline";
                fillInBtn.style = "display:none";
            }
            else{
                submitBtn.style = "display:none";
                fillInBtn.style = "display:inline";
            }
        }
        $('#submitInjuredUser').on('click', function(){
           $('button[data-modalcontent="#waitspinnerModal"]').click();
           var password = $('#password').val();
           var password_confirmation = $('#password_confirmation').val();
           var invitation_token = $('#invitationToken').val();
           var email = $('#invitationEmail').val();
           $.ajax({
               type: 'POST',
               url: '<?php echo route('registerInjuredUser'); ?>',
               data: {
                   _token: '<?php echo csrf_token(); ?>',
                   email: email,
                   password: password,
                   password_confirmation: password_confirmation,
                   token: invitation_token
               },
               success: function (data) {
                    modal.close();
                    $('button[data-modalcontent="#registerSuccess"]').click();
                    setTimeout(() => {
                        window.location.replace('/');
                    }, 5000);
               },
               error: function (data){
                    console.log(data);
                    modal.close();
                    $('#errorCode').html(data['responseJSON']['errors']['message']);
                    $('button[data-modalcontent="#errorModal"]').click();
               }
           });
        });
    </script>
@endsection    
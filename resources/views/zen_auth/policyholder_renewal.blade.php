@extends('layouts.zen5_layout_guest')
@section('maincontent')
<?php
    $company = new \App\Company();
    $intent = $company->createSetupIntent();

    //arrays for error popup [icon image, error description, error code, button color, button ID, button icon, button label]
    $error1 = array("uhoh", "something went wrong", "zen233", "red", "dismiss", "exclamation-circle", "dismiss");
    $errormessage = $error1;
    //arrays for wait spinner overlay [spinner color, center message, top message, lower message]
    $waitspinner1 = array("green", "one<br>moment", " ", "registering...");
    $spinnercontent = $waitspinner1;     
?>

<script src="https://js.stripe.com/v3/"></script>

<div class="pageContent bgimage-bgheader pagebackground16 fade-in">		
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                    <span><b>zenjuries</b> renewal</span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="{{Auth::user()->getCompany()->logo_location ?? ''}}" style="border-radius:3px;"></span><span class="subContent">{{Auth::user()->getCompany()->company_name}}</span>
                    </div>
            </div>
            <div class="pageIcon animate__animated animate__bounceInRight">
                <img src="/images/icons/insurance.png">
            </div> 
        </div>           
    </div>

    <div style="height:10px;"></div>
    <div class="contentBlock noPad">
        <div class="container noPad">

            <section class="sectionPanel">
                <div class="zen-actionPanel noHover transparent tight">                
                    <div class="sectionContent">
                        
                        <div style="margin-bottom:30px;padding-bottom:40px;border-bottom:1px solid #ffffff50;color:#aaa;">
                        <span style="font-size:1.2rem;display:block;margin-bottom:6px;">This zenjuries plan is being paid for by <span class="FG__white">[agency or policyholder name here]</span>.</span>
                        <span style="font-size:1.2rem;display:block;margin-bottom:6px;">The upcoming plan will be charged using the payment method on record, which is <span class="FG__yellow">[payment method description]</span>.</span>
                        <br>
                        If you would like to change this payment method, click the <i>"change my billing method"</i> selector above the submission button.  You will be taken to a screen where you can update your payment method before you are charged.
                        <br><br>
                        <i>Every field is required.</i>  Check current information, fill out any new information, upload your new policy, and click the submit registration button below.
                        </div>

                        <section class="formBlock dark">
                            <div class="formGrid">
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="contactName">Contact Name</label>
                                    <div class="inputIcon user"><input id="contactName" type="text" /></div>
                                    <span class="inputError">required</span>
                                </div>
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="contactEmail">Contact Email</label>
                                    <div class="inputIcon mail"><input id="contactEmail" type="text" /></div>
                                    <span class="inputError">required</span>
                                </div>
                            </div>

                            <div class="formElement center borders">
                                <div class="formInput" style="text-align:center;">
                                    <label for="claimNumberInput">Policy Renewal Mo/Day</label>
                                    <div class="inputIcon month" style="display:inline-block;">
                                        <select name="renewalMonth" id="renewalMonth" class="monthInput">                
                                            <option value="select">select</option>
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>
                                            <option value="4">April</option>
                                            <option value="5">May</option>
                                            <option value="6">June</option>
                                            <option value="7">July</option>
                                            <option value="8">August</option>
                                            <option value="9">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select> 
                                    </div>
                                    <div class="inputIcon day" style="display:inline-block;"><input type="text" id="renewalDay" name="renewalDay" style="width:60px;"></div>
                                    <span class="inputError">enter date</span>
                                </div>
                            </div>    
                            <div class="formElement center borders">
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="premium">Annual Premium</label>
                                    <div class="inputIcon cost"><input  id="premium" placeholder="in dollars" type="text"/></div>
                                    <span class="inputError">required (must be a number)</span>
                                </div>
                            </div> 
                            <div class="formElement center borders">
                                <div class="formInput">
                                    <!-- input -->
                                    <label for="upload">Upload Work Comp Policy</label>
                                    <input  id="file" type="file"/>
                                    <span class="inputError">required</span>
                                </div>
                                <div class="formInput">
                                    <input type="checkbox" name="uploadLater">
                                    <label class="small" for="uploadLater">I will upload my policy later.</label>
                                    <div class="FG__orange" style="font-size:.8rem;padding:12px 0;"> I understand I will have limited system access until policy is uploaded.</div>
                                </div>
                            </div>
                            <div class="formElement center borders">
                                <div class="formInput">
                                    <input type="radio" name="myBilling" id="billingOK" checked="checked">
                                    <label class="small" for="whoPaysButton">Current method is fine</label>
                                    <input type="radio" name="myBilling" id="billingChange">
                                    <label class="small" for="whoPaysButton">Change my billing method</label>
                                </div>
                                <br>
                                <div class="divCenter">Current payment method: [payment method description].</div>
                                <div class="divCenter FG__yellow">On submission, you will be taken to a billing update screen.</div> 
                                <br>
                                <div class="buttonArray">
                                    <button class="cyan centered hideMe" id="submitNewClient"><div class="icon icon-user"></div> renew registration</button>
                                    <button class="red centered" id="incomplete"><div class="icon icon-alert"></div> please complete all fields</button>
                                </div>                               
                            </div>

                                                   
                        </section>    
                    </div>
                </div>
            </section>


        <div style="height:160px;">
            <div class="buttonCloud small devButtons">
                <button class="showZenModal" data-modalcontent="#errorModal">error</button>
                <button class="showZenModal" data-modalcontent="#registerSuccess">success</button>
			</div>
		</div>

        </div>
    </div>
</div>

<div style="height:160px;">
    <div class="buttonCloud small devButtons">
    <button class="showZenModalLocked" data-modalcontent="#waitspinnerModal"><div class="icon icon-magic"></div> register in progress---</button>
    <button class="showZenModal" data-modalcontent="#errorModal"><div class="icon icon-magic"></div> error---</button>
    <button class="showZenModal" data-modalcontent="#registerSuccess"><div class="icon icon-magic"></div> success---</button>
    </div>
</div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
	@include('partials.modals.tos')
    @include('partials.modals.waitOverlay') 
	@include('partials.modals.errorModal')
	@include('partials.modals.registerSuccess')      
</div>    
<script>
    $('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>
<script>
    $('.showZenModalLocked').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true,
            closeButton: false,
            closeOnClick: false
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>






@endsection
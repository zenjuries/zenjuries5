@extends('layouts.zen5_layout')
@section('maincontent')
<?php
$user = Auth::user();
if(!isset($user)){
    $user = \App\User::where('id', 4500)->first();
}
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.5.1/jspdf.umd.min.js" integrity="sha512-qZvrmS2ekKPF2mSznTQsxqPgnpkI4DNTlrdUmTzrDgektczlKNRRhy5X5AAOnx5S09ydFYWWNSfcEqDTTHgtNA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<div class="pageContent bgimage-bgheader bghaze-smoke fade-in">
    <!--***********-->		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                    <span>Test page for mobile calls </span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/magnify.png"></span><span id="companyName" class="subContent">Text Here</span>
                        
                </div>
                
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/stats.png">
            </div> 
        </div>           
	</div>

    <div class="contentBlock lessPadding">
		<div class="container setMinHeight">
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> Save Device Id</span>
                <div class="sectionDescription">Send and save a test user email and device id</div> 
                <div class="sectionContent">
                    <ul class="sectionInfoPanel">
                        <li><span class="label">Device Id:</span> <input type="text" class="data" id="testDeviceId"></input></li>
                        <li><span class="label">Email:</span> <input type="text" class="data" id="testEmail"></input></li>
                    </ul>
                    
                    <br>
                    <div class="buttonArray">
                        <button class="showZenModal" id="runSaveId"><div class="icon icon-lock"></div> save</button>
                    </div>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> Send Notification</span>
                <div class="sectionDescription">Send Notification with button push</div> 
                <div class="sectionContent">
                    <div class="buttonArray">
                        <button class="showZenModal" id="sendPush"><div class="icon icon-lock"></div> Push</button>
                        <button id="multiuserSendPush"><div class="icon icon-lock"></div> Multiuser Push</button>
                        <button id="sendRoger"><div class="icon icon-lock"></div> Send to Roger</button>
                        <button id='sendJesse'><div class="icon icon-lock"></div> Send to Jesse</button>
                    </div>
                </div>
            </section>

                                                                               
        </div>
	</div>
</div>
<script>
$('#runSaveId').on('click', function(){
    var id = $('#testDeviceId').val();
    var email = $('#testEmail').val();

    if(id != "" && email != ''){
        $.ajax({
            url: '<?php echo route('saveDeviceToken') ?>',
            type: 'POST',
            data:{
                deviceToken: id,
                userEmail: email,
                _token: "<?php echo csrf_token(); ?>"
                },
            success: function(data){
                console.log('success');
            },
            error: function(data){
                console.log('error');
            }
        });
    }else{
        console.log('id: ' + id);
        console.log('email: ' + email);
    }
});
$('#sendPush').on('click', function(){
    $.ajax({
        url: '<?php echo route('manualPushTrigger')?>',
        type: 'POST',
        data:{
            id: {{Auth::user()->id}},
            _token: "<?php echo csrf_token(); ?>"
        },
        success: function(data){
            console.log('success');
        },
        error: function(data){
            console.log('error');
        }
    });
});
$('#multiuserSendPush').on('click', function(){
    $.ajax({
        url: '<?php echo route('multiUserPushTrigger')?>',
        type: 'POST',
        data:{
            id: {{Auth::user()->id}},
            _token: "<?php echo csrf_token(); ?>"
        },
        success: function(data){
            console.log('success');
        },
        error: function(data){
            console.log('error');
        }
    });
});
$('#sendRoger').on('click', function(){
    $.ajax({
        url: '<?php echo route('RogerPushNotification')?>',
        type: 'POST',
        data:{
            id: {{Auth::user()->id}},
            _token: "<?php echo csrf_token(); ?>"
        },
        success: function(data){
            console.log('success');
        },
        error: function(data){
            console.log('error');
        }
    });
});
$('#sendJesse').on('click', function(){
    $.ajax({
        url: '<?php echo route('JessePushNotification') ?>',
        type: 'POST',
        data:{
            id: {{Auth::user()->id}},
            _token: "<?php echo csrf_token(); ?>"
        },
        success: function(data){
            console.log('success');
        },
        error: function(data){
            console.log('error');
        }
    });
})
</script>
@endsection
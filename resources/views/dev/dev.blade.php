@extends('layouts.zen5_layout')
@section('maincontent')
	
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="https://www.zenployees.com/js/parallax.js"></script>

<div class="pageContent bgimage-bglb fade-in" style="background-image:url('/images/splash/redleaf.jpg');">
		<!--**********-->
	<div class="headerBlock headerFade">
		<div class="headerImage wow animate__animated animate__bounceInRight">
			<div class="headerImage__panel-small zenTheme-ZB-overlay">
				<div class="userAvatar" style="background-image:url(xx)"></div>
				<div class="userFirstname">xx</div>
				<div class="userLastname">xx</div>
				<div class="userStatus">Status: <span>xx</span></div>
			</div>
		</div>
		<div class="container">
			<div class="pageImageHeader wow animate__animated animate__bounceInLeft">
				<div class="pageImageHeader__title txt__white">
					<span>Welcome to Zenjuries</span>
				</div>
				<div class="pageImageHeader__subTitle txt__white zenTheme-fg1">
					<span>Hi,x What would you like to do?</span>
				</div>
			</div>
		</div>
	</div>

	<div class="contentBlock txt__black">
		<div class="container">
			<div class="homeButtons">
				<div class="itemGrid animate__animated animate__bounceIn">			
					<div class="itemGrid__nav"><a class="navbbg-1 invisible" href="/zengarden/diary"><span class="navImg"><img src="https://www.zenployees.com/images/clipart/diary-o.png"></span><span class="subText">report new</span><span class="mainText">zenjury</span></a></div>
					<div class="itemGrid__nav"><a class="navbbg-2 invisible" href="/zengarden/schedule"><span class="navImg"><img src="https://www.zenployees.com/images/clipart/schedule-o.png"></span><span class="subText">view my</span><span class="mainText">messages</span></a></div>
					<div class="itemGrid__nav"><a class="navbbg-3 invisible" href="/zengarden/team"><span class="navImg"><img src="https://www.zenployees.com/images/clipart/medical-o.png"></span><span class="subText">unresolved</span><span class="mainText">injuries</span></a></div>														
					<div class="itemGrid__nav"><a class="navbbg-4 invisible" href="/zengarden/myinfo"><span class="navImg"><img src="https://www.zenployees.com/images/clipart/myinfo-o.png"></span><span class="subText">zenMetrics</span><span class="mainText">reports</span></a></div>
					<div class="itemGrid__nav"><a class="navbbg-5 invisible" href="/zengarden/settings"><span class="navImg"><img src="https://www.zenployees.com/images/clipart/tools-o.png"></span><span class="subText">help &</span><span class="mainText">support</span></a></div>
					<div class="itemGrid__nav"><a class="navbbg-6 invisible" href="/zengarden/faq"><span class="navImg"><img src="https://www.zenployees.com/images/clipart/help-o.png"></span><span class="subText">view your</span><span class="mainText">documents</span></a></div>												
				</div>
			</div>			
		</div>
	</div>

<div class="progressMeter status6">
<svg viewBox="0 0 100 100">
  <circle r="50%" cx="50%" cy="50%"></circle>
  <div class="overlay"><div class="avatar"></div></div>
</svg>
</div>
<br>
<div class="progressMeter small inline alert status3">
<svg viewBox="0 0 100 100">
  <circle r="50%" cx="50%" cy="50%"></circle>
  <div class="overlay"><div class="avatar"></div></div>
</svg>
</div>
<div class="progressMeter small inline critical status5">
<svg viewBox="0 0 100 100">
  <circle r="50%" cx="50%" cy="50%"></circle>
  <div class="overlay"><div class="avatar"></div></div>
</svg>
</div>
<div class="progressMeter small inline status7">
<svg viewBox="0 0 100 100">
  <circle r="50%" cx="50%" cy="50%"></circle>
  <div class="overlay"><div class="avatar"></div></div>
</svg>
</div>
</div>

	


@endsection

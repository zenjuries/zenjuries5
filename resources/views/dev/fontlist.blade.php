@extends('layouts.zen5_layout_simple')
@section('maincontent')
<style type="text/css">
    body{background-color:black;}
.container{margin:15px auto;width:80%;background-color:#777777;border-radius:15px !important;}
ul.css-mapping{background-color:#303030;border-radius:15px !important;margin-top:20px !important;border:0 !important;}
ul.character-mapping{background-color:#303030;border-radius:15px !important;margin-top:20px !important;border:0 !important;}
.glyphs.character-mapping li{margin:0 30px 20px 0;display:inline-block;width:90px}
.glyphs.character-mapping .icon{margin:10px 0 10px 15px;padding:15px;position:relative;width:65px;height:65px;color:#efefef !important;overflow:hidden;-webkit-border-radius:3px;border-radius:3px;font-size:42px;}
.glyphs.character-mapping .icon svg{fill:#fff}
.glyphs.character-mapping input{margin:0;padding:2px 0;line-height:12px;font-size:12px;display:block;width:100%;border:1px solid #606060 !important;-webkit-border-radius:5px;border-radius:5px;text-align:center;outline:0;color:white;background-color:black;}
.glyphs.character-mapping input:focus{border:1px solid #fbde4a;-webkit-box-shadow:inset 0 0 3px #fbde4a;box-shadow:inset 0 0 3px #fbde4a}
.glyphs.character-mapping input:hover{-webkit-box-shadow:inset 0 0 3px #fbde4a;box-shadow:inset 0 0 3px #fbde4a}
.glyphs.css-mapping{margin:0 0 60px 0;padding:30px 0 20px 30px;color:rgba(0,0,0,0.5);border:1px solid #d8e0e5;-webkit-border-radius:3px;border-radius:3px;}
.glyphs.css-mapping li{margin:0 30px 20px 0;padding:0;display:inline-block;overflow:hidden}
.glyphs.css-mapping .icon{margin:0;margin-right:10px;padding:13px;height:50px;width:50px;color:#efefef !important;overflow:hidden;float:left;font-size:24px}
.glyphs.css-mapping input{margin:0;margin-top:5px;padding:4px;line-height:14px;font-size:14px;display:block;width:150px;height:40px;border:1px solid #606060 !important;-webkit-border-radius:5px;border-radius:5px;color:white;background-color:black;outline:0;float:right;}
.glyphs.css-mapping input:focus{border:1px solid #fbde4a;-webkit-box-shadow:inset 0 0 3px #fbde4a;box-shadow:inset 0 0 3px #fbde4a}.glyphs.css-mapping input:hover{-webkit-box-shadow:inset 0 0 3px #fbde4a;box-shadow:inset 0 0 3px #fbde4a}
</style>
<div class="center"><img src="/branding/default/images/logo_stacked_dark.png" width="180px"></div>
<h2>zenjuriesfont mapping</h2>
<fieldset class="thin"><legend>CSS mapping</legend>
<ul class="glyphs css-mapping">
        <li>
          <div class="icon icon-arrow-down"></div>
          <input type="text" readonly="readonly" value="arrow-down">
        </li>
        <li>
          <div class="icon icon-arrow-left"></div>
          <input type="text" readonly="readonly" value="arrow-left">
        </li>
        <li>
          <div class="icon icon-arrow-right"></div>
          <input type="text" readonly="readonly" value="arrow-right">
        </li>
        <li>
          <div class="icon icon-arrow-up"></div>
          <input type="text" readonly="readonly" value="arrow-up">
        </li>
        <li>
          <div class="icon icon-bar-chart"></div>
          <input type="text" readonly="readonly" value="bar-chart">
        </li>
        <li>
          <div class="icon icon-bed"></div>
          <input type="text" readonly="readonly" value="bed">
        </li>
        <li>
          <div class="icon icon-bell"></div>
          <input type="text" readonly="readonly" value="bell">
        </li>
        <li>
          <div class="icon icon-bell-o"></div>
          <input type="text" readonly="readonly" value="bell-o">
        </li>
        <li>
          <div class="icon icon-bell-slash"></div>
          <input type="text" readonly="readonly" value="bell-slash">
        </li>
        <li>
          <div class="icon icon-bell-slash-o"></div>
          <input type="text" readonly="readonly" value="bell-slash-o">
        </li>
        <li>
          <div class="icon icon-book"></div>
          <input type="text" readonly="readonly" value="book">
        </li>
        <li>
          <div class="icon icon-bookmark"></div>
          <input type="text" readonly="readonly" value="bookmark">
        </li>
        <li>
          <div class="icon icon-bookmark-o"></div>
          <input type="text" readonly="readonly" value="bookmark-o">
        </li>
        <li>
          <div class="icon icon-briefcase"></div>
          <input type="text" readonly="readonly" value="briefcase">
        </li>
        <li>
          <div class="icon icon-bug"></div>
          <input type="text" readonly="readonly" value="bug">
        </li>
        <li>
          <div class="icon icon-calculator"></div>
          <input type="text" readonly="readonly" value="calculator">
        </li>
        <li>
          <div class="icon icon-calendar"></div>
          <input type="text" readonly="readonly" value="calendar">
        </li>
        <li>
          <div class="icon icon-calendar-check-o"></div>
          <input type="text" readonly="readonly" value="calendar-check-o">
        </li>
        <li>
          <div class="icon icon-calendar-minus-o"></div>
          <input type="text" readonly="readonly" value="calendar-minus-o">
        </li>
        <li>
          <div class="icon icon-calendar-o"></div>
          <input type="text" readonly="readonly" value="calendar-o">
        </li>
        <li>
          <div class="icon icon-calendar-plus-o"></div>
          <input type="text" readonly="readonly" value="calendar-plus-o">
        </li>
        <li>
          <div class="icon icon-calendar-times-o"></div>
          <input type="text" readonly="readonly" value="calendar-times-o">
        </li>
        <li>
          <div class="icon icon-camera"></div>
          <input type="text" readonly="readonly" value="camera">
        </li>
        <li>
          <div class="icon icon-certificate"></div>
          <input type="text" readonly="readonly" value="certificate">
        </li>
        <li>
          <div class="icon icon-check"></div>
          <input type="text" readonly="readonly" value="check">
        </li>
        <li>
          <div class="icon icon-check-circle"></div>
          <input type="text" readonly="readonly" value="check-circle">
        </li>
        <li>
          <div class="icon icon-circle"></div>
          <input type="text" readonly="readonly" value="circle">
        </li>
        <li>
          <div class="icon icon-circle-o"></div>
          <input type="text" readonly="readonly" value="circle-o">
        </li>
        <li>
          <div class="icon icon-clipboard"></div>
          <input type="text" readonly="readonly" value="clipboard">
        </li>
        <li>
          <div class="icon icon-cloud"></div>
          <input type="text" readonly="readonly" value="cloud">
        </li>
        <li>
          <div class="icon icon-cloud-download"></div>
          <input type="text" readonly="readonly" value="cloud-download">
        </li>
        <li>
          <div class="icon icon-cloud-upload"></div>
          <input type="text" readonly="readonly" value="cloud-upload">
        </li>
        <li>
          <div class="icon icon-cog"></div>
          <input type="text" readonly="readonly" value="cog">
        </li>
        <li>
          <div class="icon icon-cogs"></div>
          <input type="text" readonly="readonly" value="cogs">
        </li>
        <li>
          <div class="icon icon-columns"></div>
          <input type="text" readonly="readonly" value="columns">
        </li>
        <li>
          <div class="icon icon-comment"></div>
          <input type="text" readonly="readonly" value="comment">
        </li>
        <li>
          <div class="icon icon-comment-o"></div>
          <input type="text" readonly="readonly" value="comment-o">
        </li>
        <li>
          <div class="icon icon-commenting"></div>
          <input type="text" readonly="readonly" value="commenting">
        </li>
        <li>
          <div class="icon icon-commenting-o"></div>
          <input type="text" readonly="readonly" value="commenting-o">
        </li>
        <li>
          <div class="icon icon-comments"></div>
          <input type="text" readonly="readonly" value="comments">
        </li>
        <li>
          <div class="icon icon-comments-o"></div>
          <input type="text" readonly="readonly" value="comments-o">
        </li>
        <li>
          <div class="icon icon-compass"></div>
          <input type="text" readonly="readonly" value="compass">
        </li>
        <li>
          <div class="icon icon-cube"></div>
          <input type="text" readonly="readonly" value="cube">
        </li>
        <li>
          <div class="icon icon-cubes"></div>
          <input type="text" readonly="readonly" value="cubes">
        </li>
        <li>
          <div class="icon icon-database"></div>
          <input type="text" readonly="readonly" value="database">
        </li>
        <li>
          <div class="icon icon-desktop"></div>
          <input type="text" readonly="readonly" value="desktop">
        </li>
        <li>
          <div class="icon icon-download"></div>
          <input type="text" readonly="readonly" value="download">
        </li>
        <li>
          <div class="icon icon-dropbox"></div>
          <input type="text" readonly="readonly" value="dropbox">
        </li>
        <li>
          <div class="icon icon-ellipsis-h"></div>
          <input type="text" readonly="readonly" value="ellipsis-h">
        </li>
        <li>
          <div class="icon icon-ellipsis-v"></div>
          <input type="text" readonly="readonly" value="ellipsis-v">
        </li>
        <li>
          <div class="icon icon-envelope"></div>
          <input type="text" readonly="readonly" value="envelope">
        </li>
        <li>
          <div class="icon icon-envelope-o"></div>
          <input type="text" readonly="readonly" value="envelope-o">
        </li>
        <li>
          <div class="icon icon-eraser"></div>
          <input type="text" readonly="readonly" value="eraser">
        </li>
        <li>
          <div class="icon icon-exclamation-circle"></div>
          <input type="text" readonly="readonly" value="exclamation-circle">
        </li>
        <li>
          <div class="icon icon-exclamation-triangle"></div>
          <input type="text" readonly="readonly" value="exclamation-triangle">
        </li>
        <li>
          <div class="icon icon-eyedropper"></div>
          <input type="text" readonly="readonly" value="eyedropper">
        </li>
        <li>
          <div class="icon icon-facebook-square"></div>
          <input type="text" readonly="readonly" value="facebook-square">
        </li>
        <li>
          <div class="icon icon-file"></div>
          <input type="text" readonly="readonly" value="file">
        </li>
        <li>
          <div class="icon icon-file-image-o"></div>
          <input type="text" readonly="readonly" value="file-image-o">
        </li>
        <li>
          <div class="icon icon-file-o"></div>
          <input type="text" readonly="readonly" value="file-o">
        </li>
        <li>
          <div class="icon icon-file-pdf-o"></div>
          <input type="text" readonly="readonly" value="file-pdf-o">
        </li>
        <li>
          <div class="icon icon-file-text"></div>
          <input type="text" readonly="readonly" value="file-text">
        </li>
        <li>
          <div class="icon icon-file-text-o"></div>
          <input type="text" readonly="readonly" value="file-text-o">
        </li>
        <li>
          <div class="icon icon-file-video-o"></div>
          <input type="text" readonly="readonly" value="file-video-o">
        </li>
        <li>
          <div class="icon icon-files-o"></div>
          <input type="text" readonly="readonly" value="files-o">
        </li>
        <li>
          <div class="icon icon-film"></div>
          <input type="text" readonly="readonly" value="film">
        </li>
        <li>
          <div class="icon icon-filter"></div>
          <input type="text" readonly="readonly" value="filter">
        </li>
        <li>
          <div class="icon icon-floppy-o"></div>
          <input type="text" readonly="readonly" value="floppy-o">
        </li>
        <li>
          <div class="icon icon-globe"></div>
          <input type="text" readonly="readonly" value="globe">
        </li>
        <li>
          <div class="icon icon-heart"></div>
          <input type="text" readonly="readonly" value="heart">
        </li>
        <li>
          <div class="icon icon-heart-o"></div>
          <input type="text" readonly="readonly" value="heart-o">
        </li>
        <li>
          <div class="icon icon-heartbeat"></div>
          <input type="text" readonly="readonly" value="heartbeat">
        </li>
        <li>
          <div class="icon icon-home"></div>
          <input type="text" readonly="readonly" value="home">
        </li>
        <li>
          <div class="icon icon-info-circle"></div>
          <input type="text" readonly="readonly" value="info-circle">
        </li>
        <li>
          <div class="icon icon-laptop"></div>
          <input type="text" readonly="readonly" value="laptop">
        </li>
        <li>
          <div class="icon icon-life-ring"></div>
          <input type="text" readonly="readonly" value="life-ring">
        </li>
        <li>
          <div class="icon icon-lightbulb-o"></div>
          <input type="text" readonly="readonly" value="lightbulb-o">
        </li>
        <li>
          <div class="icon icon-linkedin-square"></div>
          <input type="text" readonly="readonly" value="linkedin-square">
        </li>
        <li>
          <div class="icon icon-list"></div>
          <input type="text" readonly="readonly" value="list">
        </li>
        <li>
          <div class="icon icon-location-arrow"></div>
          <input type="text" readonly="readonly" value="location-arrow">
        </li>
        <li>
          <div class="icon icon-lock"></div>
          <input type="text" readonly="readonly" value="lock">
        </li>
        <li>
          <div class="icon icon-map"></div>
          <input type="text" readonly="readonly" value="map">
        </li>
        <li>
          <div class="icon icon-map-marker"></div>
          <input type="text" readonly="readonly" value="map-marker">
        </li>
        <li>
          <div class="icon icon-map-pin"></div>
          <input type="text" readonly="readonly" value="map-pin">
        </li>
        <li>
          <div class="icon icon-map-signs"></div>
          <input type="text" readonly="readonly" value="map-signs">
        </li>
        <li>
          <div class="icon icon-medkit"></div>
          <input type="text" readonly="readonly" value="medkit">
        </li>
        <li>
          <div class="icon icon-minus-circle"></div>
          <input type="text" readonly="readonly" value="minus-circle">
        </li>
        <li>
          <div class="icon icon-mobile"></div>
          <input type="text" readonly="readonly" value="mobile">
        </li>
        <li>
          <div class="icon icon-picture-o"></div>
          <input type="text" readonly="readonly" value="picture-o">
        </li>
        <li>
          <div class="icon icon-phone-square"></div>
          <input type="text" readonly="readonly" value="phone-square">
        </li>
        <li>
          <div class="icon icon-phone"></div>
          <input type="text" readonly="readonly" value="phone">
        </li>
        <li>
          <div class="icon icon-pencil-square-o"></div>
          <input type="text" readonly="readonly" value="pencil-square-o">
        </li>
        <li>
          <div class="icon icon-pencil"></div>
          <input type="text" readonly="readonly" value="pencil">
        </li>
        <li>
          <div class="icon icon-pencil-square"></div>
          <input type="text" readonly="readonly" value="pencil-square">
        </li>
        <li>
          <div class="icon icon-paper-plane"></div>
          <input type="text" readonly="readonly" value="paper-plane">
        </li>
        <li>
          <div class="icon icon-paper-plane-o"></div>
          <input type="text" readonly="readonly" value="paper-plane-o">
        </li>
        <li>
          <div class="icon icon-pie-chart"></div>
          <input type="text" readonly="readonly" value="pie-chart">
        </li>
        <li>
          <div class="icon icon-play"></div>
          <input type="text" readonly="readonly" value="play">
        </li>
        <li>
          <div class="icon icon-play-circle"></div>
          <input type="text" readonly="readonly" value="play-circle">
        </li>
        <li>
          <div class="icon icon-plus-circle"></div>
          <input type="text" readonly="readonly" value="plus-circle">
        </li>
        <li>
          <div class="icon icon-plus"></div>
          <input type="text" readonly="readonly" value="plus">
        </li>
        <li>
          <div class="icon icon-minus"></div>
          <input type="text" readonly="readonly" value="minus">
        </li>
        <li>
          <div class="icon icon-power-off"></div>
          <input type="text" readonly="readonly" value="power-off">
        </li>
        <li>
          <div class="icon icon-question"></div>
          <input type="text" readonly="readonly" value="question">
        </li>
        <li>
          <div class="icon icon-question-circle"></div>
          <input type="text" readonly="readonly" value="question-circle">
        </li>
        <li>
          <div class="icon icon-repeat"></div>
          <input type="text" readonly="readonly" value="repeat">
        </li>
        <li>
          <div class="icon icon-reply"></div>
          <input type="text" readonly="readonly" value="reply">
        </li>
        <li>
          <div class="icon icon-reply-all"></div>
          <input type="text" readonly="readonly" value="reply-all">
        </li>
        <li>
          <div class="icon icon-retweet"></div>
          <input type="text" readonly="readonly" value="retweet">
        </li>
        <li>
          <div class="icon icon-rocket"></div>
          <input type="text" readonly="readonly" value="rocket">
        </li>
        <li>
          <div class="icon icon-search"></div>
          <input type="text" readonly="readonly" value="search">
        </li>
        <li>
          <div class="icon icon-search-minus"></div>
          <input type="text" readonly="readonly" value="search-minus">
        </li>
        <li>
          <div class="icon icon-search-plus"></div>
          <input type="text" readonly="readonly" value="search-plus">
        </li>
        <li>
          <div class="icon icon-server"></div>
          <input type="text" readonly="readonly" value="server">
        </li>
        <li>
          <div class="icon icon-share"></div>
          <input type="text" readonly="readonly" value="share">
        </li>
        <li>
          <div class="icon icon-share-alt"></div>
          <input type="text" readonly="readonly" value="share-alt">
        </li>
        <li>
          <div class="icon icon-sign-in"></div>
          <input type="text" readonly="readonly" value="sign-in">
        </li>
        <li>
          <div class="icon icon-sign-out"></div>
          <input type="text" readonly="readonly" value="sign-out">
        </li>
        <li>
          <div class="icon icon-sitemap"></div>
          <input type="text" readonly="readonly" value="sitemap">
        </li>
        <li>
          <div class="icon icon-sliders"></div>
          <input type="text" readonly="readonly" value="sliders">
        </li>
        <li>
          <div class="icon icon-sort-alpha-asc"></div>
          <input type="text" readonly="readonly" value="sort-alpha-asc">
        </li>
        <li>
          <div class="icon icon-sort-alpha-desc"></div>
          <input type="text" readonly="readonly" value="sort-alpha-desc">
        </li>
        <li>
          <div class="icon icon-sort-amount-asc"></div>
          <input type="text" readonly="readonly" value="sort-amount-asc">
        </li>
        <li>
          <div class="icon icon-sort-amount-desc"></div>
          <input type="text" readonly="readonly" value="sort-amount-desc">
        </li>
        <li>
          <div class="icon icon-spinner"></div>
          <input type="text" readonly="readonly" value="spinner">
        </li>
        <li>
          <div class="icon icon-square"></div>
          <input type="text" readonly="readonly" value="square">
        </li>
        <li>
          <div class="icon icon-square-o"></div>
          <input type="text" readonly="readonly" value="square-o">
        </li>
        <li>
          <div class="icon icon-star"></div>
          <input type="text" readonly="readonly" value="star">
        </li>
        <li>
          <div class="icon icon-star-half"></div>
          <input type="text" readonly="readonly" value="star-half">
        </li>
        <li>
          <div class="icon icon-star-half-o"></div>
          <input type="text" readonly="readonly" value="star-half-o">
        </li>
        <li>
          <div class="icon icon-star-o"></div>
          <input type="text" readonly="readonly" value="star-o">
        </li>
        <li>
          <div class="icon icon-sticky-note"></div>
          <input type="text" readonly="readonly" value="sticky-note">
        </li>
        <li>
          <div class="icon icon-sticky-note-o"></div>
          <input type="text" readonly="readonly" value="sticky-note-o">
        </li>
        <li>
          <div class="icon icon-stop"></div>
          <input type="text" readonly="readonly" value="stop">
        </li>
        <li>
          <div class="icon icon-street-view"></div>
          <input type="text" readonly="readonly" value="street-view">
        </li>
        <li>
          <div class="icon icon-suitcase"></div>
          <input type="text" readonly="readonly" value="suitcase">
        </li>
        <li>
          <div class="icon icon-sun-o"></div>
          <input type="text" readonly="readonly" value="sun-o">
        </li>
        <li>
          <div class="icon icon-table"></div>
          <input type="text" readonly="readonly" value="table">
        </li>
        <li>
          <div class="icon icon-tablet"></div>
          <input type="text" readonly="readonly" value="tablet">
        </li>
        <li>
          <div class="icon icon-tachometer"></div>
          <input type="text" readonly="readonly" value="tachometer">
        </li>
        <li>
          <div class="icon icon-tag"></div>
          <input type="text" readonly="readonly" value="tag">
        </li>
        <li>
          <div class="icon icon-tags"></div>
          <input type="text" readonly="readonly" value="tags">
        </li>
        <li>
          <div class="icon icon-tasks"></div>
          <input type="text" readonly="readonly" value="tasks">
        </li>
        <li>
          <div class="icon icon-television"></div>
          <input type="text" readonly="readonly" value="television">
        </li>
        <li>
          <div class="icon icon-th"></div>
          <input type="text" readonly="readonly" value="th">
        </li>
        <li>
          <div class="icon icon-th-large"></div>
          <input type="text" readonly="readonly" value="th-large">
        </li>
        <li>
          <div class="icon icon-th-list"></div>
          <input type="text" readonly="readonly" value="th-list">
        </li>
        <li>
          <div class="icon icon-thumb-tack"></div>
          <input type="text" readonly="readonly" value="thumb-tack">
        </li>
        <li>
          <div class="icon icon-thumbs-down"></div>
          <input type="text" readonly="readonly" value="thumbs-down">
        </li>
        <li>
          <div class="icon icon-thumbs-o-down"></div>
          <input type="text" readonly="readonly" value="thumbs-o-down">
        </li>
        <li>
          <div class="icon icon-thumbs-o-up"></div>
          <input type="text" readonly="readonly" value="thumbs-o-up">
        </li>
        <li>
          <div class="icon icon-thumbs-up"></div>
          <input type="text" readonly="readonly" value="thumbs-up">
        </li>
        <li>
          <div class="icon icon-times"></div>
          <input type="text" readonly="readonly" value="times">
        </li>
        <li>
          <div class="icon icon-times-circle"></div>
          <input type="text" readonly="readonly" value="times-circle">
        </li>
        <li>
          <div class="icon icon-times-circle-o"></div>
          <input type="text" readonly="readonly" value="times-circle-o">
        </li>
        <li>
          <div class="icon icon-trash"></div>
          <input type="text" readonly="readonly" value="trash">
        </li>
        <li>
          <div class="icon icon-trash-o"></div>
          <input type="text" readonly="readonly" value="trash-o">
        </li>
        <li>
          <div class="icon icon-twitter-square"></div>
          <input type="text" readonly="readonly" value="twitter-square">
        </li>
        <li>
          <div class="icon icon-undo"></div>
          <input type="text" readonly="readonly" value="undo">
        </li>
        <li>
          <div class="icon icon-unlock"></div>
          <input type="text" readonly="readonly" value="unlock">
        </li>
        <li>
          <div class="icon icon-unlock-alt"></div>
          <input type="text" readonly="readonly" value="unlock-alt">
        </li>
        <li>
          <div class="icon icon-upload"></div>
          <input type="text" readonly="readonly" value="upload">
        </li>
        <li>
          <div class="icon icon-users"></div>
          <input type="text" readonly="readonly" value="users">
        </li>
        <li>
          <div class="icon icon-video-camera"></div>
          <input type="text" readonly="readonly" value="video-camera">
        </li>
        <li>
          <div class="icon icon-vimeo-square"></div>
          <input type="text" readonly="readonly" value="vimeo-square">
        </li>
        <li>
          <div class="icon icon-wrench"></div>
          <input type="text" readonly="readonly" value="wrench">
        </li>
        <li>
          <div class="icon icon-youtube-square"></div>
          <input type="text" readonly="readonly" value="youtube-square">
        </li>
        <li>
          <div class="icon icon-youtube-play"></div>
          <input type="text" readonly="readonly" value="youtube-play">
        </li>
        <li>
          <div class="icon icon-bars"></div>
          <input type="text" readonly="readonly" value="bars">
        </li>
        <li>
          <div class="icon icon-trademark"></div>
          <input type="text" readonly="readonly" value="trademark">
        </li>
        <li>
          <div class="icon icon-registered"></div>
          <input type="text" readonly="readonly" value="registered">
        </li>
        <li>
          <div class="icon icon-share-alt-square"></div>
          <input type="text" readonly="readonly" value="share-alt-square">
        </li>
        <li>
          <div class="icon icon-copyright"></div>
          <input type="text" readonly="readonly" value="copyright">
        </li>
        <li>
          <div class="icon icon-credit-card"></div>
          <input type="text" readonly="readonly" value="credit-card">
        </li>
        <li>
          <div class="icon icon-graduation-cap"></div>
          <input type="text" readonly="readonly" value="graduation-cap">
        </li>
        <li>
          <div class="icon icon-palette"></div>
          <input type="text" readonly="readonly" value="palette">
        </li>
        <li>
          <div class="icon icon-workman"></div>
          <input type="text" readonly="readonly" value="workman">
        </li>
        <li>
          <div class="icon icon-arrow-circle-down"></div>
          <input type="text" readonly="readonly" value="arrow-circle-down">
        </li>
        <li>
          <div class="icon icon-arrow-circle-left"></div>
          <input type="text" readonly="readonly" value="arrow-circle-left">
        </li>
        <li>
          <div class="icon icon-arrow-circle-right"></div>
          <input type="text" readonly="readonly" value="arrow-circle-right">
        </li>
        <li>
          <div class="icon icon-arrow-circle-up"></div>
          <input type="text" readonly="readonly" value="arrow-circle-up">
        </li>
        <li>
          <div class="icon icon-arrows"></div>
          <input type="text" readonly="readonly" value="arrows">
        </li>
        <li>
          <div class="icon icon-arrows-alt"></div>
          <input type="text" readonly="readonly" value="arrows-alt">
        </li>
        <li>
          <div class="icon icon-arrows-h"></div>
          <input type="text" readonly="readonly" value="arrows-h">
        </li>
        <li>
          <div class="icon icon-arrows-v"></div>
          <input type="text" readonly="readonly" value="arrows-v">
        </li>
        <li>
          <div class="icon icon-ban"></div>
          <input type="text" readonly="readonly" value="ban">
        </li>
        <li>
          <div class="icon icon-chevron-circle-down"></div>
          <input type="text" readonly="readonly" value="chevron-circle-down">
        </li>
        <li>
          <div class="icon icon-chevron-circle-left"></div>
          <input type="text" readonly="readonly" value="chevron-circle-left">
        </li>
        <li>
          <div class="icon icon-chevron-circle-right"></div>
          <input type="text" readonly="readonly" value="chevron-circle-right">
        </li>
        <li>
          <div class="icon icon-chevron-circle-up"></div>
          <input type="text" readonly="readonly" value="chevron-circle-up">
        </li>
        <li>
          <div class="icon icon-addusers"></div>
          <input type="text" readonly="readonly" value="addusers">
        </li>
        <li>
          <div class="icon icon-profilelist"></div>
          <input type="text" readonly="readonly" value="profilelist">
        </li>
        <li>
          <div class="icon icon-checklist"></div>
          <input type="text" readonly="readonly" value="checklist">
        </li>
        <li>
          <div class="icon icon-books"></div>
          <input type="text" readonly="readonly" value="books">
        </li>
        <li>
          <div class="icon icon-reuser"></div>
          <input type="text" readonly="readonly" value="reuser">
        </li>
        <li>
          <div class="icon icon-exec"></div>
          <input type="text" readonly="readonly" value="exec">
        </li>
        <li>
          <div class="icon icon-setemail"></div>
          <input type="text" readonly="readonly" value="setemail">
        </li>
        <li>
          <div class="icon icon-tools"></div>
          <input type="text" readonly="readonly" value="tools">
        </li>
        <li>
          <div class="icon icon-injuryreport"></div>
          <input type="text" readonly="readonly" value="injuryreport">
        </li>
        <li>
          <div class="icon icon-insurance"></div>
          <input type="text" readonly="readonly" value="insurance">
        </li>
        <li>
          <div class="icon icon-shield"></div>
          <input type="text" readonly="readonly" value="shield">
        </li>
        <li>
          <div class="icon icon-support"></div>
          <input type="text" readonly="readonly" value="support">
        </li>
        <li>
          <div class="icon icon-rehab"></div>
          <input type="text" readonly="readonly" value="rehab">
        </li>
        <li>
          <div class="icon icon-profile"></div>
          <input type="text" readonly="readonly" value="profile">
        </li>
        <li>
          <div class="icon icon-hardhat"></div>
          <input type="text" readonly="readonly" value="hardhat">
        </li>
        <li>
          <div class="icon icon-stethoscope"></div>
          <input type="text" readonly="readonly" value="stethoscope">
        </li>
        <li>
          <div class="icon icon-doctor"></div>
          <input type="text" readonly="readonly" value="doctor">
        </li>
        <li>
          <div class="icon icon-checkheart"></div>
          <input type="text" readonly="readonly" value="checkheart">
        </li>
        <li>
          <div class="icon icon-removeuser"></div>
          <input type="text" readonly="readonly" value="removeuser">
        </li>
        <li>
          <div class="icon icon-newheart"></div>
          <input type="text" readonly="readonly" value="newheart">
        </li>
        <li>
          <div class="icon icon-user"></div>
          <input type="text" readonly="readonly" value="user">
        </li>
        <li>
          <div class="icon icon-plusheart"></div>
          <input type="text" readonly="readonly" value="plusheart">
        </li>
        <li>
          <div class="icon icon-firstreport"></div>
          <input type="text" readonly="readonly" value="firstreport">
        </li>
        <li>
          <div class="icon icon-firstreport2"></div>
          <input type="text" readonly="readonly" value="firstreport2">
        </li>
        <li>
          <div class="icon icon-teams"></div>
          <input type="text" readonly="readonly" value="teams">
        </li>
        <li>
          <div class="icon icon-apple"></div>
          <input type="text" readonly="readonly" value="apple">
        </li>
        <li>
          <div class="icon icon-android"></div>
          <input type="text" readonly="readonly" value="android">
        </li>
        <li>
          <div class="icon icon-newspaper-o"></div>
          <input type="text" readonly="readonly" value="newspaper-o">
        </li>
        <li>
          <div class="icon icon-envelope-square"></div>
          <input type="text" readonly="readonly" value="envelope-square">
        </li>
        <li>
          <div class="icon icon-code"></div>
          <input type="text" readonly="readonly" value="code">
        </li>
        <li>
          <div class="icon icon-code-fork"></div>
          <input type="text" readonly="readonly" value="code-fork">
        </li>
        <li>
          <div class="icon icon-history"></div>
          <input type="text" readonly="readonly" value="history">
        </li>
        <li>
          <div class="icon icon-findinsurance"></div>
          <input type="text" readonly="readonly" value="findinsurance">
        </li>
        <li>
          <div class="icon icon-bandaid"></div>
          <input type="text" readonly="readonly" value="bandaid">
        </li>
        <li>
          <div class="icon icon-injury"></div>
          <input type="text" readonly="readonly" value="injury">
        </li>
        <li>
          <div class="icon icon-crutch"></div>
          <input type="text" readonly="readonly" value="crutch">
        </li>
        <li>
          <div class="icon icon-malefigure"></div>
          <input type="text" readonly="readonly" value="malefigure">
        </li>
        <li>
          <div class="icon icon-femalefigure"></div>
          <input type="text" readonly="readonly" value="femalefigure">
        </li>
        <li>
          <div class="icon icon-mars"></div>
          <input type="text" readonly="readonly" value="mars">
        </li>
        <li>
          <div class="icon icon-venus"></div>
          <input type="text" readonly="readonly" value="venus">
        </li>
        <li>
          <div class="icon icon-company"></div>
          <input type="text" readonly="readonly" value="company">
        </li>
        <li>
          <div class="icon icon-clinic"></div>
          <input type="text" readonly="readonly" value="clinic">
        </li>
        <li>
          <div class="icon icon-hospital"></div>
          <input type="text" readonly="readonly" value="hospital">
        </li>
        <li>
          <div class="icon icon-zencircle"></div>
          <input type="text" readonly="readonly" value="zencircle">
        </li>
        <li>
          <div class="icon icon-relax"></div>
          <input type="text" readonly="readonly" value="relax">
        </li>
        <li>
          <div class="icon icon-bonsai"></div>
          <input type="text" readonly="readonly" value="bonsai">
        </li>
        <li>
          <div class="icon icon-paramedic"></div>
          <input type="text" readonly="readonly" value="paramedic">
        </li>
        <li>
          <div class="icon icon-ambulance"></div>
          <input type="text" readonly="readonly" value="ambulance">
        </li>
        <li>
          <div class="icon icon-bamboo1"></div>
          <input type="text" readonly="readonly" value="bamboo1">
        </li>
        <li>
          <div class="icon icon-bamboo2"></div>
          <input type="text" readonly="readonly" value="bamboo2">
        </li>
        <li>
          <div class="icon icon-bambooleaf"></div>
          <input type="text" readonly="readonly" value="bambooleaf">
        </li>
        <li>
          <div class="icon icon-zenrocks"></div>
          <input type="text" readonly="readonly" value="zenrocks">
        </li>
        <li>
          <div class="icon icon-pills"></div>
          <input type="text" readonly="readonly" value="pills">
        </li>
        <li>
          <div class="icon icon-syringe"></div>
          <input type="text" readonly="readonly" value="syringe">
        </li>
        <li>
          <div class="icon icon-agent"></div>
          <input type="text" readonly="readonly" value="agent">
        </li>
        <li>
          <div class="icon icon-setuser"></div>
          <input type="text" readonly="readonly" value="setuser">
        </li>
        <li>
          <div class="icon icon-setcompany"></div>
          <input type="text" readonly="readonly" value="setcompany">
        </li>
        <li>
          <div class="icon icon-new"></div>
          <input type="text" readonly="readonly" value="new">
        </li>
        <li>
          <div class="icon icon-badge"></div>
          <input type="text" readonly="readonly" value="badge">
        </li>
        <li>
          <div class="icon icon-zenlogo"></div>
          <input type="text" readonly="readonly" value="zenlogo">
        </li>
        <li>
          <div class="icon icon-zig"></div>
          <input type="text" readonly="readonly" value="zig">
        </li>
        <li>
          <div class="icon icon-area-chart"></div>
          <input type="text" readonly="readonly" value="area-chart">
        </li>
        <li>
          <div class="icon icon-bullhorn"></div>
          <input type="text" readonly="readonly" value="bullhorn">
        </li>
        <li>
          <div class="icon icon-crop"></div>
          <input type="text" readonly="readonly" value="crop">
        </li>
        <li>
          <div class="icon icon-crosshairs"></div>
          <input type="text" readonly="readonly" value="crosshairs">
        </li>
        <li>
          <div class="icon icon-exchange"></div>
          <input type="text" readonly="readonly" value="exchange">
        </li>
        <li>
          <div class="icon icon-level-down"></div>
          <input type="text" readonly="readonly" value="level-down">
        </li>
        <li>
          <div class="icon icon-level-up"></div>
          <input type="text" readonly="readonly" value="level-up">
        </li>
        <li>
          <div class="icon icon-long-arrow-down"></div>
          <input type="text" readonly="readonly" value="long-arrow-down">
        </li>
        <li>
          <div class="icon icon-long-arrow-left"></div>
          <input type="text" readonly="readonly" value="long-arrow-left">
        </li>
        <li>
          <div class="icon icon-long-arrow-right"></div>
          <input type="text" readonly="readonly" value="long-arrow-right">
        </li>
        <li>
          <div class="icon icon-long-arrow-up"></div>
          <input type="text" readonly="readonly" value="long-arrow-up">
        </li>
        <li>
          <div class="icon icon-magic"></div>
          <input type="text" readonly="readonly" value="magic">
        </li>
        <li>
          <div class="icon icon-refresh"></div>
          <input type="text" readonly="readonly" value="refresh">
        </li>
        <li>
          <div class="icon icon-share-square"></div>
          <input type="text" readonly="readonly" value="share-square">
        </li>
        <li>
          <div class="icon icon-roundheart"></div>
          <input type="text" readonly="readonly" value="roundheart">
        </li>
        <li>
          <div class="icon icon-key"></div>
          <input type="text" readonly="readonly" value="key">
        </li>
        <li>
          <div class="icon icon-burst-new"></div>
          <input type="text" readonly="readonly" value="burst-new">
        </li>
        <li>
          <div class="icon icon-checkbox"></div>
          <input type="text" readonly="readonly" value="checkbox">
        </li>
        <li>
          <div class="icon icon-arrows-compress"></div>
          <input type="text" readonly="readonly" value="arrows-compress">
        </li>
        <li>
          <div class="icon icon-arrows-expand"></div>
          <input type="text" readonly="readonly" value="arrows-expand">
        </li>
        <li>
          <div class="icon icon-arrows-in"></div>
          <input type="text" readonly="readonly" value="arrows-in">
        </li>
        <li>
          <div class="icon icon-arrows-out"></div>
          <input type="text" readonly="readonly" value="arrows-out">
        </li>
        <li>
          <div class="icon icon-first-aid"></div>
          <input type="text" readonly="readonly" value="first-aid">
        </li>
        <li>
          <div class="icon icon-graph-trend"></div>
          <input type="text" readonly="readonly" value="graph-trend">
        </li>
        <li>
          <div class="icon icon-graph-pie"></div>
          <input type="text" readonly="readonly" value="graph-pie">
        </li>
        <li>
          <div class="icon icon-graph-bar"></div>
          <input type="text" readonly="readonly" value="graph-bar">
        </li>
        <li>
          <div class="icon icon-graph-horizontal"></div>
          <input type="text" readonly="readonly" value="graph-horizontal">
        </li>
        <li>
          <div class="icon icon-lightbulb"></div>
          <input type="text" readonly="readonly" value="lightbulb">
        </li>
        <li>
          <div class="icon icon-link"></div>
          <input type="text" readonly="readonly" value="link">
        </li>
        <li>
          <div class="icon icon-map2"></div>
          <input type="text" readonly="readonly" value="map2">
        </li>
        <li>
          <div class="icon icon-paint-bucket"></div>
          <input type="text" readonly="readonly" value="paint-bucket">
        </li>
        <li>
          <div class="icon icon-skull"></div>
          <input type="text" readonly="readonly" value="skull">
        </li>
        <li>
          <div class="icon icon-unlink"></div>
          <input type="text" readonly="readonly" value="unlink">
        </li>
        <li>
          <div class="icon icon-wheelchair"></div>
          <input type="text" readonly="readonly" value="wheelchair">
        </li>
        <li>
          <div class="icon icon-onmap"></div>
          <input type="text" readonly="readonly" value="onmap">
        </li>
        <li>
          <div class="icon icon-map-o"></div>
          <input type="text" readonly="readonly" value="map-o">
        </li>
        <li>
          <div class="icon icon-paperclip"></div>
          <input type="text" readonly="readonly" value="paperclip">
        </li>
        <li>
          <div class="icon icon-money"></div>
          <input type="text" readonly="readonly" value="money">
        </li>
        <li>
          <div class="icon icon-print"></div>
          <input type="text" readonly="readonly" value="print">
        </li>
        <li>
          <div class="icon icon-ticket"></div>
          <input type="text" readonly="readonly" value="ticket">
        </li>
        <li>
          <div class="icon icon-checklist2"></div>
          <input type="text" readonly="readonly" value="checklist2">
        </li>
        <li>
          <div class="icon icon-book2"></div>
          <input type="text" readonly="readonly" value="book2">
        </li>
        <li>
          <div class="icon icon-bookmark2"></div>
          <input type="text" readonly="readonly" value="bookmark2">
        </li>
        <li>
          <div class="icon icon-dashboard"></div>
          <input type="text" readonly="readonly" value="dashboard">
        </li>
        <li>
          <div class="icon icon-credit-card2"></div>
          <input type="text" readonly="readonly" value="credit-card2">
        </li>
        <li>
          <div class="icon icon-ellipsis"></div>
          <input type="text" readonly="readonly" value="ellipsis">
        </li>
        <li>
          <div class="icon icon-database2"></div>
          <input type="text" readonly="readonly" value="database2">
        </li>
        <li>
          <div class="icon icon-puzzle"></div>
          <input type="text" readonly="readonly" value="puzzle">
        </li>
        <li>
          <div class="icon icon-air"></div>
          <input type="text" readonly="readonly" value="air">
        </li>
        <li>
          <div class="icon icon-brush"></div>
          <input type="text" readonly="readonly" value="brush">
        </li>
        <li>
          <div class="icon icon-lifebuoy"></div>
          <input type="text" readonly="readonly" value="lifebuoy">
        </li>
        <li>
          <div class="icon icon-picture"></div>
          <input type="text" readonly="readonly" value="picture">
        </li>
        <li>
          <div class="icon icon-thermometer"></div>
          <input type="text" readonly="readonly" value="thermometer">
        </li>
        <li>
          <div class="icon icon-traffic-cone"></div>
          <input type="text" readonly="readonly" value="traffic-cone">
        </li>
        <li>
          <div class="icon icon-water"></div>
          <input type="text" readonly="readonly" value="water">
        </li>
        <li>
          <div class="icon icon-skype"></div>
          <input type="text" readonly="readonly" value="skype">
        </li>
        <li>
          <div class="icon icon-folder"></div>
          <input type="text" readonly="readonly" value="folder">
        </li>
        <li>
          <div class="icon icon-folder-add"></div>
          <input type="text" readonly="readonly" value="folder-add">
        </li>
        <li>
          <div class="icon icon-folder-lock"></div>
          <input type="text" readonly="readonly" value="folder-lock">
        </li>
        <li>
          <div class="icon icon-braille"></div>
          <input type="text" readonly="readonly" value="braille">
        </li>
        <li>
          <div class="icon icon-keyboard-o"></div>
          <input type="text" readonly="readonly" value="keyboard-o">
        </li>
        <li>
          <div class="icon icon-eye"></div>
          <input type="text" readonly="readonly" value="eye">
        </li>
        <li>
          <div class="icon icon-eye-slash"></div>
          <input type="text" readonly="readonly" value="eye-slash">
        </li>
        <li>
          <div class="icon icon-light-bulb"></div>
          <input type="text" readonly="readonly" value="light-bulb">
        </li>
        <li>
          <div class="icon icon-die-six"></div>
          <input type="text" readonly="readonly" value="die-six">
        </li>
        <li>
          <div class="icon icon-trophy"></div>
          <input type="text" readonly="readonly" value="trophy">
        </li>
        <li>
          <div class="icon icon-floppy"></div>
          <input type="text" readonly="readonly" value="floppy">
        </li>
        <li>
          <div class="icon icon-flash"></div>
          <input type="text" readonly="readonly" value="flash">
        </li>
        <li>
          <div class="icon icon-dropbox-1"></div>
          <input type="text" readonly="readonly" value="dropbox-1">
        </li>
        <li>
          <div class="icon icon-docs"></div>
          <input type="text" readonly="readonly" value="docs">
        </li>
        <li>
          <div class="icon icon-basket"></div>
          <input type="text" readonly="readonly" value="basket">
        </li>
        <li>
          <div class="icon icon-adjust"></div>
          <input type="text" readonly="readonly" value="adjust">
        </li>
        <li>
          <div class="icon icon-clock"></div>
          <input type="text" readonly="readonly" value="clock">
        </li>
        <li>
          <div class="icon icon-clipboard-1"></div>
          <input type="text" readonly="readonly" value="clipboard-1">
        </li>
        <li>
          <div class="icon icon-cc-share"></div>
          <input type="text" readonly="readonly" value="cc-share">
        </li>
        <li>
          <div class="icon icon-erase"></div>
          <input type="text" readonly="readonly" value="erase">
        </li>
        <li>
          <div class="icon icon-layout"></div>
          <input type="text" readonly="readonly" value="layout">
        </li>
        <li>
          <div class="icon icon-keyboard"></div>
          <input type="text" readonly="readonly" value="keyboard">
        </li>
        <li>
          <div class="icon icon-graduation-cap-1"></div>
          <input type="text" readonly="readonly" value="graduation-cap-1">
        </li>
        <li>
          <div class="icon icon-magnet"></div>
          <input type="text" readonly="readonly" value="magnet">
        </li>
        <li>
          <div class="icon icon-clock-o"></div>
          <input type="text" readonly="readonly" value="clock-o">
        </li>
        <li>
          <div class="icon icon-gift"></div>
          <input type="text" readonly="readonly" value="gift">
        </li>
        <li>
          <div class="icon icon-glass"></div>
          <input type="text" readonly="readonly" value="glass">
        </li>
        <li>
          <div class="icon icon-quote-left"></div>
          <input type="text" readonly="readonly" value="quote-left">
        </li>
        <li>
          <div class="icon icon-quote-right"></div>
          <input type="text" readonly="readonly" value="quote-right">
        </li>
        <li>
          <div class="icon icon-random"></div>
          <input type="text" readonly="readonly" value="random">
        </li>
        <li>
          <div class="icon icon-plug"></div>
          <input type="text" readonly="readonly" value="plug">
        </li>
        <li>
          <div class="icon icon-rss"></div>
          <input type="text" readonly="readonly" value="rss">
        </li>
        <li>
          <div class="icon icon-shield-1"></div>
          <input type="text" readonly="readonly" value="shield-1">
        </li>
        <li>
          <div class="icon icon-toggle-off"></div>
          <input type="text" readonly="readonly" value="toggle-off">
        </li>
        <li>
          <div class="icon icon-toggle-on"></div>
          <input type="text" readonly="readonly" value="toggle-on">
        </li>
        <li>
          <div class="icon icon-tint"></div>
          <input type="text" readonly="readonly" value="tint">
        </li>
        <li>
          <div class="icon icon-umbrella"></div>
          <input type="text" readonly="readonly" value="umbrella">
        </li>
        <li>
          <div class="icon icon-tree"></div>
          <input type="text" readonly="readonly" value="tree">
        </li>
        <li>
          <div class="icon icon-university"></div>
          <input type="text" readonly="readonly" value="university">
        </li>
        <li>
          <div class="icon icon-truck"></div>
          <input type="text" readonly="readonly" value="truck">
        </li>
        <li>
          <div class="icon icon-whatsapp"></div>
          <input type="text" readonly="readonly" value="whatsapp">
        </li>
        <li>
          <div class="icon icon-lock-1"></div>
          <input type="text" readonly="readonly" value="lock-1">
        </li>
        <li>
          <div class="icon icon-microscope"></div>
          <input type="text" readonly="readonly" value="microscope">
        </li>
        <li>
          <div class="icon icon-pencil-1"></div>
          <input type="text" readonly="readonly" value="pencil-1">
        </li>
        <li>
          <div class="icon icon-burst"></div>
          <input type="text" readonly="readonly" value="burst">
        </li>
        <li>
          <div class="icon icon-database-1"></div>
          <input type="text" readonly="readonly" value="database-1">
        </li>
        <li>
          <div class="icon icon-foot"></div>
          <input type="text" readonly="readonly" value="foot">
        </li>
        <li>
          <div class="icon icon-infinity"></div>
          <input type="text" readonly="readonly" value="infinity">
        </li>
        <li>
          <div class="icon icon-progress-0"></div>
          <input type="text" readonly="readonly" value="progress-0">
        </li>
        <li>
          <div class="icon icon-progress-1"></div>
          <input type="text" readonly="readonly" value="progress-1">
        </li>
        <li>
          <div class="icon icon-progress-2"></div>
          <input type="text" readonly="readonly" value="progress-2">
        </li>
        <li>
          <div class="icon icon-progress-3"></div>
          <input type="text" readonly="readonly" value="progress-3">
        </li>
        <li>
          <div class="icon icon-publish"></div>
          <input type="text" readonly="readonly" value="publish">
        </li>
        <li>
          <div class="icon icon-hand-o-right"></div>
          <input type="text" readonly="readonly" value="hand-o-right">
        </li>
        <li>
          <div class="icon icon-sign-in-1"></div>
          <input type="text" readonly="readonly" value="sign-in-1">
        </li>
        <li>
          <div class="icon icon-sign-out-1"></div>
          <input type="text" readonly="readonly" value="sign-out-1">
        </li>
        <li>
          <div class="icon icon-sort"></div>
          <input type="text" readonly="readonly" value="sort">
        </li>
        <li>
          <div class="icon icon-sort-asc"></div>
          <input type="text" readonly="readonly" value="sort-asc">
        </li>
        <li>
          <div class="icon icon-sort-desc"></div>
          <input type="text" readonly="readonly" value="sort-desc">
        </li>
        <li>
          <div class="icon icon-sort-numeric-asc"></div>
          <input type="text" readonly="readonly" value="sort-numeric-asc">
        </li>
        <li>
          <div class="icon icon-sort-numeric-desc"></div>
          <input type="text" readonly="readonly" value="sort-numeric-desc">
        </li>
        <li>
          <div class="icon icon-scissors"></div>
          <input type="text" readonly="readonly" value="scissors">
        </li>
        <li>
          <div class="icon icon-fax"></div>
          <input type="text" readonly="readonly" value="fax">
        </li>
        <li>
          <div class="icon icon-smiley1"></div>
          <input type="text" readonly="readonly" value="smiley1">
        </li>
        <li>
          <div class="icon icon-smiley2"></div>
          <input type="text" readonly="readonly" value="smiley2">
        </li>
        <li>
          <div class="icon icon-smiley3"></div>
          <input type="text" readonly="readonly" value="smiley3">
        </li>
        <li>
          <div class="icon icon-smiley4"></div>
          <input type="text" readonly="readonly" value="smiley4">
        </li>
        <li>
          <div class="icon icon-smiley5"></div>
          <input type="text" readonly="readonly" value="smiley5">
        </li>
        <li>
          <div class="icon icon-smiley6"></div>
          <input type="text" readonly="readonly" value="smiley6">
        </li>
        <li>
          <div class="icon icon-smiley7"></div>
          <input type="text" readonly="readonly" value="smiley7">
        </li>
        <li>
          <div class="icon icon-smiley9"></div>
          <input type="text" readonly="readonly" value="smiley9">
        </li>
        <li>
          <div class="icon icon-smiley10"></div>
          <input type="text" readonly="readonly" value="smiley10">
        </li>
        <li>
          <div class="icon icon-smiley11"></div>
          <input type="text" readonly="readonly" value="smiley11">
        </li>
        <li>
          <div class="icon icon-smiley12"></div>
          <input type="text" readonly="readonly" value="smiley12">
        </li>
        <li>
          <div class="icon icon-smiley13"></div>
          <input type="text" readonly="readonly" value="smiley13">
        </li>
        <li>
          <div class="icon icon-poke"></div>
          <input type="text" readonly="readonly" value="poke">
        </li>
        <li>
          <div class="icon icon-smiley8"></div>
          <input type="text" readonly="readonly" value="smiley8">
        </li>
        <li>
          <div class="icon icon-randomdice"></div>
          <input type="text" readonly="readonly" value="randomdice">
        </li>
        <li>
          <div class="icon icon-smiley14"></div>
          <input type="text" readonly="readonly" value="smiley14">
        </li>
        <li>
          <div class="icon icon-zenrating"></div>
          <input type="text" readonly="readonly" value="zenrating">
        </li>
        <li>
          <div class="icon icon-zcostmeter"></div>
          <input type="text" readonly="readonly" value="zcostmeter">
        </li>
        <li>
          <div class="icon icon-zefficiency"></div>
          <input type="text" readonly="readonly" value="zefficiency">
        </li>
        <li>
          <div class="icon icon-progress"></div>
          <input type="text" readonly="readonly" value="progress">
        </li>
        <li>
          <div class="icon icon-claimcosts"></div>
          <input type="text" readonly="readonly" value="claimcosts">
        </li>
        <li>
          <div class="icon icon-zenmetrics"></div>
          <input type="text" readonly="readonly" value="zenmetrics">
        </li>
        <li>
          <div class="icon icon-certificate-2"></div>
          <input type="text" readonly="readonly" value="certificate-2">
        </li>
        <li>
          <div class="icon icon-logo"></div>
          <input type="text" readonly="readonly" value="logo">
        </li>
        <li>
          <div class="icon icon-companydetails"></div>
          <input type="text" readonly="readonly" value="companydetails">
        </li>
        <li>
          <div class="icon icon-zenco"></div>
          <input type="text" readonly="readonly" value="zenco">
        </li>
        <li>
          <div class="icon icon-zencontact"></div>
          <input type="text" readonly="readonly" value="zencontact">
        </li>
        <li>
          <div class="icon icon-zenisphere"></div>
          <input type="text" readonly="readonly" value="zenisphere">
        </li>
        <li>
          <div class="icon icon-zeniversity"></div>
          <input type="text" readonly="readonly" value="zeniversity">
        </li>
        <li>
          <div class="icon icon-zenlogin"></div>
          <input type="text" readonly="readonly" value="zenlogin">
        </li>
        <li>
          <div class="icon icon-zenproducts"></div>
          <input type="text" readonly="readonly" value="zenproducts">
        </li>
        <li>
          <div class="icon icon-flybadge-o2"></div>
          <input type="text" readonly="readonly" value="flybadge-o2">
        </li>
        <li>
          <div class="icon icon-flybadge-o3"></div>
          <input type="text" readonly="readonly" value="flybadge-o3">
        </li>
        <li>
          <div class="icon icon-flybadge"></div>
          <input type="text" readonly="readonly" value="flybadge">
        </li>
        <li>
          <div class="icon icon-flybadge-o1"></div>
          <input type="text" readonly="readonly" value="flybadge-o1">
        </li>
        <li>
          <div class="icon icon-multiuser"></div>
          <input type="text" readonly="readonly" value="multiuser">
        </li>
        <li>
          <div class="icon icon-userlisting"></div>
          <input type="text" readonly="readonly" value="userlisting">
        </li>
        <li>
          <div class="icon icon-userpanel"></div>
          <input type="text" readonly="readonly" value="userpanel">
        </li>
        <li>
          <div class="icon icon-adduser"></div>
          <input type="text" readonly="readonly" value="adduser">
        </li>
        <li>
          <div class="icon icon-chevron-down"></div>
          <input type="text" readonly="readonly" value="chevron-down">
        </li>
        <li>
          <div class="icon icon-chevron-left"></div>
          <input type="text" readonly="readonly" value="chevron-left">
        </li>
        <li>
          <div class="icon icon-chevron-right"></div>
          <input type="text" readonly="readonly" value="chevron-right">
        </li>
        <li>
          <div class="icon icon-chevron-up"></div>
          <input type="text" readonly="readonly" value="chevron-up">
        </li>
        <li>
          <div class="icon icon-flame"></div>
          <input type="text" readonly="readonly" value="flame">
        </li>
        <li>
          <div class="icon icon-alert"></div>
          <input type="text" readonly="readonly" value="alert">
        </li>
        <li>
          <div class="icon icon-bell-1"></div>
          <input type="text" readonly="readonly" value="bell-1">
        </li>
        <li>
          <div class="icon icon-down-open-big"></div>
          <input type="text" readonly="readonly" value="down-open-big">
        </li>
        <li>
          <div class="icon icon-gauge"></div>
          <input type="text" readonly="readonly" value="gauge">
        </li>
        <li>
          <div class="icon icon-angle-down"></div>
          <input type="text" readonly="readonly" value="angle-down">
        </li>
        <li>
          <div class="icon icon-angle-left"></div>
          <input type="text" readonly="readonly" value="angle-left">
        </li>
        <li>
          <div class="icon icon-angle-right"></div>
          <input type="text" readonly="readonly" value="angle-right">
        </li>
        <li>
          <div class="icon icon-angle-up"></div>
          <input type="text" readonly="readonly" value="angle-up">
        </li>
        <li>
          <div class="icon icon-gavel"></div>
          <input type="text" readonly="readonly" value="gavel">
        </li>
        <li>
          <div class="icon icon-stethoscope-1"></div>
          <input type="text" readonly="readonly" value="stethoscope-1">
        </li>
        <li>
          <div class="icon icon-pulse"></div>
          <input type="text" readonly="readonly" value="pulse">
        </li>
        <li>
          <div class="icon icon-terminal"></div>
          <input type="text" readonly="readonly" value="terminal">
        </li>
        <li>
          <div class="icon icon-stop-1"></div>
          <input type="text" readonly="readonly" value="stop-1">
        </li>
      </ul>
      <h2>Character mapping</h2>
      <ul class="glyphs character-mapping">
        <li>
          <div data-icon="&#xe000;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe000;">
        </li>
        <li>
          <div data-icon="&#xe001;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe001;">
        </li>
        <li>
          <div data-icon="&#xe002;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe002;">
        </li>
        <li>
          <div data-icon="&#xe003;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe003;">
        </li>
        <li>
          <div data-icon="&#xe004;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe004;">
        </li>
        <li>
          <div data-icon="&#xe005;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe005;">
        </li>
        <li>
          <div data-icon="&#xe006;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe006;">
        </li>
        <li>
          <div data-icon="&#xe007;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe007;">
        </li>
        <li>
          <div data-icon="&#xe008;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe008;">
        </li>
        <li>
          <div data-icon="&#xe009;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe009;">
        </li>
        <li>
          <div data-icon="&#xe00a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe00a;">
        </li>
        <li>
          <div data-icon="&#xe00b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe00b;">
        </li>
        <li>
          <div data-icon="&#xe00c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe00c;">
        </li>
        <li>
          <div data-icon="&#xe00d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe00d;">
        </li>
        <li>
          <div data-icon="&#xe00e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe00e;">
        </li>
        <li>
          <div data-icon="&#xe00f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe00f;">
        </li>
        <li>
          <div data-icon="&#xe010;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe010;">
        </li>
        <li>
          <div data-icon="&#xe011;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe011;">
        </li>
        <li>
          <div data-icon="&#xe012;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe012;">
        </li>
        <li>
          <div data-icon="&#xe013;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe013;">
        </li>
        <li>
          <div data-icon="&#xe014;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe014;">
        </li>
        <li>
          <div data-icon="&#xe015;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe015;">
        </li>
        <li>
          <div data-icon="&#xe016;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe016;">
        </li>
        <li>
          <div data-icon="&#xe017;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe017;">
        </li>
        <li>
          <div data-icon="&#xe018;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe018;">
        </li>
        <li>
          <div data-icon="&#xe019;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe019;">
        </li>
        <li>
          <div data-icon="&#xe01a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe01a;">
        </li>
        <li>
          <div data-icon="&#xe01b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe01b;">
        </li>
        <li>
          <div data-icon="&#xe01c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe01c;">
        </li>
        <li>
          <div data-icon="&#xe01d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe01d;">
        </li>
        <li>
          <div data-icon="&#xe01e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe01e;">
        </li>
        <li>
          <div data-icon="&#xe01f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe01f;">
        </li>
        <li>
          <div data-icon="&#xe020;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe020;">
        </li>
        <li>
          <div data-icon="&#xe021;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe021;">
        </li>
        <li>
          <div data-icon="&#xe022;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe022;">
        </li>
        <li>
          <div data-icon="&#xe023;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe023;">
        </li>
        <li>
          <div data-icon="&#xe024;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe024;">
        </li>
        <li>
          <div data-icon="&#xe025;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe025;">
        </li>
        <li>
          <div data-icon="&#xe026;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe026;">
        </li>
        <li>
          <div data-icon="&#xe027;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe027;">
        </li>
        <li>
          <div data-icon="&#xe028;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe028;">
        </li>
        <li>
          <div data-icon="&#xe029;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe029;">
        </li>
        <li>
          <div data-icon="&#xe02a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe02a;">
        </li>
        <li>
          <div data-icon="&#xe02b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe02b;">
        </li>
        <li>
          <div data-icon="&#xe02c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe02c;">
        </li>
        <li>
          <div data-icon="&#xe02d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe02d;">
        </li>
        <li>
          <div data-icon="&#xe02e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe02e;">
        </li>
        <li>
          <div data-icon="&#xe02f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe02f;">
        </li>
        <li>
          <div data-icon="&#xe030;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe030;">
        </li>
        <li>
          <div data-icon="&#xe031;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe031;">
        </li>
        <li>
          <div data-icon="&#xe032;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe032;">
        </li>
        <li>
          <div data-icon="&#xe033;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe033;">
        </li>
        <li>
          <div data-icon="&#xe034;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe034;">
        </li>
        <li>
          <div data-icon="&#xe035;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe035;">
        </li>
        <li>
          <div data-icon="&#xe036;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe036;">
        </li>
        <li>
          <div data-icon="&#xe037;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe037;">
        </li>
        <li>
          <div data-icon="&#xe038;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe038;">
        </li>
        <li>
          <div data-icon="&#xe039;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe039;">
        </li>
        <li>
          <div data-icon="&#xe03a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe03a;">
        </li>
        <li>
          <div data-icon="&#xe03b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe03b;">
        </li>
        <li>
          <div data-icon="&#xe03c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe03c;">
        </li>
        <li>
          <div data-icon="&#xe03d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe03d;">
        </li>
        <li>
          <div data-icon="&#xe03e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe03e;">
        </li>
        <li>
          <div data-icon="&#xe03f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe03f;">
        </li>
        <li>
          <div data-icon="&#xe040;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe040;">
        </li>
        <li>
          <div data-icon="&#xe041;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe041;">
        </li>
        <li>
          <div data-icon="&#xe042;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe042;">
        </li>
        <li>
          <div data-icon="&#xe043;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe043;">
        </li>
        <li>
          <div data-icon="&#xe044;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe044;">
        </li>
        <li>
          <div data-icon="&#xe045;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe045;">
        </li>
        <li>
          <div data-icon="&#xe046;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe046;">
        </li>
        <li>
          <div data-icon="&#xe047;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe047;">
        </li>
        <li>
          <div data-icon="&#xe048;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe048;">
        </li>
        <li>
          <div data-icon="&#xe049;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe049;">
        </li>
        <li>
          <div data-icon="&#xe04a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe04a;">
        </li>
        <li>
          <div data-icon="&#xe04b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe04b;">
        </li>
        <li>
          <div data-icon="&#xe04c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe04c;">
        </li>
        <li>
          <div data-icon="&#xe04d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe04d;">
        </li>
        <li>
          <div data-icon="&#xe04e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe04e;">
        </li>
        <li>
          <div data-icon="&#xe04f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe04f;">
        </li>
        <li>
          <div data-icon="&#xe050;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe050;">
        </li>
        <li>
          <div data-icon="&#xe051;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe051;">
        </li>
        <li>
          <div data-icon="&#xe052;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe052;">
        </li>
        <li>
          <div data-icon="&#xe053;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe053;">
        </li>
        <li>
          <div data-icon="&#xe054;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe054;">
        </li>
        <li>
          <div data-icon="&#xe055;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe055;">
        </li>
        <li>
          <div data-icon="&#xe056;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe056;">
        </li>
        <li>
          <div data-icon="&#xe057;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe057;">
        </li>
        <li>
          <div data-icon="&#xe058;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe058;">
        </li>
        <li>
          <div data-icon="&#xe059;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe059;">
        </li>
        <li>
          <div data-icon="&#xe05a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe05a;">
        </li>
        <li>
          <div data-icon="&#xe05b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe05b;">
        </li>
        <li>
          <div data-icon="&#xe05c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe05c;">
        </li>
        <li>
          <div data-icon="&#xe05d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe05d;">
        </li>
        <li>
          <div data-icon="&#xe05e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe05e;">
        </li>
        <li>
          <div data-icon="&#xe05f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe05f;">
        </li>
        <li>
          <div data-icon="&#xe060;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe060;">
        </li>
        <li>
          <div data-icon="&#xe061;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe061;">
        </li>
        <li>
          <div data-icon="&#xe062;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe062;">
        </li>
        <li>
          <div data-icon="&#xe063;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe063;">
        </li>
        <li>
          <div data-icon="&#xe064;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe064;">
        </li>
        <li>
          <div data-icon="&#xe065;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe065;">
        </li>
        <li>
          <div data-icon="&#xe066;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe066;">
        </li>
        <li>
          <div data-icon="&#xe067;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe067;">
        </li>
        <li>
          <div data-icon="&#xe068;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe068;">
        </li>
        <li>
          <div data-icon="&#xe069;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe069;">
        </li>
        <li>
          <div data-icon="&#xe06a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe06a;">
        </li>
        <li>
          <div data-icon="&#xe06b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe06b;">
        </li>
        <li>
          <div data-icon="&#xe06c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe06c;">
        </li>
        <li>
          <div data-icon="&#xe06d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe06d;">
        </li>
        <li>
          <div data-icon="&#xe06e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe06e;">
        </li>
        <li>
          <div data-icon="&#xe06f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe06f;">
        </li>
        <li>
          <div data-icon="&#xe070;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe070;">
        </li>
        <li>
          <div data-icon="&#xe071;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe071;">
        </li>
        <li>
          <div data-icon="&#xe072;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe072;">
        </li>
        <li>
          <div data-icon="&#xe073;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe073;">
        </li>
        <li>
          <div data-icon="&#xe074;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe074;">
        </li>
        <li>
          <div data-icon="&#xe075;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe075;">
        </li>
        <li>
          <div data-icon="&#xe076;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe076;">
        </li>
        <li>
          <div data-icon="&#xe077;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe077;">
        </li>
        <li>
          <div data-icon="&#xe078;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe078;">
        </li>
        <li>
          <div data-icon="&#xe079;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe079;">
        </li>
        <li>
          <div data-icon="&#xe07a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe07a;">
        </li>
        <li>
          <div data-icon="&#xe07b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe07b;">
        </li>
        <li>
          <div data-icon="&#xe07c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe07c;">
        </li>
        <li>
          <div data-icon="&#xe07d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe07d;">
        </li>
        <li>
          <div data-icon="&#xe07e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe07e;">
        </li>
        <li>
          <div data-icon="&#xe07f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe07f;">
        </li>
        <li>
          <div data-icon="&#xe080;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe080;">
        </li>
        <li>
          <div data-icon="&#xe081;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe081;">
        </li>
        <li>
          <div data-icon="&#xe082;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe082;">
        </li>
        <li>
          <div data-icon="&#xe083;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe083;">
        </li>
        <li>
          <div data-icon="&#xe084;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe084;">
        </li>
        <li>
          <div data-icon="&#xe085;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe085;">
        </li>
        <li>
          <div data-icon="&#xe086;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe086;">
        </li>
        <li>
          <div data-icon="&#xe087;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe087;">
        </li>
        <li>
          <div data-icon="&#xe088;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe088;">
        </li>
        <li>
          <div data-icon="&#xe089;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe089;">
        </li>
        <li>
          <div data-icon="&#xe08a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe08a;">
        </li>
        <li>
          <div data-icon="&#xe08b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe08b;">
        </li>
        <li>
          <div data-icon="&#xe08c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe08c;">
        </li>
        <li>
          <div data-icon="&#xe08d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe08d;">
        </li>
        <li>
          <div data-icon="&#xe08e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe08e;">
        </li>
        <li>
          <div data-icon="&#xe08f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe08f;">
        </li>
        <li>
          <div data-icon="&#xe090;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe090;">
        </li>
        <li>
          <div data-icon="&#xe091;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe091;">
        </li>
        <li>
          <div data-icon="&#xe092;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe092;">
        </li>
        <li>
          <div data-icon="&#xe093;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe093;">
        </li>
        <li>
          <div data-icon="&#xe094;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe094;">
        </li>
        <li>
          <div data-icon="&#xe095;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe095;">
        </li>
        <li>
          <div data-icon="&#xe096;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe096;">
        </li>
        <li>
          <div data-icon="&#xe097;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe097;">
        </li>
        <li>
          <div data-icon="&#xe098;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe098;">
        </li>
        <li>
          <div data-icon="&#xe099;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe099;">
        </li>
        <li>
          <div data-icon="&#xe09a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe09a;">
        </li>
        <li>
          <div data-icon="&#xe09b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe09b;">
        </li>
        <li>
          <div data-icon="&#xe09c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe09c;">
        </li>
        <li>
          <div data-icon="&#xe09d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe09d;">
        </li>
        <li>
          <div data-icon="&#xe09e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe09e;">
        </li>
        <li>
          <div data-icon="&#xe09f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe09f;">
        </li>
        <li>
          <div data-icon="&#xe0a0;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0a0;">
        </li>
        <li>
          <div data-icon="&#xe0a1;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0a1;">
        </li>
        <li>
          <div data-icon="&#xe0a2;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0a2;">
        </li>
        <li>
          <div data-icon="&#xe0a3;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0a3;">
        </li>
        <li>
          <div data-icon="&#xe0a4;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0a4;">
        </li>
        <li>
          <div data-icon="&#xe0a5;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0a5;">
        </li>
        <li>
          <div data-icon="&#xe0a6;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0a6;">
        </li>
        <li>
          <div data-icon="&#xe0a7;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0a7;">
        </li>
        <li>
          <div data-icon="&#xe0a8;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0a8;">
        </li>
        <li>
          <div data-icon="&#xe0a9;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0a9;">
        </li>
        <li>
          <div data-icon="&#xe0aa;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0aa;">
        </li>
        <li>
          <div data-icon="&#xe0ab;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0ab;">
        </li>
        <li>
          <div data-icon="&#xe0ac;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0ac;">
        </li>
        <li>
          <div data-icon="&#xe0ad;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0ad;">
        </li>
        <li>
          <div data-icon="&#xe0ae;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0ae;">
        </li>
        <li>
          <div data-icon="&#xe0af;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0af;">
        </li>
        <li>
          <div data-icon="&#xe0b0;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0b0;">
        </li>
        <li>
          <div data-icon="&#xe0b1;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0b1;">
        </li>
        <li>
          <div data-icon="&#xe0b2;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0b2;">
        </li>
        <li>
          <div data-icon="&#xe0b3;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0b3;">
        </li>
        <li>
          <div data-icon="&#xe0b4;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0b4;">
        </li>
        <li>
          <div data-icon="&#xe0b5;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0b5;">
        </li>
        <li>
          <div data-icon="&#xe0b6;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0b6;">
        </li>
        <li>
          <div data-icon="&#xe0b7;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0b7;">
        </li>
        <li>
          <div data-icon="&#xe0b8;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0b8;">
        </li>
        <li>
          <div data-icon="&#xe0b9;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0b9;">
        </li>
        <li>
          <div data-icon="&#xe0ba;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0ba;">
        </li>
        <li>
          <div data-icon="&#xe0bb;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0bb;">
        </li>
        <li>
          <div data-icon="&#xe0bc;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0bc;">
        </li>
        <li>
          <div data-icon="&#xe0bd;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0bd;">
        </li>
        <li>
          <div data-icon="&#xe0be;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0be;">
        </li>
        <li>
          <div data-icon="&#xe0bf;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0bf;">
        </li>
        <li>
          <div data-icon="&#xe0c0;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0c0;">
        </li>
        <li>
          <div data-icon="&#xe0c1;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0c1;">
        </li>
        <li>
          <div data-icon="&#xe0c2;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0c2;">
        </li>
        <li>
          <div data-icon="&#xe0c3;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0c3;">
        </li>
        <li>
          <div data-icon="&#xe0c4;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0c4;">
        </li>
        <li>
          <div data-icon="&#xe0c5;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0c5;">
        </li>
        <li>
          <div data-icon="&#xe0c6;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0c6;">
        </li>
        <li>
          <div data-icon="&#xe0c7;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0c7;">
        </li>
        <li>
          <div data-icon="&#xe0c8;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0c8;">
        </li>
        <li>
          <div data-icon="&#xe0c9;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0c9;">
        </li>
        <li>
          <div data-icon="&#xe0ca;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0ca;">
        </li>
        <li>
          <div data-icon="&#xe0cb;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0cb;">
        </li>
        <li>
          <div data-icon="&#xe0cc;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0cc;">
        </li>
        <li>
          <div data-icon="&#xe0cd;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0cd;">
        </li>
        <li>
          <div data-icon="&#xe0ce;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0ce;">
        </li>
        <li>
          <div data-icon="&#xe0d0;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0d0;">
        </li>
        <li>
          <div data-icon="&#xe0d1;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0d1;">
        </li>
        <li>
          <div data-icon="&#xe0d2;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0d2;">
        </li>
        <li>
          <div data-icon="&#xe0d3;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0d3;">
        </li>
        <li>
          <div data-icon="&#xe0d4;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0d4;">
        </li>
        <li>
          <div data-icon="&#xe0d5;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0d5;">
        </li>
        <li>
          <div data-icon="&#xe0d6;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0d6;">
        </li>
        <li>
          <div data-icon="&#xe0d7;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0d7;">
        </li>
        <li>
          <div data-icon="&#xe0d8;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0d8;">
        </li>
        <li>
          <div data-icon="&#xe0d9;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0d9;">
        </li>
        <li>
          <div data-icon="&#xe0da;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0da;">
        </li>
        <li>
          <div data-icon="&#xe0db;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0db;">
        </li>
        <li>
          <div data-icon="&#xe0dc;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0dc;">
        </li>
        <li>
          <div data-icon="&#xe0dd;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0dd;">
        </li>
        <li>
          <div data-icon="&#xe0de;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0de;">
        </li>
        <li>
          <div data-icon="&#xe0df;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0df;">
        </li>
        <li>
          <div data-icon="&#xe0e0;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0e0;">
        </li>
        <li>
          <div data-icon="&#xe0e1;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0e1;">
        </li>
        <li>
          <div data-icon="&#xe0e2;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0e2;">
        </li>
        <li>
          <div data-icon="&#xe0e3;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0e3;">
        </li>
        <li>
          <div data-icon="&#xe0e4;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0e4;">
        </li>
        <li>
          <div data-icon="&#xe0e5;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0e5;">
        </li>
        <li>
          <div data-icon="&#xe0e6;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0e6;">
        </li>
        <li>
          <div data-icon="&#xe0e7;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0e7;">
        </li>
        <li>
          <div data-icon="&#xe0e8;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0e8;">
        </li>
        <li>
          <div data-icon="&#xe0e9;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0e9;">
        </li>
        <li>
          <div data-icon="&#xe0ea;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0ea;">
        </li>
        <li>
          <div data-icon="&#xe0eb;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0eb;">
        </li>
        <li>
          <div data-icon="&#xe0ec;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0ec;">
        </li>
        <li>
          <div data-icon="&#xe0ed;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0ed;">
        </li>
        <li>
          <div data-icon="&#xe0ee;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0ee;">
        </li>
        <li>
          <div data-icon="&#xe0ef;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0ef;">
        </li>
        <li>
          <div data-icon="&#xe0f0;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0f0;">
        </li>
        <li>
          <div data-icon="&#xe0f1;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0f1;">
        </li>
        <li>
          <div data-icon="&#xe0f2;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0f2;">
        </li>
        <li>
          <div data-icon="&#xe0f3;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0f3;">
        </li>
        <li>
          <div data-icon="&#xe0f4;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0f4;">
        </li>
        <li>
          <div data-icon="&#xe0f5;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0f5;">
        </li>
        <li>
          <div data-icon="&#xe0f7;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0f7;">
        </li>
        <li>
          <div data-icon="&#xe0f8;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0f8;">
        </li>
        <li>
          <div data-icon="&#xe0f9;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0f9;">
        </li>
        <li>
          <div data-icon="&#xe0fa;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0fa;">
        </li>
        <li>
          <div data-icon="&#xe0fb;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0fb;">
        </li>
        <li>
          <div data-icon="&#xe0fd;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0fd;">
        </li>
        <li>
          <div data-icon="&#xe0fe;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0fe;">
        </li>
        <li>
          <div data-icon="&#xe0ff;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0ff;">
        </li>
        <li>
          <div data-icon="&#xe100;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe100;">
        </li>
        <li>
          <div data-icon="&#xe101;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe101;">
        </li>
        <li>
          <div data-icon="&#xe102;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe102;">
        </li>
        <li>
          <div data-icon="&#xe103;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe103;">
        </li>
        <li>
          <div data-icon="&#xe104;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe104;">
        </li>
        <li>
          <div data-icon="&#xe105;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe105;">
        </li>
        <li>
          <div data-icon="&#xe106;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe106;">
        </li>
        <li>
          <div data-icon="&#xe107;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe107;">
        </li>
        <li>
          <div data-icon="&#xe108;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe108;">
        </li>
        <li>
          <div data-icon="&#xe109;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe109;">
        </li>
        <li>
          <div data-icon="&#xe10a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe10a;">
        </li>
        <li>
          <div data-icon="&#xe10b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe10b;">
        </li>
        <li>
          <div data-icon="&#xe10c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe10c;">
        </li>
        <li>
          <div data-icon="&#xe10d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe10d;">
        </li>
        <li>
          <div data-icon="&#xe10e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe10e;">
        </li>
        <li>
          <div data-icon="&#xe10f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe10f;">
        </li>
        <li>
          <div data-icon="&#xe110;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe110;">
        </li>
        <li>
          <div data-icon="&#xe111;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe111;">
        </li>
        <li>
          <div data-icon="&#xe112;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe112;">
        </li>
        <li>
          <div data-icon="&#xe113;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe113;">
        </li>
        <li>
          <div data-icon="&#xe114;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe114;">
        </li>
        <li>
          <div data-icon="&#xe115;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe115;">
        </li>
        <li>
          <div data-icon="&#xe116;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe116;">
        </li>
        <li>
          <div data-icon="&#xe117;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe117;">
        </li>
        <li>
          <div data-icon="&#xe118;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe118;">
        </li>
        <li>
          <div data-icon="&#xe119;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe119;">
        </li>
        <li>
          <div data-icon="&#xe11a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe11a;">
        </li>
        <li>
          <div data-icon="&#xe11b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe11b;">
        </li>
        <li>
          <div data-icon="&#xe11c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe11c;">
        </li>
        <li>
          <div data-icon="&#xe11d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe11d;">
        </li>
        <li>
          <div data-icon="&#xe11e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe11e;">
        </li>
        <li>
          <div data-icon="&#xe11f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe11f;">
        </li>
        <li>
          <div data-icon="&#xe120;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe120;">
        </li>
        <li>
          <div data-icon="&#xe121;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe121;">
        </li>
        <li>
          <div data-icon="&#xe122;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe122;">
        </li>
        <li>
          <div data-icon="&#xe123;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe123;">
        </li>
        <li>
          <div data-icon="&#xe124;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe124;">
        </li>
        <li>
          <div data-icon="&#xe125;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe125;">
        </li>
        <li>
          <div data-icon="&#xe126;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe126;">
        </li>
        <li>
          <div data-icon="&#xe127;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe127;">
        </li>
        <li>
          <div data-icon="&#xe128;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe128;">
        </li>
        <li>
          <div data-icon="&#xe129;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe129;">
        </li>
        <li>
          <div data-icon="&#xe12a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe12a;">
        </li>
        <li>
          <div data-icon="&#xe12b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe12b;">
        </li>
        <li>
          <div data-icon="&#xe12c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe12c;">
        </li>
        <li>
          <div data-icon="&#xe12d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe12d;">
        </li>
        <li>
          <div data-icon="&#xe12e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe12e;">
        </li>
        <li>
          <div data-icon="&#xe12f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe12f;">
        </li>
        <li>
          <div data-icon="&#xe130;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe130;">
        </li>
        <li>
          <div data-icon="&#xe131;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe131;">
        </li>
        <li>
          <div data-icon="&#xe132;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe132;">
        </li>
        <li>
          <div data-icon="&#xe133;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe133;">
        </li>
        <li>
          <div data-icon="&#xe134;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe134;">
        </li>
        <li>
          <div data-icon="&#xe135;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe135;">
        </li>
        <li>
          <div data-icon="&#xe136;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe136;">
        </li>
        <li>
          <div data-icon="&#xe137;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe137;">
        </li>
        <li>
          <div data-icon="&#xe138;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe138;">
        </li>
        <li>
          <div data-icon="&#xe139;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe139;">
        </li>
        <li>
          <div data-icon="&#xe13a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe13a;">
        </li>
        <li>
          <div data-icon="&#xe13b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe13b;">
        </li>
        <li>
          <div data-icon="&#xe13c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe13c;">
        </li>
        <li>
          <div data-icon="&#xe13d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe13d;">
        </li>
        <li>
          <div data-icon="&#xe13e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe13e;">
        </li>
        <li>
          <div data-icon="&#xe140;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe140;">
        </li>
        <li>
          <div data-icon="&#xe141;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe141;">
        </li>
        <li>
          <div data-icon="&#xe142;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe142;">
        </li>
        <li>
          <div data-icon="&#xe143;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe143;">
        </li>
        <li>
          <div data-icon="&#xe144;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe144;">
        </li>
        <li>
          <div data-icon="&#xe145;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe145;">
        </li>
        <li>
          <div data-icon="&#xe146;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe146;">
        </li>
        <li>
          <div data-icon="&#xe147;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe147;">
        </li>
        <li>
          <div data-icon="&#xe148;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe148;">
        </li>
        <li>
          <div data-icon="&#xe149;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe149;">
        </li>
        <li>
          <div data-icon="&#xe14a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe14a;">
        </li>
        <li>
          <div data-icon="&#xe14b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe14b;">
        </li>
        <li>
          <div data-icon="&#xe14c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe14c;">
        </li>
        <li>
          <div data-icon="&#xe14d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe14d;">
        </li>
        <li>
          <div data-icon="&#xe14e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe14e;">
        </li>
        <li>
          <div data-icon="&#xe14f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe14f;">
        </li>
        <li>
          <div data-icon="&#xe150;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe150;">
        </li>
        <li>
          <div data-icon="&#xe151;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe151;">
        </li>
        <li>
          <div data-icon="&#xe152;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe152;">
        </li>
        <li>
          <div data-icon="&#xe153;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe153;">
        </li>
        <li>
          <div data-icon="&#xe154;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe154;">
        </li>
        <li>
          <div data-icon="&#xe155;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe155;">
        </li>
        <li>
          <div data-icon="&#xe156;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe156;">
        </li>
        <li>
          <div data-icon="&#xe157;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe157;">
        </li>
        <li>
          <div data-icon="&#xe158;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe158;">
        </li>
        <li>
          <div data-icon="&#xe159;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe159;">
        </li>
        <li>
          <div data-icon="&#xe15a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe15a;">
        </li>
        <li>
          <div data-icon="&#xe15b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe15b;">
        </li>
        <li>
          <div data-icon="&#xe15c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe15c;">
        </li>
        <li>
          <div data-icon="&#xe15d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe15d;">
        </li>
        <li>
          <div data-icon="&#xe15e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe15e;">
        </li>
        <li>
          <div data-icon="&#xe15f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe15f;">
        </li>
        <li>
          <div data-icon="&#xe160;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe160;">
        </li>
        <li>
          <div data-icon="&#xe161;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe161;">
        </li>
        <li>
          <div data-icon="&#xe162;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe162;">
        </li>
        <li>
          <div data-icon="&#xe163;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe163;">
        </li>
        <li>
          <div data-icon="&#xe164;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe164;">
        </li>
        <li>
          <div data-icon="&#xe165;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe165;">
        </li>
        <li>
          <div data-icon="&#xe166;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe166;">
        </li>
        <li>
          <div data-icon="&#xe167;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe167;">
        </li>
        <li>
          <div data-icon="&#xe168;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe168;">
        </li>
        <li>
          <div data-icon="&#xe169;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe169;">
        </li>
        <li>
          <div data-icon="&#xe16a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe16a;">
        </li>
        <li>
          <div data-icon="&#xe16b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe16b;">
        </li>
        <li>
          <div data-icon="&#xe16c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe16c;">
        </li>
        <li>
          <div data-icon="&#xe16d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe16d;">
        </li>
        <li>
          <div data-icon="&#xe16e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe16e;">
        </li>
        <li>
          <div data-icon="&#xe16f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe16f;">
        </li>
        <li>
          <div data-icon="&#xe170;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe170;">
        </li>
        <li>
          <div data-icon="&#xe171;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe171;">
        </li>
        <li>
          <div data-icon="&#xe172;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe172;">
        </li>
        <li>
          <div data-icon="&#xe173;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe173;">
        </li>
        <li>
          <div data-icon="&#xe174;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe174;">
        </li>
        <li>
          <div data-icon="&#xe175;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe175;">
        </li>
        <li>
          <div data-icon="&#xe176;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe176;">
        </li>
        <li>
          <div data-icon="&#xe177;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe177;">
        </li>
        <li>
          <div data-icon="&#xe178;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe178;">
        </li>
        <li>
          <div data-icon="&#xe179;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe179;">
        </li>
        <li>
          <div data-icon="&#xe17a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe17a;">
        </li>
        <li>
          <div data-icon="&#xe17b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe17b;">
        </li>
        <li>
          <div data-icon="&#xe17c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe17c;">
        </li>
        <li>
          <div data-icon="&#xe17d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe17d;">
        </li>
        <li>
          <div data-icon="&#xe17e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe17e;">
        </li>
        <li>
          <div data-icon="&#xe17f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe17f;">
        </li>
        <li>
          <div data-icon="&#xe180;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe180;">
        </li>
        <li>
          <div data-icon="&#xe181;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe181;">
        </li>
        <li>
          <div data-icon="&#xe185;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe185;">
        </li>
        <li>
          <div data-icon="&#xe182;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe182;">
        </li>
        <li>
          <div data-icon="&#xe184;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe184;">
        </li>
        <li>
          <div data-icon="&#xe183;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe183;">
        </li>
        <li>
          <div data-icon="&#xe186;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe186;">
        </li>
        <li>
          <div data-icon="&#xe187;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe187;">
        </li>
        <li>
          <div data-icon="&#xe188;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe188;">
        </li>
        <li>
          <div data-icon="&#xe189;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe189;">
        </li>
        <li>
          <div data-icon="&#xe0f6;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0f6;">
        </li>
        <li>
          <div data-icon="&#xe18a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe18a;">
        </li>
        <li>
          <div data-icon="&#xe18b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe18b;">
        </li>
        <li>
          <div data-icon="&#xe18c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe18c;">
        </li>
        <li>
          <div data-icon="&#xe0fc;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0fc;">
        </li>
        <li>
          <div data-icon="&#xe18d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe18d;">
        </li>
        <li>
          <div data-icon="&#xe18e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe18e;">
        </li>
        <li>
          <div data-icon="&#xe18f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe18f;">
        </li>
        <li>
          <div data-icon="&#xe190;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe190;">
        </li>
        <li>
          <div data-icon="&#xe191;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe191;">
        </li>
        <li>
          <div data-icon="&#xe192;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe192;">
        </li>
        <li>
          <div data-icon="&#xe13f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe13f;">
        </li>
        <li>
          <div data-icon="&#xe193;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe193;">
        </li>
        <li>
          <div data-icon="&#xe194;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe194;">
        </li>
        <li>
          <div data-icon="&#xe195;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe195;">
        </li>
        <li>
          <div data-icon="&#xe196;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe196;">
        </li>
        <li>
          <div data-icon="&#xe197;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe197;">
        </li>
        <li>
          <div data-icon="&#xe198;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe198;">
        </li>
        <li>
          <div data-icon="&#xe0cf;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe0cf;">
        </li>
        <li>
          <div data-icon="&#xe199;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe199;">
        </li>
        <li>
          <div data-icon="&#xe19a;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe19a;">
        </li>
        <li>
          <div data-icon="&#xe19b;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe19b;">
        </li>
        <li>
          <div data-icon="&#xe19c;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe19c;">
        </li>
        <li>
          <div data-icon="&#xe19d;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe19d;">
        </li>
        <li>
          <div data-icon="&#xe19e;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe19e;">
        </li>
        <li>
          <div data-icon="&#xe19f;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe19f;">
        </li>
        <li>
          <div data-icon="&#xe1a0;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe1a0;">
        </li>
        <li>
          <div data-icon="&#xe1a1;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe1a1;">
        </li>
        <li>
          <div data-icon="&#xe1a2;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe1a2;">
        </li>
        <li>
          <div data-icon="&#xe1a3;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe1a3;">
        </li>
        <li>
          <div data-icon="&#xe1a4;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe1a4;">
        </li>
        <li>
          <div data-icon="&#xe1a5;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe1a5;">
        </li>
        <li>
          <div data-icon="&#xe1a6;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe1a6;">
        </li>
        <li>
          <div data-icon="&#xe1a7;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe1a7;">
        </li>
        <li>
          <div data-icon="&#xe1a8;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe1a8;">
        </li>
        <li>
          <div data-icon="&#xe1a9;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe1a9;">
        </li>
        <li>
          <div data-icon="&#xe1aa;" class="icon"></div>
          <input type="text" readonly="readonly" value="&amp;#xe1aa;">
        </li>
      </ul>
</fieldset>
      
    <script>(function() {
  var glyphs, i, len, ref;

  ref = document.getElementsByClassName('glyphs');
  for (i = 0, len = ref.length; i < len; i++) {
    glyphs = ref[i];
    glyphs.addEventListener('click', function(event) {
      if (event.target.tagName === 'INPUT') {
        return event.target.select();
      }
    });
  }

}).call(this);

    </script>


</div>

  	<!-- **** MOBILE DETECT CODE **** -->
      @include('partials.detectMobile')
	<?php 
		//global var containing the mobile type (android/iPhone/browser)
		$mobile_check = checkApps();
        $test_me = "Test variable";
        $user = Auth::user();
        $arrayPassedIn = str_split($user->shortcuts);

     	//arrays for message popup [message icon, message title, message body, button color, button ID, button icon, button label]
        $message1 = array("hmm-color", "message title", "message body", "red", "dismiss", "search", "dismiss");
        $popmessage = $message1;

    	//arrays for message popup [spinner color, center message, top message, lower message]
        $waitspinner1 = array("green", "one<br>moment", " ", "registering...");
        $spinnercontent = $waitspinner1;

        $brandCompany = env('COMPANY_NAME');
        $brandID = env('BRAND_NAME');
        $brandApp = env('BRAND_APP_NAME');        
	?>




	<script>
		var mobile_check = '<?php echo checkApps(); ?>';
	</script>
	
	<!-- this can be used to load platform-specific css or script. Anything in this if statement will only be included if the user's platform matches. -->	
	@if($mobile_check === "android")
		<!-- android specific code -->
	@elseif($mobile_check === "iPhone")
		<!-- iOS specific code -->
	@else 
		<!-- regular browser code -->
		<script>console.log('browser');</script>
	@endif
	
	<!-- **** END MOBILE DETECT CODE -->

@extends('layouts.zen5_layout')
@section('maincontent')

<script>
new WOW().init();
</script>

<div class="pageContent bgimage-bgheader pagebackground12 fade-in">
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                    <span>personal test page</span>
                </div>
                <div class="pageHeader__subTitle">
                    <span class="icon"><img src="images/icons/heart.png"></span><span class="subContent">subcontent here.</span>
                </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/plus.png">
            </div>
        </div>
    </div>

        

    <div class="contentBlock noPad">
        <div class="container noPad">

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> MODAL POPUPS</span>
                <div class="sectionDescription">Test area for any new modals.</div> 
                <div class="sectionContent">
                    <div style="height:160px;">
                        <div class="buttonCloud devButtons" style="display:block;">
                        <button class="showZenModal" data-modalcontent="#sampleModal">SAMPLE</button>
                        <button class="showZenModal" data-modalcontent="#user_card">UserCard</button>
                        <button class="showZenModal" data-modalcontent="#welcomeModal">welcome</button>
                        <button class="showZenModal" data-modalcontent="#popmessageModal">popup message</button>
                        <button class="showZenModal" data-modalcontent="#dateRangeModal">date range</button>
                        <button class="showZenModal" data-modalcontent="#dateToCurrentModal">date2current</button>
                        </div>
                    </div>
                </div>
                <span class="testMe">test text</span>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> HELP SYSTEM</span>
                <div class="sectionDescription">Setup for the help system.</div> 
                <div class="sectionContent">
                    <div class="divCloud">
                        <div class="helpBlockTest">
                            <div class="zBadge zHelp zRedBadge important tr-index1"><a class="showZenModal" id="dashboard-redBadge1" data-modalcontent="#dashboard-h1"><span class="icon"></span><span class="badgelabel">?</span></a></div>
                            <p><b class="FG__cyan">One-time help item.  This should go away via cookie after it's clicked once.</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                            <div class="buttonArray">
                                <button class="clearhelpCookie red"><div class="icon icon-question-circle"></div> reset help</button>
                            </div>                            
                        </div>
                        <div class="helpBlockTest">
                            <div class="zBadge zHelp attention tr-index1"><a class="showZenModal" data-modalcontent="#dashboard-h2"><span class="icon"></span><span class="badgelabel">?</span></a></div>
                            <p><b class="FG__cyan">Once-per-refresh help item.  This should go away, clicked per page view.</b> Nullam lobortis nunc sed metus vehicula, eget accumsan purus accumsan.</p>
                        </div>
                        <div class="helpBlockTest">
                            <div class="zBadge zHelp info tr-index1"><a class="showZenModal" data-modalcontent="#dashboard-h3"><span class="icon"></span><span class="badgelabel">?</span></a></div>
                            <p><b class="FG__cyan">Togglable help item.  This is permanant, unless globally toggled off.</b> Vestibulum enim tellus, mollis vel.</p>
                            <div class="buttonArray">
                                <button class="toggleHelp"><div class="icon icon-question-circle"></div> toggle help</button>
                            </div>
                        </div>
                    </div>
                    <div class="divCloud">
                        <div class="helpBlockTest">
                            <div class="zBadge zHelp zRedBadge important tr-index1"><a class="showZenModal" id="dashboard-redBadge2" data-modalcontent="#dashboard-h1"><span class="icon"></span><span class="badgelabel">?</span></a></div>
                            <p><i>Resetting help will clear the help cookies and restart.</i>  Nullam lobortis nunc sed metus vehicula, eget accumsan purus accumsan. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>                           
                        </div>
                        <div class="helpBlockTest">
                            <div class="zBadge zHelp attention tr-index1"><a class="showZenModal" data-modalcontent="#dashboard-h2"><span class="icon"></span><span class="badgelabel">?</span></a></div>
                            <p>Vestibulum enim tellus, mollis vel. Nullam lobortis nunc sed metus vehicula, eget accumsan purus accumsan.</p>
                        </div>
                        <div class="helpBlockTest">
                            <div class="zBadge zHelp info tr-index1"><a class="showZenModal" data-modalcontent="#dashboard-h3"><span class="icon"></span><span class="badgelabel">?</span></a></div>
                            <p>Nullam lobortis nunc sed metus vehicula, eget accumsan purus accumsan. Vestibulum enim tellus, mollis vel.</p>
                        </div>
                    </div>                    
                </div>
            </section>    

            

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> INFO PANEL SAMPLE</span>
                <div class="sectionDescription">Set your name, email, and phone number here.  You can also add a description to help other Zenjuries users or admins be aware of any additional info or special circumstances.  You can also update your password.</div> 
                <div class="sectionContent">
                    <ul class="sectionInfoPanel">
                    <li><span class="label">Name:</span> <span class="data" id="currentUserName">qqwer</span></li>
                    <li><span class="label">Email:</span> <span class="data"id="currentUserEmail">qwer</span></li>
                    <li><span class="label">Phone:</span> <span class="data"id="currentUserPhone">qwer</span></li>
                    <li><span class="label"> Description:</span> <span class="data"id="currentUserDescription">qwer</span></li>
                    </ul>
                    <ul class="sectionHelpPanel green">
                        <li class="sectionPanelIcon"><img class="wow animate__animated animate__headShake animate__repeat-2" src="/images/icons/greencheck.png"></li>
                        <li style="text-align:center;"><span class="label small FG__yellow">password updated: </span><span class="data small FG__orange" id="passwordChangedAt"><b>qwer</b></span></li>
                    </ul>
                    <br>
                    <div class="buttonArray">
                        <button class="showZenModal" data-modalcontent="#myInfoModal"><div class="icon icon-user"></div> update info</button>
                        <button class="showZenModal" data-modalcontent="#passwordModal"><div class="icon icon-lock"></div> update password</button>
                    </div>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> INFO BUTTON</span>
                <div class="sectionDescription">button with attached info, list,  or settings.</div> 
                <div class="sectionContent">
                    <fieldset class="infoButton teamList">
                        <legend><button class="showZenModal" data-modalcontent="#myInfoModal"><div class="icon icon-users"></div> default roles</button></legend>
                        <ul class="teamMember">
                            <li>john smith - <span class="displayRole inline role1"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role1</samp></li>
                            <li>karen jones - <span class="displayRole inline role9"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role9</samp></li>
                            <li>william hornsby - <span class="displayRole inline role7"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role7</samp></li>
                            <li>jermaine jackson - <span class="displayRole inline role4"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1r</span><samp>role4</samp></li>
                            <li>henry forbes - <span class="displayRole inline role8"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role8</samp></li>
                            <li>tony stark - <span class="displayRole inline role5"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role5</samp></li>
                            <li>jean grey - <span class="displayRole inline role3"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role3</samp></li>
                            <li>marvin sperk - <span class="displayRole inline role6"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role6</samp></li>
                            <li>helen johansen - <span class="displayRole inline role2"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role2</samp></li>
                            <li>bill billingsly - <span class="displayRole inline role10"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role10</samp></li>
                        </ul>
                    </fieldset>

                    <fieldset class="infoButton teamList">
                        <legend><button class="showZenModal" data-modalcontent="#myInfoModal"><div class="icon icon-user"></div> alpha team</button></legend>
                        <ul class="teamMember">
                            <li>willaim bell - <span class="displayRole inline role1"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>janet indigo - <span class="displayRole inline role2"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>miriom bestile - <span class="displayRole inline role7"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                        </ul>
                    </fieldset>

                    <fieldset class="infoButton teamList">
                        <legend><button class="showZenModal" data-modalcontent="#myInfoModal"><div class="icon icon-user"></div> beta team</button></legend>
                        <ul class="teamMember">
                            <li>karen screech - <span class="displayRole inline role1"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>oliver james - <span class="displayRole inline role9"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>herman kinsey - <span class="displayRole inline role7"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>poindexter meert - <span class="displayRole inline role4"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1r</span></li>
                            <li>paula green - <span class="displayRole inline role8"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>jeffery beans - <span class="displayRole inline role5"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>kevin leer - <span class="displayRole inline role3"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>heindrich hish - <span class="displayRole inline role6"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>ralph mouph - <span class="displayRole inline role2"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>marvin rivets - <span class="displayRole inline role10"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                        </ul>
                    </fieldset>
                </div>
            </section>            

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> IMAGE GRID SAMPLE</span>
                <div class="sectionDescription">Sample for the image grid/gallery and controls.</div> 
                <div class="sectionContent">
                    <div class="buttonArray">
                        <button class="showZenModal" data-modalcontent="#myInfoModal"><div class="icon icon-file-image-o"></div> add new photo</button>
                    </div>                    
                    <br>
                    <div class="formGrid photos">
                        <div class="formMedia">
                            <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2016/08/29/08/55/work-1627703_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                        </div>
                        <div class="formMedia">
                            <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2017/10/22/22/16/business-2879465_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                        </div>
                        <div class="formMedia">
                            <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2018/01/17/07/06/laptop-3087585_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                        </div>
                        <div class="formMedia">
                            <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2018/01/17/07/06/laptop-3087585_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                        </div>
                        <div class="formMedia">
                            <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2017/04/25/22/28/despaired-2261021_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">251k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                        </div>
                        <div class="formMedia">
                            <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2016/08/29/08/55/work-1627703_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                        </div>
                        <div class="formMedia">
                            <div class="uploadedMedia newPhoto" id="newPhotoUpload">
                                <span style="width:100%;text-align:center;top:30px;">Add new</span>
                                <span style="width:100%;text-align:center;top:48px;font-size:1.6rem;">photo</span>
                                <div style="width:100%;text-align:center;margin:0 auto;position:absolute;top:90px;">
                                    <div style="font-size:2.6rem" class="icon inline icon-file-image-o"></div>
                                    <div style="font-size:1.6rem;top:-5px;" class="icon inline icon-plus"></div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-exclamation-circle"></div> BADGE SAMPLES - linkable/positionable</span>
                <div class="sectionContent">
                    <div class="divCloud">
                        <div class="badgeTest">badge-checkmark<div class="zBadge checkmark tl-index1"><a><span class="icon"></span><span class="badgelabel"></span></a></div></div>
                        <div class="badgeTest">badge-numbered-item<div class="zBadge item tl-index1"><a><span class="icon"></span><span class="badgelabel">3</span></a></div></div>
                        <div class="badgeTest">badge-info<div class="zBadge info tl-index1"><a><span class="icon"></span><span class="badgelabel">?</span></a></div></div>
                        <div class="badgeTest">badge-alert<div class="zBadge alert tl-index1"><a><span class="icon"></span><span class="badgelabel">2</span></a></div></div>
                        <div class="badgeTest">badge-message<div class="zBadge message tl-index1"><a><span class="icon"></span><span class="badgelabel">3</span></a></div></div>
                        <div class="badgeTest">badge-new<div class="zBadge new tl-index1"><a><span class="icon"></span><span class="badgelabel">6</span></a></div></div>
                        <div class="badgeTest">badge-note<div class="zBadge note tl-index1"><a><span class="icon"></span><span class="badgelabel">2</span></a></div></div>
                        <div class="badgeTest">badge-cloud<div class="zBadge cloud tl-index1"><a><span class="icon"></span><span class="badgelabel">6</span></a></div></div>
                        <div class="badgeTest">badge-bookmark<div class="zBadge bookmark tl-index1"><a><span class="icon"></span><span class="badgelabel">3</span></a></div></div>
                        <div class="badgeTest">badge-heart<div class="zBadge heart tl-index1"><a><span class="icon"></span><span class="badgelabel">3</span></a></div></div>
                        <div class="badgeTest">badge-clipboard<div class="zBadge clipboard tl-index1"><a><span class="icon"></span><span class="badgelabel">3</span></a></div></div>
                        <div class="badgeTest">badge-[multiple index] - you can change the index number to position the badges across their position.  All indexes count inward.
                            <div class="zBadge item tl-index1"><a><span class="icon"></span><span class="badgelabel">1</span></a></div>
                            <div class="zBadge message tl-index2"><a><span class="icon"></span><span class="badgelabel">2</span></a></div>
                            <div class="zBadge alert tl-index3"><a><span class="icon"></span><span class="badgelabel">3</span></a></div>
                            <div class="zBadge heart tl-index4"><a><span class="icon"></span><span class="badgelabel">4</span></a></div>
                        </div>
                        <div class="badgeTest">badge-positions- you can position the badges on any of the corners (tl-index, tr-index, bl-index, br-index)
                            <div class="zBadge clipboard tl-index1"><a><span class="icon"></span><span class="badgelabel">7</span></a></div>
                            <div class="zBadge new tr-index1"><a><span class="icon"></span><span class="badgelabel">8</span></a></div>
                            <div class="zBadge note bl-index1"><a><span class="icon"></span><span class="badgelabel">9</span></a></div>
                            <div class="zBadge bookmark br-index1"><a><span class="icon"></span><span class="badgelabel">10</span></a></div>
                        </div>
                    </div>
                </div>                
            </section>

        </div>
    </div>
</div>

<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
    @include('partials.modals.waitOverlay')  
    <!-- CONTENT for help --> 
    @include('partials.help.dashboard-h1') 
    @include('partials.help.dashboard-h2') 
    @include('partials.help.dashboard-h3')  
    
    @include('partials.modals.modalSample')         
	@include('partials.modals.newWelcome')

	@include('partials.modals.popmessage')
    
    @include('partials.modals.dateRange')

    @include('partials.modals.dateToCurrent')
    @include('partials.modals.userCard')
</div>

<script>
    $('.showZenModalLocked').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true,
            closeButton: false,
            closeOnClick: false
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>
<script>
    $('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>




@endsection
  	<!-- **** MOBILE DETECT CODE **** -->
      @include('partials.detectMobile')
	<?php 
		//global var containing the mobile type (android/iPhone/browser)
		$mobile_check = checkApps();
        $test_me = "Test variable";
        $user = Auth::user();
        $arrayPassedIn = str_split($user->shortcuts);

     	//arrays for message popup [message icon, message title, message body, button color, button ID, button icon, button label]
        $message1 = array("hmm-color", "message title", "message body", "red", "dismiss", "search", "dismiss");
        $popmessage = $message1;

    	//arrays for message popup [spinner color, center message, top message, lower message]
        $waitspinner1 = array("green", "one<br>moment", " ", "registering...");
        $spinnercontent = $waitspinner1;

	?>



 
	<script>
		var mobile_check = '<?php echo checkApps(); ?>';
	</script>
	
	<!-- this can be used to load platform-specific css or script. Anything in this if statement will only be included if the user's platform matches. -->	
	@if($mobile_check === "android")
		<!-- android specific code -->
	@elseif($mobile_check === "iPhone")
		<!-- iOS specific code -->
	@else 
		<!-- regular browser code -->
		<script>console.log('browser');</script>
	@endif
	
	<!-- **** END MOBILE DETECT CODE -->

@extends('layouts.zen5_layout')
@section('maincontent')

<script>
new WOW().init();
</script>

<style>
    .badgeTest{position:relative;padding:20px 28px;text-align:center;width:max-content;margin:12px;border-radius:6px;border:1px solid #ffffff50;background-color:#00000050;}
    .helpBlockTest{position:relative;padding:8px;text-align:center;width:max-content;margin:10px;border-radius:3px;background-color:#00000050;}
</style>

<div class="pageContent bgimage-bgheader pagebackground12 fade-in">
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                    <span>test page</span>
                </div>
                <div class="pageHeader__subTitle">
                    <span class="icon"><img src="images/icons/heart.png"></span><span class="subContent">subcontent here.</span>
                </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/plus.png">
            </div>
        </div>
    </div>

        

    <div class="contentBlock noPad">
        <div class="container noPad">

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> MODAL POPUPS</span>
                <div class="sectionDescription">Test area for any new modals.</div> 
                <div class="sectionContent">
                    <div style="height:160px;">
                        <div class="buttonCloud devButtons" style="display:block;">
                        <button class="showZenModal" data-modalcontent="#sampleModal">SAMPLE</button>
                        <button class="showZenModal" data-modalcontent="#user_card">UserCard</button>
                        <button class="showZenModal" data-modalcontent="#welcomeModal">welcome</button>
                        <button class="showZenModal" data-modalcontent="#popmessageModal">popup message</button>
                        <button class="showZenModal" data-modalcontent="#dateRangeModal">date range</button>
                        <button class="showZenModal" data-modalcontent="#dateToCurrentModal">date2current</button>
                        <button class="showZenModal" data-modalcontent="#modalTabs">modal tabs</button>
                        <button class="showZenModal red" data-modalcontent="#testingRequest">TEST</button>
                        </div>
                    </div>
                    <div style="margin:0 auto;border:1px dashed grey;">
                        <span class="metricIcon" style="background-image:url('images/icons/date-alert.png">34<span>days</span></span>
                    </div>
                </div>
                <div class="buttonArray">
                    <button class="blue"><div class="icon icon-question-circle"></div> test button</button>
                    <a href="http://www.google.com" target="_blank"><button class="red"><div class="icon icon-question-circle"></div> test button with anchor</button></a>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> SCALE TEST</span>
                <div class="sectionDescription">Scaling test, mood test.</div> 
                <table style="width:50%;margin:0 auto;">
                    <tr>
                        <td style="text-align:right;">no class:</td>
                        <td><div class="mood myMood mood-1"><span><div class="moodVisual"><div class="currentMood"></div></div></span></div></td>
                    </tr>
                    <tr></tr>
                        <td style="text-align:right;">.smaller:</td>
                        <td><div class="mood myMood mood-2"><span><div class="moodVisual smaller"><div class="currentMood"></div></div></span></div></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;">.scale50:</td>
                        <td><div class="mood myMood mood-3"><span><div class="moodVisual scale50"><div class="currentMood"></div></div></span></div></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;">.scale60:</td>
                        <td><div class="mood myMood mood-4"><span><div class="moodVisual scale60"><div class="currentMood"></div></div></span></div></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;">.scale70:</td>
                        <td><div class="mood myMood mood-5"><span><div class="moodVisual scale70"><div class="currentMood"></div></div></span></div></td>
                    </tr>
                </table>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> HELP SYSTEM</span>
                <div class="sectionDescription">Setup for the help system.</div> 
                <div class="sectionContent">
                    <div class="divCloud">
                        <div class="helpBlockTest">
                            <div class="zBadge zHelp zRedBadge important tr-index1"><a class="showZenModal" id="dashboard-redBadge1" data-modalcontent="#dashboard-h1"><span class="icon"></span><span class="badgelabel">?</span></a></div>
                            <p><b class="FG__cyan">One-time help item.  This should go away via cookie after it's clicked once.</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                            <div class="buttonArray">
                                <button class="clearhelpCookie red"><div class="icon icon-question-circle"></div> reset help</button>
                            </div>                            
                        </div>
                        <div class="helpBlockTest">
                            <div class="zBadge zHelp attention tr-index1"><a class="showZenModal" data-modalcontent="#dashboard-h2"><span class="icon"></span><span class="badgelabel">?</span></a></div>
                            <p><b class="FG__cyan">Once-per-refresh help item.  This should go away, clicked per page view.</b> Nullam lobortis nunc sed metus vehicula, eget accumsan purus accumsan.</p>
                        </div>
                        <div class="helpBlockTest">
                            <div class="zBadge zHelp info tr-index1"><a class="showZenModal" data-modalcontent="#dashboard-h3"><span class="icon"></span><span class="badgelabel">?</span></a></div>
                            <p><b class="FG__cyan">Togglable help item.  This is permanant, unless globally toggled off.</b> Vestibulum enim tellus, mollis vel.</p>
                            <div class="buttonArray">
                                <button class="toggleHelp"><div class="icon icon-question-circle"></div> toggle help</button>
                            </div>
                        </div>
                    </div>
                    <div class="divCloud">
                        <div class="helpBlockTest">
                            <div class="zBadge zHelp zRedBadge important tr-index1"><a class="showZenModal" id="dashboard-redBadge2" data-modalcontent="#dashboard-h1"><span class="icon"></span><span class="badgelabel">?</span></a></div>
                            <p><i>Resetting help will clear the help cookies and restart.</i>  Nullam lobortis nunc sed metus vehicula, eget accumsan purus accumsan. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>                           
                        </div>
                        <div class="helpBlockTest">
                            <div class="zBadge zHelp attention tr-index1"><a class="showZenModal" data-modalcontent="#dashboard-h2"><span class="icon"></span><span class="badgelabel">?</span></a></div>
                            <p>Vestibulum enim tellus, mollis vel. Nullam lobortis nunc sed metus vehicula, eget accumsan purus accumsan.</p>
                        </div>
                        <div class="helpBlockTest">
                            <div class="zBadge zHelp info tr-index1"><a class="showZenModal" data-modalcontent="#dashboard-h3"><span class="icon"></span><span class="badgelabel">?</span></a></div>
                            <p>Nullam lobortis nunc sed metus vehicula, eget accumsan purus accumsan. Vestibulum enim tellus, mollis vel.</p>
                        </div>
                    </div>                    
                </div>
            </section>    

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> WAIT SPINNERS</span>
                <div class="sectionDescription">A selection of wait spinners.</div> 
                <div class="sectionContent">
                    <div class="divCloud">
                        <div class="center">
                            spinner.orange
                            <div class="waitSpinner">
                                <div class="spinnerContainer">
                                    <span class="spinner orange"></span>
                                    <div class="centerText animate__animated animate__pulse animate__infinite">Please wait...</div>
                                    <span class="topText">Title</span>
                                    <span class="bottomText">subtitle</span>
                                </div>
                            </div>
                        </div>
                        <div class="center">
                            spinner.blue
                            <div class="waitSpinner">
                                <div class="spinnerContainer">
                                    <span class="spinner blue"></span>
                                    <div class="centerText animate__animated animate__pulse animate__infinite">Loading...</div>
                                </div>
                            </div>
                        </div>
                        <div class="center">
                            spinner.green
                            <div class="waitSpinner">
                                <div class="spinnerContainer">
                                    <span class="spinner green"></span>
                                    <div class="centerText animate__animated animate__heartBeat animate__infinite animate__slow">One Moment...</div>
                                </div>
                            </div>
                        </div>
                        <div class="center">
                            spinner.purple
                            <div class="waitSpinner">
                                <div class="spinnerContainer">
                                    <span class="spinner purple"></span>
                                    <div class="centerText animate__animated animate__headShake animate__infinite animate__slow">Finding Files...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section class="sectionPanel">
                        <div class="waitPanelOverlay">
                            <div class="waitPanelCenter">
                                <div class="waitSpinner scale90">
                                    <div class="spinnerContainer">
                                        <span class="spinner purple"></span>
                                        <div class="centerText animate__animated animate__headShake animate__infinite animate__slow">Finding Files...</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> PANEL ONLY SPINNER</span>
                        <div class="sectionDescription">Look at this code for a panel-only spinner setup.</div> 
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet ultrices diam, eu sagittis metus. Sed vel odio sed nulla maximus vehicula id id sem. Nullam lobortis nunc sed metus vehicula, eget accumsan purus accumsan. Sed id nisl eget mauris ornare volutpat iaculis eget urna. Vestibulum enim tellus, mollis vel porttitor nec, pulvinar non purus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed aliquet mollis purus at ornare. Integer blandit massa eu aliquet consequat. Quisque pulvinar nisi eu dictum laoreet. Nulla porttitor purus egestas lectus tristique efficitur.</p>

                        <p>Phasellus mattis quam eu ultricies placerat. Ut velit erat, euismod tincidunt sollicitudin at, semper sed dui. Donec in ligula nulla. Suspendisse non nisi et ante gravida consequat malesuada in nunc. Etiam id ultricies risus. Vestibulum pellentesque convallis ultricies. Aliquam scelerisque feugiat aliquet. Proin tincidunt auctor tincidunt. Mauris porttitor neque a erat vehicula porttitor. Vestibulum ac ultrices erat. Proin ex purus, imperdiet auctor turpis vitae, mollis hendrerit eros. Proin consequat sollicitudin neque, hendrerit condimentum felis placerat vel.</p>

                        <p>Donec sit amet velit sapien. In hac habitasse platea dictumst. Nulla lobortis nisi sit amet urna posuere, ac semper massa aliquam. Aliquam erat volutpat. Sed et metus at ex pulvinar faucibus sit amet nec erat. Sed tincidunt eu urna id pretium. Morbi porta, dui non ultricies feugiat, odio orci ultrices urna, et efficitur massa ante quis lectus. Donec et bibendum odio. Sed placerat eget velit sed molestie. Vestibulum consequat ante vel ipsum accumsan accumsan.</p>

                        <p>Vestibulum id enim et erat viverra condimentum nec eget leo. Etiam fermentum cursus mauris, nec ultricies ipsum. Etiam leo eros, iaculis et ipsum ut, pharetra aliquet eros. Ut interdum, dui et imperdiet pharetra, enim erat gravida libero, ac egestas ex nunc vel nisl. Integer quis nisi id dui ornare vehicula ullamcorper eget nibh. Vivamus quis accumsan purus. Etiam facilisis sagittis elementum.</p>

                    </section>
                    <div class="center"><button class="showZenModalLocked" data-modalcontent="#waitspinnerModal"><div class="icon icon-magic"></div> wait... (locked full screen overlay)</button></div>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> INFO PANEL SAMPLE</span>
                <div class="sectionDescription">Set your name, email, and phone number here.  You can also add a description to help other Zenjuries users or admins be aware of any additional info or special circumstances.  You can also update your password.</div> 
                <div class="sectionContent">
                    <ul class="sectionInfoPanel">
                    <li><span class="label">Name:</span> <span class="data" id="currentUserName">qqwer</span></li>
                    <li><span class="label">Email:</span> <span class="data"id="currentUserEmail">qwer</span></li>
                    <li><span class="label">Phone:</span> <span class="data"id="currentUserPhone">qwer</span></li>
                    <li><span class="label"> Description:</span> <span class="data"id="currentUserDescription">qwer</span></li>
                    </ul>
                    <ul class="sectionHelpPanel green">
                        <li class="sectionPanelIcon"><img class="wow animate__animated animate__headShake animate__repeat-2" src="/images/icons/greencheck.png"></li>
                        <li style="text-align:center;"><span class="label small FG__yellow">password updated: </span><span class="data small FG__orange" id="passwordChangedAt"><b>qwer</b></span></li>
                    </ul>
                    <br>
                    <div class="buttonArray">
                        <button class="showZenModal" data-modalcontent="#myInfoModal"><div class="icon icon-user"></div> update info</button>
                        <button class="showZenModal" data-modalcontent="#passwordModal"><div class="icon icon-lock"></div> update password</button>
                    </div>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> INFO BUTTON</span>
                <div class="sectionDescription">button with attached info, list,  or settings.</div> 
                <div class="sectionContent">
                    <fieldset class="infoButton teamList">
                        <legend><button class="showZenModal" data-modalcontent="#myInfoModal"><div class="icon icon-users"></div> default roles</button></legend>
                        <ul class="teamMember">
                            <li>john smith - <span class="displayRole inline role1"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role1</samp></li>
                            <li>karen jones - <span class="displayRole inline role9"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role9</samp></li>
                            <li>william hornsby - <span class="displayRole inline role7"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role7</samp></li>
                            <li>jermaine jackson - <span class="displayRole inline role4"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1r</span><samp>role4</samp></li>
                            <li>henry forbes - <span class="displayRole inline role8"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role8</samp></li>
                            <li>tony stark - <span class="displayRole inline role5"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role5</samp></li>
                            <li>jean grey - <span class="displayRole inline role3"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role3</samp></li>
                            <li>marvin sperk - <span class="displayRole inline role6"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role6</samp></li>
                            <li>helen johansen - <span class="displayRole inline role2"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role2</samp></li>
                            <li>bill billingsly - <span class="displayRole inline role10"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span><samp>role10</samp></li>
                        </ul>
                    </fieldset>

                    <fieldset class="infoButton teamList">
                        <legend><button class="showZenModal" data-modalcontent="#myInfoModal"><div class="icon icon-user"></div> alpha team</button></legend>
                        <ul class="teamMember">
                            <li>willaim bell - <span class="displayRole inline role1"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>janet indigo - <span class="displayRole inline role2"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>miriom bestile - <span class="displayRole inline role7"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                        </ul>
                    </fieldset>

                    <fieldset class="infoButton teamList">
                        <legend><button class="showZenModal" data-modalcontent="#myInfoModal"><div class="icon icon-user"></div> beta team</button></legend>
                        <ul class="teamMember">
                            <li>karen screech - <span class="displayRole inline role1"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>oliver james - <span class="displayRole inline role9"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>herman kinsey - <span class="displayRole inline role7"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>poindexter meert - <span class="displayRole inline role4"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1r</span></li>
                            <li>paula green - <span class="displayRole inline role8"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>jeffery beans - <span class="displayRole inline role5"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>kevin leer - <span class="displayRole inline role3"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>heindrich hish - <span class="displayRole inline role6"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>ralph mouph - <span class="displayRole inline role2"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                            <li>marvin rivets - <span class="displayRole inline role10"><span class="roleIcon"></span><span class="roleTxt"></span></span><span class="title"> - title1</span></li>
                        </ul>
                    </fieldset>
                </div>
            </section>            

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-pencil-square-o"></div> IMAGE GRID SAMPLE</span>
                <div class="sectionDescription">Sample for the image grid/gallery and controls.</div> 
                <div class="sectionContent">
                    <div class="buttonArray">
                        <button class="showZenModal" data-modalcontent="#myInfoModal"><div class="icon icon-file-image-o"></div> add new photo</button>
                    </div>                    
                    <br>
                    <div class="formGrid photos">
                        <div class="formMedia">
                            <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2016/08/29/08/55/work-1627703_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                        </div>
                        <div class="formMedia">
                            <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2017/10/22/22/16/business-2879465_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                        </div>
                        <div class="formMedia">
                            <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2018/01/17/07/06/laptop-3087585_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                        </div>
                        <div class="formMedia">
                            <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2018/01/17/07/06/laptop-3087585_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                        </div>
                        <div class="formMedia">
                            <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2017/04/25/22/28/despaired-2261021_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">251k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                        </div>
                        <div class="formMedia">
                            <div class="uploadedMedia" style="background-image:url('https://cdn.pixabay.com/photo/2016/08/29/08/55/work-1627703_960_720.jpg');"><div class="mediaSelectOverlay showImage"></div><div class="mediaCaptionSummary">summary here very long summary here it issummary here very long summary here it issummary here very long summary here it issummary here very long summary here it is</div><span class="mediaInfo">231k | filename</span><span class="mediaDelete"></span><span class="mediaCaption"></span></div>
                        </div>
                        <div class="formMedia">
                            <div class="uploadedMedia newPhoto" id="newPhotoUpload">
                                <span style="width:100%;text-align:center;top:30px;">Add new</span>
                                <span style="width:100%;text-align:center;top:48px;font-size:1.6rem;">photo</span>
                                <div style="width:100%;text-align:center;margin:0 auto;position:absolute;top:90px;">
                                    <div style="font-size:2.6rem" class="icon inline icon-file-image-o"></div>
                                    <div style="font-size:1.6rem;top:-5px;" class="icon inline icon-plus"></div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-exclamation-circle"></div> BADGE SAMPLES - linkable/positionable</span>
                <div class="sectionContent">
                    <div class="divCloud">
                        <div class="badgeTest">badge-checkmark<div class="zBadge checkmark tl-index1"><a><span class="icon"></span><span class="badgelabel"></span></a></div></div>
                        <div class="badgeTest">badge-numbered-item<div class="zBadge item tl-index1"><a><span class="icon"></span><span class="badgelabel">3</span></a></div></div>
                        <div class="badgeTest">badge-info<div class="zBadge info tl-index1"><a><span class="icon"></span><span class="badgelabel">?</span></a></div></div>
                        <div class="badgeTest">badge-alert<div class="zBadge alert tl-index1"><a><span class="icon"></span><span class="badgelabel">2</span></a></div></div>
                        <div class="badgeTest">badge-message<div class="zBadge message tl-index1"><a><span class="icon"></span><span class="badgelabel">3</span></a></div></div>
                        <div class="badgeTest">badge-new<div class="zBadge new tl-index1"><a><span class="icon"></span><span class="badgelabel">6</span></a></div></div>
                        <div class="badgeTest">badge-note<div class="zBadge note tl-index1"><a><span class="icon"></span><span class="badgelabel">2</span></a></div></div>
                        <div class="badgeTest">badge-cloud<div class="zBadge cloud tl-index1"><a><span class="icon"></span><span class="badgelabel">6</span></a></div></div>
                        <div class="badgeTest">badge-bookmark<div class="zBadge bookmark tl-index1"><a><span class="icon"></span><span class="badgelabel">3</span></a></div></div>
                        <div class="badgeTest">badge-heart<div class="zBadge heart tl-index1"><a><span class="icon"></span><span class="badgelabel">3</span></a></div></div>
                        <div class="badgeTest">badge-clipboard<div class="zBadge clipboard tl-index1"><a><span class="icon"></span><span class="badgelabel">3</span></a></div></div>
                        <div class="badgeTest">badge-[multiple index] - you can change the index number to position the badges across their position.  All indexes count inward.
                            <div class="zBadge item tl-index1"><a><span class="icon"></span><span class="badgelabel">1</span></a></div>
                            <div class="zBadge message tl-index2"><a><span class="icon"></span><span class="badgelabel">2</span></a></div>
                            <div class="zBadge alert tl-index3"><a><span class="icon"></span><span class="badgelabel">3</span></a></div>
                            <div class="zBadge heart tl-index4"><a><span class="icon"></span><span class="badgelabel">4</span></a></div>
                        </div>
                        <div class="badgeTest">badge-positions- you can position the badges on any of the corners (tl-index, tr-index, bl-index, br-index)
                            <div class="zBadge clipboard tl-index1"><a><span class="icon"></span><span class="badgelabel">7</span></a></div>
                            <div class="zBadge new tr-index1"><a><span class="icon"></span><span class="badgelabel">8</span></a></div>
                            <div class="zBadge note bl-index1"><a><span class="icon"></span><span class="badgelabel">9</span></a></div>
                            <div class="zBadge bookmark br-index1"><a><span class="icon"></span><span class="badgelabel">10</span></a></div>
                        </div>
                    </div>
                </div>
                
            </section>


            <section class="sectionPanel">
                <div class="flexPanel"><div class="zBadge info tl-index1"><a class="showZenModal" data-modalcontent="#dashboard-h1"><span class="icon"></span><span class="badgelabel">?</span></a></div>
                    <div class="viewControl">
                        <div class="compactViewLeft animate__animated animate__bounceInLeft">
                            
                            <span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/insurance.png">
                            title
                            </span>
                        </div>
                        <div class="compactViewExpand hideMobile">[ expand ]</div>
                        <div class="compactViewRight animate__animated animate__bounceInRight">
                            <ul>
                                <li><span class="value FG__green"><b>0</b></span> <span class="icon"><img src="/images/icons/insurancenum.png"></span></li>
                            </ul>
                            <div class="compactViewX">[&nbsp;minimize&nbsp;]</div>
                        </div>
                    </div>
                    <div class="viewContent">
                        <div class="zen-actionPanel transparent tight">
                            <div class="sectionContent dynamicSize-pad">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Leo integer malesuada nunc vel risus. Ridiculus mus mauris vitae ultricies leo integer malesuada nunc. Viverra orci sagittis eu volutpat odio facilisis mauris sit amet. Id aliquet risus feugiat in ante metus. Facilisis volutpat est velit egestas dui id ornare. In fermentum posuere urna nec tincidunt praesent semper feugiat. Sed odio morbi quis commodo odio aenean sed adipiscing diam. Vel orci porta non pulvinar neque. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Cras tincidunt lobortis feugiat vivamus at augue. Odio ut enim blandit volutpat maecenas volutpat blandit aliquam.</p>
                                
                                <p>Ac placerat vestibulum lectus mauris ultrices. Id cursus metus aliquam eleifend mi in nulla posuere sollicitudin. Arcu odio ut sem nulla pharetra diam sit. Adipiscing tristique risus nec feugiat. Pharetra magna ac placerat vestibulum. Ligula ullamcorper malesuada proin libero nunc consequat interdum varius sit. Ac orci phasellus egestas tellus. Blandit massa enim nec dui nunc. Mi ipsum faucibus vitae aliquet nec ullamcorper. Quis auctor elit sed vulputate mi sit. Ut pharetra sit amet aliquam id. Diam maecenas sed enim ut sem viverra. Volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque.</p>

                                <p>Mi eget mauris pharetra et ultrices neque ornare. Nam libero justo laoreet sit amet cursus sit. Habitasse platea dictumst vestibulum rhoncus est. Vestibulum lectus mauris ultrices eros in cursus turpis massa. Cursus turpis massa tincidunt dui. Diam maecenas ultricies mi eget mauris pharetra. Vitae ultricies leo integer malesuada nunc vel risus. Netus et malesuada fames ac turpis egestas maecenas pharetra. Lobortis mattis aliquam faucibus purus. Pharetra massa massa ultricies mi quis hendrerit. Gravida quis blandit turpis cursus in hac habitasse. Nec feugiat in fermentum posuere urna nec. Risus nec feugiat in fermentum posuere urna nec. Venenatis a condimentum vitae sapien pellentesque habitant.</p>

                                <p>Turpis nunc eget lorem dolor sed viverra ipsum nunc aliquet. Viverra tellus in hac habitasse platea. Urna nunc id cursus metus aliquam eleifend. Ut venenatis tellus in metus vulputate eu. Imperdiet dui accumsan sit amet. Tempus iaculis urna id volutpat lacus. Faucibus et molestie ac feugiat sed lectus vestibulum. Quis varius quam quisque id diam vel quam elementum. Quis vel eros donec ac odio tempor orci dapibus ultrices. Felis imperdiet proin fermentum leo vel. Nibh venenatis cras sed felis eget velit. Ipsum dolor sit amet consectetur. Ac feugiat sed lectus vestibulum mattis. Viverra tellus in hac habitasse platea dictumst vestibulum rhoncus est. In tellus integer feugiat scelerisque varius morbi enim. Quisque id diam vel quam. In cursus turpis massa tincidunt dui ut. In dictum non consectetur a erat nam at lectus. Leo duis ut diam quam nulla porttitor massa id neque.</p>

                                <p>Sed vulputate odio ut enim blandit volutpat maecenas. Congue quisque egestas diam in arcu. Neque gravida in fermentum et sollicitudin. Penatibus et magnis dis parturient montes nascetur ridiculus. Ut sem viverra aliquet eget. In dictum non consectetur a erat nam at lectus. Eget dolor morbi non arcu risus quis varius quam. Sit amet justo donec enim diam vulputate. Mauris pharetra et ultrices neque ornare aenean euismod. Nisl tincidunt eget nullam non nisi est sit amet. Duis tristique sollicitudin nibh sit amet commodo nulla facilisi. Consequat nisl vel pretium lectus quam id leo in vitae. Iaculis urna id volutpat lacus laoreet non curabitur gravida. Id porta nibh venenatis cras sed felis eget. Vitae semper quis lectus nulla at.</p>

                                <p>Eget lorem dolor sed viverra ipsum nunc aliquet bibendum. Ultricies tristique nulla aliquet enim tortor at. Donec massa sapien faucibus et molestie ac feugiat sed. Pellentesque elit eget gravida cum. Semper viverra nam libero justo laoreet sit amet cursus. Faucibus et molestie ac feugiat. Blandit massa enim nec dui nunc mattis enim ut. Tincidunt tortor aliquam nulla facilisi cras fermentum. Eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Urna id volutpat lacus laoreet. At volutpat diam ut venenatis tellus in metus vulputate. Est ante in nibh mauris cursus mattis molestie a.</p>

                                <p>Molestie nunc non blandit massa enim nec dui. Amet volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Commodo odio aenean sed adipiscing diam donec adipiscing. Amet volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Vivamus at augue eget arcu dictum varius duis. Ullamcorper eget nulla facilisi etiam dignissim diam quis. Elit sed vulputate mi sit amet mauris commodo quis imperdiet. Vel turpis nunc eget lorem dolor sed viverra. Ullamcorper morbi tincidunt ornare massa eget egestas purus viverra. Arcu non odio euismod lacinia at quis risus. Maecenas sed enim ut sem viverra aliquet eget sit amet. Enim facilisis gravida neque convallis a cras semper auctor neque.</p>
                            </div>					
                        </div>
                        <div class="pagination">
                            <button class="pagePrev ghost">prev</button>
                            <div class="pages" id="pageInfoSpan">showing x of x</div>
                            <button class="pageNext ">next</button>
                        </div>
                    </div>				
                </div>

                <div class="flexPanel">
                    <div class="viewControl">
                        <div class="compactViewLeft animate__animated animate__bounceInLeft">
                            <span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/heart.png">
                            title 2
                            </span>
                        </div>
                        <div class="compactViewExpand hideMobile">[ expand ]</div>
                        <div class="compactViewRight animate__animated animate__bounceInRight">
                            <ul>
                                <li><span class="value FG__green"><b>0</b></span> <span class="icon"><img src="/images/icons/heart.png"></span></li>
                            </ul>
                            <div class="compactViewX">[&nbsp;minimize&nbsp;]</div>
                        </div>
                    </div>
                    <div class="viewContent">
                        <div class="zen-actionPanel transparent tight">
                            <div class="sectionContent dynamicSize-nopad">
                                <p>Eget lorem dolor sed viverra ipsum nunc aliquet bibendum. Ultricies tristique nulla aliquet enim tortor at. Donec massa sapien faucibus et molestie ac feugiat sed. Pellentesque elit eget gravida cum.</p>
                            </div>					
                        </div>
                        <div class="pagination">
                            <button class="pagePrev ghost">prev</button>
                            <div class="pages" id="pageInfoSpan">showing x of x</div>
                            <button class="pageNext ">next</button>
                        </div>
                    </div>				
                </div> 

                <p style="width:1010px;margin:0 auto;">Turpis nunc eget lorem dolor sed viverra ipsum nunc aliquet. Viverra tellus in hac habitasse platea. Urna nunc id cursus metus aliquam eleifend. Ut venenatis tellus in metus vulputate eu. Imperdiet dui accumsan sit amet. Tempus iaculis urna id volutpat lacus. Faucibus et molestie ac feugiat sed lectus vestibulum. Quis varius quam quisque id diam vel quam elementum. Quis vel eros donec ac odio tempor orci dapibus ultrices. Felis imperdiet proin fermentum leo vel. Nibh venenatis cras sed felis eget velit. Ipsum dolor sit amet consectetur. Ac feugiat sed lectus vestibulum mattis. Viverra tellus in hac habitasse platea dictumst vestibulum rhoncus est. In tellus integer feugiat scelerisque varius morbi enim. Quisque id diam vel quam. In cursus turpis massa tincidunt dui ut. In dictum non consectetur a erat nam at lectus. Leo duis ut diam quam nulla porttitor massa id neque.</p>

                <div class="flexPanel">
                    <div class="viewControl">
                        <div class="compactViewLeft animate__animated animate__bounceInLeft">
                            <span class="sectionTitle wide"><img class="sectionIcon" src="/images/icons/plus.png">
                            title 3
                            </span>
                        </div>
                        <div class="compactViewExpand hideMobile">[ expand ]</div>
                        <div class="compactViewRight animate__animated animate__bounceInRight">
                            <ul>
                                <li><span class="value FG__green"><b>0</b></span> <span class="icon"><img src="/images/icons/plus.png"></span></li>
                            </ul>
                            <div class="compactViewX">[&nbsp;minimize&nbsp;]</div>
                        </div>
                    </div>
                    <div class="viewContent">
                        <div class="zen-actionPanel transparent tight">
                            <div class="sectionContent dynamicSize">
                                <p>Sed vulputate odio ut enim blandit volutpat maecenas. Congue quisque egestas diam in arcu. Neque gravida in fermentum et sollicitudin. Penatibus et magnis dis parturient montes nascetur ridiculus. Ut sem viverra aliquet eget. In dictum non consectetur a erat nam at lectus. Eget dolor morbi non arcu risus quis varius quam. Sit amet justo donec enim diam vulputate. Mauris pharetra et ultrices neque ornare aenean euismod. Nisl tincidunt eget nullam non nisi est sit amet. Duis tristique sollicitudin nibh sit amet commodo nulla facilisi. Consequat nisl vel pretium lectus quam id leo in vitae. Iaculis urna id volutpat lacus laoreet non curabitur gravida. Id porta nibh venenatis cras sed felis eget. Vitae semper quis lectus nulla at.</p>

                                <p>Eget lorem dolor sed viverra ipsum nunc aliquet bibendum. Ultricies tristique nulla aliquet enim tortor at. Donec massa sapien faucibus et molestie ac feugiat sed. Pellentesque elit eget gravida cum. Semper viverra nam libero justo laoreet sit amet cursus. Faucibus et molestie ac feugiat. Blandit massa enim nec dui nunc mattis enim ut. Tincidunt tortor aliquam nulla facilisi cras fermentum. Eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Urna id volutpat lacus laoreet. At volutpat diam ut venenatis tellus in metus vulputate. Est ante in nibh mauris cursus mattis molestie a.</p>

                                <p>Molestie nunc non blandit massa enim nec dui. Amet volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Commodo odio aenean sed adipiscing diam donec adipiscing. Amet volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Vivamus at augue eget arcu dictum varius duis. Ullamcorper eget nulla facilisi etiam dignissim diam quis. Elit sed vulputate mi sit amet mauris commodo quis imperdiet. Vel turpis nunc eget lorem dolor sed viverra. Ullamcorper morbi tincidunt ornare massa eget egestas purus viverra. Arcu non odio euismod lacinia at quis risus. Maecenas sed enim ut sem viverra aliquet eget sit amet. Enim facilisis gravida neque convallis a cras semper auctor neque.</p>
                            </div>					
                        </div>
                        <div class="pagination">
                            <button class="pagePrev ghost">prev</button>
                            <div class="pages" id="pageInfoSpan">showing x of x</div>
                            <button class="pageNext ">next</button>
                        </div>
                    </div>				
                </div>                        
                
            </section>

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-camera"></div> camera test</span>
                <div class="center" style="padding-top:20px;">

                    <div class="formGrid''">  
                        <div class="formInput">
                            <label>click to take a picture.</label>
                            <input type="file" accept="image/*" capture="camera" multiple class="camera-upload">                 
                        </div>
                    </div>
                </div>
            </section>  

            <section class="sectionPanel">
                <div class="chartContainer">
                    <div class="darkChart wow animate__animated animate__fadeIn">
                        <span class="chartTitle center">zenDex Score</span>
                        <div class="arcGauge">
                            <div class="arcGaugeMask">
                                <div class="arcGaugeContainer">
                                    <div class="arcGaugeTrack"></div>
                                    <div class="arcArc" style='--arcGaugeValue:111deg;'>
                                        <div class="arcGaugeArc yellow"></div>
                                    </div>
                                </div>
                            </div>
                            <span class="arcLabel1">label</span>
                            <span class="arcValue">85</span>
                            <span class="arcLabel2">improving</span>
                        </div>                                
                    </div>
                </div>


                <h2>animated bar test</h2>
                <div class="hBarGraph"><div class="fillBar" style="width:50%;--barGraphBarBG:red;"></div><div></div></div>
                <div class="hBarGraph"><div class="fillBar" style="width:70%;--barGraphBarBG:orange;"></div>Data 2</div>
                <div class="hBarGraph"><div class="fillBar" style="width:30%;--barGraphBarBG:yellow;"></div>Data 3</div>
                <div class="hBarGraph"><div class="fillBar" style="width:80%;--barGraphBarBG:green;"></div>Data 4</div>
                <div class="hBarGraph"><div class="fillBar" style="width:20%;--barGraphBarBG:blue;"></div>Data 5</div>

                <hr>
                <h2>touch date/time test mobile</h2>
                <div class="center"><input type="text" placeholder="set date" ontouchstart="(this.type='date')" class="mobileDateTime"></div>
                <div class="center"><input type="text" placeholder="set time" ontouchstart="(this.type='time')" class="mobileDateTime"></div>
                <hr>
                <fieldset class="disclaimer">
                    <legend>disclaimer title</legend>
                    disclaimer content
                </fieldset>
            </section> 

            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-bar-chart"></div> js charts test</span>
                <div class="sectionContent" style="text-align:center;">
                    <div style="width:380px;height:400px;margin:0 20px;display:inline-block;">
                        <canvas id="myChart"></canvas>
                    </div>

                    <div style="width:380px;height:400px;margin:0 20px;display:inline-block">
                        <canvas id="myChart2"></canvas>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>

<div id="zenModal" class="zModal" style="display:none">
    <!-- CONTENT for modals -->
    @include('partials.modals.waitOverlay')  
    <!-- CONTENT for help --> 
    @include('partials.help.dashboard-h1') 
    @include('partials.help.dashboard-h2') 
    @include('partials.help.dashboard-h3')  
    
    @include('partials.modals.modalSample')         
	@include('partials.modals.newWelcome')

	@include('partials.modals.popmessage')
    
    @include('partials.modals.dateRange')

    @include('partials.modals.dateToCurrent')
    @include('partials.modals.userCard')
    @include('partials.modals.modalTabSample')

    @include('partials.modals.testingrequest')
</div>

<script>
    $('.showZenModalLocked').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true,
            closeButton: false,
            closeOnClick: false
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>
<script>
    $('.showZenModal').on('click', function(){
        //initialize the modal
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#zenModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
		//modal ID goes here
		modal.open();
    });
</script>



    <!-- CREATE CHART 1-->
    <script>
        const labels = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
        ];

        const data = {
            labels: labels,
            datasets: [{
                label: 'some Zenjuries Data',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: [0, 10, 5, 2, 20, 30, 45],
            }]
        };

        const config = {
            type: 'line',
            data: data,
            options: {}
        };
    </script>
    <!-- RENDER CHART 1-->
    <script>
        const myChart = new Chart(
            document.getElementById('myChart'),
            config
        );
    </script>

    <!-- CREATE CHART 2 -->
    <script>
        const ctx = document.getElementById('myChart2');
        const myChart2 = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                datasets: [{
                    label: '# of Users',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>
    <!-- RENDER CHART 2-->
    <script>
        const myChart2 = new Chart(
            document.getElementById('myChart2'),
            config
        );
    </script>

    <!-- BASE MODAL HTML -->
    <div id="testModal" class="zModal" style="display:none">

        <!-- SWAPPABLE CONTENT, with IDs that match the modalcontent attribute for its respective button
            Should also contain the modalBlock class -->
        <div class="modalContent modalBlock" id="contentBlock1" style="display:none">
            <div class="modalHeader"><span class="modalTitle">Block 1</span></div>
            <div class="modalBody" style="min-width:260px;">
                <div class="modalContent">
                    Showing block 1. here's some random content to be shown.
                </div>
            </div>
        </div>

        <div class="modalContent modalBlock" id="contentBlock2" style="display:none">
            <div class="modalHeader"><span class="modalTitle">Block 2</span></div>
            <div class="modalBody" style="min-width:260px;">
                <div class="modalContent">
                    Showing block 2. 1239oifdkalkewoio
                </div>
            </div>
        </div>

        <div class="modalContent modalBlock" id="contentBlock3" style="display: none">
            <div class="modalHeader"><span class="modalTitle">Block 3</span></div>
            <div class="modalBody" style="min-width:260px;">
                <div class="modalContent">
                    Showing block 3. asdfdlsfkalksdaflsdklf12321312
                </div>
            </div>
        </div>
    </div>

    <script>
        showAlert("testing alerts", "confirm", 5);

        //modal globalVar to contain the reference to the modal
        var modal;

        //THIS FUNCTION SHOULD BE easy to drop on a page, the class triggering the onclick and the modal name should be the main changes.
        $('.showTestModal').on('click', function(){
            //initialize the modal
            modal = new jBox('Modal', {
                addClass: 'zBox',
                //modal ID goes here
                content: $('#testModal'),
                isolateScroll: true
            });
            //get the content for the modal
            var target = $(this).data('modalcontent');
            //hide all content blocks
            $('.modalBlock').hide();
            //show the target
            $(target).show();

            //script related to a specific content block can be added like this
            //status modal
            if(target === "#contentBlock1"){
                //scrpit for contentBlock1
            }
            modal.open();
        });
    </script>


    <script>
        $(document).ready(function() {
            var favArray = <?php echo json_encode($arrayPassedIn); ?>;
            var zHelpClasses = document.getElementsByClassName('zHelp');

            if(favArray[1] == 1){
                for(var i = 0; i < zHelpClasses.length; i++){
                    zHelpClasses[i].style = "display:none";
                }
            }
            else{
                for(var i = 0; i < zHelpClasses.length; i++){
                    zHelpClasses[i].style = "display:inline block";
                    var idOfBadge = zHelpClasses[i].firstChild.id;
                    var trimIdOfBadge = idOfBadge.charAt(idOfBadge.length - 1);
                    if(localStorage.getItem("badgeCookie" + trimIdOfBadge) == trimIdOfBadge){
                        zHelpClasses[i].style = "display:none";
                    }
                }
            }

            $('.toggleHelp').on('click', function(){
                if(favArray[1] == 0){
                    favArray[1] = 1;
                    updateStringPassedIn();
                    for(var i = 0; i < zHelpClasses.length; i++){
                        zHelpClasses[i].style = "display:none";
                    }
                }
                else{
                    favArray[1] = 0;
                    updateStringPassedIn();
                    for(var i = 0; i < zHelpClasses.length; i++){
                        zHelpClasses[i].style = "display:inline block";
                        var idOfBadge = zHelpClasses[i].firstChild.id;
                        var trimIdOfBadge = idOfBadge.charAt(idOfBadge.length - 1);
                        if(localStorage.getItem("badgeCookie" + trimIdOfBadge) == trimIdOfBadge){
                            zHelpClasses[i].style = "display:none";
                        }
                    }
                }
            });

            $('.clearhelpCookie').on('click', function(){
                var zRedBadges = document.getElementsByClassName('zRedBadge');
                for(var i = 0; i < zRedBadges.length; i++){
                    localStorage.setItem("badgeCookie" + (i + 1), ""); 
                    if(favArray[1] == 0){
                        zRedBadges[i].style = "display:inline block"
                    }
                }

            });

            function updateStringPassedIn(){
        	    var stringPassedIn = favArray.toString().replaceAll(",", "");
        	    $.ajax({
        		    	type: "POST",
        		    	url: "{{ route('updateUserShortcuts') }}",
        		    	data: {
            	    	_token: '<?php echo csrf_token(); ?>',
            	    	user_id: {{$user->id}},
            	    	shortcuts: stringPassedIn
    			    }
    		    });
    	    }
            
            $('.showZenModal').on('click', function(){
                var modalContentId = $(this).attr("data-modalcontent");
                var typeOfBadge = modalContentId.charAt(modalContentId.length - 1);
                if(typeOfBadge == "1"){
                    var id = $(this).attr("id");
                    var numOfId = id.charAt(id.length - 1);
                    this.parentElement.style = "display:none";
                    localStorage.setItem("badgeCookie" + numOfId, numOfId);
                }
                else if(typeOfBadge == "2"){
                    this.parentElement.style = "display:none";
                }
                else if(typeOfBadge == "3"){
                    return;
                }
            });
        });
    </script>

@endsection
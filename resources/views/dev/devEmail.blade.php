@extends('layouts.zen5_layout_simple')
@section('maincontent')
<?php 
$emails =  DB::table('communication_templates')->get();
?>
<style>
    span.desc{display:block;width:100%;text-align:center;font-size:.8rem;font-weight:300;margin-top:12px;margin-bottom:5px;height:40px;overflow:hidden;text-overflow:ellipsis;padding:0 6px;}
    span.trigger{display:block;width:100%;text-align:center;font-size:.8rem;font-weight:300;margin-top:12px;margin-bottom:5px;height:40px;overflow:hidden;text-overflow:ellipsis;padding:0 6px;}
    span.recipient{display:block;width:100%;text-align:center;font-size:.8rem;font-weight:300;margin-top:12px;margin-bottom:5px;height:40px;overflow:hidden;text-overflow:ellipsis;padding:0 6px;}
    span.url{display:block;width:100%;text-align:center;color:#556677;font-size:.8rem;margin-bottom:12px;}
    .devLinkContainer {width:100%;height:100px;text-align:center;padding:10px;}
    .devLink {height:100px;width:100%;border-radius:12px;border:1px solid black;text-align:center;background-color:#444;}
    .itemGrid .itemGrid__item.thirds{min-height:90px;height:120px;}
</style>

<div class="center"><img src="/branding/default/images/logo_stacked_dark.png" width="180px"></div>

<div style="margin:5%;padding:5%;padding-top:0;margin-top:12px;">
<h2>email System Templates</h2>
<hr><br>

<div class="itemGrid">
    @foreach($emails as $key => $email)
    <div class="itemGrid__item thirds">
        <div class="devLinkContainer">
            <div class="devLink">
                <span class="desc">{{$email->description}}</span>
                <button class="black small" onclick="location.href='/devEmailTemplateView/{{$email->template_identifier}}';"><div class="icon icon-envelope-o"></div> <span style="font-size:.6rem;position:relative;top:-1px;">{{$email->name}}</span></button>    
            </div>    
        </div>
    </div>   
    @endforeach
</div>

<div class="itemGrid">

    <div class="itemGrid__item thirds">
        <div class="devLinkContainer">
            <div class="devLink">
                <span class="desc">description</span>
                <span class="trigger">trigger: what triggers mail</span>
                <span class="recipient">recipient: person 1, person 2</span>
                <button class="black small" onclick="location.href='/devEmailTemplateView/{{$email->template_identifier}}';"><div class="icon icon-envelope-o"></div> <span style="font-size:.6rem;position:relative;top:-1px;">{{$email->name}}</span></button>    
            </div>    
        </div>
    </div>   

</div>







<!--
    <div class="itemGrid__item thirds">
        <div class="devLinkContainer">
            <div class="devLink">
                <span class="desc">description of this email</span>
                <button class="zenblue" onclick="location.href='#';">email title</button>    
            </div>    
        </div>
    </div>    
    <div class="itemGrid__item thirds">
        <div class="devLinkContainer">
        <div class="devLink">
                <span class="desc">description of this email</span>
                <button class="zenblue" onclick="location.href='#';">email title</button>    
            </div>    
        </div>
    </div>
    <div class="itemGrid__item thirds">
        <div class="devLinkContainer">
        <div class="devLink">
                <span class="desc">description of this email</span>
                <button class="zenblue" onclick="location.href='#';">email title</button>    
            </div>     
        </div>
    </div>
--> 

<!--
<div class="itemGrid">
    <div class="itemGrid__item thirds">
        <div class="devLinkContainer">
        <div class="devLink">
                <span class="desc">description of this email</span>
                <button class="zenblue" onclick="location.href='#';">email title</button>    
            </div>    
        </div>
    </div>    
    <div class="itemGrid__item thirds">
        <div class="devLinkContainer">
        <div class="devLink">
                <span class="desc">description of this email</span>
                <button class="zenblue" onclick="location.href='#';">email title</button>    
            </div>    
        </div>
    </div>
    <div class="itemGrid__item thirds">
        <div class="devLinkContainer">
        <div class="devLink">
                <span class="desc">description of this email</span>
                <button class="zenblue" onclick="location.href='#';">email title</button>    
            </div>   
        </div>
    </div>    
</div>
-->
</div>

@extends('layouts.zen5_layout_guest')
@section('maincontent')
<?php 
use Carbon\Carbon;
$user = Auth::user();

$theme = $user->zengarden_theme;
$theme = str_replace("theme", "", $theme);
$theme = strtolower($theme);
//new theme values
$brandID = env('BRAND_NAME');
$themeID = 'theme' . $user->theme_id;
$currentTeamId = $user->current_team_id;
$teamName =  \App\Team::where('id', $currentTeamId)->value('name');
$arrayPassedIn = str_split($user->shortcuts);

//getting timestamp for last password change
$passwordUpdatedAt = new DateTime($user->password_updated_at);
$passwordUpdatedAt = $passwordUpdatedAt->format('m-d-Y h:i A');
?>

<div class="pageContent bgimage-bgheader fade-in" style="background-color:#333;">		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>code testing</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/gear.png"></span><span class="subContent">submit finished function for testing</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/bug.png">
            </div> 
        </div>           
	</div>

    <div class="contentBlock lessPadding">
		<div class="container setMinHeight">
            <section class="sectionPanel">
                <span class="sectionTitle"><div class="icon icon-bug"></div> initiate testing</span>
                <div class="sectionDescription">Select your project and task, and add a short description if necessary to assist in the testing procedure. Your completed function from project management will be go into the testing queue and will be assigned to the next available QA tech.
                <span class="FG__yellow note">There are 2 levels of standard functionality testing.  If you think your code will require some more in-depth testing select a deep test, which will trigger testing of your code against related funtions.  Otherwise select basic for a standard test.</span>
                </div> 
                <div class="sectionContent">
                    <section class="formBlock dark">
                        <div class="formGrid">  
                            <div class="formInput">
                                <!-- input -->
                                <label for="phone">developer</label>
                                <div class="inputIcon user"><input id="developer" type="text" placeholder="login id"/></div>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="agency_name">priority</label>
                                <div class="inputIcon alert">
                                    <select id="priority" name="priority" style="width:212px;">
                                    <option value="low" style="color:cyan;">low</option>
                                    <option value="standard" style="color:yellow;" selected>standard</option>
                                    <option value="high" style="color:orange;">high</option>
                                    <option value="emergency" style="color:red;">emergency</option>
                                    </select>
                                </div>
                            </div> 
                        </div>
                        <div class="formGrid"> 
                            <div class="formInput">
                                <!-- input -->
                                <label for="agency_name">project</label>
                                <div class="inputIcon cube">
                                    <select id="project" name="project" style="width:212px;">
                                    <option value="low" style="color:cyan;">from AC</option>
                                    </select>
                                </div>
                            </div>
                            <div class="formInput">
                                <!-- input -->
                                <label for="agency_name">task</label>
                                <div class="inputIcon cubes">
                                    <select id="task" name="task" style="width:212px;">
                                    <option value="low" style="color:cyan;">from AC</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </section>
                    <br>
                    <div class="buttonArray">
                        <button class="level1test orange" data-modalcontent="#myInfoModal"><div class="icon icon-bug"></div> initiate basic test</button>
                        <button class="level2test red" data-modalcontent="#passwordModal"><div class="icon icon-bug"></div> initiate deep test</button>
                    </div>
                </div>
            </section>

		</div>
	</div>
</div>

@endsection

@section('devcontent')
<div class="buttonArray">
    <button class="red centered small"><div class="icon icon-retweet"></div> report error</button>
    <button class="cyan centered small"><div class="icon icon-retweet"></div> comment</button>
</div> 
@endsection

@section('testcontent')
<div class="buttonArray">
    <button class="red centered small"><div class="icon icon-retweet"></div> report error</button>
    <button class="cyan centered small"><div class="icon icon-retweet"></div> comment</button>
</div> 
@endsection
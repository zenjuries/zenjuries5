@extends('layouts.zen5_layout_simple')
@section('maincontent')
<?php
$queuedMessage = new stdClass();
$queuedMessage->user_id = 700;
$queuedMessage->injury_id = 92;
$queuedMessage->communication_id = 1249;
$queuedMessage->company_id = 100;
if($template === "FirstReportofInjury"){
    $template = "firstReport";
}
if($template === "FirstReportofInjuryReminder"){
    $tempalte = "firstReportReminder";
}
if($template === "getAppointmentDetails"){
    $template = "getFirstAppointmentDetails";
}

echo "Template before loading email: " . $template;

?>
<div class="center"><button onclick="history.back()">go back</button></div>
<div>

@includeIf('emailTemplates.' . $template . 'Email')
<?php
echo $template . 'Email<br>';
$template = lcfirst($template);
echo $template . 'Email<br>';
?>
@includeIf('emailTemplates.' . $template . 'Email')
<?php 
if(strlen($template) > 6){
    $template[5] = strtoupper($template[5]);
    echo $template . 'Email';
}
?>
@includeIf('emailTemplates.' . $template . 'Email')
</div>

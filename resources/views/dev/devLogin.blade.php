@extends('layouts.zen5_layout_guest')
@section('maincontent')

<style>
    span.title{display:block;width:100%;text-align:center;font-size:.9rem;margin-top:12px;}
    span.url{display:block;width:100%;text-align:center;color:#556677;font-size:.8rem;margin-bottom:12px;}
    .devLinkContainer {width:100%;height:100px;text-align:center;padding:10px;}
    .devLink {height:92px;width:100%;border-radius:8px;text-align:center;
box-shadow: 1px 5px 20px -2px rgba(0,0,0,0.72);
-webkit-box-shadow: 1px 5px 20px -2px rgba(0,0,0,0.72);
-moz-box-shadow: 1px 5px 20px -2px rgba(0,0,0,0.72);
padding-top:1px;padding-bottom:1px;
background-color:#383838;
}
    .itemGrid .itemGrid__item.thirds{min-height:90px;height:120px;}
</style>

<div class="pageContent bgimage-bgheader fade-in" style="background-color:#333;">		
	<div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                <span>dev resources</span></b></span>
                </div>
                <div class="pageHeader__subTitle">
                        <span class="icon"><img src="images/icons/power.png"></span><span class="subContent">links to dev tools</span>
                    </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/userinfo.png">
            </div> 
        </div>           
	</div>

    <div class="contentBlock lessPadding">
		<div class="container setMinHeight">


            <div class="itemGrid">
                <div class="itemGrid__item thirds">
                    <div class="devLinkContainer">
                        <div class="devLink">
                            <span class="title">Simple user auto-login </span>
                            <button class="zenblue" onclick="window.location.href='/autoLogin/user'">Login as User</button>
                            <span class="url">/autoLogin/user</span>
                        </div>    
                    </div>
                </div>    
                <div class="itemGrid__item thirds">
                <div class="devLinkContainer">
                        <div class="devLink">
                            <span class="title">ZAgent auto-login</span>
                            <button class="zengreen" onclick="window.location.href='/autoLogin/zagent'">Login as ZAgent</button>
                            <span class="url">/autoLogin/zagent</span>
                        </div>    
                    </div>
                </div>
                <div class="itemGrid__item thirds">
                <div class="devLinkContainer">
                        <div class="devLink">
                            <span class="title">zenpro auto-login</span>
                            <button class="purple" onclick="window.location.href='/autoLogin/zenpro'">Login as Zenpro</button>
                            <span class="url">/autoLogin/zenpro</span>
                        </div>                                   
                    </div>
                </div>
            </div>

            <div class="itemGrid">
                <div class="itemGrid__item thirds">
                <div class="devLinkContainer">
                        <div class="devLink">
                            <span class="title">aggregate auto-login</span>
                            <button class="orange" onclick="window.location.href='/aggregate_list'">Login as Aggregate</button>
                            <span class="url">/aggregate_list</span>
                        </div>    
                    </div>
                </div>    
                <div class="itemGrid__item thirds">
                <div class="devLinkContainer">
                        <div class="devLink">
                            <span class="title">admin auto-login</span>
                            <button class="red" onclick="window.location.href='/admin_list'">Login as Admin</button>
                            <span class="url">/zenadmin_list</span>
                        </div>    
                    </div>
                </div>
                <div class="itemGrid__item thirds">
                <div class="devLinkContainer">
                    <div class="devLink">
                            <span class="title">admin dashboards</span>
                            <button class="small" onclick="window.location.href='/admin'">admin</button><br>
                            <button class="small" onclick="window.location.href='/superadmin'">suparadmin</button>
                        </div>                                  
                    </div>
                </div>
            </div>

            <div class="itemGrid">
                <div class="itemGrid__item thirds">
                <div class="devLinkContainer">
                        <div class="devLink">
                            <span class="title">zenjuries registration</span>
                            <button class="small" onclick="window.location.href='/userRegister'">user Register invalid</button><br>
                            <button class="small" onclick="window.location.href='/userRegister?invitation=cymGG02MG3c6vDnEnV2rEzAvfAX3dfVcNAWpi5Gh'">user Register</button>
                        </div>    
                    </div>
                </div>    
                <div class="itemGrid__item thirds">
                <div class="devLinkContainer">
                        <div class="devLink">
                            <span class="title">zenjuries registration</span>
                            <button onclick="window.location.href='/agencyRegister'">agency Register</button>
                            <span class="url">/agencyRegister</span>
                        </div>    
                    </div>
                </div>
                <div class="itemGrid__item thirds">
                <div class="devLinkContainer">
                        <div class="devLink">
                            <span class="title">zenjuries registration</span>
                            <button class="small" onclick="window.location.href='/verifyAccountNewPolicyHolder/jiVSFtBDT6coP6p6WyoQ/4503/1707'">Policyholder Paid</button><br>
                            <button class="small" onclick="window.location.href='/verifyAccountNewPolicyHolder/4GsLd9S4fN3b3Z4rI9Ui/4504/1708'">Policyholder NoPay</button>
                        </div>                                  
                    </div>
                </div>
            </div>

            <div class="itemGrid">
                <div class="itemGrid__item thirds">
                <div class="devLinkContainer">
                        <div class="devLink">
                            <span class="title">Main Login</span>
                            <button onclick="window.location.href='/zenjuriesLogin'">Login Page</button>
                            <span class="url">/zenjuriesLogin</span>
                        </div>    
                    </div>
                </div>    
                <div class="itemGrid__item thirds">
                <div class="devLinkContainer">
                        <div class="devLink">
                            <span class="title">Zenjuries 3.2 dev</span>
                            <button onclick="window.location.href='https://dev.zenjuries.com/dev')" class="zenblue">3.2 dev login</button>
                            <span class="url">https://dev.zenjuries.com/dev</span>
                        </div>    
                    </div>
                </div>
                <div class="itemGrid__item thirds">
                <div class="devLinkContainer">
                        <div class="devLink">
                            <span class="title">All emails</span>
                            <button onclick="window.location.href='/devEmail'">Email List</button>
                            <span class="url">/devEmail</span>
                        </div>                                  
                    </div>
                </div>
            </div>

            <div class="itemGrid">
                <div class="itemGrid__item thirds">
                <div class="devLinkContainer">
                        <div class="devLink">
                            <span class="title">Zengarden</span>
                            <button onclick="window.location.href='/autoLogin/injured')" class="olive">Login as Injured Employee</button>
                            <span class="url">/autoLogin/injured</span>
                        </div>             
                    </div>
                </div>    
                <div class="itemGrid__item thirds">
                <div class="devLinkContainer">
                        <div class="devLink">
                            <span class="title">initiate testing</span>
                            <button class="green" onclick="window.location.href='/testing'">submit test</button>
                            <span class="url">[submit your code to testers]</span>
                        </div>
                    </div>
                </div>
                <div class="itemGrid__item thirds">
                <div class="devLinkContainer">
                        <div class="devLink">
                            <span class="title">gylphs / samples</span>
                            <button class="orange small" onclick="window.location.href='/fontlist'">glyph list</button><br>
                            <button class="gold small" onclick="window.location.href='/samples'">samples page</button>
                        </div>                               
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@extends('layouts.zen5_layout')
@section('maincontent')
<?php

    $user = Auth::user();
    $arrayPassedIn = str_split($user->shortcuts);
?>

<script>
     $(document).ready(function() {
        var favArray = <?php echo json_encode($arrayPassedIn); ?>;
        
        //dashboard
        for(var i = 0; i < favArray.length; i++){
            if(favArray[i] === "1"){
                var favBoxId = "#fav" + (i + 1);
                $(favBoxId).show();
            }
        }

        //dashboard
        function updateStringPassedIn(){
            var stringPassedIn = favArray.toString().replaceAll(",", "");
            $.ajax({
            type: "POST",
            url: "{{ route('updateUserShortcuts') }}",
            data: {
                _token: '<?php echo csrf_token(); ?>',
                user_id: {{$user->id}},
                shortcuts: stringPassedIn
            },
            success: function(){
                alert(stringPassedIn);
            }
            });
        }

        //dashboard
        $('.favRemove').on("click", function() {
            $(this.parentElement.parentElement).hide();
            var boxId = this.parentElement.parentElement.id.replaceAll("fav", "");
            favArray[boxId - 1] = "0";
            updateStringPassedIn();
        }) 
             
        //heart code
        var favClasses = document.getElementsByClassName('setFavorite');
        for(var i = 0; i < favClasses.length; i++){
            var heartNum = favClasses[i].id.replaceAll("heart", "");
            if(favArray[heartNum - 1] === "1"){
                $(favClasses[i]).toggleClass("set");
            }
        }

        //heart code
        $('.setFavorite').on("click", function() {
            var heartId = this.id.replaceAll("heart", "");
            if(favArray[heartId - 1] === "0"){
                $("#fav" + heartId).show();
                $(this).toggleClass("set");
                favArray[heartId - 1] = "1";
                updateStringPassedIn();
            }
            else{
                $("#fav" + heartId).hide();
                $(this).toggleClass("set");
                favArray[heartId - 1] = "0";
                updateStringPassedIn();
            }
        }) 
    })
</script>

<div class="pageContent bgimage-bgheader pagebackground8 fade-in">
    <!--***********-->
    <div class="headerBlock">
        <div class="headerContainer">
            <div class="pageHeader wow animate__animated animate__bounceInLeft">
                <div class="pageHeader__title">
                    <span>favorites test page</span>
                </div>
                <div class="pageHeader__subTitle">
                    <!-- using 'set' with the 'setFavorite' class will make it active.  toggle to make inactive-->
                    <span class="icon"><img src="images/icons/heart.png"></span><span class="subContent">set favorite 8.<button class="setFavorite" id="heart8"></button></span>
                    <span class="icon"><img src="images/icons/heart.png"></span><span class="subContent">set favorite 4.<button class="setFavorite" id="heart4"></button></span>
                </div>
            </div>
            <div class="pageIcon wow animate__animated animate__bounceInRight">
                <img src="/images/icons/checkheart.png">
            </div>
        </div>
    </div>

    <div class="contentBlock noPad">
        <div class="container noPad">
            <div class="testPanel">
                <section class="sectionPanel" style="min-height:400px;">
                    <span class="sectionTitle"><div class="icon icon-bar-heart"></div> FAVORITES</span>
                    <!-- add/remove class 'showAll' to override favBox display:none (default style)-->
                    <div class="homeFavorites">
                        <section>
                            <div class="favBox" id="fav1"><a class="noLink favItem"><span class="favTitle">Favorite 1</span><span class="favRemove">X</span>
                            <span class="favContent">
                                <hr>
                            Lorem ipsum dolor sit amet, consectetur elit.
                            </span></a></div>

                            <div class="favBox" id="fav2"><a class="noLink favItem"><span class="favTitle">Favorite 2</span><span class="favRemove">X</span>
                            <span class="favContent">
                            <hr>
                            Sed tincidunt odio sit amet tempus pellentesque.
                            </span></a></div>
                            
                            <div class="favBox" id="fav4"><a class="noLink favItem"><span class="favTitle">Favorite 4</span><span class="favRemove">X</span>
                            <span class="favContent">
                            <hr>
                            Nunc blandit commodo augue, at malesuada leo.
                            </span></a></div>
                            
                            <div class="favBox" id="fav8"><a class="noLink favItem"><span class="favTitle">Favorite 8</span></span><span class="favRemove">X</span>
                            <span class="favContent">
                            <hr>
                            Sed scelerisque rutrum malesuada. Praesent mollis
                            </span></a></div>
                        </section>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>


@endsection
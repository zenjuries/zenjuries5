<script>
    injuryDate = new MtrDatepicker({
        target: "injuryDate",
        animation: true,
        smartHours: true,
        timestamp: new Date(),

    });



    //$('.inputError').removeClass('show');

    $('.typeButton').on('click', function(){
        $(this).toggleClass('selected');
    });

    $('.locationButton').on('click', function(){
       var code = $(this).data('naiscscode');
       console.log(code);
       //this should toggle the class on the duplicate buttons (on the view all modal + other modals)
       $('.locationButton[data-naiscscode="' + code +'"]').toggleClass('selected');
    });


    var claimType = '';
    var sev = false;
    var gen = false;

    //set review panel labels to red until populated
    $('#claimLabel').css('color', 'red');
    $('#sevLabel').css('color', 'red');
    $('#nameLabel').css('color', 'red');
    $('#genLabel').css('color', 'red');
    $('#dateLabel').css('color', 'red');
    $('#emailLabel').css('color', 'red');
    $('#mobileLabel').css('color', 'red');
    $('#locLabel').css('color', 'red');
    $('#typeLabel').css('color', 'red');
    $('#teamLabel').css('color', 'red');
    $('#imgLabel').css('color', 'red');

    var injury = {
        claim_setting: null,
        policyNumber: null,
        classification: null,
        severity: null,
        name: null,
        gender: null,
        email: null,
        phone: null,
        date: null,
        squadID: 121,
        squadName: null,
        notes: null,
        img: null,
        locations: [],
        location_codes: [],
        types: [],
        photos: [],
        injuryLocationsImage: null,
    }

    $('.newInjuryPanel').on('click', function(){
        storeBasicInfo();
        setReviewPanel();
    });
    //Post the injury data
    $('#submitButton').on('click', function(){
        console.log("submit clicked");
        postInjury(injury);
    });

    function storeBasicInfo(){
        //check all fields to see if they are populated - display or hide errors
        //      runValidation();

        if ($("#newClaim").prop("checked")){
            injury.claim_setting = "new";
        } else if ($("#preClaim").prop("checked")){
           injury.claim_setting = "preexisting";
        } else if ($("#demoClaim").prop("checked")){
            injury.claim_setting = 'demo';
        }

        injury.name = $('#firstName').val() + " " + $('#lastName').val();

        if ($("#male").prop("checked")){
            injury.gender = "male";
            gen = true;
        } else if ($("#female").prop("checked")){
            injury.gender = "female";
            gen = true;
        }
        if ($("#firstAid").prop("checked")){
            injury.severity = "Mild";
            sev = true;
        }else if ($("#modInjury").prop("checked")){
            injury.severity = "Moderate";
            sev = true;
        }else if ($("#sevInjury").prop("checked")){
            injury.severity = "Severe";
            sev = true;
        }

        //injury.email = $('#email').val().replace(/ /g, '');
        injury.policyNumber = "1000001";
        injury.email = $('#email').val();
        injury.phone = $('#phone').val();
        if(injury.severity === "Mild"){
            injury.classification = "First Aid";
        }else{
            injury.classification = "Work Comp";
        }
        injury.squadID = "15";
        //injury.date = $('#datetime12').combodate('getValue', null);
        injury.date = injuryDate.toLocaleString();
        injury.notes = $('#textarea').val();
        //injury.location_codes THIS WILL BE SET WHEN LOCATIONS ARE SET
        //injury.locations =
        setLocationsArray();
        setTypesArray();
        injury.squadName = "team1"

        runValidation();
        console.log(injury);
    }

    function setTypesArray(){
        //create a new array for the types
        var types = []
        //get all elements that have both classes
        var selected_types = $('.typeButton.selected');
        //loop through each element
        selected_types.each(function(){
            //push the type stored in the element's html to the array
            types.push($(this).html());
        });
        injury.types = types;
        writeTypes();
    }

    function setLocationsArray(){
        //create a new array for the types
        var locations = [];
        var location_codes = [];
        //get all elements that have both classes
        var selected_locations = $('.locationButton.selected');
        //loop through each element
        selected_locations.each(function(){
            //push the type stored in the element's html to the array
            location_codes.push($(this).data('naiscscode'));
            locations.push($(this).html());
        });
        //there will be duplicants that need to be removed
        injury.locations = Array.from(new Set(locations));
        injury.location_codes = Array.from(new Set(location_codes));
        writeLocations();
    }

    function writeLocations(){
        var locations = injury.locations;
        var html = "";
        for(var i = 0; i < locations.length; i ++){
            html += locations[i] + ", ";
        }
        html = html.slice(0, -2);
        console.log(html);
        $('.locationSpan').html(html);
    }

    function writeTypes(){
        var types = injury.types;
        console.log(types);
        var html = "";
        for(var i = 0; i < types.length; i ++){
            html += types[i] + ", ";
        }
        html = html.slice(0, -2);

        $('#set_injurytypearray').html(html);
    }

    function setReviewPanel(){
        $('#set_claimtype').html(claimType);
        $('#set_injuryseverity').html(injury.severity);
        $('#set_employeename').html(injury.name);
        $('#set_employeegender').html(injury.gender);
        $('#set_injurydatetime').html(injury.date);
        $('#set_employeemail').html(injury.email);
        $('#set_employeemobile').html(injury.phone);
        $('#set_injurylocationarray').html(injury.location);
        $('#set_injurytypearray').html(injury.type);
        $('#set_injuryteam').html(injury.squadName);

    }

    function runValidation(){
        //alert("running validation");
        var validData = true;
        var claimDiv = true;
        var sevDiv = true;
        var genDiv = true;
        var fnameDiv = true;
        var lnameDiv = true;
        var dateDiv = true;
        var sumDiv = true;
        var mailDiv = true;
        var phoneDiv = true;
        var typeDiv = true;
        var locationDiv = true;
        //$('.inputError').removeClass('show');
        // if (claimType === "" || undefined){
        //     console.log("claimType is undefined");
        //     var errorDiv = $('#newClaim').parent().find('.inputError');
        //     if (errorDiv != ""){
        //         errorDiv.html("Claim Type Missing");
        //         claimDiv = false;
        //         //validData = false;
        //     }
        // }
        if (claimType === "" || claimType === undefined){
            // console.log("claimType is undefined");
            $('#claimStatus').parent().find('.inputError').addClass('show');
            claimDiv = false;
        }else{
            $('#claimStatus').parent().find('.inputError').removeClass('show');
            $('#claimLabel').css('color', '');
        }

        if (sev === false){
            // console.log("Severity is undefined");
            $('#sevStatus').parent().find('.inputError').addClass('show');
            sevDiv = false;
        }else{
            $('#sevStatus').parent().find('.inputError').removeClass('show');
            $('#sevLabel').css('color', '');
        }

        if (gen === false){
            //console.log("Gender is undefined");
            $('infoStatus').parent().find('.inputError').addClass('show');
            genDiv = false;
        }else{
            $('#infoStatus').parent().find('.inputError').removeClass('show');
            $('#genLabel').css('color', '');
        }

        var text = $('#textarea').val();
        if (text === "" || text === undefined){
            // console.log("Summary is undefined");
            $('#textarea').parent().find('.inputError').addClass('show');
            sumDiv = false;
        }else{
            $('#textarea').parent().find('.inputError').removeClass('show');
        }

        var selDate = $('#mobi-picker').val();
        if (selDate === "" || selDate === undefined){
            // console.log("Summary is undefined");
            $('#mobi-picker').parent().find('.inputError').addClass('show');
            sumDiv = false;
        }else{
            $('#mobi-picker').parent().find('.inputError').removeClass('show');
            $('#dateLabel').css('color', '');
        }

        // Commenting until issues with new datepicker resolved
        // var date = $('#datetime').val();
        // if (date === "" || undefined){
        //     console.log("Date is undefined");
        //     $('#datetime').parent().find('.inputError').addClass('show');
        //     dateDiv = false;
        // }

        var empEmail = $('#email').val();
        if (empEmail === "" || undefined){
            //NOT REQUIRED
            /*
            $('#email').parent().find('.inputError').addClass('show');
            mailDiv = false;
            */
        }else{
           // $('#email').parent().find('.inputError').removeClass('show');
            $('#emailLabel').css('color', '');
        }

        var empPhone = $('#phone').val();
        if (empPhone === "" || undefined){
            //NOT REQUIRED
            /*
            $('#phone').parent().find('.inputError').addClass('show');
            phoneDiv = false;
            */
        }else{
            //$('#phone').parent().find('.inputError').removeClass('show');
            $('#mobileLabel').css('color', '');
        }

        // if (sev === false){
        //     console.log("Severity is undefined");
        //     var errorDiv = $('#firstAid').parent().find('.inputError');
        //     if (errorDiv != ""){
        //         errorDiv.html("Severity is Missing");
        //         sevDiv = false;
        //         //validData = false;
        //     }
        // }

        var fName = $('#firstName').val();
        //console.log("fName" + fName);
        if (fName === "" || undefined){
            //console.log("fName is undefined");
            var errorDiv = $('#firstName').parent().find('.inputError');
            if (errorDiv != ""){
                errorDiv.html("First Name Missing");
                fnameDiv = false;
                //validData = false;
            }
        }else{
            $('#firstName').parent().find('.inputError').removeClass('show');
            $('#nameLabel').css('color', '');
            // var errorDiv = $('#firstName').parent().find('.inputError');
            // errorDiv.html("");

        }
        var lName = $('#lastName').val();
        //console.log("lName" + lName);
        if (lName === "" || undefined){
            //console.log("lName is undefined");
            var errorDiv = $('#lastName').parent().find('.inputError');
            if (errorDiv != ""){
                errorDiv.html("Last Name Missing");
                lnameDiv = false;
                //validData = false;
            }
        }else{
            $('#lastName').parent().find('.inputError').removeClass('show');
            $('#nameLabel').css('color', '');
        }

        //types
        if(injury.types === undefined || injury.types.length == 0){
            typeDiv = false;
            $('#typeLabel').css('color', 'red');
        }else{
            $('#typeLabel').css('color', '');
        }

        if(injury.locations === undefined || injury.locations.length == 0){
            locationDiv = false;
            $('#locLabel').css('color', 'red');
        }else{
            $('#locLabel').css('color', '');
        }

        //console.log("valid data = " + validData);
        if (lnameDiv === false || fnameDiv === false || sevDiv === false || claimDiv === false || genDiv === false || sumDiv === false || dateDiv === false || typeDiv === false || locationDiv === false){
            validData = false;
        }

        if (validData === true) {
            $('.inputError').removeClass('show');
            //console.log(validData);
            //console.log("show button");
            $('#submitButton').show();
        }else{
            //console.log("hide button");
            $('#submitButton').hide();
        }
    }

    //photo upload
    $('#submitUpload').on('click', function(){
        uploadPhoto();
    });

    var photos = [];

    function uploadPhoto(){
        var upload_errors = $('#photoUpload').parent().find('.inputError')
        var file = document.getElementById('photoUpload').files[0];
        if(file === undefined || file === ""){
            upload_errors.html("Photo Required").addClass('show');
            return false;
        }

        if(file.type == "image/png" || file.type == "image/jpeg"){
            var name = $('#photoNameInput').val();
            var description = $('#photoUploadDescription').val();
            var reader = new FileReader();
            reader.onload = function(){
                var photo = {
                    file: file,
                    name: name,
                    description: description,
                    data: reader.result,
                }
                photos.push(photo);
                console.log(photo);
                buildPhotos();
                modal.close();
            }
            reader.readAsDataURL(file);


        }else{
            showAlert("Sorry, please make sure your image is a png or a jpeg.", "deny", 5);
            upload_errors.html("Invalid File Type").addClass('show');
        }
    }

    function buildPhotos(){
        var html = "";
        var canvas = document.getElementById('photoCanvas');
        var ctx = canvas.getContext("2d");

        for(var i = 0; i < photos.length; i++){

            /*
            var image = new Image;
            image.onload = function(){
                ctx.drawImage(image, 0, 0);
            }
            image.src = photos[i].file;
            var base64 = canvas.toDataURL();
*/
            html+= '<div class="formMedia" data-index="' + i + '">' +
                '<div class="uploadedMedia" style="background-image:url(' + photos[i].data + ');"><div class="mediaSelectOverlay"></div>' +
                '<div class="mediaCaptionSummary">' + photos[i].description +'</div><span class="mediaInfo">' + photos[i].name + '</span><span class="mediaDelete" data-index="' + i + '"></span><span class="mediaCaption showFileModal" data-modalcontent="#mediaDescriptionModal" data-index="' + i + '"></span></div>' +
                '</div>';
        }
        $('#photoTarget').html(html);
        injury.photos = photos;
    }

    $('#photoTarget').on('click', '.mediaDelete', function(){
        var index = $(this).data('index');
        photos.splice(index, 1);
        buildPhotos();
    });

    $('#setDescription').on('click', function(){
       var index = $('#descriptionIndex').val();
       var description = $('#photo_description').val();

       photos[index].description = description;
       buildPhotos();
        modal.close();
    });

    function postInjury(injury){

        $.ajax({
            type: 'POST',
            url: '<?php echo route('saveInjury'); ?>',
            data: {
                _token: '<?php echo csrf_token(); ?>',
                injury: injury
            },
            success: function(data){
                alert('success!');
            },
            error: function(data){
                console.log(data);
                alert('error');
            }
        });

    }
</script>
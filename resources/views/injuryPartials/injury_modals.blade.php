

<!-- BASE MODAL HTML -->
<div id="locationModal" class="zModal" style="display:none">
    <!-- this is filled with each location modal data -->
    <!-- SWAPPABLE CONTENT, with IDs that match the modalcontent attribute for its respective button -->
    <!-- using the class "selected" will make the button red.  use that to signify something user picked.-->
    <div class="modalContent modalBlock" id="showAll" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>systematic</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-test="here's some data" data-naiscscode="800 body system">body system</button>
                        <button class="locationButton" data-naiscscode="999 nonclassifiable">nonclassifiable</button>
                        <button class="locationButton" data-naiscscode="700 multiple parts">multiple parts</button>
                        <button class="locationButton" data-naiscscode="900 body parts">body parts</button>
                        <button class="locationButton" data-naiscscode="000 death">death</button>
                    </div>
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>head</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="100 head">head</button>
                        <button class="locationButton" data-naiscscode="110 brain">brain</button>
                        <button class="locationButton" data-naiscscode="120 ear(s)">ear(s)</button>
                        <button class="locationButton" data-naiscscode="121 ear(s) external">ear(s) external</button>
                        <button class="locationButton" data-naiscscode="122 lef ear external">left ear external</button>
                        <button class="locationButton" data-naiscscode="123 right ear external">right ear external</button>
                        <button class="locationButton" data-naiscscode="124 ear(s) internal">ear(s) internal</button>
                        <button class="locationButton" data-naiscscode="125 left ear internal">left ear internal</button>
                        <button class="locationButton" data-naiscscode="126 right ear internal">right ear internal</button>
                        <button class="locationButton" data-naiscscode="130 eye(s)">eye(s)</button>
                        <button class="locationButton" data-naiscscode="132 left eye">left eye</button>
                        <button class="locationButton" data-naiscscode="134 right eye">right eye</button>
                        <button class="locationButton" data-naiscscode="140 face">face</button>
                        <button class="locationButton" data-naiscscode="141 jaw">jaw</button>
                        <button class="locationButton" data-naiscscode="144 mouth">mouth</button>
                        <button class="locationButton" data-naiscscode="146 nose">nose</button>
                        <button class="locationButton" data-naiscscode="148 face, multi-part">face, multi-part</button>
                        <button class="locationButton" data-naiscscode="149 face, NEC">face, NEC</button>
                        <button class="locationButton" data-naiscscode="150 scalp">scalp</button>
                        <button class="locationButton" data-naiscscode="160 skull">skull</button>
                        <button class="locationButton" data-naiscscode="198 head, multi-part">head, multi-part</button>
                        <button class="locationButton" data-naiscscode="199 head, NEC">head, NEC</button>
                    </div>
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>neck</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="200 neck">neck</button>
                    </div>
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>right arm</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="304 right arm">right arm</button>
                        <button class="locationButton" data-naiscscode="300 upper right arm">upper right arm</button>
                        <button class="locationButton" data-naiscscode="314 right elbow">right elbow</button>
                        <button class="locationButton" data-naiscscode="317 right forearm">right forearm</button>
                        <button class="locationButton" data-naiscscode="324 right wrist">right wrist</button>
                        <button class="locationButton" data-naiscscode="334 right hand">right hand</button>
                        <button class="locationButton" data-naiscscode="344 right fingers, thumb">right fingers, thumb</button>
                        <button class="locationButton" data-naiscscode="397 right lower extremities">right lower extremities</button>
                    </div>
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>left arm</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="302 left arm">left arm</button>
                        <button class="locationButton" data-naiscscode="308 upper left arm">upper left arm</button>
                        <button class="locationButton" data-naiscscode="312 left elbow">left elbow</button>
                        <button class="locationButton" data-naiscscode="316 left forearm">left forearm</button>
                        <button class="locationButton" data-naiscscode="322 left wrist">left wrist</button>
                        <button class="locationButton" data-naiscscode="332 left hand">left hand</button>
                        <button class="locationButton" data-naiscscode="342 left fingers, thumb">left fingers, thumb</button>
                        <button class="locationButton" data-naiscscode="396 left lower extremities">left lower extremities</button>
                    </div>
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>trunk</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="400 trunk">trunk</button>
                        <button class="locationButton" data-naiscscode="410 abdomen">abdomen</button>
                        <button class="locationButton" data-naiscscode="420 back">back</button>
                        <button class="locationButton" data-naiscscode="430 chest">chest</button>
                        <button class="locationButton" data-naiscscode="440 hips, both">hips, both</button>
                        <button class="locationButton" data-naiscscode="442 left hip">left hip</button>
                        <button class="locationButton" data-naiscscode="444 right hip">right hip</button>
                        <button class="locationButton" data-naiscscode="450 shoulders, both">shoulders, both</button>
                        <button class="locationButton" data-naiscscode="452 left shoulder">left shoulder</button>
                        <button class="locationButton" data-naiscscode="454 right shoulder">right shoulder</button>
                        <button class="locationButton" data-naiscscode="498 trunk, multi-part">trunk, multi-part</button>
                        <button class="locationButton" data-naiscscode="499 trunk, NEC">trunk, NEC</button>
                    </div>
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>both arms</b> (upper extremities)</span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="310 arms">arms</button>
                        <button class="locationButton" data-naiscscode="311 upper arms">upper arms</button>
                        <button class="locationButton" data-naiscscode="313 elbows">elbows</button>
                        <button class="locationButton" data-naiscscode="315 forearms">forearms</button>
                        <button class="locationButton" data-naiscscode="318 arms, multi-part">arms, multi-part</button>
                        <button class="locationButton" data-naiscscode="319 arms, NEC">arms, NEC</button>
                        <button class="locationButton" data-naiscscode="320 wrists">wrists</button>
                        <button class="locationButton" data-naiscscode="330 hands">hands</button>
                        <button class="locationButton" data-naiscscode="340 fingers, thumbs">fingers, thumbs</button>
                        <button class="locationButton" data-naiscscode="398 upper extremities">upper extremities</button>
                        <button class="locationButton" data-naiscscode="399 upper extremities, NEC">upper extremities, NEC</button>
                    </div>
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>right leg</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="508 right leg">right leg</button>
                        <button class="locationButton" data-naiscscode="514 right knee">right knee</button>
                        <button class="locationButton" data-naiscscode="517 right shin">right shin</button>
                        <button class="locationButton" data-naiscscode="524 right ankle">right ankle</button>
                        <button class="locationButton" data-naiscscode="534 right foot">right foot</button>
                        <button class="locationButton" data-naiscscode="542 right toes">right toes</button>
                        <button class="locationButton" data-naiscscode="597 lower extremities, right">lower extremities, right</button>
                    </div>
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>left leg</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="507 left leg">left leg</button>
                        <button class="locationButton" data-naiscscode="512 left knee">left knee</button>
                        <button class="locationButton" data-naiscscode="516 left shin">left shin</button>
                        <button class="locationButton" data-naiscscode="522 left ankle">left ankle</button>
                        <button class="locationButton" data-naiscscode="532 left foot">left foot</button>
                        <button class="locationButton" data-naiscscode="542 left toes">left toes</button>
                        <button class="locationButton" data-naiscscode="596 lower extremities, right">lower extremities, right</button>
                    </div>
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>both legs</b> (lower extremities)</span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="510 both legs">both legs</button>
                        <button class="locationButton" data-naiscscode="513 both knees">both knees</button>
                        <button class="locationButton" data-naiscscode="515 lower legs">lower legs</button>
                        <button class="locationButton" data-naiscscode="518 legs, multi-part">legs, multi-part</button>
                        <button class="locationButton" data-naiscscode="520 both ankles">both ankles</button>
                        <button class="locationButton" data-naiscscode="530 both feet">both feet</button>
                        <button class="locationButton" data-naiscscode="540 all toes">all toes</button>
                        <button class="locationButton" data-naiscscode="598 lower extremities, multi">lower extremities, multi</button>
                        <button class="locationButton" data-naiscscode="598 lower extremities, NEC">lower extremities, NEC</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;margin-bottom:60px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                            <button class="red"><div class="icon inline icon-refresh"></div> reset</button>
                        </div>
                    </div>
                    <br><br>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showSystematic" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>systematic</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <div class="buttonCloud">
                            <button class="locationButton" data-naiscscode="800 body system">body system</button>
                            <button class="locationButton" data-naiscscode="999 nonclassifiable">nonclassifiable</button>
                            <button class="locationButton" data-naiscscode="700 multiple parts">multiple parts</button>
                            <button class="locationButton" data-naiscscode="900 body parts">body parts</button>
                            <button class="locationButton" data-naiscscode="000 death">death</button>
                        </div>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                            <button class="red"><div class="icon inline icon-refresh"></div> reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showHead" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>head</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="100 head">head</button>
                        <button class="locationButton" data-naiscscode="110 brain">brain</button>
                        <button class="locationButton" data-naiscscode="120 ear(s)">ear(s)</button>
                        <button class="locationButton" data-naiscscode="121 ear(s) external">ear(s) external</button>
                        <button class="locationButton" data-naiscscode="122 left ear external">left ear external</button>
                        <button class="locationButton" data-naiscscode="123 right ear external">right ear external</button>
                        <button class="locationButton" data-naiscscode="124 ear(s) internal">ear(s) internal</button>
                        <button class="locationButton" data-naiscscode="125 left ear internal">left ear internal</button>
                        <button class="locationButton" data-naiscscode="126 right ear internal">right ear internal</button>
                        <button class="locationButton" data-naiscscode="130 eye(s)">eye(s)</button>
                        <button class="locationButton" data-naiscscode="132 left eye">left eye</button>
                        <button class="locationButton" data-naiscscode="134 right eye">right eye</button>
                        <button class="locationButton" data-naiscscode="140 face">face</button>
                        <button class="locationButton" data-naiscscode="141 jaw">jaw</button>
                        <button class="locationButton" data-naiscscode="144 mouth">mouth</button>
                        <button class="locationButton" data-naiscscode="146 nose">nose</button>
                        <button class="locationButton" data-naiscscode="148 face, multi-part">face, multi-part</button>
                        <button class="locationButton" data-naiscscode="149 face, NEC">face, NEC</button>
                        <button class="locationButton" data-naiscscode="150 scalp">scalp</button>
                        <button class="locationButton" data-naiscscode="160 skull">skull</button>
                        <button class="locationButton" data-naiscscode="198 head, multi-part">head, multi-part</button>
                        <button class="locationButton" data-naiscscode="199 head, NEC">head, NEC</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                            <button class="red"><div class="icon inline icon-refresh"></div> reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showHead" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>neck</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button>200 neck</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                            <button class="red"><div class="icon inline icon-refresh"></div> reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showRightArm" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>right arm</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="304 right arm">right arm</button>
                        <button class="locationButton" data-naiscscode="309 upper right arm">upper right arm</button>
                        <button class="locationButton" data-naiscscode="314 right elbow">right elbow</button>
                        <button class="locationButton" data-naiscscode="317 right forearm">right forearm</button>
                        <button class="locationButton" data-naiscscode="324 right wrist">right wrist</button>
                        <button class="locationButton" data-naiscscode="334 right hand">right hand</button>
                        <button class="locationButton" data-naiscscode="344 right fingers, thumb">right fingers, thumb</button>
                        <button class="locationButton" data-naiscscode="397 right lower extremities">right lower extremities</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                            <button class="red"><div class="icon inline icon-refresh"></div> reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showLeftArm" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>left arm</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="302 left arm">left arm</button>
                        <button class="locationButton" data-naiscscode="308 upper left arm">upper left arm</button>
                        <button class="locationButton" data-naiscscode="312 left elbow">left elbow</button>
                        <button class="locationButton" data-naiscscode="316 left forearm">left forearm</button>
                        <button class="locationButton" data-naiscscode="322 left wrist">left wrist</button>
                        <button class="locationButton" data-naiscscode="332 left hand">left hand</button>
                        <button class="locationButton" data-naiscscode="342 left fingers, thumb">left fingers, thumb</button>
                        <button class="locationButton" data-naiscscode="396 left lower extremities">left lower extremities</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                            <button class="red"><div class="icon inline icon-refresh"></div> reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showTrunk" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>trunk</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="400 trunk">trunk</button>
                        <button class="locationButton" data-naiscscode="410 abdomen">abdomen</button>
                        <button class="locationButton" data-naiscscode="420 back">back</button>
                        <button class="locationButton" data-naiscscode="430 chest">chest</button>
                        <button class="locationButton" data-naiscscode="440 hips, both">hips, both</button>
                        <button class="locationButton" data-naiscscode="442 left hip">left hip</button>
                        <button class="locationButton" data-naiscscode="444 right hip">right hip</button>
                        <button class="locationButton" data-naiscscode="450 shoulders, both">shoulders, both</button>
                        <button class="locationButton" data-naiscscode="452 left shoulder">left shoulder</button>
                        <button class="locationButton" data-naiscscode="454 right shoulder">right shoulder</button>
                        <button class="locationButton" data-naiscscode="498 trunk, multi-part">trunk, multi-part</button>
                        <button class="locationButton" data-naiscscode="499 trunk, NEC">trunk, NEC</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                            <button class="red"><div class="icon inline icon-refresh"></div> reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showBothArms" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>both arms</b> (upper extremities)</span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="310 arms">arms</button>
                        <button class="locationButton" data-naiscscode="311 upper arms">upper arms</button>
                        <button class="locationButton" data-naiscscode="313 elbows">elbows</button>
                        <button class="locationButton" data-naiscscode="315 forearms">forearms</button>
                        <button class="locationButton" data-naiscscode="318 arms, multi-part">arms, multi-part</button>
                        <button class="locationButton" data-naiscscode="319 arms, NEC">arms, NEC</button>
                        <button class="locationButton" data-naiscscode="320 wrists">wrists</button>
                        <button class="locationButton" data-naiscscode="330 hands">hands</button>
                        <button class="locationButton" data-naiscscode="340 fingers, thumbs">fingers, thumbs</button>
                        <button class="locationButton" data-naiscscode="398 upper extremities">upper extremities</button>
                        <button class="locationButton" data-naiscscode="399 upper extremities, NEC">upper extremities, NEC</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                            <button class="red"><div class="icon inline icon-refresh"></div> reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showRightLeg" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>right leg</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="599 lower extremities (all)">lower extremities (all)</button>
                        <button class="locationButton" data-naiscscode="510 legs (both)">legs (both)</button>
                        <button class="locationButton" data-naiscscode="513 knees (both)">knees (both)</button>
                        <button class="locationButton" data-naiscscode="520 ankles (both)">ankles (both)</button>
                        <button class="locationButton" data-naiscscode="530 feet (both)">feet (both)</button>
                        <button class="locationButton" data-naiscscode="540 toes (both feet)">toes (both feet)</button>
                        <button class="locationButton" data-naiscscode="596 left lower extremities">left lower extremities</button>
                        <button class="locationButton" data-naiscscode="508 left leg">left leg</button>
                        <button class="locationButton" data-naiscscode="514 left knee">left knee</button>
                        <button class="locationButton" data-naiscscode="522 left ankle">left ankle</button>
                        <button class="locationButton" data-naiscscode="532 left foot">left foot</button>
                        <button class="locationButton" data-naiscscode="542 left foot toes">left foot toes</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                            <button class="red"><div class="icon inline icon-refresh"></div> reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showLeftLeg" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>left leg</b></span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="599 lower extremities (all)">lower extremities (all)</button>
                        <button class="locationButton" data-naiscscode="510 legs (both)">legs (both)</button>
                        <button class="locationButton" data-naiscscode="513 knees (both)">knees (both)</button>
                        <button class="locationButton" data-naiscscode="520 ankles (both)">ankles (both)</button>
                        <button class="locationButton" data-naiscscode="530 feet (both)">feet (both)</button>
                        <button class="locationButton" data-naiscscode="540 toes (both feet)">toes (both feet)</button>
                        <button class="locationButton" data-naiscscode="596 left lower extremities">left lower extremities</button>
                        <button class="locationButton" data-naiscscode="508 left leg">left leg</button>
                        <button class="locationButton" data-naiscscode="514 left knee">left knee</button>
                        <button class="locationButton" data-naiscscode="522 left ankle">left ankle</button>
                        <button class="locationButton" data-naiscscode="532 left foot">left foot</button>
                        <button class="locationButton" data-naiscscode="542 left foot toes">left foot toes</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                            <button class="red"><div class="icon inline icon-refresh"></div> reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalContent modalBlock" id="showBothLegs" style="display:none">
        <div class="modalBody" style="min-width:260px;">
            <div class="modalContent">
                <div class="selectionContainer">
                    <span class="sectionTitle"><div class="icon icon-stethoscope"></div> select location - <b>both legs</b> (lower extremities)</span>
                    <hr class="opaque">
                    <div class="buttonCloud">
                        <button class="locationButton" data-naiscscode="510 both legs">both legs</button>
                        <button class="locationButton" data-naiscscode="513 both knees">both knees</button>
                        <button class="locationButton" data-naiscscode="515 lower legs">lower legs</button>
                        <button class="locationButton" data-naiscscode="518 legs, multi-part">legs, multi-part</button>
                        <button class="locationButton" data-naiscscode="520 both ankles">both ankles</button>
                        <button class="locationButton" data-naiscscode="530 both feet">both feet</button>
                        <button class="locationButton" data-naiscscode="540 all toes">all toes</button>
                        <button class="locationButton" data-naiscscode="598 lower extremities, multi">lower extremities, multi</button>
                        <button class="locationButton" data-naiscscode="598 lower extremities, NEC">lower extremities, NEC</button>
                    </div>
                    <hr class="opaque" style="margin-top:30px;">
                    <div class="center">
                        <div class="buttonArray">
                            <button class="green"><div class="icon inline icon-check"></div> set</button>
                            <button class="red"><div class="icon inline icon-refresh"></div> reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div id="fileModal" class="zModal" style="display:none">
    <!-- upload Image modal -->
    <div class="modalContent modalBlock wide" id="uploadImageModal" style="display: none">
        <div class="modalHeader"><span class="modalTitle"><div class="icon inline icon-picture"></div> upload image</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="formBlock dark">
                <div class="formInput">
                    <label for="photoUpload">Choose Photo:</label>
                    <input id="photoUpload" type="file">
                    <span class="inputError"></span>
                </div>
                <div class="formInput">
                    <label for="photoNameInput">Name:</label>
                    <input id="photoNameInput" name="photoNameInput">
                    <span class="inputError"></span>
                </div>
                <div class="formInput">
                    <label for="photoUploadDescription">Description:</label>
                    <textarea id="photoUploadDescription"></textarea>
                </div>
                <button id="submitUpload">Upload</button>
            </section>
        </div>
    </div>

    <div class="modalContent modalBlock dark" id="mediaDescriptionModal" style="display: none">
        <div class="modalHeader"><span class="modalTitle"><div class="icon inline icon-picture"></div> edit description</span></div>
        <div class="modalBody" style="min-width:260px;">
            <section class="sectionPanel dark">
                <div class="sectionContent">
                    <section class="formBlock dark">
                        <div class="formGrid">
                            <div class="formInput">
                                <!-- input -->
                                <input type="hidden" id="descriptionIndex">
                                <label for="photo_description">description</label>
                                <textarea id="photo_description"></textarea>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="center"><button id="setDescription" class="cyan">set description</button></div>
            </section>
        </div>
    </div>
</div>
<script>
    var modal;

    $('body').on('click', '.showFileModal', function(){
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#fileModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');

        if(target === "#mediaDescriptionModal"){
            var index =$(this).data('index');
            $('#descriptionIndex').val(index);
        }

        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
        modal.open();
    });

    //THIS FUNCTION SHOULD BE easy to drop on a page, the class triggering the onclick and the modal name should be the main changes.
    $('.showLocationModal').on('click', function(){
        modal = new jBox('Modal', {
            addClass: 'zBox',
            closeButton: true,
            //modal ID goes here
            content: $('#locationModal'),
            isolateScroll: true
        });
        //get the content for the modal
        var target = $(this).data('modalcontent');
        //hide all content blocks
        $('.modalBlock').hide();
        //show the target
        $(target).show();
        modal.open();
    });

    $('#locationModal').on('click', 'button[class="red"]', function(){
        $('.locationButton').removeClass('selected');
        //setLocationsArray();
    });


    $('#locationModal').on('click', 'button[class="green"]', function(){
        modal.close();
    });


</script>
<?php

namespace Laravel\Spark\Interactions\Settings\Teams;

use Laravel\Spark\Spark;
use Illuminate\Support\Facades\Validator;
use Laravel\Spark\Events\Teams\TeamCreated;
use Laravel\Spark\Contracts\Repositories\TeamRepository;
use Laravel\Spark\Contracts\Interactions\Settings\Teams\CreateTeam as Contract;
use Laravel\Spark\Contracts\Interactions\Settings\Teams\AddTeamMember as AddTeamMemberContract;
use App\Events\Teams\Subscription\RegistrationComplete;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class CreateTeam implements Contract
{
    /**
     * {@inheritdoc}
     */
    public function validator($user, array $data)
    {
        $validator = Validator::make($data, [
            'name' => 'required',
        ]);

        $validator->after(function ($validator) use ($user) {
            $this->validateMaximumTeamsNotExceeded($validator, $user);
        });

        return $validator;
    }

    /**
     * Validate that the maximum number of teams hasn't been exceeded.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @return void
     */
    protected function validateMaximumTeamsNotExceeded($validator, $user)
    {
        if (! $plan = $user->sparkPlan()) {
            return;
        }

        if (is_null($plan->teams)) {
            return;
        }

        if ($plan->teams <= $user->ownedTeams()->count()) {
            $validator->errors()->add('name', 'Please upgrade your subscription to create more teams.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function handle($user, array $data)
    {
        Log::debug(var_export($data, true));
        event(new TeamCreated($team = Spark::interact(
            TeamRepository::class.'@create', [$user, $data]
        )));

        Spark::interact(AddTeamMemberContract::class, [
            $team, $user, 'owner'
        ]);

        if($data['plan'] === "zenjuriesTrial"){
            $team->on_grace_period = 1;
            $team->grace_period_ends = Carbon::today()->addMonth()->toDateString();
            $team->save();
        }
/*
        if(!empty($data['coupon'])){
            $team->coupon = $data['coupon'];
            $team->save();
        }
*/
        return $team;
    }
}

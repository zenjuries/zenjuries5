<?php

namespace Laravel\Spark\Interactions\Settings\Teams;

use App\ExternalUserCompanies;
use Laravel\Spark\Spark;
use Laravel\Spark\Events\Teams\TeamMemberAdded;
use Laravel\Spark\Contracts\Interactions\Settings\Teams\AddTeamMember as Contract;
use App\TeamUser;

class AddTeamMember implements Contract
{
    /**
     * {@inheritdoc}
     */
    public function handle($team, $user, $role = null)
    {
        if($role == null){
            if($user->type == 'internal'){
                $role = 'employee';
            }else if($user->type == 'agent'){
                $role = 'agent';
            }else if($user->type == 'external'){
                $role = 'external user';
            }
        }

        $team_user = \App\TeamUser::where('user_id', $user->id)->where('team_id', $team->id)->first();
        if(is_null($team_user)){
            $team->users()->attach($user, ['role' => $role ?: Spark::defaultRole()]);
        }

        event(new TeamMemberAdded($team, $user));
            $company_id = \App\Company::where('team_id', $team->id)->value('id');
        if($user->type == 'internal' && $user->company_id == NULL){
            $user->company_id =$company_id;
            $user->save();
        }else if($user->type !== 'internal'){
            $links = \App\ExternalUserCompanies::where('company_id', $company_id)->where('user_id', $user->id)->first();
            if(is_null($links)){
                $newLink = new ExternalUserCompanies();
                $newLink->company_id = $company_id;
                $newLink->user_id = $user->id;
                $newLink->save();
            }
        }
    }
}

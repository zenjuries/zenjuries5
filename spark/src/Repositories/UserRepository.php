<?php

namespace Laravel\Spark\Repositories;

use App\ExternalUserCompanies;
use App\ZagentZuser;
use Carbon\Carbon;
use Laravel\Spark\Spark;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Laravel\Spark\Events\PaymentMethod\VatIdUpdated;
use Laravel\Spark\Events\PaymentMethod\BillingAddressUpdated;
use Laravel\Spark\Contracts\Repositories\UserRepository as UserRepositoryContract;
use Illuminate\Support\Facades\Log;

class UserRepository implements UserRepositoryContract
{
    /**
     * {@inheritdoc}
     */
    public function current()
    {
        if (Auth::check()) {
            return $this->find(Auth::id())->shouldHaveSelfVisibility();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        $user = Spark::user()->where('id', $id)->first();

        return $user ? $this->loadUserRelationships($user) : null;
    }

    /**
     * Load the relationships for the given user.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @return \Illuminate\Contracts\Auth\Authenticatable
     */
    protected function loadUserRelationships($user)
    {
        $user->load('subscriptions');

        if (Spark::usesTeams()) {
            $user->load(['ownedTeams.subscriptions', 'teams.subscriptions']);

            $user->currentTeam();
        }

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function search($query, $excludeUser = null)
    {
        $search = Spark::user()->with('subscriptions');

        // If a user to exclude was passed to the repository, we will exclude their User
        // ID from the list. Typically we don't want to show the current user in the
        // search results and only want to display the other users from the query.
        if ($excludeUser) {
            $search->where('id', '<>', $excludeUser->id);
        }

        return $search->where(function ($search) use ($query) {
            $search->where('email', 'like', $query)
                   ->orWhere('name', 'like', $query);
        })->get();
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        $user = Spark::user();

        $is_zenpro = 0;
        if($data['type'] === 'zenpro'){
            $is_zenpro = 1;
            $data['type'] = 'agent';
        }

        $injured = $data['injured'];
        if($injured == 1){
            $injury = \App\Injury::where('id', $data['injuryID'])->first();
            $user = \App\User::where('id', $injury->injured_employee_id)->first();
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->password = bcrypt($data['password']);
            if(array_key_exists('phone', $data)){
                $user->phone = $data['phone'];
            }
            $user->last_read_announcements_at = Carbon::now();
            $user->trial_ends_at = Carbon::now()->addDays(Spark::trialDays());
            $user->type = $data['type'];
            $user->save();
        }else {
            $user->forceFill([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'last_read_announcements_at' => Carbon::now(),
                'trial_ends_at' => Carbon::now()->addDays(Spark::trialDays()),
                'type' => $data['type'],
                'is_zagent' => $data['zagent'],
                'is_zenpro' => $is_zenpro,
            ])->save();
            if (array_key_exists('phone', $data)) {
                $user->phone = $data['phone'];
                $user->save();
            }
            if($user->is_zenpro){
                $user->admin = 1;
                $user->photo_url = "/images/zenjuries-logo.jpg";
                $user->picture_location = "/images/zenjuries-logo.jpg";
                $user->save();
            }

            if ($data['zagent'] == 1) {
                if (isset($data['invitation'])) {
                    $team_id = DB::table('invitations')->where('token', $data['invitation'])->value('team_id');
                    $company = \App\Company::where('team_id', $team_id)->first();
                    $user->zagent_id = $company->id;
                    $user->save();
                    //add them to the client company rosters
                    $clients = \App\Company::where('agency_id', $company->agency_id)->where('is_agency', 0)->get();
                    foreach ($clients as $client) {
                        $link = \App\ExternalUserCompanies::where('company_id', $client->id)->where('user_id', $user->id)->first();
                        if (is_null($link)) {
                            $link = new ExternalUserCompanies();
                            $link->user_id = $user->id;
                            $link->company_id = $client->id;
                            $link->save();
                        }
                    }

                    //handle if agency is broker
                    if($company->is_broker === 1){
                        //loop through the agent clients of the broker
                        $broker_agencies = \App\Company::where('broker_id', $company->id)->get();
                        foreach($broker_agencies as $broker_agency){
                            //create a link for the broker zagent and the agency
                            $link = new \App\ExternalUserCompanies();
                            $link->user_id = $user->id;
                            $link->company_id = $broker_agency->id;
                            $link->save();
                            //then loop through the client agency's policy holders and create a link for each of them and the broker zagent
                            $broker_agency_clients = \App\Company::where('agency_id', $broker_agency->agency_id)->where('is_agency', 0)->get();
                            foreach($broker_agency_clients as $broker_agency_client){
                                $link = new \App\ExternalUserCompanies();
                                $link->user_id = $user->id;
                                $link->company_id = $broker_agency_client->id;
                                $link->save();
                            }
                        }
                    }
                }
            }
        }
        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function updateBillingAddress($user, array $data)
    {
        $user->forceFill([
            'card_country' => array_get($data, 'card_country'),
            'billing_address' => array_get($data, 'address'),
            'billing_address_line_2' => array_get($data, 'address_line_2'),
            'billing_city' => array_get($data, 'city'),
            'billing_state' => array_get($data, 'state'),
            'billing_zip' => array_get($data, 'zip'),
            'billing_country' => array_get($data, 'country'),
        ])->save();

        event(new BillingAddressUpdated($user));
    }

    /**
     * {@inheritdoc}
     */
    public function updateVatId($user, $vatId)
    {
        $user->forceFill(['vat_id' => $vatId])->save();

        event(new VatIdUpdated($user));
    }
}

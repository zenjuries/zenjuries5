<?php

namespace Laravel\Spark\Http\Controllers\Auth;

use App\Events\Teams\Subscription\RegistrationComplete;
use App\Repositories\CommunicationRepository;
use App\Repositories\ZenProRepository;
use Carbon\Carbon;
use Laravel\Spark\Spark;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Spark\Events\Auth\UserRegistered;
use Laravel\Spark\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Laravel\Spark\Contracts\Interactions\Auth\Register;
use Laravel\Spark\Contracts\Http\Requests\Auth\RegisterRequest;
use Illuminate\Support\Facades\DB;
use App\Repositories\MessagingRequestsRepository;
use App\SquadMember;
use App\Squad;
use App\Repositories\StripeRepository;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class RegisterController extends Controller
{
    use RedirectsUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    //protected $redirectTo = '/loginSetup';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(MessagingRequestsRepository $requestRepo, StripeRepository $stripeRepo, CommunicationRepository $communicationRepository)
    {
        $this->middleware('guest');
        $this->repo = $requestRepo;
        $this->stripeRepo = $stripeRepo;
        $this->commRepo = $communicationRepository;
    }

    /**
     * Show the application registration form.
     *
     * @param  Request  $request
     * @return Response
     */
    public function showRegistrationForm(Request $request)
    {
        $percentOff = 0;
        if (Spark::promotion() && ! $request->has('coupon')) {
            // If the application is running a site-wide promotion, we will redirect the user
            // to a register URL that contains the promotional coupon ID, which will force
            // all new registrations to use this coupon when creating the subscriptions.
            return redirect($request->fullUrlWithQuery([
                'coupon' => Spark::promotion()
            ]));
        }

        if($request->has('coupon')) {
            $couponCode = $request->coupon;
                $coupon = $this->stripeRepo->getCoupon($couponCode);
                if($coupon){
                    if (isset($coupon->percent_off)) {
                        $percentOff = $coupon->percent_off;
                    }
                }
        }

        return view('spark::auth.register', ['percent_off' => $percentOff]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  RegisterRequest  $request
     * @return Response
     */
    public function register(RegisterRequest $request)
    {
        if(Auth::check()){
            Auth::logout();
        }

        Auth::login($user = Spark::interact(
            Register::class, [$request]
        ));

        event(new UserRegistered($user));

        //THIS CODE WILL RUN WHEN A NEW COMPANY REGISTERS
        $owner = DB::table('team_users')->where('user_id', $user->id)->where('role', 'owner')->first();
        if(!is_null($owner)) {
            $company = \App\Company::where('team_id', $owner->team_id)->first();
            $squad = new Squad();
            $squad->company_id = $company->id;
            $squad->squad_name = "Default Squad";
            $squad->save();

            $squad->assignDefaultIcon();

            $squadRoleCE = new SquadMember();
            $squadRoleCE->squad_id = $squad->id;
            $squadRoleCE->position_id = 1;
            $squadRoleCE->user_id = $user->id;
            $squadRoleCE->save();

            $squadRoleCN = new SquadMember();
            $squadRoleCN->squad_id = $squad->id;
            $squadRoleCN->position_id = 9;
            $squadRoleCN->user_id = $user->id;
            $squadRoleCN->save();

           // $this->repo->storeInformationRequest()
            /*
            $requestTemplate = \App\InformationType::where('name', "Renewal Date")->first();
            $this->repo->storeInformationRequest($requestTemplate->name, NULL, $requestTemplate->request_body, $user->id, $requestTemplate->id, NULL, NULL, $company->id);
            */
            $this->commRepo->getRenewalDate($company->id);
            event(new RegistrationComplete(\App\Team::where('id', $owner->team_id)->first()));

            if($request->same_as_mailing == "true"){
                $company->mailing_address = $request->address;
                if(!empty($request->address_line_2)) {
                    $company->mailing_address_line_2 = $request->address_line_2;
                }
                $company->mailing_city = $request->city;
                $company->mailing_state = $request->state;
                $company->mailing_zip = $request->zip;
            }else{
                $company->mailing_address = $request->mailing_address;
                if(!empty($request->mailing_address_line_2)) {
                    $company->mailing_address_line_2 = $request->mailing_address_line_2;
                }
                $company->mailing_city = $request->mailing_city;
                $company->mailing_state = $request->mailing_state;
                $company->mailing_zip = $request->mailing_zip;
            }
            if(!empty($request->company_phone)){
                $company->company_phone = $request->company_phone;
            }
            if(isset($request->zen_carrier) && $request->zen_carrier != "false"){
                $company->zen_carrier_id = $request->zen_carrier;
                $zen_carrier = \App\ZenCarrier::where('id', $request->zen_carrier)->first();
                $company->carrier_id = $zen_carrier->carrier_id;
            }

            $company->registered_at = Carbon::now();

            $team = \App\Team::where('id', $owner->team_id)->first();

            // ZENPROCODE
            if(strpos($team->current_billing_plan, "Pro") !== false){
                $company->assignZenpro();
            }
            /*
            if(strpos($team->current_billing_plan, "Pro") !== false){
                $company->preordered_zenpro = 1;
            }
            */

            $company->save();
        }

        $user->setRandomAvatar();

        $user->setUniqueApiToken();

        return response()->json([
            'redirect' => $this->redirectPath()
        ]);

    }
}


<?php

namespace Laravel\Spark\Http\Controllers\Settings\Billing;

use Laravel\Spark\Spark;
use Illuminate\Support\Facades\Mail;
use DOMPDF;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

trait SendsInvoiceNotifications
{
    /**
     * The invoice notification e-mail view.
     *
     * @var string
     */
    protected $emailView = 'spark::settings.invoices.emails.invoice';

    protected $invoiceView = 'resources::invoice';

    /**
     * Send an invoice notification e-mail.
     *
     * @param  mixed  $billable
     * @param  \Laravel\Cashier\Invoice
     * @return void
     */
    protected function sendInvoiceNotification($billable, $invoice)
    {
        $invoiceData = Spark::invoiceDataFor($billable);

        $data = compact('billable', 'invoice', 'invoiceData');

        Mail::send($this->emailView, $data, function ($message) use ($billable, $invoice, $invoiceData) {
            $this->buildInvoiceMessage($message, $billable, $invoice, $invoiceData);
        });
    }

    /**
     * Build the invoice notification message.
     *
     * @param  \Illuminate\Mail\Message  $message
     * @param  mixed  $billable
     * @param  \Laravel\Cashier\Invoice
     * @param  array  $invoiceData
     * @return void
     */
    protected function buildInvoiceMessage($message, $billable, $invoice, array $invoiceData)
    {
		Log::debug('dumping Billable');
		Log::debug(var_export($billable, true));

        function createInvoicePDF($invoiceData, $invoice, $billable){
            if(! defined('DOMPDF_ENABLE_AUTOLOAD')) {
                define('DOMPDF_ENABLE_AUTOLOAD', true);
            }

            if(! defined('DOMPDF_ENABLE_REMOTE')) {
	            define('DOMPDF_ENABLE_REMOTE', true);
            }

            if(file_exists($configPath = base_path().'/vendor/dompdf/dompdf/dompdf_config.inc.php')){
                require_once $configPath;
            }

			$dompdf = new DOMPDF;
            //$dompdf = new DOMPDF(array('enable_remote' => true));

/*
            function returnView($invoiceData, $invoice){
                return View::make($this->invoiceView, array_merge(
                    $invoiceData, ['invoice' => $invoice, 'user' => $invoice->user]
                ));
            }
*/

			function returnView($invoiceData, $invoice, $billable){
				/*
				return View::make('', array_merge(
					$invoiceData, ['invoice' => $invoice, 'user' => $invoice->user]
				));
				*/
					Log::debug("In returnView function");
					Log::debug("dump invoice var");
					Log::debug(var_export($invoice, true));
					Log::debug("dump user var");
					Log::debug(var_export($billable->user, true));

					return view('invoice', array_merge($invoiceData, ['invoice' => $invoice, 'user' => $billable]
				));
			}

            $view = returnView($invoiceData, $invoice, $billable);

            $dompdf->load_html($view->render());

            $dompdf->render();

            return $dompdf->output();
        }

        $message->to($billable->email, $billable->name)
                ->subject($invoiceData['product'].' Invoice')
                ->attachData(createInvoicePDF($invoiceData, $invoice, $billable), 'invoice.pdf');
    }
}

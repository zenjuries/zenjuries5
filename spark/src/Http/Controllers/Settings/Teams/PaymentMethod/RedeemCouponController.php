<?php

namespace Laravel\Spark\Http\Controllers\Settings\Teams\PaymentMethod;

use App\Events\Teams\Subscription\LicenseRedeemed;
use Laravel\Spark\Team;
use Laravel\Spark\Spark;
use Illuminate\Http\Request;
use Laravel\Spark\Http\Controllers\Controller;
use Laravel\Spark\Contracts\Repositories\CouponRepository;
use Laravel\Spark\Contracts\Interactions\Settings\PaymentMethod\RedeemCoupon;
use Illuminate\Support\Facades\Log;
use App\Repositories\StripeRepository;
use App\Repositories\AgencyRepository;

class RedeemCouponController extends Controller
{
    /**
     * The coupon repository implementation.
     *
     * @var CouponRepository
     */
    protected $coupons;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CouponRepository $coupons, StripeRepository $repo, AgencyRepository $agencyRepository)
    {
        $this->coupons = $coupons;

        $this->stripeRepo = $repo;

        $this->agencyRepo = $agencyRepository;

        $this->middleware('auth');
    }

    /**
     * Redeem the given coupon code.
     *
     * @param  Request  $request
     * @param  Team  $team
     * @return Response
     */
    public function redeem(Request $request, Team $team)
    {
        //This runs when coupons are used from settings page, not registration

        /*
         License coupons have to be handled here
         */

        abort_unless($request->user()->ownsTeam($team), 403);

        $this->validate($request, [
            'coupon' => 'required',
        ]);

        // We will verify that the coupon can actually be redeemed. In some cases even
        // valid coupons can not get redeemed by an existing user if this coupon is
        // running as a promotion for brand new registrations to the application.
        if (! $this->coupons->canBeRedeemed($request->coupon)) {
            return response()->json(['coupon' => [
                'This coupon code is invalid.'
            ]], 422);
        }

        $couponCode = $request->coupon;
        $coupon = $this->stripeRepo->getCoupon($couponCode);
        if($coupon){
            if (isset($coupon->percent_off)) {
                if($coupon->percent_off === 100){

                    /*
                     We need to handle...
                    1) Invalid Companies using license (simply validate their account and assign them to the agent)
                    2) Valid Companies with paid subscriptions (cancel their subscription. At the end of the time they have, they'll start using the license)

                     */

                    if($team->valid === 0 || $team->on_grace_period === 1){
                        $companyName = \App\Company::where('team_id', $team->id)->value('company_name');
                        $team->coupon = $couponCode;
                        $team->valid = 1;
                        $team->on_grace_period = 0;
                        $team->grace_period_ends = NULL;
                        $team->save();
                        $this->agencyRepo->newClient($couponCode, $companyName, $team->id);
                    }else{
                         abort_unless($request->user()->ownsTeam($team), 403);

                        $team->subscription()->cancel();

                        $team->old_subscription_ends_on = $team->subscription()->ends_at;

                        event(new LicenseRedeemed($team->fresh()));

                        $team->coupon = $couponCode;
                        $team->valid = 1;
                        $team->on_grace_period = 0;
                        $team->grace_period_ends = NULL;
                        $team->save();

                        $companyName = \App\Company::where('team_id', $team->id)->value('company_name');
                        $this->agencyRepo->newClient($couponCode, $companyName, $team->id);

                    }
                    /*
                    return response()->json(['coupon' => [
                        '100% off'
                    ]], 422);
                    */
                }else{
                    Spark::interact(RedeemCoupon::class, [
                        $team, $request->coupon
                    ]);
                    $companyName = \App\Company::where('team_id', $team->id)->value('company_name');
                    $this->agencyRepo->newClient($couponCode, $companyName, $team->id);
                }
            }
        }
    }
}

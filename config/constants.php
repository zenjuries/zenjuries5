<?php

/*
|--------------------------------------------------------------------------
| Global Constants
|--------------------------------------------------------------------------
|
| Define all global constants here.
|
| To access these, you can use the config helper function
| https://laravel.com/docs/8.x/helpers#method-config
| like:
|   $value = config('constants.file_storage_limit');
|
*/

return [

    /*
    |--------------------------------------------------------------------------
    | File storage limit
    |--------------------------------------------------------------------------
    |
    | Total amount of file storage in MB allowed per injury.
    | Includes all file uploads including images, documents, etc.
    |
    */
    'file_storage_limit' => 50,

];

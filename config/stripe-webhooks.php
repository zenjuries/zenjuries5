<?php

return [
    /*
     * Stripe will sign each webhook using a secret. You can find the used secret at the
     * webhook configuration settings: https://dashboard.stripe.com/account/webhooks.
     */
    'signing_secret' => env('STRIPE_WEBHOOK_SECRET'),

    /*
     * You can define a default job that should be run for all other Stripe event type
     * without a job defined in next configuration.
     * You may leave it empty to store the job in database but without processing it.
     */
    'default_job' => '',

    /*
     * You can define the job that should be run when a certain webhook hits your application
     * here. The key is the name of the Stripe event type with the `.` replaced by a `_`.
     *
     * You can find a list of Stripe webhook types here:
     * https://stripe.com/docs/api#event_types.
     */
    'jobs' => [
        'charge_dispute_created' => \App\Jobs\StripeWebhooks\HandleChargeDisputeCreated::class,
        'charge_failed' => \App\Jobs\StripeWebhooks\HandleChargeFailed::class,
        'charge_refunded' => \App\Jobs\StripeWebhooks\HandleChargeRefunded::class,
        'customer_created' => \App\Jobs\StripeWebhooks\HandleCustomerCreated::class,
        'invoice_created' => \App\Jobs\StripeWebhooks\HandleInvoiceCreated::class,
        'invoice_finalization_failed' => \App\Jobs\StripeWebhooks\HandleInvoiceFinalizationFailed::class,
        'invoice_finalized' => \App\Jobs\StripeWebhooks\HandleInvoiceFinalized::class,
        'invoice_paid' => \App\Jobs\StripeWebhooks\HandleInvoicePaid::class,
        'invoice_payment_action_required' => \App\Jobs\StripeWebhooks\HandleInvoicePaymentActionRequired::class,
        'invoice_payment_failed' => \App\Jobs\StripeWebhooks\HandleInvoicePaymentFailed::class,
        'invoice_payment_succeeded' => \App\Jobs\StripeWebhooks\HandleInvoicePaymentSucceeded::class,
        'invoice_upcoming' => \App\Jobs\StripeWebhooks\HandleInvoiceUpcoming::class,
        'invoice_updated' => \App\Jobs\StripeWebhooks\HandleInvoiceUpdated::class,
        'payment_intent_created' => \App\Jobs\StripeWebhooks\HandlePaymentIntentCreated::class,
        'payment_intent_processing' => \App\Jobs\StripeWebhooks\HandlePaymentIntentProcessing::class,
        'payment_intent_succeeded' => \App\Jobs\StripeWebhooks\HandlePaymentIntentSucceeded::class,
        'checkout_session_completed' => \App\Jobs\StripeWebhooks\HandleSessionCompleted::class,
        'checkout_session_expired' => \App\Jobs\StripeWebhooks\HandleSessionExpired::class,
        'subscription_created' => \App\Jobs\StripeWebhooks\HandleSubscriptionCreated::class,
        'subscription_deleted' => \App\Jobs\StripeWebhooks\HandleSubscriptionDeleted::class,
        // 'subscription_trial_will_end' => \App\Jobs\StripeWebhooks\HandleSubscriptionTrialWillEnd::class,
        'subscription_updated' => \App\Jobs\StripeWebhooks\HandleSubscriptionUpdated::class,
        'setup_intent_succeeded' => \App\Jobs\StripeWebhooks\HandleSetupIntentSucceeded::class,
        // 'source_chargeable' => \App\Jobs\StripeWebhooks\HandleChargeableSource::class,
    ],

    /*
     * The classname of the model to be used. The class should equal or extend
     * Spatie\WebhookClient\Models\WebhookCall.
     */
    'model' => \Spatie\WebhookClient\Models\WebhookCall::class,

    /**
     * This class determines if the webhook call should be stored and processed.
     */
    'profile' => \Spatie\StripeWebhooks\StripeWebhookProfile::class,

    /*
     * When disabled, the package will not verify if the signature is valid.
     * This can be handy in local environments.
     */
    'verify_signature' => env('STRIPE_SIGNATURE_VERIFY', true),
];

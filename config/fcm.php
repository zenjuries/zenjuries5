<?php

// config/fcm.php

return [
    'token' => env('FIREBASE_AUTH_KEY'),
];
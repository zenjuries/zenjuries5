<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Squad extends Model
{
    //
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function squadMembers()
    {
        return $this->hasMany('App\SquadMember');
    }

    public function firstReportOfInjuries(){
        return $this->hasMany('App\FirstReportOfInjury');
    }

    public function injuries(){
        return $this->hasMany('App\Injury');
    }

    public function assignDefaultIcon(){
        //$default_icon = \App\DefaultSquadIcons::inRandomOrder()->first();
        //$default_icon = DB::table('default_squad_icons')->inRandomOrder()->first();
        $default_icon = DB::table('default_squad_icons')->orderByRaw("RAND()")->first();
        if(!is_null($default_icon)){
            $this->avatar_location = $default_icon->avatar_location;
            $this->background_color = $default_icon->background_color;
            $this->avatar_color = $default_icon->avatar_color;
            $this->avatar_number = $default_icon->avatar_number;
            $this->avatar_icon_color = $default_icon->avatar_icon_color;
            $this->save();
        }
    }

    public function createInjuryTeam($team_id){
        $old_team = \App\Squad::where('id', $team_id)->first();
        $old_team_members = \App\SquadMember::where('squad_id', $team_id)->get();

        $this->company_id = $old_team->company_id;
        //want this to have the injured person's name
        $this->squad_name = "Injury Team";
        $this->avatar_location = $old_team->avatar_location;
        $this->background_color = $old_team->background_color;
        $this->avatar_color = $old_team->avatar_color;
        $this->avatar_number = $old_team->avatar_number;
        $this->avatar_icon_color = $old_team->avatar_icon_color;
        $this->avatar_times_changed = 0;
        $this->injury_squad = 1;
        $this->original_squad_id = $team_id;
        $this->save();

        foreach($old_team_members as $old_team_member){
            $new_team_member = new SquadMember();
            $new_team_member->squad_id = $this->id;
            $new_team_member->position_id = $old_team_member->position_id;
            $new_team_member->user_id = $old_team_member->user_id;
            $new_team_member->save();
        }

        return $this->id;
    }

    public function getChiefNavigatorName(){
        $member = \App\SquadMember::where('squad_id', $this->id)->where('position_id', 9)->first();
        $member->printMemberName();
    }

    public function getChiefExecutiveName(){
        $member = \App\SquadMember::where('squad_id', $this->id)->where('position_id', 1)->first();
        $member->printMemberName();
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Communication_Template extends Model
{
    protected $table = 'communication_templates';

    public function company(){
        return $this->belongsTo('App/Company');
    }

    public function followup(){
        return $this->hasOne('App/Communication_Template', 'id');
    }

    public function communications(){
       return  $this->hasMany('App/Communication');
    }

    public function messages(){
        return $this->hasManyThrough('App\Communication_Message', 'App\Communication');
    }

    public function communication_position(){
        return $this->hasOne('App\Communication_Position');
    }

    public function trigger(){
        return $this->belongsTo('App\Trigger');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FirstReportOfInjury extends Model
{
    //
    protected $table = 'first_report_of_injuries';

    protected $dates = ['last_message_sent_at'];

    public function Injury(){
        return $this->belongsTo('App/Injury');
    }

    public function Squad(){
        return $this->belongsTo('App/Squad');
    }
}

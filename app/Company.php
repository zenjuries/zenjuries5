<?php

namespace App;

use App\Repositories\ZenProRepository;
use App\Repositories\StatsRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Laravel\Spark\Interactions\Settings\Teams\SendInvitation;
use Laravel\Cashier\Billable;
use App\Squad;
use App\SquadMember;
use Illuminate\Support\Facades\DB;

class Company extends Model
{
    //
    use Billable;

    protected $fillable = [
        'company_name'
    ];

    public function createDefaultSquad($user_id){
        $default_squad = new Squad();
        $default_squad->company_id = $this->id;
        $default_squad->squad_name = "Default Squad";
        $default_squad->save();
        $default_squad->assignDefaultIcon();
        /*** Create the squad members ***/
        $chief_exec = new SquadMember();
        $chief_exec->squad_id = $default_squad->id;
        $chief_exec->position_id = 1;
        $chief_exec->user_id = $user_id;
        $chief_exec->save();
        $chief_nav = new SquadMember();
        $chief_nav->squad_id = $default_squad->id;
        $chief_nav->position_id = 9;
        $chief_nav->user_id = $user_id;
        $chief_nav->save();
    }

    public static function returnValidCompanies(){
        $companies = Company::join('teams', 'teams.id', '=', 'companies.id')
            ->where('teams.valid', 1)
            ->select('companies.*')
            ->get();
        return $companies;
    }

    public function getCompanyOwner(){
        $team = \App\Team::where('id', $this->team_id)->first();
        if($team !== NULL){
            $owner = \App\User::where('id', $team->owner_id)->first();
        }else{
            return NULL;
        }
        return $owner;
    }

    public function getPolicySubmission(){
        $submission = PolicySubmission::where('company_id', $this->id)->first();
        return $submission;
    }

    public function getAgency(){
        if($this->agency_id !== NULL){
            $agency = Agency::where('id', $this->agency_id)->first();
            return $agency;
        }else return NULL;
    }

    public function addAgents(){
        $company = $this;
        $agent_company = \App\Company::where('agency_id', $company->agency_id)->where('is_agency', 1)->first();
        $agent_company_id = $agent_company->id;
        if($agent_company !== NULL){
            $zagent_zusers = \App\User::where('zagent_id', $agent_company_id)->get();
            foreach ($zagent_zusers as $agent) {
                $existing_link = ExternalUserCompanies::where('user_id', $agent->id)->where('company_id', $company->id)->first();
                if(is_null($existing_link)){
                    $link = new ExternalUserCompanies();
                    $link->user_id = $agent->id;
                    $link->company_id = $company->id;
                    $link->save();
                    //$this->comm_repo->newClientZAgentZUser($agent->id, $company->id);
                }

            }
            /*
            //check if agency belongs to broker, if so add agents
            if($agent_company->broker_id !== NULL){
                $broker_agency = \App\Company::where('id', $agent_company->broker_id)->first();
                if(!is_null($broker_agency)){
                    $broker_zagents = \App\User::where('zagent_id', $broker_agency->id)->get();
                    foreach($broker_zagents as $broker_zagent){
                        $link = new ExternalUserCompanies();
                        $link->user_id = $broker_zagent->id;
                        $link->company_id = $company->id;
                        $link->save();
                        $this->comm_repo->newClientZAgentZUser($broker_zagent->id, $company->id);
                    }
                }
            }
            */

        }
    }

    public function getInjuries($status = "all", $start_date = null, $end_date = null){

        // check for date period
        $datePeriodCheck = ($start_date != null && $end_date != null) ? True : False;

        if($status === "all"){
            $injuries = Injury::where('company_id', $this->id)
                ->when($datePeriodCheck, function ($query) use ($start_date, $end_date){
                    $dateMatch = [['created_at', '>=', Carbon::parse($start_date)], ['created_at', '<=', Carbon::parse($end_date)]];
                    $query->where($dateMatch);
                })
                ->get();
        }else if($status === "open"){
            $injuries = Injury::where('company_id', $this->id)
                ->when($datePeriodCheck, function ($query) use ($start_date, $end_date){
                    $dateMatch = [['created_at', '>=', Carbon::parse($start_date)], ['created_at', '<=', Carbon::parse($end_date)]];
                    $query->where($dateMatch);
                })
                ->where('resolved', 0)
                ->get();
        }else if($status === "resolved"){
            $injuries = Injury::where('company_id', $this->id)
                ->when($datePeriodCheck, function ($query) use ($start_date, $end_date){
                    $dateMatch = [['created_at', '>=', Carbon::parse($start_date)], ['created_at', '<=', Carbon::parse($end_date)]];
                    $query->where($dateMatch);
                })
                ->where('resolved', 1)
                ->get();

        }
        return $injuries;
    }

    public function getCriticalInjuries(){
        $injuries = Injury::where('company_id', $this->id)
                ->where('resolved', 0)
                ->where('created_at', '<=', Carbon::now()->subDays(30)->toDateTimeString())
                ->get();

        return $injuries;
    }

    public function percentageOfOnTimeClaims(){
        $injuries = $this->getInjuries();
        $same_day = 0;
        $same_day_percent = 0;
        $count = 0;
        Log::debug('created_at controller: ' . $this->created_id);
        $company_joined_on = Carbon::createFromFormat("Y-m-d H:i:s", $this->created_at);
        Log::debug('company joined on: ' . $company_joined_on);
        foreach($injuries as $injury){

            $reported_at = Carbon::createFromFormat("Y-m-d H:i:s", $injury->created_at);
            $occurred_at = Carbon::createFromFormat("m-d-Y g:i a", $injury->injury_date);

            if($occurred_at->gt($company_joined_on)){
                //injury occured after company joined Zenjuries, count it
                $hours = $reported_at->diffInHours($occurred_at);
                if($hours <= 24){
                    $same_day++;
                }
                $count++;
            }
        }

        if($count !== 0){
            $same_day_percent = ($same_day / $count) * 100;
        }else{
            return 100;
        }

        return round($same_day_percent);

    }

    public function percentageOfClaimsLitigated(){
        $injuries = $this->getInjuries();
        $total = count($injuries);
        if($total > 0){
            $litigated_count = 0;
            foreach($injuries as $injury){
                if($injury->paused_for_litigation === 1){
                    $litigated_count++;
                }
            }

            return strval(round(($litigated_count / $total) * 100, 0)) . "%";
        }else return "N/A";
    }



    public function getTop10Claims($start_date, $end_date){
        $dateMatch = [['created_at', '>=', Carbon::createFromFormat('d-m-Y', $start_date)->format('Y-m-d')], ['created_at', '<=', Carbon::createFromFormat('d-m-Y', $end_date)->format('Y-m-d')]];
        $injuries = Injury::where('company_id', $this->id)
            ->where($dateMatch)
            ->limit(10)
            ->get();

        $formattedInjuries = $injuries->map(function($injury) {
            $injuredEmployeeName = $injury->employee->name;
            $injuredEmployeeNameSplit = explode(' ', $injuredEmployeeName);
            $injuredEmployeeFirstName = $injuredEmployeeNameSplit[0] ?? "None";
            $injuredEmployeeLastName = $injuredEmployeeNameSplit[1] ?? "None";
            $formattedInjury = [
                "id"=>$injury->id,
                // claim number
                "claim_number"=>$injury->claim_number ?? "None",
                // first name
                "first_name"=>$injuredEmployeeFirstName,
                // last name
                "last_name"=>$injuredEmployeeLastName,
                // injury date
                "injured_date"=>$injury->injury_date,
                // status
                "status"=>$injury->resolved,
                // incurred
                "incurred"=>$injury->getIncurredTotal() ?? 0,
                // paid
                "paid"=>$injury->getPaidTotal() ?? 0,
                // reserves
                "reserves"=>$injury->getReserveTotal() ?? 0
            ];
            return $formattedInjury;
        });

        $filteredInjuries = $formattedInjuries->sortByDesc('incurred')->take(10);

        return $filteredInjuries;
    }

    // public function getTotalIncurred($start_date = null, $end_date = null){
    //     // check for date period
    //     $datePeriodCheck = ($start_date != null && $end_date != null) ? True : False;

    //     $injuriesFinalCost = Injury::where('company_id', $this->id)
    //         ->orderBy('final_cost', 'desc')
    //         ->when($datePeriodCheck, function ($query) use ($start_date, $end_date){
    //             $dateMatch = [['created_at', '>=', Carbon::parse($start_date)], ['created_at', '<=', Carbon::parse($end_date)]];
    //             $query->where($dateMatch);
    //         })
    //         ->sum('final_cost');

    //     return $injuriesFinalCost;
    // }

    public function getTotalIncurred($start_date = null, $end_date = null){

        $datePeriodCheck = ($start_date != null && $end_date != null) ? True : False;

        $start_date = ($datePeriodCheck) ? Carbon::createFromFormat('m-d-Y', $start_date)->format('Y-m-d') : NULL;
        $end_date = ($datePeriodCheck) ? Carbon::createFromFormat('m-d-Y', $end_date)->format('Y-m-d'): NULL;

        $injuries = ($datePeriodCheck) ? $this->getInjuries("all", $start_date, $end_date) : $this->getInjuries("all");

        $total = count($injuries);
        // Log::debug('avg claim duration count: ' . $total);
        if($total > 0){
            $totalIncurred = 0;
            foreach ($injuries as $injury){
                $totalIncurred+= $injury->getIncurredTotal();
            }
            return $totalIncurred;
        }else return 0;

    }

    public function getTotalReserve($start_date = null, $end_date = null){

        $datePeriodCheck = ($start_date != null && $end_date != null) ? True : False;

        $start_date = ($datePeriodCheck) ? Carbon::createFromFormat('m-d-Y', $start_date)->format('Y-m-d') : NULL;
        $end_date = ($datePeriodCheck) ? Carbon::createFromFormat('m-d-Y', $end_date)->format('Y-m-d'): NULL;

        $injuries = ($datePeriodCheck) ? $this->getInjuries("all", $start_date, $end_date) : $this->getInjuries("all");

        $total = count($injuries);
        // Log::debug('avg claim duration count: ' . $total);
        if($total > 0){
            $totalIncurred = 0;
            foreach ($injuries as $injury){
                $totalIncurred+= $injury->getReserveTotal();
            }
            return $totalIncurred;
        }else return 0;

    }

    public function getTotalPaid($start_date = null, $end_date = null){

        $datePeriodCheck = ($start_date != null && $end_date != null) ? True : False;

        $start_date = ($datePeriodCheck) ? Carbon::createFromFormat('m-d-Y', $start_date)->format('Y-m-d') : NULL;
        $end_date = ($datePeriodCheck) ? Carbon::createFromFormat('m-d-Y', $end_date)->format('Y-m-d'): NULL;

        $injuries = ($datePeriodCheck) ? $this->getInjuries("all", $start_date, $end_date) : $this->getInjuries("all");

        $total = count($injuries);
        // Log::debug('avg claim duration count: ' . $total);
        if($total > 0){
            $totalIncurred = 0;
            foreach ($injuries as $injury){
                $totalIncurred+= $injury->getPaidTotal();
            }
            return $totalIncurred;
        }else return 0;

    }

    public function getTotalClaims($start_date = null, $end_date = null){
        // check for date period
        $datePeriodCheck = ($start_date != null && $end_date != null) ? True : False;

        $totalAmountOfInjuries = Injury::where('company_id', $this->id)
            ->when($datePeriodCheck, function ($query) use ($start_date, $end_date){
                $dateMatch = [['created_at', '>=', Carbon::createFromFormat('m-d-Y', $start_date)->format('Y-m-d')], ['created_at', '<=', Carbon::createFromFormat('m-d-Y', $end_date)->format('Y-m-d')]];
                $query->where($dateMatch);
            })
            ->count();
        return $totalAmountOfInjuries;

    }


    public function getTopInjuryLocation($start_date = null, $end_date = null){
        // check for date period
        $datePeriodCheck = ($start_date != null && $end_date != null) ? True : False;

        $injuryLocations = $this->hasManyThrough(InjuryLocation::class, Injury::class)
            ->when($datePeriodCheck, function ($query) use ($start_date, $end_date){
                $dateMatch = [['created_at', '>=', Carbon::createFromFormat('d-m-Y', $start_date)->format('Y-m-d')], ['created_at', '<=', Carbon::createFromFormat('d-m-Y', $end_date)->format('Y-m-d')]];
                $query->where($dateMatch);
            })
            ->select('location')
            ->selectRaw('COUNT(*) AS count')
            ->groupBy('location')
            ->orderByDesc('count')
            ->limit(1)
            ->get();

        $formattedResponse = [
            "location" => $injuryLocations[0]->location ?? 0,
            "count" => $injuryLocations[0]->count ?? 0
        ];
        return $formattedResponse;

    }

    public function getTopInjuryDay($start_date = null, $end_date = null){
        // check for date period
        $datePeriodCheck = ($start_date != null && $end_date != null) ? True : False;

        $injuries = Injury::where('company_id', $this->id)
            ->when($datePeriodCheck, function ($query) use ($start_date, $end_date){
                $dateMatch = [['created_at', '>=', Carbon::createFromFormat('d-m-Y', $start_date)->format('Y-m-d')], ['created_at', '<=', Carbon::createFromFormat('d-m-Y', $end_date)->format('Y-m-d')]];
                $query->where($dateMatch);
            })
            ->get();
        $statRepo = new StatsRepository();
        $dailyBreakdown=$statRepo->calculateDailyBreakdown($injuries);
        //Get the max value in the array.
        $maxVal = max($dailyBreakdown);

        //Get all of the keys that contain the max value.
        $maxValKeys = array_keys($dailyBreakdown, $maxVal);
        $formattedResponse = [
            "days" => $maxValKeys ?? 0,
            "max" => $maxVal ?? 0
        ];
        return $formattedResponse;

    }

    public function getInjuryReportTime($start_date = null, $end_date = null){
        // check for date period
        $datePeriodCheck = ($start_date != null && $end_date != null) ? True : False;

        $injuries = Injury::where('company_id', $this->id)
            ->when($datePeriodCheck, function ($query) use ($start_date, $end_date){
                $dateMatch = [['created_at', '>=', Carbon::createFromFormat('d-m-Y', $start_date)->format('Y-m-d')], ['created_at', '<=', Carbon::createFromFormat('d-m-Y', $end_date)->format('Y-m-d')]];
                $query->where($dateMatch);
            })
            ->get();
        $statRepo = new StatsRepository();
        $breakdown=$statRepo->calculateReportingTimeArray($injuries);
        $injuryCount = count($injuries);
        $amountUnder24Hours = $breakdown[0] ?? 0;
        $percentCalculation = ($injuryCount > 0 && $amountUnder24Hours > 0) ? $amountUnder24Hours/$injuryCount*100 : 0;

        $formattedResponse = [
            "amount" => $amountUnder24Hours ?? 0,
            "percent" => $percentCalculation ?? 0
        ];
        return $formattedResponse;

    }

    function getStats($start_date, $end_date){
        $stats_repo = new StatsRepository();
        $injuries = $stats_repo->getCompanyInjuries($this->id, $start_date, $end_date);
        $stats_array = $stats_repo->returnStatsArray($injuries);
        // dd($stats_array);
        return $stats_array;
    }


    public function averageClaimDuration($start_date = null, $end_date = null){
        $injuries = $this->getInjuries("resolved");
        $total = count($injuries);
        // Log::debug('avg claim duration count: ' . $total);
        if($total > 0){
            $total_days = 0;
            foreach ($injuries as $injury){
                $total_days+= $injury->getDuration();
            }
            Log::debug('avg claim duration total days: ' . $total_days);
            return strval(round($total_days / $total, 0));
        }else return 0;

    }

    // 1:1 with Spark Team
    public function team()
    {
        return $this->belongsTo('App\Team');
    }

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function squads(){

        return $this->hasMany('App\Squad');
    }

    public function squadMembers(){
        return $this->hasManyThrough('App\SquadMember', 'App\Squad');
    }

    public function injuries(){
        return $this->hasMany('App\Injury');
    }

    public function open_injuries(){
        return $this->hasMany('App\Injury')->where('resolved', '=', 0)->where('hidden', 0);
    }

    public function communication_template(){
        return $this->hasMany('App\Communication_Template');
    }

    public function communications(){
        return $this->hasMany('App\Communication');
    }

    //function that sends email to every member of the company
    //accepts the email template and subject as parameters
    public function emailAll($template, $subject){
        $that = $this;
        $employees = \App\User::where('company_id', $that->id)->get();
        foreach($employees as $employee){
            $employee->sendEmail($template, $subject);
        }
    }

    //function that sends email to every currently injured member of the company
    //accepts the email template and subject as paramenters
    public function emailInjured($template, $subject){
        $that = $this;
        $employees = \App\User::where('company_id', $that->id)->where('injured', 1)->get();
        foreach($employees as $employee){
            $employee->sendEmil($template, $subject);
        }
    }

    //function that sends email to every member of the company with specified position
    //accepts email template, subject, and the position of the employee as parameters
    public function emailPosition($template, $subject, $position){
        $that = $this;
        $employees = \App\User::where('company_id', $that->id)->where('position', $position)->get();
        foreach($employees as $employee){
            $employee->sendEmail($template, $subject);
        }
    }

    //function that sends email to every member of a squad
    //accepts email template, subject, and squadname as paramaters
    //squadname is an optional paramater
    // if no squadname is entered every member of every squad belonging to that company will be emailed
    public function emailSquad($template, $subject, $squadname = ""){
        $that = $this;
        if($squadname !== ""){
            echo $squadname . "<br>";
            $squad = \App\Squad::where('company_id', $that->id)->where('squad_name', $squadname)->first();
            if($squad !== ""){
                echo "squad<br>";
            }
            $squadmates = \App\SquadMember::where('squad_id', $squad->id)->get();
            //var_dump($squadmates);
        } else {
            echo "no squadname <br>";
            $squadmates = $that->squadMembers;
        }
        $count = 0;
        foreach($squadmates as $squadmate){
            echo $count . '<br>';
            $user = \App\User::where('id', $squadmate->user_id)->first();
            $user->sendEmail($template, $subject);
            $count = $count + 1;
        }
    }

    //function that sends text to every member of the company
    //accepts the message that will be sent as a parameter
    public function textAll($message){
        $that = $this;
        $employees = \App\User::where('company_id', $that->id)->get();
        foreach($employees as $employee){
            $employee->sendSMS($message);
        }
    }

    //function that sends text to every currently injured member of the company
    //accepts the message that will be sent as a parameter
    public function textInjured($message){
        $that = $this;
        $employees = \App\User::where('company_id', $that->id)->where('injured', 1)->get();
        foreach($employees as $employee){
            $employee->sendSMS($message);
        }
    }

    //function that sends text to every member of the company with specified position
    //accepts the message that will be sent and the desired position as parameters
    public function textPosition($message, $position){
        $that = $this;
        $employees = \App\User::where('company_id', $that->id)->where('position', $position)->get();
        foreach($employees as $employee){
            $employee->sendSMS($message);
        }
    }

    //function that sends text to every member of a company's squad
    //accepts the message that will be sent and optionally the name of the squad
    //if no name is given the text will be sent to all members of that company's squad
    public function textSquad($message, $squadname = ""){
        $that = $this;
        if($squadname !== ""){
            echo $squadname . "<br>";
            $squad = \App\Squad::where('company_id', $that->id)->where('squad_name', $squadname)->first();
            if($squad !== ""){
                echo "squad<br>";
            }
            $squadmates = \App\SquadMember::where('squad_id', $squad->id)->get();
            //var_dump($squadmates);
        } else {
            echo "no squadname <br>";
            $squadmates = $that->squadMembers;
        }
        $count = 0;
        foreach($squadmates as $squadmate){
            echo $count . '<br>';
            $user = \App\User::where('id', $squadmate->user_id)->first();
            $user->sendSMS($message);
            $count = $count + 1;
        }
    }

    //excludes in_progress injuries

    //only includes open injuries
    public function echoCurrentInjuryCount(){
        $count = count(\App\Injury::where('company_id', $this->id)->where('resolved', 0)->where('in_progress', 0)->where('hidden', 0)->get());
        echo $count;
    }

    //only includes open injuries
    public function returnOpenInjuryCount(){
        $count = count(\App\Injury::where('company_id', $this->id)->where('resolved', 0)->where('in_progress', 0)->where('hidden', 0)->get());
        return $count;
    }

    //includes both open and resolved injuries
    public function returnInjuryCount(){
        $count = count(\App\Injury::where('company_id', $this->id)->where('in_progress', 0)->where('hidden', 0)->get());
        return $count;
    }

    public function resendClientInvite(){
        $team_id = $this->team_id;
        $user_id = \App\TeamUser::where('team_id', $team_id)->where('role', 'owner')->value('user_id');
        $user = \App\User::where('id', $user_id)->first();
        Mail::send('emailTemplates.verifyAccount', ['user' => $user], function ($m) use ($user){
            $m->to($user->email, $user->name)->subject('Welcome to Zenjuries!');
        });
        if(Mail::flushMacros()){
            return false;
        }else{
            $this->invite_resent_on = Carbon::now();
            $this->save();
            return $user->email;
        }
    }

    public function assignZenpro(){
        $zenpro_repo = new ZenProRepository();
        $zenpro_repo->assignZenPro($this->id);
    }

    public function getZenproId(){
        /*
        if($this->has_zenpro === 1){
            $zenpro_id = \App\ExternalUserCompanies::join('users', 'external_users_companies.user_id', '=', 'users.id')->
                where('external_users_companies.company_id', $this->id)->where('users.is_zenpro', 1)->select('users.id')->first();
            return $zenpro_id->id;
        }else return false;
        */
        $zenpro_id = \App\ExternalUserCompanies::join('users', 'external_users_companies.user_id', '=', 'users.id')->
            where('external_users_companies.company_id', $this->id)->where('users.is_zenpro', 1)->select('users.id')->first();
        return $zenpro_id->id;
    }

    function setCompanyColor($color){
        Log::debug($color);
        $this->logo_color = $color;
        $this->save();
    }

    function pays(){
        // Check if company is an agent
        if ($this->is_agency == 1) return true;

        // Grab policy submission
        $submission = PolicySubmission::where('company_id', $this->id)->first();
        // Check for policy submission
        if(is_null($submission)) return false;
        // Check if client pays
        if($submission->client_pays != 1) return false;

        return true;
    }

}

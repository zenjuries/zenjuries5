<?php

namespace App\Jobs\StripeWebhooks;

use Stripe;
use Laravel\Cashier\Cashier;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repositories\PaymentsRepository;
use Illuminate\Support\Facades\Log;
use Spatie\WebhookClient\Models\WebhookCall;

class HandleSetupIntentSucceeded implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /** @var \Spatie\WebhookClient\Models\WebhookCall */
    public $webhookCall;

    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    public function handle()
    {
        $data = $this->webhookCall->payload;
        
        try {
            $stripe = new \Stripe\StripeClient(config('stripe.secret'));
            $intent = $stripe->setupIntents->retrieve(
                $data["data"]["object"]["id"],
                []
            );

            $metadata = $intent->metadata;
            $customer = $metadata->customer;
            $companyId = $metadata->company_id;

            // verifications && additional checking
            
            $company = \App\Company::where('id', $companyId)->first();

            $payment_repo = new PaymentsRepository;

            if ($customer == "register"){
                $payment_repo->newZagentRegistration($company->id, $intent->payment_method);
            } else {
                $payment_repo->updateCard($company->id, $intent->payment_method);
            }

            // send notification of payment change

            return 200;
            
        } catch (Exception $e) {
            // $e
        }
    }
}
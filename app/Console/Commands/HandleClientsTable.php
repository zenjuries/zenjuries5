<?php

namespace App\Console\Commands;

use App\Models\AgencyClient;
use Illuminate\Console\Command;

class HandleClientsTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'handle:clients_table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //json_encode the data coming from the zagent site, decode it here

        $client_array = json_decode('[{"zenjuries_company_id":1,"zenjuries_agency_id":13},{"zenjuries_company_id":2,"zenjuries_agency_id":13},{"zenjuries_company_id":3,"zenjuries_agency_id":13},{"zenjuries_company_id":36,"zenjuries_agency_id":13},{"zenjuries_company_id":37,"zenjuries_agency_id":13},{"zenjuries_company_id":39,"zenjuries_agency_id":16},{"zenjuries_company_id":40,"zenjuries_agency_id":16},{"zenjuries_company_id":41,"zenjuries_agency_id":16},{"zenjuries_company_id":42,"zenjuries_agency_id":16},{"zenjuries_company_id":44,"zenjuries_agency_id":17},{"zenjuries_company_id":45,"zenjuries_agency_id":17},{"zenjuries_company_id":46,"zenjuries_agency_id":17},{"zenjuries_company_id":47,"zenjuries_agency_id":17},{"zenjuries_company_id":48,"zenjuries_agency_id":17},{"zenjuries_company_id":49,"zenjuries_agency_id":17},{"zenjuries_company_id":50,"zenjuries_agency_id":17},{"zenjuries_company_id":51,"zenjuries_agency_id":17},{"zenjuries_company_id":52,"zenjuries_agency_id":17},{"zenjuries_company_id":53,"zenjuries_agency_id":17},{"zenjuries_company_id":54,"zenjuries_agency_id":17},{"zenjuries_company_id":56,"zenjuries_agency_id":18},{"zenjuries_company_id":57,"zenjuries_agency_id":18},{"zenjuries_company_id":58,"zenjuries_agency_id":18},{"zenjuries_company_id":59,"zenjuries_agency_id":18},{"zenjuries_company_id":60,"zenjuries_agency_id":18},{"zenjuries_company_id":61,"zenjuries_agency_id":18},{"zenjuries_company_id":62,"zenjuries_agency_id":18},{"zenjuries_company_id":63,"zenjuries_agency_id":18},{"zenjuries_company_id":64,"zenjuries_agency_id":18},{"zenjuries_company_id":65,"zenjuries_agency_id":18},{"zenjuries_company_id":66,"zenjuries_agency_id":18},{"zenjuries_company_id":67,"zenjuries_agency_id":18},{"zenjuries_company_id":null,"zenjuries_agency_id":20},{"zenjuries_company_id":null,"zenjuries_agency_id":20},{"zenjuries_company_id":null,"zenjuries_agency_id":20},{"zenjuries_company_id":null,"zenjuries_agency_id":20},{"zenjuries_company_id":71,"zenjuries_agency_id":20},{"zenjuries_company_id":null,"zenjuries_agency_id":20},{"zenjuries_company_id":72,"zenjuries_agency_id":20},{"zenjuries_company_id":null,"zenjuries_agency_id":20},{"zenjuries_company_id":73,"zenjuries_agency_id":20},{"zenjuries_company_id":null,"zenjuries_agency_id":20},{"zenjuries_company_id":74,"zenjuries_agency_id":20},{"zenjuries_company_id":75,"zenjuries_agency_id":20},{"zenjuries_company_id":76,"zenjuries_agency_id":20},{"zenjuries_company_id":85,"zenjuries_agency_id":20},{"zenjuries_company_id":86,"zenjuries_agency_id":20},{"zenjuries_company_id":87,"zenjuries_agency_id":20},{"zenjuries_company_id":88,"zenjuries_agency_id":20},{"zenjuries_company_id":89,"zenjuries_agency_id":20},{"zenjuries_company_id":90,"zenjuries_agency_id":20},{"zenjuries_company_id":91,"zenjuries_agency_id":20},{"zenjuries_company_id":92,"zenjuries_agency_id":20},{"zenjuries_company_id":93,"zenjuries_agency_id":20},{"zenjuries_company_id":95,"zenjuries_agency_id":21},{"zenjuries_company_id":96,"zenjuries_agency_id":21},{"zenjuries_company_id":97,"zenjuries_agency_id":21},{"zenjuries_company_id":98,"zenjuries_agency_id":21},{"zenjuries_company_id":99,"zenjuries_agency_id":21},{"zenjuries_company_id":100,"zenjuries_agency_id":21},{"zenjuries_company_id":101,"zenjuries_agency_id":21},{"zenjuries_company_id":102,"zenjuries_agency_id":21},{"zenjuries_company_id":103,"zenjuries_agency_id":21},{"zenjuries_company_id":104,"zenjuries_agency_id":21},{"zenjuries_company_id":105,"zenjuries_agency_id":21},{"zenjuries_company_id":106,"zenjuries_agency_id":21},{"zenjuries_company_id":111,"zenjuries_agency_id":25},{"zenjuries_company_id":112,"zenjuries_agency_id":25},{"zenjuries_company_id":113,"zenjuries_agency_id":25},{"zenjuries_company_id":115,"zenjuries_agency_id":26},{"zenjuries_company_id":116,"zenjuries_agency_id":26},{"zenjuries_company_id":117,"zenjuries_agency_id":26},{"zenjuries_company_id":119,"zenjuries_agency_id":27},{"zenjuries_company_id":121,"zenjuries_agency_id":26},{"zenjuries_company_id":122,"zenjuries_agency_id":26},{"zenjuries_company_id":123,"zenjuries_agency_id":26},{"zenjuries_company_id":124,"zenjuries_agency_id":26},{"zenjuries_company_id":125,"zenjuries_agency_id":28},{"zenjuries_company_id":126,"zenjuries_agency_id":28},{"zenjuries_company_id":127,"zenjuries_agency_id":26},{"zenjuries_company_id":128,"zenjuries_agency_id":26},{"zenjuries_company_id":129,"zenjuries_agency_id":26},{"zenjuries_company_id":130,"zenjuries_agency_id":26},{"zenjuries_company_id":131,"zenjuries_agency_id":26},{"zenjuries_company_id":132,"zenjuries_agency_id":26},{"zenjuries_company_id":134,"zenjuries_agency_id":29},{"zenjuries_company_id":136,"zenjuries_agency_id":30},{"zenjuries_company_id":138,"zenjuries_agency_id":31},{"zenjuries_company_id":139,"zenjuries_agency_id":26},{"zenjuries_company_id":140,"zenjuries_agency_id":26},{"zenjuries_company_id":142,"zenjuries_agency_id":320000},{"zenjuries_company_id":144,"zenjuries_agency_id":33},{"zenjuries_company_id":145,"zenjuries_agency_id":26},{"zenjuries_company_id":146,"zenjuries_agency_id":26},{"zenjuries_company_id":147,"zenjuries_agency_id":26},{"zenjuries_company_id":148,"zenjuries_agency_id":26},{"zenjuries_company_id":149,"zenjuries_agency_id":26},{"zenjuries_company_id":150,"zenjuries_agency_id":26},{"zenjuries_company_id":151,"zenjuries_agency_id":26},{"zenjuries_company_id":152,"zenjuries_agency_id":26},{"zenjuries_company_id":153,"zenjuries_agency_id":26},{"zenjuries_company_id":154,"zenjuries_agency_id":26},{"zenjuries_company_id":155,"zenjuries_agency_id":26},{"zenjuries_company_id":156,"zenjuries_agency_id":26},{"zenjuries_company_id":157,"zenjuries_agency_id":26},{"zenjuries_company_id":158,"zenjuries_agency_id":26},{"zenjuries_company_id":159,"zenjuries_agency_id":26},{"zenjuries_company_id":160,"zenjuries_agency_id":26},{"zenjuries_company_id":161,"zenjuries_agency_id":26},{"zenjuries_company_id":162,"zenjuries_agency_id":26},{"zenjuries_company_id":163,"zenjuries_agency_id":32},{"zenjuries_company_id":164,"zenjuries_agency_id":26}]');

       // var_dump($client_array);

        //check for null data that will break the loop
        foreach($client_array as $client){
            if(!is_null($client->zenjuries_agency_id) && !is_null($client->zenjuries_company_id)){
                $client_record = new AgencyClient();
                $client_record->agency_id = $client->zenjuries_agency_id;
                $client_record->company_id = $client->zenjuries_company_id;
                $client_record->save();
            }
        }
    }
}

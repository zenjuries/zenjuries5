<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Models\UserInvite;
use App\Company;
use Illuminate\Support\Facades\Log;
use App\Repositories\InvitationRepository;

class InviteNewUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:invite';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Commandline tool for making new user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $company_id = $this->ask('what is the ID of the user company?');
        $email = $this->ask('what is the email of the user?');
        $name = $this->ask('what is the name of the user?');
        Log::debug("company_id: " . $company_id);
        Log::debug("email: " . $email);
        $company = Company::where('id', $company_id)->first();

        if(is_null($company)){
            $this->error("We couldn't find a company that matches the ID " . $company_id);
            return false;
        }

        if(DB::table('users')->where('email', $email)->exists()){
            $user = User::where('email', $email)->first();
            $user->addUserToCompany($company_id);
            $this->info("The user has been added to the company!");
        }else{
            $user_invite = new InvitationRepository();
            $user_invite->inviteNewUser($name, $email, $company_id);
            $this->info("Your Invite Was Sent!");
        }

        return 0;
    }
}

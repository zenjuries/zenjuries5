<?php

namespace App\Console\Commands;

use App\Repositories\ZenProRepository;
use Illuminate\Console\Command;

class NewZenPro extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'new:zenpro {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ZenProRepository $zenProRepo)
    {
        $this->zenProRepo = $zenProRepo;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = $this->argument('email');
        $this->zenProRepo->inviteNewZenpro($email, "");
        //return 0;
    }
}

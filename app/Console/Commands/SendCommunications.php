<?php

namespace App\Console\Commands;

use App\Repositories\CommunicationRepository;
use Illuminate\Console\Command;
class SendCommunications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * If limit = 0, then all queued messages are sent.
     * If no limit is specified, then all queued messages are sent.
     *
     * @var string
     */
    protected $signature = 'send:communications {limit=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends any queued communications and updates their status in the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CommunicationRepository $repo)
    {
        parent::__construct();
        $this->repo = $repo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $limit = $this->argument('limit');
      $bol = $this->repo->sendQueued($limit);
      //$this->line($bol);
    }


}

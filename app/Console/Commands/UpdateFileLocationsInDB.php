<?php

namespace App\Console\Commands;

use App\InjuryFile;
use App\InjuryImage;
use App\User;
use App\Company;
use Illuminate\Console\Command;

class UpdateFileLocationsInDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'files:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $users = User::get();
        foreach($users as $user){
            $pic_location = $user->picture_location;
            $photo_url = $user->photo_url;
            if($pic_location !== NULL){
                $pic_location = str_replace("/data", "/storage", $pic_location);
                $pic_location = str_replace("/storage/public", "/storage", $pic_location);
            }
            if($photo_url !== NULL){
                $photo_url = str_replace("/data", "/storage", $photo_url);
                $photo_url = str_replace("/storage/public", "/storage", $photo_url);
            }
            $user->picture_location = $pic_location;
            $user->photo_url = $photo_url;
            $user->save();
        }

        $injury_images = InjuryImage::get();
        foreach($injury_images as $injury_image){
            $path = $injury_image->location;
            $path = str_replace("/data", "/storage/injuryImages", $path);
            $path = str_replace("/storage/public", "/storage", $path);
            $path = str_replace("injuryImages/", "", $path);
            $injury_image->location = $path;
            $injury_image->save();
        }

        $companies = Company::get();
        foreach($companies as $company){
            $path = $company->logo_location;
            $path = str_replace("/storage/public", "/storage", $path);
            $company->logo_location = $path;
            $company->save();
        }

    }
}

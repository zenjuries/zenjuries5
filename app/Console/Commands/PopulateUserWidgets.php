<?php

namespace App\Console\Commands;

use App\UserWidget;
use Illuminate\Console\Command;

class PopulateUserWidgets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'widgets:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Loop through the user list to create default widget settings for everyone';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = \App\User::get();

        //default widget IDs

        foreach($users as $user){
            //replace with real widget ID, repeat as needed for all default widgets
            $user_widget = new UserWidget();
            $user_widget->createNewUserWidget($user->id, 'default_widget_id_1');
        }
    }
}

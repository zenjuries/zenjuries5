<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\CompanyInvite;

class InviteNewCompany extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'company:invite';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Commandline tool for making new company ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = $this->ask("what is the recipient email?");
        $company_name = $this->ask("what is the company name?");
        $company_type = "Agency";

        $invite = new CompanyInvite();
        $invite->inviteNewCompany($email, $company_name, $company_type);

        $this->info('Success!');

        return 0;
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SetUserEmailPreferences extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emailPreferences:set';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Loops through the users and makes sure an email setting exists for each template';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return 0;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SquadMember extends Model
{
    //
    public function squad(){
        return $this->belongsTo('App\Squad');
    }

    public function user(){
        return $this->hasOne('App\User');
    }

    public function position(){
        return $this->belongsTo('App\Position');
    }

    public function saveNewSquadMember($squad_id, $position_id, $user_id){
        $this->squad_id = $squad_id;
        $this->position_id = $position_id;
        $this->user_id = $user_id;
        $this->save();
    }

    public function printMemberName(){
        $user = \App\User::where('id', $this->user_id)->first();
        echo $user->name;
    }

    public function printMemberPosition(){
        $position = \App\Position::where('id', $this->position_id)->first();
        if($position->name === "Owner/Manager"){
            echo "Chief Executive";
        }else if($position->name === "Human Resources"){
            echo "Chief Navigator";
        }else if($position->name === "Adjuster/Case Manager"){
            echo "Claim Manager";
        }else if($position->name === "Insurance Agent"){
            echo "Work Comp Agent";
        }else if($position->name === "Insurance Customer Service"){
            echo "Service Agent";
        }else if($position->name === "Safety Coordinator"){
            echo "Safety Manager";
        }else {
            echo $position->name;
        }
    }
}

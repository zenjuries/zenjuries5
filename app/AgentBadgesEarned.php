<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentBadgesEarned extends Model
{
    //
    protected $table = 'agent_badges_earned';
}

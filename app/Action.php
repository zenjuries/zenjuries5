<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    //
    public function action_requests(){
        return $this->hasMany('App\ActionRequest');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZenCarrier extends Model
{
    //
    protected $table = 'zen_carriers';
}

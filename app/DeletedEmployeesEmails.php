<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeletedEmployeesEmails extends Model
{
    //
    protected $table = 'deleted_users_emails';
}

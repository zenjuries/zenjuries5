<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionRequest extends Model
{
    //
    public function action(){
        return $this->belongsTo('App\Action');
    }
}

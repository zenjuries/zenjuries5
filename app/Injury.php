<?php

namespace App;

use App\Repositories\CommunicationRepository;
use App\Repositories\InvitationRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
//use DOMPDF;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use App\Repositories\MessagingRequestsRepository;
use Laravel\Spark\Interactions\Settings\Teams\SendInvitation;
use Psy\Exception\ErrorException;
use DateTime;
use App\TreePost;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use App\SquadMember;
use App\DeviceId;
use App\User;
use App\Services\FCMService;
use App\SummaryPost;

class Injury extends Model
{
    //
    protected $table = 'injuries';

    protected $dates = ['last_reminders_sent_at',
        'values_updated_at',
        'employee_status_reminder_sent_at',
        'final_cost_reminder_sent_at',
        'resolved_at',
        'full_report_delivered_at',
        'claim_close_date',
        ];

    public function getClaimCosts(){
        $cost_array = array();
        $cost_array['paid_medical'] = MedicalCosts::where('injury_id', $this->id)->where('current', 1)->value('cost');
        $cost_array['paid_legal'] = LegalCosts::where('injury_id', $this->id)->where('current', 1)->value('cost');
        $cost_array['paid_indemnity'] = IndemnityCosts::where('injury_id', $this->id)->where('current', 1)->value('cost');
        $cost_array['paid_misc'] = MiscellaneousCosts::where('injury_id', $this->id)->where('current', 1)->value('cost');
        $cost_array['reserve_medical'] = ReserveMedicalCost::where('injury_id', $this->id)->where('current', 1)->value('cost');
        $cost_array['reserve_legal'] = ReserveLegalCost::where('injury_id', $this->id)->where('current', 1)->value('cost');
        $cost_array['reserve_indemnity'] = ReserveIndemnityCost::where('injury_id', $this->id)->where('current', 1)->value('cost');
        $cost_array['reserve_misc'] = ReserveMiscellaneousCost::where('injury_id', $this->id)->where('current', 1)->value('cost');

        return $cost_array;

    }

    public function getPaidTotal(){
        $costs = $this->getClaimCosts();
        $total = 0;
        if(!is_null($costs['paid_medical'])){
            $total+= $costs['paid_medical'];
        }
        if(!is_null($costs['paid_indemnity'])){
            $total+= $costs['paid_indemnity'];
        }
        if(!is_null($costs['paid_legal'])){
            $total+= $costs['paid_legal'];
        }
        if(!is_null($costs['paid_misc'])){
            $total+= $costs['paid_misc'];
        }
        return $total;
    }

    public function getReserveTotal(){
        $costs = $this->getClaimCosts();
        $total = 0;
        if(!is_null($costs['reserve_medical'])){
            $total+= (!is_null($costs['paid_medical'])) ? $costs['reserve_medical']-$costs['paid_medical'] : $total+= $costs['reserve_medical'];
        }
        if(!is_null($costs['reserve_indemnity'])){
            $total+= (!is_null($costs['paid_indemnity'])) ? $costs['reserve_indemnity']-$costs['paid_indemnity'] : $total+= $costs['reserve_indemnity'];
        }
        if(!is_null($costs['reserve_legal'])){
            $total+= (!is_null($costs['paid_legal'])) ? $costs['reserve_legal']-$costs['paid_legal'] : $total+= $costs['reserve_legal'];
        }
        if(!is_null($costs['reserve_misc'])){
            $total+= (!is_null($costs['paid_misc'])) ? $costs['reserve_misc']-$costs['paid_misc'] : $total+= $costs['reserve_misc'];
        }
        return $total;
    }

    public function getIncurredTotal(){
        $total = $this->getReserveTotal()+$this->getPaidTotal();
        return $total;
    }

    public function getTypes(){
        $types = InjuryType::where('injury_id', $this->id)->select('type')->get();
        return $types;
    }

    public function getLocations(){
        $locations = InjuryLocation::where('injury_id', $this->id)->select('location')->get();
        return $locations;
    }

    public function getDiaryPosts($post_type = NULL){
        if($post_type === NULL){
            $posts = DiaryPost::where('injury_id', $this->id)->get();
        }else{
            $posts = DiaryPost::where('injury_id', $this->id)->where('post_type', $post_type)->get();
        }

        return $posts;
    }

    public function getTreePosts($type = NULL){
        $posts = TreePost::where('injury_id', $this->id)->get();

        foreach ($posts as &$post){
            $user =  User::where('id', $post->user_id)->select('name', 'photo_url')->first();
            if($post->type === "diaryPost" || $post->type === "chatPost"){
                $post->profile_image = $user->photo_url;
            }else{
                $post->profile_image = '';
            }
            if(!is_null($user)){
                $post->user_id = $user->name;
            }
            $post->formatted_date = Carbon::createFromFormat("Y-m-d H:i:s", $post->created_at)->toDateTimeString();

            //defualt post title to the type of the post
            $post->title = $post->type;

            //checking the body of system posts for key words to change the post title
            /*
            if($post->type === 'systemPost'){
                //checking for new note
                if(str_contains($post->body, 'Note')) {
                    $post->title = "New injury note";
                }else if(str_contains($post->body, 'note')) {
                    $post->title = "New injury note";
                }elseif(str_contains($post->body, 'appointment')){
                    //checking for deleted appointments
                    if(str_contains($post->body, 'deleted')){
                        $post->title = "Appointment deleted";

                        //checking for new new appointments
                    }elseif(str_contains($post->body, 'new appointment')){
                        $post->title = 'New appointment scheduled';
                    }
                //checking for new injuries
                }elseif(str_contains($post->body, 'suffered')){
                    $post->title = "New injury reported";

                //check for new care locations
                }elseif(str_contains($post->body, 'new Care Location')){
                    $post->title = "New care location";
                }
            }
            else if($post->type === 'employeeStatusPost'){
                if(str_contains($post->body, 'Status')) {
                    $post->title =  "Employee status updated";
                }elseif(str_contains($post->body, 'info')){
                    $post->title = "Employee info updated";
                }
            }
            else if($post->type === 'resolvedPost'){
                $post->title = "Injury was resolved";
            }
            else if($post->type === 'valuePost'){
                $post->title = "Claim costs updated";
            }
            else if($post->type === 'finalCostPost'){
                $post->title = "Final cost submitted";
            }
            else if($post->type === 'reclassifiedPost'){
                if(str_contains($post->body, 'litigation')) {
                    $post->title = "Injury was litigated";
                }elseif(str_contains($post->body, 'Severity')){
                    $post->title = "Injury severity update";
                }
            }
            else if($post->type === 'claimNumberPost'){
                $post->title = "Claim number submitted";
            }
            else if($post->type === 'photoPost'){
                if(str_contains($post->body, 'added')) {
                    $post->title = "Photo uploaded";
                }elseif(str_contains($post->body, 'deleted')){
                    $post->title = "Photo deleted";
                }
            }
            else if($post->type === 'firstReportPost'){
                if(str_contains($post->body, 'Report')) {
                    $post->title = "First report of injury";
                }elseif(str_contains($post->body, 'Alert')){
                    $post->title = "Injury alert delivered";
                }
            }
            else if($post->type === 'reopenedPost'){
                $post->title = "Injury reopened";
            }
            else if($post->type === 'adjusterPost'){
                $post->title = "Adjuster info updated";
            }
            else if($post->type === 'reportTimeGoodPost'){
                $post->title = "Fast claim post";
            }
            else if($post->type === 'reportTimeOkayPost'){
                $post->title = "Medium claim post";
            }
            else if($post->type === 'reportTimeBadPost'){
                $post->title = "Slow claim post";
            }
            else if($post->type === 'filePost'){
                if(str_contains($post->body, 'added')) {
                    $post->title = "File uploaded";
                }elseif(str_contains($post->body, 'deleted')){
                    $post->title = "File deleted";
                }
            }
            else if($post->type === 'chatPost'){
                $post->title = "New chat post";
            }
            else if($post->type === 'newInjuryPost'){
                $post->title = "New injury reported";
            }
            else if($post->type === 'reminderPost'){
                $post->title = "Zenjuries reminder";
            }
            else if($post->type === 'diaryPost'){
                $post->title = "New diary post";
            }
            else if($post->type === 'completeTaskPost'){
                $post->title = "Completed task";
            }
            else if($post->type === 'newTaskPost'){
                $post->title = "New task posted";
            }
            else if($post->type === 'taskReminderPost'){
                $post->title = "Task reminder";
            }
            else if($post->type === 'expiredTaskPost'){
                $post->title = "Expired task";
            }
            */

        }
        return $posts;
    }

    public function getDuration(){
        if($this->resolved){
            $injury_date = $this->getFormattedInjuryDate();
            if($this->claim_close_date === NULL){
                $close_date = $this->resolved_at;
            }else $close_date = $this->claim_close_date;

            return $injury_date->diffInDays($close_date, true);
        }else return false;
    }

    public function getLength($unit = "days"){
        $injury_date = $this->getFormattedInjuryDate();
        if($this->resolved){
            if($this->claim_close_date !== NULL){
                $close_date = $this->claim_close_date;
            }else{
                $close_date = $this->resolved_at;
            }
        }else{
            $close_date = Carbon::now();
        }

        return $injury_date->diffInDays($close_date, true);
    }

    public function getFormattedInjuryDate(){
        return Carbon::createFromFormat("m-d-Y g:i a", $this->injury_date);
    }

    public function checkFirstReport(){
        $first_report = DB::table('first_report_of_injuries')->where('injury_id', $this->id)->first();
        return $first_report->action_taken;
    }

    //there are a set of conditions we can check to determine the claim step.
    // This should be used whenever there is a possibility of the claim step changing
    // When employee status is changed, when first report is delivered, when injury is resolved, and when claim severity is changed this function should be run
    public function setClaimStep(){
        $first_report = DB::table('first_report_of_injuries')->where('injury_id', $this->id)->first();
        if($this->resolved){
            $this->claim_step_id = DB::table('claim_steps')->where('name', 'Claim Resolved')->value('id');
        }else if($first_report->action_taken == 0){
            $this->claim_step_id = DB::table('claim_steps')->where('name', 'Injury Reported')->value('id');
        }else{
            if($this->employee_status == "Maximum Medical Improvement"){
                $this->claim_step_id = DB::table('claim_steps')->where('name', 'Maximum Medical Improvement')->value('id');
            }else if($this->employee_status == "Light Duty" || $this->employee_status == "Full Duty"){
                $this->claim_step_id = DB::table('claim_steps')->where('name', 'Return to Work')->value('id');
            }else if($this->employee_status == "Home"){
                $this->claim_step_id = DB::table('claim_steps')->where('name', 'Home Recovery')->value('id');
            }else if($this->employee_status == NULL || $this->employee_status == "Terminated" || $this->employee_status == "Resigned"){
                $this->claim_step_id = DB::table('claim_steps')->where('name', 'First Report Delivered')->value('id');
            }else{
                $this->claim_step_id = DB::table('claim_steps')->where('name', 'Received Treatment')->value('id');
            }
        }
        $this->save();
    }

    //return whether a claim was resolved in the best practice time, normally, or delayed
    public function returnClaimReportingTime(){
        $late_reporting_date = $this->created_at;
        $late_reporting_date = $late_reporting_date->subDays(2);
        $ideal_reporting_date = $this->created_at;
        $ideal_reporting_date = $ideal_reporting_date->subDay();
        $injury_date_time = Carbon::createFromFormat("m-d-Y g:i a", $this->injury_date);
        if($injury_date_time->gte($late_reporting_date)){
            if($injury_date_time->gte($ideal_reporting_date)){
                return "ideal";
            }else{
                return "standard";
            }
        }else{
            return "late";
        }
    }


    function checkForFinalCosts(){
        if(is_null($this->final_cost) || is_null($this->final_medical_cost) || is_null($this->final_indemnity_cost) ||
        is_null($this->final_legal_cost) ||is_null($this->final_misc_cost))
        {
            return false;
        }else {
            return true;
        }
    }

    public function getReserveDifference(){
        //***** OLD VALUES *****/
        $first_reserve_medical = DB::table('reserve_medical_costs')->where('injury_id', $this->id)->orderBy('created_at', 'asc')->value('cost');
        if(is_null($first_reserve_medical)){$first_reserve_medical = 0;}

        $first_reserve_indemnity = DB::table('reserve_indemnity_costs')->where('injury_id', $this->id)->orderBy('created_at', 'asc')->value('cost');
        if(is_null($first_reserve_indemnity)){$first_reserve_indemnity = 0;}

        $first_reserve_legal = DB::table('reserve_legal_costs')->where('injury_id', $this->id)->orderBy('created_at', 'asc')->value('cost');
        if(is_null($first_reserve_legal)){$first_reserve_legal = 0;}

        $first_reserve_misc = DB::table('reserve_miscellaneous_costs')->where('injury_id', $this->id)->orderBy('created_at', 'asc')->value('cost');
        if(is_null($first_reserve_misc)){$first_reserve_misc = 0;}

        $total_old_reserve = $first_reserve_medical + $first_reserve_indemnity + $first_reserve_legal + $first_reserve_misc;
        Log::debug(var_export($total_old_reserve, true));

        $current_reserve_medical = DB::table('reserve_medical_costs')->where('injury_id', $this->id)->where('current', 1)->value('cost');
        if(is_null($current_reserve_medical)){$current_reserve_medical = 0;}

        $current_reserve_indemnity = DB::table('reserve_indemnity_costs')->where('injury_id', $this->id)->where('current', 1)->value('cost');
        if(is_null($current_reserve_indemnity)){$current_reserve_indemnity = 0;}

        $current_reserve_legal = DB::table('reserve_legal_costs')->where('injury_id', $this->id)->where('current', 1)->value('cost');
        if(is_null($current_reserve_legal)){$current_reserve_legal = 0;}

        $current_reserve_misc = DB::table('reserve_miscellaneous_costs')->where('injury_id', $this->id)->where('current', 1)->value('cost');
        if(is_null($current_reserve_misc)){$current_reserve_misc = 0;}

        $total_current_reserve = $current_reserve_medical + $current_reserve_indemnity + $current_reserve_legal + $current_reserve_misc;
        Log::debug(var_export($total_current_reserve, true));

        $value_array = array();

        $value_array['old_reserve'] = $total_old_reserve;
        $value_array['new_reserve'] = $total_current_reserve;
        $value_array['difference'] = $total_current_reserve - $total_old_reserve;
        return $value_array;
    }

    function handleMildInjuryFirstReport(){
        $company = DB::table('companies')->where('id', $this->company_id)->first();
        $report = \App\FirstReportOfInjury::where('injury_id', $this->id)->first();

        if($report->action_taken !== 1){
            $report->action_taken = 1;
            $report->action_taken_at = Carbon::now();
            $report->save();

            $this->setClaimStep();

            $tree_post = new \App\TreePost();
            $tree_post->injury_id = $this->id;
            $tree_post->user_id = NULL;
            $tree_post->type = "firstReportPost";
            $tree_post->body = "A copy of the Injury Alert can be found in the Attachments section.
            Since this is a First-Aid injury, it should be resolved within 48 hours or upgraded to Moderate.";
            $tree_post->save();

        }
    }


    function deliverFirstReportToZenCarrier(){
        Log::debug('autodeliver');
        $company = DB::table('companies')->where('id', $this->company_id)->first();
        $zen_carrier = DB::table('zen_carriers')->where('id', $company->zen_carrier_id)->first();
        Log::debug(var_export($zen_carrier));
        if(!is_null($zen_carrier) && $zen_carrier->carrier_email !== ""){
            try {
                $injury = $this;
                $pdf = App::make('dompdf.wrapper');
                $data = ['injury_id' => $injury->id];
                $pdf->setPaper('A3')->loadVIEW('render.firstReportRender', ['data' => $data]);
                Mail::send('emailTemplates.autoDeliverFirstReportEmail',
                    ['injury' => $injury, 'pdf' => $pdf, 'company' => $company, 'zen_carrier' => $zen_carrier],
                    function ($m) use ($injury, $pdf, $company, $zen_carrier) {
                        $m->attachData($pdf->output(), 'InjuryAlert' . $company->company_name . '.pdf');
                        $m->to($zen_carrier->carrier_email)->subject('Injury Alert for ' . $company->company_name);
                    });
            }catch (\ErrorException $e){
                Log::error($e);
                return false;
            }catch (\Swift_RfcComplianceException $e){
                Log::error($e);
                return false;
            }

            $report = \App\FirstReportOfInjury::where('injury_id', $this->id)->first();

            if($report->action_taken !== 1){
                $report->action_taken = 1;
                $report->action_taken_at = Carbon::now();
                $report->save();
                //move claim step
                $this->setClaimStep();

                //get the Chief Nav for that team
                $chief_nav = DB::table('squad_members')->where('squad_id', $injury->squad_id)->where('position_id', 9)->
                join('users', 'users.id', '=', 'squad_members.user_id')->select('users.*')->first();

                $treePost = new \App\TreePost();
                $treePost->injury_id = $this->id;
                $treePost->user_id = $chief_nav->id;
                $treePost->type = "firstReportPost";
                $treePost->body = "The delivery of the Injury Alert has been confirmed by " . $chief_nav->name . " at " . Carbon::now() . ".";
                $treePost->save();

            }

            return true;
        }else return false;
    }

    function getInjuryMonths(){
        $start = $this->created_at;

        if(!is_null($this->resolved_at)){
            $end = $this->resolved_at;
        }else{
            $end = Carbon::today()->startOfMonth();
        }

        do{
            $months[$start->format('m-Y')] = $start->format('F Y');
        }while($start->addMonth() <= $end);
        //add the current month on there
        $months[$end->format('m-Y')] = $end->format('F Y');
        return $months;
    }

    function getClaimHistoryByMonth(){
        $months = $this->getInjuryMonths();
        $medical_costs = \App\MedicalCosts::where('injury_id', $this->id)->orderBy('created_at', 'desc')->select('created_at', 'cost')->get();
        $legal_costs = \App\LegalCosts::where('injury_id', $this->id)->orderBy('created_at', 'desc')->select('created_at', 'cost')->get();
        $indemnity_costs = \App\IndemnityCosts::where('injury_id', $this->id)->orderBy('created_at', 'desc')->select('created_at', 'cost')->get();
        $misc_costs = \App\MiscellaneousCosts::where('injury_id', $this->id)->orderBy('created_at', 'desc')->select('created_at', 'cost')->get();
        $reserve_medical_costs = \App\ReserveMedicalCost::where('injury_id', $this->id)->orderBy('created_at', 'desc')->select('created_at', 'cost')->get();
        $reserve_legal_costs = \App\ReserveLegalCost::where('injury_id', $this->id)->orderBy('created_at', 'desc')->select('created_at', 'cost')->get();
        $reserve_indemnity_costs = \App\ReserveIndemnityCost::where('injury_id', $this->id)->orderBy('created_at', 'desc')->select('created_at', 'cost')->get();
        $reserve_misc_costs = \App\ReserveMiscellaneousCost::where('injury_id', $this->id)->orderBy('created_at', 'desc')->select('created_at', 'cost')->get();

        $history_array = array();
        $medical_cost_array = array();
        $legal_cost_array = array();
        $indemnity_cost_array = array();
        $misc_cost_array = array();
        $reserve_medical_cost_array = array();
        $reserve_indemnity_cost_array = array();
        $reserve_legal_cost_array = array();
        $reserve_misc_cost_array = array();
        $final_medical_cost_array = array();
        $final_indemnity_cost_array = array();
        $final_legal_cost_array = array();
        $final_misc_cost_array = array();

        foreach($months as $month){
            $carbon_month = Carbon::createFromFormat("F Y j", $month . " 1")->endOfMonth();
            //Log::debug(var_export($carbon_month, true));

            //*** MEDICAL ***//
            if(count($medical_costs)){
                foreach($medical_costs as $medical_cost){
                    if($medical_cost->created_at->lte($carbon_month)){
                        $medical_cost_array[$month] = $medical_cost->cost;
                        break;
                    }
                }
                if(empty($medical_cost_array[$month])){
                    $medical_cost_array[$month] = 0;
                }
            }else{
                $medical_cost_array[$month] = 0;
            }

            //*** LEGAL ***//
            if(count($legal_costs)){
                foreach($legal_costs as $legal_cost){
                    //Log::debug(var_export($legal_cost->created_at, true));
                    //Log::debug(var_export($carbon_month, true));
                    if($legal_cost->created_at->lte($carbon_month)){
                        $legal_cost_array[$month] = $legal_cost->cost;
                        break;
                    }
                }
                if(empty($legal_cost_array[$month])){
                    $legal_cost_array[$month] = 0;
                }
            }else{
                $legal_cost_array[$month] = 0;
            }

            //*** INDEMNITY ***//
            if(count($indemnity_costs)){
                foreach($indemnity_costs as $indemnity_cost){
                    if($indemnity_cost->created_at->lte($carbon_month)){
                        $indemnity_cost_array[$month] = $indemnity_cost->cost;
                        break;
                    }
                }
                if(empty($indemnity_cost_array[$month])){
                    $indemnity_cost_array[$month] = 0;
                }
            }else{
                $indemnity_cost_array[$month] = 0;
            }

            //*** MISC ***//
            if(count($misc_costs)){
                foreach($misc_costs as $misc_cost){
                    if($misc_cost->created_at->lte($carbon_month)){
                        $misc_cost_array[$month] = $misc_cost->cost;
                        break;
                    }
                }
                if(empty($misc_cost_array[$month])){
                    $misc_cost_array[$month] = 0;
                }
            }else{
                $misc_cost_array[$month] = 0;
            }

            //*** RESERVE MEDICAL ***//
            if(count($reserve_medical_costs)){
                foreach($reserve_medical_costs as $reserve_medical_cost){
                    if($reserve_medical_cost->created_at->lte($carbon_month)){
                        $reserve_medical_cost_array[$month] = $reserve_medical_cost->cost;
                        break;
                    }
                }
                if(empty($reserve_medical_cost_array[$month])){
                    $reserve_medical_cost_array[$month] = 0;
                }
            }else{
                $reserve_misc_cost_array[$month] = 0;
            }

            //*** RESERVE LEGAL ***//
            if(count($reserve_legal_costs)){
                foreach($reserve_legal_costs as $reserve_legal_cost){
                    if($reserve_legal_cost->created_at->lte($carbon_month)){
                        //Log::debug(var_export("if was true: " . $reserve_legal_cost->cost, true));
                        $reserve_legal_cost_array[$month] = $reserve_legal_cost->cost;
                        //Log::debug(var_export("set array value to : " . $reserve_legal_cost_array[$month], true));
                        break;
                    }
                }
                if(empty($reserve_legal_cost_array[$month])){
                    //Log::debug('array was empty');
                    $reserve_legal_cost_array[$month] = 0;
                }
            }else{
                //Log::debug('reserve_legal_cost empty');
                $reserve_legal_cost_array[$month] = 0;
            }

            //*** RESERVE INDEMNITY ***//
            if(count($reserve_indemnity_costs)){
                foreach($reserve_indemnity_costs as $reserve_indemnity_cost){
                    if($reserve_indemnity_cost->created_at->lte($carbon_month)){
                        $reserve_indemnity_cost_array[$month] = $reserve_indemnity_cost->cost;
                        break;
                    }
                }
                if(empty($reserve_indemnity_cost_array[$month])){
                    $reserve_indemnity_cost_array[$month] = 0;
                }
            }else{
                $reserve_indemnity_cost_array[$month] = 0;
            }

            //*** RESERVE MISC ***//
            if(count($reserve_misc_costs)){
                foreach($reserve_misc_costs as $reserve_misc_cost){
                    if($reserve_misc_cost->created_at->lte($carbon_month)){
                        $reserve_misc_cost_array[$month] = 0;
                        break;
                    }
                }
                if(empty($reserve_misc_cost_array[$month])){
                    $reserve_misc_cost_array[$month] = 0;
                }
            }else{
                $reserve_misc_cost_array[$month] = 0;
            }

            $final_medical_cost_array[$month] = null;
            $final_indemnity_cost_array[$month] = null;
            $final_legal_cost_array[$month] = null;
            $final_misc_cost_array[$month] = null;

        }

        if($this->resolved === 1 && !is_null($this->resolved_at)){
            $history_array['month_resolved'] = $this->resolved_at->format('F Y');
        }else{
            $history_array['month_resolved'] = false;
        }
        $history_array['final_costs'] = false;

        if($this->resolved === 1 && !is_null($this->resolved_at)
        && !is_null($this->final_medical_cost) && !is_null($this->final_legal_cost)
        && !is_null($this->final_indemnity_cost) && !is_null($this->final_misc_cost)){
            $history_array['final_costs'] = true;
            foreach($months as $month) {
                $carbon_month = Carbon::createFromFormat("F Y j", $month . " 1")->endOfMonth();
                //*** FINAL COSTS ***
                if($this->resolved_at->lte($carbon_month)){
                    if($history_array['month_resolved'] === $month){
                        $medical_cost_array[$month] = $this->final_medical_cost;
                        $legal_cost_array[$month] = $this->final_legal_cost;
                        $indemnity_cost_array[$month] = $this->final_indemnity_cost;
                        $misc_cost_array[$month] = $this->final_misc_cost;
                    }else{
                        $medical_cost_array[$month] = NULL;
                        $legal_cost_array[$month] = NULL;
                        $indemnity_cost_array[$month] = NULL;
                        $misc_cost_array[$month] = NULL;
                    }

                    $reserve_medical_cost_array[$month] = 0;
                    $reserve_legal_cost_array[$month] = 0;
                    $reserve_indemnity_cost_array[$month] = 0;
                    $reserve_misc_cost_array[$month] = 0;

                    $final_medical_cost_array[$month] = $this->final_medical_cost;
                    $final_legal_cost_array[$month] = $this->final_legal_cost;
                    $final_indemnity_cost_array[$month] = $this->final_indemnity_cost;
                    $final_misc_cost_array[$month] = $this->final_misc_cost;

                }
            }
        }

        $history_array['medical'] = $medical_cost_array;
        $history_array['legal'] = $legal_cost_array;
        $history_array['indemnity'] = $indemnity_cost_array;
        $history_array['misc'] = $misc_cost_array;
        $history_array['reserve_medical'] = $reserve_medical_cost_array;
        $history_array['reserve_legal'] = $reserve_legal_cost_array;
        $history_array['reserve_indemnity'] = $reserve_indemnity_cost_array;
        $history_array['reserve_misc'] = $reserve_misc_cost_array;

        $history_array['final_medical'] = $final_medical_cost_array;
        $history_array['final_legal'] = $final_legal_cost_array;
        $history_array['final_indemnity'] = $final_indemnity_cost_array;
        $history_array['final_misc'] = $final_misc_cost_array;

        return $history_array;
    }

    function updateDescription($notes, $user){
        $this->notes = $notes;
        $this->save();
        $post = new TreePost();
        $post->injury_id = $this->id;
        $post->user_id = $user->id;
        $post->body = $user->name . " updated the injury note to \"" . $notes . "\"";
        $post->type = "systemPost";
        $post->save();
        $summary = new SummaryPost();
        $summary->createSummary($this->id, $user->id, $user->name . " updated the injury note", "systemPost");

            /*** push notification ***/
            $squad_members = \App\SquadMember::where('squad_id', $this->squad_id)->get();
            $injured_employee = \App\User::where('id', $this->injured_employee_id)->first();
            $sent_notification_array = [];
            foreach($squad_members as $squad_member){
                if(!in_array($squad_member->user_id, $sent_notification_array)){
                    $recipient_user = \App\User::where('id', $squad_member->user_id)->first();
                    if($recipient_user->notification_priority_3 === 1){
                        Log::debug('user ' . $user->name . ' has notification 3 set to ' . $user->notification_priority_3);
                        $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                        if($devices != NULL || $devices != ""){
                            $devices->sendNotification('Injury Details Updated', $user->name . " updated the injury note to \"" . $notes . "\"");
                        }
                    }
                    array_push($sent_notification_array, $squad_member->user_id);
                }
            }
            /***********************************************************/
    }

    //$status = the new status,$date = the effective date of the new status,$user = the user doing the updating
    function updateEmployeeStatus($status, $note = "", $date, $user){
        if($this->employee_status !== $status || $this->status_changed_effective_date !== $date){
            $old_status = $this->employee_status;
            $summary = new SummaryPost();
            $summary->createSummary($this->id, $user->id, "status has changed from " . $old_status . " to " . $status . ".", "employeeStatusPost");

            $this->employee_status = $status;
            $this->status_changed_effective_date = $date;
            $this->save();

            $status_history = new \App\EmployeeStatusHistory();
            $status_history->injury_id = $this->id;
            $status_history->status = $status;
            $status_history->status_changed_date = $date;
            $status_history->save();

            $post = new \App\TreePost();
            $post->injury_id = $this->id;
            $post->user_id = $user->id;
            if($note != ""){
                $post->body = $user->name . ' changed the Employee Status to "' . $status . '". '. $note . ' ' . $date . ".";
            }else{
                $post->body = $user->name . ' changed the Employee Status to "' . $status . '". The effective date of this status change is ' . $date . ".";
            }
            $post->type = 'employeeStatusPost';
            $post->save();

            if($this->employee_status !== $status){
                $comm_repo = new CommunicationRepository();
                $comm_repo->employeeStatusUpdated($this->id);
            }
            $this->setClaimStep();

            /*** push notification ***/
            $squad_members = \App\SquadMember::where('squad_id', $this->squad_id)->get();
            $injured_employee = \App\User::where('id', $this->injured_employee_id)->first();
            $sent_notification_array = [];
            foreach($squad_members as $squad_member){
                if(!in_array($squad_member->user_id, $sent_notification_array)){
                    $recipient_user = \App\User::where('id', $squad_member->user_id)->first();
                    if($recipient_user->notification_priority_1 === 1){
                        $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                        if($devices != NULL || $devices != ""){
                            $devices->sendNotification('Employee Status Updated', $user->name . " updated the status of " . $injured_employee->name . " to " . $status . ".");
                        }
                    }
                    array_push($sent_notification_array, $squad_member->user_id);
                }
            }
            /***********************************************************/
        }
        return true;
    }

    //$user = the user resolving the injury
    function resolveInjury($user, $date = null){
        if($this->resolved !== 1){
            $injured_employee = \App\User::where('id', $this->injured_employee_id)->first();
            $this->resolved = 1;
            if($date != null){
                $this->resolved_at = $date;
            }
            $this->save();
            $this->setClaimStep();

            $comm_repo = new CommunicationRepository();
            $comm_repo->claimResolved($this->id);

            $post = new TreePost();
            $post->injury_id = $this->id;
            $post->body = $user->name . " resolved this injury on " . Carbon::now() . ".";
            $post->type = "resolvedPost";
            $post->user_id = $user->id;
            $post->save();
            $summary = new SummaryPost();
            $summary->createSummary($this->id, $user->id, $user->name . " resolved this injury", "resolvedPost");

            $employee_injuries = \App\Injury::where('injured_employee_id', $injured_employee->id)->where('resolved', 0)->get();
            if ($employee_injuries->isEmpty()) {
                $injured_employee->injured = 0;
                $injured_employee->save();
            }

            $comm_repo->getFinalCost($this->id);
            $comm_repo->getClaimCloseDate($this->id);

            $this->final_cost_reminders++;
            $this->final_cost_reminder_sent_at = Carbon::now();
            $this->save();

            /*** push notification ***/
            $squad_members = \App\SquadMember::where('squad_id', $this->squad_id)->get();
            $injured_employee = \App\User::where('id', $this->injured_employee_id)->first();
            $sent_notification_array = [];
            foreach($squad_members as $squad_member){
                if(!in_array($squad_member->user_id, $sent_notification_array)){
                    $recipient_user = \App\User::where('id', $squad_member->user_id)->first();
                    if($recipient_user->notification_priority_1 === 1){
                        $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                        if($devices != NULL || $devices != ""){
                            $devices->sendNotification('Injury Resolved', $user->name . " resolved this injury.");
                        }
                    }
                    array_push($sent_notification_array, $squad_member->user_id);
                }
            }
            /***********************************************************/
        }
        else if($this->resolved === 1 && $this->resolved_at === null){
            $this->resolved_at = $date;
            $this->save();

               /*** push notification ***/
               $squad_members = \App\SquadMember::where('squad_id', $this->squad_id)->get();
               $injured_employee = \App\User::where('id', $this->injured_employee_id)->first();
               $sent_notification_array = [];
               foreach($squad_members as $squad_member){
                   if(!in_array($squad_member->user_id, $sent_notification_array)){
                        $recipient_user = \App\User::where('id', $squad_member->user_id)->first();
                        if($recipient_user->notification_priority_1 === 1){
                            $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                            if($devices != NULL || $devices != ""){
                                $devices->sendNotification('Injury Resolved', $user->name . " resolved this injury on " . Carbon::now() . ".");
                        }
                    }
                       array_push($sent_notification_array, $squad_member->user_id);
                   }
               }
               /***********************************************************/
        }
    }

    //$user = user reopening injury
    function reopenInjury($user){
        $injured_employee = \App\User::where('id', $this->injured_employee_id)->first();
        $this->resolved = 0;
        $this->reopened_at = Carbon::now();
        $this->final_cost = NULL;
        $this->claim_close_date = NULL;
        $this->claim_close_date_reminder_count = 0;
        $this->save();
        //make claim reopened communication (maybe 2.1)
        //$this->repo->claimResolved($injury_id, $injury->squad_id);
        $treePost = new TreePost();
        $treePost->injury_id = $this->id;
        $treePost->body = $user->name . " reopened this injury on " . Carbon::now() . ". It's claim costs have been reset.";
        $treePost->type = "reopenedPost";
        $treePost->user_id = $user->id;
        $treePost->save();
        $summary = new SummaryPost();
        $summary->createSummary($this->id, $user->id, $user->name . " reopened this injury", "reopenedPost");

        $current_medical = \App\MedicalCosts::where('injury_id', $this->id)->where('current', 1)->update(['current' => 0]);
        $current_reserve = \App\ReserveCosts::where('injury_id', $this->id)->where('current', 1)->update(['current' => 0]);
        $current_indemnity = \App\IndemnityCosts::where('injury_id', $this->id)->where('current', 1)->update(['current' => 0]);
        $current_legal = \App\LegalCosts::where('injury_id', $this->id)->where('current', 1)->update(['current' => 0]);
        $current_misc = \App\MiscellaneousCosts::where('injury_id', $this->id)->where('current', 1)->update(['current' => 0]);

        $claim_values_array = [
            0 => new MedicalCosts(),
            1 => new IndemnityCosts(),
            2 => new ReserveCosts(),
            3 => new LegalCosts(),
            4 => new MiscellaneousCosts()
        ];

        foreach($claim_values_array as $claim_value){
            $claim_value->injury_id = $this->id;
            $claim_value->updater_id = $user->id;
            $claim_value->cost = 0;
            $claim_value->current = 1;
            $claim_value->save();
        }

        $userInjuries = \App\Injury::where('injured_employee_id', $user->id)->where('resolved', 0)->get();
        $injured_employee->injured = 1;
        $injured_employee->save();
    }

    function pauseClaimForLitigation($user){
        $this->paused_for_litigation = 1;
        $this->litigation_paused_date = Carbon::now();
        $this->save();

        $tree_post = new \App\TreePost();
        $tree_post->injury_id = $this->id;
        $tree_post->user_id = $user->id;
        $tree_post->body = $user->name . " marked this claim as being under litigation on " . Carbon::today()->toDateString() . ".";
        $tree_post->type = 'reclassifiedPost';
        $tree_post->save();

        $comm_repo = new CommunicationRepository();
        $comm_repo->claimPaused($this->id);
    }

    //$cost = the new cost, $type = the type of cost being updated, $user = the user updating the cost
    function updateClaimCost($cost, $type, $user){
        $costs_are_equal = false;
        if($type == "Paid Medical"){
            $old_values = \App\MedicalCosts::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\MedicalCosts();
        }else if($type == "Paid Indemnity"){
            $old_values = \App\IndemnityCosts::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\IndemnityCosts();
        }else if($type == "Paid Legal"){
            $old_values = \App\LegalCosts::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\LegalCosts();
        }else if($type == "Paid Miscellaneous"){
            $old_values = \App\MiscellaneousCosts::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\MiscellaneousCosts();
        }else if($type == "Reserve Medical"){
            $old_values = \App\ReserveMedicalCost::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\ReserveMedicalCost();
        }else if($type == "Reserve Indemnity"){
            $old_values = \App\ReserveIndemnityCost::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\ReserveIndemnityCost();
        }else if($type == "Reserve Legal"){
            $old_values = \App\ReserveLegalCost::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\ReserveLegalCost();
        }else if($type == "Reserve Miscellaneous"){
            $old_values = \App\ReserveMiscellaneousCost::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\ReserveMiscellaneousCost();
        }else return false;
        if(count($old_values)){
            if($old_values->first()->cost === $cost){
                $costs_are_equal = true;
            }
        }

        foreach($old_values as $old_value){
            $old_value->current = 0;
            $old_value->save();
        }

        $claim_cost->injury_id = $this->id;
        $claim_cost->updater_id = $user->id;
        $claim_cost->cost = $cost;
        $claim_cost->current = 1;
        $claim_cost->save();

        $post = new \App\TreePost();
        $post->injury_id = $this->id;
        $post->user_id = $user->id;
        //Check what the tree post should say
        if($costs_are_equal === false){
            $post->body = $user->name . " updated the " . (($type == "misc")? "Miscellaneous" : ucfirst($type)) . " Cost for this claim to $" . $cost . " on " . $claim_cost->created_at . ".";
        }else{
            $post->body = $user->name . " confirmed that the " . (($type == "misc")? "Miscellaneous" : ucfirst($type)) . " Cost for this claim is up to date on " . $claim_cost->created_at . ".";
        }
        $post->type = 'valuePost';
        $post->task_type = 'claim';
        $post->save();

        /*** push notification ***/
        $squad_members = \App\SquadMember::where('squad_id', $this->squad_id)->get();
        $injured_employee = \App\User::where('id', $this->injured_employee_id)->first();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                $recipient_user = \App\User::where('id', $squad_member->user_id)->first();
                if($recipient_user->notification_priority_1 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('Claim Costs Updated', $user->name . " updated the claim costs for " . $injured_employee->name . "'s claim.");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/

        $this->values_updated_at = Carbon::now();
        $this->reminders_since_last_update = 0;
        $this->last_reminders_sent_at = NULL;
        $this->save();
    }

    function updateFinalCost($final_cost, $medical_cost, $indemnity_cost, $legal_cost, $misc_cost, $user){
        $this->final_cost = $final_cost;
        $this->final_medical_cost = $medical_cost;
        $this->final_indemnity_cost = $indemnity_cost;
        $this->final_legal_cost = $legal_cost;
        $this->final_misc_cost = $misc_cost;
        $this->save();

        $post = new \App\TreePost();
        $post->injury_id = $this->id;
        $post->user_id = $user->id;
        $post->body = $user->name . " updated the Final Cost of this claim to $" . $final_cost . " on " . Carbon::now()->toDateString() . ".";
        $post->type = 'finalCostPost';
        $post->task_type = 'finalcost';
        $post->save();

        $final_claim_request_id = \App\InformationType::where('name', 'Final Cost')->value('id');

        $comm_repo = new CommunicationRepository();
        $comm_repo->finalCostUpdated($this->id);

            /*** push notification ***/
            $squad_members = \App\SquadMember::where('squad_id', $this->squad_id)->get();
            $injured_employee = \App\User::where('id', $this->injured_employee_id)->first();
            $sent_notification_array = [];
            foreach($squad_members as $squad_member){
                if(!in_array($squad_member->user_id, $sent_notification_array)){
                    $recipient_user = \App\User::where('id', $squad_member->user_id)->first();
                    if($recipient_user->notification_priority_1 === 1){
                        $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                        if($devices != NULL || $devices != ""){
                            $devices->sendNotification('Final Cost Updated', $user->name . " updated the Final Cost of this claim to $" . $final_cost . ".");
                        }
                    }
                    array_push($sent_notification_array, $squad_member->user_id);
                }
            }
            /***********************************************************/
    }

    //$severity = the injury's new severity, $user = the user updating the severity
    function updateSeverity($severity, $note = "", $user){
        $firstReport = \App\FirstReportOfInjury::where('injury_id', $this->id)->first();
        $comm_repo = new CommunicationRepository();
        $company = \App\Company::where('id', $this->company_id)->first();
        $old_type = $this->type;
        $this->severity = $severity;
        if($severity == "Mild"){
            $this->type = "First Aid";
        }else{
            $this->type = "Work Comp";
        }
        $this->save();
        if (is_null($firstReport)) {
            $firstReport = new FirstReportOfInjury();
            $firstReport->injury_id = $this->id;
            $firstReport->squad_id = $this->squad_id;
        }
        if($this->report_processed != 1 && $severity !== "Mild") {
            /*
            $firstReport->action_taken = 0;
            $firstReport->number_of_reminders_sent = 0;
            $firstReport->last_message_sent_at = NULL;
            $firstReport->save();
            $this->report_processed = 1;
            $this->save();

            //if the company has a zen carrier and the injury isn't mild, auto-deliver the first report
            $renewal_date = $company->renewal_date;
            if(!is_null($renewal_date)){
                $renewal_date = \Carbon\Carbon::createFromFormat("Y-m-d", $renewal_date);
            }
            if($this->severity === "Mild"){
                //$this->handleMildInjuryFirstReport();
            }else if(!is_null($company->zen_carrier_id) && $this->severity !== "Mild" && !is_null($renewal_date) && $renewal_date->gt(Carbon::now())){
                $delivery = $this->deliverFirstReportToZenCarrier();
                if($delivery === false){
                    $comm_repo->firstReportOfInjury($firstReport->id);
                }
            }else{
                $comm_repo->firstReportOfInjury($firstReport->id);
            }
            //$comm_repo->firstReportOfInjury($firstReport->id);
            */
        }
        //update claim step, since having to redeliver the first report should reset the claim steps
        $this->setClaimStep();

        $post = new \App\TreePost();
        $post->injury_id = $this->id;
        $post->user_id = $user->id;
        $post->type = "reclassifiedPost";
        if($note != ""){
            $post->body = $user->name . " updated this injury's Severity to " . $severity . ". " . $note ." on " . Carbon::now() . ".";
        }else{
            $post->body = $user->name . " updated this injury's Severity to " . $severity . " on " . Carbon::now() . ".";
        }
        $post->save();

        $comm_repo->severityUpdated($this->id);

        /*** push notification ***/
        $squad_members = \App\SquadMember::where('squad_id', $this->squad_id)->get();
        $injured_employee = \App\User::where('id', $this->injured_employee_id)->first();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                $recipient_user = \App\User::where('id', $squad_member->user_id)->first();
                if($recipient_user->notification_priority_1 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('Injury Details Updated', $user->name . " updated this injury's Severity to " . $severity . ".");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/
    }

    function updateInjuredEmployeeEmail($new_email){
        $injured_employee = \App\User::where('id', $this->injured_employee_id)->first();
        $old_user = \App\User::where('email', $new_email)->first();
        $comm_repo = new CommunicationRepository();
        if (!is_null($old_user)) {
            if ($old_user->type == 'internal') {
                if ($old_user->company_id == $this->company_id) {
                    $this->injured_employee_id = $old_user->id;
                    $this->save();
                    $old_user->injured =1;
                    $old_user->save();
                    $comm_repo->employeeExpectationsDelivery($this->id);
                    return 'true';
                } else {
                    return Response::json("Sorry, that email is taken by someone else who is not a part of this company.", 422);
                }
            } else {
                $old_user_companies = \App\ExternalUserCompanies::where('user_id', $old_user->id)->get();
                $check = false;
                foreach ($old_user_companies as $old_user_company) {
                    if ($old_user_company->company_id == $this->company_id) {
                        $check = true;
                    }
                }
                if ($check == false) {
                    return Response::json("Sorry, that email is taken by someone else who is not a part of this company.", 422);
                } else {
                    $this->injured_employee_id = $old_user->id;
                    $this->save();
                    $old_user->injured =1;
                    $old_user->save();
                    $comm_repo->employeeExpectationsDelivery($this->id);
                    return 'true';
                }
            }
        }

        $team = \App\Company::where('id', $this->company_id)->value('team_id');
        $team = \App\Team::where('id', $team)->first();
        $invitation_repo = new InvitationRepository();
        $invitation_repo->inviteNewUser($injured_employee->name, $new_email, $this->company_id, 0, "internal", 1, $this->id);
        /*** push notification ***/
        $squad_members = \App\SquadMember::where('squad_id', $this->squad_id)->get();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                $user = \App\User::where('id', $squad_member->user_id)->first();
                if($user->notification_priority_3 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('Zengarden Invite Sent', $injured_employee->name . " was invited to the Zengarden.");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/
        $injured_employee->email = $new_email;
        $injured_employee->save();
        $comm_repo->employeeExpectationsDelivery($this->id);
        return 'true';
    }

    function updateInjuredEmployeePhone($phone){
        $user = \App\User::where('id', $this->injured_employee_id)->first();
        if($user->phone === NULL){
            $user->phone = $phone;
            $user->save();
        }else{
            return Response::json("Sorry, the injured employee for this injury already has a phone number on record", 422);
        }
    }

    //$user is the user confirming the delivery
    function confirmFirstReportDelivery($user){
        $report = \App\FirstReportOfInjury::where('injury_id', $this->id)->first();
		Log::debug("confirm first report delivery");
        if($report->action_taken !== 1) {
            $report->action_taken = 1;
            $report->action_taken_at = Carbon::now();
            $report->save();
            //move claim step
            //$firstReportClaimStep = \App\ClaimStep::where('name', 'First Report Delivered')->first();
            $this->setClaimStep();

            $treePost = new \App\TreePost();
            $treePost->injury_id = $this->id;
            $treePost->user_id = $user->id;
            $treePost->type = "firstReportPost";
            $treePost->body = "The delivery of the Injury Alert has been confirmed by " . $user->name . " at " . Carbon::now() . ".";
            $treePost->save();

             /*** push notification ***/
            $squad_members = \App\SquadMember::where('squad_id', $this->squad_id)->get();
            $injured_employee = \App\User::where('id', $this->injured_employee_id)->first();
            $sent_notification_array = [];
            foreach($squad_members as $squad_member){
                if(!in_array($squad_member->user_id, $sent_notification_array)){
                    $user = \App\User::where('id', $squad_member->user_id)->first();
                    if($user->notification_priority_2 === 1){
                        $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                        if($devices != NULL || $devices != ""){
                            $devices->sendNotification('Injury Alert Confirmed', "The delivery of the Injury Alert has been confirmed by " . $user->name . " at " . Carbon::now() . ".");
                        }
                    }
                    array_push($sent_notification_array, $squad_member->user_id);
                }
            }
            /***********************************************************/

        }else{
	        Log::debug('failed');
            return Response::json('Sorry, that injury has already had its Injury Alert delivered. Thank you!', 422);
        }
    }

    function confirmOfficialFirstReportDelivered($user){
        $this->full_injury_report_delivered = 1;
        $this->full_report_delivered_at = Carbon::now();
        $this->save();
        $tree_post = new \App\TreePost();
        $tree_post->injury_id = $this->id;
        $tree_post->user_id = $user->id;
        $tree_post->body = $user->name . " confirmed delivery of the Official First Report of Injury on " . Carbon::today()->format("M d, Y") . ".";
        $tree_post->type = "firstReportPost";
        $tree_post->save();


        /*** push notification ***/
        $squad_members = \App\SquadMember::where('squad_id', $this->squad_id)->get();
        $injured_employee = \App\User::where('id', $this->injured_employee_id)->first();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                $user = \App\User::where('id', $squad_member->user_id)->first();
                if($user->notification_priority_2 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('First Report Confirmed', $user->name . " confirmed delivery of the Official First Report of Injury on " . Carbon::today()->format("M d, Y") . ".");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/

        $this->confirmFirstReportDelivery($user);
    }


    function updateClaimNumber($claim_number, $user){
        $this->claim_number = $claim_number;
        $this->save();

        $comm_repo = new CommunicationRepository();
        $comm_repo->claimNumber($this->id);

        $post = new TreePost();
        $post->injury_id = $this->id;
        $post->user_id = $user->id;
        $post->type = "claimNumberPost";
        $post->body = "The claim number for this injury is " . $claim_number . ".";
        $post->save();
        $summary = new SummaryPost();
        $summary->createSummary($this->id, $user->id, "The claim number as been updated", "claimNumberPost");

             /*** push notification ***/
             $squad_members = \App\SquadMember::where('squad_id', $this->squad_id)->get();
             $injured_employee = \App\User::where('id', $this->injured_employee_id)->first();
             $sent_notification_array = [];
             foreach($squad_members as $squad_member){
                 if(!in_array($squad_member->user_id, $sent_notification_array)){
                    $user = \App\User::where('id', $squad_member->user_id)->first();
                    if($user->notification_priority_2 === 1){
                        $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                        if($devices != NULL || $devices != ""){
                            $devices->sendNotification('Injury Details Updated', "The claim number for this injury is " . $claim_number . ".");
                        }
                    }
                     array_push($sent_notification_array, $squad_member->user_id);
                 }
             }
             /***********************************************************/


        //if the first report of injury hasn't been delivered
        $this->confirmFirstReportDelivery($user);
    }

    //$name = name of adjuster, $email = email of adjuster, $phone = phone of adjuster, $user = user entering info
    function updateAdjusterInfo($name, $email, $phone, $user){

        if(!empty($name)){$this->adjuster_name = $name;}
        if(!empty($email)){$this->adjuster_email = $email;}
        if(!empty($phone)){$this->adjuster_phone = $phone;}
        //$injury->adjuster_reminder_sent = 1;
        $this->save();

        $tree_post = new TreePost();
        $tree_post->injury_id = $this->id;
        $tree_post->user_id = $user->id;
        $tree_post->body = $user->name . " updated the adjuster info.";
        $tree_post->type = "adjusterPost";
        $tree_post->task_type = 'adjuster';
        $tree_post->save();

        $summary = new SummaryPost();
        $summary->createSummary($this->id, $user->id, $user->name . " updated the adjuster info.", "adjusterPost");

        $comm_repo = new CommunicationRepository();
        $comm_repo->adjusterInfoAdded($this->id);

        /*********** push notification ********************************/
            $squad_members = \App\SquadMember::where('squad_id', $this->squad_id)->get();
            $injured_employee = \App\User::where('id', $this->injured_employee_id)->first();
            $sent_notification_array = [];
            foreach($squad_members as $squad_member){
                if(!in_array($squad_member->user_id, $sent_notification_array)){
                    $user = \App\User::where('id', $squad_member->user_id)->first();
                    if($user->notification_priority_2 === 1){
                        $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                        if($devices != NULL || $devices != ""){
                            $devices->sendNotification('Adjuster Info Updated', "adjuster info updated on " . $injured_employee->name . "'s claim.");
                        }
                    }
                    array_push($sent_notification_array, $squad_member->user_id);
                }
            }
        /*************************************************************/



    }

    //$request_name = name of request, $request_description = description/body of request,
    // $recipient_id_array = array of users to be assigned to request, $due_date = optional date the request is due by
    // $user = user creating the request
    /*
    function createCustomRequest($request_name, $request_description, $recipient_id_array, $due_date, $user){
        if($due_date === ""){
            $due_date = NULL;
        }else{
            try{
                $due_date = Carbon::createFromFormat("m-d-Y", $due_date);
            }catch (\InvalidArgumentException $e) {
                return Response::json("Sorry, that date is not valid. Status Dates should be in this format: 01-31-2017", 422);
            }

            $today = Carbon::today();
            if($due_date->lt($today)){
                return Response::json("Sorry, you can't choose a past date.", 422);
            }else{
                $due_date = $due_date->toDateString();
            }

        }
        //Create a new Action with the details of the task
        $action = new \App\Action();
        $action->name = $request_name;
        $action->description = "Custom";
        $action->request_body = $request_description;
        if($due_date !== NULL){
            $action->due_on = $due_date;
        }
        $action->custom = 1;
        $action->save();

        $comm_repo = new CommunicationRepository();

        $comm_repo->newCustomTask($recipient_id_array, $action->id, $this->id, $user->id, $due_date);

        $recipient_names = array();

        foreach($recipient_id_array as $recipient){
            $recipient_name = \App\User::where('id', $recipient)->value('name');
            array_push($recipient_names, $recipient_name);
        }
        if(count($recipient_names) === 1){
            $recipient_string = $recipient_names[0];
        }else {
            $recipient_string = "";

            //***get the last key ***

            //move the array pointer to the last element
            end($recipient_names);
            //return the index of the current array position
            $last_key = key($recipient_names);
            //reset the array pointer
            reset($recipient_names);

            foreach ($recipient_names as $key => $recipient) {
                if($key === $last_key){
                    $recipient_string .= "and " . $recipient;
                }else {
                    if(count($recipient_id_array) !== 2){
                        $recipient_string .= $recipient . ", ";
                    }else{
                        $recipient_string .= $recipient . " ";
                    }
                }
            }
        }
        $due_date_string = "";
        if(!is_null($due_date)){
            $due_date_string = "This request is due on " . $due_date;
        }

        $post = new TreePost();
        $post->injury_id = $this->id;
        $post->user_id = $user->id;
        $post->body = $user->name . " created the request \"$request_name\" and assigned $recipient_string to it. $due_date_string";
        $post->type = 'newTaskPost';
        $post->save();
    }
*/
    //$img_data = base_64 encoded data for image, $name = name of photo, $description = description of photo, $user = user uploading photo
    function uploadPhoto($img_data, $name, $description, $user){
        if($name === "" || $name === NULL){
            $name = "Injury Image";
        }

        if($description === ""){
            $description = NULL;
        }

        $img_count = \App\InjuryImage::where('injury_id', $this->id)->get();

        $image_name = '/storage/public/injuryUploads/injury_' . $this->id . '_' . count($img_count) . '.png';

        $data = explode(',', $img_data);
        $img_file = base64_decode($data[1]);
        Storage::put('/public/injuryUploads/injury_' . $this->id . '_' . count($img_count) . '.png', $img_file);

        $injuryIMG = new \App\InjuryImage();
        $injuryIMG->injury_id = $this->id;
        $injuryIMG->location = $image_name;
        $injuryIMG->name = $name;
        $injuryIMG->description = $description;
        $injuryIMG->save();

        $post = new TreePost();
        $post->injury_id = $this->id;
        $post->user_id = $user->id;
        $post->body = $user->name . " added a photo.";
        $post->type = 'photoPost';
        $post->save();

        $summary = new SummaryPost();
        $summary->createSummary($this->id, $user->id, $user->name . " added a photo.", "photoPost");
    }

    function savePreexistingInjury($injury_array, $user){
        $duplicate_claim = $this->checkForDuplicateClaim($injury_array);
        if($duplicate_claim === false){
            //get repos for use later
            $comm_repo = new CommunicationRepository();
            $invite_repo = new InvitationRepository();

            $company = \App\Company::where('id', $injury_array['company_id'])->first();

            //create a copy of the team picked for this injury. This injury will use that copy.
            $squad = new \App\Squad();
            $squad_id = $squad->createInjuryTeam($injury_array['squadID']);

            $this->company_id = $injury_array['company_id'];
            $this->claim_step_id = 1;
            $this->type = $injury_array['classification'];
            $this->resolved = 0;
            $this->squad_id = $squad_id;
            $this->injured_employee_id = NULL;
            $this->injured_employee_gender = $injury_array['gender'];
            $this->injury_date = $injury_array['date'];
            $this->severity = $injury_array['severity'];
            $this->notes = $injury_array['notes'];
            if($injury_array['severity'] == "Mild"){
                $this->report_processed = 0;
            }
            //in progress
            $this->in_progress = 1;

            //check for claim number
            if($injury_array['claim_number'] !== NULL && $injury_array['claim_number'] !== ""){
                $this->claim_number = $injury_array['claim_number'];
            }
            $this->save();

            if($injury_array['adjuster_name'] !== NULL && $injury_array['adjuster_name'] !== ""){
                $adjuster_info = false;
                if($injury_array['adjuster_phone'] !== NULL && $injury_array['adjuster_phone'] !== ""){
                    $adjuster_info = true;
                    $this->adjuster_phone = $injury_array['adjuster_phone'];
                }
                if($injury_array['adjuster_email'] !== NULL && $injury_array['adjuster_email'] !== ""){
                    $adjuster_info = true;
                    $this->adjuster_email = $injury_array['adjuster_email'];
                }
                if($adjuster_info){
                    $this->adjuster_name = $injury_array['adjuster_name'];
                }
            }

            if ($injury_array['email'] !== "" && $injury_array['email'] !== NULL) {
                $injuredEmployee = \App\User::where('email', $injury_array['email'])->first();
            } else {
                $injuredEmployee = NULL;
            }

            if (is_null($injuredEmployee)) {
                $team = \App\Team::where('id', $company->team_id)->first();
                if ($injury_array['email'] !== "" and $injury_array['email'] !== NULL) {
                    $invite_repo->inviteNewUser($injury_array['name'], $injury_array['email'], $company->id, 0, "internal", 1, $this->id);
                }
                $injuredEmployee = new \App\User();
                $injuredEmployee->name = $injury_array['name'];
                $injuredEmployee->company_id = $company->id;
                if ($injury_array['email'] !== "" and $injury_array['email'] !== NULL) {
                    $injuredEmployee->email = $injury_array['email'];
                }
                if ($injury_array['phone'] !== "" and $injury_array['phone'] !== NULL) {
                    $injuredEmployee->phone = $injury_array['phone'];
                }
                $injuredEmployee->injured = 1;
                $injuredEmployee->type = 'internal';
                $injuredEmployee->uses_squads = 0;
                $injuredEmployee->current_team_id = $team->id;
                $injuredEmployee->save();

                $team_user = new \App\TeamUser();
                $team_user->user_id = $injuredEmployee->id;
                $team_user->team_id = $team->id;
                $team_user->role = "employee";
                $team_user->save();

                $injuredEmployee->setRandomAvatar();

            } else {
                $injuredEmployee->injured = 1;
                $injuredEmployee->save();
            }

            $this->injured_employee_id = $injuredEmployee->id;
            $this->save();

            //update the new squad's name to match the injured employee
            $squad->squad_name = $injuredEmployee->name . "'s Team";
            $squad->save();

            //*** CREATE TREE POSTS ***
            $detailsPost = new \App\TreePost();
            $detailsPost->injury_id = $this->id;
            $detailsPost->user_id = NULL;
            $day = explode(" ", $injury_array['date']);
            $detailsPost->body = $injuredEmployee->name . " suffered a " . $injury_array['severity'] . " injury on " . $day[0] . ".
        This was reported as an In-Progress Claim, so no immediate action is required.";
            $detailsPost->type = "newInjuryPost";
            $detailsPost->save();

            if ($this->notes) {
                $notesPost = new \App\TreePost();
                $notesPost->injury_id = $this->id;
                $notesPost->user_id = NULL;
                $notesPost->body = "Injury Note: " . $this->notes;
                $notesPost->type = "systemPost";
                $notesPost->save();
            }

            //*** SAVE THE INJURY LOCATIONS AND TYPES ***
            foreach ($injury_array['locations'] as $location) {
                $newLoc = new \App\InjuryLocation();
                $newLoc->injury_id = $this->id;
                $newLoc->location = $location;
                $newLoc->save();
            }
            foreach ($injury_array['types'] as $type) {
                $newType = new \App\InjuryType();
                $newType->injury_id = $this->id;
                $newType->type = $type;
                $newType->save();
            }

            //*** IF AN IMAGE WAS INCLUDED WITH THE INJURY, SAVE IT ***
            if (!is_null($injury_array['img']) && !empty($injury_array['img'])) {
                foreach($injury_array['img'] as $img){
                    $imgCount = \App\InjuryImage::where('injury_id', $this->id)->get();
                    $imageName = '/public/injuryUploads/injury_' . $this->id . '_' . count($imgCount) . '.png';
                    //'/public/injuryUploads/injury_' . $injury_id . '_' . count($imgCount) . '.png',
                    //$image = base64_to_jpeg($img, $this->id, $imgCount, $imageName);
                    /*
                    function base64_to_jpeg($base64_string, $injury_id, $imgCount, $fileLocation)
                    {
                        $data = explode(',', $base64_string);
                        $imgFile = base64_decode($data[1]);
                        Storage::put(
                            $fileLocation,
                            $imgFile
                        );
                    }
                    */
                    $data = explode(',', $img);
                    $img_file = base64_decode($data[1]);
                    Storage::put($imageName, $img_file);
                    $imageName = "/storage" . $imageName;
                    $injuryIMG = new \App\InjuryImage();
                    $injuryIMG->injury_id = $this->id;
                    $injuryIMG->name = "Injury Photo";
                    $injuryIMG->description = "Uploaded from Injury Wizard";
                    $injuryIMG->location = $imageName;
                    $injuryIMG->save();
                }
                $post = new \App\TreePost();
                $post->injury_id = $this->id;
                $post->user_id = $user->id;
                $post->body = $user->name . " added a photo.";
                $post->type = 'photoPost';
                $post->save();
            }

            //****** INJURED GUY IMAGE *******
            if($injury_array['injuryLocationsImage'] !== NULL){
                $imgCount = \App\InjuryImage::where('injury_id', $this->id)->get();
                $imageName = '/public/injuryImages/injury_' . $this->id . '_' . count($imgCount) . '.png';
                //$image = base64_to_jpeg($injury_locations_image, $injury->id, $imgCount, $imageName);
                $data = explode(',', $injury_array['injuryLocationsImage']);
                $img_file = base64_decode($data[1]);
                Storage::put($imageName, $img_file);
                $imageName = "/storage" . $imageName;
                $this->injury_locations_image = $imageName;
                $this->save();
            }
            //first report
            $firstReport = new \App\FirstReportOfInjury();
            $firstReport->injury_id = $this->id;
            $firstReport->squad_id = $this->squad_id;
            $firstReport->action_taken = 1;
            $firstReport->action_taken_at = Carbon::now();
            $firstReport->save();

            //communications go here
            $comm_repo->newInProgressInjury($this->id);
        }else{
            Log::debug('duplicate claim reported.');
            Log::debug(var_export($injury_array, true));
            return false;
        }

    }

    function saveInjury($injury_array, $user){
        $duplicate_claim = $this->checkForDuplicateClaim($injury_array);
        if($duplicate_claim === false){
            Log::debug(var_export($injury_array, true));
            $comm_repo = new CommunicationRepository();
            $invite_repo = new InvitationRepository();

            if ($injury_array['email'] !== "" && $injury_array['email'] !== NULL) {
                $injuredEmployee = \App\User::where('email', $injury_array['email'])->first();
            } else {
                $injuredEmployee = NULL;
            }

            $company = \App\Company::where('id', $injury_array['company_id'])->first();
            if ($company->policy_number == NULL) {
                $company->policy_number = $injury_array['policyNumber'];
                if($injury_array['policyNumber'] != NULL && trim($injury_array['policyNumber']) != '') {
                    $company->policy_updated_at = \Carbon\Carbon::now();
                }
                $company->save();
            }

            //*** CREATE THE NEW INJURY ***
            /*
            company_id: 187,
           policyNumber: 12345,
           classification: "First Aid",
           severity: "Mild",
           name: "Sample Injured",
           gender: "male",
           email: null,
           phone: null,
           date: "09-19-2017 2:25 pm",
           squadID: 235,
           squadName: "Default Squad",
           notes: "Sent From API",
           img: [],
           locations: ["Right Knee", "Right Shin"],
           types: ["Bruise", "Fire"],
           injuryLocationsImage: null,
             */

            $squad = new \App\Squad();
            $squad_id = $squad->createInjuryTeam($injury_array['squadID']);

            $this->company_id = $injury_array['company_id'];
            $this->claim_step_id = 1;
            $this->type = $injury_array['classification'];
            $this->resolved = 0;
            $this->squad_id = $squad_id;
            $this->injured_employee_id = NULL;
            $this->injured_employee_gender = $injury_array['gender'];
            $this->injury_date = $injury_array['date'];
            $this->severity = $injury_array['severity'];
            $this->notes = $injury_array['notes'];
            /*
            if($injury_array['severity'] == "Mild"){
                $this->report_processed = 0;
            }
            */
            $this->save();

            //*** CREATE THE INJURED EMPLOYEE IF THEY DON'T EXIST ***

            if (is_null($injuredEmployee)) {
                $team = \App\Team::where('id', $company->team_id)->first();
                if ($injury_array['email'] !== "" and $injury_array['email'] !== NULL) {
                   $invite_repo->inviteNewUser($injury_array['name'], $injury_array['email'], $company->id, 0, "internal", 1, $this->id);
                   //($team, $injury_array['email'], 'internal', 1, $this->id);
                }
                $injuredEmployee = new \App\User();
                $injuredEmployee->name = $injury_array['name'];
                $injuredEmployee->company_id = $company->id;
                if ($injury_array['email'] !== "" and $injury_array['email'] !== NULL) {
                    $injuredEmployee->email = $injury_array['email'];
                }
                if ($injury_array['phone'] !== "" and $injury_array['phone'] !== NULL) {
                    $injuredEmployee->phone = $injury_array['phone'];
                }
                $injuredEmployee->injured = 1;
                $injuredEmployee->type = 'internal';
                $injuredEmployee->uses_squads = 0;
                $injuredEmployee->current_team_id = $team->id;
                $injuredEmployee->save();

                $team_user = new \App\TeamUser();
                $team_user->user_id = $injuredEmployee->id;
                $team_user->team_id = $team->id;
                $team_user->role = "employee";
                $team_user->save();

                $injuredEmployee->setRandomAvatar();

            } else {
                $injuredEmployee->injured = 1;
                $injuredEmployee->save();
            }
            $this->injured_employee_id = $injuredEmployee->id;
            $this->save();

            //update the new squad's name to match the injured employee
            $squad->squad_name = $injuredEmployee->name . "'s Team";
            $squad->save();

            //*** CREATE TREE POSTS ***
            $detailsPost = new \App\TreePost();
            $detailsPost->injury_id = $this->id;
            $detailsPost->user_id = $user->id;
            $day = explode(" ", $injury_array['date']);
            $detailsPost->body = $injuredEmployee->name . " suffered a " . $injury_array['severity'] . " injury on " . $day[0] . ".
        Please let them know you're thinking about them.";
            $detailsPost->type = "newInjuryPost";
            $detailsPost->save();

            /*** push notification ***/
            $squad_members = \App\SquadMember::where('squad_id', $squad_id)->get();
            $sent_notification_array = [];
            foreach($squad_members as $squad_member){
                if(!in_array($squad_member->user_id, $sent_notification_array)){
                    $recipient_user = \App\User::where('id', $squad_member->user_id)->first();
                    if($recipient_user->notification_priority_1 === 1){
                        $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                        if($devices != NULL || $devices != ""){
                            $devices->sendNotification('New Injury Reported', $injuredEmployee->name . " suffered a " . $injury_array['severity'] . " injury. Please let them know you're thinking about them.");
                        }
                    }
                    array_push($sent_notification_array, $squad_member->user_id);
                }
            }
            /***********************************************************/

            if ($this->notes) {
                $notesPost = new \App\TreePost();
                $notesPost->injury_id = $this->id;
                $notesPost->user_id = NULL;
                $notesPost->body = "Injury Note: " . $this->notes;
                $notesPost->type = "systemPost";
                $notesPost->save();
            }

            //*** SAVE THE INJURY LOCATIONS AND TYPES ***
            foreach ($injury_array['locations'] as $location) {
                $newLoc = new \App\InjuryLocation();
                $newLoc->injury_id = $this->id;
                $newLoc->location = $location;
                $newLoc->save();
            }
            foreach ($injury_array['types'] as $type) {
                $newType = new \App\InjuryType();
                $newType->injury_id = $this->id;
                $newType->type = $type;
                $newType->save();
            }

            //*** CREATE REPORTING TIME TREE POST ***
            //returnClaimReportingTime() will return "ideal", "standard", or "late". generate a tree post depending on the return value
            $reportTime = $this->returnClaimReportingTime();
            if($reportTime == "ideal"){
                $reportTimePost = new \App\TreePost();
                $reportTimePost->injury_id = $this->id;
                $reportTimePost->user_id = NULL;
                $reportTimePost->body = "Congratulations! By reporting this claim within 24 hours of the injury, you've followed Zenjuries' best practices. " .
                    "Studies show that claims reported in this window could cost less than the average by up to 47%. &zwnj;<i class='em em-smiley'></i>&zwnj;";
                $reportTimePost->type = "reportTimeGoodPost";
                $reportTimePost->save();
            }else if($reportTime == "standard"){
                $reportTimePost = new \App\TreePost();
                $reportTimePost->injury_id = $this->id;
                $reportTimePost->user_id = NULL;
                $reportTimePost->body = "This claim was reported within 48 hours of the injury. Studies show that to reduce claim costs, a claim should be reported within 24 hours of the injury. &zwnj;<i class='em em-confused'></i>&zwnj;";
                $reportTimePost->type = "reportTimeOkayPost";
                $reportTimePost->save();
            }else if($reportTime == "late"){
                $reportTimePost = new \App\TreePost();
                $reportTimePost->injury_id = $this->id;
                $reportTimePost->user_id = NULL;
                $reportTimePost->body = "This claim was reported over 48 hours after the injury occurred.
            Studies show that the longer you wait to report a claim, the higher the cost will be. Future injuries should be reported within 24 hours to reduce costs. &zwnj;<i class='em em-worried'></i>&zwnj;";
                $reportTimePost->type = "reportTimeBadPost";
                $reportTimePost->save();
            }

            //*** IF AN IMAGE WAS INCLUDED WITH THE INJURY, SAVE IT ***
            if (!is_null($injury_array['photos']) && !empty($injury_array['photos']) && $injury_array !== "") {
                foreach($injury_array['photos'] as $photo){
                    // Log::debug('Processing photo' . var_export($photo, true));

                    // This will read the "data:image/jpeg;base64" portion of data and decode the base64 contents
                    $img_file_contents = file_get_contents($photo['data']);

                    $imgCount = \App\InjuryImage::where('injury_id', $this->id)->get();
                    $imageName = 'injuryUploads/injury_' . $this->id . '_' . count($imgCount) . '.png';
                    Storage::disk('public')->put($imageName, $img_file_contents);

                    $imageName = "/storage/" . $imageName;
                    $injuryIMG = new \App\InjuryImage();
                    $injuryIMG->injury_id = $this->id;
                    $injuryIMG->name = $photo['name'];
                    $injuryIMG->description = $photo['description'];
                    $injuryIMG->location = $imageName;
                    $injuryIMG->save();
                }
                $post = new \App\TreePost();
                $post->injury_id = $this->id;
                $post->user_id = $user->id;
                $post->body = $user->name . " added a photo.";
                $post->type = 'photoPost';
                $post->save();
            }

            //****** INJURED GUY IMAGE *******
            if($injury_array['injuryLocationsImage'] !== NULL && $injury_array['injuryLocationsImage'] !== ""){
                $imgCount = \App\InjuryImage::where('injury_id', $this->id)->get();
                $imageName = '/public/injuryImages/injury_' . $this->id . '_' . count($imgCount) . '.png';
                //$image = base64_to_jpeg($injury_locations_image, $injury->id, $imgCount, $imageName);
                $data = explode(',', $injury_array['injuryLocationsImage']);
                $img_file = base64_decode($data[1]);
                Storage::put($imageName, $img_file);
                $imageName = "/storage" . $imageName;
                $this->injury_locations_image = $imageName;
                $this->save();
            }

            //*** QUEUE COMMUNICATIONS ***
            $comm_repo->newInjury($this->id, $this->squad_id);
            /*
            if ($injury_array['classification'] == "Work Comp") {
                $comm_repo->newInjury($this->id, $this->squad_id);
            } else if ($injury_array['classification'] == "First Aid") {

                $squadMembers = \App\SquadMember::where('squad_id', $this->squad_id)->get();
                foreach ($squadMembers as $squadMember) {
                    if ($squadMember->position_id == 1) {
                        $comm_repo->newFirstAidInjuryOwner($this->id, $squadMember->user_id);
                    } else if ($squadMember->position_id == 7) {
                        $comm_repo->newFirstAidInjurySafetyCoordinator($this->id, $squadMember->user_id);
                    } else if ($squadMember->position_id == 8) {
                        $comm_repo->newFirstAidInjurySupervisor($this->id, $squadMember->user_id);
                    } else if ($squadMember->position_id == 9) {
                        $comm_repo->newFirstAidInjuryHumanResources($this->id, $squadMember->user_id);
                    } else if ($squadMember->position_id == 10) {
                        $comm_repo->newFirstAidInjuryOther($this->id, $squadMember->user_id);
                    }
                }

            }
            */
            if ($injury_array['email'] !== null && $injury_array['email']) {
                $comm_repo->employeeExpectationsDelivery($this->id);
            }

            //*** FIRST REPORT ***
            $firstReport = new \App\FirstReportOfInjury();
            $firstReport->injury_id = $this->id;
            $firstReport->squad_id = $this->squad_id;
            $firstReport->save();
            //if the company has a zen carrier and the injury isn't mild, auto-deliver the first report
            if(!is_null($company->zen_carrier_id) && $company->policy_number !== "" && $company->policy_number !== NULL){
                Log::debug('in zen_carrier block');
                $delivery = $this->deliverFirstReportToZenCarrier();
                if($delivery === false){
                    Log::debug('delivery = false');
                    $comm_repo->firstReportOfInjury($firstReport->id);
                }
            }else{
                Log::debug('skipped zen_carrier block');
                $comm_repo->firstReportOfInjury($firstReport->id);
            }


            return $this->id;
        }else{
            Log::debug('duplicate claim reported.');
            Log::debug(var_export($injury_array, true));
            return false;
        }
    }

    function savePhotoArray($photo_array, $user_id){
	    $new_upload_count = count($photo_array);
	    if($new_upload_count > 0){
		    $user_name = \App\User::where('id', $user_id)->value('name');
		    foreach($photo_array as $photo){
			    $image_count = count(\App\InjuryImage::where('injury_id', $this->id)->get());
			    $imageName = '/public/injuryUploads/injury_' . $this->id . '_' . $image_count . '.png';
			    $data = explode(',', $photo);
			    $img_file = base64_decode($data[1]);
			    Storage::put($imageName, $img_file);
			    $imageName = "/storage" . $imageName;
			    $injuryIMG = new \App\InjuryImage();
			    $injuryIMG->injury_id = $this->id;
			    $injuryIMG->name = "Injury Photo";
			    $injuryIMG->description = "Uploaded from Tree of Communications";
			    $injuryIMG->location = $imageName;
			    $injuryIMG->save();
	    	}
		    $post = new \App\TreePost();
		    $post->injury_id = $this->id;
		    $post->user_id = $user_id;
		    if($new_upload_count === 1){
			    $post->body = $user_name . " added a photo.";
		    }else{
			    $post->body = $user_name . " added some photos.";
		    }
		    $post->type = "photoPost";
		    $post->save();
	    }
    }

    function getLastDiaryUpdate(){
        $diary_post = \App\DiaryPost::where('injury_id', $this->id)->latest()->first();
        if(!is_null($diary_post)){
            $date = $diary_post->created_at;
            if($date->isToday()){
                return "Today " . $date->format('g:i a');
            }else if($date->isYesterday()){
                return "Yesterday " . $date->format('g:i a');
            }else{
                return $date->format('F jS g:i a');
            }
        }else{
            return "0";
        }
    }

    //record the last time the user viewed tree posts for a specific injury
    function markTreePostsAsViewed($user_id){
        //if they've viewed tree posts for this injury already there will be a record, we'll just update that one
        $tree_post_view = \App\TreePostView::where('user_id', $user_id)->where('injury_id', $this->id)->first();
        //if its their first time viewing this injury, we'll have to create a new record
        if(is_null($tree_post_view)){
            $tree_post_view = new \App\TreePostView();
            $tree_post_view->user_id = $user_id;
            $tree_post_view->injury_id = $this->id;
        }

        $tree_post_view->last_read_at = \Carbon\Carbon::now();
        $tree_post_view->save();
    }

    //check if the claim has had any costs entered
    function hasClaimCosts(){
        $medical = \App\MedicalCosts::where('injury_id', $this->id)->first();
        $legal = \App\LegalCosts::where('injury_id', $this->id)->first();
        $indemnity = \App\IndemnityCosts::where('injury_id', $this->id)->first();
        $misc = \App\MiscellaneousCosts::where('injury_id', $this->id)->first();

        $reserve_medical = \App\ReserveMedicalCost::where('injury_id', $this->id)->first();
        $reserve_legal = \App\ReserveLegalCost::where('injury_id', $this->id)->first();
        $reserve_indemnity = \App\ReserveIndemnityCost::where('injury_id', $this->id)->first();
        $reserve_misc = \App\ReserveMiscellaneousCost::where('injury_id', $this->id)->first();

        if($medical !== NULL || $legal !== NULL || $indemnity !== NULL || $misc !== NULL ||
            $reserve_medical !== NULL|| $reserve_legal !== NULL || $reserve_indemnity !== NULL || $reserve_misc !== NULL){
            return true;
        }else return false;
    }

    function getTotalCost($type = "paid"){
        if($type === "paid"){
            $med = \App\MedicalCosts::where('injury_id', $this->id)->where('current', 1)->value('cost');
            if(is_null($med)){$med = 0;}
            $indemnity = \App\IndemnityCosts::where('injury_id', $this->id)->where('current', 1)->value('cost');
            if(is_null($indemnity)){$indemnity = 0;}
            $legal = \App\LegalCosts::where('injury_id', $this->id)->where('current', 1)->value('cost');
            if(is_null($legal)){$legal = 0;}
            $misc = \App\MiscellaneousCosts::where('injury_id', $this->id)->where('current', 1)->value('cost');
            if(is_null($misc)){$misc = 0;}
        }else if($type === "reserve"){
            $med = \App\ReserveMedicalCost::where('injury_id', $this->id)->where('current', 1)->value('cost');
            if(is_null($med)){$med = 0;}
            $indemnity = \App\ReserveIndemnityCost::where('injury_id', $this->id)->where('current', 1)->value('cost');
            if(is_null($indemnity)){$indemnity = 0;}
            $legal = \App\ReserveLegalCost::where('injury_id', $this->id)->where('current', 1)->value('cost');
            if(is_null($legal)){$legal = 0;}
            $misc = \App\ReserveMiscellaneousCost::where('injury_id', $this->id)->where('current', 1)->value('cost');
            if(is_null($misc)){$misc = 0;}
        }else return false;

        return ($med + $indemnity + $legal + $misc);
    }

    function checkForDuplicateClaim($injury_array){
        /*
         $this->company_id = $injury_array['company_id'];
        $this->claim_step_id = 1;
        $this->type = $injury_array['classification'];
        $this->resolved = 0;
        $this->squad_id = $squad_id;
        $this->injured_employee_id = NULL;
        $this->injured_employee_gender = $injury_array['gender'];
        $this->injury_date = $injury_array['date'];
        $this->severity = $injury_array['severity'];
        $this->notes = $injury_array['notes'];
        /*
         */
        //company_id
        //created_at, check if same day?
        //type/severity
        //injured employee gender
        //injury_date
        //severity
        //notes
        $duplicate_injury = \App\Injury::where('company_id', $injury_array['company_id'])
            ->where('injury_date', $injury_array['date'])
            ->where('notes', $injury_array['notes'])
            ->first();
        if(is_null($duplicate_injury)){
            return false;
        }else return true;

    }

    function getLatestInjuryPost(){
        $diary_post = \App\DiaryPost::where('injury_id', $this->id)->where('post_type', 'injury')->latest()->first();
        return $diary_post;
    }

    function getLatestClaimPost(){
        $diary_post = \App\DiaryPost::where('injury_id', $this->id)->where('post_type', 'claim')->latest()->first();
        return $diary_post;
    }

    function getSquadID(){
        $squad = \App\Squad::where('id', $this->squad_id)->first();
        return $squad->id;
    }

    function getSquadName(){
        $squad = \App\Squad::where('id', $this->squad_id)->first();
        return $squad->squad_name;
    }

    function printInjuryTypes(){
        $types = \App\InjuryType::where('injury_id', $this->id)->get();
        if(count($types) === 1){
            echo $types[0]->type;
        }else{
            $type_string = "";
            $i = 1;
            foreach($types as $type){
                if($i < count($types)){
                    $type_string .= $type->type . ", ";
                }else{
                    $type_string .= $type->type;
                }
                $i++;
            }
            echo $type_string;
        }
    }

    function printInjuryLocations(){
        $locations = \App\InjuryLocation::where('injury_id', $this->id)->get();
        if(count($locations) === 1){
            echo $locations[0]->location;
        }else{
            $location_string = "";
            $i = 1;
            foreach($locations as $location){
                if($i < count($locations)){
                    $location_string .= $location->type . ", ";
                }else{
                    $location_string .= $location->type;
                }
                $i++;
            }
            echo $location_string;
        }
    }

    // Get Employee Details

    function employee(){
        return $this->hasOne(User::class, "id", "injured_employee_id");
    }
}

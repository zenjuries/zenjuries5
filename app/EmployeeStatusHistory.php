<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeStatusHistory extends Model
{
    //
    protected $table = "employee_status_history";
}

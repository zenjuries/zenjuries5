<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    //
    function getLocation(){
        $location = \App\InjuryCareLocation::where('id', $this->location_id)->first();
        return $location;
    }

    function addSummary($summary){
        $this->summary = $summary;
        $this->save();
    }

    function newAppointment($name, $phone, $email, $datetime, $description, $injury_id, $user_id){
        $care_location = new InjuryCareLocation();
        $care_location->injury_id = $injury_id;
        $care_location->name = $name;
        $care_location->email = $email;
        $care_location->phone = $phone;
        $care_location->save();

        $this->injury_id = $injury_id;
        $this->location_id = $care_location->id;
        $this->description = $description;
        $this->appointment_time = $datetime;
        $this->save();

        $tree_post = new \App\TreePost();
        $tree_post->newAppointmentPost($this, $user_id);

		return $this;
    }
}

<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Log;
use App\DiaryPost;
use App\NotePost;
use App\Appointment;
use App\Injury;

class SummaryPost extends Model
{
    protected $table = 'summary_posts';

    function createSummary($injury_id, $user_id, $summary, $type){
        $this->injury_id = $injury_id;
        $this->user_id = $user_id;
        $this->summary = $summary;
        $this->type = $type;
        $this->save();
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public function company()
    {
        return $this->hasOne('App\Company');
    }

    protected $dates = ['old_subscription_ends_on'];

    function getCompany(){
        $company = \App\Company::where('team_id', $this->id)->first();
        return $company;
    }

    function getOwner(){
        $user = \App\User::where('id', $this->owner_id)->first();
        return $user;
    }
}

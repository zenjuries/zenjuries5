<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use App\Services\FCMService;

class DeviceId extends Model
{
    protected $table = 'device_tokens';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'device_token'
    ];

    public function sendNotification($title, $body){
        $token_1 = $this->device_token_1;
        $token_2 = $this->device_token_2;
        $token_3 = $this->device_token_3;
        $tokens = [$token_1, $token_2, $token_3];
        foreach($tokens as $token){
            Log::debug($token);
            if($token != NULL){
                FCMService::send(
                    $token,
                    [
                        'title' => $title,
                        'body' => $body,
                        'sound' => 'default',
                        'badge' => '1',
                    ]
                );
            }
        }
    }


}

<?php

namespace App\Http\Controllers;

use App\User;
use App\UserWidget;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    //stats_bar_open is a boolean on the User table, 1 = open
    //inputs: none
    function toggleStatsBarSetting(Request $request){
        //Auth::user() is a laravel helper function that will get the currently authenticated user,
        // basically it will return whoever made the request.
        // Any route (get or post) that logged in user's will access has the 'auth' middleware,
        // meaning we know there will be a logged in user if the request reaches this point.
        $user = Auth::user();
        if($user->stats_bar_open === 1){
            $user->stats_bar_open = 0;
        }else{
            $user->stats_bar_open = 1;
        }
        $user->save();
    }

    //inputs: widget_id
    function toggleWidget(Request $request){
        $widget_id = $request->widget_id;
        //see if the user already has a record for that widget
        $user_widget = UserWidget::where('id', Auth::user()->id)->where('widget_id', $widget_id)->first();
        //if not, we'll create one, toggling the widget to "On" for that user.
        if(is_null($user_widget)){
            $user_widget = new UserWidget();
            //createNewUserWidget is a function defined on the UserWidget model
            $user_widget->createNewUserWidget(Auth::user()->id, $widget_id);
        }else{
            //if they already have a record for that widget we'll delete it, toggling that widget to "off" for that user
            $user_widget->delete();
        }
    }

    function devLogin(Request $request, $type){
        if(env('APP_ENV') === "local"){
            if($type === "user"){
                $user = User::where('id', 164)->first();
            }else if($type === "zagent"){
                $user = User::where('id', 206)->first();
            }else if($type === "zenpro"){
                $user = User::where('id', 6)->first();
            }else if($type === "injured"){
                $user = User::where('id', 9)->first();
            }
            Auth::login($user);

        }
        return redirect('/');
    }

    function logout(){
        Auth::logout();
        return redirect('/');
    }

    function updateCommunicationSettings(Request $request)
    {
        $id = $request->input('user_id');
        $weeklyUpdate = $request->input('weeklyUpdate');
        $priority2Email = $request->input('priority2Email');
        $priority3Email = $request->input('priority3Email');
        $user = \App\User::where('id', $id)->first();

        $user->gets_weekly_summary = $weeklyUpdate;
        $user->gets_priority_2_emails = $priority2Email;
        $user->gets_priority_3_emails = $priority3Email;
        $user->save();

        Log::debug('prior 2: '. $user->gets_priority_2_emails);
        Log::debug('prior 3: '. $user->gets_priority_3_emails);

        if( $priority2Email == 1 && $priority3Email == 1){
            $alert_setting = "full";
        }else if($priority3Email == 0 && $priority2Email == 1){
            $alert_setting = "partial";
        }else if($priority3Email == 1 && $priority2Email == 0){
            $alert_setting = "partial";
        }else{
            $alert_setting = "none";
        }

        return $alert_setting;
    }

    function updatePushNotificationSettings(Request $request){
        $id = $request->input('user_id');
        $basePush = $request->input('basePush');
        $additionalPush = $request->input('additionalPush');
        $allcommsPush = $request->input('allcommsPush');
        $user = \App\User::where('id', $id)->first();
        
        $user->notification_priority_1 = $basePush;
        $user->notification_priority_2 = $additionalPush;
        $user->notification_priority_3 = $allcommsPush;
        $user->save();
    }

    public function updateUserShortcuts(Request $request){
        $user = Auth::user();
        $shortcuts = $request->shortcuts;
        $user->shortcuts = $shortcuts;
        $user->save();
    }

    public function updateUserTheme(Request $request){
        $user = Auth::user();
        $theme_id = $request->theme;
        $user->theme_id = $theme_id;
        $user->save();
    }

}

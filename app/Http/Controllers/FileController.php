<?php
namespace App\Http\Controllers;

use App\InjuryFile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Dompdf\Dompdf;
use Dompdf\Options;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\View;

class FileController extends Controller
{
    //'photo' => 'mimes:jpeg,bmp,png'

    public function justForTesting(){
        Log::debug("Just For Testing");
    }

    public function ajaxFileUpload(Request $request){
        Log::debug('ajaxFileUpload');
        //return Response::json("File Uploaded");
        Log::debug(var_export($request->input('fileName'), true));
        Log::debug(var_export($request, true));

        $validator = Validator::make($request->all(), [
           'fileName' => 'required',
           'file' => 'required|mimes:pdf,doc,docx,rtf,txt,eml,csv,xls,xlsx'
        ]);

        if($validator->fails()){
            Log::debug('ajax upload failed');
            return response::json(['Please make sure your file matches one of these types: .pdf, .doc, .docx, .rtf, .csv, .xls, .txt, or .eml'], 422);
        }

        $file = $request->file('file');

        $fileName = $request->fileName;
        $fileDesc = $request->fileDesc;
        $injury_id = $request->injury_id;
        $extension = $file->getClientOriginalExtension();

        Log::debug('got data');
        Log::debug(var_export($file, true));
        Log::debug(var_export($extension, true));

        $count = \App\InjuryFile::where('injury_id', $injury_id)->get();
        $count = count($count);
        $modifiedName = preg_replace("/[^A-Za-z0-9 ]/", '', $fileName);
        $modifiedName = preg_replace('/\s+/', '', $modifiedName);
        $nameOfFile = $injury_id . "_" . $modifiedName . "_" . $count . "." . $extension;
        $file->move('storage/public/injuryFiles/', $nameOfFile);
        $nameOfFile = 'injuryFiles/' . $nameOfFile;

        Log::debug('file stored');

        $newFile = new InjuryFile();
        $newFile->injury_id = $injury_id;
        $newFile->name = $fileName;
        $newFile->description = $fileDesc;
        $newFile->file_type = $extension;
        $newFile->location = $nameOfFile;
        $newFile->save();

        Log::debug('saved in DB');

        $post = new \App\TreePost();
        $post->injury_id = $injury_id;
        $post->user_id = Auth::user()->id;
        $post->body = Auth::user()->name . " added a file named " . $newFile->name . ".";
        $post->type = 'filePost';
        $post->save();

        Log::debug('saved tree post');

        $user = Auth::user();
        $user->zenjuries_userdocs_uploaded++;
        $user->save();

        Log::debug('updated user');

        return Response::json('success');
    }

    public function deleteInjuryFile(Request $request){
	    $file_id = $request->input('file_id');
	    $file = \App\InjuryFile::where('id', $file_id)->first();

	    $injury = \App\Injury::where('id', $file->injury_id)->first();

	    $file->delete();

	    $post = new \App\TreePost();
        $post->injury_id = $injury->id;
        $post->user_id = Auth::user()->id;
        $post->body = Auth::user()->name . " deleted a file named " . $file->name . ".";
        $post->type = 'filePost';
        $post->save();
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'fileName' => 'required',
            'file' => 'required|mimes:pdf,doc,docx,rtf,txt,msg,eml,csv,xls,xlsx'
        ]);

        if($validator->fails()){

            $validator = Validator::make($request->all(),[
               'file' => 'is_msg_file'
            ]);

            if($validator->fails()){
                return Redirect::back()->withErrors("The file must be a file of type: pdf, doc, docx, rtf, txt, eml, csv, xls, xlsx, or msg");
            }
        }

        $file = $request->file('file');

        $fileName = $request->fileName;
        $fileDesc = $request->fileDesc;
        $injury_id = $request->injury_id;
        $extension = $file->getClientOriginalExtension();

        $count = \App\InjuryFile::where('injury_id', $injury_id)->get();
        $count = count($count);
        $modifiedName = preg_replace("/[^A-Za-z0-9 ]/", '', $fileName);
        $modifiedName = preg_replace('/\s+/', '', $modifiedName);
        $nameOfFile = $injury_id . "_" . $modifiedName . "_" . $count . "." . $extension;
        $file->move('storage/public/injuryFiles/', $nameOfFile);
        $nameOfFile = 'injuryFiles/' . $nameOfFile;
        $newFile = new InjuryFile();
        $newFile->injury_id = $injury_id;
        $newFile->name = $fileName;
        $newFile->description = $fileDesc;
        $newFile->file_type = $extension;
        $newFile->location = $nameOfFile;
        $newFile->save();
        $post = new \App\TreePost();
        $post->injury_id = $injury_id;
        $post->user_id = Auth::user()->id;
        $post->body = Auth::user()->name . " added a file named " . $newFile->name . ".";
        $post->type = 'filePost';
        $post->save();
        $user = Auth::user();
        $user->zenjuries_userdocs_uploaded++;
        $user->save();

        return redirect($request->redirect_url . '/' . $injury_id);



        /*
        $validator = Validator::make($request->all(), [
           'file' => 'is_msg_file'
        ]);

        if($validator->fails()){
            //return Redirect::back()->withErrors($validator);
            return "false";
        }else{
            return "true";
        }
        */
       // Log::debug(var_export($request->file, true));
        /*
        Log::debug(var_export($request, true));

        //Log::debug(var_export($request->file, true));

        */
    }

    public function renderSummaryReport($start_date, $end_date){
        $data = [
            'start_date' => $start_date,
            'end_date' => $end_date
        ];
        // return view('render/summary', ["data"=>$data]);
        $options = new Options();
        $options->set('isRemoteEnabled', TRUE);
        $pdf = new Dompdf($options);
        $path = view('render/summary', ["data"=>$data]);
        $pdf->loadHtml($path);
        // $pdf->setPaper('A3', 'portrait');
        $pdf->render();
        try{
            return $pdf->stream('summary.pdf', array("Attachment" => true));
        }catch (\ErrorException $e) {
            Log::debug("dompdf error with certificate download: " . $e->getMessage());
        }
    }

    public function renderFirstReport($injury_id){
        $data = [
            'injury_id' => $injury_id
        ];
        $options = new Options();
        $options->set('isRemoteEnabled', TRUE);
        $pdf = new Dompdf($options);
        $path = view('render/firstReportRender', ["data"=>$data]);
        $pdf->loadHtml($path);
        $pdf->setPaper('A3', 'landscape');
        $pdf->render();
        try{
            return $pdf->stream('InjuryAlert.pdf', array("Attachment" => true));
        }catch (\ErrorException $e) {
            Log::debug("dompdf error with certificate download: " . $e->getMessage());
        }
        /*
        $injury_id = $id;
        $injuredID = \App\Injury::where('id', $injury_id)->value('injured_employee_id');
        $injuredName = \App\User::where('id', $injuredID)->value('name');
        $injuredName = preg_replace('/\s+/', '', $injuredName);
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3')->loadVIEW('render.firstReportRender', ['injuryID' => $injury_id]);
        //BUG: dompdf (the package we use to convert HTML pages as pdfs) has a bug in PHP 7.1 that causes it to throw an exception
        //this try-catch block will log the error and re-run the code to generate the pdf, with the errors suppressed
        //this should only happen in testing environments, as the production server is not running PHP 7.1 everything should work normally.
        try {
            return $pdf->download('InjuryAlert' . $injuredName . '.pdf');
        }catch (\ErrorException $e){
            Log::debug("dompdf error with Injury Alert download: " . $e->getMessage());
        }
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3')->loadVIEW('render.firstReportRender', ['injuryID' => $injury_id]);
        return @$pdf->download('InjuryAlert' . $injuredName . '.pdf');
        */
    }

    public function renderOfficialFirstReport($id){
        $injury_id = $id;
        $injuredID = \App\Injury::where('id', $injury_id)->value('injured_employee_id');
        $injuredName = \App\User::where('id', $injuredID)->value('name');
        $injuredName = preg_replace('/\s+/', '', $injuredName);
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3')->loadVIEW('render.official', ['injuryID' => $injury_id]);
        return @$pdf->download('OfficialFirstReport' . $injuredName . '.pdf');
    }

    public function renderOfficialFirstReportBlank(){
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3')->loadVIEW('render.officialFirstReportRenderBlank');
        return @$pdf->download('ZenjuriesOfficialFirstReport.pdf');
    }

    public function renderMedicalDocument($id){
        $injury_id = $id;
        $injured_employee_id = \App\Injury::where('id', $injury_id)->value('injured_employee_id');
        $injured_employee_name = \App\User::where('id', $injured_employee_id)->value('name');
        $injured_employee_name = preg_replace('/\s+/', '', $injured_employee_name);
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3')->loadVIEW('render.medicalDocument', ['injuryID' => $injury_id]);
        //BUG: dompdf (the package we use to convert HTML pages as pdfs) has a bug in PHP 7.1 that causes it to throw an exception
        //this try-catch block will log the error and re-run the code to generate the pdf, with the errors suppressed
        //this should only happen in testing environments, as the production server is not running PHP 7.1 everything should work normally.
        try {
            return $pdf->download('MedicalInformationDocument' . $injured_employee_name . '.pdf');
        }catch (\ErrorException $e){
            Log::debug("dompdf error with Medical Information Document download: " . $e->getMessage());
        }
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3')->loadVIEW('render.medicalDocument', ['injuryID' => $injury_id]);
        return @$pdf->download('MedicalInformationDocument' . $injured_employee_name . '.pdf');

    }

    public function renderCert($id){
/*
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3', 'landscape')->loadVIEW('render.certRender', ['company_id' => $id]);
        //BUG: dompdf (the package we use to convert HTML pages as pdfs) has a bug in PHP 7.1 that causes it to throw an exception
        //this try-catch block will log the error and re-run the code to generate the pdf, with the errors suppressed
        //this should only happen in testing environments, as the production server is not running PHP 7.1 everything should work normally.
        try{
            return $pdf->download('ZenjuriesCertificate.pdf');
        }catch (\ErrorException $e){
            Log::debug("dompdf error with certificate download: " . $e->getMessage());
        }
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3', 'landscape')->loadVIEW('render.certRender', ['company_id' => $id]);
        return @$pdf->download('ZenjuriesCertificate.pdf');
*/

/*** THIS WORKS FOR THE DOWNLOAD, BUT NO CSS
 *   (USING THE LARAVEL-DOMPDF PACKAGE)
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3', 'landscape')->loadVIEW('render.renderCert', ['company_id' => $id]);
        try{
            return $pdf->download('ZenjuriesCertificate.pdf');
        }catch (\ErrorException $e){
            Log::debug("dompdf error with certificate download: " . $e->getMessage());
        }
*/
        //option class for dompdf

        //THIS ALSO WORKS USING THE DEFAULT IMPLEMENTATION, BUT NO CSS
        $options = new Options();
        //set the chroot to the path were the view is
       // $options->set('chroot', resource_path() . '/views/pages');
        //set remote to true so you can download
        $options->set('isRemoteEnabled', TRUE);
        //dompdf class with the the options
        $pdf = new Dompdf($options);

        $path = view('render/renderCert');

        $pdf->loadHtml($path);
        $pdf->setPaper('A3', 'landscape');
        //render builds the pdf
        $pdf->render();

        try{
            //stream downloads the pdf
            return $pdf->stream('ZenjuriesCertificate.pdf', array("Attachment" => true));
            //return view('render.certRender');
        }catch (\ErrorException $e) {
            Log::debug("dompdf error with certificate download: " . $e->getMessage());
        }

    }

    public function downloadAgencyStats($agency_id, $start_date, $end_date){
        $stats_repo = new AgencyStatsRepository();
        $injuries = $stats_repo->getAgencyInjuries($agency_id, $start_date, $end_date);
        $stats_array = $stats_repo->returnStatsArray($injuries);
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3', 'landscape')->loadView('render.agencyStatsDownload', ['stats_array' => $stats_array,
            'agency_id' => $agency_id,
            'start_date' => $start_date,
            'end_date' => $end_date]);
        //BUG: dompdf (the package we use to convert HTML pages as pdfs) has a bug in PHP 7.1 that causes it to throw an exception
        //this try-catch block will log the error and re-run the code to generate the pdf, with the errors suppressed
        //this should only happen in testing environments, as the production server is not running PHP 7.1 everything should work normally.
        try{
            return $pdf->download('ZenjuriesAgencyStats.pdf');
        }catch(\ErrorException $e){
            Log::debug("dompdf error with agency stats download: " . $e->getMessage());
        }
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3', 'landscape')->loadView('render.agencyStatsDownload', ['stats_array' => $stats_array,
            'agency_id' => $agency_id,
            'start_date' => $start_date,
            'end_date' => $end_date]);
        return @$pdf->download('ZenjuriesAgencyStats.pdf');
    }

    public function downloadCompanyStats($company_id, $start_date, $end_date){
        $stats_repo = new AgencyStatsRepository();
        $injuries = $stats_repo->getCompanyInjuries($company_id, $start_date, $end_date);
        $stats_array = $stats_repo->returnStatsArray($injuries);
        $company = DB::table('companies')->where('id', $company_id)->first();
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3', 'landscape')->loadView('render.companyStatsDownload',
            ['stats_array' => $stats_array,
                'company_name' => $company->company_name,
                'company_id' => $company_id,
                'start_date' => $start_date,
                'end_date' => $end_date]);
        //BUG: dompdf (the package we use to convert HTML pages as pdfs) has a bug in PHP 7.1 that causes it to throw an exception
        //this try-catch block will log the error and re-run the code to generate the pdf, with the errors suppressed
        //this should only happen in testing environments, as the production server is not running PHP 7.1 everything should work normally.
        try{
            return $pdf->download('ZenjuriesCompanyStats.pdf');
        }catch(\ErrorException $e){
            Log::debug("dompdf error with company stats download: " . $e->getMessage());
        }
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3', 'landscape')->loadView('render.companyStatsDownload',
            ['stats_array' => $stats_array,
                'company_name' => $company->company_name,
                'company_id' => $company_id,
                'start_date' => $start_date,
                'end_date' => $end_date]);
        return @$pdf->download('ZenjuriesCompanyStats.pdf');
    }

    /* RETURN DOWNLOAD OF INJURIES SUMMARY/LOSS REPORT */
    public function renderLossReport($start_date, $end_date, $claim_type, $claim_status){
        //to pass data to the view, all the data must be an array
        $data = [
            'start_date' => $start_date,
            'end_date' => $end_date,
            'claim_type' => $claim_type,
            'claim_status' => $claim_status
        ];
        Log::debug($data);
        $options = new Options();
        //set isRemoteEnabled to true so download is possible
        $options->set('isRemoteEnabled', TRUE);
        //dompdf class with the options
        $pdf = new Dompdf($options);
        //set the path to the view
        $path = view('render/lossReport', ["data"=>$data]);
        Log::debug('after view call');
        //loads the view
        $pdf->loadHtml($path);
        //set the size of the pdf
        $pdf->setPaper('A3', 'landscape');
        //render builds the pdf
        $pdf->render();
        try{
            return $pdf->stream('LossReport.pdf', array("Attachment" => true));
        }catch (\ErrorException $e) {
            Log::debug("dompdf error with loss report download: " . $e->getMessage());
        }

}

    public function returnInjuriesSummaryReport($start_date, $end_date, $claim_type, $claim_status){
        $start_date = Carbon::createFromFormat('d-m-Y', $start_date)->format('m-d-Y');
        $end_date = Carbon::createFromFormat('d-m-Y', $end_date)->format('m-d-Y');
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3', 'landscape')->loadVIEW('render.injuryReport',
            ['start_date' => $start_date, 'end_date' => $end_date, 'claim_type' => $claim_type, 'claim_status' => $claim_status, 'download' => true]);

        //BUG: dompdf (the package we use to convert HTML pages as pdfs) has a bug in PHP 7.1 that causes it to throw an exception
        //this try-catch block will log the error and re-run the code to generate the pdf, with the errors suppressed
        //this should only happen in testing environments, as the production server is not running PHP 7.1 everything should work normally.

        try{
            return $pdf->download('ZenjuriesClaimSummary.pdf');
        }catch(\ErrorException $e){
            Log::debug("dompdf error with injury summary: " . $e->getMessage());
        }
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3', 'landscape')->loadVIEW('render.injuryReport',
            ['start_date' => $start_date, 'end_date' => $end_date, 'claim_type' => $claim_type, 'claim_status' => $claim_status, 'download' => true]);
        return @$pdf->download('ZenjuriesClaimSummary.pdf');
    }

    /* RETURN INJURIES SUMMARY/LOSS REPORT VIEW */
    function getLossReport($start_date, $end_date, $claim_type, $claim_status){
        $start_date = Carbon::createFromFormat('d-m-Y', $start_date)->format('m-d-Y');
        $end_date = Carbon::createFromFormat('d-m-Y', $end_date)->format('m-d-Y');
        return view('render.injuryReport',
            ['start_date' => $start_date, 'end_date' => $end_date, 'claim_type' => $claim_type, 'claim_status' => $claim_status, 'download' => false]);
    }

    public function downloadEmployeeExpectations(){
        return response()->download('downloads/EmployeeExpectations.pdf');
    }

    public function downloadAPMarketing1(){
        return response()->download('downloads/marketing_AP_1.pdf');
    }

    public function downloadAFMarketing1(){
        return response()->download('downloads/marketing_AF_1.pdf');
    }

    public function displayFile($id){
        $fileID = $id;
        $file = \App\InjuryFile::where('id', $fileID)->first();
        $path = storage_path('app/public/' . $file->location);
        return Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$file->name.'"'
        ]);
    }

    public function returnDownload($id){
        //get the record of the requested file
        $file = \App\InjuryFile::where('id', $id)->first();
        //we'll make sure the user requesting the file should be able to access it
        //to do that, check the company the file belongs to and make sure the user is part of that company
        $company_id = \App\Injury::where('id', $file->injury_id)->value('company_id');

        $user = Auth::user();
        $user_check = \App\ExternalUserCompanies::where('user_id', $user->id)->where('company_id', $company_id)->first();
        if(!is_null($user_check) or $user->company_id == $company_id or $user->checkIfZcareOnInjury($file->injury_id)) {
            $user->zenjuries_userdocs_downloaded++;
            $user->save();
            return response()->download(storage_path() . '/app/public/' . $file->location);
        }else {
            return response::json('You don\'t have permission to download that file.');
        }
    }

    public function returnDownloadMobile($id){
        //get the record of the requested file
        $file = \App\InjuryFile::where('id', $id)->first();
        //we'll make sure the user requesting the file should be able to access it
        //to do that, check the company the file belongs to and make sure the user is part of that company
        $company_id = \App\Injury::where('id', $file->injury_id)->value('company_id');

        $user = Auth::guard('api')->user();
        $user_check = \App\ExternalUserCompanies::where('user_id', $user->id)->where('company_id', $company_id)->first();
        if(!is_null($user_check) or $user->company_id == $company_id){
            $user->zenjuries_userdocs_downloaded++;
            $user->save();
            return response()->download(storage_path() . '/app/public/' . $file->location);
        }else {
            return response::json("you don\t have permission to download that file.");
        }
    }

    public function saveCostHistoryChart(Request $request){
        $img = $request->input('img');
        $injury_id = $request->input('injury_id');
        $data = explode(',', $img);
        $imgFile = base64_decode($data[1]);
        Storage::put('/public/costHistory/cost_history' . $injury_id . '.' . 'png', $imgFile);
    }

    public function saveStatCharts(Request $request){
        $paid_chart = $request->input('paidCostChart');
        $reserve_chart = $request->input('reserveCostChart');
        $final_chart = $request->input('finalCostChart');
        $agency_id = $request->input('agency_id');
        $data = explode(',', $paid_chart);
        $img = base64_decode($data[1]);
        Storage::put('/public/agencyStats/paid_chart_' . $agency_id . '.png', $img);
        $data = explode(',', $reserve_chart);
        $img = base64_decode($data[1]);
        Storage::put('public/agencyStats/reserve_chart_' . $agency_id . '.png', $img);
        $data = explode(',', $final_chart);
        $img = base64_decode($data[1]);
        Storage::put('public/agencyStats/final_chart_' . $agency_id . '.png', $img);
    }



    public function saveCompanyStatCharts(Request $request){
        $paid_chart = $request->input('paidCostChart');
        $reserve_chart = $request->input('reserveCostChart');
        $final_chart = $request->input('finalCostChart');
        $company_id = $request->input('company_id');
        $data = explode(',', $paid_chart);
        $img = base64_decode($data[1]);
        Storage::put('/public/agencyStats/paid_chart_company_' . $company_id . '.png', $img);
        $data = explode(',', $reserve_chart);
        $img = base64_decode($data[1]);
        Storage::put('public/agencyStats/reserve_chart_company_' . $company_id . '.png', $img);
        $data = explode(',', $final_chart);
        $img = base64_decode($data[1]);
        Storage::put('public/agencyStats/final_chart_company_' . $company_id . '.png', $img);
    }

    public function downloadCostHistoryChart($injury_id){
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A3', 'landscape')->loadView('render.costHistory', ['injury_id' => $injury_id]);
        return $pdf->download('claimCostHistory.pdf');
    }

    public function downloadInjuryWizardTutorial(){
        return response()->download('downloads/tutorials/injurywizardpdf.pdf');
    }

    public function downloadTreeOfCommunicationsTutorial(){
        return response()->download('downloads/tutorials/treeofcommunicationspdf.pdf');

    }

    public function downloadTeambuilderTutorial(){
        return response()->download('downloads/tutorials/teambuilderpdf.pdf');
    }

    public function downloadDocument(Request $request){
        $filePath = str_replace('storage/', '', $request->location);
        $filePath = storage_path() . '/app/public/' . $filePath;
        $fileType = substr($filePath, strpos($filePath, ".") + 1);
    	$headers = ['Content-Type: application/'.$fileType];
    	$fileName = 'zenjuries_download_file'.$fileType;
        Log::debug("filename: " . $fileName);
    	return [response()->download($filePath, $fileName, $headers), $fileType, $fileName];
    }

    public function deleteDocument(Request $request){
        $file_id = $request->id;
	    $file = \App\InjuryFile::where('id', $file_id)->first();
	    $injury = \App\Injury::where('id', $file->injury_id)->first();

	    $file->delete();

	    $post = new \App\TreePost();
        $post->injury_id = $injury->id;
        $post->user_id = Auth::user()->id;
        $post->body = Auth::user()->name . " deleted a file named " . $file->name . ".";
        $post->type = 'filePost';
        $post->save();

        /*** push notification ***/
        $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
        $injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                $user = \App\User::where('id', $squad_member->user_id)->first();
                if($user->notification_priority_2 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('Document Deleted', Auth::user()->name . " deleted a document from " . $injured_employee->name . "'s claim.");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/

    }

}

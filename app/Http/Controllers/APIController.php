<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\PolicySubmission;

class APIController extends Controller
{
    public function getPolicyById(Request $request){

        // Check for policy paramater
        if (!isset($request->policy)) return response()->json(['error' => 'Missing Policy ID'], 400);

        //Find Policy

        $policy = PolicySubmission::where('id', '=', $request->policy)->first();

        //Ensure there is a policy

        if (!$policy) return response()->json(['error' => 'Policy not found!'], 400);

        //Filter policy data

        $policyFormatted = $policy;

        // Return Policy in nice format

        return response()->json(['policy' => $policyFormatted], 200);

    }
}

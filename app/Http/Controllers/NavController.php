<?php

namespace App\Http\Controllers;

use App\Injury;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Log;

class NavController extends Controller
{
    //
    function getLandingPage(){
        session()->forget('company_id');
        if(Auth::user()->injured === 1){
            if(count(Injury::where('injured_employee_id', Auth::user()->id)->get()) > 1){
                return redirect('/zengarden/zengardenInjuryList');
            }else{
                return redirect('/zengarden/home');
            }
        }else if(Auth::user()->is_zenpro){
            return redirect('/zenpro');
        }else if(Auth::user()->is_zagent){
            return redirect('/zagent');
        }else if(Auth::user()->type === "agent"){
            return redirect('/policyholders');
        }else return redirect('/zenboard');
    }

    function selectCompany($company_id){
        Log::debug('$company_id: ' . $company_id);
        session()->put('company_id', $company_id);
        Log::debug('session("company_id"): ' . session('company_id'));
        return redirect('/zenboard');
    }

    function selectCompanyPost(Request $request){
        $company_id = $request->company_id;
        session()->put('company_id', $company_id);
        return Response::json("/zenboard");
    }

    function getCompanyList(){
        if(Auth::user()->is_zagent){
            return redirect('/zagent');
        }else return view('pages.company_list');
    }

    public function getZenGarden(Request $request){
        $id = $request->injury_id;
        if(!is_null($id)){
            $request->session()->put('injury_id', $id);
            if(Auth::user()->type !== "internal"){
                $company_id = \App\Injury::where('id', $id)->value('company_id');
                $request->session()->put('company_id', $company_id);
            }
        }
        return redirect('/zengarden/home');
    }
}

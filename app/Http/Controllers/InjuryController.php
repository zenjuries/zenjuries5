<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\DeletedEmployeesEmails;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Injury;
use Illuminate\Support\Facades\Response;
use App\Repositories\CommunicationRepository;
use App\Repositories\InvitationRepository;
use App\Squad;
use App\SquadMember;
use App\ExternalUserCompanies;
use App\TeamUser;
use DB;
//use Laravel\Spark\Contracts\Interactions\Settings\Teams\SendInvitation;

class InjuryController extends Controller
{
    public function __construct(CommunicationRepository $repo, InvitationRepository $invitationRepo){
        $this->repo = $repo;
        $this->invitationRepo = $invitationRepo;
    }
    public function saveInjury(Request $request)
    {
        function base64_to_jpeg($base64_string, $injury_id, $imgCount, $fileLocation)
        {
            $data = explode(',', $base64_string);
            $imgFile = base64_decode($data[1]);
            Storage::put(
                //'/public/injuryUploads/injury_' . $injury_id . '_' . count($imgCount) . '.png',
                $fileLocation,
                $imgFile
            );
        }
        //**** GET THE INJURY DATA ****/
        //$company = \App\Company::where('id', $company_id)->first();
        //$injury_info = Request::input('injury');
       // $injury = saveInjury($injury_info, request::input);
        $injury_info = $request->injury;
        //EDITED TYLER
        $injury_info['company_id'] = Auth::user()->getCompany()->id;

        // reformat injury date
        // date time picker sends format    = '2022-04-30T23:05'
        // injury date format in DB         = '04-30-2022 11:05 pm'
        $formatted_date = Carbon::createFromFormat("Y-m-d\TG:i", $injury_info['date']);
        $injury_info['date'] = $formatted_date->format('m-d-Y g:i a');

        Log::debug(var_export($injury_info, true));
        $injury = new Injury();

        $injury->saveInjury($injury_info, Auth::user());
    }

    function savePhotoArray($photo_array, $user_id){
	    $new_upload_count = count($photo_array);
	    if($new_upload_count > 0){
		    $user_name = \App\User::where('id', $user_id)->value('name');
		    foreach($photo_array as $photo){
			    $image_count = count(\App\InjuryImage::where('injury_id', $this->id)->get());
			    $imageName = '/public/injuryUploads/injury_' . $this->id . '_' . $image_count . '.png';
			    $data = explode(',', $photo);
			    $img_file = base64_decode($data[1]);
			    Storage::put($imageName, $img_file);
			    $imageName = "/storage" . $imageName;
			    $injuryIMG = new \App\InjuryImage();
			    $injuryIMG->injury_id = $this->id;
			    $injuryIMG->name = "Injury Photo";
			    $injuryIMG->description = "Uploaded from Tree of Communications";
			    $injuryIMG->location = $imageName;
			    $injuryIMG->save();
	    	}
		    $post = new \App\TreePost();
		    $post->injury_id = $this->id;
		    $post->user_id = $user_id;
		    if($new_upload_count === 1){
			    $post->body = $user_name . " added a photo";
		    }else{
			    $post->body = $user_name . " added some photos";
		    }
		    $post->type = "photoPost";
		    $post->save();
	    }
    }

    function getSquadRoster(Request $request){
        //WE WANT ALL UNINJURED USERS AND ALL INJURED USERS CURRENTLY ON THAT COMPANY'S TEAM
        $companyID = $request->input('companyID');
        $userArray = collect();
        //get all employees of that company
        $employees = \App\User::where('company_id', $companyID)->whereNotNull('email')->orderBy('name', 'asc')
            ->select('id', 'name', 'email', 'phone', 'description', 'picture_location', 'avatar_color', 'uses_squads', 'injured', 'zagent_id', 'is_zagent', 'gets_priority_2_emails', 'gets_priority_3_emails', 'theme_id')
            ->get();
        foreach ($employees as $employee) {
            Log::debug($employee);
            if ($employee->uses_squads === 1) {
                //if they're uninjured, go ahead and add them to the list
                if ($employee->injured == 0) {
                    if(!$employee->theme_id){
                        $employee->theme_id = 1;
                    }
                    $valid = true;
                    foreach($userArray as $user){
                        if($user->id === $employee->id){
                            $valid = false;
                        }
                        //Log::debug("Comparing user_id: " . $user->id . " to employee_id: " . $employee->id);
                    }
                    if($valid === true){
                        $userArray->push($employee);
                        Log::debug($employee);
                    }
                    //$userArray->push($employee);
                    //if they are injured we have to find out if they're on a team belonging to that company
                } else if ($employee->injured == 1) {
                    $userInSquad = false;
                    //get all positions that user is filling
                    $userSquadPositions = \App\SquadMember::where('user_id', $employee->id)->get();
                    //make sure that position is in a squad belonging to that company
                    if (count($userSquadPositions)) {
                        //we'll make a list of the employees squads to tell the user which ones to edit.
                        $squadList = array();
                        foreach ($userSquadPositions as $position) {
                            $squad = \App\Squad::where('id', $position->squad_id)->first();
                            //if it is, we'll set userInSquad to true so we'll know to push them
                            if ($squad->company_id == $companyID) {
                                array_push($squadList, $squad->squad_name);
                                $userInSquad = true;
                            }
                        }
                        //If userInSquad is true, add them to the list
                        if ($userInSquad == true) {
                            $i = 0;
                            $employeeSquads = "";
                            $squadList = array_values(array_unique($squadList));
                            foreach ($squadList as $squadName) {
                                if ($i == 0) {
                                    $employeeSquads = $squadName;
                                } else {
                                    $employeeSquads = $employeeSquads . ", " . $squadName;
                                }
                                $i++;
                            }
                            $employee->squadString = $employeeSquads;
                            $userArray->push($employee);

                        }
                    }
                }
            }
        }
        //We'll repeat the process for external users
        $externalEmployees = \App\ExternalUserCompanies::where('company_id', $companyID)->get();

        foreach ($externalEmployees as $externalEmployee) {
            $valid = true;
            $emp = \App\User::where('id', $externalEmployee->user_id)
                ->select('id', 'name', 'email', 'phone', 'description', 'picture_location', 'avatar_color', 'uses_squads', 'injured', 'zagent_id', 'gets_priority_2_emails', 'gets_priority_3_emails')
                ->first();
            if ($emp->uses_squads === 1) {
                //array_push($employees, $emp);
                if ($emp->injured == 0) {
                    if ($emp->email !== NULL) {
                        foreach($userArray as $user){
                            if($user->id === $emp->id){
                                $valid = false;
                            }
                        }
                        if($valid){
                            $userArray->push($emp);
                        }
                    }
                } else if ($emp->injured == 1) {
                    $userInSquad = false;
                    //get all positions that user is filling
                    $userSquadPositions = \App\SquadMember::where('user_id', $emp->id)->get();
                    //make sure that position is in a squad belonging to that company
                    if (count($userSquadPositions)) {
                        //we'll make a list of the employees squads to tell the user which ones to edit.
                        $squadList = array();
                        foreach ($userSquadPositions as $position) {
                            $squad = \App\Squad::where('id', $position->squad_id)->first();
                            //if it is, we'll set userInSquad to true so we'll know to push them
                            if ($squad->company_id == $companyID) {
                                array_push($squadList, $squad->squad_name);
                                $userInSquad = true;
                            }
                        }
                        //If userInSquad is true, add them to the list
                        if ($userInSquad == true) {
                            $i = 0;
                            $employeeSquads = "";
                            $squadList = array_values(array_unique($squadList));
                            foreach ($squadList as $squadName) {
                                if ($i == 0) {
                                    $employeeSquads = $squadName;
                                } else {
                                    $employeeSquads = $employeeSquads . ", " . $squadName;
                                }
                                $i++;
                            }
                            $emp->squadString = $employeeSquads;
                            $userArray->push($emp);
                        }
                    }
                }
            }
        }
        //Log::debug(print_r($userArray, true));
        $userArray->sortBy('name');
        //format phone string
        foreach($userArray as $user){
            $user->phone = $user->getFormattedPhoneNumber();
            $user_name = explode(' ', $user->name);
            $user->first_name = strtok($user->name, " ");
            /*
            $user->first_name = $user_name[0];
            $user->last_name = $user_name[1];
            */
            //$user->company_squad_list = [];
            $company_squad_list = [];
            $user_squads = \App\SquadMember::where('user_id', $user->id)->groupBy('squad_id')->pluck('squad_id');
            foreach($user_squads as $squad_id){
	            $squad = \App\Squad::where('id', $squad_id)->where('company_id', $companyID)->select('id', 'squad_name', 'avatar_location')->first();
	            if(!is_null($squad)){
		            //$user->squads->push($squad);
		            array_push($company_squad_list, $squad);
	            }
            }
            $user->company_squad_list = $company_squad_list;
        }



        //sort the collection by name,
        // then use the values() method to reset the keys to consecutively numbered indexes
        /*
        $userArray = $userArray->sortByDesc('name');
        $userArray->values()->all();
        */
        return Response::json($userArray);
    }

    function getTeamList(Request $request){
        $company_id = $request->input('company_id');
        $squad_type = $request->input('squad_type');
        if($squad_type === "injury"){
            $squads = \App\Squad::where('company_id', $company_id)->where('injury_squad', 1)->where('hidden', 0)->get();
        }else{
            $squads = \App\Squad::where('company_id', $company_id)->where('injury_squad', 0)->get();
        }
        Log::debug($squads);
        foreach($squads as $squad){
	        $members = \App\SquadMember::where('squad_id', $squad->id)->select('user_id', 'position_id')->get();
	        $squad->members = $members;
        }
        return Response::json($squads);
    }

    function loadSquad(Request $request){
        Log::debug('in loadSquad');
        $squadID = $request->input('squadID');
        Log::debug('squad id: ' . $squadID);
        $squadMembers = \App\SquadMember::where('squad_id', $squadID)
            ->leftJoin('squads', 'squad_members.squad_id', '=', 'squads.id')
            ->leftJoin('users', 'squad_members.user_id', '=', 'users.id')
            ->select('squad_members.*', 'squads.squad_name', 'squads.avatar_location', 'squads.avatar_color', 'squads.background_color', 'squads.injury_squad', 'users.name', 'users.zengarden_theme')
            ->get();
        if(count($squadMembers)) {
            return Response::json($squadMembers);
        }else{
            $squadName = \App\Squad::where('id', $squadID)->value('squad_name');
            return Response::json(['emptySquad' => $squadName], 422);
        }
    }

    function postNewSquad(Request $request){
        //EDITED - TYLER
        if (Auth::user()->type == 'agent') {
            if(session('company_id')){
                $company_id = session('company_id');
            }else{
                $company_id = Auth::user()->getCompany()->id;
            }
        } else {
            $company_id = Auth::user()->getCompany()->id;
        }
        Log::debug('Company Id: ' . $company_id);
        $squad_array["chief_executive_array"] = $request->input('chiefExecutiveArray');
        $squad_array["doctor_array"] = $request->input('doctorArray');
        $squad_array["therapist_array"] = $request->input('therapistArray');
        $squad_array["claim_manager_array"] = $request->input('claimManagerArray');
        $squad_array["agent_array"] = $request->input('agentArray');
        $squad_array["customer_service_array"] = $request->input('customerServiceArray');
        $squad_array["safety_coordinator_array"] = $request->input('safetyCoordinatorArray');
        $squad_array["supervisor_array"] = $request->input('supervisorArray');
        $squad_array["chief_navigator_array"] = $request->input('chiefNavigatorArray');
        $squad_array["other_array"] = $request->input('otherArray');
        $squad_array["squad_name"] = $request->input('squadName');

        $chief_executive_position_id = \App\Position::where('name', 'Owner/Manager')->value('id');
        $doctor_position_id = \App\Position::where('name', 'Doctor\'s Office')->value('id');
        $therapist_position_id = \App\Position::where('name', 'Physical Therapist')->value('id');
        $claim_manager_position_id = \App\Position::where('name', 'Adjuster/Case Manager')->value('id');
        $agent_position_id = \App\Position::where('name', 'Insurance Agent')->value('id');
        $customer_service_position_id = \App\Position::where('name', 'Insurance Customer Service')->value('id');
        $safety_coordinator_position_id = \App\Position::where('name', 'Safety Coordinator')->value('id');
        $supervisor_position_id = \App\Position::where('name', 'Supervisor')->value('id');
        $chief_navigator_position_id = \App\Position::where('name', 'Human Resources')->value('id');
        $other_position_id = \App\Position::where('name', 'Other')->value('id');

        Log::debug(var_export($squad_array, true));

        $squad = new Squad();
        $squad->company_id = $company_id;
        $squad->squad_name = ($squad_array["squad_name"] !== "" ? $squad_array["squad_name"] : 'newSquad_' . Carbon::today()->toDateString());
        $squad->save();
        if(is_array($squad_array["chief_executive_array"])){
            foreach($squad_array["chief_executive_array"] as $chief_executive_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $chief_executive_position_id, $chief_executive_id);
                $this->repo->addedToSquadOwner($chief_executive_id, $company_id);
            }
        }
        if(is_array($squad_array["doctor_array"])){
            foreach($squad_array["doctor_array"] as $doctor_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $doctor_position_id, $doctor_id);
                $this->repo->addedToSquadDoctor($doctor_id, $company_id);
            }
        }
        if(is_array($squad_array["therapist_array"])){
            foreach($squad_array["therapist_array"] as $therapist_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $therapist_position_id, $therapist_id);
                $this->repo->addedToSquadPhysicalTherapist($therapist_id, $company_id);
            }
        }
        if(is_array($squad_array["claim_manager_array"])){
            foreach($squad_array["claim_manager_array"] as $claim_manager_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $claim_manager_position_id, $claim_manager_id);
                $this->repo->addedToSquadAdjuster($claim_manager_id, $company_id);
            }
        }
        if(is_array($squad_array["agent_array"])){
            foreach($squad_array["agent_array"] as $agent_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $agent_position_id, $agent_id);
                $this->repo->addedToSquadAgent($agent_id, $company_id);
            }
        }
        if(is_array($squad_array["customer_service_array"])){
            foreach($squad_array["customer_service_array"] as $customer_service_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $customer_service_position_id, $customer_service_id);
                $this->repo->addedToSquadCustomerService($customer_service_id, $company_id);
            }
        }
        if(is_array($squad_array["safety_coordinator_array"])){
            foreach($squad_array["safety_coordinator_array"] as $safety_coordinator_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $safety_coordinator_position_id, $safety_coordinator_id);
                $this->repo->addedToSquadSafetyCoordinator($safety_coordinator_id, $company_id);
            }
        }
        if(is_array($squad_array["supervisor_array"])){
            foreach($squad_array["supervisor_array"] as $supervisor_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $supervisor_position_id, $supervisor_id);
                $this->repo->addedToSquadSupervisor($supervisor_id, $company_id);
            }
        }
        if(is_array($squad_array["chief_navigator_array"])){
            foreach($squad_array["chief_navigator_array"] as $chief_navigator_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $chief_navigator_position_id, $chief_navigator_id);
                $this->repo->addedToSquadHumanResources($chief_navigator_id, $company_id);
            }
        }
        if(is_array($squad_array["other_array"])){
            foreach($squad_array["other_array"] as $other_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $other_position_id, $other_id);
                $this->repo->addedToSquadOther($other_id, $company_id);
            }
        }

        $squad->assignDefaultIcon();
        Log::debug('returning id');
        return $squad->id;
    }

    function postNewSquadOld(Request $request){
        //EDITED - TYLER
        if (Auth::user()->type == 'agent') {
            $company_id = session('company_id');
        } else {
            $company_id = Auth::user()->getCompany()->id;
        }

        $squad_array["chief_executive_array"] = $request->input('chiefExecutiveArray');
        $squad_array["doctor_array"] = $request->input('doctorArray');
        $squad_array["therapist_array"] = $request->input('therapistArray');
        $squad_array["claim_manager_array"] = $request->input('claimManagerArray');
        $squad_array["agent_array"] = $request->input('agentArray');
        $squad_array["customer_service_array"] = $request->input('customerServiceArray');
        $squad_array["safety_coordinator_array"] = $request->input('safetyCoordinatorArray');
        $squad_array["supervisor_array"] = $request->input('supervisorArray');
        $squad_array["chief_navigator_array"] = $request->input('chiefNavigatorArray');
        $squad_array["other_array"] = $request->input('otherArray');
        $squad_array["squad_name"] = $request->input('squadName');

        $icon = $request->input('avatar_location');
        $background_color = $request->input('background_color');
        $outline_color = $request->input('avatar_color');
        $icon_color = $request->input('icon_color');
        $avatar_number = $request->input('avatar_number');

        Log::debug(var_export($icon, true));
        Log::debug(var_export($background_color, true));
        Log::debug(var_export($outline_color, true));

        $chief_executive_position_id = \App\Position::where('name', 'Owner/Manager')->value('id');
        $doctor_position_id = \App\Position::where('name', 'Doctor\'s Office')->value('id');
        $therapist_position_id = \App\Position::where('name', 'Physical Therapist')->value('id');
        $claim_manager_position_id = \App\Position::where('name', 'Adjuster/Case Manager')->value('id');
        $agent_position_id = \App\Position::where('name', 'Insurance Agent')->value('id');
        $customer_service_position_id = \App\Position::where('name', 'Insurance Customer Service')->value('id');
        $safety_coordinator_position_id = \App\Position::where('name', 'Safety Coordinator')->value('id');
        $supervisor_position_id = \App\Position::where('name', 'Supervisor')->value('id');
        $chief_navigator_position_id = \App\Position::where('name', 'Human Resources')->value('id');
        $other_position_id = \App\Position::where('name', 'Other')->value('id');

        Log::debug(var_export($squad_array, true));

        $squad = new Squad();
        $squad->company_id = $company_id;
        $squad->squad_name = ($squad_array["squad_name"] !== "" ? $squad_array["squad_name"] : 'newSquad_' . Carbon::today()->toDateString());
        $squad->save();
        if(is_array($squad_array["chief_executive_array"])){
            foreach($squad_array["chief_executive_array"] as $chief_executive_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $chief_executive_position_id, $chief_executive_id);
                $this->repo->addedToSquadOwner($chief_executive_id, $company_id);
            }
        }
        if(is_array($squad_array["doctor_array"])){
            foreach($squad_array["doctor_array"] as $doctor_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $doctor_position_id, $doctor_id);
                $this->repo->addedToSquadDoctor($doctor_id, $company_id);
            }
        }
        if(is_array($squad_array["therapist_array"])){
            foreach($squad_array["therapist_array"] as $therapist_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $therapist_position_id, $therapist_id);
                $this->repo->addedToSquadPhysicalTherapist($therapist_id, $company_id);
            }
        }
        if(is_array($squad_array["claim_manager_array"])){
            foreach($squad_array["claim_manager_array"] as $claim_manager_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $claim_manager_position_id, $claim_manager_id);
                $this->repo->addedToSquadAdjuster($claim_manager_id, $company_id);
            }
        }
        if(is_array($squad_array["agent_array"])){
            foreach($squad_array["agent_array"] as $agent_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $agent_position_id, $agent_id);
                $this->repo->addedToSquadAgent($agent_id, $company_id);
            }
        }
        if(is_array($squad_array["customer_service_array"])){
            foreach($squad_array["customer_service_array"] as $customer_service_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $customer_service_position_id, $customer_service_id);
                $this->repo->addedToSquadCustomerService($customer_service_id, $company_id);
            }
        }
        if(is_array($squad_array["safety_coordinator_array"])){
            foreach($squad_array["safety_coordinator_array"] as $safety_coordinator_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $safety_coordinator_position_id, $safety_coordinator_id);
                $this->repo->addedToSquadSafetyCoordinator($safety_coordinator_id, $company_id);
            }
        }
        if(is_array($squad_array["supervisor_array"])){
            foreach($squad_array["supervisor_array"] as $supervisor_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $supervisor_position_id, $supervisor_id);
                $this->repo->addedToSquadSupervisor($supervisor_id, $company_id);
            }
        }
        if(is_array($squad_array["chief_navigator_array"])){
            foreach($squad_array["chief_navigator_array"] as $chief_navigator_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $chief_navigator_position_id, $chief_navigator_id);
                $this->repo->addedToSquadHumanResources($chief_navigator_id, $company_id);
            }
        }
        if(is_array($squad_array["other_array"])){
            foreach($squad_array["other_array"] as $other_id){
                $squad_member = new SquadMember();
                $squad_member->saveNewSquadMember($squad->id, $other_position_id, $other_id);
                $this->repo->addedToSquadOther($other_id, $company_id);
            }
        }


        $data = explode(',', $icon);
        $imgFile = base64_decode($data[1]);
        Storage::put('/public/teamIcons/team_' . $squad->id . '.' . 'png', $imgFile);
        //$imageName contains the location of the image to be stored in the DB
        $imageName = '/storage/public/teamIcons/team_' . $squad->id . '.' . 'png';
        $squad->avatar_location = $imageName;
        $squad->background_color = $background_color;
        $squad->avatar_color = $outline_color;
        $squad->avatar_number = $avatar_number;
        $squad->avatar_icon_color = $icon_color;
        $squad->save();

        return $squad->id;
    }

    function editSquad(Request $request){
        //set the company ID
        $company_id = Auth::user()->getCompany()->id;
        

        //get the squad variables. Each will contain an array of user.ids of users in that role
        $squad_array["chief_executive_array"] = $request->input('chiefExecutiveArray');
        $squad_array["doctor_array"] = $request->input('doctorArray');
        $squad_array["therapist_array"] = $request->input('therapistArray');
        $squad_array["claim_manager_array"] = $request->input('claimManagerArray');
        $squad_array["agent_array"] = $request->input('agentArray');
        $squad_array["customer_service_array"] = $request->input('customerServiceArray');
        $squad_array["safety_coordinator_array"] = $request->input('safetyCoordinatorArray');
        $squad_array["supervisor_array"] = $request->input('supervisorArray');
        $squad_array["chief_navigator_array"] = $request->input('chiefNavigatorArray');
        $squad_array["other_array"] = $request->input('otherArray');
        $squad_array["squad_name"] = $request->input('squadName');
        $squad_id = $request->input('squad_id');

        //get the ids of each different role/position that makes up the squad
        $chief_executive_position_id = \App\Position::where('name', 'Owner/Manager')->value('id');
        $doctor_position_id = \App\Position::where('name', 'Doctor\'s Office')->value('id');
        $therapist_position_id = \App\Position::where('name', 'Physical Therapist')->value('id');
        $claim_manager_position_id = \App\Position::where('name', 'Adjuster/Case Manager')->value('id');
        $agent_position_id = \App\Position::where('name', 'Insurance Agent')->value('id');
        $customer_service_position_id = \App\Position::where('name', 'Insurance Customer Service')->value('id');
        $safety_coordinator_position_id = \App\Position::where('name', 'Safety Coordinator')->value('id');
        $supervisor_position_id = \App\Position::where('name', 'Supervisor')->value('id');
        $chief_navigator_position_id = \App\Position::where('name', 'Human Resources')->value('id');
        $other_position_id = \App\Position::where('name', 'Other')->value('id');

        //get the squad we're editing
        $squad = \App\Squad::where('id', $squad_id)->first();
        //get a list of the current squad members
        $current_squad_members = \App\SquadMember::where('squad_id', $squad->id)->get();

        Log::debug($squad_array);
        
        //we'll loop through each current squad member and see if they're still on the team
        foreach($current_squad_members as $squad_member){
            $member_still_on_team = false;
            //loop through the new arrays for each role and see if the user's ID is in there
            if($squad_member->position_id === $chief_executive_position_id){
                if(is_array($squad_array["chief_executive_array"])){
                    foreach($squad_array["chief_executive_array"] as $chief_executive_id){
                        if($squad_member->user_id == $chief_executive_id){
                            //if the user ID matches one in the array, then they're still in that spot on the team
                            $member_still_on_team = true;
                        }
                    }
                }
            }else if($squad_member->position_id === $doctor_position_id){
                if(is_array($squad_array["doctor_array"])){
                    foreach($squad_array["doctor_array"] as $doctor_id){
                        if($squad_member->user_id == $doctor_id){
                            $member_still_on_team = true;
                        }
                    }
                }
            }else if($squad_member->position_id === $therapist_position_id){
                if(is_array($squad_array["therapist_array"])){
                    foreach($squad_array["therapist_array"] as $therapist_id){
                        if($squad_member->user_id == $therapist_id){
                            $member_still_on_team = true;
                        }
                    }
                }
            }else if($squad_member->position_id === $claim_manager_position_id){
                if(is_array($squad_array["claim_manager_array"])){
                    foreach($squad_array["claim_manager_array"] as $claim_manager_id){
                        if($squad_member->user_id == $claim_manager_id){
                            $member_still_on_team = true;
                        }
                    }
                }
            }else if($squad_member->position_id === $agent_position_id){
                if(is_array($squad_array["agent_array"])){
                    foreach($squad_array["agent_array"] as $agent_id){
                        if($squad_member->user_id == $agent_id){
                            $member_still_on_team = true;
                        }
                    }
                }
            }else if($squad_member->position_id === $customer_service_position_id){
                if(is_array($squad_array["customer_service_array"])){
                    foreach($squad_array["customer_service_array"] as $customer_service_id){
                        if($squad_member->user_id == $customer_service_id){
                            $member_still_on_team = true;
                        }
                    }
                }
            }else if($squad_member->position_id === $safety_coordinator_position_id){
                if(is_array($squad_array["safety_coordinator_array"])){
                    foreach($squad_array["safety_coordinator_array"] as $safety_coordinator_id){
                        if($squad_member->user_id == $safety_coordinator_id){
                            $member_still_on_team = true;
                        }
                    }
                }
            }else if($squad_member->position_id === $supervisor_position_id){
                if(is_array($squad_array["supervisor_array"])){
                    foreach($squad_array["supervisor_array"] as $supervisor_id){
                        if($squad_member->user_id == $supervisor_id){
                            $member_still_on_team = true;
                        }
                    }
                }
            }else if($squad_member->position_id === $chief_navigator_position_id){
                if(is_array($squad_array["chief_navigator_array"])){
                    foreach($squad_array["chief_navigator_array"] as $chief_navigator_id){
                        if($squad_member->user_id == $chief_navigator_id){
                            $member_still_on_team = true;
                        }
                    }
                }
            }else if($squad_member->position_id === $other_position_id){
                if(is_array($squad_array["other_array"])){
                    foreach($squad_array["other_array"] as $other_id){
                        if($squad_member->user_id == $other_id){
                            $member_still_on_team = true;
                        }
                    }
                }
            }
            //if that user is no longer filling that role on the team, delete them
            if($member_still_on_team === false){
                Log::debug('Deleting member');
                Log::debug('Squad Memeber: ' . $squad_member);
                $squad_member->delete();
            }

        }


            //loop through each new role array
            if(is_array($squad_array["chief_executive_array"])){
                foreach($squad_array["chief_executive_array"] as $chief_executive_id){
                    if($squad->injury_squad === 0){
                        //user_on_team checks if this user is already filling that role on the team
                        $user_on_team = \App\SquadMember::where('squad_id', $squad_id)
                            ->where('user_id', $chief_executive_id)
                            ->where('position_id', $chief_executive_position_id)->first();
                        //if they aren't, we'll save them
                        if(is_null($user_on_team)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $chief_executive_position_id, $chief_executive_id);
                            //$this->repo->addedToSquadOwner($chief_executive_id, $company_id);
                            $this->repo->handleAddedToSquadCommunication(false, "Owner", $chief_executive_id, $company_id);
                        }
                    }else{
                        //check if the user holds any positions on the injury team already
                        // (for example, if they're being added to multiple roles we only want the first one to trigger the email)
                        $existing_role = \App\SquadMember::where('user_id', $chief_executive_id)->where('squad_id', $squad_id)->first();
                        if(is_null($existing_role)){
                            $send_email = true;
                        }else{
                            $send_email = false;
                        }
                        $user_has_position = \App\SquadMember::where('user_id', $chief_executive_id)->where('squad_id', $squad_id)
                            ->where('position_id', $chief_executive_position_id)->first();
                        if(is_null($user_has_position)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $chief_executive_position_id, $chief_executive_id);
                        }
                        if($send_email === true){
                            $this->repo->handleAddedToSquadCommunication(true, "", $chief_executive_id, $company_id, $squad->id);
                        }
                    }

                }
            }

            if(is_array($squad_array["doctor_array"])){
                foreach($squad_array["doctor_array"] as $doctor_id){
                    if($squad->injury_squad === 0){
                        $user_on_team = \App\SquadMember::where('squad_id', $squad_id)
                            ->where('user_id', $doctor_id)
                            ->where('position_id', $doctor_position_id)->first();
                        if(is_null($user_on_team)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $doctor_position_id, $doctor_id);
                            //$this->repo->addedToSquadDoctor($doctor_id, $company_id);
                            $this->repo->handleAddedToSquadCommunication(false, "Doctor", $doctor_id, $company_id);
                        }
                    }else{
                        //check if the user holds any positions on the injury team already
                        // (for example, if they're being added to multiple roles we only want the first one to trigger the email)
                        $existing_role = \App\SquadMember::where('user_id', $doctor_id)->where('squad_id', $squad_id)->first();
                        if(is_null($existing_role)){
                            $send_email = true;
                        }else{
                            $send_email = false;
                        }
                        $user_has_position = \App\SquadMember::where('user_id', $doctor_id)->where('squad_id', $squad_id)
                            ->where('position_id', $doctor_position_id)->first();
                        if(is_null($user_has_position)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $doctor_position_id, $doctor_id);
                        }
                        if($send_email === true){
                            $this->repo->handleAddedToSquadCommunication(true, "", $doctor_id, $company_id, $squad->id);
                        }
                    }
                }
            }

            if(is_array($squad_array["therapist_array"])){
                foreach($squad_array["therapist_array"] as $therapist_id){
                    if($squad->injury_squad === 0){
                        $user_on_team = \App\SquadMember::where('squad_id', $squad_id)
                            ->where('user_id', $therapist_id)
                            ->where('position_id', $therapist_position_id)->first();
                        if(is_null($user_on_team)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $therapist_position_id, $therapist_id);
                            //$this->repo->addedToSquadPhysicalTherapist($therapist_id, $company_id);
                            $this->repo->handleAddedToSquadCommunication(false, "Physical Therapist", $therapist_id, $company_id);
                        }
                    }else{
                        //check if the user holds any positions on the injury team already
                        // (for example, if they're being added to multiple roles we only want the first one to trigger the email)
                        $existing_role = \App\SquadMember::where('user_id', $therapist_id)->where('squad_id', $squad_id)->first();
                        if(is_null($existing_role)){
                            $send_email = true;
                        }else{
                            $send_email = false;
                        }
                        $user_has_position = \App\SquadMember::where('user_id', $therapist_id)->where('squad_id', $squad_id)
                            ->where('position_id', $therapist_position_id)->first();
                        if(is_null($user_has_position)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $therapist_position_id, $therapist_id);
                        }
                        if($send_email === true){
                            $this->repo->handleAddedToSquadCommunication(true, "", $therapist_id, $company_id, $squad->id);
                        }
                    }

                }
            }

            if(is_array($squad_array["claim_manager_array"])){
                foreach($squad_array["claim_manager_array"] as $claim_manager_id){
                    if($squad->injury_squad === 0){
                        $user_on_team = \App\SquadMember::where('squad_id', $squad_id)
                            ->where('user_id', $claim_manager_id)
                            ->where('position_id', $claim_manager_position_id)->first();
                        if(is_null($user_on_team)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $claim_manager_position_id, $claim_manager_id);
                            //$this->repo->addedToSquadAdjuster($claim_manager_id, $company_id);
                            $this->repo->handleAddedToSquadCommunication(false, "Adjuster", $claim_manager_id, $company_id);
                        }
                    }else {
                        //check if the user holds any positions on the injury team already
                        // (for example, if they're being added to multiple roles we only want the first one to trigger the email)
                        $existing_role = \App\SquadMember::where('user_id', $claim_manager_id)->where('squad_id', $squad_id)->first();
                        if(is_null($existing_role)){
                            $send_email = true;
                        }else{
                            $send_email = false;
                        }
                        $user_has_position = \App\SquadMember::where('user_id', $claim_manager_id)->where('squad_id', $squad_id)
                            ->where('position_id', $claim_manager_position_id)->first();
                        if(is_null($user_has_position)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $claim_manager_position_id, $claim_manager_id);
                        }
                        if($send_email === true){
                            $this->repo->handleAddedToSquadCommunication(true, "", $claim_manager_id, $company_id, $squad->id);
                        }
                    }
                }
            }

            if(is_array($squad_array["agent_array"])){
                foreach($squad_array["agent_array"] as $agent_id){
                    if($squad->injury_squad === 0){
                        $user_on_team = \App\SquadMember::where('squad_id', $squad_id)
                            ->where('user_id', $agent_id)
                            ->where('position_id', $agent_position_id)->first();
                        if(is_null($user_on_team)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $agent_position_id, $agent_id);
                            //$this->repo->addedToSquadAgent($agent_id, $company_id);
                            $this->repo->handleAddedToSquadCommunication(false, "Insurance Agent", $agent_id, $company_id);
                        }
                    }else{
                        //check if the user holds any positions on the injury team already
                        // (for example, if they're being added to multiple roles we only want the first one to trigger the email)
                        $existing_role = \App\SquadMember::where('user_id', $agent_id)->where('squad_id', $squad_id)->first();
                        if(is_null($existing_role)){
                            $send_email = true;
                        }else{
                            $send_email = false;
                        }
                        $user_has_position = \App\SquadMember::where('user_id', $agent_id)->where('squad_id', $squad_id)
                            ->where('position_id', $agent_position_id)->first();
                        if(is_null($user_has_position)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $agent_position_id, $agent_id);
                        }
                        if($send_email === true){
                            $this->repo->handleAddedToSquadCommunication(true, "", $agent_id, $company_id, $squad->id);
                        }
                    }
                }
            }

            if(is_array($squad_array["customer_service_array"])){
                foreach($squad_array["customer_service_array"] as $customer_service_id){
                    if($squad->injury_squad === 0){
                        $user_on_team = \App\SquadMember::where('squad_id', $squad_id)
                            ->where('user_id', $customer_service_id)
                            ->where('position_id', $customer_service_position_id)->first();
                        if(is_null($user_on_team)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $customer_service_position_id, $customer_service_id);
                            //$this->repo->addedToSquadCustomerService($customer_service_id, $company_id);
                            $this->repo->handleAddedToSquadCommunication(false, "Insurance Customer Service", $customer_service_id, $company_id);
                        }
                    }else{
                        //check if the user holds any positions on the injury team already
                        // (for example, if they're being added to multiple roles we only want the first one to trigger the email)
                        $existing_role = \App\SquadMember::where('user_id', $customer_service_id)->where('squad_id', $squad_id)->first();
                        if(is_null($existing_role)){
                            $send_email = true;
                        }else{
                            $send_email = false;
                        }
                        $user_has_position = \App\SquadMember::where('user_id', $customer_service_id)->where('squad_id', $squad_id)
                            ->where('position_id', $customer_service_position_id)->first();
                        if(is_null($user_has_position)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $customer_service_position_id, $customer_service_id);
                        }
                        if($send_email === true){
                            $this->repo->handleAddedToSquadCommunication(true, "", $customer_service_id, $company_id, $squad->id);
                        }
                    }

                }
            }

            if(is_array($squad_array["safety_coordinator_array"])){
                foreach($squad_array["safety_coordinator_array"] as $safety_coordinator_id){
                    if($squad->injury_squad === 0){
                        $user_on_team = \App\SquadMember::where('squad_id', $squad_id)
                            ->where('user_id', $safety_coordinator_id)
                            ->where('position_id', $safety_coordinator_position_id)->first();
                        if(is_null($user_on_team)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $safety_coordinator_position_id, $safety_coordinator_id);
                            //$this->repo->addedToSquadSafetyCoordinator($safety_coordinator_id, $company_id);
                            $this->repo->handleAddedToSquadCommunication(false, "Safety Coordinator", $safety_coordinator_id, $company_id);
                        }
                    }else{
                        //check if the user holds any positions on the injury team already
                        // (for example, if they're being added to multiple roles we only want the first one to trigger the email)
                        $existing_role = \App\SquadMember::where('user_id', $safety_coordinator_id)->where('squad_id', $squad_id)->first();
                        if(is_null($existing_role)){
                            $send_email = true;
                        }else{
                            $send_email = false;
                        }
                        $user_has_position = \App\SquadMember::where('user_id', $safety_coordinator_id)->where('squad_id', $squad_id)
                            ->where('position_id', $safety_coordinator_position_id)->first();
                        if(is_null($user_has_position)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $safety_coordinator_position_id, $safety_coordinator_id);
                        }
                        if($send_email === true){
                            $this->repo->handleAddedToSquadCommunication(true, "", $safety_coordinator_id, $company_id, $squad->id);
                        }
                    }
                }
            }

            if(is_array($squad_array["supervisor_array"])){
                foreach($squad_array["supervisor_array"] as $supervisor_id){
                    if($squad->injury_squad === 0){
                        $user_on_team = \App\SquadMember::where('squad_id', $squad_id)
                            ->where('user_id', $supervisor_id)
                            ->where('position_id', $supervisor_position_id)->first();
                        if(is_null($user_on_team)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $supervisor_position_id, $supervisor_id);
                            //$this->repo->addedToSquadSupervisor($supervisor_id, $company_id);
                            $this->repo->handleAddedToSquadCommunication(false, "Supervisor", $supervisor_id, $company_id);
                        }
                    }else{
                        //check if the user holds any positions on the injury team already
                        // (for example, if they're being added to multiple roles we only want the first one to trigger the email)
                        $existing_role = \App\SquadMember::where('user_id', $supervisor_id)->where('squad_id', $squad_id)->first();
                        if(is_null($existing_role)){
                            $send_email = true;
                        }else{
                            $send_email = false;
                        }
                        $user_has_position = \App\SquadMember::where('user_id', $supervisor_id)->where('squad_id', $squad_id)
                            ->where('position_id', $supervisor_position_id)->first();
                        if(is_null($user_has_position)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $supervisor_position_id, $supervisor_id);
                        }
                        if($send_email === true){
                            $this->repo->handleAddedToSquadCommunication(true, "", $supervisor_id, $company_id, $squad->id);
                        }
                    }
                }
            }

            if(is_array($squad_array["chief_navigator_array"])){
                foreach($squad_array["chief_navigator_array"] as $chief_navigator_id){
                    if($squad->injury_squad === 0){
                        $user_on_team = \App\SquadMember::where('squad_id', $squad_id)
                            ->where('user_id', $chief_navigator_id)
                            ->where('position_id', $chief_navigator_position_id)->first();
                        if(is_null($user_on_team)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $chief_navigator_position_id, $chief_navigator_id);
                            //$this->repo->addedToSquadHumanResources($chief_navigator_id, $company_id);
                            $this->repo->handleAddedToSquadCommunication(false, "Human Resources", $chief_navigator_id, $company_id);
                        }
                    }else{
                        //check if the user holds any positions on the injury team already
                        // (for example, if they're being added to multiple roles we only want the first one to trigger the email)
                        $existing_role = \App\SquadMember::where('user_id', $chief_navigator_id)->where('squad_id', $squad_id)->first();
                        if(is_null($existing_role)){
                            $send_email = true;
                        }else{
                            $send_email = false;
                        }
                        $user_has_position = \App\SquadMember::where('user_id', $chief_navigator_id)->where('squad_id', $squad_id)
                            ->where('position_id', $chief_navigator_position_id)->first();
                        if(is_null($user_has_position)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $chief_navigator_position_id, $chief_navigator_id);
                        }
                        if($send_email === true){
                            $this->repo->handleAddedToSquadCommunication(true, "", $chief_navigator_id, $company_id, $squad->id);
                        }
                    }

                }
            }

            if(is_array($squad_array["other_array"])){
                foreach($squad_array["other_array"] as $other_id){
                    if($squad->injury_squad === 0){
                        $user_on_team = \App\SquadMember::where('squad_id', $squad_id)
                            ->where('user_id', $other_id)
                            ->where('position_id', $other_position_id)->first();
                        if(is_null($user_on_team)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $other_position_id, $other_id);
                            //$this->repo->addedToSquadOther($other_id, $company_id);
                            $this->repo->handleAddedToSquadCommunication(false, "Other", $other_id, $company_id);
                        }
                    }else{
                        //check if the user holds any positions on the injury team already
                        // (for example, if they're being added to multiple roles we only want the first one to trigger the email)
                        $existing_role = \App\SquadMember::where('user_id', $other_id)->where('squad_id', $squad_id)->first();
                        if(is_null($existing_role)){
                            $send_email = true;
                        }else{
                            $send_email = false;
                        }
                        $user_has_position = \App\SquadMember::where('user_id', $other_id)->where('squad_id', $squad_id)
                            ->where('position_id', $other_position_id)->first();
                        if(is_null($user_has_position)){
                            $squad_member = new SquadMember();
                            $squad_member->saveNewSquadMember($squad_id, $other_position_id, $other_id);
                        }
                        if($send_email === true){
                            $this->repo->handleAddedToSquadCommunication(true, "", $other_id, $company_id, $squad->id);
                        }
                    }
                }
            }

            $squad->squad_name = $squad_array["squad_name"];
            $squad->save();
    }

    function pingAgent(Request $request){
        //TODO: Add Ping Agent
    }

    function removeUser(Request $request){
        $userID = $request->input('user_id');
        $companyID = $request->input('company_id');
        Log::debug('userID: ' . $userID);
        Log::debug('companyID: ' . $companyID);
        $user = \App\User::where('id', $userID)->first();
        //** DETERMINE IF THE USER IS ON ANY OF THAT COMPANY'S TEAMS **/
        $user_squads = DB::table('squad_members')
            ->leftJoin('squads', 'squad_members.squad_id', '=', 'squads.id')
            ->where('squad_members.user_id', $userID)
            ->where('squads.company_id', $companyID)
            ->select('squads.id', 'squads.squad_name')
            ->get();

        Log::debug('User Squads');
        Log::debug(print_r($user_squads, true));
        if($user_squads === []){
            Log::debug('Deleting user === []');
        }else if(empty($user_squads)){
            Log::debug('Deleting user === empty');
        }else{
            Log::debug('NOT Deleting user === [?]');
        }
        //** IF THEY ARE NOT, CONTINUE WITH DELETION **/
        if (count($user_squads) === 0) {
            /** IF THEY'RE AN INTERNAL USER ONLY BELONGING TO THAT COMPANY, JUST DELETE THEM (UNLESS THEY'RE THE OWNER) **/
            if ($user->type === "internal") {
                $team_id = \App\Company::where('id', $companyID)->value('team_id');
                $team_user = \App\TeamUser::where('team_id', $team_id)->where('user_id', $user->id)->first();
                /** IF THEY ARE THE OWNER, RETURN AN ERROR **/
                if ($team_user->role === "owner") {
                    return Response::json(['error' => "You can't delete the zenjuries account owner!"], 422);
                    /** IF THEY'RE NOT, WE'LL STORE THEIR EMAIL AND THEN SOFT-DELETE THEIR USER RECORD **/
                } else {
                    //store email
                    $deleted_email = new DeletedEmployeesEmails();
                    $deleted_email->user_id = $user->id;
                    $deleted_email->email = $user->email;
                    $deleted_email->save();
                    //delete team_user record
                    $team_user = \App\TeamUser::where('team_id', $team_id)->where('user_id', $user->id)->delete();
                    //null users email and then soft delete them
                    //nulling the email allows them to register again in the future with the same email
                    $user->email = NULL;
                    $user->save();
                    $user->delete();
                }
            } else {
                /** IF THEY'RE NOT AN EXTERNAL USER, AND NOT A ZAGENT, WE JUST REMOVE THEIR LINK TO THE COMPANY DELETING THEM **/
                $team_id = \App\Company::where('id', $companyID)->value('team_id');
                $external_user_link = \App\ExternalUserCompanies::where('user_id', $user->id)->where('company_id', $companyID)->delete();
                $team_link = \App\TeamUser::where('user_id', $userID)->where('team_id', $team_id)->delete();
            }
        } else {
            /** IF THEY'RE PART OF THAT COMPANY'S TEAMS, WE SENT A MESSAGE TELLING THEM TO REMOVE THEM FROM TEAMS BEFORE DELETION **/
            $responseMessage = "This user is currently a member of the following teams: <br><ul>";
            $squad_id_array = array();
            foreach ($user_squads as $squad) {
                if (!in_array($squad->id, $squad_id_array, true)) {
                    //$squad_name = \App\Squad::where('id', $squad->squad_members.squad_id)->value('name');
                    $responseMessage = $responseMessage . "<li>" . $squad->squad_name . "</li>";
                    array_push($squad_id_array, $squad->id);
                }
            }
            $responseMessage = $responseMessage . "</ul>Please remove them from all teams before deleting them.";
            /** RETURN MESSAGE ASKING THEM TO REMOVE THE USER FROM TEAMS BEFORE DELETION **/
            return Response::json(['error' => $responseMessage], 422);
        }
        /** ASSUMING EVERYTHING WENT ACCORDING TO PLAN AND THE USER WAS DELETED, RETURN A SUCCESS MESSAGE **/
        return Response::json('User Deleted!');
    }

    function deleteZagentZuser(Request $request){
        $user_id = $request->input('user_id');
        $company_id = $request->input('company_id');
        $user_zagent = \App\User::where('id', $user_id)->first();
        if ($user_zagent->zagent_id == $company_id) {
            //find out if they're on any client teams
            $user_squads = DB::table('squad_members')
                ->leftJoin('squads', 'squad_members.squad_id', '=', 'squads.id')
                ->leftJoin('companies', 'companies.id', '=', 'squads.company_id')
                ->where('squad_members.user_id', $user_id)
                ->select('squads.id', 'squads.squad_name', 'companies.company_name')
                ->get();
            //return Response::json($user_squads);
            Log::debug(print_r($user_squads, true));
            if (count($user_squads) > 0) {
                Log::debug(count($user_squads));
                //remove duplicates from the array (currently the array contains an entry for each position the user holds)
                $user_squads = array_unique($user_squads, SORT_REGULAR);
                $user_squads = array_values($user_squads);
                //return the individual teams the user is a part of
                return Response::json($user_squads, 422);
            } else {
                //remove them from companies other than their agency
                $companies = \App\ExternalUserCompanies::where('user_id', $user_id)->delete();
                //update user record
                $user = \App\User::where('id', $user_id)->first();
                $user->is_zagent = 0;
                $user->zagent_id = NULL;
                //store email
                $deleted_email = new DeletedEmployeesEmails();
                $deleted_email->user_id = $user->id;
                $deleted_email->email = $user->email;
                $deleted_email->save();
                //delete team_user record
                $team_user = \App\TeamUser::where('team_id', $user->team_id)->where('user_id', $user->id)->delete();
                //null users email and then soft delete them
                //nulling the email allows them to register again in the future with the same email
                $user->email = NULL;
                $user->save();
                $user->delete();
            }
        }
    }

    function replaceZagentZuser(Request $request){
         //the agent who's being deleted
         $old_user_id = $request->input('old_user_id');
         //the agent who's replacing him
         $replacement_user_id = $request->input('new_user_id');
         //company id of ZAgent
         $zagent_id = \App\User::where('id', $replacement_user_id)->value('zagent_id');
         //get the positions that the old agent filled
         $old_user_roles = \App\SquadMember::where('user_id', $old_user_id)
             ->leftJoin('squads', 'squad_members.squad_id', '=', 'squads.id')
             ->select('squad_members.user_id', 'squad_members.position_id', 'squads.company_id', 'squads.id')
             ->get();
         //for each of these, we'll create a new one with the new agent replacing them.
         foreach ($old_user_roles as $role) {
             $company_id = $role->company_id;
             //the other agent should already be a part of these companies, but just in case they're not...
             $external_link = \App\ExternalUserCompanies::where('user_id', $replacement_user_id)->where('company_id', $company_id)->first();
             if (is_null($external_link)) {
                 $external_link = new ExternalUserCompanies();
                 $external_link->user_id = $replacement_user_id;
                 $external_link->company_id = $company_id;
                 $external_link->save();
             }
             //
             $replacement_squad_member = new SquadMember();
             $replacement_squad_member->squad_id = $role->id;
             $replacement_squad_member->position_id = $role->position_id;
             $replacement_squad_member->user_id = $replacement_user_id;
             $replacement_squad_member->save();
         }
         //now the replaced agent's roles must be deleted;
         $deleted_roles = \App\SquadMember::where('user_id', $old_user_id)->delete();
         //notify the replacement
         $this->repo->replacementZAgentZUser($replacement_user_id, $zagent_id);
    }

    function getZagentList(Request $request){
        $company_id = $request->input('company_id');
        $zagent_zusers = \App\User::where('zagent_id', $company_id)->select('id', 'name')->get();
        return Response::json($zagent_zusers);
    }

    function addUser(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);
        Log::debug('In add user');
        //get the company ID
        $company = Auth::user()->getCompany();
        $company_id = $company->id;
        //get variables
        $name = $request->input('name');
        $name = ucwords($name);
        $email = $request->input('email');

        //$company = \App\Company::where('id', $company_id)->first();
        $team = \App\Team::where('id', $company->team_id)->first();

        $user = \App\User::where('email', $email)->first();

        //email address is new to the system
        if(is_null($user)){
            //$this->invitationRepo->($team, $email, 'internal');
            $this->invitationRepo->inviteNewUser($name, $email, $company_id);
        }else{
            //check if the user is currently in this company
            $user_in_company = false;
            if($user->company_id !== NULL && $user->company_id == $company_id){
                $user_in_company = true;
            }
            if(\App\ExternalUserCompanies::where('user_id', $user->id)->where('company_id', $company_id)->first() !== NULL){
                $user_in_company = true;
            }
            //User is already a part of the current company
            /*
            Log::debug(var_export($user_in_company, true));
            if($user_in_company === false){
                Log::debug(var_export("company_id = " . $company_id, true));
                Log::debug(var_export("user_company_id = " . $user->company_id, true));
            }
            */
            if($user_in_company === true){
                if($user->uses_squads === 0){
                    $user->uses_squads = 1;
                    $user->save();
                    return Response::json("User Added!");
                }else{
                    return Response::json(['error' => 'Your Teammate ' . $user->name . " is already using that email!"], 422);
                }
            }else{
                //$this->invitationRepo->handle($team, $email, 'agent');
                $this->invitationRepo->inviteNewUser($name, $email, $company_id, 1, "agent");
                $companyConnecter = new ExternalUserCompanies();
                $companyConnecter->user_id = $user->id;
                $companyConnecter->company_id = $company_id;
                $companyConnecter->save();
                $teamUser = new TeamUser();
                $teamUser->team_id = $team->id;
                $teamUser->user_id = $user->id;
                $teamUser->role = 'agent';
                $teamUser->save();
                if ($user->type !== 'agent') {
                    if ($user->company_id !== NULL) {
                        $companyConnecter = new ExternalUserCompanies();
                        $companyConnecter->user_id = $user->id;
                        $companyConnecter->company_id = $user->company_id;
                        $companyConnecter->save();
                    }
                    $user->type = 'agent';
                    $user->company_id = NULL;
                    $user->uses_squads = 1;
                    $user->save();
                }
            }
        }
        return Response::json("User Added!");
    }

    function renameSquad(Request $request){
        $squad_id = $request->input('squad_id');
        $squad_name = $request->input('squad_name');
        $squad = \App\Squad::where('id', $squad_id)->first();
        $squad->squad_name = $squad_name;
        $squad->save();
        return $squad->squad_name;
    }

    function updateSquadImage(Request $request){
        $squad_id = $request->input('squad_id');
        $img64 = $request->input('image');
        $outline_color = $request->input('outline_color');
        $background_color = $request->input('background_color');
        $icon_color = $request->input('icon_color');
        $avatar_number = $request->input('avatar_number');
        $data = explode(',', $img64);
        $imgFile = base64_decode($data[1]);
        $squad = \App\Squad::where('id', $squad_id)->first();

        if($squad->avatar_times_changed % 2 == 0){
            $trailing_character = "_0";
        }else{
            $trailing_character = "_1";
        }
        Storage::put('/public/teamIcons/team_' . $squad_id . $trailing_character . '.' . 'png', $imgFile);
        //get the user to store the image

        //$imageName contains the location of the image to be stored in the DB
        $imageName = '/storage/public/teamIcons/team_' . $squad_id . $trailing_character . '.' . 'png';
        $squad->avatar_location = $imageName;
        $squad->background_color = $background_color;
        $squad->avatar_color = $outline_color;
        $squad->avatar_number = $avatar_number;
        $squad->avatar_icon_color = $icon_color;
        $squad->avatar_times_changed = $squad->avatar_times_changed + 1;
        $squad->save();
        return $imageName;
    }

    function deleteSquad(Request $request){
        $squad_id = $request->input('squad_id');

        $squad = \App\Squad::where('id', $squad_id)->first();
        if($squad->injury_squad === 1){
            return Response::json("Sorry, that team is being used for an injury and cannot be deleted.", 422);
        }
        $squad_injury = \App\Injury::where('squad_id', $squad_id)->first();
        if(!is_null($squad_injury)){
            return Response::json("Sorry, that team is being used for an injury and cannot be deleted.", 422);
        }
        $squad_count = count(\App\Squad::where('company_id', $squad->company_id)->where('injury_squad', 0)->get());
        if($squad_count < 2){
            return Response::json("Sorry, but this is your company's only team! You must have at least one.", 422);
        }


        $squad_members = \App\SquadMember::where('squad_id', $squad_id)->get();
        foreach($squad_members as $member){
            $member->delete();
        }
        $squad->delete();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Repositories\ActiveCollabRepository;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;



class ActiveCollabController extends Controller
{

    public function __construct(ActiveCollabRepository $repo)
    {
        $this->repo = $repo;
    }

    public function postNewTask(Request $request){
        //$file = $request->file;
        $label = $request->label;
        $taskName = $request->taskName;
        $name = $request->name;
        $company = $request->company;
        $body = $request->body;
        $this->repo->postNewTask($taskName, $name, $body, $company, $label);
        //if(Auth::user()->injured == 0){
        //    return redirect('/mySupport');
        //}else{
        //    return redirect('/zengarden/support');
        //}
    }
}


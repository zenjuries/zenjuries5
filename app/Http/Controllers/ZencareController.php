<?php

namespace App\Http\Controllers;

use App\Injury;
use App\InjuryFile;
use App\InjuryImage;
use App\User;
use App\Repositories\CommunicationRepository;
use App\SquadMember;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\TreePost;
use App\MedicalCosts;
use App\LegalCosts;
use App\IndemnityCosts;
use App\MiscellaneousCosts;
use App\ReserveMedicalCost;
use App\ReserveLegalCost;
use App\ReserveMiscellaneousCost;
use App\ReserveIndemnityCost;
use App\DeviceId;
use App\Services\FCMService;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use App\SummaryPost;


class ZencareController extends Controller
{
    public function __construct(CommunicationRepository $comm_repo)
    {
        $this->repo = $comm_repo;
    }

    //TODO:: CLEANUP
    function getInjuryList(Request $request){
        $company_id = $request->company_id;
        $user_teams = SquadMember::where('user_id', Auth::user()->id)->pluck('squad_id');

        if(Auth::user()->is_zenpro === 1){
            $injuries = DB::table('injuries')->where('injuries.company_id', $company_id)->where('injuries.hidden', 0)
                ->leftJoin('users', 'injuries.injured_employee_id', '=', 'users.id')
                ->select('injuries.*', 'users.name as user_name', 'users.email as user_email', 'users.picture_location as user_pic_location', 'users.avatar_color as user_avatar_color')
                ->get();
        }else{
            $injuries = DB::table('injuries')->where('injuries.company_id', $company_id)->where('injuries.hidden', 0)
                ->whereIn('squad_id', $user_teams)
                ->leftJoin('users', 'injuries.injured_employee_id', '=', 'users.id')
                //->orderBy()
                ->select('injuries.*', 'users.name as user_name', 'users.email as user_email', 'users.picture_location as user_pic_location', 'users.avatar_color as user_avatar_color')
                ->get();
        }

        foreach($injuries as $injury){
            //02-27-2018 2:13 pm
            //$injury->injury_date = Carbon::createFromFormat("m-d-Y h:i a", $injury->injury_date)->format("d/m/Y");
            $injury->injury_date = Carbon::createFromFormat("m-d-Y h:i a", $injury->injury_date)->format("m/d/Y");

            if($injury->resolved === 1){
                $injury->current_cost = $injury->final_cost;
            }else{
                $medical_cost = \App\MedicalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                if(is_null($medical_cost)){$medical_cost = 0;}

                $legal_cost = \App\LegalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                if(is_null($legal_cost)){$legal_cost = 0;}

                $indemnity_cost = \App\IndemnityCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                if(is_null($indemnity_cost)){$indemnity_cost = 0;}

                $misc_cost = \App\MiscellaneousCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                if(is_null($misc_cost)){$misc_cost = 0;}

                $injury->current_cost = $medical_cost + $legal_cost + $indemnity_cost + $misc_cost;
            }

            if(is_null($injury->current_cost)){
                $injury->current_cost = 0;
            }

            $reserve_medical_cost = \App\ReserveMedicalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if(is_null($reserve_medical_cost)){$reserve_medical_cost = 0;}
            $reserve_legal_cost = \App\ReserveLegalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if(is_null($reserve_legal_cost)){$reserve_legal_cost = 0;}
            $reserve_indemnity_cost = \App\ReserveIndemnityCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if(is_null($reserve_indemnity_cost)){$reserve_indemnity_cost = 0;}
            $reserve_misc_cost = \App\ReserveMiscellaneousCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if(is_null($reserve_misc_cost)){$reserve_misc_cost = 0;}

            $injury->reserve_cost = $reserve_medical_cost + $reserve_legal_cost + $reserve_indemnity_cost + $reserve_misc_cost;

            $injury->mood = \App\DiaryPost::where('injury_id', $injury->id)->orderBy('created_at', 'desc')->value('mood');

            //status
            if(!is_null($injury->employee_status)){

                if($injury->employee_status === "Hospital"){
                    $injury->status_number = 1;
                }else if($injury->employee_status === "Urgent Care"){
                    $injury->status_number = 2;
                }else if($injury->employee_status === "Family Practice"){
                    $injury->status_number = 3;
                }else if($injury->employee_status === "Home"){
                    $injury->status_number = 4;
                }else if($injury->employee_status === "Light Duty"){
                    $injury->status_number = 5;
                }else if($injury->employee_status === "Full Duty"){
                    $injury->status_number = 6;
                }else if($injury->employee_status === "Maximum Medical Improvement"){
                    $injury->status_number = 7;
                }else if($injury->employee_status === "Resigned"){
                    $injury->status_number = 8;
                }else if($injury->employee_status === "Terminated"){
                    $injury->status_number = 9;
                }else{
                    $injury->status_number = 1;
                }
            }else{
                $injury->status_number = 1;
            }

            $injury_alert = \App\FirstReportOfInjury::where('injury_id', $injury->id)->first();

            if(!is_null($injury_alert)){
                $injury->injury_alert_delivered = $injury_alert->action_taken;
                $injury->first_report_id = $injury_alert->id;
            }else{
                $injury->injury_alert_delivered = 0;
                $injury->first_report_id = NULL;
            }

            $injury->days = Carbon::now()->diffInDays($injury->created_at);

        }

        return Response::json($injuries);
    }

    function getTreePosts(Request $request){
        $injury = Injury::where('id', $request->injury_id)->first();
        $posts = $injury->getTreePosts();
        return Response::json($posts);
    }

    function getRecentPosts(Request $request){
        $injury_id = $request->injury_id;
        $posts = SummaryPost::latest()->where('injury_id', $injury_id)->take(3)->get();
        foreach($posts as $post){
            $user =  User::where('id', $post->user_id)->select('name', 'photo_url')->first();
            if($post->type === "diaryPost" || $post->type === "chatPost"){
                $post->profile_image = $user->photo_url;
            }else{
                $post->profile_image = '';
            }
            if(!is_null($user)){
                $post->user_id = $user->name;
            }
            $post->formatted_date = Carbon::createFromFormat("Y-m-d H:i:s", $post->created_at)->toDateTimeString();

            //defualt post title to the type of the post
            $post->title = $post->type;
        }

        return Response::json($posts);
    }

    function newTreePost(Request $request){
        //injury_id, user_id, body

        $post = new TreePost();
        $post_id = $post->newChatPost($request->injury_id, $request->user_id, $request->body);

        $user = \App\User::where('id', $request->user_id)->first();
        $summary = new SummaryPost();
        $summary->createSummary($request->injury_id, $user->id, $user->name . " sent a new chat post.", "chatPost");

        $injury = \App\Injury::where('id', $request->injury_id)->first();
        $today = Carbon::today();

        $this->repo->newTreePost($request->injury_id, $post_id, $request->user_id);
        $injury->last_tree_post_email_sent_on = $today;
        $injury->save();
    }

    function updateClaimCosts(Request $request){
        $injury_data = [];
        $injury_data['injury_id'] = $request->injury_id;
        $injury_data['paid_medical'] = $request->paid_medical;
        $injury_data['paid_legal'] = $request->paid_legal;
        $injury_data['paid_indemnity'] = $request->paid_indemnity;
        $injury_data['paid_miscellaneous'] = $request->paid_misc;
        $injury_data['reserve_medical'] = $request->reserve_medical;
        $injury_data['reserve_legal'] = $request->reserve_legal;
        $injury_data['reserve_indemnity'] = $request->reserve_indemnity;
        $injury_data['reserve_miscellaneous'] = $request->reserve_misc;

        $injury = Injury::where('id', $request->injury_id)->first();
        $injury_id = $injury->id;
        $user_id = Auth::user()->id;
        $user = Auth::user();

        if ($injury_data['paid_medical'] === "" || is_numeric($injury_data['paid_medical']) === false) {
            $injury_data['paid_medical'] = \App\MedicalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if ($injury_data['paid_medical'] === NULL) {
                $injury_data['paid_medical'] = 0;
            }
        } else {
            $injury_data['paid_medical'] = round($injury_data['paid_medical']);
        }

        if ($injury_data['paid_legal'] === "" || is_numeric($injury_data['paid_legal']) === false) {
            $injury_data['paid_legal'] = \App\LegalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if ($injury_data['paid_legal'] === NULL) {
                $injury_data['paid_legal'] = 0;
            }
        } else {
            $injury_data['paid_legal'] = round($injury_data['paid_legal']);
        }

        if ($injury_data['paid_indemnity'] === "" || is_numeric($injury_data['paid_indemnity']) === false) {
            $injury_data['paid_indemnity'] = \App\IndemnityCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if ($injury_data['paid_indemnity'] === NULL) {
                $injury_data['paid_indemnity'] = 0;
            }
        } else {
            $injury_data['paid_indemnity'] = round($injury_data['paid_indemnity']);
        }

        if ($injury_data['paid_miscellaneous'] === "" || is_numeric($injury_data['paid_miscellaneous']) === false) {
            $injury_data['paid_miscellaneous'] = \App\MiscellaneousCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if ($injury_data['paid_miscellaneous'] === NULL) {
                $injury_data['paid_miscellaneous'] = 0;
            }
        } else {
            $injury_data['paid_miscellaneous'] = round($injury_data['paid_miscellaneous']);
        }

        if ($injury_data['reserve_medical'] === "" || is_numeric($injury_data['reserve_medical']) === false) {
            $injury_data['reserve_medical'] = \App\ReserveMedicalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if ($injury_data['reserve_medical'] === NULL) {
                $injury_data['reserve_medical'] = 0;
            }
        } else {
            $injury_data['reserve_medical'] = round($injury_data['reserve_medical']);
        }

        if ($injury_data['reserve_legal'] === "" || is_numeric($injury_data['reserve_legal']) === false) {
            $injury_data['reserve_legal'] = \App\ReserveLegalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if ($injury_data['reserve_legal'] === NULL) {
                $injury_data['reserve_legal'] = 0;
            }
        } else {
            $injury_data['reserve_legal'] = round($injury_data['reserve_legal']);
        }

        if ($injury_data['reserve_indemnity'] === "" || is_numeric($injury_data['reserve_indemnity']) === false) {
            $injury_data['reserve_indemnity'] = \App\ReserveIndemnityCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if ($injury_data['reserve_indemnity'] === NULL) {
                $injury_data['reserve_indemnity'] = 0;
            }
        } else {
            $injury_data['reserve_indemnity'] = round($injury_data['reserve_indemnity']);
        }

        if ($injury_data['reserve_miscellaneous'] === "" || is_numeric($injury_data['reserve_miscellaneous']) === false) {
            $injury_data['reserve_miscellaneous'] = \App\ReserveMiscellaneousCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if ($injury_data['reserve_miscellaneous'] === NULL) {
                $injury_data['reserve_miscellaneous'] = 0;
            }
        } else {
            $injury_data['reserve_miscellaneous'] = round($injury_data['reserve_miscellaneous']);
        }

        if ($injury_data['paid_medical'] !== 0 || $injury_data['paid_legal'] !== 0 || $injury_data['paid_indemnity'] !== 0 || $injury_data['paid_miscellaneous'] ||
            $injury_data['reserve_medical'] !== 0 || $injury_data['reserve_legal'] !== 0 || $injury_data['reserve_indemnity'] !== 0 || $injury_data['reserve_miscellaneous'] !== 0) {


            $paid_medical = new MedicalCosts();
            $paid_medical_prev_val = $paid_medical->updateCost($user_id, $injury_data['injury_id'], $injury_data['paid_medical']);

            $paid_legal = new LegalCosts();
            $paid_legal_prev_val = $paid_legal->updateCost($user_id, $injury_data['injury_id'], $injury_data['paid_legal']);

            $paid_indemnity = new IndemnityCosts();
            $paid_indemnity_prev_val = $paid_indemnity->updateCost($user_id, $injury_data['injury_id'], $injury_data['paid_indemnity']);

            $paid_miscellaneous = new MiscellaneousCosts();
            $paid_miscellaneous_prev_val = $paid_miscellaneous->updateCost($user_id, $injury_data['injury_id'], $injury_data['paid_miscellaneous']);

            $reserve_medical = new ReserveMedicalCost();
            $reserve_medical_prev_val = $reserve_medical->updateCost($user_id, $injury_data['injury_id'], $injury_data['reserve_medical']);

            $reserve_legal = new ReserveLegalCost();
            $reserve_legal_prev_val = $reserve_legal->updateCost($user_id, $injury_data['injury_id'], $injury_data['reserve_legal']);

            $reserve_indemnity = new ReserveIndemnityCost();
            $reserve_indemnity_prev_val = $reserve_indemnity->updateCost($user_id, $injury_data['injury_id'], $injury_data['reserve_indemnity']);

            $reserve_miscellaneous = new ReserveMiscellaneousCost();
            $reserve_miscellaneous_prev_val = $reserve_miscellaneous->updateCost($user_id, $injury_data['injury_id'], $injury_data['reserve_miscellaneous']);

            if ($paid_medical_prev_val !== false || $paid_legal_prev_val !== false || $paid_indemnity_prev_val !== false || $paid_miscellaneous_prev_val !== false
                || $reserve_medical_prev_val !== false || $reserve_legal_prev_val !== false || $reserve_indemnity_prev_val !== false || $reserve_miscellaneous_prev_val !== false) {

                $paid_medical_updated = false;
                $paid_legal_updated = false;
                $paid_indemnity_updated = false;
                $paid_miscellaneous_updated = false;
                $reserve_medical_updated = false;
                $reserve_legal_updated = false;
                $reserve_indemnity_updated = false;
                $reserve_miscellaneous_updated = false;

                if ($paid_medical_prev_val === false) {

                    $paid_medical_prev_val = $injury_data['paid_medical'];
                } else $paid_medical_updated = true;

                if ($paid_legal_prev_val === false) {
                    $paid_legal_prev_val = $injury_data['paid_legal'];
                } else $paid_legal_updated = true;

                if ($paid_indemnity_prev_val === false) {
                    $paid_indemnity_prev_val = $injury_data['paid_indemnity'];
                } else $paid_indemnity_updated = true;

                if ($paid_miscellaneous_prev_val === false) {
                    $paid_miscellaneous_prev_val = $injury_data['paid_miscellaneous'];
                } else $paid_miscellaneous_updated = true;

                if ($reserve_medical_prev_val === false) {
                    $reserve_medical_prev_val = $injury_data['reserve_medical'];
                } else $reserve_medical_updated = true;

                if ($reserve_legal_prev_val === false) {
                    $reserve_legal_prev_val = $injury_data['reserve_legal'];
                } else $reserve_legal_updated = true;

                if ($reserve_indemnity_prev_val === false) {
                    $reserve_indemnity_prev_val = $injury_data['reserve_indemnity'];
                } else $reserve_indemnity_updated = true;

                if ($reserve_miscellaneous_prev_val === false) {
                    $reserve_miscellaneous_prev_val = $injury_data['reserve_miscellaneous'];
                } else $reserve_miscellaneous_updated = true;

                $prev_paid_total = $paid_medical_prev_val + $paid_legal_prev_val + $paid_indemnity_prev_val + $paid_miscellaneous_prev_val;
                $current_paid_total = $injury_data['paid_medical'] + $injury_data['paid_legal'] + $injury_data['paid_indemnity'] + $injury_data['paid_miscellaneous'];

                $prev_reserve_total = $reserve_medical_prev_val + $reserve_legal_prev_val + $reserve_indemnity_prev_val + $reserve_miscellaneous_prev_val;
                $current_reserve_total = $injury_data['reserve_medical'] + $injury_data['reserve_legal'] + $injury_data['reserve_indemnity'] + $injury_data['reserve_miscellaneous'];

                $prev_total_cost = $prev_paid_total + $prev_reserve_total;
                $current_total_cost = $current_paid_total + $current_reserve_total;

                $difference = $prev_total_cost - $current_total_cost;

                $summary = new SummaryPost();
                $summary->createSummary($request->injury_id, $user->id, $user->name . " updated the claims costs.", "valuePost");
                $tree_post = new TreePost();
                $tree_post->injury_id = $injury->id;
                $tree_post->user_id = $user->id;
                $tree_post->body = $user->name . " has updated the claim costs for this injury on " . Carbon::now()->format("M d, Y") . ".";
                $tree_post->type = "valuePost";
                if ($difference > 0) {
                    $tree_post->body .= " Congratulations! The total cost of this claim was reduced by $" . $difference . "!";
                } else {
                    $tree_post->body .= " The total cost of this claim increased by $" . abs($difference) . ".";
                }

                if ($paid_medical_updated === true) {
                    $tree_post->body .= "<br> The new Paid Medical value is $" . $injury_data['paid_medical'] . ".";
                }
                if ($paid_legal_updated === true) {
                    $tree_post->body .= "<br> The new Paid Legal value is $" . $injury_data['paid_legal'] . ".";
                }
                if ($paid_indemnity_updated === true) {
                    $tree_post->body .= "<br> The new Paid Indemnity value is $" . $injury_data['paid_indemnity'] . ".";
                }
                if ($paid_miscellaneous_updated === true) {
                    $tree_post->body .= "<br> The new Paid Miscellaneous value is $" . $injury_data['paid_miscellaneous'] . ".";
                }
                if ($reserve_medical_updated === true) {
                    $tree_post->body .= "<br> The new Reserve Medical value is $" . $injury_data['reserve_medical'] . ".";
                }
                if ($reserve_legal_updated === true) {
                    $tree_post->body .= "<br> The new Reserve Legal value is $" . $injury_data['reserve_legal'] . ".";
                }
                if ($reserve_indemnity_updated === true) {
                    $tree_post->body .= "<br> The new Reserve Indemnity value is $" . $injury_data['reserve_indemnity'] . ".";
                }
                if ($reserve_miscellaneous_updated === true) {
                    $tree_post->body .= "<br> The new Reserve Miscellaneous value is $" . $injury_data['reserve_miscellaneous'] . ".";
                }

                $tree_post->save();

                /*** push notification ***/
                $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
                $injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();
                $sent_notification_array = [];
                foreach($squad_members as $squad_member){
                    if(!in_array($squad_member->user_id, $sent_notification_array)){
                        $recipient_user = \App\User::where('id', $squad_member->user_id)->first();
                        if($recipient_user->notification_priority_1 === 1){
                            $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                            if($devices != NULL || $devices != ""){
                                $devices->sendNotification('Claim Costs Updated', $user->name . " updated the claim costs for " . $injured_employee->name . "'s claim.");
                            }
                        }
                        array_push($sent_notification_array, $squad_member->user_id);
                    }
                }
                /***********************************************************/

            }
        }
    }

    function updateEmployeeStatus(Request $request){
        $injuryID = $request->injury_id;
        $status = $request->status;
        $date = $request->date;
        $note = $request->note;
        $injury = Injury::where('id', $injuryID)->first();
        $injury->updateEmployeeStatus($status, $note, $date, Auth::user());
        return Carbon::createFromFormat("Y-m-d", $date)->toFormattedDateString();
    }

    function updateAdjuster(Request $request){
        $injuryID = $request->injury_id;
        $adjuster_name = $request->adjuster_name;
        $adjuster_email = $request->adjuster_email;
        $adjuster_phone = $request->adjuster_phone;
        $injury = Injury::where('id', $injuryID)->first();
        $injury->updateAdjusterInfo($adjuster_name, $adjuster_email, $adjuster_phone, Auth::user());
    }

    function updateSeverity(Request $request){
        $injury_id = $request->injury_id;
        $severity = $request->severity;
        $note = $request->note;
        $injury = Injury::where('id', $injury_id)->first();
        $injury->updateSeverity($severity, $note, Auth::user());
    }

    function updateInjuredEmail(Request $request){
        //get the vars
        $injury_id = $request->injury_id;
        $employee_id = $request->employee_id;
        $employee_name = $request->employee_name;
        $employee_email = $request->employee_email;
        $employee_phone = $request->employee_phone;

        //update the employee info
        $injured_employee = \App\User::where('id', $employee_id)->first();
        $injured_employee->name = $employee_name;
        $injured_employee->email = $employee_email;
        $injured_employee->phone = $employee_phone;

        //create the tree post
        $injury = Injury::where('id', $injury_id)->first();
        $tree_post = new TreePost();
        $tree_post->injury_id = $injury->id;
        $tree_post->user_id = Auth::user()->id;
        $tree_post->body = "Injured employee's info updated on " . Carbon::today()->toDateString() . ".";
        $tree_post->type = 'employeeStatusPost';
        $tree_post->save();

        $summary = new SummaryPost();
        $summary->createSummary($request->injury_id, Auth::user()->id, "Employee info has been updated.", "employeeStatusPost");

              /*** push notification ***/
              $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
              $injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();
              $sent_notification_array = [];
              foreach($squad_members as $squad_member){
                  if(!in_array($squad_member->user_id, $sent_notification_array)){
                        $user = \App\User::where('id', $squad_member->user_id)->first();
                        if($user->notification_priority_2 === 1){
                            $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                            if($devices != NULL || $devices != ""){
                                $devices->sendNotification('Injury Details Updated', "Injured employee's info updated on " . Carbon::today()->toDateString() . ".");
                            }
                        }
                      array_push($sent_notification_array, $squad_member->user_id);
                  }
              }
              /***********************************************************/

        //save
        $injured_employee->save();
    }

    function updateZenGardenEmail(Request $request){
        Log::debug("UpdateZenGardenEmail");
        $injury_id = $request->injury_id;
        $employee_id = $request->employee_id;
        $employee_email = $request->employee_email;

        $injured_employee = \App\User::where('id', $employee_id)->first();
        $injured_employee->email = $employee_email;
        $injury = Injury::where('id', $injury_id)->first();
        $injury->updateInjuredEmployeeEmail($employee_email);
        $injured_employee->save();
    }

    function optOutZengarden(Request $request){
        $injury_id = $request->injury_id;
        $injury = Injury::where('id', $injury_id)->first();
        $injury->zengarden_opt_out = 1;
        $injury->save();
    }

    function updateInjuryDescription(Request $request){
        $injury_id = $request->injury_id;
        $notes = $request->notes;
        $injury = Injury::where('id', $injury_id)->first();
        $injury->updateDescription($notes, Auth::user());
    }

    function updateFinalCost(Request $request){
        $injuryID = $request->injury_id;
        $final_cost = $request->final_cost;
        $medical_cost = $request->medical_cost;
        $legal_cost = $request->legal_cost;
        $indemnity_cost = $request->indemnity_cost;
        $misc_cost = $request->misc_cost;
        $injury = Injury::where('id', $injuryID)->first();
        $injury->updateFinalCost($final_cost, $medical_cost, $legal_cost, $indemnity_cost, $misc_cost, Auth::user());
    }

    function resolveInjury(Request $request){
        $injuryID = $request->injury_id;
        $date = $request->date;
        $injury = Injury::where('id', $injuryID)->first();
        if(isset($date)){
            $injury->resolveInjury(Auth::user(), $date);
        }
        else{
            $injury->resolveInjury(Auth::user());
        }
    }

    function reopenClaimStatus(Request $request){

        if (!isset($request->injury_id)) return response()->json(['error' => 'Missing Injury ID'], 400);
        $injuryID = $request->injury_id;
        $injury = Injury::where('id', $injuryID)->first();
        if (!$injury) return response()->json(['error' => 'Injury not found!'], 400);
        $injury->resolved = 0;
        $injury->reopened_at = Carbon::now();
        $injury->save();
    }

    function pauseClaimForLitigation(Request $request){
        $injury_id = $request->injury_id;

        $injury = Injury::where('id', $injury_id)->first();
        $injury->paused_for_litigation = 1;
        $injury->litigation_paused_date = Carbon::now();
        $injury->save();

        $tree_post = new TreePost();
        $tree_post->injury_id = $injury->id;
        $tree_post->user_id = Auth::user()->id;
        $tree_post->body = Auth::user()->name . " marked this claim as being under litigation on " . Carbon::today()->toDateString() . ".";
        $tree_post->type = 'reclassifiedPost';
        $tree_post->save();

        $summary = new SummaryPost();
        $summary->createSummary($request->injury_id, $user->id, $user->name . " marked this claim as being under litigation.", "reclassifiedPost");

        /*** push notification ***/
        $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
        $injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                $recipient_user = \App\User::where('id', $squad_member->user_id)->first();
                if($recipient_user->notification_priority_1 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('Paused for Litigation', Auth::user()->name . " paused " . $injured_employee->name . "'s claim for litigation.");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/

        $this->repo->claimPaused($injury_id);
    }

    //appointments
    function addAppointment(Request $request){
        $injury_id = $request->input('injury_id');
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $datetime = $request->input('datetime');
        $description = $request->input('description');
        $user_id = Auth::user()->id;
        Log::debug("appoint phone");
        Log::debug($phone);
        //2021-02-25 11:50 AM
        //$datetime = Carbon::createFromFormat("Y-m-d h:i A", $datetime);
        $datetime = Carbon::createFromFormat("Y-m-d\TG:i", $datetime);

        $appointment = new \App\Appointment();

        $appointment = $appointment->newAppointment($name, $phone, $email, $datetime, $description, $injury_id, $user_id);



        //Formatting for returning appointment

        $appointment = DB::table('appointments')->join('injuries_care_locations', 'appointments.location_id', '=', 'injuries_care_locations.id')->where('appointments.id', $appointment->id)
            ->where('appointments.cancelled', 0)
            ->select('appointments.*', 'injuries_care_locations.name', 'injuries_care_locations.email', 'injuries_care_locations.phone')->first();
        Log::debug("appointment after query");
        //Log::debug($appointment);
        //->where('appointments.appointment_time', '>', \Carbon\Carbon::now())
        if($appointment->appointment_time > \Carbon\Carbon::now()){
            Log::debug("future if");
            $appointment->appointment_status = "future";
        }else{
            Log::debug("past else");
            $appointment->appointment_status = "past";
        }
        Log::debug("before assigning appointment formatted date");
        $appointment->formatted_date = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $appointment->appointment_time)->format("M j Y / g:ia");
        Log::debug("appointment after date formatting");
        //Log::debug($appointment);

        return Response::json($appointment);
    }

    function editAppointment(Request $request){
        $appointment_id = $request->input('appointment_id');
        log::info("appointment");
        log::info($appointment_id);
        $injury_id = $request->input('injury_id');
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $datetime = $request->input('datetime');
        //$datetime = Carbon::createFromFormat("Y-m-d\TG:i", $datetime);
        Log::debug("edit datetime");
        Log::debug($datetime);
        $description = $request->input('description');

        $appointment = \App\Appointment::where('id', $appointment_id)->first();
        Log::debug($appointment);
        $location = \App\InjuryCareLocation::where('id', $appointment->location_id)->first();
        $location->name = $name;
        $location->email = $email;
        $location->phone = $phone;
        $location->save();

        $appointment->description = $description;
        //$appointment->appointment_time = $datetime = Carbon::createFromFormat("Y-m-d h:i A", $datetime);
        $appointment->appointment_time = $datetime = Carbon::createFromFormat("Y-m-d\TG:i", $datetime);
        $appointment->save();
        /*** push notification ***/
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
        $injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                $user = \App\User::where('id', $squad_member->user_id)->first();
                if($user->notification_priority_2 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('Appointment Edited', Auth::user()->name . " edited  " . $injured_employee->name . "'s appointment detials.");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/
    }

    function addAppointmentSummary(Request $request){
        $appointment_id = $request->input('appointment_id');
        $summary = $request->input('summary');
        $appointment = \App\Appointment::where('id', $appointment_id)->first();
        $appointment->addSummary($summary);
    }

    function addInjuryPhoto(Request $request){
        $photo = $request->photo;
        $name = $request->name;
        if(empty($name)){
            $name = "Injury Photo";
        }
        $description = $request->description;
        if(empty($description)){
            $description = "";
        }
        $injury = Injury::where('id', $request->injury_id)->first();
        /*
        $date = Carbon::now();
        $date = str_replace(' ', '_', $date);

        $image_count = count(\App\InjuryImage::where('injury_id', $injury->id)->get());
        */
        $dateOfImage = Carbon::now();
        $dateOfImage = str_replace(' ', '', $dateOfImage);
        $imageName = 'injuryUploads/injury_' . $injury->id . '_' . $dateOfImage .'.png';

        Storage::disk('public')->put($imageName, file_get_contents($photo));
        $imageName = "/storage/" . $imageName;
        $injuryIMG = new \App\InjuryImage();
        $injuryIMG->injury_id = $injury->id;
        $injuryIMG->name = $name;
        $injuryIMG->description = $description;
        $injuryIMG->location = $imageName;
        $injuryIMG->save();

        $post = new \App\TreePost();
        $post->injury_id = $injury->id;
        $post->user_id = Auth::user()->id;
        $post->body = Auth::user()->name . " added a photo.";
        $post->type = "photoPost";
        $post->save();

        /*** push notification ***/
        $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
        $injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                $user = \App\User::where('id', $squad_member->user_id)->first();
                if($user->notification_priority_2 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('Photo Uploaded', Auth::user()->name . " uploaded a photo to " . $injured_employee->name . "'s claim.");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/

        return Response::json('success');
    }

    function getInjuryPhotos(Request $request){
        $images = InjuryImage::where('injury_id', $request->injury_id)->get();
        Log::debug("images from getInjuryPhotos");
        Log::debug($images);
        return Response::json($images);
    }

    function getInjuryDocuments(Request $request){
        $documents = InjuryFile::where('injury_id', $request->injury_id)->get();
        Log::debug("documents from getInjuryDocuments");
        Log::debug($documents);
        return Response::json($documents);
    }

    function deleteInjuryPhoto(Request $request){
        $photo = InjuryImage::where('id', $request->photo_id)->first();
        //Storage::disk('data')->delete($photo->location);
        $photo_location = str_replace("/storage/", "", $photo->location);
        Storage::disk('public')->delete($photo_location);
        $tree_post = new \App\TreePost();
        $tree_post->injury_id = $photo->injury_id;
        $tree_post->user_id = Auth::user()->id;
        $tree_post->body = Auth::user()->name . " deleted a photo.";
        $tree_post->type = "photoPost";
        $tree_post->save();

        /*** push notification ***/
        $injury = \App\Injury::where('id', $photo->injury_id)->first();
        $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
        $injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                $user = \App\User::where('id', $squad_member->user_id)->first();
                if($user->notification_priority_2 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('Photo Deleted', Auth::user()->name . " deleted a photo from " . $injured_employee->name . "'s claim.");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/

        $photo->delete();
    }

    function updatePhotoDescription(Request $request){
        $photo_id = $request->photo_id;
        $description = $request->description;

        $photo = InjuryImage::where('id', $photo_id)->first();
        $photo->description = $description;
        $photo->save();
    }

    function addClaimNumber(Request $request){
        $claim_number = $request->claim_number;
        $claim_number_confirm = $request->claim_number_confirm;
        $injury_id = $request->injury_id;
        $injury = Injury::where('id', $injury_id)->first();

        if($claim_number == $claim_number_confirm){
            Log::debug('saving claim #');
            $injury->updateClaimNumber($claim_number, Auth::user());
        }
    }

    function deliverInjuryAlert(Request $request){
        $injury_id = $request->injury_id;
        $injury = \App\Injury::where('id', $injury_id)->first();
        $injury_alert = \App\FirstReportOfInjury::where('injury_id', $injury_id)->first();
        $injury_alert->action_taken = 1;
        $injury_alert->updated_at = Carbon::now();
        $injury->confirmFirstReportDelivery(Auth::user());
        $injury_alert->save();
    }

    function firstOfficialReportDelivered(Request $request){
        $injury_id = $request->injury_id;
        $injury = \App\Injury::where('id', $injury_id)->first();
        $injury->full_injury_report_delivered = 1;
        $injury->full_report_delivered_at = Carbon::now();
        $injury->confirmOfficialFirstReportDelivered(Auth::user());
        $injury->save();
    }

    function uploadDocument(Request $request){
        Log::debug('ajaxFileUpload');
        $name = $request->file_name;
        if(empty($name)){
            $name = "Injury Photo";
        }

        $file = $request->file('file');

        $fileName = $request->file_title;
        $fileDesc = $request->file_description;
        $injury_id = $request->injury_id;
        $extension = $file->getClientOriginalExtension();

        Log::debug('got data');
        Log::debug(var_export($file, true));
        Log::debug(var_export($extension, true));

        $count = \App\InjuryFile::where('injury_id', $injury_id)->get();
        $count = count($count);
        $modifiedName = preg_replace("/[^A-Za-z0-9 ]/", '', $fileName);
        $modifiedName = preg_replace('/\s+/', '', $modifiedName);
        $nameOfFile = 'injuryFiles/'.$injury_id . "_" . $modifiedName . "_" . $count . "." . $extension;
        Storage::disk('public')->put($nameOfFile, file_get_contents($file));
        //$file->move('storage/public/injuryFiles/', $nameOfFile);
        $nameOfFile = 'storage/' . $nameOfFile;

        Log::debug('file stored');

        $newFile = new InjuryFile();
        $newFile->injury_id = $injury_id;
        $newFile->name = $fileName;
        $newFile->description = $fileDesc;
        $newFile->file_type = $extension;
        $newFile->location = $nameOfFile;
        $newFile->user_id = Auth::user()->id;
        $newFile->save();

        Log::debug('saved in DB');

        $post = new \App\TreePost();
        $post->injury_id = $injury_id;
        $post->user_id = Auth::user()->id;
        $post->body = Auth::user()->name . " added a file named " . $newFile->name . ".";
        $post->type = 'filePost';
        $post->save();

        Log::debug('saved tree post');

        $user = \App\User::where('id', Auth::user()->id)->first();
        $user->zenjuries_userdocs_uploaded++;
        $user->save();

        Log::info('updated user');

        /*** push notification ***/
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
        $injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                if($user->notification_priority_2 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('Document Uploaded', $user->name . " uploaded a document to " . $injured_employee->name . "'s claim.");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/

        /*** push notification ***/
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
        $injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                if($user->notification_priority_2 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('Document Uploaded', $user->name . " uploaded a document to " . $injured_employee->name . "'s claim.");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/

        return Response::json('success');
    }

    public function getClaimCostHistory(Request $request){
        $injury_id = $request->injury_id;
        $injury = \App\Injury::where('id', $injury_id)->first();
        $months = $injury->getInjuryMonths();
        $cost_history = $injury->getClaimHistoryByMonth();
        $response_array['months'] = $months;
        $response_array['history'] = $cost_history;
        return Response::json($response_array);
    }

    function getUnresolvedInjuryList(Request $request){
        $company_id = Auth::user()->getCompany()->id;
        $user_teams = SquadMember::where('user_id', Auth::user()->id)->pluck('squad_id');
        $now = Carbon::now();
        $injuries = DB::table('injuries')->where('injuries.company_id', $company_id)->where('injuries.hidden', 0)->where('resolved', 0)
                ->whereIn('squad_id', $user_teams)
                ->leftJoin('users', 'injuries.injured_employee_id', '=', 'users.id')
                ->select('injuries.*', 'users.name as user_name', 'users.email as user_email', 'users.picture_location as user_pic_location', 'users.avatar_color as user_avatar_color')
                ->get();

        foreach($injuries as $injury){
            $injury->injury_date = Carbon::createFromFormat("m-d-Y h:i a", $injury->injury_date)->format("m/d/Y");

            if($injury->resolved === 1){
                $injury->current_cost = $injury->final_cost;
            }else{
                $medical_cost = \App\MedicalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                if(is_null($medical_cost)){$medical_cost = 0;}

                $legal_cost = \App\LegalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                if(is_null($legal_cost)){$legal_cost = 0;}

                $indemnity_cost = \App\IndemnityCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                if(is_null($indemnity_cost)){$indemnity_cost = 0;}

                $misc_cost = \App\MiscellaneousCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                if(is_null($misc_cost)){$misc_cost = 0;}

                $injury->current_cost = $medical_cost + $legal_cost + $indemnity_cost + $misc_cost;
            }

            if(is_null($injury->current_cost)){
                $injury->current_cost = 0;
            }

            $reserve_medical_cost = \App\ReserveMedicalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if(is_null($reserve_medical_cost)){$reserve_medical_cost = 0;}
            $reserve_legal_cost = \App\ReserveLegalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if(is_null($reserve_legal_cost)){$reserve_legal_cost = 0;}
            $reserve_indemnity_cost = \App\ReserveLegalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if(is_null($reserve_indemnity_cost)){$reserve_indemnity_cost = 0;}
            $reserve_misc_cost = \App\ReserveMiscellaneousCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if(is_null($reserve_misc_cost)){$reserve_misc_cost = 0;}

            $injury->reserve_cost = $reserve_medical_cost + $reserve_legal_cost + $reserve_indemnity_cost + $reserve_misc_cost;

            $injury->mood = \App\DiaryPost::where('injury_id', $injury->id)->orderBy('created_at', 'desc')->value('mood');

            //status
            if(!is_null($injury->employee_status)){

                if($injury->employee_status === "Hospital"){
                    $injury->status_number = 1;
                }else if($injury->employee_status === "Urgent Care"){
                    $injury->status_number = 2;
                }else if($injury->employee_status === "Family Practice"){
                    $injury->status_number = 3;
                }else if($injury->employee_status === "Home"){
                    $injury->status_number = 4;
                }else if($injury->employee_status === "Light Duty"){
                    $injury->status_number = 5;
                }else if($injury->employee_status === "Full Duty"){
                    $injury->status_number = 6;
                }else if($injury->employee_status === "Maximum Medical Improvement"){
                    $injury->status_number = 7;
                }else if($injury->employee_status === "Resigned"){
                    $injury->status_number = 8;
                }else if($injury->employee_status === "Terminated"){
                    $injury->status_number = 9;
                }else{
                    $injury->status_number = 1;
                }
            }else{
                $injury->status_number = 1;
            }

            $injury_alert = \App\FirstReportOfInjury::where('injury_id', $injury->id)->first();

            if(!is_null($injury_alert)){
                $injury->injury_alert_delivered = $injury_alert->action_taken;
                $injury->first_report_id = $injury_alert->id;
            }else{
                $injury->injury_alert_delivered = 0;
                $injury->first_report_id = NULL;
            }

            $injury->days = Carbon::now()->diffInDays($injury->created_at);

        }

        return Response::json($injuries);
    }

    function getCriticalInjuryList(Request $request){
        if(session('company_id')){
            $company_id = session('company_id');
        }else{
            $company_id = Auth::user()->getCompany()->id;
        }

        $user_teams = SquadMember::where('user_id', Auth::user()->id)->pluck('squad_id');
        $now = Carbon::now();
        if(Auth::user()->is_zenpro === 1){
            $injuries = DB::table('injuries')->where('injuries.company_id', $company_id)->where('injuries.hidden', 0)->where('resolved', 0)
                ->leftJoin('users', 'injuries.injured_employee_id', '=', 'users.id')
                ->select('injuries.*', 'users.name as user_name', 'users.email as user_email', 'users.picture_location as user_pic_location', 'users.avatar_color as user_avatar_color')
                ->get();
        }else{
            $injuries = DB::table('injuries')->where('injuries.company_id', $company_id)->where('injuries.hidden', 0)->where('resolved', 0)
                ->whereIn('squad_id', $user_teams)
                ->leftJoin('users', 'injuries.injured_employee_id', '=', 'users.id')
                ->select('injuries.*', 'users.name as user_name', 'users.email as user_email', 'users.picture_location as user_pic_location', 'users.avatar_color as user_avatar_color')
                ->get();
        }

        foreach($injuries as $injury){
            $injury->injury_date = Carbon::createFromFormat("m-d-Y h:i a", $injury->injury_date)->format("m/d/Y");

            if($injury->resolved === 1){
                $injury->current_cost = $injury->final_cost;
            }else{
                $medical_cost = \App\MedicalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                if(is_null($medical_cost)){$medical_cost = 0;}

                $legal_cost = \App\LegalCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                if(is_null($legal_cost)){$legal_cost = 0;}

                $indemnity_cost = \App\IndemnityCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                if(is_null($indemnity_cost)){$indemnity_cost = 0;}

                $misc_cost = \App\MiscellaneousCosts::where('injury_id', $injury->id)->where('current', 1)->value('cost');
                if(is_null($misc_cost)){$misc_cost = 0;}

                $injury->current_cost = $medical_cost + $legal_cost + $indemnity_cost + $misc_cost;
            }

            if(is_null($injury->current_cost)){
                $injury->current_cost = 0;
            }

            $reserve_medical_cost = \App\ReserveMedicalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if(is_null($reserve_medical_cost)){$reserve_medical_cost = 0;}
            $reserve_legal_cost = \App\ReserveLegalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if(is_null($reserve_legal_cost)){$reserve_legal_cost = 0;}
            $reserve_indemnity_cost = \App\ReserveLegalCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if(is_null($reserve_indemnity_cost)){$reserve_indemnity_cost = 0;}
            $reserve_misc_cost = \App\ReserveMiscellaneousCost::where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if(is_null($reserve_misc_cost)){$reserve_misc_cost = 0;}

            $injury->reserve_cost = $reserve_medical_cost + $reserve_legal_cost + $reserve_indemnity_cost + $reserve_misc_cost;

            $injury->mood = \App\DiaryPost::where('injury_id', $injury->id)->orderBy('created_at', 'desc')->value('mood');

            //status
            if(!is_null($injury->employee_status)){

                if($injury->employee_status === "Hospital"){
                    $injury->status_number = 1;
                }else if($injury->employee_status === "Urgent Care"){
                    $injury->status_number = 2;
                }else if($injury->employee_status === "Family Practice"){
                    $injury->status_number = 3;
                }else if($injury->employee_status === "Home"){
                    $injury->status_number = 4;
                }else if($injury->employee_status === "Light Duty"){
                    $injury->status_number = 5;
                }else if($injury->employee_status === "Full Duty"){
                    $injury->status_number = 6;
                }else if($injury->employee_status === "Maximum Medical Improvement"){
                    $injury->status_number = 7;
                }else if($injury->employee_status === "Resigned"){
                    $injury->status_number = 8;
                }else if($injury->employee_status === "Terminated"){
                    $injury->status_number = 9;
                }else{
                    $injury->status_number = 1;
                }
            }else{
                $injury->status_number = 1;
            }

            $injury_alert = \App\FirstReportOfInjury::where('injury_id', $injury->id)->first();

            if(!is_null($injury_alert)){
                $injury->injury_alert_delivered = $injury_alert->action_taken;
                $injury->first_report_id = $injury_alert->id;
            }else{
                $injury->injury_alert_delivered = 0;
                $injury->first_report_id = NULL;
            }

            $injury->days = Carbon::now()->diffInDays($injury->created_at);

        }

        return Response::json($injuries);
    }

    public function getCaseloadInjuries(Request $request){
        $user_teams = SquadMember::where('user_id', Auth::user()->id)->pluck('squad_id');
        $injuries = DB::table('injuries')->where('resolved', 0)->whereIn('squad_id', $user_teams)->leftJoin('users', 'injuries.injured_employee_id', '=', 'users.id')
        ->select('injuries.*', 'users.name as user_name', 'users.email as user_email', 'users.picture_location as user_pic_location', 'users.avatar_color as user_avatar_color')
        ->get();

        foreach($injuries as $injury){
            $company = \App\Company::where('id', $injury->company_id)->first();
            $injury->company_name = $company->company_name;
            $injury->days = Carbon::now()->diffInDays($injury->created_at);
        }
        return Response::json($injuries);

    }


}

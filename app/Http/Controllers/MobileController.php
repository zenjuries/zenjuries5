<?php

namespace App\Http\Controllers;
use App\User;
use App\Injury;
use App\DeviceId;
use App\UserWidget;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Log;
use App\Services\FCMService;


class MobileController extends Controller
{
    //
    function mobileLogin(Request $request){
        $validation = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $email = $request->email;
        $password = $request->password;

        $user = \App\User::where('email', $email)->first();

        if(is_null($user)){
            return Response::json("Sorry, no user was found with that email address.", 422);
        }

        if(Hash::check($password, $user->password)){
            return Response::json($user->api_token);
        }else return Response::json("Sorry, no user was found with those credentials", 422);
    }

    function mobileRedirect(Request $request, $api_token){
        $user = \App\User::where('api_token', $api_token)->first();

        if(is_null($user)){
            return Response::json("Sorry, we couldn't find the user associated with that token. Please try logging in again.", 422);
        }

        Auth::login($user);
        Session::put('zen_mobile', 'true');
        return redirect('/')->withCookie('mobile_auth_token', $api_token, 360);
    }

    public function store(Request $request)
    {
        $request->validate(['email' => 'required|email']);

        $email = $request->email;
        $user = \App\User::where('email', $email)->first();

        if(is_null($user)){
            return Response::json("Sorry, no user was found with that email address.", 422);
        }
        else{
            // We will send the password reset link to this user. Once we have attempted
            // to send the link, we will examine the response then see the message we
            // need to show to the user. Finally, we'll send out a proper response.
            $status = Password::sendResetLink(
                $request->only('email')
            );

            return $status == Password::RESET_LINK_SENT
                        ? back()->with(['status' => __($status)])
                        : back()->withErrors(['email' => __($status)]);
        }
    }

    function getUserSquad(Request $request){
        $company_name = $request->companyname;
        $company = \App\Company::where('company_name', $company_name)->first();
        $squads = \App\Squad::where('company_id', $company->id)->where('injury_squad', 0)->get();
        $squad_string = "";
        foreach($squads as $squad){
            //$members = \App\SquadMember::where('squad_id', $squad->id)->select('user_id', 'position_id')->get();
            //foreach($members as $member){
            //    $user_name = \App\User::where('id', $member->user_id)->select('name')->get();
            //    $member->name = $user_name;
            //}
            //$squad->members = $members;
            $squad_string .= $squad->squad_name . '!' . $squad->id . "!";
        }
        //Log::debug('mobile string: ' . $squad_string);
        return $squad_string;

    }

    function getUserCompanies(Request $request){
        $request->validate(['email' => 'required|email']);

        $email = $request->email;
        $user = \App\User::where('email', $email)->first();

        if($user->is_zenpro || $user->is_zagent || $user->type === "agent"){
            $companies = $user->getUserCompanies();
            $company_names = "";


            foreach($companies as $company){
                $company_names .= $company->company_name . "!" . $company->id . '!';
            }

            //Log::debug($company_names);
            return $company_names;
        }
        else{
            $company = $user->getCompany();
            return $company->company_name . '!' . $company->id . '!';
        }
    }

    function getSquadMembers(Request $request){
        $squad_id = $request->squadid;
        $members = \App\SquadMember::where('squad_id', $squad_id)->select('user_id', 'position_id')->get();
        $member_string = "";
        foreach($members as $member){
            $user_name = \App\User::where('id', $member->user_id)->select('name')->get();
            $member->name = $user_name;
            $member_string .= $user_name;
            switch($member->position_id){
                case 1:
                    $member_string .= ' - chief executive!';
                    break;
                case 2:
                    $member_string .= ' - medical support!';
                    break;
                case 3:
                    $member_string .= ' - physical therapist!';
                    break;
                case 4:
                    $member_string .= ' - claim manager!';
                    break;
                case 5:
                    $member_string .= ' - work comp agent!';
                    break;
                case 6:
                    $member_string .= ' - service agent!';
                    break;
                case 7:
                    $member_string .= ' - safety manager!';
                    break;
                case 8:
                    $member_string .= ' - employee supervisor!';
                    break;
                case 9:
                    $member_string .= ' - human resources!';
                    break;
                case 10:
                    $member_string .= ' - other!';
                    break;
                default;
            }
        }

        return $member_string;

    }

    public function saveInjury(Request $request)
    {
        Log::debug('mobile save injury');
        /*
        function base64_to_jpeg($base64_string, $injury_id, $imgCount, $fileLocation)
        {
            $data = explode(',', $base64_string);
            $imgFile = base64_decode($data[1]);
            Storage::put(
                //'/public/injuryUploads/injury_' . $injury_id . '_' . count($imgCount) . '.png',
                $fileLocation,
                $imgFile
            );
        }
        */
        //**** GET THE INJURY DATA ****/
        //$company = \App\Company::where('id', $company_id)->first();
        //$injury_info = Request::input('injury');
       // $injury = saveInjury($injury_info, request::input);
       $user_email = $request->user_email;
       $claim_setting = $request->claim_setting;
       $policy_number = $request->policy_number;
       $classification = $request->classification;
       $severity = $request->severity;
       $name = $request->name;
       $gender = $request->gender;
       $email = $request->email;
       $phone = $request->phone;
       $date = $request->date;
       $squad_id = $request->squad_id;
       $squad_name = $request->squad_name;
       $notes = $request->notes;
       $img = $request->img;
       $locations = $request->locations;
       $locations_codes = $request->locations_codes;
       $types = $request->types;
       $injury_locations_image = $request->injury_locations_image;
       $company_id = $request->company_id;
       $injury_photos_data = $request->injury_photos_data;
       $injury_photos_name = $request->injury_photos_name;
       $injury_photos_description = $request->injury_photos_description;

       $photo_name_array = explode('?', $injury_photos_name);
       $photo_desc_array = explode('?', $injury_photos_description);
       $photo_string_array = explode('?', $injury_photos_data);
       $photo_array = array();
       Log::debug($photo_string_array);
       Log::debug($photo_name_array);
       Log::debug($photo_desc_array);
       if($photo_name_array[0] != '' || $photo_desc_array[0] != '' || $photo_string_array[0] != ''){

            foreach($photo_name_array as $key=>$photo){
                    $array = [
                        'name' => $photo_name_array[$key],
                        'description' => $photo_desc_array[$key],
                        'data' => $photo_string_array[$key]
                    ];
                    array_push($photo_array, $array);
            }
        }

       //$photosArray = [
       // 'name' => [],
       // 'description' => [],
       // 'data' => explode('?', $injury_photos)
       //];

       $injury_info = [
        'claim_setting' => $claim_setting,
        'policy_number' => $policy_number,
        'classification' => $classification,
        'severity' => $severity,
        'name' => $name,
        'gender' => $gender,
        'email' => $email,
        'phone' => $phone,
        'date' => $date,
        'squadID' => $squad_id,
        'squad_name' => $squad_name,
        'notes' => $notes,
        'img' => $img,
        'locations' => explode('?', $locations),
        'locations_codes' => explode('?', $locations_codes),
        'types' => explode('?', $types),
        //'photos' => $photosArray,
        'photos' => $photo_array,
        'injuryLocationsImage' => $injury_locations_image,
        'company_id' => $company_id
       ];

        $user = \App\User::where('email', $user_email)->first();

        $injury_info['date'] = Carbon::createFromFormat('m-d-Y G:i', $injury_info['date'])->format('m-d-Y g:i a');
        Log::debug($injury_info['date']);

        //OUTPUT- 10-20-2022 23:50


        //$injury_info['date'] = Carbon::createFromFormat('m-d-Y h:m a', $injury_info['date'])->format('m-d-Y h:m a');
        //$injury_info['date'] = $injury_info['date']->format('m-d-Y h:m a');
        //$injury_info = json_encode($injury_info, JSON_HEX_APOS | JSON_UNESCAPED_SLASHES);

        //Log::debug('logging injury info');
        //Log::debug(var_export($injury_info, true));
        //Log::debug($injury_info['date']);

        $injury = new Injury();
        $injury->saveInjury($injury_info, $user);
    }


    function saveDeviceToken(Request $request){
        $user_email = $request->userEmail;
        $device_token = $request->deviceToken;
        $expired_device_token = $request->expiredDeviceToken;
        $device_type = $request->deviceType;
        Log::debug("token: " . $device_token);
        Log::debug('old token: ' . $expired_device_token);
        $user = \App\User::where('email', $user_email)->first();

        Log::debug($user);
        if($user){
            $device = \App\DeviceId::where('user_id', $user->id)->first();
            if($device){
                Log::debug('device exsits');
                if($expired_device_token === "" || $expired_device_token === NULL){
                    Log::debug('old device token is empty or null');
                    if($device_type === "phone"){
                        if($device->device_token_1 === "" || $device->device_token_1 === NULL){
                            $device->device_token_1 = $device_token;
                        }else{
                            $device->device_token_3 = $device_token;
                        }
                    }else if($device_type === "nophone"){
                        if($device->device_token_2 === "" || $device->device_token_2 === NULL){
                            $device->device_token_2 = $device_token;
                        }else{
                            $device->device_token_3 = $device_token;
                        }
                    }
                }else{
                    Log::debug('old device token is NOT empty or null');
                    //device token is expired..
                    //where == $expired_device_token
                    if($device->device_token_1 === $expired_device_token){
                        $device->device_token_1 = $device_token;
                    }
                    else if($device->device_token_2 === $expired_device_token){
                        $device->device_token_2 = $device_token;
                    }
                    else if($device->device_token_3 === $expired_device_token){
                        $device->device_token_3 = $device_token;
                    }
                    else{
                        $device->device_token_3 = $device_token;
                    }
                }
                $device->save();
            }else{
                Log::debug('creating new device_token entry');
                $new_device = new DeviceId;
                if($device_type === "phone"){
                    $new_device->device_token_1 = $device_token;
                }else if($device_type === "nophone"){
                    $new_device->device_token_2 = $device_token;
                }
                $new_device->user_id = $user->id;
                $new_device->save();
            }
        }else{
            Log::debug('no user');
        }
    }

    function manualPushTrigger(Request $request){
        $id = $request->id;
        $device = \App\DeviceId::where('user_id', $id)->first();
        $device->sendNotification('Testing Title', 'Testing Body from device method');
    }

    function multiUserPushTrigger(Request $request){
        $ids = [4500, 4501];
        foreach($ids as $id){
            $user = \App\User::where('id', $id)->first();
            $device = \App\DeviceId::where('user_id', $user->id)->first();
            $device->sendNotification('Testing Multiuser', 'Testing Body for multiuser notification');

        }
    }

    function RogerPushNotification(Request $request){
        $user = \App\User::where('id', 3994)->first();
        $device = \App\DeviceId::where('user_id', $user->id)->first();
        $device->sendNotification('Roger!', "Hey Roger, here's a notifcation");
    }

    function JessePushNotification(Request $request){
        $user = \App\User::where('id', 2755)->first();
        $device = \App\DeviceId::where('user_id', $user->id)->first();
        $device->sendNotification('Jesse!', "Here's your little sneak peak!");
    }
}

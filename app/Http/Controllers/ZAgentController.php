<?php

namespace App\Http\Controllers;

use App\Agency;
use App\Payment;
use App\PaymentType;
use App\PolicySubmission;
use App\Repositories\InvitationRepository;
use App\Repositories\PaymentsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Company;
use App\User;
use App\Team;
use App\TeamUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class ZAgentController extends Controller
{
    public function __construct(PaymentsRepository $payment_repo, InvitationRepository $invitationRepo)
    {
     $this->payment_repo = $payment_repo;
     $this->invitationRepo = $invitationRepo;
    }

    function newClient(Request $request){

        $validation = $request->validate([
            'company_name' => 'required',
            'contact_name' => 'required',
            'email' => 'required|email|unique:users',
            'premium' => 'required|numeric',
            'renewal_month' => 'required|numeric|max:12',
            'renewal_day' => 'required|numeric|max:31',
            //'payment_method' => 'required',
            //'file' => 'sometimes|mimes:pdf,png,jpeg',
            //'client_pays' => 'required'
        ]);

        $inviting_agency = Agency::where('id', $request->agency_id)->first();
        $inviting_company = Company::where('is_agency', 1)->where('agency_id', $inviting_agency->id)->first();

        //HANDLE PAYMENT

        //Create the Policy Submission
        $now = Carbon::now();
        $renewal_date = Carbon::createFromDate($now->year, $request->renewal_month, $request->renewal_day);

        if($renewal_date->gt($now)){

        }else{
            $renewal_date->addYear();
        }

        // Calculate final cost
        // If grandfathered agency then premium rate = 0
        $premium_rate = $inviting_agency->new_client ? $inviting_agency->premium_rate : 0;
        $premium = (float)$request->premium;
        $final_cost = round($premium * ((float)$premium_rate * .01), 2);

        $final_cost_day_rate = $final_cost / 365;
        Log::debug("final cost day rate: " . var_export($final_cost_day_rate, true));
        $days_before_renewal = $now->diffInDays($renewal_date);
        Log::debug("days before renewal: " . var_export($days_before_renewal, true));

        $final_cost = round($final_cost_day_rate * $days_before_renewal, 2);

        // Add the company
        $company = new Company();
        $company->company_name = $request->company_name;
        $company->agency_id = $request->agency_id;
        $company->is_agency = 0;
        $company->activated = 0;

        if ($inviting_agency->new_client) {
            // Add the payment and save the company
            if(isset($request->client_pays) && isset($request->payment_method)){
                $payment = new Payment();
                $payment->cost = $final_cost;
                $payment->paid = 0;
                $payment->payment_type_id = PaymentType::where('payment_type', 'New Client')->value('id');
                if ($request->client_pays === '0'){
                    // Don't save company until after payment succeeds
                    // $company->save();
                    $payment->company_id = $inviting_company->id;
                } else {
                    // Client pays, so need to save company before creating payment
                    $company->save();
                    $payment->company_id = $company->id;
                }

                $payment->save();

                // If the agency pays, go ahead and charge
                if($request->client_pays === '0' && $request->payment_method === "card"){
                    try {
                        $this->payment_repo->chargeExistingCustomer($payment->id, $request->company_name);
                    }
                    catch(\Exception $e) {
                        Log::debug('Error from chargeExistingCustomer');
                        Log::error($e);
                        return Response::json("Unable to process payment using your current payment method.", 402);
                    }

                    // Save company only after successful payment
                    $company->save();
                }
            }
        }
        else {
            // Grandfathered agency - no payment just save the company
            $company->save();
        }

        if($request->premium !== NULL){
            $new_policy = new PolicySubmission();
            $new_policy->agency_id = $company->agency_id;
            $new_policy->company_id = $company->id;
            $new_policy->premium = $request->premium;
            $new_policy->final_cost = $final_cost;

            if(isset($request->client_pays) && isset($request->payment_method)){
                //$new_policy->upload_location = $submission->upload_location;
                $new_policy->client_pays = $request->client_pays;
                if($request->payment_method === "check"){
                    $new_policy->pays_with_check = 1;
                }
            }

            $new_policy->renewal_day = $request->renewal_day;
            $new_policy->renewal_month = $request->renewal_month;
            $new_policy->upcoming_renewal_date = $renewal_date;
            $new_policy->save();
        }

        //TODO: store upload
        /*
        $file = $request->file('file');

        $count = count(PolicySubmission::get());

        $file_name = "policy_upload_" . $count . "_" . $new_policy->id;

        $extension = $file->getClientOriginalExtension();
        $file_name.= '.' . $extension;

        $file->move('storage/public/policyUploads/', $file_name);

        $path = 'storage/public/policyUploads/' . $file_name;

        $new_policy->upload_location = $path;
        $new_policy->save();
        */
        if($request->premium !== NULL){
            $company->policy_submission_id = $new_policy->id;
        }
        $company->save();

        //*** Create the user ***
        $user = new User();
        $user->name = $request->contact_name;
        $user->email = $request->email;
        $user->company_id = $company->id;
        $user->type = "internal";
        $user->trial_ends_at = Carbon::now();
        $user->last_read_announcements_at = Carbon::now();
        $user->password_token = Str::random(20);
        $user->is_zagent = 0;
        $user->zengarden_theme = "themeBamboo";
        $user->save();
        $user->setRandomAvatar();
        $user->setUniqueApiToken();
        $user->setDefaultWidgets();

        //*** Create the team ***
        $team = new Team();
        $team->owner_id = $user->id;
        $team->name = $company->company_name;
        $team->coupon = "Agency License";
        $team->valid = 1;
        $team->trial_ends_at = Carbon::now();
        $team->save();

        //*** Update company's team id ***
        $company->team_id = $team->id;
        $company->save();

        //*** Update user's team id ***
        $user->current_team_id = $team->id;
        $user->save();

        //*** Create Team user record to link user with team ***
        $team_user = new TeamUser();
        $team_user->team_id = $team->id;
        $team_user->user_id = $user->id;
        $team_user->role = "owner";
        $team_user->save();

        $company->createDefaultSquad($user->id);

        $invite_repo = new InvitationRepository();
        $invite_repo->inviteCompany($company->id);

        $company->assignZenpro();


        return $company->id;

    }

    function refreshCompanies(Request $request){
        $agency = Agency::where('id', $request->agency_id)->first();
        $company_array = [];
        $company_array['activated'] = $agency->getCompanies("activated");
        $company_array['pending'] = $agency->getCompanies("pending");
        return Response::json($company_array);
    }

    function myZagentInviteZagent(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email'
        ]);
        $email = $request->input('email');
        $name = $request->input('name');
        //Log::debug(var_export($email, true));
        $company_id = $request->input('company_id');
        //$company = \App\Company::where('id', $company_id);

        //This works for agents but wont works for zenpros
        /*
        $agency = Auth::user()->getAgency();
        Log::debug('Agency from getAgency: ' . $agency);
        $company = \App\Company::where('agency_id', $agency->id)->where('is_agency', 1)->first();
        Log::debug('Company from getting company with agency id and is agency: ' . $company);
        */

        $current_company = Auth::user()->getCompany();
        Log::debug('current_company: ' . $current_company);
        $agency = \App\Company::where('agency_id', $current_company->agency_id)->where('is_agency', 1)->first();
        Log::debug('agency from company table: ' . $agency);
        $email_check = \App\User::where('email', $email)->first();
        if (is_null($email_check)) {
            /*
            $invitation_repo = new SendInvitation();
            $team_id = \App\Company::where('id', $company_id)->value('team_id');
            $team = \App\Team::where('id', $team_id)->first();
            $invitation_repo->handle($team, $email, 'agent');
            */
            //inviteNewUser($name, $email, $company_id, $is_zagent = 0, $user_type = "internal", $injured = 0, $injury_id = NULL)
            $this->invitationRepo->inviteNewUser($name, $email, $agency->id, 1, 'agent', 0, NULL);
            return Response::json(['email' => 'Your invitation has been sent!']);
        }else{
            return Response::json(['error' => 'Sorry, that email is already in use.']);
        }
    }

    function resendClientInvite(Request $request){
        $company_id = $request->input('company_id');
        Log::debug("company_id: " . $company_id);
        $company = \App\Company::where('id', $company_id)->first();
        if($company){
        $response = $company->resendClientInvite();
        }else{
            $response = 'company not set';
        }
        return Response::json($response);
    }
}

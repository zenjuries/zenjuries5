<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\CompanyInvite;
use App\Models\UserInvite;
use App\Repositories\PaymentsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use App\Carrier;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    public function __construct(PaymentsRepository $payment_repo)
    {
        $this->payment_repo = $payment_repo;
    }

    //handles a company registering through an invite
    function registerCompanyInvite(Request $request){
        //validate incoming data
        $validated = $request->validate([
            'companyName' => 'required',
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|confirmed',
            'renewalDay' => 'numeric|max:31'
        ]);

        //remove non-numeric characters from phone
        $company_phone = preg_replace("/[^0-9]/", "", $request->companyPhone);

        //handle optional policy data
        //TODO: validate policy day/month
        /*
        if(!is_null($request->classCode) || !is_null($request->policyNumber) || !is_null($request->renewalDay) || !is_null($request->renewalMonth)){
            $policy_info_array = array();
            //$policy_info_array['renewal_date', 'class_code', 'policy_number'];
            if(!is_null($request->renewalDay) && !is_null($request->renewalMonth)){
                $renewal_date = Carbon::create(Carbon::now()->year, $request->renewalMonth, $request->renewalDay);
                $policy_info_array['renewal_date'] = $renewal_date;
            }else $policy_info_array['renewal_date'] = NULL;

            if(!is_null($request->classCode)){
                $policy_info_array['class_code'] = $request->classCode;
            }else $policy_info_array['class_code'] = NULL;

            if(!is_null($request->policyNumber)){
                $policy_info_array['policy_number'] = $request->policyNumber;
            }else $policy_info_array['policy_number'] = NULL;

        }else{
            $policy_info_array = NULL;
        }
        */


        //create the company
        $company = new Company();
        $company = $company->createCompany($request->companyName, $company_phone);

        //update policy info
        $company->updatePolicyInfo($request->classCode, $request->policyNumber, $request->renewalDay, $request->renewalMonth);

        //create the company owner
        $company_owner = new User();
        $company_owner->createUser($request->name, $request->email, $request->password, $company->id);
        //add them to the company
        $company_owner->addUserToCompany($company->id);
        //update the company to show the owner
        $company->updateCompanyOwner($company_owner->id);
        //update the company invite to show that its been redeemed
        $invite = CompanyInvite::where('token', $request->inviteToken)->first();
        $invite->redeemed_at = Carbon::now();
        $invite->save();
        //login the new user
        Auth::login($company_owner);
        //redirect to welcome page
        return redirect('/');

    }

    function updateCompanyInfo(Request $request){
        //validate incoming data
        $validated = $request->validate([
            'companyId' => 'required',
            'phone' => 'required',
            'mailingAddress' => 'required',
            'mailingCity' => 'required',
            'mailingState' => 'required',
            'mailingZip' => 'required'
        ]);
        $id = $request->input('companyId');
        $phoneNumber = $request->input('phone');
        $mailingAddress = $request->input('mailingAddress');
        $mailingAddressLine2 = $request->input('mailingAddressLine2');
        $city = $request->input('mailingCity');
        $state = $request->input('mailingState');
        $zip = $request->input('mailingZip');

        $company = \App\Company::where('id', $id)->first();
        $company->company_phone = $phoneNumber;
        $company->mailing_address = $mailingAddress;
        $company->mailing_address_line_2 = $mailingAddressLine2;
        $company->mailing_city = $city;
        $company->mailing_state = $state;
        $company->mailing_zip = $zip;

        $company->save();

        /*** push notification ***/
        $owner = $company->getCompanyOwner();
        if($owner->notification_priority_2 === 1){
            $devices = \App\DeviceId::where('user_id', $owner->id)->first();
            if($devices != NULL || $devices != ""){
                $devices->sendNotification('Company Info Updated', "Your company info has been updated!");
            }
        }
        /***********************************************************/


    }

    function updateCompanyBillingInfo(Request $request){
        //validate incoming data
        $validated = $request->validate([
            'companyId' => 'required',
            'billingAddress' => 'required',
            'billingCity' => 'required',
            'billingState' => 'required',
            'billingZip' => 'required'
        ]);
        $id = $request->input('companyId');
        $billingAddress = $request->input('billingAddress');
        $billingAddressLine2 = $request->input('billingAddressLine2');
        $city = $request->input('billingCity');
        $state = $request->input('billingState');
        $zip = $request->input('billingZip');

        $company = \App\Company::where('id', $id)->first();
        $company->billing_address = $billingAddress;
        $company->billing_address_line_2 = $billingAddressLine2;
        $company->billing_city = $city;
        $company->billing_state = $state;
        $company->billing_zip = $zip;

        $company->save();


    }

    function updatePolicyInfo(Request $request){
        $validated = $request->validate([
            'companyId' => 'required',
            'classCode' => 'required',
            'policyNumber' => 'required',
            'renewalDay' => 'required',
            'renewalMonth' => 'required'
        ]);

        //grab the inputs
        $id = $request->input('companyId');
        $class_code = $request->input('classCode');
        $policy_number = $request->input('policyNumber');
        $renewal_day = $request->input('renewalDay');
        $renewal_month = $request->input('renewalMonth');

        //set the company
        $company = \App\Company::where('id', $id)->first();

        //set the class code and policy number
        $company->class_code = $class_code;
        $company->policy_number = $policy_number;

        //determine if the renewal date is this year or next year
        if (!empty($renewal_month) && !empty($renewal_day)) {
            $now = Carbon::now();
            $currYear = $now->year;
            $checkDate = Carbon::createFromDate($currYear, $renewal_month, $renewal_day);
            if ($checkDate->gt($now)) {
                $company->renewal_date = $checkDate;
            } else {
                $currYear = Carbon::now()->addYear()->year;
                $finalDate = Carbon::createFromDate($currYear, $renewal_month, $renewal_day);
                $company->renewal_date = $finalDate;
            }
        }

        $company->save();
        /*** push notification ***/
        $owner = $company->getCompanyOwner();
        if($owner->notification_priority_2 === 1){
            $devices = \App\DeviceId::where('user_id', $owner->id)->first();
            if($devices != NULL || $devices != ""){
                $devices->sendNotification('Policy Info Updated', "Your company policy info has been updated!");
            }
        }
        /***********************************************************/
    }

    function newPolicyNumber(Request $request){
        $validated = $request->validate([
            'company_id' => 'required',
            'policy_number' => 'required',
            'renewal_day' => 'required',
            'renewal_month' => 'required'
        ]);

        //grab the inputs
        $id = $request->input('company_id');
        $policy_number = $request->input('policy_number');
        $renewal_day = $request->input('renewal_day');
        $renewal_month = $request->input('renewal_month');

        //set the company
        $company = \App\Company::where('id', $id)->first();

        //set the class code and policy number
        $company->policy_number = $policy_number;

        //determine if the renewal date is this year or next year
        if (!empty($renewal_month) && !empty($renewal_day)) {
            $now = Carbon::now();
            $currYear = $now->year;
            $checkDate = Carbon::createFromDate($currYear, $renewal_month, $renewal_day);
            if ($checkDate->gt($now)) {
                $company->renewal_date = $checkDate;
            } else {
                $currYear = Carbon::now()->addYear()->year;
                $finalDate = Carbon::createFromDate($currYear, $renewal_month, $renewal_day);
                $company->renewal_date = $finalDate;
            }
        }

        $company->save();
    }

    function addNewCarrier(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'email'
         ]);

         $name = $request->input('name');
         $email = $request->input('email');
         $phone = $request->input('phone');
         $company_id = $request->input('company_id');

         $company = \App\Company::where('id', $company_id)->first();

         $carrier = new Carrier();
         $carrier->name = $name;
         if($phone !== NULL && $phone !== ""){
             $carrier->phone = $phone;
         }
         if($email !== NULL && $email !== ""){
             $carrier->email = $email;
         }
         $carrier->save();

         $company->carrier_id = $carrier->id;
         $company->zen_carrier_id = NULL;
         $company->save();

         $new_carrier['name'] = $carrier->name;
         $new_carrier['phone'] = $carrier->phone;
         $new_carrier['email'] = $carrier->email;

         /*** push notification ***/
        $owner = $company->getCompanyOwner();
        if($owner->notification_priority_2 === 1){
            $devices = \App\DeviceId::where('user_id', $owner->id)->first();
            if($devices != NULL || $devices != ""){
                $devices->sendNotification('Carrier Updated', "Your carrier has been updated!");
            }
        }
        /***********************************************************/

         return Response::json($new_carrier);
    }

    function selectZenCarrier(Request $request){
        $zencarrier_id = $request->input('zencarrier_id');
        $company_id = $request->input('company_id');

        $company = \App\Company::where('id', $company_id)->first();

        $zencarrier = \App\ZenCarrier::where('id', $zencarrier_id)->first();
        if(!is_null($zencarrier)){
            $company->zen_carrier_id = $zencarrier->id;
            $company->carrier_id = $zencarrier->carrier_id;
            $company->save();
        }

        $carrier['name'] = $zencarrier->carrier_name;
        $carrier['phone'] = $zencarrier->carrier_phone;
        $carrier['email'] = $zencarrier->carrier_email;

        return Response::json($carrier);
    }

    function changeCompanyOwner(Request $request){
        //$oldOwnerId = Input::get('oldOwnerId');
        $newOwnerId = $request->input('newOwnerId');
        $teamId = $request->input('teamId');
        $team = \App\Team::where('id', $teamId)->first();

        /*** push notification ***/
        $recipient_user = \App\User::where('id', $team->owner_id)->first();
        if($recipient_user->notification_priority_1 === 1){
            $devices = \App\DeviceId::where('user_id', $team->owner_id)->first();
            if($devices != NULL || $devices != ""){
                $devices->sendNotification('Company Account Owner Change', "There has been a change in the company account owner!");
            }
        }
        /***********************************************************/

        \App\TeamUser::where('user_id', $team->owner_id)->where('team_id', $teamId)->where('role', 'owner')->update(['role' => 'employee']);
        \App\TeamUser::where('user_id', $newOwnerId)->where('team_id', $teamId)->update(['role' => 'owner']);
        //\App\Team::where('id', $teamId)->where('id', $teamId)->update(['owner_id' => $newOwnerId]);
        $team->owner_id = $newOwnerId;
        $team->save();

        /*** push notification ***/
        $recipient_user = \App\User::where('id', $team->owner_id)->first();
        if($recipient_user->notification_priority_1 === 1){
            $devices = \App\DeviceId::where('user_id', $team->owner_id)->first();
            if($devices != NULL || $devices != ""){
                $devices->sendNotification('Company Account Owner Change', "You are now the company account owner!");
            }
        }
        /***********************************************************/
    }

    function changeCompanyEmailSubscription(Request $request){
        //$company_id = $request->input('companyId');
        $company_id = $request->input('company_id');
        Log::debug($company_id);
        $changeEmailSub = $request->input('changeEmailSub');
        Log::debug("changeEMailSub");
        Log::debug($changeEmailSub);

        $company = \App\Company::where('id', $company_id)->first();
        Log::debug($company);
        $company->gets_unsubscribed_emails = $changeEmailSub;
        $company->save();
    }

    function updateCompanyBilling(Request $request){
        $payment_method = $request->payment_method;
        $company_id = $request->company_id;

        $company = \App\Company::where('id', $company_id)->first();

        if($company->stripe_id === NULL){
            $this->payment_repo->createNewStripeCustomer($company_id);
        }

        $this->payment_repo->updateCard($company_id, $payment_method);

        $company = \App\Company::where('id', $company_id)->first();

        /*** push notification ***/
        $owner = $company->getCompanyOwner();
        if($owner->notification_priority_1 === 1){
            $devices = \App\DeviceId::where('user_id', $owner->id)->first();
            if($devices != NULL || $devices != ""){
                $devices->sendNotification('Billing Info Updated', "Your company's billing info has been updated!");
            }
        }
        /***********************************************************/

        return Response::json($company->pm_last_four);
    }

    function setCompanyColor(Request $request){
        $company_id = $request->company_id;
        $color = $request->color;
        Log::debug($color);
        $company = \App\Company::where('id', $company_id)->first();
        $company->setCompanyColor($color);
    }

    function createCompanyLogo(Request $request){
        $company_id = $request->company_id;
        $img_data_url = $request->img;
        $company = \App\Company::where('id', $company_id)->first();

        $data = explode(',', $img_data_url);
        $img_file = base64_decode($data[1]);

        Storage::disk('public')->put('companyLogo_' . $company->id . '.png', $img_file);
        $image_name = '/storage/companyLogo_' . $company->id . '.png';

        $company->logo_location = $image_name;
        $company->save();

        return Response::json($company->logo_location);
    }

    function createDefaultCompanyLogo(Request $request){
        $company_id = $request->company_id;
        $random_image_number = $request->random_image_number;
        $company = \App\Company::where('id', $company_id)->first();

        $image_name = '/images/default_logos/company' . $random_image_number . '.png';

        $company->logo_location = $image_name;
        $company->save();

        return Response::json($company->logo_location);
    }
    
}

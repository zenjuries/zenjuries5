<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Svg\Tag\Rect;
use App\Services\FCMService;

class ZenployeeController extends Controller
{
    function newMoodPost(Request $request){
        Log::debug('new mood post');
        $diary_post = new \App\DiaryPost();
        $diary_post->injury_id = $request->input('injury_id');
        $diary_post->user_id = $request->input('user_id');
        $diary_post->mood = $request->input('mood');
        $diary_post->note = '';
        $diary_post->post_type = $request->input('post_type');
        $diary_post->save();
        Log::debug($diary_post);

        $tree_post = new \App\TreePost();
        $tree_post->newZenployeeMoodPost($diary_post);
    }

    function newMoodAndNotePost(Request $request){
        Log::debug("in newMoodAndNotePost function");
        $diary_post = new \App\DiaryPost();

        $diary_post->injury_id = $request->input('injury_id');
        $diary_post->user_id = $request->input('user_id');
        $diary_post->mood = $request->input('mood');
        $diary_post->note = $request->input('note');
        $diary_post->post_type = $request->input('type');
        $diary_post->save();

        $tree_post = new \App\TreePost();
        $tree_post->newZenployeeMoodAndNotePost($diary_post);
    }

    function newNotePost(Request $request){

        $diary_post = new \App\NotePost();
        $diary_post->injury_id = $request->input('injury_id');
        $diary_post->user_id = $request->input('user_id');
        $diary_post->note = $request->input('note');
        $diary_post->save();

        $tree_post = new \App\TreePost();
        $tree_post->newZenployeeNotePost($diary_post);
    }

    function updateUserInfo(Request $request){
        $this->validate($request,[
        'name' => 'required',
        'email' => 'required|email|unique:users,email,'.$request->user_id,
            ]);
        $user = \App\User::where('id', $request->input('user_id'))->first();

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->description = $request->input('description');
        $user->save();
        /*** push notification ***/
        if($user->notification_priority_2 === 1){
            $devices = \App\DeviceId::where('user_id', $user->id)->first();
            if($devices != NULL || $devices != ""){
                $devices->sendNotification('Info Updated', "Your info has been updated!");
            }
        }
        /***********************************************************/

    }

    function uploadPhoto(Request $request){

        $user_id = $request->input('user_id');
        $img = $request->input('img');
        $color = $request->input('color');

        $user = \App\User::where('id', $user_id)->first();

        $data = explode(',', $img);
        $imgFile = base64_decode($data[1]);

        if($user->zenjuries_avatar_changed % 2 == 0){
            $trailing_character = "_0";
        }else{
            $trailing_character = "_1";
        }
        //Storage::put('/public/profiles/profile_pic_' . $user->id . $trailing_character . '.' . 'png', $imgFile);
        Storage::disk('public')->put('profiles/profile_pic_' . $user->id . $trailing_character . '.' . 'png', $imgFile);
        //get the user to store the image

		$user->zenjuries_photo_changed++;
        $user->zenjuries_avatar_changed++;


        //$imageName contains the location of the image to be stored in the DB
        //$imageName = '/storage/public/profiles/profile_pic_' . $user->id . $trailing_character . '.' . 'png?' . $user->zenjuries_photo_changed;
        $imageName = '/storage/profiles/profile_pic_' . $user->id . $trailing_character . '.' . 'png?' . $user->zenjuries_photo_changed;

        $user->picture_location = $imageName;
        $user->photo_url = $imageName;

        $user->avatar_number = NULL;
        $user->avatar_color = $color;
        $user->save();
        //clear their old avatar, if it exists
        $oldAvatar = \App\Avatar::where('user_id', $user->id)->first();
        if ($oldAvatar !== null) {
            $oldAvatar->delete();
        }


        //$user->uploadProfilePhoto($img);
        return Response::json(Auth::user()->picture_location);
    }

    function setIcon(Request $request){
        $user_id = $request->input('user_id');
        $icon = $request->input('avatar_url');

        $user = \App\User::where('id', $user_id)->first();

        $user->zenjuries_photo_changed++;
        $user->zenjuries_avatar_changed++;

        $user->picture_location = $icon . "?" + $user->zenjuries_avatar_changed;
        $user->photo_url = $icon . "?" + $user->zenjuries_avatar_changed;

        $user->save();

        //$user->setIcon($icon);
    }

    function createAvatar(Request $request){
        $user_id = $request->input('user_id');
        $img_data_url = $request->input('img');
        $avatar_number = $request->input('avatar_number');
        $color = $request->input('color');

        $user = \App\User::where('id', $user_id)->first();

        $data = explode(',', $img_data_url);
        $img_file = base64_decode($data[1]);


        if($user->zenjuries_avatar_changed % 2 == 0){
            $trailing_character = "_0";
        }else{
            $trailing_character = "_1";
        }

        //Storage::put('/public/profiles/profile_pic_' . $user->id . $trailing_character . '.png', $img_file);
        Storage::disk('public')->put('profiles/profile_pic_' . $user->id . $trailing_character . '.png', $img_file);

        //$image_name = '/storage/public/profiles/profile_pic_' . $user->id . $trailing_character . '.png';
        $image_name = '/storage/profiles/profile_pic_' . $user->id . $trailing_character . '.png';

		$user->zenjuries_avatar_changed++;

		$image_name.= "?" . $user->zenjuries_avatar_changed;

        $user->picture_location = $image_name;
        $user->photo_url = $image_name;
        //$user->zenjuries_avatar_changed++;
        $user->avatar_number = $avatar_number;
        $user->avatar_color = $color;
        $user->save();

        //$user->createAvatar($img_data_url, $avatar_number);
        return Response::json(Auth::user()->picture_location);


    }

    function setColor(Request $request){
        $user_id = $request->input('user_id');
        $color = $request->input('color');

        $user = \App\User::where('id', $user_id)->first();
        $user->setColor($color);
    }

    function addAppointment(Request $request){
        $injury_id = $request->input('injury_id');
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $datetime = $request->input('datetime');
        $description = $request->input('description');
        $user_id = $request->input('user_id');

        //2021-02-25 11:50 AM
        $datetime = Carbon::createFromFormat("Y-m-d h:i A", $datetime);


        $appointment = new \App\Appointment();

        $appointment = $appointment->newAppointment($name, $phone, $email, $datetime, $description, $injury_id, $user_id);



        //Formatting for returning appointment

        $appointment = DB::table('appointments')->join('injuries_care_locations', 'appointments.location_id', '=', 'injuries_care_locations.id')->where('appointments.id', $appointment->id)
        	->where('appointments.cancelled', 0)
			->select('appointments.*', 'injuries_care_locations.name', 'injuries_care_locations.email', 'injuries_care_locations.phone')->first();

			//->where('appointments.appointment_time', '>', \Carbon\Carbon::now())
		if($appointment->appointment_time > \Carbon\Carbon::now()){
			$appointment->appointment_status = "future";
		}else{
			$appointment->appointment_status = "past";
		}

        $appointment->formatted_date = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $appointment->appointment_time)->format("M j Y / g:ia");

        return Response::json($appointment);


    }

    function editAppointment(Request $request){
	    $appointment_id = $request->input('appointment_id');
	    $name = $request->input('name');
	    $email = $request->input('email');
	    $phone = $request->input('phone');
	    $datetime = $request->input('datetime');
	    $description = $request->input('description');

	    $appointment = \App\Appointment::where('id', $appointment_id)->first();
	    $location = \App\InjuryCareLocation::where('id', $appointment->location_id)->first();

	    $location->name = $name;
	    $location->email = $email;
	    $location->phone = $phone;
	    $location->save();

	    $appointment->description = $description;
	    $appointment->appointment_time = $datetime = Carbon::createFromFormat("Y-m-d h:i A", $datetime);
	    $appointment->save();

    }

    function addAppointmentSummary(Request $request){
        $appointment_id = $request->input('appointment_id');
        $summary = $request->input('summary');
        $appointment = \App\Appointment::where('id', $appointment_id)->first();
        $appointment->addSummary($summary);
    }

    function getAppointments(Request $request){
	    $injury_id = $request->input('injury_id');

	    $future_appointments = DB::table('appointments')->join('injuries_care_locations', 'appointments.location_id', '=', 'injuries_care_locations.id')->where('appointments.injury_id', $injury_id)
	    	->where('appointments.cancelled', 0)->where('appointments.appointment_time', '>', \Carbon\Carbon::now())
			->select('appointments.*', 'injuries_care_locations.name', 'injuries_care_locations.email', 'injuries_care_locations.phone')->get();

	    $past_appointments = DB::table('appointments')->join('injuries_care_locations', 'appointments.location_id', '=', 'injuries_care_locations.id')->where('appointments.injury_id', $injury_id)
	    	->where('appointments.cancelled', 0)->where('appointments.appointment_time', '<=', \Carbon\Carbon::now())
	        ->select('appointments.*', 'injuries_care_locations.name', 'injuries_care_locations.email', 'injuries_care_locations.phone')->get();

	    $appointments['future'] = $future_appointments;
	    $appointmens['past'] = $past_appointments;

	    return Response::json($appointmens);
    }

    function updatePassword(Request $request){
        $this->validate($request,[
           'password' => 'required|confirmed|min:6'
        ]);
        $user = \App\User::where('id', $request->input('user_id'))->first();
        $timestamp = $user->updatePassword($request->input('password'));
        /*** push notification ***/
        if($user->notification_priority_2 === 1){
            $devices = \App\DeviceId::where('user_id', $user->id)->first();
            if($devices != NULL || $devices != ""){
                $devices->sendNotification('Password Updated', "Your password has been updated!");
            }
        }
        /***********************************************************/
        return $timestamp;
    }

    function updateAlertSettings(Request $request){
        $user = \App\User::where('id', $request->input('user_id'))->first();

        $alert_setting = $request->input('setting');

        if($alert_setting === "full"){
            $user->gets_priority_2_emails = 1;
            $user->notification_priority_2 = 1;
            $user->gets_priority_3_emails = 1;
            $user->notification_priority_3 = 1;
        }else if($alert_setting === "partial"){
            $user->gets_priority_3_emails = 0;
            $user->notification_priority_3 = 0;
            $user->gets_priority_2_emails = 1;
            $user->notification_priority_2 = 1;
        }else if($alert_setting === "none"){
            $user->gets_priority_3_emails = 0;
            $user->notification_priority_3 = 0;
            $user->gets_priority_2_emails = 0;
            $user->notification_priority_2 = 0;
        }

        $user->save();
    }

    function updateZengardenTheme(Request $request){
        $user = \App\User::where('id', $request->input('user_id'))->first();

        $user->zengarden_theme = $request->input('theme');
        $user->save();
    }

    function logout()
    {
        Auth::logout();
        return Redirect('https://zenployees.com/');
    }

    function ZenployeeMobileLogin(Request $request){
        Log::debug('zenployee mobile login');
        Log::debug(var_export($request, true));
        $this->validate($request,[
           'email' => 'required|email',
           'password' => 'required',
        ]);

        $email = $request->email;
        $password = $request->password;
        Log::debug(var_export($email, true));
        Log::debug(var_export($password, true));

        $user = \App\User::where('email', $email)->first();

        if(is_null($user)){
            return Response::json('Sorry, no user was found with that email address', 422);
        }

        if(Hash::check($password, $user->password)){
            return Response::json($user->api_token);
        }else return Response::json("Sorry, no user was found with those credentials", 422);


    }

    function ZenployeeMobileRedirect($api_token){
        Log::debug('zenployee mobile redirect');
        Log::debug(var_export($api_token, true));
        $user = \App\User::where('api_token', $api_token)->first();

        if(is_null($user)){
            return Response::json("Sorry, we couldn't find the user associated with that token. Please try logging in again.", 422);
        }

        Auth::login($user);
        Session::put('zengarden_mobile', 'true');
        //return the view with a cookie containing their api token that will last for 6 hours
        return redirect('/zengarden/home')->withCookie('zengarden_mobile_auth_token', $api_token, 360);
    }

    function ZenployeeCheckLogin($api_token){
	    /*
	    $user = \App\User::where('api_token', $api_token)->first();

	    if($user->is_authen)
	    */
    }
}

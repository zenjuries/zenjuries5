<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class CommunicationLinkController extends Controller
{
    function getCurrentInjury(Request $request, $id){
        Log::debug('using mobile route');
        if(Auth::user()->type === "internal"){
            Log::debug("internal user");
            return redirect()->route('zencare', ['injury_id' => $id]);
        }else{
            Log::debug("agent user");
            $company_id = \App\Injury::where('id', $id)->value('company_id');
            $agent_record = \App\ExternalUserCompanies::where('company_id', $company_id)
                ->where('user_id', Auth::user()->id)
                ->first();
            //check if the user has access to that company EDITED - TYLER
            if($agent_record !== NULL || Auth::user()->getCompany()->id === $company_id){
                Log::debug("has access");
                $request->session()->put('company_id', $company_id);
                return redirect()->route('zencare', ['injury_id' => $id]);
            }else{
                Log::debug("no access");
                Log::debug("company_id = " . var_export($company_id, true));
                return redirect('/zagent_list')->with("status", "Sorry, you don't have permission to view that company. They can give you access by inviting you.");
            }
        }
    }

    function getAdmin(Request $request, $company_id){
        if(Auth::user()->type === "internal"){
            return redirect()->route('zenboard');
        }else{
            $agent_record = \App\ExternalUserCompanies::where('company_id', $company_id)
                ->where('user_id', Auth::user()->id)
                ->first();
                                        //EDITED - TYLER
            if($agent_record !== NULL || Auth::user()->getCompany()->id === $company_id){
                $request->session()->put('company_id', $company_id);
                return redirect()->route('zagent');
            }else{
                return redirect('/zagent_list')->with("status", "Sorry, you don't have permission to view that company. They can give you access by inviting you.");
            }
        }
    }

    function getZenboard(Request $request, $company_id){
        if(Auth::user()->type === "internal"){
            return redirect()->route('zenboard');
        }else{
            $agent_record = \App\ExternalUserCompanies::where('company_id', $company_id)
                ->where('user_id', Auth::user()->id)
                ->first();
                                            //EDITED - TYLER
            if($agent_record !== NULL || Auth::user()->getCompany()->id === $company_id){
                $request->session()->put('company_id', $company_id);
                return redirect()->route('zenboard');
            }else{
                return redirect('/agent/companyList')->with("status", "Sorry, you don't have permission to view that company. They can give you access by inviting you.");
            }
        }
    }

    function getUserProfile(Request $request){
        if(Auth::user()->type === "internal"){
            return redirect('/profile');
        }else{
            if(Auth::user()->is_zagent === 1){
                $company_id = Auth::user()->zagent_id;
            }else{
                $company_id = \App\ExternalUserCompanies::where('user_id', Auth::user()->id)->value('company_id');
            }
            if($company_id !== NULL){
                $request->session()->put('company_id', $company_id);
                return redirect('/profile');
            }else{
                return redirect('/agent/companyList')->with("status", "Please select a company!");
            }
        }
    }

    function getMessages(Request $request){
        if(Auth::user()->type === "internal"){
            return redirect('/mine');
        }else{
            if(Auth::user()->is_zagent === 1){
                $company_id = Auth::user()->zagent_id;
            }else{
                $company_id = \App\ExternalUserCompanies::where('user_id', Auth::user()->id)->value('company_id');
            }
            if($company_id !== NULL){
                $request->session()->put('company_id', $company_id);
                return redirect('/mine');
            }else{
                return redirect('/agent/companyList')->with("status", "Please select a company!");
            }
        }
    }
}

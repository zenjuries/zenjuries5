<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Repositories\PaymentsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Laravel\Cashier\Cashier;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{
    public function __construct(PaymentsRepository $repo)
    {
        $this->repo = $repo;
    }

    function createNewPayment(Request $request){
        $payment = new Payment();
        $this->repo->createNewPayment($request->cost, $request->company_id, $request->payment_type);
    }

    function createNewSubscription(Request $request){
        $this->repo->createNewSubscription($request->company_id);
    }

    function createNewStripeCustomer(Request $request){
        $this->repo->createNewStripeCustomer($request->company_id);
    }

    function updateCard(Request $request){
        $this->repo->updateCard($request->company_id, $request->payment_method);

    }

    function chargeExistingCustomer(Request $request){
        if(\Illuminate\Support\Facades\Request::has('client_id')){
            $this->repo->chargeExistingCustomer($request->payment_id, $request->client_id);
        }else{
            $this->repo->chargeExistingCustomer($request->payment_id);
        }
    }

    // these are not too useful at the moment
    function createSetupIntent(Request $request){
        $companyId = $request->companyId;
        if (!isset($companyId)) return response()->json(['error' => 'Missing Company ID'], 400);
        
        $this->repo->createSetupIntent($companyId);
    }

    function setupIntentRedirect(Request $request){

        // get company
        //EDITED - TYLER
        $company_id = Auth::user()->getCompany()->id;

        // get setup intent

        $intent = $request->setup_intent;

        if (!isset($intent)) return response()->json(['error' => 'Missing Intent'], 400);

        // get payment method
        
    }

    function cancelSubscription(Request $request){
        
        if (!isset($request->companyId)) return response()->json(['error' => 'Missing Company Id'], 400);
        $this->repo->cancelSubscription($request->companyId);
    }

}

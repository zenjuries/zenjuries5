<?php

namespace App\Http\Controllers;

use App\Agency;
use App\Company;
use App\ExternalUserCompanies;
use App\Invitation;
use App\PaymentType;
use App\PolicySubmission;
use App\Repositories\PaymentsRepository;
use App\TeamUser;
use App\User;
use App\Team;
use App\Squad;
use App\SquadMember;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class RegisterController extends Controller
{
    public function __construct(PaymentsRepository $payment_repo)
    {
        $this->payment_repo = $payment_repo;
    }

    function getZAgentPayment(Request $request){

    }

    function createZAgentAccount(Request $request){

    }

    function registerZAgent(Request $request){

        $validation = $request->validate([
            'agency_name' => 'required',
            'street_address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zipcode' => 'required',
            'phone' => 'required|numeric',
            'contact_name' => 'required',
            'email_address' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:8'
        ]);




        //create the Agency
        $agency = new Agency();
        $agency->name = $request->agency_name;
        $agency->agency_type = "ZAgent";
        $agency->image_location = "";
        $agency->new_client = 1;
        $agency->save();

        $company = new Company();
        $company->company_name = $request->agency_name;
        $company->agency_id = $agency->id;
        $company->company_phone = $request->phone;
        $company->mailing_address = $request->street_address;
        $company->mailing_city = $request->city;
        $company->mailing_state = $request->state;
        $company->mailing_zip = $request->zipcode;
        $company->is_agency = 1;
        $company->client_pays = 0;
        $company->policy_submission_id = NULL;
        $company->final_cost = NULL;
        $company->activated = 1;

        //HANDLE BRANDING

        $company->save();

        //*** Create the user ***
        $user = new User();
        $user->name = $request->contact_name;
        $user->email = $request->email_address;
        $user->password = bcrypt($request->password);
        $user->company_id = $company->id;
        $user->type = "agent";
        $user->trial_ends_at = Carbon::now();
        $user->last_read_announcements_at = Carbon::now();
        $user->password_token = NULL;
        $user->is_zagent = 1;
        $user->zagent_id = $company->id;
        $user->zengarden_theme = "themeBamboo";
        $user->save();
        $user->setRandomAvatar();
        $user->setUniqueApiToken();
        $user->setDefaultWidgets();


        // all of this stuff below needs to become a job

        
        //*** Create the team ***
        $team = new Team();
        $team->owner_id = $user->id;
        $team->name = $request->agency_name;
        $team->coupon = NULL;
        $team->valid = 1;
        $team->trial_ends_at = Carbon::now();
        $team->save();
        //*** Update company's team id ***
        $company->team_id = $team->id;
        $company->save();
        //*** Update user's team id ***
        $user->current_team_id = $team->id;
        $user->save();
        //*** Create Team user record to link user with team ***
        $team_user = new TeamUser();
        $team_user->team_id = $team->id;
        $team_user->user_id = $user->id;
        $team_user->role = "owner";
        $team_user->save();
        //create the user link
        $external_user_link = new ExternalUserCompanies();
        $external_user_link->user_id = $user->id;
        $external_user_link->company_id = $company->id;
        $external_user_link->save();

        //*** Create their default squad ***
        $company->createDefaultSquad($user->id);

        $company->assignZenpro();

        // $this->payment_repo->newZagentRegistration($company->id, $request->payment_method);
        if(isset($request->payment_intent)) {
            $this->payment_repo->modifySetupIntent($request->payment_intent, $company->id);
        }

        Auth::login($user);

    }

    function verifyAccountNewPolicyHolder(Request $request){
        $validation = $request->validate([
            'name' => 'required',
            'password' => 'required|confirmed|min:8',
            'company_phone' => 'required|numeric',
            'mailing_street' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
        ]);

        $user = User::where('id', $request->user_id)->where('password_token', $request->pw_token)->first();

        if(is_null($user)){
            return Response::json("Sorry, we couldn\'t authenticate this request. This usually means you've already finished registration.", 422);
        }

        $company = Company::where('id', $request->company_id)->first();
        $policy_submission = $company->getPolicySubmission();

        if($policy_submission->client_pays === 1 && $request->payment_method !== NULL){
            Log::debug("in payment block");
            $payment = \App\Payment::where('company_id', $company->id)->where('paid', 0)
                ->where('payment_type_id', PaymentType::where('payment_type',"New Client")->value('id'))
                ->first();
            $this->payment_repo->createNewStripeCustomer($company->id);
            $this->payment_repo->updateCard($company->id, $request->payment_method);
            $this->payment_repo->chargeExistingCustomer($payment->id, $company->company_name);

            $payment->paid = 1;
            $payment->paid_at = Carbon::now();
            $payment->save();
        }else{
            Log::debug('not in payment block.');
            Log::debug(var_export($policy_submission->client_pays, true));
            Log::debug(var_export($request->payment_method, true));
        }


        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->password_token = NULL;
        $user->save();

        $company->company_phone = $request->company_phone;
        $company->mailing_address = $request->mailing_address;
        $company->mailing_city = $request->city;
        $company->mailing_state = $request->state;
        $company->mailing_zip = $request->zip;
        $company->activated = 1;
        $company->registered_at = Carbon::now();
        $company->save();

        if($company->agency_id !== NULL){
            $company->addAgents();
        }


        Auth::login($user);
        return redirect('/')->with(['msg' => 'Your account has been verified! You\'re ready to login and start using Zenjuries!']);


        /*
         *  name: name,
                   email: email,
                   password: password,
                   password_confirmation: password_confirmation,
                   company_phone: company_phone,
                   mailing_street: mailing_street,
                   city: city,
                   state: state,
                   zip: zip
         */
    }

    function registerNewUser(Request $request){
        $validation = $request->validate([
           'name' => 'required',
           'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:8',
            'token' => 'required'
        ]);

        $invitation = Invitation::where('token', $request->token)->first();
        if(is_null($invitation)){
            return Response::json('Sorry, your invitation could not be found', 422);
        }
        if(!is_null($invitation->user_id)){
            return Response::json("Your invitation has already been used.", 422);
        }

        $team = \App\Team::where('id', $invitation->team_id)->first();
        $company = $team->getCompany();

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->company_id = $company->id;
        $user->current_team_id = $team->id;
        $user->type = $invitation->user_type;
        $user->trial_ends_at = Carbon::now();
        $user->last_read_announcements_at = Carbon::now();
        $user->password_token = NULL;
        $user->zengarden_theme = "themeBamboo";
        if($invitation->is_zagent){
            $user->is_zagent = 1;
            $user->zagent_id = $company->id;
        }else{
            $user->is_zagent = 0;
            $user->zagent_id = NULL;
        }

        $user->save();
        $user->setRandomAvatar();
        $user->setUniqueApiToken();
        $user->setDefaultWidgets();

        $team_user = new TeamUser();
        $team_user->team_id = $team->id;
        $team_user->user_id = $user->id;
        $team_user->role = "employee";
        $team_user->save();

        if($user->is_zagent){
            $user->setZAgentCompanies();
        }

        Auth::login($user);
        return redirect("/");
    }

    function registerInjuredUser(Request $request){
        $validation = $request->validate([
            'email' => 'required|email',
            'password' => 'required|confirmed|min:8',
            'token' => 'required'
        ]);

        $invitation = Invitation::where('token', $request->token)->first();
        if(is_null($invitation)){
            return Response::json('Sorry, your invitation could not be found', 422);
        }
        if(!is_null($invitation->user_id)){
            return Response::json("Your invitation has already been used.", 422);
        }

        $user = \App\User::where('email', $request->email)->first();
        $user->password = bcrypt($request->password);
        $user->zengarden_theme = "themeBamboo";
        $user->save();

        Auth::login($user);
        return redirect("/");
    }
}

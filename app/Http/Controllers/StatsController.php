<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use App\Repositories\StatsRepository;

class StatsController extends Controller
{
    function getStatsView(){
        // if(Auth::user()->is_zagent === 1 && !is_null(Auth::user()->zagent_id)){
        //     $agency_id = \App\Company::where('id', Auth::user()->zagent_id)->value('agency_id');
        //     if(!is_null($agency_id)){
        //         return view('pages.stats', ['agency_id' => $agency_id, 'load_company' => true, 'company_id' => NULL, 'company_name' => NULL, 'logo_location' => NULL]);
        //     }
        // }else{
        //     $company = \App\Company::where('id', Auth::user()->getCompany()->id)->first();
        //     return view('pages.stats', ['agency_id' => NULL, 'load_company' => true, 'company_id' => $company->id, 'company_name' => $company->company_name, 'logo_location' => $company->logo_location]);
        // }
        return view('pages.stats');
    }

    function getAgencyStats(Request $request){
        $agency_id = $request->input('agency_id');
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        Log::debug('start_date: ' . $start_date);
        Log::debug('end_date: ' . $end_date);
        $agency = \App\Agency::where('id', $agency_id)->first();
        if(!is_null($agency)){
            $stats_repo = new StatsRepository();
            $injuries = $stats_repo->getAgencyInjuries($agency_id, $start_date, $end_date);

            $stats_array = $stats_repo->returnStatsArray($injuries);

            return Response::json($stats_array);
        }else return false;
    }

    function getCompanyStats(Request $request){
        $company_id = $request->input('company_id');
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');

        Log::debug('start_date: ' . $start_date);
        Log::debug('end_date: ' . $end_date);

        $company = \App\Company::where('id', $company_id)->first();
        if(!is_null($company)){
            $stats_repo = new StatsRepository();
            $injuries = $stats_repo->getCompanyInjuries($company_id, $start_date, $end_date);
            $stats_array = $stats_repo->returnStatsArray($injuries);
            return Response::json($stats_array);
        }else{
            Log::debug('company_id is null');
            return false;
        }
    }

    function getZenboardStats(Request $request){
        $company_id = $request->input('company_id');
        $start_date = null;
        $end_date = null;

        $company = \App\Company::where('id', $company_id)->first();
        if(!is_null($company)){
            $stats_repo = new StatsRepository();
            $injuries = $stats_repo->getCompanyInjuries($company_id, $start_date, $end_date);
            Log::debug($injuries);
            $stats_array = $stats_repo->returnStatsArray($injuries);
            return Response::json($stats_array);
        }else{
             return false;
        }
    }
}

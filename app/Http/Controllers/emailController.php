<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Verified;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class emailController extends Controller
{

    public function emailAll(Request $request){
        $emailList = $request->input('emailList');
        $subject = $request->input('subject');
        $messageText = $request->input('msgText');
        Log::debug('emailAll');
        Log::debug($messageText);
        Log::debug($subject);
        Log::debug($emailList);

        // Mail::send('emailTemplates.sendCustomEmail', ['emails' => $emailList, 'subject' => $subject, 'message' => $messageText], function($m) use ($emailList, $subject){
           Mail::send(['html'=> 'emailTemplates.sendCustomEmail'],['msg' => $messageText], function($m) use ($emailList, $subject){
            foreach($emailList as $emails){
                $m->to($emails)->Subject($subject);
            }
        });
         Log::debug("return view");

    }

}

<?php

namespace App\Http\Middleware;

use App\User;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        //mobile auth
        /*
        if(isset($_COOKIE['zengarden_mobile_auth_token'])){
            $token = Crypt::decrypt($_COOKIE['zengarden_mobile_auth_token']);

            $user = User::where('api_token', $token)->first();
            if(!is_null($user)){
                return redirect('/mobile/sessionExpired');
            }
        }
        */

        if (! $request->expectsJson()) {
            return route('login');
        }
    }
}

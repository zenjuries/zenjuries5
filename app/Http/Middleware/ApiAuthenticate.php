<?php

namespace App\Http\Middleware;

use Closure;
use \App\ApiKey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $permission)
    {

        // check if they pass token
        if ($request->bearerToken() == null) return response()->json(["error" => "Invalid Bearer Token!"], 400);

        // find token
        $apiKey = ApiKey::where('api_token', '=', $request->bearerToken())->first();

        // check if token exists
        if (!$apiKey) return response()->json(["error" => "Bearer Token Not Found!"], 400);

        // check permission level
        if (!str_contains($apiKey->permissions, $permission)) return response()->json(["error" => "Missing permissions to access this resource!"], 401);

        // pass through
        return $next($request);
    }
}

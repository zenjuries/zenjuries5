<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;
use Illuminate\Http\Request;
use Sentry\State\Scope;
use App\User;

class SentryTags
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(app()->bound('sentry')) {
            \Sentry\configureScope(function (Scope $scope): void {
                $scope->setTag('instance', env('COMPANY_SHORT_NAME', 'zenjuries'));

                if(Auth::check())
                {
                    $scope->setUser([
                        'id'    => Auth::user()->id,
                        'email' => Auth::user()->email,
                        'name'  => Auth::user()->name,
                    ]);
                    $scope->setTag('user.name', Auth::user()->name);

                    $company = Auth::user()->getCompany();
                    if ($company) {
                        $scope->setTag('company.id', $company->id);
                        $scope->setTag('company.name', $company->company_name);
                    };

                }
                else {
                    $scope->setTag('user.name', 'unauthenticated');
                }
            });
        }

        return $next($request);
    }
}

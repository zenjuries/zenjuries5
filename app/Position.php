<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    //
    protected $table = 'positions';

    public $timestamps = false;

    public function user(){
        return $this->hasMany('App\User');
    }

    public function communication_positions(){
        return $this->hasMany('App/Communication_Position');
    }

    public function squadMembers(){
        return $this->hasMany('App/SquadMember');
    }
}

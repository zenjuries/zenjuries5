<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncompleteInjuryLocation extends Model
{
    //
    protected $table = 'incomplete_injury_locations';

    public $timestamps = false;
}

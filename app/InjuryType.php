<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InjuryType extends Model
{
    //
    protected $table = 'injury_types';

    public $timestamps = false;
}

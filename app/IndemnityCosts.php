<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndemnityCosts extends Model
{
    //
    protected $table = 'indemnity_costs';

    //take the user making the update, the ID for the injury, and the new value
    function updateCost($user_id, $injury_id, $value){
        //we need to know if the costs are equal, if not then there's no change needed
        $costs_are_equal = false;
        //get the current value for that injury
        $current_value = IndemnityCosts::where('injury_id', $injury_id)->where('current', 1)->first();
        //there might not be a current value set, so we want to check for null
        if($current_value !== NULL){
            if($current_value->cost == $value){
                //if the costs are equal we don't have to update anything
                $costs_are_equal = true;
            }else{
                //if the costs have changed, mark the previous record as no longer being current
                $current_value->current = 0;
                $current_value->save();
            }
        }
        //if the cost has changed, create a new record for it
        if($costs_are_equal === false){
            $this->injury_id = $injury_id;
            $this->updater_id = $user_id;
            $this->cost = $value;
            $this->current = 1;
            $this->save();
        }

        //we want to return the old value if an update was made, and false if there was no change to values
        if($costs_are_equal === false){

            if($current_value === NULL){
                return "0";
            }else{
                return $current_value->cost;
            }
        }else{

            return false;
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncompleteInjuryType extends Model
{
    //
    protected $table = 'incomplete_injury_types';

    public $timestamps = false;
}

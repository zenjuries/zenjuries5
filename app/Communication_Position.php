<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Communication_Position extends Model
{
    //
    protected $table = 'communication_positions';

    public function communication(){
        return $this->belongsTo('App/Communication');
    }

    public function position(){
        return $this->belongsTo('App/Position');
    }

    public function communication_template(){
        return $this->belongsTo('App/Communication_Template');
    }
}

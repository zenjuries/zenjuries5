<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InjuryFile extends Model
{
	use SoftDeletes;
    //
    protected $table = 'injury_files';
    
    protected $dates = ['deleted_at'];
}

<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DiaryPost extends Model
{
    //
    protected $table = 'diary_posts';

    public function getFormattedDate(){
        return Carbon::create($this->created_at)->toFormattedDateString();
    }

    public function user(){
        return $this->belongsTo('App/User');
    }

    public function injury(){
        return $this->belongsTo('App/Injury');
    }

    public function diaryPosts(){
        return $this->hasMany('App/DiaryPost');
    }


}

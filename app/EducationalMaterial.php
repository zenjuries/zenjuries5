<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationalMaterial extends Model
{
    //
    protected $table = 'educational_material';
}

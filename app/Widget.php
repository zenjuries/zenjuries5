<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    use HasFactory;

    function newInjuryWidget($injury_id){
        $this->widget_name = "Injury Widget";
        $this->type = "injury";
        $this->description = "Injury Widget representing a specific claim";
        $this->injury_id = $injury_id;
        $this->save();
    }
}

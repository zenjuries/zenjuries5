<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DefaultSquadIcons extends Model
{
    protected $table = "default_squad_icons";
}

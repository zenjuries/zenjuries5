<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserWidget extends Model
{
    use HasFactory;

    function createNewUserWidget($user_id, $widget_id){
        $this->user_id = $user_id;
        $this->widget_id = $widget_id;
        $this->save();
    }

    function deleteUserWidget(){
        $this->delete();
    }
}

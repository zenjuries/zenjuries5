<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trigger extends Model
{
    //
    public function communication_templates(){
        return $this->hasMany('App\Communication_Template');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;


class Agency extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    function getCompanies($type = "all"){
        if($type === "activated"){
            $companies = \App\Company::where('agency_id', $this->id)->where('is_agency', 0)->where('activated', 1)->get();
        }else if($type === "pending"){
            $companies = \App\Company::where('agency_id', $this->id)->where('is_agency', 0)->where('activated', 0)->get();
        }else{
            $companies = \App\Company::where('agency_id', $this->id)->where('is_agency', 0)->get();
        }
        foreach($companies as $company){
            $company->total_injuries = count($company->getInjuries());
            $company->open_injuries = count($company->getInjuries("open"));
            $company->resolved_injuries = count($company->getInjuries("resolved"));
            $company->critical_injuries = count($company->getCriticalInjuries());
            $company->owner = $company->getCompanyOwner();
            $company->invited_at = $company->invite_resent_on;
            if(is_null($company->invited_at)){
                $company->invited_at = $company->created_at;
            }
            $company->invited_at = Carbon::createFromFormat("Y-m-d H:i:s", $company->invited_at)->toFormattedDateString();
        }

        return $companies;
    }

    function debugZendex(){
        if($this->agency_type === "ZAgent") {
            /*
            Zendex score is a total of the scores an agency receives based on several pieces of criteria.
            First Report - 20 points. How quickly was the 1st report of injury delivered
            Prompt Report - 20 points. How quickly after the injury occured was it reported
            Claim Values Updated - 10 points. Does the agency keep the claim values updated
            Final Costs Updated - 10 points. Does the agency update final costs for its injuries
            Reduced Reserves - 10 points. how much less is the final cost of the injury compared to its highest cost with reserves
            Updated Employee Status - 10 points. Are the employee status' kept updated
            Injury Info Added - 10 points. Points for filling in injury details in Zenjuries
            Active Chatter - 10 points. Points for chat posts on tree of communications
            */
            $injuries = $this->getAgencyInjuries();
            if (count($injuries)) {
                $total_points = 0;
                //FIRST REPORT
                $first_report_points = $this->getFirstReportZendexPoints($injuries);
                var_dump("Your first report points are " . $first_report_points);
                $total_points += $first_report_points;
                //PROMPT REPORT
                $prompt_reporting_points = $this->getPromptReportingPoints($injuries);
                var_dump("Your prompt reporting points are " . $prompt_reporting_points);
                $total_points += $prompt_reporting_points;
                //CLAIM VALUES UPDATED
                $claim_value_points = $this->getClaimValuePoints($injuries);
                var_dump("Your claim value points are " . $claim_value_points);
                $total_points += $claim_value_points;
                //FINAL COSTS UPDATED
                $final_cost_points = $this->getFinalCostPoints($injuries);
                var_dump("Your final cost points are " . $final_cost_points);
                $total_points += $final_cost_points;
                //REDUCED RESERVES
                $reduced_reserves = $this->getReducedReservePoints($injuries);
                var_dump("Your reduced reserve points are " . $reduced_reserves);
                $total_points += $reduced_reserves;
                //UPDATED EMPLOYEE STATUS
                $employee_status_points = $this->getEmployeeStatusPoints($injuries);
                var_dump("Your employee status points are " . $employee_status_points);
                $total_points += $employee_status_points;
                //INJURY INFO ADDED
                $injury_info_points = $this->getInjuryInfoPoints($injuries);
                var_dump("Your injury info points are " . $injury_info_points);
                $total_points += $injury_info_points;
                //ACTIVE CHATTER
                $chat_points = $this->getInjuryChatPoints($injuries);
                var_dump("Your chat points are " . $chat_points);
                $total_points += $chat_points;
                //ADD A 20 POINT CURVE
                $total_points += 20;
                //100 is the max, so if the curve brings the score above that then reset $total_points to 100.
                if ($total_points > 100) {
                    $total_points = 100;
                }
                var_dump("Your ZenDex is " . $total_points . ".");
                return $total_points;
            } else {
                return false;
            }
        }else return false;
    }

    function calculateZendex(){
        if($this->agency_type === "ZAgent") {
            /*
            Zendex score is a total of the scores an agency receives based on several pieces of criteria.
            First Report - 20 points. How quickly was the 1st report of injury delivered
            Prompt Report - 20 points. How quickly after the injury occured was it reported
            Claim Values Updated - 10 points. Does the agency keep the claim values updated
            Final Costs Updated - 10 points. Does the agency update final costs for its injuries
            Reduced Reserves - 10 points. how much less is the final cost of the injury compared to its highest cost with reserves
            Updated Employee Status - 10 points. Are the employee status' kept updated
            Injury Info Added - 10 points. Points for filling in injury details in Zenjuries
            Active Chatter - 10 points. Points for chat posts on tree of communications
            */
            $injuries = $this->getCurrentAgencyInjuries();
            if (count($injuries)) {
                $points_array = array();
                $total_points = 0;
                //FIRST REPORT
                $first_report_points = $this->getFirstReportZendexPoints($injuries);
                $points_array['first_report_points'] = $first_report_points;
                $total_points += $first_report_points;
                //PROMPT REPORT
                $prompt_reporting_points = $this->getPromptReportingPoints($injuries);
                $points_array['prompt_reporting_points'] = $prompt_reporting_points;
                $total_points += $prompt_reporting_points;
                //CLAIM VALUES UPDATED
                $claim_value_points = $this->getClaimValuePoints($injuries);
                $points_array['claim_value_points'] = $claim_value_points;
                $total_points += $claim_value_points;
                //FINAL COSTS UPDATED
                $final_cost_points = $this->getFinalCostPoints($injuries);
                $points_array['final_cost_points'] = $final_cost_points;
                $total_points += $final_cost_points;
                //REDUCED RESERVES
                $reduced_reserves = $this->getReducedReservePoints($injuries);
                //$points_array['reduced_reserves'] = $reduced_reserves;
                $total_points += $reduced_reserves;
                //UPDATED EMPLOYEE STATUS
                $employee_status_points = $this->getEmployeeStatusPoints($injuries);
                $points_array['employee_status_points'] = $employee_status_points;
                $total_points += $employee_status_points;
                //INJURY INFO ADDED
                $injury_info_points = $this->getInjuryInfoPoints($injuries);
                $points_array['injury_info_points'] = $injury_info_points;
                $total_points += $injury_info_points;
                //ACTIVE CHATTER
                $chat_points = $this->getInjuryChatPoints($injuries);
                $points_array['chat_points'] = $chat_points;
                $total_points += $chat_points;
                //var_dump($points_array);
                //get an element from the array with the lowest point total
                $worst_category = min(array_keys($points_array, min($points_array)));
                if($worst_category === "first_report_points"){
                    $message = "Make sure the Injury Alert is delivered ASAP after reporting the claim!";
                }else if($worst_category === "prompt_reporting_points"){
                    $message = "Make sure to report an injury in Zenjuries as soon as it occurs! Fast reporting time is key to reducing claim costs.";
                }else if($worst_category === "claim_value_points"){
                    $message = "Make sure the claim costs stay updated on the Tree of Communications page!";
                }else if($worst_category === "final_cost_points"){
                    $message = "Make sure resolved claims have their Final Costs updated!";
                }else if($worst_category === "employee_status_points"){
                    $message = "Make sure all injuries have the Employee Status updated!";
                }else if($worst_category === "injury_info_points"){
                    $message = "Make sure all the injury details are filled out for an injury!";
                }else if($worst_category === "chat_points"){
                    $message = "Make sure your teams are actively participating in the tree of communications!";
                }else{
                    $message = "Follow Zenjuries best practices to improve your Zendex Rating.";
                }
                //ADD A 20 POINT CURVE
                $total_points += 20;
                //100 is the max, so if the curve brings the score above that then reset $total_points to 100.
                if ($total_points > 100) {
                    $total_points = 100;
                }
                $zendex_array = array();
                $zendex_array['points'] = $total_points;
                $zendex_array['message'] = $message;
                return $zendex_array;
            } else {
                //return false;
                $zendex_array = array();
                $zendex_array['points'] = 70;
                $zendex_array['message'] = "You currently have no injuries.";
                return $zendex_array;
            }
        }
    }

    function getFirstReportZendexPoints($injuries){
        $total_points_count = 0;
        $injury_count = count($injuries);//we'll be subtracting recently reported injuries from this
        if($injury_count > 0) {
            foreach ($injuries as $injury) {
                $injury_created_at = Carbon::createFromFormat("Y-m-d H:i:s", $injury->created_at);
                if ($injury->action_taken === 1 && !is_null($injury->action_taken_at)) {
                    $delivery_time = $injury_created_at->diffInHours(Carbon::createFromFormat("Y-m-d H:i:s", $injury->action_taken_at));
                    // var_dump(Carbon::createFromFormat("Y-m-d H:i:s", $injury->created_at));
                    // var_dump(Carbon::createFromFormat("Y-m-d H:i:s", $injury->action_taken_at));
                    // var_dump($delivery_time);
                    if ($delivery_time === 0) {
                        $total_points_count += 20;
                    } else if ($delivery_time === 1) {
                        $total_points_count += 19;
                    } else if ($delivery_time === 2) {
                        $total_points_count += 18;
                    } else if ($delivery_time === 3) {
                        $total_points_count += 17;
                    } else if ($delivery_time === 4) {
                        $total_points_count += 16;
                    } else if ($delivery_time === 5) {
                        $total_points_count += 15;
                    } else if ($delivery_time <= 24) {
                        $total_points_count += 10;
                    } else if ($delivery_time <= 48) {
                        $total_points_count += 5;
                    }
                } else {
                    /*
                    if($injury_created_at->diffInHours(Carbon::now()) <= 24){
                        //subtract one from the injury count, because we aren't counting injuries that are less than a day old
                        $injury_count--;
                    }
                    */
                }
            }
            if ($total_points_count > 0) {
                $points = $total_points_count / $injury_count;
                $points = round($points, 0);
            }else{
                $points = 0;
            }
            return $points;
        }else return 10;
    }

    function getPromptReportingPoints($injuries){
        $total_points = 0;
        foreach($injuries as $injury){
            $injury_reported_at = Carbon::createFromFormat("Y-m-d H:i:s", $injury->created_at);
            $injury_occurred_at = Carbon::createFromFormat("m-d-Y g:i a", $injury->injury_date);
            if(!is_null($injury_reported_at) && !is_null($injury_occurred_at)){
                $hours_before_reporting = $injury_reported_at->diffInHours($injury_occurred_at);
                //var_dump($injury->id);
                //var_dump($injury_reported_at);
                //var_dump($injury_occurred_at);
                //var_dump($hours_before_reporting);
                if($hours_before_reporting < 12){//full points for reporting within a day
                    $total_points+= 20;
                }else if($hours_before_reporting < 24){
                    $total_points+= 15;
                }else if($hours_before_reporting < 36){
                    $total_points+= 10;
                }else if($hours_before_reporting < 48){//half points for reporting within 2 days
                    $total_points+= 5;
                }//no points for taking longer than 2 days
            }
        }
        if(count($injuries)) {
            $points = $total_points / count($injuries);
            $points = round($points, 0);
            return $points;
        }else{
            return 10;
        }
    }

    function getClaimValuePoints($injuries){
        if(count($injuries)) {
            $total_points = 0;
            foreach ($injuries as $injury) {
                $claim_value_updates = 0;
                $claim_value_updates += DB::table('medical_costs')->where('injury_id', $injury->id)->count();
                $claim_value_updates += DB::table('legal_costs')->where('injury_id', $injury->id)->count();
                $claim_value_updates += DB::table('indemnity_costs')->where('injury_id', $injury->id)->count();
                $claim_value_updates += DB::table('miscellaneous_costs')->where('injury_id', $injury->id)->count();
                $claim_value_updates += DB::table('reserve_medical_costs')->where('injury_id', $injury->id)->count();
                $claim_value_updates += DB::table('reserve_legal_costs')->where('injury_id', $injury->id)->count();
                $claim_value_updates += DB::table('reserve_indemnity_costs')->where('injury_id', $injury->id)->count();
                $claim_value_updates += DB::table('reserve_miscellaneous_costs')->where('injury_id', $injury->id)->count();
                if ($claim_value_updates >= 8) {
                    $total_points += 10;
                } else {
                    $total_points += $claim_value_updates;
                }
            }
            $points = $total_points / count($injuries);
            $points = round($points, 0);
            return $points;
        }else{
            return 5;
        }
    }

    function getFinalCostPoints($injuries){
        $injury_collection = collect($injuries)->where('resolved', 1);
        //var_dump($injury_collection);
        $resolved_injuries_count = $injury_collection->count();
        if($resolved_injuries_count > 0) {
            //var_dump($resolved_injuries_count);
            $resolved_injuries_with_final_cost_count = $injury_collection->filter(function ($injury) {
                return !is_null($injury->final_cost);
            })->count();
            //var_dump($resolved_injuries_with_final_cost_count);
            $percent_with_final_cost = round($resolved_injuries_with_final_cost_count / $resolved_injuries_count, 2) * 100;
            //var_dump($percent_with_final_cost);
            if ($percent_with_final_cost >= 75) {
                $points = 10;
            } else if ($percent_with_final_cost >= 50) {
                $points = 7;
            } else if ($percent_with_final_cost >= 25) {
                $points = 4;
            } else {
                $points = 0;
            }
        }else{
            $points = 5;
        }
        return $points;
    }

    function getEmployeeStatusPoints($injuries){
        $injury_collection = collect($injuries);
        //var_dump($injury_collection);
        $injury_count = $injury_collection->count();
        if($injury_count > 0) {
            //var_dump($injury_count);
            $injuries_with_employee_status_count = $injury_collection->filter(function ($injury) {
                return !is_null($injury->employee_status);
            })->count();
            //var_dump($injuries_with_employee_status_count);
            $percent_with_status = round($injuries_with_employee_status_count / $injury_count, 2) * 100;
            //var_dump($percent_with_status);
            if ($percent_with_status >= 75) {
                $points = 10;
            } else if ($percent_with_status >= 50) {
                $points = 7;
            } else if ($percent_with_status >= 25) {
                $points = 4;
            } else {
                $points = 0;
            }
        }else{
            $points = 5;
        }
        return $points;
    }

    function getInjuryInfoPoints($injuries){
        $total_injury_points = 0;
        $injury_count = count($injuries);
        if($injury_count > 0) {
            foreach ($injuries as $injury) {
                $injury_points = 0;
                if (!is_null($injury->claim_number)) {
                    $injury_points += 4;
                    //var_dump("Claim Number Info Points: 4");
                }
                //check for adjuster info
                if (!is_null($injury->adjuster_name) && !is_null($injury->adjuster_email)) {
                    $injury_points += 4;
                   // var_dump("Adjuster Info: 4");
                } else {
                    //check for adjuster on the team
                    $adjuster = DB::table('squad_members')->where('squad_id', $injury->squad_id)->where('position_id', 4)->first();
                    if (!is_null($adjuster)) {
                        $injury_points += 4;
                        //var_dump("Adjuster on team");
                    }
                }
                //check for employee status
                if (!is_null($injury->employee_status)) {
                    $injury_points += 2;
                    //var_dump("Employee Status: 2");
                }
                $total_injury_points += $injury_points;
            }
            //var_dump($total_injury_points);
            //var_dump($injury_count);
            $points = ($total_injury_points / $injury_count);
            $points = round($points, 0);
            //var_dump($points);
        }else{
            $points = 5;
        }
        return $points;
    }

    function getInjuryChatPoints($injuries){
        $total_points = 0;
        $injury_count = count($injuries);
        if($injury_count > 0) {
            foreach ($injuries as $injury) {
                $chat_count = DB::table('tree_post')->where('injury_id', $injury->id)->where('type', 'chatPost')->count();
                if ($chat_count >= 5) {
                    $injury_points = 10;
                } else if ($chat_count === 4) {
                    $injury_points = 8;
                } else if ($chat_count === 3) {
                    $injury_points = 6;
                } else if ($chat_count === 2) {
                    $injury_points = 4;
                } else if ($chat_count === 1) {
                    $injury_points = 2;
                } else {
                    $injury_points = 0;
                }
                $total_points += $injury_points;
            }
            $points = ($total_points / $injury_count);
            $points = round($points, 0);
            // var_dump($total_points);
            //var_dump($injury_count);
            //var_dump($points);
        }else{
            $points = 5;
        }
        return $points;
    }

    function getReducedReservePoints($injuries){
        $injury_collection = collect($injuries)->where('resolved', 1)->filter(function($injury){
            return !is_null($injury->final_cost);
        });
        $injury_count = $injury_collection->count();
        $point_total = 0;
        if($injury_count > 0) {
            foreach ($injury_collection as $injury) {
                $injury_points = NULL;

                $paid_medical = DB::table('medical_costs')->where('injury_id', $injury->id)->get();
                $paid_medical = (is_null($paid_medical) ? 0 : collect($paid_medical)->max('cost'));

                $paid_indemnity = DB::table('indemnity_costs')->where('injury_id', $injury->id)->get();
                $paid_indemnity = (is_null($paid_indemnity) ? 0 : collect($paid_indemnity)->max('cost'));

                $paid_legal = DB::table('legal_costs')->where('injury_id', $injury->id)->get();
                $paid_legal = (is_null($paid_legal) ? 0 : collect($paid_legal)->max('cost'));

                $paid_misc = DB::table('miscellaneous_costs')->where('injury_id', $injury->id)->get();
                $paid_misc = (is_null($paid_misc) ? 0 : collect($paid_misc)->max('cost'));

                $reserve_medical = DB::table('reserve_medical_costs')->where('injury_id', $injury->id)->get();
                $reserve_medical = (is_null($reserve_medical) ? 0 : collect($reserve_medical)->max('cost'));

                $reserve_indemnity = DB::table('reserve_indemnity_costs')->where('injury_id', $injury->id)->get();
                $reserve_indemnity = (is_null($reserve_indemnity) ? 0 : collect($reserve_indemnity)->max('cost'));

                $reserve_legal = DB::table('reserve_legal_costs')->where('injury_id', $injury->id)->get();
                $reserve_legal = (is_null($reserve_legal) ? 0 : collect($reserve_legal)->max('cost'));

                $reserve_misc = DB::table('reserve_miscellaneous_costs')->where('injury_id', $injury->id)->get();
                $reserve_misc = (is_null($reserve_misc) ? 0 : collect($reserve_misc)->max('cost'));

                $total_claim_cost = $paid_medical + $paid_indemnity + $paid_legal + $paid_misc + $reserve_medical + $reserve_legal + $reserve_indemnity + $reserve_misc;
                if($total_claim_cost !== 0){
                    /*
                    var_dump("A New Injury");
                    var_dump("Total Cost: " . $total_claim_cost);
                    var_dump("Final Cost: " . $injury->final_cost);
                    */
                    $difference = ($total_claim_cost - $injury->final_cost);
                   // var_dump("Difference: " . $difference);
                    if($difference > 0){
                       // var_dump("Reduced cost");
                        $percent_difference = round($difference/$total_claim_cost, 2) * 100;
                       // var_dump($percent_difference);
                        if($percent_difference >= 50){
                            $injury_points = 10;
                        }else if($percent_difference >= 40){
                            $injury_points = 9;
                        }else if($percent_difference >= 30){
                            $injury_points = 8;
                        }else if($percent_difference >= 20){
                            $injury_points = 7;
                        }else if($percent_difference >= 1){
                            $injury_points = 6;
                        }else{
                            $injury_points = 5;
                        }
                    }else if($difference < 0){
                       // var_dump("Increased cost");
                        $percent_difference = round((abs($difference)/$total_claim_cost), 2) * 100;
                       // var_dump($percent_difference);
                        if($percent_difference >= 50){
                            $injury_points = 0;
                        }else if($percent_difference >= 40){
                            $injury_points = 1;
                        }else if($percent_difference >= 30){
                            $injury_points = 2;
                        }else if($percent_difference >= 20){
                            $injury_points = 3;
                        }else if($percent_difference >= 1){
                            $injury_points = 4;
                        }else{
                            $injury_points = 5;
                        }
                    }else{
                        $injury_points = 5;
                    }
                    /*
                    $final_cost_change = round(($total_claim_cost / $injury->final_cost), 2);
                    var_dump("New");
                    var_dump($total_claim_cost);
                    var_dump($injury->final_cost);
                    var_dump($final_cost_change);
                    if($final_cost_change > 0){
                       // var_dump("reduced cost by " . $final_cost_change . " percent");
                    }else{
                       // var_dump("increased cost by " . ($final_cost_change . 100) . " percent");
                    }
                    */
                }
                //$injury_points = 5;
                //$point_total+= $injury_points;
                if(is_null($injury_points)){
                    $injury_points = 5;
                }
                $point_total+= $injury_points;
                //var_dump("Injury Points " . $injury_points);
            }
            $points = round(($point_total/$injury_count), 0);
           // var_dump($points);
            return $points;
        }else{
            //if they don't have any relevant injuries then they get 5 points, which is baseline for this
            return 5;
        }
        //$points = round(($point_total/$injury_count), 0);
        //return $points;

    }


    function getAgencyInjuries(){
        $injuries = DB::table('companies')->where('agency_id', $this->id)
            ->join('injuries', 'companies.id', '=', 'injuries.company_id')
            ->where('injuries.type', 'Work Comp')
            ->where('injuries.created_at', '<=', Carbon::now()->subWeek())
            ->join('first_report_of_injuries', 'injuries.id', '=', 'first_report_of_injuries.injury_id')
            ->select('injuries.*', 'first_report_of_injuries.action_taken', 'first_report_of_injuries.action_taken_at')->get();
        //var_dump($injuries);
        //Log::debug(var_export($injuries, true));
        return $injuries;
        /*
        $agency_companies = DB::table('companies')->where('agency_id', $this->id)->select('id')->get();
        Log::debug(var_export($agency_companies, true));
        var_dump($agency_companies);
        $injuries = DB::table('injuries')->whereIn('company_id', $agency_companies)->get();
        var_dump($injuries);
        Log::debug(var_export($injuries, true));
        return $injuries;
        */
    }

    function getCurrentAgencyInjuries(){
        $injuries = DB::table('companies')->where('agency_id', $this->id)
            ->join('injuries', 'companies.id', '=', 'injuries.company_id')
            ->where('injuries.type', 'Work Comp')
            ->where('injuries.created_at', '<=', Carbon::now()->subWeek())
            //->where(Carbon::createFromFormat('m-d-Y g:i a', 'injuries.injury_date'), '>=', 'companies.created_at')
            ->join('first_report_of_injuries', 'injuries.id', '=', 'first_report_of_injuries.injury_id')
            ->select('injuries.*', 'companies.created_at as company_created_at', 'first_report_of_injuries.action_taken', 'first_report_of_injuries.action_taken_at')->get();
        $injury_array = array();
        foreach($injuries as $injury){
            $injury_date =  Carbon::createFromFormat("m-d-Y g:i a", $injury->injury_date);
            $company_created_at = Carbon::createFromFormat("Y-m-d H:i:s", $injury->company_created_at);
            if($injury_date->gt($company_created_at)){
                array_push($injury_array, $injury);
            }
        }
        //var_dump($injury_array);
        return $injury_array;
    }

    function setZendexSessionData(){
        $zendex = $this->calculateZendex();
        session(['zendex_score' => $zendex['points']]);
        session(['zendex_message' => $zendex['message']]);
        session(['zendex_agency_id' => $this->id]);
    }

    function displayZendex($zendex_score, $user){
        if($this->agency_type === "ZAgent") {
            if ($zendex_score !== false) {
                if ($user->is_zagent === 1) {
                    $agency_id = DB::table('companies')->where('id', $user->zagent_id)->value('agency_id');
                    if (!is_null($agency_id)) {
                        if ($agency_id === $this->id) {
                            $user_is_zagent = true;
                        } else {
                            $user_is_zagent = false;
                        }
                    } else {
                        $user_is_zagent = true;
                    }
                } else {
                    $user_is_zagent = false;
                }
                if ($zendex_score > 70 || $user_is_zagent) {
                    if ($zendex_score === 100) {
                        $level = "level10";
                    } else if ($zendex_score >= 90) {
                        $level = "level9";
                    } else if ($zendex_score >= 80) {
                        $level = "level8";
                    } else if ($zendex_score >= 70) {
                        $level = "level7";
                    } else if ($zendex_score >= 60) {
                        $level = "level6";
                    } else if ($zendex_score >= 50) {
                        $level = "level5";
                    } else if ($zendex_score >= 40) {
                        $level = "level4";
                    } else if ($zendex_score >= 30) {
                        $level = "level3";
                    } else if ($zendex_score >= 20) {
                        $level = "level2";
                    } else {
                        $level = "level1";
                    }
                    $zendex_display = "<div class='headerZendexScore'>
                    <div class='zendexScore tiny $level'>
	                    <div class='scorecolor'>
	            	        <div class='zdimage'><div class='value'>$zendex_score</div></div>
	                    </div>
                    </div>
		        </div> ";
                    echo $zendex_display;
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    function echoAgencyType(){
        if($this->agency_type === "ZAgent"){
            echo "Z-Agent";
        }else if($this->agency_type === "ZCare" || $this->agency_type === "ZCare_Both" || $this->agency_type === "ZCare_Locations"){
            echo "Z-Care";
        }
    }

    function echoAgencyTypeMemberTitle($plural = false){
        if($this->agency_type === "ZAgent"){
            echo ($plural === false ? "Z-Agent" : "Z-Agents");
        }else if($this->agency_type === "ZCare" || $this->agency_type === "ZCare_Both" || $this->agency_type === "ZCare_Locations"){
            echo ($plural === false ? "Care Provider" : "Care Providers");
        }
    }

    function getInactiveCompanies(){
        $inactive_companies = \App\Company::where('agency_id', $this->id)
            ->where('companies.activated', 0)
            ->join('teams', 'companies.team_id', '=', 'teams.id')
            ->join('users', 'teams.owner_id', '=', 'users.id')
            ->select('companies.*', 'users.email', 'users.name as user_name')
            ->get();
        return $inactive_companies;
    }

    function isBroker(){
        $company = \App\Company::where('is_agency', 1)->where('is_broker', 1)->where('agency_id', $this->id)->first();
        if(is_null($company)){
            return false;
        }else return true;
    }
}

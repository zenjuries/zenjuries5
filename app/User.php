<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Agency;
use Illuminate\Support\Facades\URL;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    //use CanJoinTeams;

    use SoftDeletes;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'injured', 'position'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'authy_id',
        'country_code',
        //'phone',
        'card_brand',
        'card_last_four',
        'card_country',
        'billing_address',
        'billing_address_line_2',
        'billing_city',
        'billing_zip',
        'billing_country',
        'extra_billing_information',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trial_ends_at' => 'date',
        'uses_two_factor_auth' => 'boolean',
    ];

    protected $dates = ['deleted_at'];

    public function sendEmail($template, $subject){
        $that = $this;
        Mail::send('emails.' . $template, ['user' => $that, 'subject' => $subject], function ($message) use ($that, $subject) {
            $message->to($that->email, $that->name)->subject($subject);
        });
    }

    public function sendSMS($message){
        $that = $this;
        $phonenumber = $that->phone;
        if(isset($phonenumber)){
            Twilio::message($phonenumber, $message);
        }
    }

    public function getCompany(){
        if(session('company_id')){
            $company = \App\Company::where('id', session('company_id'))->first();
        }else{
            $company = \App\Company::where('id', $this->company_id)->first();
        }

        // If there's no company_id, try to get the company from the agency id
        if($company === NULL && $this->company_id === NULL && $this->zagent_id != NULL && $this->type === "agent"){
            $agency_id = $this->zagent_id;
            $company = \App\Company::where('id', $agency_id)->first();
        }

        // If there's no company_id or zagent_id, try to get a company from another source
        if ($company === NULL && $this->type === "agent"){
            // First try using ExternalCompanies table
            $company =ExternalUserCompanies::where('user_id', $this->id)->first();

            // Try using team id
            if ($company === NULL){
                $company = \App\Company::where('team_id', $this->current_team_id)->where('is_agency', 1)->first();
            }
        }
        return $company;
    }

    public function getFirstName(){
        //$name = explode(" ", $this->name, 2);
        // removed the 2 to fix an error, invalid length causing 500s
        $name = explode(" ", $this->name);
        return $name[0];
    }

    public function getUserCompanies(){
        if($this->type === "agent"){
            $company_array = array();
            $external_companies = ExternalUserCompanies::where('user_id', $this->id)->get();
            foreach ($external_companies as $external_company){
                $company = $external_company->getCompany();
                $company->open_claims = count($company->getInjuries("open"));
                array_push($company_array, $company);

            }
            return $company_array;
        }else{
            return Company::where('id', $this->company_id)->first();
        }

    }

    public function getCompanyListByAgency(){
        $company_array = $this->getUserCompanies();
        $agency_array = array();
        foreach($company_array as $company){
            $agency_name = $company->agency_name;
            if(empty($agency_name)){
                $agency_name = "Independent Companies";
            }
            if(array_key_exists($agency_name, $agency_array)){
                array_push($agency_array[$agency_name], $company);
            }else{
                $agency_array[$agency_name] = array();
                array_push($agency_array[$agency_name], $company);
            }
        }
        $agency_array = array_values($agency_array);
        return $agency_array;
        //return $agency_array;

        //return $company_array;

    }

    public function getZenproCompanyList($company_type){
        if($this->type === "agent"){
            $company_array = array();
            $external_companies = ExternalUserCompanies::where('user_id', $this->id)->get();
            foreach ($external_companies as $external_company){
                Log::debug('company going to external user model: ' . $external_company);
                $company = $external_company->getCompanyBasicInfo($company_type);
                if($company === NULL && $company_type === "active"){
                    continue;
                }
                $company->open_claims = count($company->getInjuries("open"));
                $company->critical_claims = count($company->getCriticalInjuries());
                array_push($company_array, $company);

            }
            return $company_array;
        }
    }

    public function getZenproCompanyListByAgency($company_type = "all"){
        if($company_type === "all"){
            $company_array = $this->getZenproCompanyList($company_type);
        }elseif($company_type === "active"){
            $company_array = $this->getZenproCompanyList($company_type);
        }
        $agency_array = array();
        foreach($company_array as $company){
            $agency_name = $company->agency_name;
            if(empty($agency_name)){
                $agency_name = "Independent Companies";
            }
            if(array_key_exists($agency_name, $agency_array)){
                array_push($agency_array[$agency_name], $company);
            }else{
                $agency_array[$agency_name] = array();
                array_push($agency_array[$agency_name], $company);
            }
        }
        $agency_array = array_values($agency_array);
        return $agency_array;
    }

    public function getAgency(){
        if($this->is_zagent && $this->zagent_id !== NULL){
            $agency = \App\Company::where('id', $this->zagent_id)->where('is_agency', 1)->first();
            $agency = \App\Agency::where('id', $agency->agency_id)->first();
            return $agency;
        }else return false;
    }

    public function setDefaultWidgets(){

    }

    public function getSquads()
    {
        $that = $this;
        $squadIDs = \App\SquadMember::where('user_id', $that->id)->get();
        $IDs = array();
        foreach($squadIDs as $squadID){
            array_push($IDs, $squadID->squad_id);
        }
        $IDs = array_unique($IDs);
        $squads = array();
        foreach($IDs as $ID){
            $squad = \App\Squad::where('id', $ID)->first();
            array_push($squads, $squad);
        }
        return $squads;

    }

    public function setZAgentCompanies(){
        if($this->is_zagent === 1 && $this->zagent_id !== NULL){
            $agency = Company::where('team_id', $this->current_team_id)->where('is_agency', 1)->first();
            if(!is_null($agency)){
                $zagent_companies = Company::where('agency_id', $agency->agency_id)->get();
                foreach($zagent_companies as $company){
                    $external_user_check = ExternalUserCompanies::where('company_id', $company->id)
                        ->where('user_id', $this->id)->first();
                    if(is_null($external_user_check)){
                        $external_user_record = new ExternalUserCompanies();
                        $external_user_record->user_id = $this->id;
                        $external_user_record->company_id = $company->id;
                        $external_user_record->save();
                    }
                }

            }
        }
    }

    /*
    public function setRandomAvatar(){
        try{
            //generate a random number between 1 and 55 (the number of avatars there are)
            $avatar_number = rand(1, 55);
            //get the url of the corresponding avatar image
            $avatar_image = imagecreatefrompng("images/avatars/avatar_icons/avatar$avatar_number.png");
            //imageAlphaBlending and imageSaveAlpha are necessary to preserve the transparency of the avatar image
            imageAlphaBlending($avatar_image, true);
            imageSaveAlpha($avatar_image, true);

            //generate a random number between 0 and 15 (the number of colors there are)
            $color_index = rand(0, 13);
            //an array of the RGB values for each background color
            $color_array = array(
                array("red" => 255, "green" => 0, "blue" => 0),
                array("red" => 139, "green" => 0, "blue" => 0),
                array("red" => 255, "green" => 69, "blue" => 0),
                array("red" => 255, "green" => 140, "blue" => 0),
                array("red" => 255, "green" => 215, "blue" => 0),
                array("red" => 128, "green" => 128, "blue" => 0),
                array("red" => 50, "green" => 205, "blue" => 50),
                array("red" => 32, "green" => 178, "blue" => 170),
                array("red" => 30, "green" => 144, "blue" => 255),
                array("red" => 0, "green" => 0, "blue" => 205),
                array("red" => 255, "green" => 0, "blue" => 255),
                array("red" => 148, "green" => 0, "blue" => 211),
                array("red" => 255, "green" => 105, "blue" => 180),
                array("red" => 160, "green" => 82, "blue" => 45)
            );

            $image = imagecreatetruecolor(128, 128);

            $background = imagecolorallocate($image, $color_array[$color_index]["red"], $color_array[$color_index]["green"], $color_array[$color_index]["blue"]);
            imagefill($image, 0, 0, $background);

            //Log::debug(var_export($background, true));
            //merge the generated background image with the avatar image

            $merged = imagecopy($image, $avatar_image, 0, 0, 0, 0, 128, 128);
            //Log::debug(var_export($merged, true));

            $user_id = $this->id;

            $path = storage_path("/app/public/profiles/profile_pic_$user_id.png");

            $saved = imagepng($image, $path);

            $this->picture_location = "/storage/public/profiles/profile_pic_$user_id.png";
            $this->photo_url = "/storage/public/profiles/profile_pic_$user_id.png";
            $this->save();
        }catch (\ErrorException $e){
            Log::error($e);
        }
    }
    */
    public function setRandomAvatar(){
        Log::debug('in setRandomAvatar function');
        if($this->is_zenpro !== 1) {
            try {
                //generate a random number between 1 and 55 (the number of avatars there are)
                $avatar_number = rand(1, 55);
                Log::debug('avatar number = ' . $avatar_number);
                //get the url of the corresponding avatar image
                $context = [
                    'ssl' => [
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ];

                $url = URL::asset("images/avatar_icons/avatar$avatar_number.png");
                $response = file_get_contents($url, false, stream_context_create($context));

                $avatar_image = imagecreatefromstring($response);

                //$avatar_image = imagecreatefrompng("http://app.zenjuries.com/images/avatars/avatar_icons/avatar$avatar_number.png");
                //imageAlphaBlending and imageSaveAlpha are necessary to preserve the transparency of the avatar image
                imageAlphaBlending($avatar_image, true);
                imageSaveAlpha($avatar_image, true);

                //generate a random number between 0 and 15 (the number of colors there are)
                $color_index = rand(0, 13);
                Log::debug('color_index = ' . $color_index);
                //an array of the RGB values for each background color
                $color_array = array(
                    array("red" => 255, "green" => 0, "blue" => 0),
                    array("red" => 139, "green" => 0, "blue" => 0),
                    array("red" => 255, "green" => 69, "blue" => 0),
                    array("red" => 255, "green" => 140, "blue" => 0),
                    array("red" => 255, "green" => 215, "blue" => 0),
                    array("red" => 128, "green" => 128, "blue" => 0),
                    array("red" => 50, "green" => 205, "blue" => 50),
                    array("red" => 32, "green" => 178, "blue" => 170),
                    array("red" => 30, "green" => 144, "blue" => 255),
                    array("red" => 0, "green" => 0, "blue" => 205),
                    array("red" => 255, "green" => 0, "blue" => 255),
                    array("red" => 148, "green" => 0, "blue" => 211),
                    array("red" => 255, "green" => 105, "blue" => 180),
                    array("red" => 160, "green" => 82, "blue" => 45)
                );

                $image = imagecreatetruecolor(128, 128);

                $background = imagecolorallocate($image, $color_array[$color_index]["red"], $color_array[$color_index]["green"], $color_array[$color_index]["blue"]);
                imagefill($image, 0, 0, $background);

                //Log::debug(var_export($background, true));
                //merge the generated background image with the avatar image

                $merged = imagecopy($image, $avatar_image, 0, 0, 0, 0, 128, 128);
                //Log::debug(var_export($merged, true));

                $user_id = $this->id;

                $path = storage_path("app/public/profiles/profile_pic_$user_id.png");

                Log::debug(var_export(storage_path()));

                Log::debug(var_export($path, true));

                $saved = imagepng($image, $path);
                ///storage/profiles/profile_pic_' . $user->id . $trailing_character . '.png';

                $this->picture_location = "/storage/profiles/profile_pic_$user_id.png";
                $this->photo_url = "/storage/public/profiles/profile_pic_$user_id.png";
                $this->save();
            } catch (\ErrorException $e) {
                Log::debug('errors adding avatar');
                Log::error($e);
            }
        }else{
            Log::debug('no avatar set');
        }
    }


    public function getFormattedPhoneNumber(){
        //function to format phone number
        $value = $this->phone;

        $value = preg_replace('/[^0-9]/','',$value);

        if(strlen($value) > 10) {
            $countryCode = substr($value, 0, strlen($value)-10);
            $areaCode = substr($value, -10, 3);
            $nextThree = substr($value, -7, 3);
            $lastFour = substr($value, -4, 4);

            $value = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
        }
        else if(strlen($value) == 10) {
            $areaCode = substr($value, 0, 3);
            $nextThree = substr($value, 3, 3);
            $lastFour = substr($value, 6, 4);

            $value = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
        }
        else if(strlen($value) == 7) {
            $nextThree = substr($value, 0, 3);
            $lastFour = substr($value, 3, 4);

            $value = $nextThree.'-'.$lastFour;
        }
        return $value;
    }

    public function printCompanyName($append_apostrophe = false){
        if(!is_null($this->company_id)){
            $company_name = DB::table('companies')->where('id', $this->company_id)->value('company_name');
            if(!is_null($company_name)){
                if($append_apostrophe === true){
                    $company_name.= "'s";
                }
                echo $company_name;
            }
        }
    }
/*
    public function getAgency(){
        if(!is_null($this->zagent_id)){
            $agency_id = DB::table('companies')->where('id', $this->zagent_id)->value('agency_id');
            if(!is_null($agency_id)){
                $agency = Agency::where('id', $agency_id)->first();
                return $agency;
            }
        }
        return false;
    }
*/
    public function setUniqueApiToken(){
        $existing_tokens = \App\User::where('api_token', '!=', "")->where('api_token', '!=', NULL)->pluck('api_token');
        $valid = true;
        $end_loop = false;
        do {
           // $token = str_random(60);
            $token = Str::random(60);
            foreach($existing_tokens as $existing_token){
                if($existing_token === $token){
                    $valid = false;
                }
            }
            if($valid === true){
                $end_loop = true;
            }
        } while ($end_loop === false);

        $this->api_token = $token;
        $this->save();
        return true;

    }

    public function checkIfZcareOnInjury($injury_id){
        if($this->type === "zcare_admin") {
            $agency_id = \App\Company::where('id', $this->zagent_id)->value('agency_id');
            $location_ids = \App\ZcareLocation::where('agency_id', $agency_id)->pluck('id');
            $injury = \App\InjuryCareLocation::whereIn('location_id', $location_ids)->where('injury_id', $injury_id)->first();
            if (is_null($injury) === false) {
                return true;
            }
        }
        return false;
    }

    public function getNewInjuryCount(){
	    $company_id = $this->company_id;
	    if(is_null($company_id)){
		    if(session()->has('company_id')){
			    $company_id = session('company_id');
		    }
	    }
	    if(!is_null($company_id)){
		    $user_teams = \App\SquadMember::where('user_id', $this->id)->pluck('squad_id');
		    if($this->is_zenpro === 1){
		        $injuries = DB::table('injuries')->where('injuries.resolved', 0)->where('injuries.company_id', $company_id)->where('injuries.hidden', 0)->get();
            }else{
                $injuries = DB::table('injuries')->where('injuries.resolved', '0')->where('injuries.company_id', $company_id)->where('injuries.hidden', 0)
                    ->whereIn('squad_id', $user_teams)
                    ->get();
            }

            session()->put('injury_count', count($injuries));
	    }
    }

    /*
    public function printCompanyName(){
	    $company_name = "";
	    if($this->company_id !== NULL){
		    $company_name = \App\Company::where('id', $this->company_id)->value('company_name');
	    }
	    return $company_name;
    }
    */


    public function returnInjuryIDs($show_resolved = true){
        //$squad_IDs = \App\SquadMember::where('user_id', $this->id)->select('squad_id')->get();
        $squad_IDs = DB::table('squad_members')->where('user_id', $this->id)->pluck('squad_id');
        //$squad_IDs = json_decode(json_encode($squad_IDs));
        if($show_resolved === true){
            $injuries = \App\Injury::whereIn('squad_id', $squad_IDs)->where('hidden', 0)->get();
        }else{
            $injuries = \App\Injury::whereIn('squad_id', $squad_IDs)->where('hidden', 0)->where('resolved', 0)->get();
        }

        /*
        $injuries = \App\Injury::whereIn('squad_id', $squad_IDs)->get();
        return $injuries;
        */
        return $injuries;
    }

    public function returnResolvedInjuries(){
        $squad_IDs = DB::table('squad_members')->where('user_id', $this->id)->pluck('squad_id');

        $injuries = \App\Injury::whereIn('squad_id', $squad_IDs)->where('resolved', 0)->where('hidden', 0)->get();

        return $injuries;
    }

    public function updateCommunicationSettings($priority_two, $priority_three){
        if($priority_two === "false"){
            $priority_two = 0;
        }else if($priority_two === "true"){
            $priority_two = 1;
        }

        if($priority_three === "false"){
            $priority_three = 0;
        }else if($priority_three === "true"){
            $priority_three = 1;
        }
        $this->gets_priority_2_emails = $priority_two;
        $this->gets_priority_3_emails = $priority_three;
        $this->save();
    }

    public function uploadProfilePhoto($img){
        $data = explode(',', $img);
        $imgFile = base64_decode($data[1]);

        if($this->zenjuries_avatar_changed % 2 == 0){
            $trailing_character = "_0";
        }else{
            $trailing_character = "_1";
        }
        Storage::put('/public/profiles/profile_pic_' . $this->id . $trailing_character . '.' . 'png', $imgFile);
        //get the user to store the image

        //$imageName contains the location of the image to be stored in the DB
        $imageName = '/storage/public/profiles/profile_pic_' . $this->id . $trailing_character . '.' . 'png';
        $this->picture_location = $imageName;
        $this->photo_url = $imageName;
        $this->zenjuries_photo_changed++;
        $this->zenjuries_avatar_changed++;
        $this->avatar_number = NULL;
        $this->save();
        //clear their old avatar, if it exists
        $oldAvatar = \App\Avatar::where('user_id', $this->id)->first();
        if ($oldAvatar !== null) {
            $oldAvatar->delete();
        }

        return true;
    }

    function updatePassword($password){
        $this->password = bcrypt($password);
        $this->password_updated_at = Carbon::now();
        $this->save();
        return $this->password_updated_at->format('m-d-Y h:i');
    }

    function setColor($color){
        $this->avatar_color = $color;
        $this->save();
    }

    function setIcon($icon){
        $this->picture_location = $icon;
        $this->photo_url = $icon;
        $this->zenjuries_photo_changed++;
        $this->zenjuries_avatar_changed++;
        $this->save();
    }

    function createAvatar($img_data, $avatar_number){
        $data = explode(',', $img_data);
        $img_file = base64_decode($data[1]);

        if($this->zenjuries_avatar_changed % 2 == 0){
            $trailing_character = "_0";
        }else{
            $trailing_character = "_1";
        }

        Storage::put('/public/profiles/profile_pic_' . $this->id . $trailing_character . '.png', $img_file);

        $image_name = '/storage/public/profiles/profile_pic_' . $this->id . $trailing_character . '.png';

        $this->picture_location = $image_name;
        $this->photo_url = $image_name;
        $this->zenjuries_avatar_changed++;
        $this->avatar_number = $avatar_number;
        $this->save();

    }

    function checkInjuredUserForInjuryTeams(){
        //check if the user is injured
        if($this->injured){
            //grab the squads where the position does not equal the postion of the injured user
            $on_squads = \App\SquadMember::where('user_id', $this->id)->where('position_id', '!=', 12)->get();
            if(count($on_squads) >= 1){
                Log::debug('on_squads: ' . $on_squads);
                //injured and on squads
                return true;
            }else{
                //injured and not on squads
                return false;
            }
        }else{
            //not injured and not on squads
            return false;
        }
    }

}


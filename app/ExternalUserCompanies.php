<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class ExternalUserCompanies extends Model
{
    //
    protected $table = 'external_users_companies';

    public function getCompany(){
        $company = Company::where('id', $this->company_id)->first();

        if($company->agency_id !== NULL){
            $company->agency_name = Agency::where('id', $company->agency_id)->value('name');
        }else{
            $company->agency_name = "";
        }

        return $company;
    }

    public function getCompanyBasicInfo($company_type){
        Log::debug('this company id ' . $this->company_id);
        if($company_type === "all"){
            Log::debug('company_type is all');
            $company = Company::where('id', $this->company_id)->select('id', 'company_name', 'renewal_date', 'agency_id', 'logo_location', 'activated')->first();

            if($company->agency_id !== NULL){
                $company->agency_name = Agency::where('id', $company->agency_id)->value('name');
            }else{
                $company->agency_name = "";
            }

            return $company;

        }elseif($company_type === "active"){
            Log::debug('company_type is active');
            $company = Company::where('id', $this->company_id)->where('activated', 1)->select('id', 'company_name', 'renewal_date', 'agency_id', 'logo_location', 'activated')->first();

            if($company !== NULL){
                if($company->agency_id !== NULL){
                    $company->agency_name = Agency::where('id', $company->agency_id)->value('name');
                }else{
                    $company->agency_name = "";
                }

                return $company;
            }
        }
        /*
        if($company->agency_id !== NULL){
            $company->agency_name = Agency::where('id', $company->agency_id)->value('name');
        }else{
            $company->agency_name = "";
        }

        return $company;
        */
    }
}

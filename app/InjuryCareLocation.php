<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InjuryCareLocation extends Model
{
    //
    protected $table = "injuries_care_locations";
}

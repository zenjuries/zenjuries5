<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Communication_Message extends Model
{
    //
    protected $table = 'communication_messages';

    public $timestamps = false;

    public function communication(){
        return $this->belongsTo('App/Communication');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InjuryLocation extends Model
{
    protected $table = 'injury_locations';

    public $timestamps = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class SecondaryPolicyInfo extends Model
{
    //
    protected $table = 'secondary_policy_info';
    
    function getRenewalDay(){
	    $day = Carbon::createFromFormat("Y-m-d H:i:s", $this->renewal_date)->day;
	    return $day;
    }
    
    function getRenewalMonth(){
	    $month = Carbon::createFromFormat("Y-m-d H:i:s", $this->renewal_date)->month;
	    return $month;

    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Injury;

class AddedToInjuryEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $injury;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Injury $injury)
    {
        $this->injury = $injury;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.added_to_injury');
    }
}

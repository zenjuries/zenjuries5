<?php

namespace App\Mail;

use App\Models\UserInvite;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserInviteEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user_invite;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserInvite $user_invite)
    {
        $this->user_invite = $user_invite;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.user_invite');
    }
}

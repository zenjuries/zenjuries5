<?php

namespace App\Mail;

use App\Models\Injury;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NegativeDiaryFeedbackEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $injury;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Injury $injury)
    {
        $this->injury = $injury;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.negative_diary_feedback');
    }
}

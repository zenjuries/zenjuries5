<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\CompanyInvite;

class CompanyInviteEmail extends Mailable
{
    use Queueable, SerializesModels;

    //CompanyInvite model
    public $company_invite;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(CompanyInvite $company_invite)
    {
        $this->company_invite = $company_invite;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.company_invite');
    }
}

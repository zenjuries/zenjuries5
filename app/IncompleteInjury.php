<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncompleteInjury extends Model
{
    //
    protected $table = "incomplete_injuries";
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    //
    protected $fillable = ['color1', 'color2', 'position'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}

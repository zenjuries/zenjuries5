<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Log;
use App\DiaryPost;
use App\NotePost;
use App\Appointment;
use App\Injury;
use App\DeviceId;
use App\Services\FCMService;

class TreePost extends Model
{
    //
    protected $table = 'tree_post';

    function newZenfuciusReminderPost($injury_id, $recipients, $action_text){
        if(count($recipients)) {
            if ($recipients[0] instanceof SquadMember) {
                foreach ($recipients as $recipient) {
                    $recipient->name = \App\User::where('id', $recipient->user_id)->value('name');
                }
            }
            if (count($recipients) === 1) {
                $recipient_string = $recipients[0]->name;
            } else if (count($recipients) > 1) {
                $recipient_string = "";

                //we want to know what the last key is
                $last_key = $recipients->keys()->last();

                foreach ($recipients as $key => $recipient) {
                    if ($key === $last_key) {
                        $recipient_string .= "and " . $recipient->name;
                    } else {
                        if (count($recipients) !== 2) {
                            $recipient_string .= $recipient->name . ", ";
                        } else {
                            $recipient_string .= $recipient->name . " ";
                        }
                    }
                }
            } else {
                return false;
            }
            $this->injury_id = $injury_id;
            $this->body = "Zenjuries sent " . $recipient_string . " a reminder to " . $action_text . " on " . Carbon::now()->toDateTimeString();
            $this->type = "reminderPost";
            $this->save();

            /*** push notification ***/
            $injury = \App\Injury::where('id', $injury_id)->first();
            $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
            $sent_notification_array = [];
            foreach($squad_members as $squad_member){
                if(!in_array($squad_member->user_id, $sent_notification_array)){
                    $user = \App\User::where('id', $squad_member->user_id)->first();
                    if($user->notification_priority_2 === 1){
                        $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                        if($devices != NULL || $devices != ""){
                            $devices->sendNotification('Reminder Post', "Zenjuries sent " . $recipient_string . " a reminder to " . $action_text . " on " . Carbon::now()->toDateTimeString() . ".");
                        }
                    }
                    array_push($sent_notification_array, $squad_member->user_id);
                }
            }
            /***********************************************************/
        }
    }

    function newChatPost($injury_id, $user_id, $post_body){
        $this->injury_id = $injury_id;
        $this->user_id = $user_id;
        $this->type = "chatPost";
        $this->body = $post_body;
        $this->save();
        /*** push notification ***/
        $injury = \App\Injury::where('id', $injury_id)->first();
        $injuried_employee = \App\User::where('id', $injury->injured_employee_id)->first();
        $employee = \App\User::where('id', $user_id)->first();
        $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                $recipient_user = \App\User::where('id', $squad_member->user_id)->first();
                if($recipient_user->notification_priority_1 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('New Chat Post', $employee->name . ' posted a new message on ' . $injuried_employee->name . "'s injury.");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/
        return $this->id;
    }

    function newZenployeeMoodPost(DiaryPost $post){
        $this->injury_id = $post->injury_id;
        $this->user_id = $post->user_id;
        if($post->post_type === "injury"){
            $this->body = "Diary Post: on a scale from 1 to 10, I feel like a " . $post->mood . ".";
        }else{
            $this->body = "Diary Post: on a scale from 1 to 10, my claim experience feels like a " . $post->mood . ".";
        }
        $this->type = "diaryPost";
        $this->diary_post_id = $post->id;
        /*** push notification ***/
        $injury = \App\Injury::where('id', $this->injury_id)->first();
        $injuried_employee = \App\User::where('id', $injury->injured_employee_id)->first();
        //$employee = \App\User::where('id', $user_id)->first();
        $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                $recipient_user = \App\User::where('id', $squad_member->user_id)->first();
                if($recipient_user->notification_priority_1 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('Mood Update', $injuried_employee->name . " updated their mood, go check and see how they're doing.");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/
        $this->save();
    }

    function newZenployeeMoodAndNotePost(DiaryPost $post){
        $this->injury_id = $post->injury_id;
        $this->user_id = $post->user_id;
        if($post->post_type === "injury"){
            $this->body = "On a scale from 1 to 10, I feel like a " . $post->mood . ". \"" . $post->note . "\"";
        }else{
            $this->body = "On a scale from 1 to 10, my claim experience feels like a " . $post->mood . ". \"" . $post->note . "\"";
        }
        $this->type = "diaryPost";
        $this->diary_post_id = $post->id;
        $this->save();

        /*** push notification ***/
        $injury = \App\Injury::where('id', $this->injury_id)->first();
        $injuried_employee = \App\User::where('id', $injury->injured_employee_id)->first();
        //$employee = \App\User::where('id', $user_id)->first();
        $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                $recipient_user = \App\User::where('id', $squad_member->user_id)->first();
                if($recipient_user->notification_priority_1 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('Mood Update', $injuried_employee->name . " updated their mood, go check and see how they're doing.");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/
    }

    function newZenployeeNotePost(NotePost $post){
        $this->injury_id = $post->injury_id;
        $this->user_id = $post->user_id;
        $user_name = \App\User::where('id', $post->user_id)->first();
        $this->body = $user_name->name . " posted for the team: " . $post->note;
        $this->type = "diaryPost";
        $this->diary_post_id = $post->id;
        $this->save();
        /*** push notification ***/
        $injury = \App\Injury::where('id', $this->injury_id)->first();
        $injuried_employee = \App\User::where('id', $injury->injured_employee_id)->first();
        $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                $recipient_user = \App\User::where('id', $squad_member->user_id)->first();
                if($recipient_user->notification_priority_1 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('New Team Post', $user_name->name . ' posted for '. $injuried_employee->name ."'s team.");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/
    }

    function newAppointmentPost(Appointment $appointment, $user_id){
        $user = \App\User::where('id', $user_id)->first();

        $this->injury_id = $appointment->injury_id;
        $this->user_id = $user_id;
        $this->body = $user->name . " entered a new appointment for this injury, taking place on " . Carbon::createFromFormat("Y-m-d H:i:s", $appointment->appointment_time)->toDayDateTimeString();
        $this->type = "systemPost";
        $this->save();

        /*** push notification ***/
        $injury = \App\Injury::where('id', $this->injury_id)->first();
        $injuried_employee = \App\User::where('id', $injury->injured_employee_id)->first();
        $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
        $sent_notification_array = [];
        foreach($squad_members as $squad_member){
            if(!in_array($squad_member->user_id, $sent_notification_array)){
                $sender_user = \App\User::where('id', $squad_member->user_id)->first();
                if($sender_user->notification_priority_2 === 1){
                    $devices = \App\DeviceId::where('user_id', $squad_member->user_id)->first();
                    if($devices != NULL || $devices != ""){
                        $devices->sendNotification('New Appointment', $user->name . " entered a new appointment for " . $injuried_employee->name . ".");
                    }
                }
                array_push($sent_notification_array, $squad_member->user_id);
            }
        }
        /***********************************************************/
    }
}

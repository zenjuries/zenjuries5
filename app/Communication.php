<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Communication extends Model
{
    protected $table = 'communications';

    public function communication_messages(){
        return $this->hasMany('App/Communication_Message');
    }

    public function communication_positions(){
        return $this->hasMany('App/Communication_Position');
    }

    public function company(){
        return $this->belongsTo('App/Company');
    }

    public function injury(){
        return $this->belongsTo('App/Injury');
    }

    public function communication_template(){
        return $this->belongsTo('App/Communication_Template');
    }

    public function followup(){
        return $this->hasOne('App/Communication', 'id');
    }
}

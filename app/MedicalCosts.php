<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class MedicalCosts extends Model
{
    //
    protected $table = 'medical_costs';

    //take the user making the update, the ID for the injury, and the new value
    function updateCost($user_id, $injury_id, $value){
        //we need to know if the costs are equal, if not then there's no change needed
        $costs_are_equal = false;
        //get the current value for that injury
        $current_value = MedicalCosts::where('injury_id', $injury_id)->where('current', 1)->first();
        /*
        if($injury_id === '1'){
            Log::debug('medical val comparison');
            Log::debug(var_export($current_value->cost, true));
            Log::debug(var_export($value, true));
        }
        */
        //there might not be a current value set, so we want to check for null
        if($current_value !== NULL){
            if($current_value->cost == $value){
                //if the costs are equal we don't have to update anything
                $costs_are_equal = true;
            }else{
                //if the costs have changed, mark the previous record as no longer being current
                $current_value->current = 0;
                $current_value->save();
            }
        }
        //if the cost has changed, create a new record for it
        if($costs_are_equal === false){
            $this->injury_id = $injury_id;
            $this->updater_id = $user_id;
            $this->cost = $value;
            $this->current = 1;
            $this->save();
        }

        Log::debug("injury_id = " . var_export($injury_id, true));

        //we want to return the old value if an update was made, and false if there was no change to values
        if($costs_are_equal === false){

            if($current_value === NULL){
                return "0";
            }else{
                return $current_value->cost;
            }
        }else{

            return false;
        }
    }
    /*
    $costs_are_equal = false;
        if($type == "Paid Medical"){
            $old_values = \App\MedicalCosts::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\MedicalCosts();
        }else if($type == "Paid Indemnity"){
            $old_values = \App\IndemnityCosts::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\IndemnityCosts();
        }else if($type == "Paid Legal"){
            $old_values = \App\LegalCosts::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\LegalCosts();
        }else if($type == "Paid Miscellaneous"){
            $old_values = \App\MiscellaneousCosts::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\MiscellaneousCosts();
        }else if($type == "Reserve Medical"){
            $old_values = \App\ReserveMedicalCost::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\ReserveMedicalCost();
        }else if($type == "Reserve Indemnity"){
            $old_values = \App\ReserveIndemnityCost::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\ReserveIndemnityCost();
        }else if($type == "Reserve Legal"){
            $old_values = \App\ReserveLegalCost::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\ReserveLegalCost();
        }else if($type == "Reserve Miscellaneous"){
            $old_values = \App\ReserveMiscellaneousCost::where('injury_id', $this->id)->where('current', 1)->get();
            $claim_cost = new \App\ReserveMiscellaneousCost();
        }else return false;
        if(count($old_values)){
            if($old_values->first()->cost === $cost){
                $costs_are_equal = true;
            }
        }

        foreach($old_values as $old_value){
            $old_value->current = 0;
            $old_value->save();
        }

        $claim_cost->injury_id = $this->id;
        $claim_cost->updater_id = $user->id;
        $claim_cost->cost = $cost;
        $claim_cost->current = 1;
        $claim_cost->save();

        $post = new \App\TreePost();
        $post->injury_id = $this->id;
        $post->user_id = $user->id;
        //Check what the tree post should say
        if($costs_are_equal === false){
            $post->body = $user->name . " updated the " . (($type == "misc")? "Miscellaneous" : ucfirst($type)) . " Cost for this claim to $" . $cost . " on " . $claim_cost->created_at;
        }else{
            $post->body = $user->name . " confirmed that the " . (($type == "misc")? "Miscellaneous" : ucfirst($type)) . " Cost for this claim is up to date on " . $claim_cost->created_at;
        }
        $post->type = 'valuePost';
        $post->task_type = 'claim';
        $post->save();

        $this->values_updated_at = Carbon::now();
        $this->reminders_since_last_update = 0;
        $this->last_reminders_sent_at = NULL;
        $this->save();
     */


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActiveMarketingMaterial extends Model
{
    //
    protected $table = 'active_marketing_material';
}

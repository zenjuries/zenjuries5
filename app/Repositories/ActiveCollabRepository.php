<?php namespace App\Repositories;

use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


class ActiveCollabRepository{

    const PROJECT_ID = 6;

    const API_SUPPORT_PROJECT_ID = 222;

    public function getToken(){
        // Provide name of your company, name of the app that you are developing, your email address and password.
        $authenticator = new \ActiveCollab\SDK\Authenticator\Cloud('Zenjuries', 'Zen BugSupport', 'support@zenjuries.com', 'Zenjuries01');

        $token = $authenticator->issueToken(116436);

// Did we get it?
        if ($token instanceof \ActiveCollab\SDK\TokenInterface) {
            return $token;
        } else {
            print "Invalid response\n";
            die();
        }
    }

    public function postNewTask($taskName, $name, $body, $company, $label){
        $token = $this->getToken();
        $client = new \ActiveCollab\SDK\Client($token);
        $name = str_replace("'", "", $name);
        $name = str_replace("/", "", $name);
        $name = preg_replace('/[^A-Za-z0-9\-]/', ' ', $name);
        $response = $client->post('/upload-files',[
            'ac.png',
            'image\/png'
        ]);
        //Log::debug($response->getJson());

        if($label === "PRIORITY-1" || $label === "PRIORITY-2"){
            $is_important = true;
        }
        else{
            $is_important = false;
        }


        $response2 = $client->post('/projects/' . ActiveCollabRepository::API_SUPPORT_PROJECT_ID . '/tasks', [
            'name' => $taskName,
            'body' => "Bug reported by: " . $name . " - " . $company . "<br />" . $body,
            'labels' => [$label],
            'assignee_id' => 33,
            'is_important' => $is_important
        ]);
        //Log::debug($response2->getBody());
    }

    public function postApiFailure($function_name, $body){
        $token = $this->getToken();
        $client = new \ActiveCollab\SDK\Client($token);
        $name = $string = str_replace("'", "", $function_name);
        $name = str_replace("/", "", $function_name);
        $name = preg_replace('/[^A-Za-z0-9\-]/', ' ', $function_name);
        $response = $client->post('projects/' . ActiveCollabRepository::API_SUPPORT_PROJECT_ID . '/tasks', [
            'name' => $function_name . " " . "Zenjuries",
            'body' => Carbon::now()->timezone('America/New_York') . " " . $body,
            'assignee_id' => 4
        ]);
        //Log::debug(json_decode($response->getBody()));
    }

    public function postNewNote($name, $body){
        $token = $this->getToken();
        $client = new \ActiveCollab\SDK\Client($token);
        $client->post('projects/' . ActiveCollabRepository::PROJECT_ID . '/notes', [
           'name' => $name,
            'body' => $body
        ]);
    }

    public function postNewDiscussion($name, $body){
        $token = $this->getToken();
        $client = new \ActiveCollab\SDK\Client($token);
        $response = $client->post('projects/' . ActiveCollabRepository::PROJECT_ID . '/discussions', [
           'name' => $name,
            'body' => $body
        ]);

    }

    public function postNewFile(){
        $token = $this->getToken();
        $client = new \ActiveCollab\SDK\Client($token);
        try {
            $response = $client->post('upload-files',[
                [
                    'test-file.txt',
                    'text\/plain'
                ]
            ]);
           echo $response->getBody();

        } catch(AppException $e) {
            //Log::debug($e->getMessage() . '<br><br>';
            // var_dump($e->getServerResponse()); (need more info?)
        }

    }


    public function indexTasksByEmail($email)
    {
        $token = $this->getToken();
        $client = new \ActiveCollab\SDK\Client($token);
        $response = $client->get('projects/' . ActiveCollabRepository::PROJECT_ID . '/tasks');
        $response = json_decode($response->getBody());
        foreach ($response->tasks as $task) {
            $status = 'NONE';
            if (stristr($task->name, $email) !== FALSE) {
                //echo $task->name . "<br>";
                $id = $task->id;
                $modalLink = 'modalLink' . $id;
                $commentsRoute = route('taskComments', ['id' => $id]);
                if(isset($task->labels)){
                    foreach($task->labels as $label){
                        if($label->name == 'NEW'){
                            $status = $label->name;
                        }
                    }
                }
                $comments = $client->get('comments/task/' . $task->id);
                $comments = json_decode($comments->getBody());
                if(isset($comments[0])){
                    $body = $comments[0]->body;
                } else {
                    $body = $task->body;
                }
                echo "<tr>
                <td>" .
                    $status .
                "</td><td>" .
                    str_replace($email, "", $task->name) .
                "</td><td>" .
                    date('Y-m-d h:i:s',$task->created_on) .
                "</td><td>" .
                   $body .
                "</td><td>
                <a id='$modalLink' href='$commentsRoute' data-toggle='modal' data-target='#acModal' data-backdrop='static'><i class='fa fa-comment'></i></a>
                </td></tr>
                ";
            }
        }
    }

    public function indexTasks($email){
        $token = $this->getToken();
        $client = new \ActiveCollab\SDK\Client($token);
        $response = $client->get('projects/' . ActiveCollabRepository::PROJECT_ID . '/tasks');
        $response = json_decode($response->getBody());
        echo '<input id="_token" type="hidden" name="_token" value="'. csrf_token() . '">';
        foreach (array_reverse($response->tasks) as $task) {
            $status = 'Updating';
            $class = 'acStatus updating';
            if(Auth::user()->injured == 1){
                $link = '/zengarden/taskComments/' . $task->id;
            }else{
                $link = '/taskComments/' . $task->id;
            }
            if(isset($task->labels)){
                foreach($task->labels as $label){

                    if($label->name == 'SUPPORT-New'){
                        $status = 'New';
                        $class = 'acStatus new';
                    }else if ($label->name == 'SUPPORT-In Progress'){
                        $status = 'In Progress';
                        $class = 'acStatus inProgress';
                    }else if ($label->name == 'SUPPORT-Complete'){
                        $status = 'Complete';
                        $class = 'acStatus complete';
                    }else if ($label->name == 'SUPPORT-Awaiting Response'){
                        $status = 'Awaiting Response';
                        $class = 'acStatus awaitingResponse';
                    }else if ($label->name == 'SUPPORT-Viewed'){
                        $status = 'Viewed';
                        $class = 'acStatus viewed';
                    }
                }
            }
            if (stristr($task->name, $email) !== FALSE) {
	            $commentURL = route('getNewComment', ['id' => $task->id, 'title' => str_replace($email, "", $task->name)]);
                $commentButton = " <a onclick=\"$('.popoverLink') . not(this) . popover('hide');\" class=\"popoverLink reply\" title=\"Comment\" role=\"button\" tabindex=\"0\" data-container=\"\" data-trigger=\"click\" data-html=\"true\" data-toggle=\"popover\" data-placement=\"right\" data-content=\"
 <div class='threadReply'><form id='replyForm' action = 'postreply' class='replyForm' ><a id = 'exitButton' >&times;</a ><input type = 'hidden' name = 'id' id = 'id' value ='" . $task->id . "'>
<textarea class='editTextArea' name='comment' id='comment'></textarea>
<a id='popoverReplySubmit'>Send Message</a></form></div>
<script>

$('#popoverReplySubmit').click(function () {

    $('#replyForm').submit();
    return false;
});

$('#exitButton').click(function () {
    $('.popoverLink').popover('hide');
});

jQuery(document).ready(function ($) {

    $('#replyForm').on('submit', function (e) {
        e.preventDefault();

        //use this kinda deal to get all the variables you need for your request
        var taskID = $('#id').val();
        var comment = $('#comment').val();
        var _token = $('#_token').val();
        //alert(id + ' ' + reply + ' ' + _token);
        $.ajax({
            type: 'POST',
            url: '" . route('postNewComment') . "',

            data: {taskID: taskID, comment: comment, _token: _token},
            success: function () {
                $.bootstrapGrowl('Message Sent!', {
                    offset: {from: 'top', amount: 200},
                    align: 'center',
                    type: 'success'
                });
            }
        });
    });
});

</script>
 \"><i class=\"fa fa-commenting-o\"></i> Post Comment</a>";


                echo '
                <div class="col-xs-12 col-md-12">
                <div class="messageOverview">
<div class="title ' . $class . '">
        <i class="fa fa-comment"></i>' .
                    str_replace($email, "", $task->name) .
               ' <!-- this code needs to be applied to the following <a>    <button></button>-->
        <a href="' . $link . '" class="history"><i class="fa fa-history"></i> History</a>
</div>
<div class="body">
        <span class="summary">' .
                    $task->body
                    . '</span>' .
                    '<div class="messageButton"><a class="reply" href="' . $commentURL . '" data-toggle="modal" data-target="#commentModal" onclick="clearCommentModal()"><i class="fa fa-commenting-o"></i> Post Comment</a></div>' .
'</div>
<div class="footer">
        <span class="' . $class . '">' .
                    $status .
                    '</span>
        <span class="date">'.
                    date('Y-m-d h:i:s',$task->created_on) .
                    '</span>
</div>
</div>
</div>
<br>
';
            }
        }
        echo '<!-- Comment Modal -->
<div class="modal fade messageSupport" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></a>
        <span class="title" id="myModalLabel">Modal title</span>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
        <a type="button" class="btn btn-primary"><i class="fa fa-check"></i> Send</a>
      </div>
    </div>
  </div>
</div>

<script>
    function clearCommentModal(){
        $("#commentModal").removeData("bs.modal");
    }
</script>';
    }


    public function getTaskComments($taskID){
        $token = $this->getToken();
        $client = new \ActiveCollab\SDK\Client($token);
//$response = $client->get('projects/5/tasks');
        $response = $client->get('comments/task/' . $taskID);
        $response = json_decode($response->getBody());
        if(count($response) == 0){
            echo "<h4>Sorry, this post has no comments yet!</h4>";
        } else {
            echo "<table class='table reportsTable'><tr><th>Comment</th><th>Date</th></tr>";
            foreach ($response as $comment) {
                echo "<tr><td>$comment->body</td><td>" . date('Y-m-d h:i:s', $comment->created_on) . "</td>";
            }
            echo "</table>";
        }

    }

    public function getCommentHistory($taskID){
        $token = $this->getToken();
        $client = new \ActiveCollab\SDK\Client($token);
//$response = $client->get('projects/5/tasks');
        $response = $client->get('comments/task/' . $taskID);
        $response = json_decode($response->getBody());
        if(count($response) == 0){
                echo "<h4>Sorry, this post has no comments yet!</h4>";
        } else {
	        echo '<ul class="chatList">';
	        $count = 0;
            foreach ($response as $comment){
                if($comment->created_by_id == 6){
                    /*THIS GENERATES THE USER'S COMMENT*/
                    $user = Auth::user();
                    /******************************/
                    //get the user's pic
                    $pic = $user->picture_location;
                    if(empty($pic)){
                        $pic = "/images/avatars/no_profile_image.jpg";
                    }
                    //get users avatar color
                    if(empty($user->avatar_color)){
                        $theme = "";
                    }else{
                        $theme = 'style= "background: ' . $user->avatar_color . '"';
                    }
                    //start drawing the chat bubble
                    echo '<li><div class="chatAvatar right';
                    //if it is the first comment it gets the class "new"
                    if($count == 0){
                        echo ' new';
                    }
                    //draw teardrop with color
                    echo '"><span class="teardrop right"' . $theme . '>';
                    // draw user's picture
                    echo '<img src="' . $pic . '" alt="Profile Pic"></span></div><div class="chatBubble';
                    if($count == 0){
                        echo ' new';
                    }
                    //draw content
                    echo '"><div class="username">' . $user->name . //next line
                        '<div><div class="body">' . $comment->body . //next line
                        '<div><div class="timestamp">Posted ' . date('Y-m-d h:i:s', $comment->created_on) . '</div></div></li>';
                //THIS GENERATES THE REPLIES FROM ZENJURIES
                }else{
                    $pic = "/images/zenjuries-logo.jpg";
                    echo '<li><div class="chatAvatar left';
                    if($count == 0){
                        echo ' new';
                    }
                    echo '"><span class="teardrop left" style="background: rgb(30, 144, 255)"';
                    echo '><img src="' . $pic . '" alt="Profile Pic"></span></div><div class="chatBubble alt';
                    if($count == 0){
                        echo ' new';
                    }
                    echo '"><div class="username">Zenjuries<div><div class="body">' . $comment->body . //next line
                        '<div><div class="timestamp">Posted ' . date('Y-m-d h:i:s', $comment->created_on) . //next line
                        '</div></div></li>';
                }
                $count++;
            }
            echo "</ul>";
        }
    }

    public function postNewComment($taskID, $comment){
        $token = $this->getToken();
        $client = new \ActiveCollab\SDK\Client($token);
        $response = $client->post('comments/task/' . $taskID,[
            'body' => $comment
        ]);
        //var_dump($response);
        /*
        $response = $client->get('comments/task/45');
        echo $response->getBody();*/
    }

    /*
    public function indexProjects(){
        $token = $this->getToken();
        $client = new \ActiveCollab\SDK\Client($token);
        $response = $client->get('projects/names');
        var_dump($response);
    }*/
}

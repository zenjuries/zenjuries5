<?php namespace App\Repositories;

use App\Company;
use App\Invitation;
use App\Team;
use App\User;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Str;


class InvitationRepository{

    function inviteCompany($company_id){
        $company = Company::where('id', $company_id)->first();

        $user = $company->getCompanyOwner();

        Mail::send('emailTemplates.verifyAccountNewPolicyHolder', ['user' => $user, 'company' => $company], function ($m) use ($user, $company) {
            $m->to($user->email, $user->name)->subject('Welcome to Zenjuries!');
        });
    }

    function inviteNewUser($name, $email, $company_id, $is_zagent = 0, $user_type = "internal", $injured = 0, $injury_id = NULL){
        $name = ucwords($name);
        $company = Company::where('id', $company_id)->first();
        $team = Team::where('id', $company->team_id)->first();

        $invitation = new Invitation();
        $invitation->id = Uuid::uuid4();
        $invitation->team_id = $team->id;
        $invitation->user_id = NULL;
        $invitation->email = $email;
        $invitation->token = Str::random(40);
        $invitation->user_type = $user_type;
        $invitation->injured = $injured;
        $invitation->injury_id = $injury_id;
        $invitation->is_zagent = $is_zagent;
        $invitation->save();

        if($injured){
            Mail::send("emailTemplates.inviteToZengarden", compact('invitation'), function ($m) use ($invitation) {
                $m->to($invitation->email)->subject("Welcome to Zengarden!");
            });
        }else{
            Mail::send("emailTemplates.inviteNewUser", compact('invitation'), function ($m) use ($invitation) {
            $m->to($invitation->email)->subject("Welcome to Zenjuries!");
            });
        }


    }


}
<?php namespace App\Repositories;

use App\ExternalUserCompanies;
use App\SquadMember;
use Laravel\Spark\Contracts\Interactions\Settings\Teams\SendInvitation;

class ZenProRepository{

    public function __construct(){
        //$this->inviteRepo = $inviteRepo;
    }

    function assignZenPro($company_id){
        $zenpros = \App\User::where('is_zenpro', 1)->get();

        $company_count_array = array();

        foreach($zenpros as &$zenpro){
            $company_count = count(\App\ExternalUserCompanies::where('user_id', $zenpro->id)->orderBy('company_id')->get());
            $zenpro->company_count = $company_count;
            array_push($company_count_array, $company_count);
        }

        $lowest_company_count = min($company_count_array);

        $zenpro_id = NULL;

        foreach($zenpros as $zenpro){
            if($zenpro->company_count === $lowest_company_count){
                $zenpro_id = $zenpro->id;
                break;
            }
        }

        if(!is_null($zenpro_id)){
            $external_user_company = new ExternalUserCompanies();
            $external_user_company->company_id = $company_id;
            $external_user_company->user_id = $zenpro_id;
            $external_user_company->save();

            $company = \App\Company::where('id', $company_id)->first();
            $company->has_zenpro = 1;
            $company->save();

            $this->addZenproToDefaultSquad($company->id);
            return true;
        }else return false;
    }

    function inviteNewZenpro($email, $name){

        $admin_company = \App\Company::where('admin', 1)->first();
        $team = \App\Team::where('id', $admin_company->team_id)->first();

        //$this->inviteRepo->handle($team, $email, 'zenpro');
    }

    function addZenproToDefaultSquad($company_id){
        $default_squad = \App\Squad::where('company_id', $company_id)->where('squad_name', 'Default Squad')->first();
        if($default_squad !== NULL){
            $company = \App\Company::where('id', $company_id)->first();
            $zenpro_id = $company->getZenproId();
            if($zenpro_id){
                $zenpro_on_squad = \App\SquadMember::where('user_id', $zenpro_id)->where('squad_id', $default_squad->id)->first();
                if(is_null($zenpro_on_squad)){
                    $zenpro_on_squad = new SquadMember();
                    $zenpro_on_squad->squad_id = $default_squad->id;
                    $zenpro_on_squad->position_id = 10;
                    $zenpro_on_squad->user_id = $zenpro_id;
                    $zenpro_on_squad->save();
                }
            }

        }
    }

    function getCompanyZenpro($company_id){
        $company = \App\Company::where('id', $company_id)->first();
        if(!is_null($company) && $company->has_zenpro === 1){

            $external_users = \App\ExternalUserCompanies::where('company_id', $company_id)->pluck('user_id')->toArray();
            var_dump($external_users);

            $zenpros = \App\User::where('is_zenpro', 1)->get();

            foreach($zenpros as $zenpro){
                if(in_array($zenpro->id, $external_users)){
                    return $zenpro;
                }
            }
            return false;


        }else return false;
    }

}
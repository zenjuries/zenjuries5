<?php namespace App\Repositories;

use Stripe;
use App\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Laravel\Cashier\Cashier;
use Illuminate\Support\Facades\Log;

class PaymentsRepository{

    function newZagentRegistration($company_id, $payment_method){
        $this->createNewStripeCustomer($company_id);
        $this->updateCard($company_id, $payment_method);
        $this->createNewSubscription($company_id);
    }

    function createNewPayment($cost, $company_id, $payment_type){
        $payment = new Payment();
        $payment->cost = $cost;
        $payment->company_id = $company_id;
        $type = \App\PaymentType::where('payment_type', $payment_type)->first();
        $payment->payment_type_id = $type->id;
        $payment->save();
    }

    static function createNewSubscription($company_id){
        //stripe subscription ID = zagent_monthly_fee
        $company = \App\Company::where('id', $company_id)->first();

        $payment_method = $company->defaultPaymentMethod();
        $payment_method = $payment_method->asStripePaymentMethod();
        Log::debug(var_export($payment_method->id, true));

        $company->newSubscription(
            "ZAgent_Monthly_Fee", "zagent_monthly_fee"
        )->create($payment_method->id);

    }

    function createNewStripeCustomer($company_id){
        $company = \App\Company::where('id', $company_id)->first();
        $owner = $company->getCompanyOwner();
        $options = array(['email' => $owner->email]);

        $customer = $company->createAsStripeCustomer($options);
    }

    function updateCard($company_id, $payment_method){
        $company = \App\Company::where('id', $company_id)->first();
        $payment_method = $payment_method;
        $company->updateDefaultPaymentMethod($payment_method);
        $company->updateDefaultPaymentMethodFromStripe();
    }

    function chargeExistingCustomer($payment_id, $client_name = NULL){
        $payment = \App\Payment::where('id', $payment_id)->first();
        $company = \App\Company::where('id', $payment->company_id)->first();

        if(!is_null($client_name)){
            $invoice_description = "Zenjuries Subscription for " . $client_name;
        }else $invoice_description = "Zenjuries Subscription";

        //stripe expects payments to be in cents, so multiply the cost by 100
        $final_price = $payment->cost * 100;
        $company->invoiceFor($invoice_description, $final_price);

        $payment->paid = 1;
        $payment->paid_at = Carbon::now();
        $payment->save();
    }

    // going to change as creation of setup intents should be one singular editable place instead of 2 like it is now
    static function createSetupIntent($company_id){
        $company = \App\Company::where('id', $company_id)->first();
        $stripe = new \Stripe\StripeClient(config('stripe.secret'));

        $paymentIntent = $stripe->setupIntents->create(
            [
                'customer' => $company->stripe_id,
                'payment_method_types' => ['us_bank_account', 'card'],
            ]
        );
        return $paymentIntent;
    }

    function modifySetupIntent($intent_id, $company_id){
        $stripe = new \Stripe\StripeClient(config('stripe.secret'));
        $company = \App\Company::where('id', $company_id)->first();
        $stripe->setupIntents->update(
            $intent_id,
            ['metadata' => ['company_id' => $company->id, 'customer' => 'register']]
        );
    }



    function checkIfActive($company_id){
        $company = \App\Company::where('id', $company_id)->first();

        // check company age
        $registrationDate = Carbon::parse($company->created_at);
        $currentDate = Carbon::now();
        $companyAge = $registrationDate->diffInDays($currentDate);

        if ($companyAge < 3) return true;

        // check if company is an agent
        if ($company->is_agency != 1) return true;
      
        // check if company is subscribed
        if ($company->subscribed('ZAgent_Monthly_Fee')) return true;

        return false;
    }


    function cancelSubscription($company_id){
        // Get company by id
        $company = \App\Company::where('id', $company_id)->first();

        // Find & Cancel subscription
        // return "xo";
        $company->subscription('ZAgent_Monthly_Fee')->cancel();
    }

}

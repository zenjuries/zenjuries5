<?php namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Injury;
//THIS CLASS WILL PROVIDE FUNCTIONS FOR CALCULATING STATS ON AGENCY STATS PAGE
class StatsRepository{

    public function getAgencyInjuries($agency_id, $start_date, $end_date){
        //convert the dates into a carbon object, or set them to null if they're empty
        $agency = \App\Agency::where('id', $agency_id)->first();
        $use_dates = false;
        if($start_date === "" || $start_date === "null" || $start_date === NULL){
            $start_date = NULL;
        }else{
            $start_date = Carbon::createFromFormat("m-d-Y", $start_date);
        }
        if($end_date === "" || $end_date === "null" || $end_date === NULL){
            $end_date = NULL;
        }else{
            $end_date = Carbon::createFromFormat("m-d-Y", $end_date);
        }
        //if the start and end dates are set, we only want to report on injuries reported between those dates
        if($start_date !== NULL && $end_date !== NULL){
            $injuries = DB::table('companies')->where('agency_id', $agency_id)
                ->where('activated', 1)
                ->join('injuries', 'companies.id', '=', 'injuries.company_id')
                //->where('injuries.type', 'Work Comp')
                //exclude preexisting injuries
                //->where('injuries.in_progress', 0)
                ->where('injuries.hidden', 0)
                ->where('injuries.created_at', '<=', $end_date)
                ->where('injuries.created_at', '>=', $start_date)
                ->join('first_report_of_injuries', 'injuries.id', '=', 'first_report_of_injuries.injury_id')
                ->select('injuries.*', 'first_report_of_injuries.action_taken', 'first_report_of_injuries.action_taken_at')->get();
            //otherwise, use all injuries
        }else{
            $injuries = DB::table('companies')->where('agency_id', $agency_id)
                ->where('activated', 1)
                ->join('injuries', 'companies.id', '=', 'injuries.company_id')
                //exclude preexisting injuries
                //->where('injuries.in_progress', 0)
                ->where('injuries.hidden', 0)
                //->where('injuries.type', 'Work Comp')
                //->where('injuries.created_at', '<=', Carbon::now()->subWeek())
                ->join('first_report_of_injuries', 'injuries.id', '=', 'first_report_of_injuries.injury_id')
                ->select('injuries.*', 'first_report_of_injuries.action_taken', 'first_report_of_injuries.action_taken_at')->get();

        }

        return $injuries;
    }

    public function getCompanyInjuries($company_id, $start_date, $end_date){
        //convert the dates into a carbon object, or set them to null if they're empty
        if($start_date === "" || $start_date === "null" || $start_date === NULL){
            $start_date = NULL;
            Log::debug('repo start date is null');
        }else{
            $start_date = Carbon::createFromFormat("m-d-Y", $start_date);
            Log::debug('repo start date is NOT null');
        }
        if($end_date === "" || $end_date === "null" || $end_date === NULL){
            $end_date = NULL;
            Log::debug('repo end date is null');
        }else{
            $end_date = Carbon::createFromFormat("m-d-Y", $end_date);
            Log::debug('repo end date is NOT null');
        }
        if($start_date !== NULL && $end_date !== NULL){
            Log::debug('repo getting injuries with Dates');
            $injuries = DB::table('injuries')->where('company_id', $company_id)
                ->where('injuries.created_at', '<=', $end_date)
                ->where('injuries.created_at', '>=', $start_date)
                ->where('injuries.hidden', 0)
                ->join('first_report_of_injuries', 'injuries.id', '=', 'first_report_of_injuries.injury_id')
                ->select('injuries.*', 'first_report_of_injuries.action_taken', 'first_report_of_injuries.action_taken_at')
                ->get();
            // dd($injuries);
        }else{
            Log::debug('repo getting injuries without dates');
            $injuries = DB::table('injuries')->where('company_id', $company_id)
                //exclude preexisting injuries
                //->where('injuries.in_progress', 0)
                ->where('injuries.hidden', 0)
                //->where('injuries.created_at', '<=', Carbon::now()->subWeek())
                ->join('first_report_of_injuries', 'injuries.id', '=', 'first_report_of_injuries.injury_id')
                ->select('injuries.*', 'first_report_of_injuries.action_taken', 'first_report_of_injuries.action_taken_at')
                ->get();

        }
        return $injuries;

    }

    public function returnStatsArray($injuries){
        $stats_array['average_injury_length'] = $this->calculateAverageInjuryLength($injuries);
        $stats_array['average_reporting_time'] = $this->calculateAverageReportingTime($injuries);
        $stats_array['average_first_report_time'] = $this->calculateAverageFirstReportDeliveryTime($injuries);
        $stats_array['average_paid_costs'] = $this->calculateAveragePaidCostsOpenClaims($injuries);
        $stats_array['average_reserve_costs'] = $this->calculateAverageReserveCostsOpenClaims($injuries);
        $stats_array['average_paid_medical'] = $this->calculateAverageIndividualCost("medical_costs", $injuries);
        $stats_array['average_paid_legal'] = $this->calculateAverageIndividualCost("legal_costs", $injuries);
        $stats_array['average_paid_indemnity'] = $this->calculateAverageIndividualCost("indemnity_costs", $injuries);
        $stats_array['average_paid_miscellaneous'] = $this->calculateAverageIndividualCost("miscellaneous_costs", $injuries);
        $stats_array['average_reserve_medical'] = $this->calculateAverageIndividualCost("reserve_medical_costs", $injuries);
        $stats_array['average_reserve_legal'] = $this->calculateAverageIndividualCost("reserve_legal_costs", $injuries);
        $stats_array['average_reserve_indemnity'] = $this->calculateAverageIndividualCost("reserve_indemnity_costs", $injuries);
        $stats_array['average_reserve_miscellaneous'] = $this->calculateAverageIndividualCost("reserve_miscellaneous_costs", $injuries);
        $stats_array['average_final_cost'] = $this->calculateAverageFinalCost($injuries);
        $stats_array['average_final_medical'] = $this->calculateAverageIndividualFinalCost("final_medical_cost", $injuries);
        $stats_array['average_final_legal'] = $this->calculateAverageIndividualFinalCost("final_legal_cost", $injuries);
        $stats_array['average_final_indemnity'] = $this->calculateAverageIndividualFinalCost("final_indemnity_cost", $injuries);
        $stats_array['average_final_miscellaneous'] = $this->calculateAverageIndividualFinalCost("final_misc_cost", $injuries);
        $stats_array['average_severity'] = $this->calculateAverageSeverity($injuries);
        $stats_array['reserve_trend'] = $this->calculateReserveTrend($injuries);
        $stats_array['average_reserve_movement'] = $this->calculateAverageReserveMovement($injuries);
        $stats_array['average_reserve_increase'] = $this->calculateAverageReserveIncrease($injuries);
        $stats_array['average_reserve_decrease'] = $this->calculateAverageReserveDecrease($injuries);
        $stats_array['average_injury_type'] = $this->calculateAverageInjuryType($injuries);
        $stats_array['average_injury_location'] = $this->calculateAverageInjuryLocation($injuries);
        $stats_array['total_injury_count'] = $this->calculateInjuryCount($injuries);
        $stats_array['open_injury_count'] = $this->calculateOpenInjuryCount($injuries);
        $stats_array['resolved_injury_count'] = $this->calculateResolvedInjuryCount($injuries);
        $stats_array['total_paid_medical'] = $this->calculateTotalIndividualCost("medical_costs", $injuries);
        $stats_array['total_paid_legal'] = $this->calculateTotalIndividualCost("legal_costs", $injuries);
        $stats_array['total_paid_indemnity'] = $this->calculateTotalIndividualCost("indemnity_costs", $injuries);
        $stats_array['total_paid_miscellaneous'] = $this->calculateTotalIndividualCost("miscellaneous_costs", $injuries);
        $stats_array['total_reserve_medical'] = $this->calculateTotalIndividualCost("reserve_medical_costs", $injuries);
        $stats_array['total_reserve_legal'] = $this->calculateTotalIndividualCost("reserve_legal_costs", $injuries);
        $stats_array['total_reserve_indemnity'] = $this->calculateTotalIndividualCost("reserve_indemnity_costs", $injuries);
        $stats_array['total_reserve_miscellaneous'] = $this->calculateTotalIndividualCost("reserve_miscellaneous_costs", $injuries);
        $stats_array['total_final_medical'] = $this->calculateTotalIndividualFinalCost("final_medical_cost", $injuries);
        $stats_array['total_final_legal'] = $this->calculateTotalIndividualFinalCost("final_legal_cost", $injuries);
        $stats_array['total_final_indemnity'] = $this->calculateTotalIndividualFinalCost("final_indemnity_cost", $injuries);
        $stats_array['total_final_miscellaneous'] = $this->calculateTotalIndividualFinalCost("final_misc_cost", $injuries);
        $stats_array['reporting_time_array'] = $this->calculateReportingTimeArray($injuries);
        $stats_array['daily_breakdown'] = $this->calculateDailyBreakdown($injuries);
        $stats_array['injury_location_breakdown'] = $this->calculateInjuryLocationBreakdown($injuries);
        $stats_array['injury_type_breakdown'] = $this->calculateInjuryTypeBreakdown($injuries);
        $stats_array['total_paid'] = $this->calculateTotalPaid($injuries);
        $stats_array['total_reserves'] = $this->calculateTotalReserves($injuries);
        $stats_array['total_incurred'] = $this->calculateTotalIncurred($injuries);
        return $stats_array;
    }

    public function returnZenboardStatsArray($injuries){
        $stats_array['reporting_time_array'] = $this->calculateReportingTimeArray($injuries);
        $stats_array['total_paid_medical'] = $this->calculateTotalIndividualCost("medical_costs", $injuries);
        $stats_array['total_paid_legal'] = $this->calculateTotalIndividualCost("legal_costs", $injuries);
        $stats_array['total_paid_indemnity'] = $this->calculateTotalIndividualCost("indemnity_costs", $injuries);
        $stats_array['total_paid_miscellaneous'] = $this->calculateTotalIndividualCost("miscellaneous_costs", $injuries);
        $stats_array['total_reserve_medical'] = $this->calculateTotalIndividualCost("reserve_medical_costs", $injuries);
        $stats_array['total_reserve_legal'] = $this->calculateTotalIndividualCost("reserve_legal_costs", $injuries);
        $stats_array['total_reserve_indemnity'] = $this->calculateTotalIndividualCost("reserve_indemnity_costs", $injuries);
        $stats_array['total_reserve_miscellaneous'] = $this->calculateTotalIndividualCost("reserve_miscellaneous_costs", $injuries);
        $stats_array['total_open'] = $this->calculateTotalCostOfClaims($injuries, 0);
        $stats_array['total_closed'] = $this->calculateTotalCostOfClaims($injuries, 1);
        $stats_array['average_reporting_time'] = $this->calculateAverageReportingTime($injuries);
        return $stats_array;
    }

    public function calculateAverageInjuryLength($injuries, $format_data = true){
        $days = 0;
        $count = 0;
        foreach($injuries as $injury){
            if($injury->resolved === 1 && !is_null($injury->resolved_at)){
                $created_at = Carbon::createFromFormat("Y-m-d H:i:s", $injury->created_at);
                $resolved_at = Carbon::createFromFormat("Y-m-d H:i:s", $injury->resolved_at);
                $count++;
                $days+= $created_at->diffInDays($resolved_at);
            }
        }
        if($days > 0 && $count > 0){
            $days = $days / $count;
            $days = round($days, 0);
        }
        if($format_data === false){
            return $days;
        }else{
            if($days === 1){
                return $days . " Day";
            }else{
                return $days . " Days";
            }
        }
    }

    public function calculateAverageReportingTime($injuries, $format_data = true){
        $hours = 0;
        $count = 0;
        foreach($injuries as $injury){
            $company_joined_on = DB::table('companies')->where('id', $injury->company_id)->value('created_at');
            $company_joined_on = Carbon::createFromFormat("Y-m-d H:i:s", $company_joined_on);

            $reported_at = Carbon::createFromFormat("Y-m-d H:i:s", $injury->created_at);
            $occurred_at = Carbon::createFromFormat("m-d-Y g:i a", $injury->injury_date);

            if($occurred_at->gt($company_joined_on)){
                //injury occured after company joined Zenjuries, count it
                $hours+= $reported_at->diffInHours($occurred_at);
                $count++;
            }


        }
        if($hours > 0 && $count > 0){
            $hours = $hours / $count;
            $hours = round($hours, 0);
        }
        if($format_data === false){
            return $hours;
        }else{
            if($hours === 1){
                return $hours . " Hour";
            }else{
                return $hours . " Hours";
            }
        }
    }

    public function calculateReportingTimeArray($injuries, $format_data = true){
        $same_day = 0;
        $next_day = 0;
        $same_week = 0;
        $two_weeks = 0;
        $great_than_two_weeks = 0;
        $count = 0;
        foreach($injuries as $injury){
            $company_joined_on = DB::table('companies')->where('id', $injury->company_id)->value('created_at');
            $company_joined_on = Carbon::createFromFormat("Y-m-d H:i:s", $company_joined_on);

            $reported_at = Carbon::createFromFormat("Y-m-d H:i:s", $injury->created_at);
            $occurred_at = Carbon::createFromFormat("m-d-Y g:i a", $injury->injury_date);

            if($occurred_at->gt($company_joined_on)){
                //injury occured after company joined Zenjuries, count it
                $hours = $reported_at->diffInHours($occurred_at);
                if($hours <= 24){
                    $same_day++;
                }elseif($hours > 24 && $hours <= 48){
                    $next_day++;
                }elseif($hours > 48 && $hours <= 168){
                    $same_week++;
                }elseif($hours > 168 && $hours <= 336){
                    $two_weeks++;
                }else{
                    $great_than_two_weeks++;
                }
                $count++;
            }


        }

        $report_time = [$same_day, $next_day, $same_week, $two_weeks, $great_than_two_weeks];
        return $report_time;
    }

    public function calculateAverageFirstReportDeliveryTime($injuries, $format_data = true){
        $hours = 0;
        $count = 0;
        foreach($injuries as $injury){
            if($injury->action_taken === 1 && !is_null($injury->action_taken_at)){
                $company_joined_on = DB::table('companies')->where('id', $injury->company_id)->value('created_at');
                $company_joined_on = Carbon::createFromFormat("Y-m-d H:i:s", $company_joined_on);
                $reported_at = Carbon::createFromFormat("m-d-Y g:i a", $injury->injury_date);
                $action_taken_at = Carbon::createFromFormat("Y-m-d H:i:s", $injury->action_taken_at);
                if($reported_at->gt($company_joined_on)){
                    $count++;
                    $hours+= $reported_at->diffInHours($action_taken_at);
                }
            }
        }
        if($hours > 0 && $count > 0){
            $hours = $hours / $count;
            $hours = round($hours, 0);
        }
        if($format_data === false){
            return $hours;
        }else{
            if($hours === 1){
                return $hours . " Hour";
            }else{
                return $hours . " Hours";
            }
        }
    }

    public function calculateAveragePaidCostsOpenClaims($injuries, $format_data = true){
        $total = 0;
        $count = 0;

        foreach($injuries as $injury){
            if($injury->resolved === 0){
                $paid_legal = DB::table('legal_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
                $paid_medical = DB::table('medical_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
                $paid_indemnity = DB::table('indemnity_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
                $paid_miscellaneous = DB::table('miscellaneous_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');

                if(!is_null($paid_legal) || !is_null($paid_medical) || !is_null($paid_indemnity) || !is_null($paid_miscellaneous)){
                    $total += (is_null($paid_legal) ? 0 : $paid_legal);
                    $total += (is_null($paid_medical) ? 0 : $paid_medical);
                    $total += (is_null($paid_indemnity) ? 0 : $paid_indemnity);
                    $total += (is_null($paid_miscellaneous) ? 0 : $paid_miscellaneous);
                    $count++;
                }

            }
        }

        if($total > 0 && $count > 0){
            $total = $total / $count;
        }

        if($format_data === false){
            return number_format(round($total, 2), 2);
        }else{
            return "$" . number_format(round($total, 2), 2);
        }
    }

    public function calculateAverageReserveCostsOpenClaims($injuries, $format_data = true){
        $total = 0;
        $count = 0;
        foreach($injuries as $injury){
            if($injury->resolved === 0){
                $reserve_legal = DB::table('reserve_legal_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
                $reserve_medical = DB::table('reserve_medical_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
                $reserve_indemnity = DB::table('reserve_indemnity_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
                $reserve_miscellaneous = DB::table('reserve_miscellaneous_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');

                if(!is_null($reserve_legal) || !is_null($reserve_medical) || !is_null($reserve_indemnity) || !is_null($reserve_miscellaneous)){
                    $total += (is_null($reserve_legal) ? 0 : $reserve_legal);
                    $total += (is_null($reserve_medical) ? 0 : $reserve_medical);
                    $total += (is_null($reserve_indemnity) ? 0 : $reserve_indemnity);
                    $total += (is_null($reserve_miscellaneous) ? 0 : $reserve_miscellaneous);
                    $count++;
                }

            }
        }
        if($total > 0 && $count > 0){
            $total = $total / $count;
        }
        if($format_data === false){
            return number_format(round($total, 2), 2);
        }else{
            return "$" . number_format(round($total, 2), 2);
        }
    }

    //$cost_table_name can be "legal_costs", "medical_costs", "indemnity_costs", "miscellaneous_costs",
    // "reserve_legal_costs", "reserve_medical_costs", "reserve_indemnity_costs", "reserve_miscellaneous_costs"
    public function calculateAverageIndividualCost($cost_table_name, $injuries, $format_data = true){
        $total = 0;
        $count = 0;
        foreach($injuries as $injury){
            if($injury->resolved === 0){
                $cost = DB::table($cost_table_name)->where('injury_id', $injury->id)->where('current', 1)->value('cost');
                if(!is_null($cost)){
                    $total+= (is_null($cost) ? 0 : $cost);
                    $count++;
                }

            }
        }
        if($total > 0 && $count > 0){
            $total = $total / $count;
        }
        if($format_data === false){
            return number_format(round($total, 2), 2);
        }else {
            return "$" . number_format(round($total, 2), 2);
        }

    }
    public function calculateTotalPaid($injuries){
        $total = 0;
        foreach($injuries as $injury){
            $injury = Injury::where('id', $injury->id)->first();
            $total += $injury->getPaidTotal();
        }
        if ($total > 0) return '$'.number_format((float)$total, 2, '.', '');
        return $total;
    }
    public function calculateTotalReserves($injuries){
        $total = 0;
        foreach($injuries as $injury){
            $injury = Injury::where('id', $injury->id)->first();
            $total += $injury->getReserveTotal();
        }

        if ($total > 0) return '$'.number_format((float)$total, 2, '.', '');
        return $total;
    }
    public function calculateTotalIncurred($injuries){
        $total = 0;
        foreach($injuries as $injury){
            $injury = Injury::where('id', $injury->id)->first();
            $total += $injury->getIncurredTotal();
        }

        if ($total > 0) return '$'.number_format((float)$total, 2, '.', '');
        return $total;
    }

    public function calculateTotalIndividualCost($cost_table_name, $injuries, $format_data = true){
        $total = 0;
        $count = 0;
        foreach($injuries as $injury){
            $cost = DB::table($cost_table_name)->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            if(!is_null($cost)){
                $total+= (is_null($cost) ? 0 : $cost);
                $count++;
            }

        }
        if($format_data === false){
            return number_format(round($total, 2), 2);
        }else {
            return "$" . number_format(round($total, 2), 2);
        }
    }

    public function calculateAverageFinalCost($injuries, $format_data = true){
        $total = 0;
        $count = 0;
        foreach($injuries as $injury){
            if(!is_null($injury->final_cost) && $injury->resolved === 1){
                $total += $injury->final_cost;
                $count++;
            }
        }
        if($total > 0 && $count > 0){
            $total = $total / $count;
        }
        if($format_data === false){
            return number_format(round($total, 2), 2);
        }else{
            return "$" . number_format(round($total, 2), 2);
        }

    }

    //$cost_type can be "final_medical_cost", "final_indemnity_cost", "final_legal_cost", "final_misc_cost"
    public function calculateAverageIndividualFinalCost($cost_type, $injuries, $format_data = true){
        $total = 0;
        $count = 0;
        foreach($injuries as $injury){
            if(!is_null($injury->$cost_type) && $injury->resolved === 1){
                $total += $injury->$cost_type;
                $count++;
            }
        }
        if($total > 0 && $count > 0){
            $total = $total / $count;
        }
        if($format_data === false){
            return number_format(round($total, 2), 2);
        }else{
            return "$" . number_format(round($total, 2), 2);
        }
    }

    public function calculateTotalIndividualFinalCost($cost_type, $injuries, $format_data = true){
        $total = 0;
        $count = 0;
        foreach($injuries as $injury){
            if(!is_null($injury->$cost_type) && $injury->resolved === 1){
                $total += $injury->$cost_type;
                $count++;
            }
        }

        if($format_data === false){
            return number_format(round($total, 2), 2);
        }else{
            return "$" . number_format(round($total, 2), 2);
        }
    }

    public function calculateAverageSeverity($injuries){
        $severity_count['severe'] = 0;
        $severity_count['moderate'] = 0;
        $severity_count['mild'] = 0;

        foreach($injuries as $injury){
            if($injury->severity === "Severe"){
                $severity_count['severe']++;
            }else if($injury->severity === "Moderate"){
                $severity_count['moderate']++;
            }else if($injury->severity === "Mild"){
                $severity_count['mild']++;
            }
        }
        //Log::debug(var_export($severity_count, true));
        if($severity_count['severe'] !== 0 || $severity_count['moderate'] !== 0 || $severity_count['mild'] !== 0){
            //max($severity_count) will return the highest value in the array
            $average_severity = max($severity_count);
            //array_search() will use that value and return that value's key
            $average_severity = array_search($average_severity, $severity_count);
            //in the case of multiple max values, the first matching array key (severe, then moderate, then mild) will be returned
            return ucfirst($average_severity);
        }else{
            return "N/A";
        }

    }

    public function getFirstAndFinalReserveTotals($injury){
        $first_reserve_total = 0;
        $final_reserve_total = 0;

        $first_reserve_medical = DB::table('reserve_medical_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
        $first_reserve_total+= (is_null($first_reserve_medical) ? 0 : $first_reserve_medical);

        $first_reserve_legal = DB::table('reserve_legal_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
        $first_reserve_total+= (is_null($first_reserve_legal) ? 0 : $first_reserve_legal);

        $first_reserve_indemnity = DB::table('reserve_indemnity_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
        $first_reserve_total+= (is_null($first_reserve_indemnity) ? 0 : $first_reserve_indemnity);

        $first_reserve_misc = DB::table('reserve_miscellaneous_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
        $first_reserve_total+= (is_null($first_reserve_misc) ? 0 : $first_reserve_misc);

        $final_reserve_medical = DB::table('reserve_medical_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
        $final_reserve_total+= (is_null($final_reserve_medical) ? 0 : $final_reserve_medical);

        $final_reserve_legal = DB::table('reserve_legal_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
        $final_reserve_total+= (is_null($final_reserve_legal) ? 0 : $final_reserve_legal);

        $final_reserve_indemnity = DB::table('reserve_indemnity_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
        $final_reserve_total+= (is_null($final_reserve_indemnity) ? 0 : $final_reserve_indemnity);

        $final_reserve_misc = DB::table('reserve_miscellaneous_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
        $final_reserve_total+= (is_null($final_reserve_misc) ? 0 : $final_reserve_misc);

        $reserve_array = array();
        $reserve_array['first'] = $first_reserve_total;
        $reserve_array['final'] = $final_reserve_total;
        return $reserve_array;
    }

    //determine whether on average the reserves increase or decrease from the first value to the end of the claim
    public function calculateReserveTrend($injuries){
        $increased_count = 0;
        $decreased_count = 0;
        foreach($injuries as $injury){
            /*$reserve_array = $this->getFirstAndFinalReserveTotals($injury);
            $first_reserve_total = $reserve_array['first'];
            $final_reserve_total = $reserve_array['final'];
            */
            $first_reserve_total = 0;
            $final_reserve_total = 0;

            $first_reserve_medical = DB::table('reserve_medical_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_medical) ? 0 : $first_reserve_medical);

            $first_reserve_legal = DB::table('reserve_legal_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_legal) ? 0 : $first_reserve_legal);

            $first_reserve_indemnity = DB::table('reserve_indemnity_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_indemnity) ? 0 : $first_reserve_indemnity);

            $first_reserve_misc = DB::table('reserve_miscellaneous_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_misc) ? 0 : $first_reserve_misc);

            $final_reserve_medical = DB::table('reserve_medical_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_medical) ? 0 : $final_reserve_medical);

            $final_reserve_legal = DB::table('reserve_legal_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_legal) ? 0 : $final_reserve_legal);

            $final_reserve_indemnity = DB::table('reserve_indemnity_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_indemnity) ? 0 : $final_reserve_indemnity);

            $final_reserve_misc = DB::table('reserve_miscellaneous_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_misc) ? 0 : $final_reserve_misc);

            if($final_reserve_total < $first_reserve_total){
                $decreased_count++;
            }else if($final_reserve_total > $first_reserve_total){
                $increased_count++;
            }
        }
        if($increased_count > $decreased_count){
            return "Increased";
        }else if($increased_count < $decreased_count){
            return "Decreased";
        }else if($increased_count == $decreased_count){
            return "N/A";
        }
    }

    public function calculateAverageReserveMovement($injuries, $format_data = true){
        $total_reserve_movement = 0;
        $count = 0;
        foreach($injuries as $injury){
            /*$reserve_array = $this->getFirstAndFinalReserveTotals($injury);
            $first_reserve_total = $reserve_array['first'];
            $final_reserve_total = $reserve_array['final'];*/
            $first_reserve_total = 0;
            $final_reserve_total = 0;

            $first_reserve_medical = DB::table('reserve_medical_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_medical) ? 0 : $first_reserve_medical);

            $first_reserve_legal = DB::table('reserve_legal_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_legal) ? 0 : $first_reserve_legal);

            $first_reserve_indemnity = DB::table('reserve_indemnity_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_indemnity) ? 0 : $first_reserve_indemnity);

            $first_reserve_misc = DB::table('reserve_miscellaneous_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_misc) ? 0 : $first_reserve_misc);

            $final_reserve_medical = DB::table('reserve_medical_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_medical) ? 0 : $final_reserve_medical);

            $final_reserve_legal = DB::table('reserve_legal_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_legal) ? 0 : $final_reserve_legal);

            $final_reserve_indemnity = DB::table('reserve_indemnity_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_indemnity) ? 0 : $final_reserve_indemnity);

            $final_reserve_misc = DB::table('reserve_miscellaneous_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_misc) ? 0 : $final_reserve_misc);

            if($final_reserve_total !== 0 && $first_reserve_total !== 0){
                $reduction = $first_reserve_total - $final_reserve_total;
                $total_reserve_movement += $reduction;
                $count++;
            }
        }
        if($total_reserve_movement !== 0 && $count > 0){
            $total_reserve_movement = $total_reserve_movement / $count;
        }
        if($format_data === true){
            if($total_reserve_movement < 0 || $total_reserve_movement === 0){
                return "$" . number_format(round(abs($total_reserve_movement), 2), 2) . " Increase";
            }else{
                return "$" . number_format(round(abs($total_reserve_movement), 2), 2) . " Decrease";
            }
        }else{
            return number_format(round($total_reserve_movement, 2), 2);
        }

        //return "$" . round($total_reserve_movement, 2);
    }

    public function calculateAverageReserveIncrease($injuries, $format_data = true){
        $total_reserve_increase = 0;
        $count = 0;
        foreach($injuries as $injury){

            $first_reserve_total = 0;
            $final_reserve_total = 0;

            $first_reserve_medical = DB::table('reserve_medical_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_medical) ? 0 : $first_reserve_medical);

            $first_reserve_legal = DB::table('reserve_legal_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_legal) ? 0 : $first_reserve_legal);

            $first_reserve_indemnity = DB::table('reserve_indemnity_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_indemnity) ? 0 : $first_reserve_indemnity);

            $first_reserve_misc = DB::table('reserve_miscellaneous_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_misc) ? 0 : $first_reserve_misc);

            $final_reserve_medical = DB::table('reserve_medical_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_medical) ? 0 : $final_reserve_medical);

            $final_reserve_legal = DB::table('reserve_legal_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_legal) ? 0 : $final_reserve_legal);

            $final_reserve_indemnity = DB::table('reserve_indemnity_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_indemnity) ? 0 : $final_reserve_indemnity);

            $final_reserve_misc = DB::table('reserve_miscellaneous_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_misc) ? 0 : $final_reserve_misc);

            if($final_reserve_total > $first_reserve_total){
                $increase = $final_reserve_total - $first_reserve_total;
                $total_reserve_increase+= $increase;
                $count++;
            }
        }
        if($total_reserve_increase > 0 && $count > 0){
            $total_reserve_increase = $total_reserve_increase / $count;
        }
        if($format_data === false){
            return number_format(round($total_reserve_increase, 2), 2);
        }else{
            return "$" . number_format(round($total_reserve_increase, 2), 2);
        }
    }

    public function calculateAverageReserveDecrease($injuries, $format_data = true){
        $total_reserve_decrease = 0;
        $count = 0;
        foreach($injuries as $injury){

            $first_reserve_total = 0;
            $final_reserve_total = 0;

            $first_reserve_medical = DB::table('reserve_medical_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_medical) ? 0 : $first_reserve_medical);

            $first_reserve_legal = DB::table('reserve_legal_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_legal) ? 0 : $first_reserve_legal);

            $first_reserve_indemnity = DB::table('reserve_indemnity_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_indemnity) ? 0 : $first_reserve_indemnity);

            $first_reserve_misc = DB::table('reserve_miscellaneous_costs')->where('injury_id', $injury->id)->orderBy('created_at', 'asc')->value('cost');
            $first_reserve_total+= (is_null($first_reserve_misc) ? 0 : $first_reserve_misc);

            $final_reserve_medical = DB::table('reserve_medical_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_medical) ? 0 : $final_reserve_medical);

            $final_reserve_legal = DB::table('reserve_legal_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_legal) ? 0 : $final_reserve_legal);

            $final_reserve_indemnity = DB::table('reserve_indemnity_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_indemnity) ? 0 : $final_reserve_indemnity);

            $final_reserve_misc = DB::table('reserve_miscellaneous_costs')->where('injury_id', $injury->id)->where('current', 1)->value('cost');
            $final_reserve_total+= (is_null($final_reserve_misc) ? 0 : $final_reserve_misc);

            if($first_reserve_total > $final_reserve_total){
                $decrease = $first_reserve_total - $final_reserve_total;
                $total_reserve_decrease+= $decrease;
                $count++;
            }
        }
        if($total_reserve_decrease > 0 && $count > 0){
            $total_reserve_decrease = $total_reserve_decrease / $count;
        }
        if($format_data === false){
            return number_format(round($total_reserve_decrease, 2), 2);
        }else{
            return "$" . number_format(round($total_reserve_decrease, 2), 2);
        }
    }

    //public function get

    public function calculateAverageInjuryType($injuries){
        $injury_id_array = array();
        foreach($injuries as $injury){
            array_push($injury_id_array, $injury->id);
        }
        /*
        $injury_id_array = array_map(function($injuries) {
            return is_object($injuries) ? $injuries->id : $injuries['id'];
        }, $injury_array);*/
        $injury_types = DB::table('injury_types')->whereIn('injury_id', $injury_id_array)->select('type')->get();
        $injury_types = collect($injury_types)->map(function($item, $key){
            return $item->type;
        });
        $average_injury_type = $this->array_mode($injury_types);
        return $average_injury_type;
        //return $injury_types;
    }

    public function calculateAverageInjuryLocation($injuries){
        $injury_id_array = array();
        foreach($injuries as $injury){
            array_push($injury_id_array, $injury->id);
        }
        /*
        $injury_id_array = array_map(function($injuries) {
            return is_object($injuries) ? $injuries->id : $injuries['id'];
        }, $injuries);*/
        $injury_locations = DB::table('injury_locations')->whereIn('injury_id', $injury_id_array)->select('location')->get();
        $injury_locations = collect($injury_locations)->map(function($item, $key){
            return $item->location;
        });
        $average_injury_location = $this->array_mode($injury_locations);
        return $average_injury_location;
    }

    public function calculateInjuryCount($injuries){
        $count = count($injuries);
        return $count;
    }

    public function calculateOpenInjuryCount($injuries){
        $count = 0;
        foreach($injuries as $injury){
            if($injury->resolved === 0){
                $count++;
            }
        }
        return $count;
    }

    public function calculateResolvedInjuryCount($injuries){
        $count = 0;
        foreach($injuries as $injury){
            if($injury->resolved === 1){
                $count++;
            }
        }
        return $count;
    }

    //we will use this function to get the mode of both the injury types and the injury locations
    function array_mode($arr) {
        $count = array();
        //var_dump($arr);
        foreach ($arr as $val) {
            if (!isset($count[$val])) { $count[$val] = 0; }
            $count[$val]++;
        }
        arsort($count);
        return key($count);
    }

    public function calculateDailyBreakdown($injuries){
        $mon = 0;
        $tues = 0;
        $wedn = 0;
        $thurs = 0;
        $fri = 0;
        $sat = 0;
        $sun = 0;
        foreach($injuries as $injury){
            //$created_at = Carbon::createFromFormat("Y-m-d H:i:s", $injury->created_at);
            $created_at = Carbon::createFromFormat("m-d-Y H:i a", $injury->injury_date);
            $day = $created_at->englishDayOfWeek;
            if($day === 'Monday'){
                $mon++;
            }elseif($day === 'Tuesday'){
                $tues++;
            }elseif($day === 'Wednesday'){
                $wedn++;
            }elseif($day === 'Thursday'){
                $thurs++;
            }elseif($day === 'Friday'){
                $fri++;
            }elseif($day === 'Saturday'){
                $sat++;
            }elseif($day === 'Sunday'){
                $sun++;
            }
        }

        $days = [
            "Monday" => $mon,
            "Tuesday" => $tues,
            "Wednesday" => $wedn,
            "Thursday" => $thurs,
            "Friday" => $fri,
            "Saturday" => $sat,
            "Sunday" => $sun
        ];

        return $days;
    }

    public function calculateInjuryLocationBreakdown($injuries){
        $locations_array = array();
        foreach($injuries as $injury){
            $injury_location = DB::table('injury_locations')->where('injury_id', $injury->id)->select('location')->get();
            foreach($injury_location as $location){
                array_push($locations_array, $location->location);
            }
        }

        $location_count_for_key = array_count_values($locations_array);
        $location_count_for_occur = array_count_values($locations_array);
        rsort($location_count_for_occur);
        arsort($location_count_for_key);
        $location = array_slice(array_keys($location_count_for_key), 0, 5, true);
        $count = array_slice($location_count_for_occur, 0, 5);
        return [$location, $count];


    }

    public function calculateInjuryTypeBreakdown($injuries){
        $types_array = array();
        foreach($injuries as $injury){
            $injury_type = DB::table('injury_types')->where('injury_id', $injury->id)->select('type')->get();
            foreach($injury_type as $type){
                array_push($types_array, $type->type);
            }
        }

        $type_count_for_key = array_count_values($types_array);
        $type_count_for_occur = array_count_values($types_array);
        rsort($type_count_for_occur);
        arsort($type_count_for_key);
        $type = array_slice(array_keys($type_count_for_key), 0, 5, true);
        $count = array_slice($type_count_for_occur, 0, 5);
        return [$type, $count];


    }

    public function calculateTotalCostOfClaims($injuries, $resolved){
        $total = 0;
        $count = 0;
        $paid_table_array = ["medical_costs", "legal_costs", "indemnity_costs", "miscellaneous_costs"];
        foreach($injuries as $injury){
            for($i = 0; $i < 4; $i++){
                $cost = DB::table($paid_table_array[$i])->where('injury_id', $injury->id)->where('resolved', $resolved)->where('current', 1)->value('cost');
                if(!is_null($cost)){
                    $total+= (is_null($cost) ? 0 : $cost);
                    $count++;
                }
            }

        }
        if($format_data === false){
            return number_format(round($total, 2), 2);
        }else {
            return "$" . number_format(round($total, 2), 2);
        }
    }

    public function calculateAverageLitigation($injuries){
        $injury_count = count((array)$injuries);
        $litigated_injuries = 0;
        foreach($injuries as $injury){
            if($injury->paused_for_litigation){
                $litigated_injuries++;
            }
        }

        $percent_litigated = ($litigated_injuries/$injury_count) * 100 . "%";

        return $percent_litigated;

    }

}

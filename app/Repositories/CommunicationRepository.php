<?php namespace App\Repositories;

use App\TreePost;
use Carbon\Carbon;
use Laravel\Spark\Interactions\Settings\Teams\SendInvitation;
use Mail;
use Twilio;
use DOMPDF;
use Illuminate\Support\Facades\App;
use App\Repositories\MessagingRequestsRepository;
use App\Repositories\ZenProRepository;
use Illuminate\Support\Facades\Log;


class CommunicationRepository {

    public function __construct()
    {
        //$this->repo = $repo;

       // $zenproRepo = new ZenProRepository(new SendInvitation());

        //$this->zenpro_repo = $zenproRepo;
    }

    public function newCommunication($company_id, $injury_id, $template_id){
        $template = \App\Communication_Template::where('id', $template_id)->first();
        $team_id = \App\Company::where('id', $company_id)->value('team_id');
        $valid = \App\Team::where('id', $team_id)->value('valid');
        //we still want to send the "grace period ended" communication if the company is invalid
        if($template->template_identifier === "gracePeriodEnded"){
            $valid = 1;
        }
        if($valid === 1) {
            $communication = new \App\Communication;
            $communication->company_id = $company_id;
            $communication->injury_id = $injury_id;
            $communication->communication_template_id = $template->id;
            $communication->followup_id = $template->followup_template_id;
            $communication->save();
            return $communication->id;
        }else{
            return false;
        }
    }

    //create record new message in database, queue message for sending
    public function queueMessage($user_id, $method, $communication_id, $injury_id){
        $communication = \App\Communication::where('id', $communication_id)->first();
        $template = \App\Communication_Template::where('id', $communication->communication_template_id)->first();
        $user = \App\User::where('id', $user_id)->first();
        if($user->deleted_at !== NULL){
            return false;
        }
        if($method == 'email' or $method == 'Email'){
            if($user->email == NULL){
                return false;
            }
        }
        if($method == 'sms' or $method == 'SMS'){
            if($user->phone == NULL or $user->phone == ""){
                return false;
            }
        }
        $company = \App\Company::where('id', $communication->company_id)->first();
        $trigger = \App\Trigger::where('id', $template->trigger_id)->first();
        $message = new \App\Communication_Message;
        $message->communication_id = $communication->id;
        $message->status = "Queued";
        if($method == 'email' or $method == 'Email'){
            $message->content = $template->email_content;
            $address = \App\User::where('id', $user_id)->value('email');
            //HANDLE UPDATING THE "LAST REPORT REMINDER SENT AT" FIELD
            if($template->template_identifier == "FirstReportofInjury" or
                $template->template_identifier == "FirstReportofInjuryReminder" or
                $template->template_identifier == "FirstReportofInjuryReminderOwner" or
                $template->template_identifier == "FirstReportofInjuryFirstAid"
            ){
                $firstReport = \App\FirstReportOfInjury::where('injury_id', $injury_id)->first();
                if(!is_null($firstReport)){
                    $firstReport->last_message_sent_at = Carbon::now();
                    $firstReport->save();
                }
            }

            if($template->priority === 2){
                if($user->gets_priority_2_emails === 0){
                    $message->status = "Disabled";
                }
            }else if($template->priority === 3){
                if($user->gets_priority_3_emails === 0){
                    $message->status = "Disabled";
                }
            }else if($template->priority === 0){
                $message->status = "Disabled";
            }

        } else if ($method == 'sms' or $method == 'SMS' or $method == 'Sms'){
            $message->content = $template->sms_content;
            $address = \App\User::where('id', $user_id)->value('phone');
            // Not currently sending SMS messages, so set all to disabled
            $message->status = "Disabled";
            // if($user->send_texts === 0 or $user->communication_setting == "none"){
            //     $message->status = "Disabled";
            // }
        }

        if($injury_id !== "" && $injury_id !== NULL){
            $injury = \App\Injury::where('id', $injury_id)->first();
            if($injury !== NULL && $injury->paused_for_litigation === 1 && $template->template_identifier !== "ClaimPaused"){
                $message->status = "Disabled";
            }else if($injury !== NULL && $injury->hidden === 1){
                $message->status = "Disabled";
            }
        }

        $message->delivery_method = $method;
        $message->deliver_to_address = $address;
        $now = Carbon::now();
        $message->date_created = $now;
        $current_Hour = Carbon::createFromTime($now->hour);
        $start = Carbon::createFromTime($company->start_hour);
        $end = Carbon::createFromTime($company->end_hour);
        if($template->template_identifier !== "appointmentReminder") {
            //if the open and close times are the same, just send it whenever
            if ($start == $end || $template->template_identifier === "FirstReportofInjury") {
                $message->date_to_send = $now->copy()->addMinutes($template->trigger_interval);
                //if the close time is earlier in the day than the open time
            } else if ($end < $start) {
                //if the current hour is after they open or before they close
                //for example, if they open at 2PM and close at 2AM
                //so a communication generated at 5PM should send, so should one at 1AM.
                if ($current_Hour > $start or $current_Hour < $start) {
                    $message->date_to_send = $now->copy()->addMinutes($template->trigger_interval);
                    //but one generated at 11AM should send when they open
                } else {
                    $message->date_to_send = $start;
                }
            } else {
                if ($current_Hour >= $start and $current_Hour < $end) {
                    $message->date_to_send = $now->copy()->addMinutes($template->trigger_interval);
                } else if ($current_Hour < $start) {
                    $message->date_to_send = $start;
                } else if ($current_Hour >= $end) {
                    $message->date_to_send = $start->addDay();
                }
            }
        }else{
            $message->date_to_send = $now->copy()->addMinutes($template->trigger_interval);
        }
        $message->triggered_by = $trigger->name;
        $message->number_of_attempts = 0;
        $message->user_id = $user_id;
        $message->injury_id = $injury_id;

        $message->save();
    }

    //send all queued messages
    public function sendQueued($limit = 0){
        if ($limit > 0){
            // send chunk
            $queuedMessages = \App\Communication_Message::where(function ($query){
                $query->where('status', 'Queued')->where('date_to_send', '<', Carbon::now());
            })->orWhere(function ($query){
                $query->where('status', 'Error')->where('date_to_resend', '<', Carbon::now());
            })->orderBy('date_to_send')->limit($limit)->get();
        }
        else{
            // send all
            $queuedMessages = \App\Communication_Message::where(function ($query){
                $query->where('status', 'Queued')->where('date_to_send', '<', Carbon::now());
            })->orWhere(function ($query){
                $query->where('status', 'Error')->where('date_to_resend', '<', Carbon::now());
            })->get();
        }

        //$queuedMessages = \App\Communication_Message::where('status', 'Queued')->get();
        foreach($queuedMessages as $queuedMessage){
            $communication = \App\Communication::where('id', $queuedMessage->communication_id)->first();
            $template = \App\Communication_Template::where('id', $communication->communication_template_id)->first();
            if($queuedMessage->delivery_method == "Email"){
                try {
                    if($queuedMessage->content == "injuredEmployeeExpectationsEmail"){
                        $check = Mail::send('emailTemplates.' . $queuedMessage->content, ['queuedMessage' => $queuedMessage, 'template' => $template], function ($m) use ($queuedMessage, $template) {
                            $m->attach('public/downloads/EmployeeExpectations.pdf');
                            $m->to($queuedMessage->deliver_to_address)->subject($template->email_subject);
                        });
                    }elseif($queuedMessage->content == "firstReportEmail" or $queuedMessage->content == "firstReportFirstAidEmail"){
                        $injury_id = $queuedMessage->injury_id;
                        $data = ['injury_id' => $injury_id];
                        $injuredID = \App\Injury::where('id', $injury_id)->value('injured_employee_id');
                        $injuredName = \App\User::where('id', $injuredID)->value('name');
                        $injuredName = preg_replace('/\s+/', '', $injuredName);
                        $pdf = App::make('dompdf.wrapper');
                        $pdf->setPaper('A3')->loadVIEW('render.firstReportRender', ['data' => $data]);

                        $injury_photos = \App\InjuryImage::where('injury_id', $injury_id)->get();
                        $injury_photo_array = [];
                        foreach ($injury_photos as $injury_photo){
                            array_push($injury_photo_array, $injury_photo->location);
                        }

                        $check = Mail::send('emailTemplates.' . $queuedMessage->content, ['queuedMessage' => $queuedMessage, 'template' => $template, 'pdf' => $pdf, 'injuredName' => $injuredName, 'injury_photo_array' => $injury_photo_array], function ($m) use ($queuedMessage, $template, $pdf, $injuredName, $injury_photo_array) {
                            $m->attachData(@$pdf->output(), 'InjuryAlert' . $injuredName . '.pdf');
                            foreach($injury_photo_array as $injury_photo){
                                $m->attach(public_path() . "/" . $injury_photo);
                            }
                            $m->to($queuedMessage->deliver_to_address)->subject($template->email_subject);
                        });


                    }else if($queuedMessage->content == "educationalMaterialEmail"){
                        $company_educational_material_number = \App\Company::where('id', $communication->company_id)->value('educational_material_number');
                        $edMats = \App\EducationalMaterial::get();
                        $count = 1;
                        $subject = "Educational Material";
                        foreach($edMats as $edMat) {
                            if ($count == $company_educational_material_number) {
                                $subject = $edMat->title;
                            }
                            $count++;
                        }
                        $check = Mail::send('emailTemplates.' . $queuedMessage->content, ['queuedMessage' => $queuedMessage, 'template' => $template, 'subject' => $subject], function ($m) use ($queuedMessage, $template, $subject) {
                            $m->to($queuedMessage->deliver_to_address)->subject($subject);
                        });
                    }else if($queuedMessage->content == "officialFirstReportEmail"){
                        Log::debug("OfficialFirstReport");
                        $injury_id = $queuedMessage->injury_id;
                        $injured_employee_id = \App\Injury::where('id', $injury_id)->value('injured_employee_id');
                        $injured_employee_name = \App\User::where('id', $injured_employee_id)->value('name');
                        $injured_employee_name = preg_replace('/\s+/', '', $injured_employee_name);
                        $pdf = App::make('dompdf.wrapper');
                        $pdf->setPaper('A3')->loadVIEW('render.officialFirstReportRender', ['injuryID' => $injury_id]);
                        $check = Mail::send('emailTemplates.' . $queuedMessage->content, ['queuedMessage' => $queuedMessage, 'template' => $template, 'pdf' => $pdf, 'injuredName' => $injured_employee_name], function($m) use ($queuedMessage, $template, $pdf, $injured_employee_name){
                            $m->attachData(@$pdf->output(), 'OfficialFirstReport' . $injured_employee_name . '.pdf');
                            $m->to($queuedMessage->deliver_to_address)->subject($template->email_subject);
                        });
                    }
                    else if($queuedMessage->content == "newInjuryAdjusterEmail" ||
                        $queuedMessage->content == "newInjuryAgentEmail" ||
                        $queuedMessage->content == "newInjuryCustomerServiceEmail" ||
                        $queuedMessage->content == "newInjuryDoctorEmail" ||
                        $queuedMessage->content == "newInjuryHumanResourcesEmail" ||
                        $queuedMessage->content == "newInjuryOwnerEmail" ||
                        $queuedMessage->content == "newInjuryPhysicalTherapistEmail" ||
                        $queuedMessage->content == "newInjurySafetyCoordinatorEmail" ||
                        $queuedMessage->content == "newInjurySupervisorEmail"){
                            $injury_id = $queuedMessage->injury_id;
                            $data = ['injury_id' => $injury_id];
                            $pdf = App::make('dompdf.wrapper');
                            $pdf->setPaper('A3')->loadVIEW('render.firstReportRender', ['data' => $data]);
                            //$pdf->setPaper('A3')->loadVIEW('render.medicalDocument', ['injuryID' => $injury_id]);
                            try {
                                $pdf_output = $pdf->output();
                            } catch (\ErrorException $e){
                                Log::debug("errored");
                                $pdf = App::make('dompdf.wrapper');
                                $pdf->setPaper('A3')->loadVIEW('render.firstReportRender', ['data' => $data]);
                                $pdf_output = @$pdf->output();
                            }
                            //$pdf_output = @$pdf->output();
                            $check = Mail::send('emailTemplates.' . $queuedMessage->content, ['queuedMessage' => $queuedMessage, 'template' => $template, 'pdf_output' => $pdf_output], function ($m) use ($queuedMessage, $template, $pdf_output){
                               $m->attachData(
                                   $pdf_output,
                                   'InjuryAlert.pdf');
                               $m->to($queuedMessage->deliver_to_address)->subject($template->email_subject);
                            });
                    }
                    else {
                        $check = Mail::send('emailTemplates.' . $queuedMessage->content, ['queuedMessage' => $queuedMessage, 'template' => $template], function ($m) use ($queuedMessage, $template) {
                            $m->to($queuedMessage->deliver_to_address)->subject($template->email_subject);
                        });
                    }
                    $queuedMessage->date_sent = Carbon::now();
                    $queuedMessage->status = "Sent";
                    $queuedMessage->number_of_attempts++;
                    $queuedMessage->error_code = "";
                    $queuedMessage->error_message = "";
                    $queuedMessage->date_to_resend = "";
                    $queuedMessage->save();
                    if($template->template_identifier == "InjuredEmployeeDiary"){
                        $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
                        $injury->last_diary_reminder = Carbon::now();
                        $injury->save();
                    }else if($template->template_identifier == "FirstReportofInjury" or
                        $template->template_identifier == "FirstReportofInjuryReminder" or
                        $template->template_identifier == "FirstReportofInjuryReminderOwner" or
                        $template->template_identifier == "FirstReportofInjuryFirstAid"
                    ){
                        $firstReport = \App\FirstReportOfInjury::where('injury_id', $queuedMessage->injury_id)->first();
                        $firstReport->last_message_sent_at = Carbon::now();
                        $firstReport->save();
                    }else if($template->template_identifier == "GetClaimNumber"){
                        $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
                        $injury->claim_number_reminder_sent_at = Carbon::now();
                        $injury->save();
                    }
                    //return "Success";
                } catch (\Exception $e){
                    //var_dump($e);
                    //return "Failure";
                    $queuedMessage->error_code = get_class($e);
                    $queuedMessage->error_message = $e->getMessage();
                    $queuedMessage->number_of_attempts++;
                    if($queuedMessage->number_of_attempts >= $template->retry_attempts){
                        $queuedMessage->status = "Failed";
                        $queuedMessage->date_to_resend = NULL;
                    } else {
                        $queuedMessage->status = "Error";
                        $user = \App\User::where('id', $queuedMessage->user_id)->first();
                        $company = \App\Company::where('id', $communication->company_id)->first();
                        $now = Carbon::now();
                        $current_Hour = Carbon::createFromTime($now->hour);
                        $start = Carbon::createFromTime($company->start_hour);
                        $end = Carbon::createFromTime($company->end_hour);
                        if($current_Hour >= $start and $current_Hour < $end) {
                            $queuedMessage->date_to_resend = $now->copy()->addMinutes($template->trigger_interval);
                        } else if ($current_Hour < $start){
                            $queuedMessage->date_to_resend = $start;
                        } else if ($current_Hour >= $end){
                            $queuedMessage->date_to_resend = $start->addDay();
                        }
                    }
                    $queuedMessage->save();
                    //return "Failure";
                }
            } else if ($queuedMessage->delivery_method == "SMS"){
                $contents = view('smsTemplates.' . $queuedMessage->content, ['queuedMessage' => $queuedMessage])->render();
                try{
                    Twilio::message($queuedMessage->deliver_to_address, $contents);
                    $queuedMessage->date_sent = Carbon::now();
                    $queuedMessage->status = "Sent";
                    $queuedMessage->number_of_attempts++;
                    $queuedMessage->error_code = "";
                    $queuedMessage->error_message = "";
                    $queuedMessage->save();
                    if($template->template_identifier == "InjuredEmployeeDiary"){
                        $injury = \App\Injury::where('id', $queuedMessage->injury_id)->first();
                        $injury->last_diary_reminder = Carbon::now();
                        $injury->save();
                    }
                    //return "Success";
                } catch(\Exception $e) {
                    $queuedMessage->error_code = get_class($e);
                    $queuedMessage->error_message = $e->getMessage();
                    $queuedMessage->number_of_attempts++;
                    if($queuedMessage->number_of_attempts >= $template->retry_attempts){
                        $queuedMessage->status = "Failed";
                        $queuedMessage->date_to_resend = NULL;
                    } else {
                        $queuedMessage->status = "Error";
                        $user = \App\User::where('id', $queuedMessage->user_id)->first();
                        $company = \App\Company::where('id', $communication->company_id)->first();
                        $now = Carbon::now();
                        $current_Hour = Carbon::createFromTime($now->hour);
                        $start = Carbon::createFromTime($company->start_hour);
                        $end = Carbon::createFromTime($company->end_hour);
                        if($current_Hour >= $start and $current_Hour < $end) {
                            $queuedMessage->date_to_resend = $now->copy()->addMinutes($template->trigger_interval);
                        } else if ($current_Hour < $start){
                            $queuedMessage->date_to_resend = $start;
                        } else if ($current_Hour >= $end){
                            $queuedMessage->date_to_resend = $start->addDay();
                        }
                    }
                    $queuedMessage->save();
                    //return "Failure";
                }
            }
        }

    }

    /* ALERT USER THEY'VE RECEIVED A MESSAGE */
    public function messageReceived($user_id, $company_id){
        /* DISABLED
        $user = \App\User::where('id', $user_id)->first();
        $temp = \App\Communication_Template::where('template_identifier', 'MessageReceived')
            ->where('company_id', NULL)
            ->first();
        $communication_id = $this->newCommunication($company_id, NULL, $temp->id);
        if($communication_id) {
            $this->queueMessage($user->id, "Email", $communication_id, NULL);
        }
        */
    }
/*
    //send the message to a company's zenpro, if they have one
    public function alertZenpro($company_id, $communication_id, $injury_id = NULL){
        $zenpro = $this->zenpro_repo->getCompanyZenpro($company_id);
        if($zenpro){
            $this->queueMessage($zenpro->id, "Email", $communication_id, $injury_id);
        }
    }
*/
    /* REQUEST USER UPDATES CLAIM COSTS */
    public function claimValues($injury_id)
    {
        //$user = \App\User::where('id', $user_id)->first();
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->where(function($query){
            $query->where('position_id', 5)->orWhere('position_id', 6)->orWhere('position_id', 4);
        })->groupBy('user_id')->get();
        $temp = \App\Communication_Template::where('template_identifier', 'ClaimValue')
            ->where('company_id', NULL)->first();
        $communication_id = $this->newCommunication($injury->company_id, NULL, $temp->id);
        if ($communication_id) {
            $information_template = \App\InformationType::where('name', 'Claim Values')->first();
            foreach($squadMembers as $squadMember){
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury_id);
                //$this->repo->storeInformationRequest($information_template->name, NULL, $information_template->request_body, $squadMember->user_id, $information_template->id, $communication_id, $injury_id, $injury->company_id);
            }
            /*
            $reminder_tree_post = new TreePost();
            $reminder_tree_post->newZenfuciusReminderPost($injury->id, $squadMembers, "update the injury's Claim Costs");
            */
        }
    }

    /* GET THE CLAIM NUMBER */
    public function getClaimNumber($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->where(function($query){
            $query->where('position_id', 5)->orWhere('position_id', 6)->orWhere('position_id', 9);
        })->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'GetClaimNumber')->first();
        $information_template = \App\InformationType::where('name', 'Claim Number')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id){
            foreach ($squadMembers as $squadMember){
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury_id);
                //$this->repo->storeInformationRequest($information_template->name, NULL, $information_template->request_body, $squadMember->user_id, $information_template->id, $communication_id, $injury->id, $injury->company_id);
            }
            /*
            $reminder_tree_post = new TreePost();
            $reminder_tree_post->newZenfuciusReminderPost($injury->id, $squadMembers, "add the injury's Claim Number");
            */
        }
    }

    //** GET THE CLAIM CLOSE DATE //
    public function getClaimCloseDate($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        Log::debug($injury);
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->where(function($query){
            $query->where('position_id', 1)->orWhere('position_id', 4)->orWhere('position_id', 5)->orWhere('position_id', 6)->orWhere('position_id', 9);
        })->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'getClaimCloseDate')->first();
        Log::debug($template);
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if($communication_id){
            foreach ($squadMembers as $squadMember){
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury_id);
            }
            /*
            $reminder_tree_post = new TreePost();
            $reminder_tree_post->newZenfuciusReminderPost($injury->id, $squadMembers, "add the injury's Claim Close Date");
            */
        }
        $injury->claim_close_date_reminder_count = $injury->claim_close_date_reminder_count + 1;
        $injury->save();
    }

    //* UPDATE EMPLOYEE STATUS *
    public function updateEmployeeStatus($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->where(function($query){
            $query->where('position_id', 5)->orWhere('position_id', 6)->orWhere('position_id', 8)->orWhere('position_id', 9);
        })->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'UpdateEmployeeStatus')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id){
            foreach ($squadMembers as $squadMember){
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury_id);
            }
        }
    }

    /* NOTIFY TEAM MEMBERS AND INJURED EMPLOYEE OF CLAIM NUMBER */
    public function claimNumber($injury_id){
        //get required models
        $injury = \App\Injury::where('id', $injury_id)->first();
        //groupBy method will make sure it doesn't return duplicate users
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->groupBy('user_id')->get();
        $temp = \App\Communication_Template::where('template_identifier', 'ClaimNumber')->first();
        //create communication record
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $temp->id);
        if($communication_id){
            //send to each squad member
            foreach($squadMembers as $squadMember){
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury_id);
                $this->queueMessage($squadMember->user_id, "SMS", $communication_id, $injury_id);
            }
            //send to injured employee
            $this->queueMessage($injury->injured_employee_id, "Email", $communication_id, $injury_id);
            $this->queueMessage($injury->injured_employee_id, "SMS", $communication_id, $injury_id);
        }
    }

    /* ALERT TEAM A NEW INJURY HAS OCCURED */
    public function newInjury($injury_id, $squad_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->get();
        foreach($squadMembers as $squadMember){
            if ($squadMember->position_id == 1) {
                $temp = \App\Communication_Template::where('template_identifier', 'NewInjuryOwner')->where('company_id', NULL)->first();
            } else if ($squadMember->position_id == 2) {
                $temp = \App\Communication_Template::where('template_identifier', 'NewInjuryDoctor')->where('company_id', NULL)->first();
            } else if ($squadMember->position_id == 3) {
                $temp = \App\Communication_Template::where('template_identifier', 'NewInjuryPhysicalTherapist')->where('company_id', NULL)->first();
            } else if ($squadMember->position_id == 4) {
                $temp = \App\Communication_Template::where('template_identifier', 'NewInjuryAdjuster')->where('company_id', NULL)->first();
            } else if ($squadMember->position_id == 5) {
                $temp = \App\Communication_Template::where('template_identifier', 'NewInjuryAgent')->where('company_id', NULL)->first();
            } else if ($squadMember->position_id == 6) {
                $temp = \App\Communication_Template::where('template_identifier', 'NewInjuryCustomerService')->where('company_id', NULL)->first();
            } else if ($squadMember->position_id == 7) {
                $temp = \App\Communication_Template::where('template_identifier', 'NewInjurySafetyCoordinator')->where('company_id', NULL)->first();
            } else if ($squadMember->position_id == 8) {
                $temp = \App\Communication_Template::where('template_identifier', 'NewInjurySupervisor')->where('company_id', NULL)->first();
            } else if ($squadMember->position_id == 9) {
                $temp = \App\Communication_Template::where('template_identifier', 'NewInjuryHumanResources')->where('company_id', NULL)->first();
            } else if ($squadMember->position_id == 10) {
                $temp = \App\Communication_Template::where('template_identifier', 'NewInjuryOther')->where('company_id', NULL)->first();
            }
            $communication_id = $this->newCommunication($injury->company_id, $injury->id, $temp->id);
            if($communication_id){
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury_id);
                $this->queueMessage($squadMember->user_id, "SMS", $communication_id, $injury_id);
            }
        }
    }

    public function newInProgressInjury($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->where(function($query){
            $query->where('position_id', 1)->orWhere('position_id', 5)->orWhere('position_id', 6)->orWhere('position_id', 9);
        })->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'NewPreExistingClaim')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if($communication_id){
            foreach($squad_members as $member){
                $this->queueMessage($member->user_id, "Email", $communication_id, $injury_id);
            }
            //$this->alertZenpro($injury->company_id, $communication_id, $injury_id);
        }

    }

    public function officialFirstReport($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $recipients = \App\SquadMember::where('squad_id', $injury->squad_id)->where(function($query){
           $query->where('position_id', 9)->orWhere('position_id', 5)->orWhere('position_id', 6);
        })->groupBy('user_id')->get();

        $communication_template = \App\Communication_Template::where('template_identifier', 'OfficialFirstReport')->first();

        $communication_id = $this->newCommunication($injury->company_id, $injury_id, $communication_template->id);

        if($communication_id){
            foreach($recipients as $recipient){
                $this->queueMessage($recipient->user_id, "Email", $communication_id, $injury_id);
            }
        }

        /*
        $reminder_tree_post = new TreePost();
        $reminder_tree_post->newZenfuciusReminderPost($injury_id, $recipients, "deliver the Official First Report of Injury");
        */
    }

    /* DELIVER THE FIRST REPORT OF INJURY AND REMINDERS */
    public function firstReportOfInjury($first_report_id){
        $firstReport = \App\FirstReportOfInjury::where('id', $first_report_id)->first();
        $injury = \App\Injury::where('id', $firstReport->injury_id)->first();
        //recipients are Chief Nav and Agents
        $recipients = \App\SquadMember::where('squad_id', $firstReport->squad_id)->where(function($query){
            $query->where('position_id', 9)->orWhere('position_id', 5)->orWhere('position_id', 6);
        })->groupBy('user_id')->get();
        //template for request (internal messaging system)
        $actionTemplate = \App\Action::where('name', 'Confirm First Report Delivery')->first();
        //get the correct communication template
        if($firstReport->number_of_reminders_sent == 0){
            $commTemplate = \App\Communication_Template::where('template_identifier', 'FirstReportofInjury')->first();
        }else if($firstReport->number_of_reminders_sent > 0 and $firstReport->number_of_reminders_sent < 3){
            $commTemplate = \App\Communication_Template::where('template_identifier', 'FirstReportofInjuryReminder')->first();
        }else if($firstReport->number_of_reminders_sent >= 3){
            $commTemplate = \App\Communication_Template::where('template_identifier', 'FirstReportofInjuryReminder')->first();
            //chief exec gets involved after 2 reminders
            $ownerTemp = \App\Communication_Template::where('template_identifier', 'FirstReportofInjuryReminderOwner')->first();
        }
        //create communication, return communication_id
        $communication_id = $this->newCommunication($injury->company_id, $firstReport->injury_id, $commTemplate->id);
        //make sure the communication exists, then queue messages for each recipient
        if($communication_id) {
            foreach ($recipients as $recipient) {
                $this->queueMessage($recipient->user_id, "Email", $communication_id, $injury->id);
                //$this->repo->storeActionRequest("Confirm Injury Alert Delivery", NULL, $actionTemplate->request_body, $recipient->user_id, $actionTemplate->id, $communication_id, $injury->id, $injury->company_id);
            }
        }
        /*
        $reminder_tree_post = new TreePost();
        $reminder_tree_post->newZenfuciusReminderPost($injury->id, $recipients, "deliver the Injury Alert");
        */
        //if it's time for the owner to be notified, we'll message them too
        if (isset($ownerTemp)) {
            //make sure there is an owner
            $owner_recipients = \App\SquadMember::where('squad_id', $firstReport->squad_id)->where('position_id', 1)->groupBy('user_id')->get();
            foreach($owner_recipients as $owner_recipient){
                $communication_id = $this->newCommunication($injury->company_id, $firstReport->injury_id, $ownerTemp->id);
                $this->queueMessage($owner_recipient->user_id, "Email", $communication_id, $injury->id);
            }
            /*
            if (isset($owner_recipient)) {
                //queue their message

            }
            */
        }
        //update the reminder count for this first report of injury
        $firstReport->number_of_reminders_sent++;
        $firstReport->save();
    }

    /* PROMPTS INJURED EMPLOYEE TO POST IN DIARY */
    public function noDiaryActivity($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $injured_user_email = \App\User::where('id', $injury->injured_employee_id)->value('email');
        //if we don't have the injured user's email, then we won't continue the function
        if(!is_null($injured_user_email)){
            $temp = \App\Communication_Template::where('template_identifier', 'InjuredEmployeeDiary')->first();
            $communication_id = $this->newCommunication($injury->company_id, $injury->id, $temp->id);
            if($communication_id) {
                $this->queueMessage($injury->injured_employee_id, "Email", $communication_id, $injury->id);
            }
        }
    }

    /******* ADDED TO TEAM COMMUNICATIONS (BY ROLE) ******/

    public function handleAddedToSquadCommunication($injury_squad, $position, $user_id, $company_id, $squad_id = NULL){
        if($injury_squad === false){
            if($position === "Owner"){
                $this->addedToSquadOwner($user_id, $company_id);
            }else if($position === "Doctor"){
                $this->addedToSquadDoctor($user_id, $company_id);
            }else if($position === "Physical Therapist"){
                $this->addedToSquadPhysicalTherapist($user_id, $company_id);
            }else if($position === "Adjuster"){
                $this->addedToSquadAdjuster($user_id, $company_id);
            }else if($position === "Insurance Agent"){
                $this->addedToSquadAgent($user_id, $company_id);
            }else if($position === "Insurance Customer Service"){
                $this->addedToSquadCustomerService($user_id, $company_id);
            }else if($position === "Safety Coordinator"){
                $this->addedToSquadSafetyCoordinator($user_id, $company_id);
            }else if($position === "Supervisor"){
                $this->addedToSquadSupervisor($user_id, $company_id);
            }else if($position === "Human Resources"){
                $this->addedToSquadHumanResources($user_id, $company_id);
            }else if($position === "Other"){
                $this->addedToSquadOther($user_id, $company_id);
            }
        }else{
            $this->addedToInjurySquad($user_id, $company_id, $squad_id);
        }
    }

    public function addedToInjurySquad($user_id, $company_id, $squad_id){
        //$user = \App\User::where('id', $user_id)->first();
        $squad = \App\Squad::where('id', $squad_id)->first();
        if($squad->injury_squad === 1){
            $template = \App\Communication_Template::where('template_identifier', 'addedToInjuryTeam')->first();
            $injury = \App\Injury::where('squad_id', $squad_id)->first();
            $communication_id = $this->newCommunication($company_id, $injury->id, $template->id);
            $this->queueMessage($user_id, "Email", $communication_id, $injury->id);
        }

    }

    public function addedToSquadGeneric($user_id, $company_id){
        $template = \App\Communication_Template::where('template_identifier', 'AddedToSquadGeneric')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $template->id);
        if($communication_id) {
            $this->queueMessage($user_id, "Email", $communication_id, NULL);
            $this->queueMessage($user_id, "SMS", $communication_id, NULL);
        }
    }

    public function addedToSquadOwner($user_id, $company_id){
        $user = \App\User::where('id', $user_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'AddedToSquadOwner')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $template->id);
        if($communication_id) {
            $this->queueMessage($user->id, "Email", $communication_id, NULL);
            $this->queueMessage($user->id, "SMS", $communication_id, NULL);
        }
    }

    public function addedToSquadDoctor($user_id, $company_id){
        $user = \App\User::where('id', $user_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'AddedToSquadDoctor')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $template->id);
        if($communication_id) {
            $this->queueMessage($user->id, "Email", $communication_id, NULL);
            $this->queueMessage($user->id, "SMS", $communication_id, NULL);
        }
    }

    public function addedToSquadPhysicalTherapist($user_id, $company_id){
        $user = \App\User::where('id', $user_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'AddedToSquadPhysicalTherapist')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $template->id);
        if ($communication_id) {
            $this->queueMessage($user->id, "Email", $communication_id, NULL);
            $this->queueMessage($user->id, "SMS", $communication_id, NULL);
        }
    }

    public function addedToSquadAdjuster($user_id, $company_id){
        $user = \App\User::where('id', $user_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'AddedToSquadAdjuster')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $template->id);
        if ($communication_id) {
            $this->queueMessage($user->id, "Email", $communication_id, NULL);
            $this->queueMessage($user->id, "SMS", $communication_id, NULL);
        }
    }

    public function addedToSquadAgent($user_id, $company_id){
        $user = \App\User::where('id', $user_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'AddedToSquadAgent')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $template->id);
        if ($communication_id) {
            $this->queueMessage($user->id, "Email", $communication_id, NULL);
            $this->queueMessage($user->id, "SMS", $communication_id, NULL);
        }
    }

    public function addedToSquadCustomerService($user_id, $company_id){
        $user = \App\User::where('id', $user_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'AddedToSquadCustomerService')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $template->id);
        if ($communication_id) {
            $this->queueMessage($user->id, "Email", $communication_id, NULL);
            $this->queueMessage($user->id, "SMS", $communication_id, NULL);
        }
    }

    public function addedToSquadSafetyCoordinator($user_id, $company_id){
        $user = \App\User::where('id', $user_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'AddedToSquadSafetyCoordinator')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $template->id);
        if ($communication_id) {
            $this->queueMessage($user->id, "Email", $communication_id, NULL);
            $this->queueMessage($user->id, "SMS", $communication_id, NULL);
        }
    }

    public function addedToSquadSupervisor($user_id, $company_id){
        $user = \App\User::where('id', $user_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'AddedToSquadSupervisor')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $template->id);
        if ($communication_id) {
            $this->queueMessage($user->id, "Email", $communication_id, NULL);
            $this->queueMessage($user->id, "SMS", $communication_id, NULL);
        }
    }

    public function addedToSquadHumanResources($user_id, $company_id){
        $user = \App\User::where('id', $user_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'AddedToSquadHumanResources')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $template->id);
        if ($communication_id) {
            $this->queueMessage($user->id, "Email", $communication_id, NULL);
            $this->queueMessage($user->id, "SMS", $communication_id, NULL);
        }
    }

    public function addedToSquadOther($user_id, $company_id){
        $user = \App\User::where('id', $user_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'AddedToSquadOther')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $template->id);
        if ($communication_id) {
            $this->queueMessage($user->id, "Email", $communication_id, NULL);
            $this->queueMessage($user->id, "SMS", $communication_id, NULL);
        }
    }

    public function addedToSystem($user_id, $company_id){
        /* DISABLED
        $user = \App\User::where('id', $user_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'AddedToSystem')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $template->id);
        if ($communication_id) {
            $this->queueMessage($user->id, "Email", $communication_id, NULL);
            $this->queueMessage($user->id, "SMS", $communication_id, NULL);
        }
        */
    }

    /* NOTIFY TEAM CLAIM HAS BEEN RESOLVED */
    public function claimResolved($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'ClaimResolved')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if($communication_id) {
            foreach ($squadMembers as $squadMember) {
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury->id);
                $this->queueMessage($squadMember->user_id, "SMS", $communication_id, $injury->id);
            }
        }
    }

    /* ALERT INJURED EMPLOYEE THEY HAVE AN UPCOMING APPOINTMENT (NOT CURRENTLY IN USE) */
    public function pendingAppointment($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'InjuredEmployeeAppointment')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            $this->queueMessage($injury->injured_employee_id, "Email", $communication_id, $injury->id);
            $this->queueMessage($injury->injured_employee_id, "SMS", $communication_id, $injury->id);
        }
    }

    /* INFORM SAFETY MANAGER AN EARLY INJURY HAS BEEN REPORTED */
    public function earlyInjury($injury_id){
        /* DISABLED
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadmembers = \App\SquadMember::where('squad_id', $injury->squad_id)->where(function($query){
            $query->where('position_id', 7)->orWhere('position_id', 5)->orWhere('position_id', 9);
        })->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'SafetyCoordinatorEarlyInjury')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            foreach ($squadmembers as $squadmember){
                $this->queueMessage($squadmember->user_id, "Email", $communication_id, $injury->id);
                //$this->queueMessage($squadmember->user_id, "SMS", $communication_id, $injury->id);
            }
        }
        */
    }

    /* ALERT TEAM THAT THE INJURED USER REPORTED NEGATIVE DIARY FEEDBACK */
    public function negativeDiaryFeedback($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'negativeDiaryFeedback')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            $squadmembers = \App\SquadMember::where('squad_id', $injury->squad_id)->where(function($query){
                $query->where('position_id', 1)->orWhere('position_id', 5)->orWhere('position_id', 7)->where('position_id', 8)->where('position_id', 9);
            })->groupBy('user_id')->get();
            foreach ($squadmembers as $squadmember) {
                $this->queueMessage($squadmember->user_id, "Email", $communication_id, $injury->id);
                $this->queueMessage($squadmember->user_id, "SMS", $communication_id, $injury->id);
            }
        }
    }

    /* ALERT TEAM THAT A HIGH IMPORTANCE TREE POST HAS BEEN MADE (NOT CURRENTLY IN USE) */
    public function highImportancePost($injury_id){
        /* DISABLED
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadmembers = \App\SquadMember::where('squad_id', $injury->squad_id)->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'HighImportanceUpdate')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            foreach ($squadmembers as $squadmember) {
                $this->queueMessage($squadmember->user_id, "Email", $communication_id, $injury->id);
                $this->queueMessage($squadmember->user_id, "SMS", $communication_id, $injury->id);
            }
        }
        */
    }

    /****** NEW FIRST AID INJURY COMMUNICATIONS ******/

    public function newFirstAidInjuryOwner($injury_id, $user_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'NewFirstAidInjuryOwner')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            $this->queueMessage($user_id, "Email", $communication_id, $injury->id);
            $this->queueMessage($user_id, "SMS", $communication_id, $injury->id);
        }
    }

    public function newFirstAidInjurySafetyCoordinator($injury_id, $user_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'NewFirstAidInjurySafetyCoordinator')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            $this->queueMessage($user_id, "Email", $communication_id, $injury->id);
            $this->queueMessage($user_id, "SMS", $communication_id, $injury->id);
        }
    }

    public function newFirstAidInjurySupervisor($injury_id, $user_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'NewFirstAidInjurySupervisor')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            $this->queueMessage($user_id, "Email", $communication_id, $injury->id);
            $this->queueMessage($user_id, "SMS", $communication_id, $injury->id);
        }
    }

    public function newFirstAidInjuryHumanResources($injury_id, $user_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'NewFirstAidInjuryHumanResources')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            $this->queueMessage($user_id, "Email", $communication_id, $injury->id);
            $this->queueMessage($user_id, "SMS", $communication_id, $injury->id);
        }
    }

    public function newFirstAidInjuryOther($injury_id, $user_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'NewFirstAidInjuryOther')->where('company_id', NULL)->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            $this->queueMessage($user_id, "Email", $communication_id, $injury->id);
            $this->queueMessage($user_id, "SMS", $communication_id, $injury->id);
        }
    }

    /* DELIVERS THE EMPLOYEE EXPECTATIONS DOCUMENT TO INJURED EMPLOYEE */
    public function employeeExpectationsDelivery($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'InjuredEmployeeExpectations')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            $this->queueMessage($injury->injured_employee_id, "Email", $communication_id, $injury->id);
        }
    }

    /* TELLS USER TO LOGIN (NUDGE) */
    public function pingAgent($user_id, $company_id){
        /*
        Log::debug('sending nudge');
        $template = \App\Communication_Template::where('template_identifier', 'PingAgent')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $template->id);
        if($communication_id) {
            Log::debug('communication_id');
            $this->queueMessage($user_id, "Email", $communication_id, NULL);
            $this->queueMessage($user_id, "SMS", $communication_id, NULL);
        }else{
            Log::debug('no communication id');
        }
        */
    }

    /* ALERT TEAM THAT INJURED EMPLOYEE'S STATUS HAS CHANGED */
    public function employeeStatusUpdated($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'EmployeeStatusUpdated')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            foreach ($squadMembers as $squadMember) {
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury->id);
            }
        }
    }

    /* ALERT TEAM THAT INJURY'S SEVERITY HAS CHANGED */
    public function severityUpdated($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'SeverityUpdated')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            foreach ($squadMembers as $squadMember){
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury->id);
                $this->queueMessage($squadMember->user_id, "SMS", $communication_id, $injury->id);
            }
        }
    }

    /** Prompts the user to update the final cost for a resolved claim **/
    public function getFinalCost($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->where(function($query){
            $query->where('position_id', 4)->orWhere('position_id', 5)->orWhere('position_id', 6);
        })->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'getFinalCost')->first();
        $information_template = \App\InformationType::where('name', 'Final Cost')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id){
            foreach ($squadMembers as $squadMember){
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury_id);
                //$this->repo->storeInformationRequest($information_template->name, NULL, $information_template->request_body, $squadMember->user_id, $information_template->id, $communication_id, $injury->id, $injury->company_id);
            }
            /*
            $reminder_tree_post = new TreePost();
            $reminder_tree_post->newZenfuciusReminderPost($injury->id, $squadMembers, "update the injury's Final Costs");
            */
        }
    }

    public function getRenewalDate($company_id){
        //$company = \App\Company::where('id', $company_id)->first();
        $squad_id_array = \App\Squad::where('company_id', $company_id)->pluck('id');
        $squad_members = \App\SquadMember::whereIn('squad_id', $squad_id_array)->where(function($query){
            $query->where('position_id', 1)->orWhere('position_id', 9)->orWhere('position_id', 5)->orWhere('position_id', 6);
        })->groupBy('user_id')->get();
        $request_template = \App\InformationType::where('name', "Renewal Date")->first();
        $communication_template = \App\Communication_Template::where('template_identifier', 'GetRenewalDate')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $communication_template->id);
        if($communication_id){
            foreach($squad_members as $squad_member){
                $this->queueMessage($squad_member->user_id, "Email", $communication_id, NULL);
                //$this->repo->storeInformationRequest($request_template->name, NULL, $request_template->request_body, $squad_member->user_id, $request_template->id, $communication_id, NULL, $company_id);
            }
        }
    }

    /****** CLAIM COSTS UPDATED COMMUNICATIONS (TEAM) ******/

    /** Alerts Team that the claim's values have been updated **/
    public function claimValuesUpdated($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)
            ->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'ClaimValuesUpdated')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if($communication_id) {
            foreach ($squadMembers as $squadMember) {
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury->id);
            }
        }
    }

    /** Alerts Team that the final cost of the claim has been updated **/
    public function finalCostUpdated($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'finalCostUpdated')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if($communication_id) {
            foreach ($squadMembers as $squadMember){
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury->id);
                $this->queueMessage($squadMember->user_id, "SMS", $communication_id, $injury->id);
            }
        }
    }

    public function indemnityCostUpdated($injury_id, $indemnity_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $indemnity_value = \App\IndemnityCosts::where('id', $indemnity_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)
            ->where('user_id', '!=', $indemnity_value->updater_id)
            ->groupBy('user_id')->get();

        $template = \App\Communication_Template::where('template_identifier', 'IndemnityCostUpdated')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            foreach ($squadMembers as $squadMember) {
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury->id);
                $this->queueMessage($squadMember->user_id, "SMS", $communication_id, $injury->id);
            }
        }
    }

    public function medicalCostUpdated($injury_id, $medical_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $medical_value = \App\MedicalCosts::where('id', $medical_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)
            ->where('user_id', '!=', $medical_value->updater_id)
            ->groupBy('user_id')->get();

        $template = \App\Communication_Template::where('template_identifier', 'MedicalCostUpdated')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            foreach ($squadMembers as $squadMember) {
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury->id);
                $this->queueMessage($squadMember->user_id, "SMS", $communication_id, $injury->id);
            }
        }
    }

    public function reserveCostUpdated($injury_id, $reserve_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $reserve_value = \App\ReserveCosts::where('id', $reserve_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)
            ->where('user_id', '!=', $reserve_value->updater_id)
            ->groupBy('user_id')->get();

        $template = \App\Communication_Template::where('template_identifier', 'ReserveCostUpdated')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            foreach ($squadMembers as $squadMember) {
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury->id);
                $this->queueMessage($squadMember->user_id, "SMS", $communication_id, $injury->id);
            }
        }
    }

    /* ALERT ZAGENT ZUSER THAT THEY'VE REPLACED ANOTHER ZAGENT ZUSER ON THEIR TEAMS */
    public function replacementZAgentZUser($user_id, $company_id){
        $template = \App\Communication_Template::where('template_identifier', 'replacementZAgentZUser')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $template->id);
        if($communication_id) {
            $this->queueMessage($user_id, "Email", $communication_id, NULL);
        }
    }

    /* ALERT ZAGENT ZUSER THAT THEY'VE BEEN ADDED TO A NEW CLIENT'S COMPANY */
    public function newClientZAgentZUser($user_id, $client_id){
        //client_id is id of their new client company
        $template = \App\Communication_Template::where('template_identifier', 'newClientZAgentZUser')->first();
        $communication_id = $this->newCommunication($client_id, NULL, $template->id);
        if($communication_id) {
            $this->queueMessage($user_id, "Email", $communication_id, NULL);
        }
    }

    /* Ask agent for adjuster info for new injury */
    public function getAdjusterInfo($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->where(function($query){
            $query->where('position_id', 6)->orWhere('position_id', 5)->orWhere('position_id', 9);
        })->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'getAdjusterInfo')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            foreach($squadMembers as $squadMember){
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury->id);
            }
            /*
            $reminder_tree_post = new TreePost();
            $reminder_tree_post->newZenfuciusReminderPost($injury->id, $squadMembers, "add the adjuster's info");
            */
        }
    }

    /* Alert team that adjuster info has been added */
    public function adjusterInfoAdded($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'adjusterInfoAdded')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if ($communication_id) {
            foreach ($squadMembers as $squadMember) {
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury->id);
            }
        }
    }

    /* Alert team that a new tree post has been added */
    public function newTreePost($injury_id, $post_id, $poster_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->where('user_id', '!=', $poster_id)->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'NewTreePost')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        $communication = \App\Communication::where('id', $communication_id)->first();
        $communication->extra_data = $post_id;
        $communication->save();
        if($communication_id){
            foreach ($squadMembers as $squadMember){
                //this communication must be enabled from the settings menu, so we will check for that
                //user will be null if the user has this
                //$user = \App\User::where('id', $squadMember->user_id)->where('send_treepost_communication', 1)->first();
                if($squadMember->user_id !== $poster_id){
                    $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury->id);
                    //$this->queueMessage($squadMember->user_id, "SMS", $communication_id, $injury->id);
                }
            }
        }
    }

    /* Send weekly Tree of Communications Snapshot to user */
    public function treePostSnapshot($user_id, $company_id){
        /* DISABLED
        Log::debug('treePostSnapshot');
        $template = \App\Communication_Template::where('template_identifier', 'TreePostSnapshot')->first();
        $communication_id = $this->newCommunication($company_id, NULL, $template->id);
        if($communication_id) {
            Log::debug('comm_id');
            $this->queueMessage($user_id, "Email", $communication_id, NULL);
        }
        */
    }

    /* Informs team that a claim has been paused for litigation */
    public function claimPaused($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->groupBy('user_id')->get();
        $template = \App\Communication_Template::where('template_identifier', 'ClaimPaused')->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if($communication_id){
            foreach ($squadMembers as $squadMember){
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury->id);
                //$this->queueMessage($squadMember->user_id, "SMS", $communication_id, $injury->id);
            }
        }
    }

    /* Weekly email reminding supervisor to communicate with injured employee */
    public function injuredEmployeeContact($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        //recipient should be the supervisor. If the team has no supervisor, default to HR instead.
        $recipients = \App\SquadMember::where('squad_id', $injury->squad_id)->where('position_id', 8)->get();
        if(count($recipients) === 0){
            $recipients = \App\SquadMember::where('squad_id', $injury->squad_id)->where('position_id', 9)->get();
        }
        if(count($recipients)){
            $template = \App\Communication_Template::where('template_identifier', 'InjuredEmployeeContact')->first();
            $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
            foreach($recipients as $recipient){
                if($communication_id){
                    $this->queueMessage($recipient->user_id, "Email", $communication_id, $injury->id);
                }
            }
        }
    }

    public function possibleWorkersCompInjury($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'PossibleWorkersCompInjury')->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->where(function($query){
            $query->where('position_id', 1)->orWhere('position_id', 9)->orWhere('position_id', 5);
        })->groupBy('user_id')->get();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if($communication_id) {
            foreach($squadMembers as $squadMember){
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury->id);
                $this->queueMessage($squadMember->user_id, "SMS", $communication_id, $injury->id);
            }
            /*
            $reminder_tree_post = new TreePost();
            $reminder_tree_post->newZenfuciusReminderPost($injury->id, $squadMembers, " ")
            */
        }
    }

    public function newAppointment($injury_id, $appointment_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'newAppointment')->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->where(function($query){
           $query->where('position_id', 1)->orWhere('position_id', 9)->orWhere('position_id', 4)
               ->orWhere('position_id', 8)->orWhere('position_id', 5)->orWhere('position_id', 6);
        })->groupBy('user_id')->get();

        $injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();

        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);

        $communication = \App\Communication::where('id', $communication_id)->first();
        $communication->extra_data = $appointment_id;
        $communication->save();

        if($communication_id) {
            foreach($squadMembers as $squadMember){
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury->id);
            }
            if($injured_employee->email !== NULL){
                $this->queueMessage($injured_employee->id, "Email", $communication_id, $injury->id);
            }
        }
    }

    public function userUnsubscribed($company_id, $unsubscribed_user_id){
        Log::debug('in unsubscribed email');
        //figure out who the owner for the company is, send it ot them
        $company = \App\Company::where('id', $company_id)->first();

        $owner_id = \App\Team::where('id', $company->team_id)->value('owner_id');

        if($owner_id !== $unsubscribed_user_id){
            $template = \App\Communication_Template::where('template_identifier', 'userDisabledCommunications')->first();
            //create the new communication, that function will return the new Communication records ID
            $comm_id = $this->newCommunication($company_id, NULL, $template->id);
            $comm = \App\Communication::where('id', $comm_id)->first();
            $comm->extra_data = $unsubscribed_user_id;
            $comm->save();
            $this->queueMessage($owner_id, "Email", $comm_id, NULL);

            //use that to add the unsubscribed_user_id to the extra_data field
        }


    }

    public function appointmentDeleted($injury_id, $appointment_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'appointmentDeleted')->first();
        $squadMembers = \App\SquadMember::where('squad_id', $injury->squad_id)->where(function($query){
            $query->where('position_id', 1)->orWhere('position_id', 9)->orWhere('position_id', 4)
                ->orWhere('position_id', 8)->orWhere('position_id', 5)->orWhere('position_id', 6);
        })->groupBy('user_id')->get();

        $injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();

        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);

        $communication = \App\Communication::where('id', $communication_id)->first();
        $communication->extra_data = $appointment_id;
        $communication->save();

        if($communication_id) {
            foreach($squadMembers as $squadMember){
                $this->queueMessage($squadMember->user_id, "Email", $communication_id, $injury->id);
            }
            if($injured_employee->email !== NULL){
                $this->queueMessage($injured_employee->id, "Email", $communication_id, $injury->id);
            }
        }
    }

    public function appointmentReminder($injury_id, $appointment_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'appointmentReminder')->first();
        $squad_member = \App\SquadMember::where('squad_id', $injury->squad_id)->where('position_id', 8)->first();
        if(is_null($squad_member)){
            $squad_member = \App\SquadMember::where('squad_id', $injury->squad_id)->where('position_id', 9)->first();
        }
        $injured_employee = \App\User::where('id', $injury->injured_employee_id)->first();

        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);

        $communication = \App\Communication::where('id', $communication_id)->first();
        $communication->extra_data = $appointment_id;
        $communication->save();

        if($communication_id) {
            if(!is_null($squad_member)){
                $this->queueMessage($squad_member->user_id, "Email", $communication_id, $injury->id);
            }
            if($injured_employee->email !== NULL){
                $this->queueMessage($injured_employee->id, "Email", $communication_id, $injury->id);
            }
        }
    }

    public function appointmentFollowup($injury_id, $appointment_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'appointmentFollowup')->first();
        $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->where(function($query){
            $query->where('position_id', 1)->orWhere('position_id', 9)->orWhere('position_id', 4)
                ->orWhere('position_id', 8)->orWhere('position_id', 5)->orWhere('position_id', 6);
        })->groupBy('user_id')->get();

        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);

        $communication = \App\Communication::where('id', $communication_id)->first();
        $communication->extra_data = $appointment_id;
        $communication->save();

        if($communication_id) {
            foreach($squad_members as $squad_member){
                $this->queueMessage($squad_member->user_id, "Email", $communication_id, $injury->id);
            }
        }
    }

    public function getAppointmentDetails($injury_id){
        $injury = \App\Injury::where('id', $injury_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'getAppointmentDetails')->first();
        $squad_members = \App\SquadMember::where('squad_id', $injury->squad_id)->where(function($query){
            $query->where('position_id', 1)->orWhere('position_id', 8)->orWhere('position_id', 9);
        })->groupBy('user_id')->get();

        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);

        if($communication_id) {
            foreach($squad_members as $squad_member){
                $this->queueMessage($squad_member->user_id, "Email", $communication_id, $injury->id);
            }
        }
    }

    public function newCustomTask($recipient_array, $action_id, $injury_id, $sender_id, $due_date){

        $injury = \App\Injury::where('id', $injury_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'NewCustomTask')->first();
        $action_template = \App\Action::where('id', $action_id)->first();
        $communication_id = $this->newCommunication($injury->company_id, $injury->id, $template->id);
        if($communication_id) {
            foreach($recipient_array as $recipient){
                $this->queueMessage($recipient, "Email", $communication_id, $injury->id);
                //$this->repo->storeActionRequest($action_template->name, NULL, $action_template->request_body, $recipient, $action_template->id, $communication_id, $injury->id, $injury->company_id, $due_date);
            }
        }
    }

    public function customTaskReminder($user_id, $company_id, $injury_id){
        $template_id = \App\Communication_Template::where('template_identifier', 'CustomTaskReminder')->value('id');
        $communication_id = $this->newCommunication($company_id, $injury_id, $template_id);
        if($communication_id){
            $this->queueMessage($user_id, "Email", $communication_id, $injury_id);
        }
    }

    /***** COMMUNICATIONS FOR DROPPED ZAGENT CLIENTS ******/

    public function droppedByAgent($company_id)
    {
        /**** WE WANT SOME MARKETING MATERIAL TO ATTACH TO THIS EMAIL ****/

        $company = \App\Company::where('id', $company_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'droppedByAgent')->first();
        $squads = \App\Squad::where('company_id', $company->id)->get();
        $communication_id = $this->newCommunication($company->id, NULL, $template->id);
        if ($communication_id) {
            $owner_id = \App\TeamUser::where('team_id', $company->team_id)->where('role', 'owner')->value('user_id');
            $sent_to_array = array();
            $this->queueMessage($owner_id, "Email", $communication_id, NULL);
            array_push($sent_to_array, $owner_id);
            foreach ($squads as $squad) {
                $squad_members = \App\SquadMember::where('squad_id', $squad->id)->where(function ($query) {
                    $query->where('position_id', 1)
                        ->orWhere('position_id', 9);
                })->get();
                foreach ($squad_members as $squad_member) {
                    $check = true;
                    foreach ($sent_to_array as $sent_to) {
                        if ($sent_to == $squad_member->user_id) {
                            $check = false;
                        }
                    }
                    if ($check == true) {
                        $this->queueMessage($squad_member->user_id, "Email", $communication_id, NULL);
                        array_push($sent_to_array, $squad_member->user_id);
                    }
                }
            }
        }
    }

    public function agentCancelled($company_id){
        /**** WE WANT SOME MARKETING MATERIAL TO ATTACH TO THIS EMAIL ****/
        $company = \App\Company::where('id', $company_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'AgentCancelled')->first();
        $squads = \App\Squad::where('company_id', $company->id)->get();
        $communication_id = $this->newCommunication($company->id, NULL, $template->id);
        if ($communication_id) {
            $owner_id = \App\TeamUser::where('team_id', $company->team_id)->where('role', 'owner')->value('user_id');
            $sent_to_array = array();
            $this->queueMessage($owner_id, "Email", $communication_id, NULL);
            array_push($sent_to_array, $owner_id);
            foreach ($squads as $squad) {
                $squad_members = \App\SquadMember::where('squad_id', $squad->id)->where(function ($query) {
                    $query->where('position_id', 1)
                        ->orWhere('position_id', 9);
                })->get();
                foreach ($squad_members as $squad_member) {
                    $check = true;
                    foreach ($sent_to_array as $sent_to) {
                        if ($sent_to == $squad_member->user_id) {
                            $check = false;
                        }
                    }
                    if ($check == true) {
                        $this->queueMessage($squad_member->user_id, "Email", $communication_id, NULL);
                        array_push($sent_to_array, $squad_member->user_id);
                    }
                }
            }
        }
    }

    public function subscriptionCancelled($company_id)
    {
        $company = \App\Company::where('id', $company_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'SubscriptionCancelled')->first();
        $squads = \App\Squad::where('company_id', $company->id)->get();
        $communication_id = $this->newCommunication($company->id, NULL, $template->id);
        if ($communication_id) {
            $owner_id = \App\TeamUser::where('team_id', $company->team_id)->where('role', 'owner')->value('user_id');
            $sent_to_array = array();
            $this->queueMessage($owner_id, "Email", $communication_id, NULL);
            array_push($sent_to_array, $owner_id);
            foreach ($squads as $squad) {
                $squad_members = \App\SquadMember::where('squad_id', $squad->id)->where(function ($query) {
                    $query->where('position_id', 1)
                        ->orWhere('position_id', 9);
                })->get();
                foreach ($squad_members as $squad_member) {
                    $check = true;
                    foreach ($sent_to_array as $sent_to) {
                        if ($sent_to == $squad_member->user_id) {
                            $check = false;
                        }
                    }
                    if ($check == true) {
                        $this->queueMessage($squad_member->user_id, "Email", $communication_id, NULL);
                        array_push($sent_to_array, $squad_member->user_id);
                    }
                }
            }
        }
    }

    public function gracePeriodWeek($company_id)
    {
        $company = \App\Company::where('id', $company_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'gracePeriodWeek')->first();
        $squads = \App\Squad::where('company_id', $company->id)->get();
        $communication_id = $this->newCommunication($company->id, NULL, $template->id);
        if ($communication_id) {
            $owner_id = \App\TeamUser::where('team_id', $company->team_id)->where('role', 'owner')->value('user_id');
            $sent_to_array = array();
            $this->queueMessage($owner_id, "Email", $communication_id, NULL);
            array_push($sent_to_array, $owner_id);
            foreach ($squads as $squad) {
                $squad_members = \App\SquadMember::where('squad_id', $squad->id)->where(function ($query) {
                    $query->where('position_id', 1)
                        ->orWhere('position_id', 9);
                })->get();
                foreach ($squad_members as $squad_member) {
                    $check = true;
                    foreach ($sent_to_array as $sent_to) {
                        if ($sent_to == $squad_member->user_id) {
                            $check = false;
                        }
                    }
                    if ($check == true) {
                        $this->queueMessage($squad_member->user_id, "Email", $communication_id, NULL);
                        array_push($sent_to_array, $squad_member->user_id);
                    }
                }
            }
        }
    }

    public function gracePeriodEnding($company_id)
    {
        $company = \App\Company::where('id', $company_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'gracePeriodEnding')->first();
        $squads = \App\Squad::where('company_id', $company->id)->get();
        $communication_id = $this->newCommunication($company->id, NULL, $template->id);
        if ($communication_id) {
            $owner_id = \App\TeamUser::where('team_id', $company->team_id)->where('role', 'owner')->value('user_id');
            $sent_to_array = array();
            $this->queueMessage($owner_id, "Email", $communication_id, NULL);
            array_push($sent_to_array, $owner_id);
            foreach ($squads as $squad) {
                $squad_members = \App\SquadMember::where('squad_id', $squad->id)->where(function ($query) {
                    $query->where('position_id', 1)
                        ->orWhere('position_id', 9);
                })->get();
                foreach ($squad_members as $squad_member) {
                    $check = true;
                    foreach ($sent_to_array as $sent_to) {
                        if ($sent_to == $squad_member->user_id) {
                            $check = false;
                        }
                    }
                    if ($check == true) {
                        $this->queueMessage($squad_member->user_id, "Email", $communication_id, NULL);
                        array_push($sent_to_array, $squad_member->user_id);
                    }
                }
            }
        }
    }

    public function gracePeriodEnded($company_id)
    {
        $company = \App\Company::where('id', $company_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'gracePeriodEnded')->first();
        $squads = \App\Squad::where('company_id', $company->id)->get();
        $communication_id = $this->newCommunication($company->id, NULL, $template->id);
        if ($communication_id) {
            $owner_id = \App\TeamUser::where('team_id', $company->team_id)->where('role', 'owner')->value('user_id');
            $sent_to_array = array();
            $this->queueMessage($owner_id, "Email", $communication_id, NULL);
            array_push($sent_to_array, $owner_id);
            foreach ($squads as $squad) {
                $squad_members = \App\SquadMember::where('squad_id', $squad->id)->where(function ($query) {
                    $query->where('position_id', 1)
                        ->orWhere('position_id', 9);
                })->get();
                foreach ($squad_members as $squad_member) {
                    $check = true;
                    foreach ($sent_to_array as $sent_to) {
                        if ($sent_to == $squad_member->user_id) {
                            $check = false;
                        }
                    }
                    if ($check == true) {
                        $this->queueMessage($squad_member->user_id, "Email", $communication_id, NULL);
                        array_push($sent_to_array, $squad_member->user_id);
                    }
                }
            }
        }
    }

    public function droppedByAgentPendingLicense($company_id)
    {

        /**** WE WANT SOME MARKETING MATERIAL TO ATTACH TO THIS EMAIL ****/

        $company = \App\Company::where('id', $company_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'pendingLicenseDropped')->first();
        $squads = \App\Squad::where('company_id', $company->id)->get();
        $communication_id = $this->newCommunication($company->id, NULL, $template->id);
        if ($communication_id) {
            $owner_id = \App\TeamUser::where('team_id', $company->team_id)->where('role', 'owner')->value('user_id');
            $sent_to_array = array();
            $this->queueMessage($owner_id, "Email", $communication_id, NULL);
            array_push($sent_to_array, $owner_id);
            foreach ($squads as $squad) {
                $squad_members = \App\SquadMember::where('squad_id', $squad->id)->where(function ($query) {
                    $query->where('position_id', 1)
                        ->orWhere('position_id', 9);
                })->get();
                foreach ($squad_members as $squad_member) {
                    $check = true;
                    foreach ($sent_to_array as $sent_to) {
                        if ($sent_to == $squad_member->user_id) {
                            $check = false;
                        }
                    }
                    if ($check == true) {
                        $this->queueMessage($squad_member->user_id, "Email", $communication_id, NULL);
                        array_push($sent_to_array, $squad_member->user_id);
                    }
                }
            }
        }
    }

    public function agentCancelledPendingLicense($company_id)
    {
        /**** WE WANT SOME MARKETING MATERIAL TO ATTACH TO THIS EMAIL ****/
        $company = \App\Company::where('id', $company_id)->first();
        $template = \App\Communication_Template::where('template_identifier', 'pendingLicenseCancelled')->first();
        $squads = \App\Squad::where('company_id', $company->id)->get();
        $communication_id = $this->newCommunication($company->id, NULL, $template->id);
        if ($communication_id) {
            $owner_id = \App\TeamUser::where('team_id', $company->team_id)->where('role', 'owner')->value('user_id');
            $sent_to_array = array();
            $this->queueMessage($owner_id, "Email", $communication_id, NULL);
            array_push($sent_to_array, $owner_id);
            foreach ($squads as $squad) {
                $squad_members = \App\SquadMember::where('squad_id', $squad->id)->where(function ($query) {
                    $query->where('position_id', 1)
                        ->orWhere('position_id', 9);
                })->get();
                foreach ($squad_members as $squad_member) {
                    $check = true;
                    foreach ($sent_to_array as $sent_to) {
                        if ($sent_to == $squad_member->user_id) {
                            $check = false;
                        }
                    }
                    if ($check == true) {
                        $this->queueMessage($squad_member->user_id, "Email", $communication_id, NULL);
                        array_push($sent_to_array, $squad_member->user_id);
                    }
                }
            }
        }
    }


    /***** COMMUNICATIONS FOR PAYMENT SYSTEM & PAYMENTS ******/

    public function paymentSuccess($comapny_id) {
        // removed for now
    }

    public function paymentMethodUpdate($comapny_id) {
// removed for now
    }

    
    public function paymentFailure($comapny_id) {
// removed for now
    }

    public function invoiceGenerated($comapny_id) {

// removed for now
    }
    
    public function upcomingInvoice($comapny_id) {
// removed for now
    }
    
    public function invoicePaymentFailed($comapny_id) {
// removed for now
    }

    public function invoicePaid($comapny_id) {
// removed for now
    }



}

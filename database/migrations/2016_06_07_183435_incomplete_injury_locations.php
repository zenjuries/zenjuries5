<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IncompleteInjuryLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incomplete_injury_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('injury_id')->unsigned();
            $table->foreign('injury_id')->references('id')->on('incomplete_injuries');
            $table->string('location');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('incomplete_injury_locations');
    }
}

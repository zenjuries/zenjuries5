<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicationPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('communication_positions', function(Blueprint $table){
        $table->increments('id');
        $table->integer('communication_template_id')->unsigned();
        $table->foreign('communication_template_id')->references('id')->on('communication_templates');
        $table->integer('positions_id')->unsigned();
        $table->foreign('positions_id')->references('id')->on('positions');
        $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('communication_positions');
    }
}

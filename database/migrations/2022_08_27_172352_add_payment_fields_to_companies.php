<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('companies', 'stripe_id'))
        {
            Schema::table('companies', function (Blueprint $table) {
                $table->string('stripe_id')->nullable();
            });
        }
        
        if (!Schema::hasColumn('companies', 'pm_type'))
        {
            Schema::table('companies', function (Blueprint $table) {
                $table->string('pm_type')->nullable();
            });
        }

        if (!Schema::hasColumn('companies', 'pm_last_four'))
        {
            Schema::table('companies', function (Blueprint $table) {
                $table->string('pm_last_four')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('companies', 'stripe_id'))
        {
            Schema::table('companies', function (Blueprint $table) {
                $table->dropColumn('stripe_id');
            });
        }

        if (Schema::hasColumn('companies', 'pm_type'))
        {
            Schema::table('companies', function (Blueprint $table) {
                $table->dropColumn('pm_type');
            });
        }

        if (Schema::hasColumn('companies', 'pm_last_four'))
        {
            Schema::table('companies', function (Blueprint $table) {
                $table->dropColumn('pm_last_four');
            });
        }
    }
};

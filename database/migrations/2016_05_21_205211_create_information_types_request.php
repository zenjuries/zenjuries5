<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationTypesRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('information_types', function (Blueprint $table) {
    $table->increments('id');
    $table->string('name');
    $table->string('description');
    $table->string('request_body');
    $table->boolean('custom');
    $table->timestamps();
});
    }

/**
 * Reverse the migrations.
 *
 * @return void
 */
public function down()
{
    Schema::drop('information_types');
}
}

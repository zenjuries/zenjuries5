<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCommunicationMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('communication_messages', function ($table){
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('injury_id')->unsigned()->nullable();
            $table->foreign('injury_id')->references('id')->on('injuries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('communication_messages', function ($table){
            $table->dropColumn('user_id');
            $table->dropColumn('injury_id');
        });
    }
}

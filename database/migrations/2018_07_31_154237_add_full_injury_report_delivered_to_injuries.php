<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFullInjuryReportDeliveredToInjuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('injuries', function($table){
           $table->boolean('full_injury_report_delivered')->default(0);
           $table->datetime('full_report_delivered_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('injuries', function($table){
            $table->dropColumn('full_injury_report_delivered');
            $table->dropColumn('full_report_delivered_at');
        });
    }
}

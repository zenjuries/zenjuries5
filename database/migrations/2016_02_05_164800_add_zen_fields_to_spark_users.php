<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddZenFieldsToSparkUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Add Zenjuries custom fields to Spark generated users table
        Schema::table('users', function ($table) {
            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->boolean('injured')->default(FALSE);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table){
            $table->dropForeign('users_company_id_foreign');
            $table->dropColumn('company_id');
            $table->dropColumn('injured');
        });
    }
}

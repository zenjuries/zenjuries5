<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInjuryTeamToSquads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('squads', function($table){
           $table->boolean('injury_squad')->default(0);
           $table->integer('original_squad_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('squads', function($table){
            $table->dropColumn('injury_squad');
            $table->dropColumn('original_squad_id');
        });
    }
}

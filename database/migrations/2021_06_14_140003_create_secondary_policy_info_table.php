<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondaryPolicyInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secondary_policy_info', function(Blueprint $table){
	       $table->increments('id');
	       $table->integer('company_id')->unsigned();
	       $table->foreign('company_id')->references('id')->on('companies'); 
	       $table->string('policy_number');
	       $table->datetime('renewal_date');
	       $table->string('carrier_name');
	       $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('secondary_policy_info');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDueDateAndCompletedAtToActions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('actions', function($table){
            $table->date('due_on')->nullable();
            $table->datetime('completed_at')->nullable();
            $table->datetime('last_reminder_sent')->nullable();
            $table->integer('reminders_sent')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('actions', function($table){
            $table->dropColumn('due_on');
            $table->dropColumn('completed_at');
            $table->dropColumn('last_reminder_sent');
            $table->dropColumn('reminders_sent');
        });
    }
}

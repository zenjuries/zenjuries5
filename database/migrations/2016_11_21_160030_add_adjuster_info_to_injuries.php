<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdjusterInfoToInjuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('injuries', function($table){
            $table->string('adjuster_name')->nullable();
            $table->string('adjuster_email')->nullable();
            $table->string('adjuster_phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('injuries', function($table){
            $table->dropColumn('adjuster_name');
            $table->dropColumn('adjuster_email');
            $table->dropColumn('adjuster_phone');
        });
    }
}

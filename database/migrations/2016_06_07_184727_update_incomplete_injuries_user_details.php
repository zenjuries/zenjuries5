<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateIncompleteInjuriesUserDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incomplete_injuries', function ($table){
            $table->string('injured_name')->nullable();
            $table->string('injured_email')->nullable();
            $table->string('injured_phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incomplete_injuries', function ($table){
            $table->dropColumn('injured_name');
            $table->dropColumn('injured_email');
            $table->dropColumn('injured_phone');
        });
    }
}

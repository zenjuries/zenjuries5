<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddZenCarrierIdToCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('companies', function($table){
            $table->integer('zen_carrier_id')->unsigned()->nullable();
            $table->foreign('zen_carrier_id')->references('id')->on('zen_carriers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('companies', function($table){
            $table->dropColumn('zen_carrier_id');
        });
    }
}

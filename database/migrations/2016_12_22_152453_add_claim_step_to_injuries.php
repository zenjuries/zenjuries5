<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClaimStepToInjuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('injuries', function($table){
           $table->integer('claim_step_id')->unsigned()->nullable();
            $table->foreign('claim_step_id')->references('id')->on('claim_steps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('injuries', function($table){
           $table->dropColumn('claim_step_id');
        });
    }
}

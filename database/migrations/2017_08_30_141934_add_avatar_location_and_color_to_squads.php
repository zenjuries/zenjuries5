<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvatarLocationAndColorToSquads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('squads', function($table){
            $table->string('avatar_location')->nullable();
            $table->string('background_color')->nullable();
            $table->string('avatar_color')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('squads', function($table){
            $table->dropColumn('avatar_location');
            $table->string('background_color')->nullable();
            $table->dropColumn('avatar_color');
        });
    }
}

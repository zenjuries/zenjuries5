<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClaimCloseDateReminderCountToInjuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('injuries', function ($table){
            $table->integer('claim_close_date_reminder_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('injuries', function ($table){
           $table->dropColumn('claim_close_date_reminder_count');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActionTakenAtToFirstReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('first_report_of_injuries', function ($table){
            $table->timestamp('action_taken_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('first_report_of_injuries', function ($table){
            $table->dropColumn('action_taken_at');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployeeStatusToInjuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('injuries', function ($table){
            $table->string('employee_status')->nullable();
            $table->timestamp('reopened_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('injuries', function ($table){
            $table->dropColumn('employee_status');
            $table->dropColumn('reopened_at');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_requests', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('recipient_id')->unsigned();
        $table->foreign('recipient_id')->references('id')->on('users');
        $table->integer('sender_id')->unsigned()->nullable();
        $table->foreign('sender_id')->references('id')->on('users');
        $table->integer('injury_id')->unsigned()->nullable();
        $table->foreign('injury_id')->references('id')->on('injuries');
        $table->integer('message_id')->unsigned();
        $table->foreign('message_id')->references('id')->on('messages');
        $table->integer('communication_id')->unsigned()->nullable();
        $table->foreign('communication_id')->references('id')->on('communications');
        $table->string('action_type');
        $table->timestamp('completed_at')->nullable();
        $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('action_requests');
    }
}

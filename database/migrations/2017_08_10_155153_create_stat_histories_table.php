<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stat_histories', function(Blueprint $table){
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('injury_length_days')->nullable();
            $table->integer('reporting_time_hours')->nullable();
            $table->integer('first_report_time_hours')->nullable();
            $table->integer('final_costs')->nullable();
            $table->integer('final_medical')->nullable();
            $table->integer('final_legal')->nullable();
            $table->integer('final_indemnity')->nullable();
            $table->integer('final_miscellaneous')->nullable();
            $table->integer('paid_costs')->nullable();
            $table->integer('paid_medical')->nullable();
            $table->integer('paid_legal')->nullable();
            $table->integer('paid_indemnity')->nullable();
            $table->integer('paid_miscellaneous')->nullable();
            $table->integer('reserve_costs')->nullable();
            $table->integer('reserve_medical')->nullable();
            $table->integer('reserve_legal')->nullable();
            $table->integer('reserve_indemnity')->nullable();
            $table->integer('reserve_miscellaneous')->nullable();
            $table->string('reserve_trend')->nullable();
            $table->integer('reserve_movement')->nullable();
            $table->integer('reserve_increase')->nullable();
            $table->integer('reserve_decrease')->nullable();
            $table->string('severity')->nullable();
            $table->string('type')->nullable();
            $table->string('location')->nullable();
            $table->integer('total_injury_count')->nullable();
            $table->integer('open_injury_count')->nullable();
            $table->integer('resolved_injury_count')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('stat_histories');
    }
}

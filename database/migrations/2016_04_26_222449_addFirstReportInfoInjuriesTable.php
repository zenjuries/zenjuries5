<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFirstReportInfoInjuriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('injuries', function ($table){
            $table->string('injured_employee_gender');
            $table->string('injury_date');
            $table->string('severity');
            $table->string('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('injuries', function ($table){
            $table->dropcolumn('injured_employee_gender');
            $table->dropcolumn('injury_date');
            $table->dropcolumn('severity');
            $table->dropcolumn('notes');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTeamiconNumberToSquads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('squads', function($table){
	        $table->integer('avatar_number')->nullable();
	        $table->string('avatar_icon_color')->nullable();
	        $table->integer('avatar_times_changed')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('squads', function($table){
	       $table->dropColumn('avatar_number');
	       $table->dropColumn('avatar_icon_color'); 
	       $table->dropColumn('avatar_times_changed');
        });
    }
}

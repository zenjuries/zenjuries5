<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColorsOnAgencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('agencies', function($table){
            $table->dropColumn('color1');
            $table->dropColumn('color2');
        });

        Schema::table('agencies', function($table){
            $table->string('agency_color')->default("fafafa")->nullable();
            $table->string('menu_color')->default("2982b2")->nullable();
            $table->string('button_color')->default("588bd2")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('agencies', function($table){
            $table->dropColumn('agency_color');
            $table->dropColumn('menu_color');
            $table->dropColumn('button_color');
        });

        Schema::table('agencies', function($table){
            $table->string('color1')->nullable();
            $table->string('color2')->nullable();
        });
    }
}

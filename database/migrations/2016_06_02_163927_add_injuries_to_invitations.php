<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInjuriesToInvitations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invitations', function ($table){
            $table->boolean('injured')->default(0);
            $table->integer('injury_id')->unsigned()->nullable();
            $table->foreign('injury_id')->references('id')->on('injuries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invitations', function ($table){
            $table->dropColumn('injured');
            $table->dropColumn('injury_id');
        });
    }
}

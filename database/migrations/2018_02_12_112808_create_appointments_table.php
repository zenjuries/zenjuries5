<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function(Blueprint $table){
           $table->increments('id');
           $table->integer('injury_id')->unsigned();
           $table->foreign('injury_id')->references('id')->on('injuries');
           $table->integer('location_id')->unsigned()->nullable();
           $table->foreign('location_id')->references('id')->on('injuries_care_locations');
           $table->string('description', 1000)->nullable();
           $table->dateTime('appointment_time');
           $table->boolean('followup_scheduled')->default(0);
           $table->integer('followup_appointment_id')->unsigned()->nullable();
           $table->foreign('followup_appointment_id')->references('id')->on('appointments');
           $table->string('summary', 1000)->nullable();
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('appointments');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEditedAtToTreePosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('tree_post', function($table){
           $table->boolean('edited')->default(0);
           $table->datetime('edited_at')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tree_post', function($table){
           $table->dropColumn('edited');
           $table->dropColumn('edited_at');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHiddenToInjuryTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('injuries', function($table){
            $table->integer('hidden')->default(0);
        });

        Schema::table('squads', function($table){
           $table->integer('hidden')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('injuries', function($table){
           $table->dropColumn('hidden');
        });

        Schema::table('squads', function($table){
           $table->dropColumn('hidden');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserThemeIdToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table){
	       $table->integer('user_theme_id')->unsigned()->nullable();
	       $table->foreign('user_theme_id')->references('id')->on('user_themes'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table){
	        $table->dropColumn('user_theme_id');
        });
    }
}

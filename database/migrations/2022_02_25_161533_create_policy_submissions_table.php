<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePolicySubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy_submissions', function (Blueprint $table) {
            $table->id();
            $table->integer('agency_id')->unsigned();
            $table->foreign('agency_id')->references('id')->on('agencies');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('premium');
            $table->integer('final_cost');
            $table->string('upload_location')->nullable();
            $table->boolean('client_pays')->default(0);
            $table->boolean('approved')->default(0);
            $table->boolean('activated')->default(0);
            $table->boolean('payment_processed')->default(0);
            $table->boolean('pays_with_check')->default(0);
            $table->integer('renewal_day');
            $table->integer('renewal_month');
            $table->date('upcoming_renewal_date');
            $table->boolean('upcoming_renewal')->default(0);
            $table->boolean('on_grace_period')->default(0);
            $table->boolean('company_paused_for_renewal')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policy_submissions');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicationTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communication_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->nullable()->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->string('name');
            $table->string('description');
            $table->string('type');
            $table->string('delivery_method');
            $table->string('email_subject');
            $table->string('sms_subject');
            $table->string('email_content');
            $table->string('sms_content');
            $table->string('retry_attempts');
            $table->integer('followup_template_id')->unsigned()->nullable();
            $table->foreign('followup_template_id')->references('id')->on('communication_templates');
            $table->dateTime('followup_interval')->nullable();
            $table->integer('followup_amount')->nullable();
            $table->boolean('is_active');
            $table->string('created_by');
            $table->timestamps();
            $table->string('last_modified_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('communication_templates');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeStatusHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('employee_status_history', function(Blueprint $table){
            $table->increments('id');
            $table->integer('injury_id')->unsigned();
            $table->foreign('injury_id')->references('id')->on('injuries');
            $table->string('status');
            $table->date('status_changed_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee_status_history');
    }
}

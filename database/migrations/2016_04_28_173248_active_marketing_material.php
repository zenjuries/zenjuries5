<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActiveMarketingMaterial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_marketing_material', function (Blueprint $table) {
            $table->increments('id');
            $table->string('body', 1000);
            $table->timestamps();
        });

        Schema::table('companies', function ($table){
            $table->string('active_marketing_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('active_marketing_material');

        Schema::table('companies', function ($table){
            $table->dropColumn('active_marketing_number');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTutorialsDoneToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function($table) {
            $table->integer('zenjuries_tutorials_done')->default(0);
            $table->integer('zenjuries_tutorials_done2')->default(0);
            $table->integer('zenjuries_tutorials_done3')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function($table) {
            $table->dropColumn('zenjuries_tutorials_done');
            $table->dropColumn('zenjuries_tutorials_done2');
            $table->dropColumn('zenjuries_tutorials_done3');
        });
    }
}

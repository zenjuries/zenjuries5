<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTutorialsToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table){
            $table->boolean('zenboard_tutorial')->default(0);
            $table->boolean('squad_tutorial')->default(0);
            $table->boolean('injury_tutorial')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table){
            $table->dropColumn('zenboard_tutorial');
            $table->dropColumn('squad_tutorial');
            $table->dropColumn('injury_tutorial');
        });
    }
}

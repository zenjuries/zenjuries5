<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinalCostBreakdownToInjuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('injuries', function($table){
            $table->string('final_medical_cost')->nullable();
            $table->string('final_indemnity_cost')->nullable();
            $table->string('final_legal_cost')->nullable();
            $table->string('final_misc_cost')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('injuries', function($table){
            $table->dropColumn('final_medical_cost');
            $table->dropColumn('final_indemnity_cost');
            $table->dropColumn('final_legal_cost');
            $table->dropColumn('final_misc_cost');
        });
    }
}

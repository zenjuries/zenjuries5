<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescAndImageLocationToIncompleteInjuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('incomplete_injuries', function($table){
           $table->string('desc', 1000)->nullable();
           $table->string('image_location')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('incomplete_injuries', function($table){
           $table->dropColumn('desc');
            $table->dropColumn('image_location');
        });
    }
}

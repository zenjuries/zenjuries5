<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddValuesUpdatedAtInjuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('injuries', function ($table){
            $table->timestamp('values_updated_at')->nullable();
            $table->integer('reminders_since_last_update')->nullable();
            $table->timestamp('last_reminders_sent_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('injuries', function ($table){
            $table->dropColumn('values_updated_at');
            $table->dropColumn('reminders_since_last_update');
            $table->dropColumn('last_reminders_sent_at')->nullable();
        });
    }
}

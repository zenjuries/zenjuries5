<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGetsWeeklySummaryToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table){
           $table->boolean('gets_weekly_summary')->default(1);
           $table->timestamp('weekly_summary_sent_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table){
           $table->dropColumn('gets_weekly_summary');
           $table->dropColumn('weekly_summary_sent_at');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiaryPostIdToTreePosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tree_post', function($table){
           $table->integer('diary_post_id')->unsigned()->nullable();
           $table->foreign('diary_post_id')->references('id')->on('diary_posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tree_post', function($table){
           $table->dropColumn('diary_post_id');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPossibleWorkCompRemindersToInjuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('injuries', function($table){
           $table->integer('possible_work_comp_reminder_count')->default(0);
            $table->timestamp('possible_work_comp_reminder_sent_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('injuries', function($table){
           $table->dropColumn('possible_work_comp_reminder_count');
            $table->dropColumn('possible_work_comp_reminder_sent_at');
        });
    }
}

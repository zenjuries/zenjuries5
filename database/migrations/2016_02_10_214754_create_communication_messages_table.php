<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicationMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('communication_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('communication_id')->unsigned();
            $table->foreign('communication_id')->references('id')->on('communications');
            $table->string('content');
            $table->string('delivery_method');
            //email or phone number
            $table->string('deliver_to_address');
            $table->dateTime('date_created');
            $table->dateTime('date_to_send');
            $table->dateTime('date_sent')->nullable();
            $table->dateTime('date_to_resend')->nullable();
            $table->string('triggered_by');
            $table->string('status');
            $table->string('error_code')->nullable();
            $table->string('error_message')->nullable();
            $table->integer('number_of_attempts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('communication_messages');
    }
}

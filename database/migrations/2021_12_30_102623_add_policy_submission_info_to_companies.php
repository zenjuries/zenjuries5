<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPolicySubmissionInfoToCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function($table){
	       $table->boolean('client_pays')->default(0);
	       $table->integer('policy_submission_id')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function($table){
	       $table->dropColumn('client_pays');
	       $table->dropColumn('policy_submission_id'); 
        });
    }
}

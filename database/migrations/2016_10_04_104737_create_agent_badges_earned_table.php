<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentBadgesEarnedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_badges_earned', function (Blueprint $table){
            $table->increments('id');
            $table->integer('agent_zuser_id')->unsigned();
            $table->foreign('agent_zuser_id')->references('id')->on('users');
            $table->string('badge_key'); // 'zen_logins', 'team_logins', 'company_logo', etc.
            $table->string('level'); // 'special', 'bronze', 'silver', 'gold', 'gold0', 'gold1', 'gold2', etc.
            $table->datetime('date_earned'); // Stamped with Now() when entry created. Extra timestamp here just in case you would want to move it up.
            $table->datetime('date_claimed')->default('1900-01-01 00:00:00'); // Date is default value until claimed.
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('agent_badges_earned');
    }
}

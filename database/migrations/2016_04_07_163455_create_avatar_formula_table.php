<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvatarFormulaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avatars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            /*Body*/
            $table->integer('body_index')->nullable();
            $table->string('face_hex')->nullable();
            /*Eyes*/
            $table->integer('eye_index')->nullable();
            $table->string('iris_hex')->nullable();
            $table->string('eyelid_hex')->nullable();
            $table->string('eyeliner_hex')->nullable();
            /*Eyebrows*/
            $table->integer('eyebrow_index')->nullable();
            $table->string('eyebrow_hex')->nullable();
            /*Mouth*/
            $table->integer('mouth_index')->nullable();
            $table->string('lip_hex')->nullable();
            /*Nose*/
            $table->integer('nose_index')->nullable();
            /*Hair*/
            $table->integer('hairFront_index')->nullable();
            $table->integer('hairBack_index')->nullable();
            $table->string('hair_hex')->nullable();
            /*Beard*/
            $table->integer('beard_index')->nullable();
            /*Shirt*/
            $table->integer('shirt_index')->nullable();
            $table->string('shirt_hex')->nullable();
            $table->string('shirtColor1_hex')->nullable();
            $table->string('shirtColor2_hex')->nullable();
            $table->string('tshirt_hex')->nullable();
            $table->string('overShirt1_hex')->nullable();
            $table->string('overShirt2_hex')->nullable();
            $table->string('neckTie1_hex')->nullable();
            $table->string('neckTie2_hex')->nullable();
            $table->string('collar_hex')->nullable();
            $table->string('buttonsBase_hex')->nullable();
            $table->string('buttonsHighlight_hex')->nullable();
            /*Extras*/
            $table->integer('extra_index')->nullable();
            $table->string('color1_hex')->nullable();
            $table->string('color2_hex')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('avatars');
    }
}

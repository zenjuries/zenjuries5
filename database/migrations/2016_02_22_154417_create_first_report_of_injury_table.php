<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFirstReportOfInjuryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('first_report_of_injuries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('injury_id')->unsigned();
            $table->foreign('injury_id')->references('id')->on('injuries');
            $table->integer('squad_id')->unsigned();
            $table->foreign('squad_id')->references('id')->on('squads');
            $table->boolean('action_taken')->default(0);
            $table->integer('number_of_reminders_sent')->default(0);
            $table->timestamps();
            $table->dateTime('last_message_sent_at')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('first_report_of_injuries');
    }
}

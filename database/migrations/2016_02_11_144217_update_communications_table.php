<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCommunicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('communications', function ($table){
            $table->integer('followup_id')->nullable()->unsigned();
            $table->foreign('followup_id')->references('id')->on('communication_messages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
       /* Schema::table('communications', function ($table){
           $table->dropColumn('followup_id');
        });*/
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddZagentIdToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //link the zagent with the company they belong to
        Schema::table('users', function($table){
           $table->integer('zagent_id')->unsigned()->nullable();
            $table->foreign('zagent_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function($table){
           $table->dropColumn('zagent_id');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRequestBodies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('information_types', function ($table){
            $table->string('request_body', 1000)->change();
        });

        Schema::table('actions', function ($table){
            $table->string('request_body', 1000)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('information_types', function ($table){
            $table->string('request_body')->change();
        });

        Schema::table('actions', function ($table){
            $table->string('request_body')->change();
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastContactEmployeeReminderSentAtToInjuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(Schema::hasColumn("injuries", "employee_contact_reminder_sent_at") === false){
            Schema::table('injuries', function($table){
                $table->timestamp('employee_contact_reminder_sent_at')->nullable()->default(NULL);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('injuries', function($table){
            $table->dropColumn('employee_contact_reminder_sent_at');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdjusterReminderInfoToInjuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('injuries', function($table){
            $table->integer('adjuster_info_reminders_count')->default(0);
            $table->timestamp('adjuster_info_reminder_sent_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('injuries', function($table){
           $table->dropColumn('adjuster_info_reminders_count');
            $table->dropColumn('adjuster_info_reminder_sent_at');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInjuriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('injuries', function ($table){
            $table->string('type');
            $table->boolean('resolved')->default(false);
            $table->integer('squad_id')->unsigned();
            $table->foreign('squad_id')->references('id')->on('squads');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('injuries', function ($table){
            $table->dropColumn('type');
            $table->dropColumn('resolved');

            $table->dropForeign('injuries_squad_id_foreign');
            $table->dropColumn('squad_id');
        });
    }
}

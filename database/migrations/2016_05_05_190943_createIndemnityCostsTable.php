<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndemnityCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indemnity_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('injury_id')->unsigned();
            $table->foreign('injury_id')->references('id')->on('injuries');
            $table->integer('updater_id')->unsigned();
            $table->foreign('updater_id')->references('id')->on('users');
            $table->string('cost');
            $table->boolean('current');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('indemnity_costs');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZcareLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('zcare_locations', function(Blueprint $table){
           //$table->integer('id')->unsigned();
           $table->increments('id')->unsigned();
           $table->string('name');
           $table->string('phone')->nullable();
           $table->string('full_address');
           $table->string('custom_message')->nullable();
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('zcare_locations');
    }
}

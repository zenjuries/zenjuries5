<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPageColorUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table){
            $table->string('background_color')->nullable();
            $table->string('background_choice')->nullable();
            $table->integer('background_image_id')->unsigned()->nullable();
            $table->foreign('background_image_id')->references('id')->on('backgrounds');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table){
            $table->drop('background_color');
            $table->drop('background_choice');
            $table->drop('background_image_id');
        });
    }
}

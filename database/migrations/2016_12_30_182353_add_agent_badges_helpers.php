<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAgentBadgesHelpers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('companies', function($table) {
            $table->timestamp('policy_updated_at')->nullable();
        });

        Schema::table('injuries', function($table) {
            $table->integer('satisfaction_score');
        });

        // for badge types 'injury_adjuster', 'injury_costs', 'injury_finalcosts'
        Schema::table('tree_post', function($table) {
            $table->string('task_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('companies', function($table){
            $table->dropColumn('policy_updated_at');
        });

        Schema::table('injuries', function($table){
            $table->dropColumn('satisfaction_score');
        });

        Schema::table('tree_post', function($table){
            $table->dropColumn('task_type');
        });
    }
}

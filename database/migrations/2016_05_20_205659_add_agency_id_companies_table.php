<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAgencyIdCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function ($table){
            $table->integer('agency_id')->unsigned()->nullable();
            $table->foreign('agency_id')->references('id')->on('agencies');
        });
    }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
    {

        Schema::table('companies', function($table){
            $table->dropColumn('agency_id');
        });
    }
}

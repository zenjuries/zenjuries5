<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CorrectClaimCostDataTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medical_costs', function($table){
           $table->integer('cost')->change();
        });

        Schema::table('indemnity_costs', function($table){
           $table->integer('cost')->change();
        });

        Schema::table('legal_costs', function($table){
           $table->integer('cost')->change();
        });

        Schema::table('miscellaneous_costs', function($table){
           $table->integer('cost')->change();
        });

        Schema::table('reserve_medical_costs', function($table){
            $table->integer('cost')->change();
        });

        Schema::table('reserve_indemnity_costs', function($table){
           $table->integer('cost')->change();
        });

        Schema::table('reserve_legal_costs', function($table){
           $table->integer('cost')->change();
        });

        Schema::table('reserve_miscellaneous_costs', function($table){
           $table->integer('cost')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medical_costs', function($table){
           $table->string('cost')->change();
        });

        Schema::table('indemnity_costs', function($table){
           $table->string('cost')->change();
        });

        Schema::table('legal_costs', function($table){
           $table->string('cost')->change();
        });

        Schema::table('miscellaneous_costs', function($table){
           $table->string('cost')->change();
        });

        Schema::table('reserve_medical_costs', function($table){
           $table->string('cost')->change();
        });

        Schema::table('reserve_indemnity_costs', function($table){
           $table->string('cost')->change();
        });

        Schema::table('reserve_legal_costs', function($table){
           $table->string('cost')->change();
        });

        Schema::table('reserve_miscellaneous_costs', function($table){
           $table->string('cost')->change();
        });
    }
}

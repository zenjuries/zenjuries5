<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCommunicationTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('communication_templates', function ($table){
            $table->integer('trigger_id')->nullable()->unsigned();
            $table->foreign('trigger_id')->references('id')->on('triggers');
            $table->string('template_identifier');
            $table->boolean('editable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('communication_templates', function ($table){
        $table->dropColumn('trigger_id');
        $table->dropColumn('template_identifier');
    });
    }
}

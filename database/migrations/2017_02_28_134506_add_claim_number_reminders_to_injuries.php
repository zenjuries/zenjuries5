<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClaimNumberRemindersToInjuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('injuries', function($table){
            $table->integer('claim_number_reminders')->default(0);
            $table->dateTime('claim_number_reminder_sent_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('injuries', function($table){
            $table->dropColumn('claim_number_reminders');
            $table->dropColumn('claim_number_reminder_sent_at');
        });
    }
}

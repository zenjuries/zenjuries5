<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHoursToCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('companies', function ($table){
            $table->integer('start_hour')->default(8);
            $table->integer('end_hour')->default(17);
            $table->date('renewal_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('companies', function ($table){
            $table->dropColumn('start_hour');
            $table->dropColumn('end_hour');
        });
    }
}

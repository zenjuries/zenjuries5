<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentBadgeTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('agent_badge_types', function (Blueprint $table) {

            /* old

            $table->increments('id');
            $table->string('badge_key');
            $table->string('badge_name');
            $table->string('fancy_title');
            $table->integer('black_threshold');
            $table->integer('bronze_threshold');
            $table->integer('silver_threshold');
            $table->integer('gold_threshold');
            $table->integer('gold_plus_threshold');
            $table->string('icon_base');
            $table->string('primary_aspect_base');
            $table->string('secondary_aspect_base');
            $table->string('description');
            $table->string('blurb');
            */

            $table->increments('id');
            $table->string('badge_key');
            $table->string('badge_name');
            $table->string('cute_title');
            $table->string('icon_base');
            $table->integer('black_threshold');
            $table->integer('bronze_threshold');
            $table->integer('silver_threshold');
            $table->integer('gold_threshold');
            $table->integer('gold_plus_threshold');
            $table->string('short_description');
            $table->string('congratulations');
            $table->string('accomplishment');
            $table->string('remaining');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('agent_badge_types');
    }
}

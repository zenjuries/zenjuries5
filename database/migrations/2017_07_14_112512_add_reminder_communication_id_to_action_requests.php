<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReminderCommunicationIdToActionRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('action_requests', function($table){
           $table->integer('reminder_communication_id')->unsigned()->nullable();
            $table->foreign('reminder_communication_id')->references('id')->on('communications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('action_requests', function($table){
           $table->dropColumn('reminder_communication_id');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInjuredEmployeeIdToInjuriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('injuries', function($table){
            $table->integer('injured_employee_id')->unsigned();
            $table->foreign('injured_employee_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('injuries', function ($table){
            $table->dropColumn('injured_employee_id');
        });
    }
}

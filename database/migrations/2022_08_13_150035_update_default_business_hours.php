<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function ($table) {
            $table->integer('start_hour')->default(12)->change();
        });
        Schema::table('companies', function ($table) {
            $table->integer('end_hour')->default(12)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function ($table) {
            $table->integer('start_hour')->default(8)->change();
        });
        Schema::table('companies', function ($table) {
            $table->integer('end_hour')->default(17)->change();
        });
    }
};

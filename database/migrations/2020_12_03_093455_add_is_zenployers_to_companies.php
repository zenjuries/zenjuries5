<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsZenployersToCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function($table){
           $table->boolean('is_zenployers_admin')->default(0);
           $table->boolean('is_zenployers_company')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function($table){
            $table->dropColumn('is_zenployers_admin');
           $table->dropColumn('is_zenployers_company');
        });
    }
}

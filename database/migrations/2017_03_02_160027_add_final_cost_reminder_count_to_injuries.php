<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinalCostReminderCountToInjuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('injuries', function($table){
            $table->integer('final_cost_reminders')->default(0);
            $table->timestamp('final_cost_reminder_sent_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('injuries', function($table){
            $table->dropColumn('final_cost_reminders');
            $table->dropColumn('final_cost_reminder_sent_at');
        });
    }
}

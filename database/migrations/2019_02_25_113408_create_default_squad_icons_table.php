<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultSquadIconsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_squad_icons', function($table){
           $table->increments('id')->unsigned();
           $table->string('avatar_location');
           $table->string('background_color');
           $table->string('avatar_color');
           $table->string('avatar_number');
           $table->string('avatar_icon_color');
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('default_squad_icons');
    }
}

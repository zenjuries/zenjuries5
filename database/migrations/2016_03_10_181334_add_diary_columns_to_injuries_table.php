<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiaryColumnsToInjuriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('injuries', function ($table){
            $table->dateTime('last_diary_post')->nullable();
            $table->dateTime('last_diary_reminder')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('injuries', function ($table){
            $table->dropColumn('last_diary_post');
            $table->dropColumn('last_diary_reminder');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreePostHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tree_post_history', function(Blueprint $table){
           $table->increments('id');
           $table->integer('tree_post_id')->unsigned();
           $table->foreign('tree_post_id')->references('id')->on('tree_post');
           $table->string('body', 1000);
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropTable('tree_post_history');
    }
}

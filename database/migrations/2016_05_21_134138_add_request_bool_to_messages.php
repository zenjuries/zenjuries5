<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRequestBoolToMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function ($table){
            $table->boolean('request')->default(0);
            $table->string('request_type')->nullable();
        });

        Schema::table('threads', function ($table){
            $table->boolean('request')->default(0);
            $table->string('request_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function($table){
            $table->dropColumn('request');
            $table->dropColumn('request_type');
        });

        Schema::table('threads', function($table){
            $table->dropColumn('request');
            $table->dropColumn('request_type');
        });
    }
}

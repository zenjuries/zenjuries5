<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInformationIdToInformationRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('information_requests', function ($table){
            $table->integer('information_id')->unsigned();
            $table->foreign('information_id')->references('id')->on('information_types');
        });

        Schema::table('information_requests', function ($table){
            $table->dropColumn('value_type');
        });
    }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
    {
        Schema::table('information_requests', function ($table){
            $table->dropColumn('information_id');
        });

        Schema::table('information_requests', function ($table){
            $table->string('value_type');
        });
    }
}

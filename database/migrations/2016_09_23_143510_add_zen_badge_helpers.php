<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddZenBadgeHelpers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function($table){
            $table->integer('zenjuries_logins')->default(0);
            $table->integer('zenjuries_avatar_changed')->default(0);
            $table->integer('zenjuries_photo_changed')->default(0);
            $table->integer('zenjuries_chatcolor_changed')->default(0);
            $table->integer('zenjuries_background_changed')->default(0);
            $table->integer('zenjuries_videos_watched')->default(0);
            $table->integer('zenjuries_videos_watched2')->default(0);
            $table->integer('zenjuries_videos_watched3')->default(0);
            $table->integer('zenjuries_userdocs_downloaded')->default(0);
            $table->integer('zenjuries_userdocs_uploaded')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function($table){
            $table->dropColumn('zenjuries_logins');
            $table->dropColumn('zenjuries_avatar_changed');
            $table->dropColumn('zenjuries_photo_changed');
            $table->dropColumn('zenjuries_chatcolor_changed');
            $table->dropColumn('zenjuries_background_changed');
            $table->dropColumn('zenjuries_videos_watched');
            $table->dropColumn('zenjuries_videos_watched2');
            $table->dropColumn('zenjuries_videos_watched3');
            $table->dropColumn('zenjuries_userdocs_downloaded');
            $table->dropColumn('zenjuries_userdocs_uploaded');
        });
    }
}

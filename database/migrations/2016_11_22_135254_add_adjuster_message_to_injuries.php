<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdjusterMessageToInjuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('injuries', function($table){
            $table->boolean('adjuster_reminder_sent')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('injuries', function($table){
           $table->dropColumn('adjuster_reminder_sent');
        });
    }
}

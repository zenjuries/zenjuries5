<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotePostIdToTreePosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tree_post', function($table){
            $table->integer('note_post_id')->unsigned()->nullable();
            $table->foreign('note_post_id')->references('id')->on('note_posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tree_post', function($table){
           $table->dropColumn('note_post_id');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGracePeriodToTeams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('teams', function($table){
           $table->boolean('on_grace_period')->default(0);
            $table->date('grace_period_ends')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('teams', function($table){
            $table->dropColumn('on_grace_period');
            $table->dropColumn('grace_period_ends');
        });
    }
}

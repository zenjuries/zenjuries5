<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContactInfoToCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function($table){
            $table->string('company_phone')->nullable();
            $table->string('mailing_address')->nullable();
            $table->string('mailing_address_line_2')->nullable();
            $table->string('mailing_city')->nullable();
            $table->string('mailing_state')->nullable();
            $table->string('mailing_zip')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function($table){
            $table->dropColumn('company_phone');
            $table->dropColumn('mailing_address');
            $table->dropColumn('mailing_address_line_2');
            $table->dropColumn('mailing_city');
            $table->dropColumn('mailing_state');
            $table->dropColumn('mailing_zip');
        });
    }
}

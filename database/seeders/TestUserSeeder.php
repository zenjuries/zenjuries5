<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class TestUserSeeder extends Seeder
{
    /**
     * Create a test user
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Testy Tester',
            'email' => 'testytester@example.com',
            'password' => Hash::make('P@ssword'),
        ]);
    }
}

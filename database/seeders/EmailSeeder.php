<?php

namespace Database\Seeders;

use App\Email;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\EmailTemplate;


class EmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $added_to_injury_email = new EmailTemplate();
        $added_to_injury_email->createTemplateRecord('AddedToInjuryEmail', 'added_to_injury', 1);

        $claim_paused_email = new EmailTemplate();
        $claim_paused_email->createTemplateRecord('ClaimPausedEmail', 'claim_paused', 1);

        $first_report_email = new EmailTemplate();
        $first_report_email->createTemplateRecord('FirstReportEmail', 'first_report', 1);

        $first_report_reminder_email = new EmailTemplate();
        $first_report_reminder_email->createTemplateRecord('FirstReportReminderEmail', 'first_report_reminder', 1);

        $negative_diary_feedback_email = new EmailTemplate();
        $negative_diary_feedback_email->createTemplateRecord('NegativeDiaryFeedbackEmail', 'negative_diary_feedback', 1);

        $new_injury_email = new EmailTemplate();
        $new_injury_email->createTemplateRecord('NewInjuryEmail', 'new_injury', 1);

        $weekly_update_email = new EmailTemplate();
        $weekly_update_email->createTemplateRecord('WeeklyUpdateEmail', 'weekly_update', 1);

    }
}

<?php

namespace Database\Seeders;

use App\Position;
use Illuminate\Database\Seeder;

class PositionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $position = new Position();
        $position->name = "Decision Maker";
        $position->description = "Decision Maker is a Required Role. This should be someone in your company, such as a manager, who will oversee the injury process. The Decision Maker is in charge of everything within the Zenjuries app, especially managing teams. The Decision Maker should closely monitor injuries for updates and keep in contact with all team members.";
        $position->save();

        $position = new Position();
        $position->name = "Human Resources";
        $position->description = "Human Resources is a Required Role. Someone in your company who handles the majority of your paperwork or is part of your Human Resources department is a good fit for this role. They will be assigned to most injury tasks, such as delivering the Injury Alert. They ensure that each step of the injury is handled promptly and that all tasks get completed.";
        $position->save();

        $position = new Position();
        $position->name = "Claims";
        $position->description = "The Claims role is an in-house Claims Rep who manages all claims for your company or agency. Adjusters for individual injuries assigned by your insurance carrier will be assigned from that injury's Tree of Communications. Their job is to provide vital claim information from the insurance company to the rest of the team through the Tree of Communications as quickly as possible.";
        $position->save();

        $position = new Position();
        $position->name = "Agent";
        $position->description = "The Agent is your Insurance Agent. They will provide you with vital claim information from your insurance carrier and help ensure that all team members are following Zenjuries' best practices.";
        $position->save();

        $position = new Position();
        $position->name = "Service Agent";
        $position->description = "The Service Agent (or Customer Services Agent) is someone at your Insurance Agency who handles your policy. They will ensure that your company and their agency stay in contact through the Tree of Communications. They will also assist the Agent in providing claim information.";
        $position->save();

        $position = new Position();
        $position->name = "Safety Manager";
        $position->description = "The Safety Manager is someone at your company who is in charge of work-place safety. They should review the details of injuries and see how your company can prevent future injuries of that nature.";
        $position->save();

        $position = new Position();
        $position->name = "Employee's Supervisor";
        $position->description = "The Supervisor is someone at your company who works directly with the Injured Employee. They will be critical in maintaining communications with the Injured Employee while they're out of work. They should update the team on the Employee's status using the Tree of Communications.";
        $position->save();

        $position = new Position();
        $position->name = "Physical Therapist";
        $position->description = "This is your employee's Physical Therapist. They can keep the team updated on your Employee's healing progress through the Tree of Communications. This role is not vital for the claim process in Zenjuries.";
        $position->save();

        $position = new Position();
        $position->name = "Employee's Doctor";
        $position->description = "This is your Employee's Doctor. They can keep the team updated on your Employee's healing progress through the Tree of Communications. This role is not vital for the claim process in Zenjuries.";
        $position->save();

        $position = new Position();
        $position->name = "Other";
        $position->description = "This position is perfect for anybody else you want on the team but don't have a specific role for. They can monitor a claim and provide support to the rest of the team.";
        $position->save();
    }
}

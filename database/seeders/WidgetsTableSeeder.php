<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Widget;

class WidgetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if(is_null(Widget::where('widget_name', 'Teams Widget')->first())){
            $widget = new Widget();
            $widget->widget_name = "Teams Widget";
            $widget->type = "page";
            $widget->description = "Widget for the Teams page";
            $widget->save();
        }

        if(is_null(Widget::where('widget_name', 'Injury List Widget')->first())){
            $widget = new Widget();
            $widget->widget_name = "Injury List Widget";
            $widget->type = "page";
            $widget->description = "Widget for the Injury List page";
            $widget->save();
        }



    }
}

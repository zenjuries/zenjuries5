<?php

namespace Database\Seeders;

use App\PaymentType;
use Illuminate\Database\Seeder;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $registration_payment = new PaymentType();
        $registration_payment->payment_type = "Registration";
        $registration_payment->payment_description = "Monthly subscription for Z-Agents";
        $registration_payment->save();

        $new_client_payment = new PaymentType();
        $new_client_payment->payment_type = "New Client";
        $new_client_payment->payment_description = "Annual payment for adding a new policy holder";
        $new_client_payment->save();

        $renewal_payment = new PaymentType();
        $renewal_payment->payment_type = "Renewal";
        $renewal_payment->payment_description = "Renewal payment for policy holder";
        $renewal_payment->save();
    }
}
